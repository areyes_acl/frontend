<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Sistema De Gesti�n de Intercambio</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/custom.css" />

<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="js/jquery.Rut.js"></script>

<script type="text/javascript">
	function envio(action, id) {
		$.ajax({
			url : action,
			type : 'POST',
			data : $("#" + id).serialize(),
			beforeSend : function() {
				$('#loading').show();
			}
		}).done(function(resp) {
			$("body").empty().append(resp);
		}).error(function(error, status, setting) {
			$("#error").empty().append(error);

		}).always(function(error, status, setting) {
			$('#loading').hide();
		});
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		function formatoRut(texto){
			texto = texto.toUpperCase();
			var resultado = "";
			for (var i = 0, len = texto.length; i < len; i++) {
				var numero = parseInt(texto[i].charCodeAt(0));
				if((numero >= 48 && numero <= 57) || numero == 75){
					resultado = resultado + texto[i];
				}
			}

			return resultado;
		}

		function addDots(nStr){
        	nStr += '';
        	x = nStr.split('.');
        	x1 = x[0];
        	x2 = x.length > 1 ? '.' + x[1] : '';
        	var rgx = /(\d+)(\d{3})/;
        	while (rgx.test(x1)) {
            	x1 = x1.replace(rgx, '$1' + '.' + '$2'); // changed comma to dot here
        	}
        	return x1 + x2;
    	}

		$('#rutuser').Rut({
			format: false,
			on_success: function(){
				$("#rutuserError").toggle(false);
				var texto = formatoRut($('#rutuser').val());
				var DV = texto.charAt(texto.length-1);
            	var Rut = texto.substring(0, texto.length-1);
            	Rut = addDots(Rut);
            	$('#rutuser').val(Rut + "-" + DV);
			},
			on_error: function(){
				// alert("El Rut ingresado esta incorrecto, favor ingresar nuevamente.");
				$("#rutuserError").html("El Rut ingresado esta incorrecto, favor ingresar nuevamente.");
				$("#rutuserError").toggle(true);
				
				$('#rutuser').val("");
				$('#rutuser').focus();
			}
		});

		$("#rutuser").focus(function(){
			$(this).val(formatoRut($(this).val()));
		});

		$("input:submit").click(function() {
			if($("#rutuser").val() == ""){
				return true;
			}else{
				execLogin();
				return false;
			}
		});

		function execLogin(){
			$('#rutuser').val(formatoRut($('#rutuser').val()));
			envio('checkRutRestaurarContrasenaAction','formLogin');
		}
		
		$("#rutuserError").toggle(false);

		$("#rutuser").focus();

		$("#irLogin").click(function(){
			logout();
			return false;
		});
	});
</script>

</head>

<body>
	<div id="loading" style="display: none;">
		<img style="position: relative; left: 50%; top: 30%; margin-left: -50px; height: 75px; width: 75px" src="./img/loading.gif">
	</div>

	<header class="navbar navbar-expand-sm navbar-dark py-4">
		<!--
		Imagen de fondo roja reemplazada por color de fondo rojo
		<img src="img/login/logo.png" />
		-->
		<div class="mx-auto d-sm-flex d-block flex-sm-nowrap">
	        <a class="navbar-brand" href="#">
	            Sistema De Gesti&oacute;n De Intercambio
	        </a>
	    </div>
	</header>
	
	<!-- class="login-block" -->
	<section class="my-5">
	
		<div class="text-center shadow py-5 mb-5 bg-white rounded col-4 mx-auto">
		
			<% if (request.getSession().getAttribute("usuarioLog") == null){ %>
			<div>
			
				<h1 class="h3 mb-4">Restaurar Contrase�a</h1>
				<span class="h5">Para continuar, favor ingrese su RUT</span>
				
				<form id="formLogin" method="POST" class="mt-3 col-7 mx-auto">
					
					<div class="form-group">
						<span id="rutuserError" class="text-danger"></span>
						<input class="form-control rounded-pill py-4" type="text" name="rutuser" id="rutuser" placeholder="RUT" required autofocus />
					</div>
					
					<div class="form-group">
						<input type="submit" class="form-control rounded-pill btn btn-primary" value="Continuar" />
					</div>
					
					<a class="mt-4" href="" id="irLogin">Volver</a>
					<s:actionmessage />
					<s:actionerror />
				</form>
				
				<% if (request.getSession().getAttribute("error") != null){ %>
					<div class="alert alert-danger font-weight-bold mt-4" role="alert">
						<%=request.getSession().getAttribute("error").toString() %>
					</div>
				<% } %>

				<% }else{ %>
					<script>envio('inicio', '');</script><%
				} %>
			</div>
			
			<div id="error"></div>
			
		</div>
		
	</section>
</body>
</html>
