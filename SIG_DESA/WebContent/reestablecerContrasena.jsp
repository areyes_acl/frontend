<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Sistema De Gesti�n de Intercambio</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/custom.css" />

<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>

<script type="text/javascript">
	function envio(action, id) {
		$.ajax({
			url : action,
			type : 'POST',
			data : $("#" + id).serialize(),
			beforeSend : function() {
				$('#loading').show();
			}
		}).done(function(resp) {
			$("body").empty().append(resp);
		}).error(function(error, status, setting) {
			$("#error").empty().append(error);

		}).always(function(error, status, setting) {
			$('#loading').hide();
		});
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("input:submit").click(function() {
			return false;
		});

		$("#txtUserName").focus();

		$("#irLogin").click(function(){
			location.reload();
			return false;
		});
	});
</script>

</head>

<body>
	<div id="loading" style="display: none;">
		<img style="position: relative; left: 50%; top: 30%; margin-left: -50px; height: 75px; width: 75px" src="./img/loading.gif">
	</div>

	<header class="navbar navbar-expand-sm navbar-dark py-4" data-toggle="affix">
		<!--
		Imagen de fondo roja reemplazada por color de fondo rojo
		<img src="img/login/logo.png" />
		-->
	    <div class="mx-auto d-sm-flex d-block flex-sm-nowrap">
	        <a class="navbar-brand" href="#">
	            Sistema De Gesti&oacute;n De Intercambio
	        </a>
	    </div>
	</header>
	
	<section class="login-block">
		<div>
			<% if (request.getSession().getAttribute("usuarioLog") == null){ %>
			<div>
				<h1>Restaurar Contrase�a</h1>
				<h2>Para continuar reponda sus preguntas secretas</h2>

				<% if (request.getSession().getAttribute("error") != null){ %>
					<h1><%=request.getSession().getAttribute("error").toString() %></h1>
				<% } %>
				<form id="formLogin">
					<input type="text" name="username" id="txtUserName" />
					<input type="submit" value="Continuar" onclick="envio('login','formLogin');" />
					<a href="#" id="irLogin">Volver</a>
					<s:actionmessage />
					<s:actionerror />
				</form>

				<% }else{ %>
					<script>envio('inicio', '');</script><%
				} %>
			</div>
			<div id="error"></div>
		</div>
	</section>
</body>
</html>
