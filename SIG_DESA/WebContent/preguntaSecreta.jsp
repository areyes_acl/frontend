<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="paginas/loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/utils.js"></script>

<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>

<script type="text/javascript">
	var jsonSalida = new Object();
	var lista = "";

	$(document).ready(function(){
		cargar();
	});

	function cargar(){
		var respuestas = [];
		var sidusuario = $("#sidusuario").val();

		$.ajax({
			url : 'listaPreguntasSecretasAction',
			type : 'POST',
			dataType : "json",
			data : "sidusuario="+ sidusuario
		})
		.done(
			function(resp){
				var datos = (resp.listaPreguntas).split("~");

				$('#ps1').append($('<option>', {value: "", text: "Seleccione pregunta secreta"}));
				$('#ps2').append($('<option>', {value: "", text: "Seleccione pregunta secreta"}));
				$('#ps3').append($('<option>', {value: "", text: "Seleccione pregunta secreta"}));

				if (datos[0].split(":")[1] == 0){
					for (var i = 1; i < datos.length - 1; i++) {
						var fila = datos[i].split("|");
						$('#ps1').append($('<option>', {value: fila[0], text: fila[1]}));
						$('#ps2').append($('<option>', {value: fila[0], text: fila[1]}));
						$('#ps3').append($('<option>', {value: fila[0], text: fila[1]}));
					}
				}
			}
		)
		.error(function(error, status, setting) {
			alert("Error TipoTransac: " + error);
		});

		$.ajax({
			url : 'listaPreguntasRespuestasSecretasUsuarioAction',
			type : 'POST',
			dataType : "json",
			data : "sidusuario="+ sidusuario
		})
		.done(
			function(resp){
				var datos = (resp.listaPreguntasRespuestasUsuario).split("~");

				if (datos[1] != ""){
					for (var i = 1; i < datos.length - 1; i++){
						var fila = datos[i].split("|");
						respuestas[fila[1]] = fila[3];
					}
				}

				var cont = 1;
				respuestas.forEach(function myFunction(item, index){
					if(cont == 1){
						var valor1 = $("#ps1").val();
						if(valor1 == null){
							cargar();
							return;
						}
						$("#ps1").val(index);
					}else if(cont == 2){
						$("#ps2").val(index);
					}else if(cont == 3){
						$("#ps3").val(index);
					}
    				
    				//$("#ps"+cont+" option[value=" + index + "]").attr('selected','selected');
    				$("#rp"+cont).val(item);
    				cont = cont + 1;
				});
			}
		)
		.error(function(error, status, setting) {
			alert("Error TipoTransac: " + error);
		});

		$('#formPreguntas').submit(function(){
			var p1 = $("#ps1").val();
			var p2 = $("#ps2").val();
			var p3 = $("#ps3").val();
			if(p1 == p2 || p2 == p3 || p1 == p3){
				alert("Las preguntas no pueden repetirse.");
				return false;
			}

			var r1 = $("#rp1").val();
			var r2 = $("#rp2").val();
			var r3 = $("#rp3").val();

			$.ajax({
				url : 'guardarPreguntasRespuestasAction',
				type : 'POST',
				data : "p1="+ p1 + "&r1=" + r1 + "&p2="+ p2 + "&r2=" + r2 + "&p3="+ p3 + "&r3=" + r3 + "&sidusuario=" + sidusuario
			}).done(function(resp){
				var datos = (resp.guardarPreguntasRespuestas).split("~");
				if (datos[0].split(":")[1] == 0){
					alert("Preguntas secretas y respuestas guardadas correctamente");
					location.reload();
				}else{
					alert("Error al guardar preguntas y respuestas secretas.")
				}
			})
			.error(function(error, status, setting){
				alert("Error TipoTransac: " + error);
			});

			return false;
		});
	};
</script>
</head>

<body>
	<div id="mainContainer" style="width: 60%;">
	<table id="principal" style="width: 60%;">
		<tbody><tr>
			<th id="th2" class="celda" style="vertical-align: top; color: white;"><img src="img/login/logo.png" style="height: 62px; float: right;">

				<div id="titulo1" style="margin-top: 18px">
					<label style="color: white; text-transform: uppercase; font-weight: bold;">
						Sistema De Gestión De Intercambio</label>
				</div> <label style="color: white; color: white; font-size: 14px;"> Fecha : <script>
					var d = new Date();
					var dia = "";
					var mes = "";
					var ano = "";

					if (parseInt(d.getDate()) < 10) {
						dia = "0" + d.getDate();
					} else {
						dia = d.getDate();
					}

					if (parseInt(d.getMonth() + 1) < 10) {
						mes = "0" + parseInt(d.getMonth() + 1);
					} else {
						mes = d.getMonth() + 1;
					}

					ano = d.getFullYear();
					//$("#th2").append(d.getDate()+"/"+parseInt(d.getMonth()+1)+"/"+d.getFullYear());
					$("#th2").append(dia + "/" + mes + "/" + ano).css("color",
							"white");
						</script></label></th>
		</tr>
		<tr>
			<td id="contenidoTd" class="celda">
				<div id="bloqueo"></div>
				<div id="loading" style="display: none;">
					<img style="position: relative; left: 50%; top: 30%; margin-left: -50px; height: 75px; width: 75px" src="./img/loading.gif">
				</div>
				<div id="scroll">
					<div id="contenidoDin">
	<div class="cajaTitulo">Preguntas Secretas</div>

	<div class="cajaContenido">
		<div class="cajaFormulario">
			<form id="formPreguntas">
				<input type="hidden" id="sidusuario" name="sidusuario" value="<%=usuarioLog.getSid()%>" />
				<div>
					Pregunta Secreta nº 1 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<select name="ps1" id="ps1" required></select>
				</div>
				<div>
					Respuesta Pregunta nº 1 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="text" name="rp1" id="rp1" maxlength="50" style="width: 210px;" required />
				</div>
				<br>
				<div>
					Pregunta Secreta nº 2 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<select name="ps2" id="ps2" required></select>
				</div>
				<div>
					Respuesta Pregunta nº 2 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="text" name="rp2" id="rp2" maxlength="50" style="width: 210px;" required />
				</div>
				<br>
				<div>
					Pregunta Secreta nº 3 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<select name="ps3" id="ps3" required></select>
				</div>
				<div>
					Respuesta Pregunta nº 3 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="text" name="rp3" id="rp3" maxlength="50" style="width: 210px;" required />
				</div>
				<br />
				<div class="botonera">
					<input type="button" id="salir" value="Salir" onclick="javascript:logout();" />
					<input type="submit" id="cambiar" value="Guardar" />
				</div>
			</form>
		</div>
	</div>
</div>
				</div>
			</td>
		</tr>
	</tbody></table>
</div>

</body></html>
