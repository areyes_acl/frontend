<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Sistema De Gesti�n de Intercambio</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/custom.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-datepicker.standalone.min.css" />

<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="js/jquery.Rut.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>

<script type="text/javascript">
	function envio(action, id) {
		$.ajax({
			url : action,
			//url:"llamadaWS",
			type : 'POST',
			data : $("#" + id).serialize(),
			beforeSend : function() {
				$('#loading').show();
			}
		//data:"numeroTarjeta="+$("#txtUserName").val()
		}).done(function(resp) {
			//$("#contenidoDin").html("");
			//$("#contenidoDin").append(resp);
			$("body").empty().append(resp);
			/*var aux = resp.cadena.split("~");
			
			if(aux[0].split("|")[1] = 0)
			{
				alert(aux[1]);	
			}else{
				alert(aux[1]);
			}*/
		}).error(function(error, status, setting) {
			$("#error").empty().append(error);

		}).always(function(error, status, setting) {
			$('#loading').hide();
		});
	}
	
	// crear FUNCTION ACTIVAR BOTON
		$.ajax({
			url : 'validarAccionRecuperarContrasena',
			type : 'POST',
			dataType : "json",
			beforeSend : function() {				
			}
		}).done(function(data) {
			// SETEAR BOTON SI EL PARAMETRO ESTA ACTIVO
			if(data.mostrarOpcionRecuperarContrasena){
				$("#restaurarContrasena").show();
			}else{
				$("#restaurarContrasena").hide();
			}
		}).error(function(error, status, setting) {
			$("#error").empty().append(error);
			console.log("valor de la variable error:"+ error);

		}).always(function(error, status, setting) {
			$('#loading').hide();
		});
	
</script>

<script type="text/javascript">
	$(document).ready(function() {
		function formatoRut(texto){
			texto = texto.toUpperCase();
			var resultado = "";
			for (var i = 0, len = texto.length; i < len; i++) {
				var numero = parseInt(texto[i].charCodeAt(0));
				if((numero >= 48 && numero <= 57) || numero == 75){
					resultado = resultado + texto[i];
				}
			}

			return resultado;
		}

		function addDots(nStr){
        	nStr += '';
        	x = nStr.split('.');
        	x1 = x[0];
        	x2 = x.length > 1 ? '.' + x[1] : '';
        	var rgx = /(\d+)(\d{3})/;
        	while (rgx.test(x1)) {
            	x1 = x1.replace(rgx, '$1' + '.' + '$2'); // changed comma to dot here
        	}
        	return x1 + x2;
    	}

		$('#txtUserName').Rut({
			format: false,
			on_success: function(){
				$("#usernameError").toggle(false);
				var texto = formatoRut($('#txtUserName').val());
				var DV = texto.charAt(texto.length-1);
            	var Rut = texto.substring(0, texto.length-1);
            	Rut = addDots(Rut);
            	$('#txtUserName').val(Rut + "-" + DV);
			},
			on_error: function(){
				//alert("El Rut ingresado esta incorrecto, favor ingresar nuevamente.");
				$("#usernameError").html("El Rut ingresado esta incorrecto, favor ingresar nuevamente.");
				$("#usernameError").toggle(true);
				$('#txtUserName').val("");
				$('#txtUserName').focus();
			}
		});

		$("#txtUserName").focus(function(){
			$(this).val(formatoRut($(this).val()));
		});

		$("input:submit").click(function() {
			if($("#txtUserName").val() == "" || $("#password").val() == ""){
				return true;
			}else{
				execLogin();
				return false;
			}
		});

		function execLogin(){
			$('#txtUserName').val(formatoRut($('#txtUserName').val()));
			envio('login','formLogin');
		}

		$("#txtUserName").focus();
		
		$("#usernameError").toggle(false);
		
		$("#restaurarContrasena").click(function(){
			envio('restaurarContrasenaAction','');
			return false;
		});
	});
</script>

</head>

<body>
	<div id="loading" style="display: none;">
		<img style="position: relative; left: 50%; top: 30%; margin-left: -50px; height: 75px; width: 75px" src="./img/loading.gif">
	</div>

	<header class="navbar navbar-expand-sm navbar-dark py-4" data-toggle="affix">
		<!--
		Imagen de fondo roja reemplazada por color de fondo rojo
		<img src="img/login/logo.png" />
		-->
	    <div class="mx-auto d-sm-flex d-block flex-sm-nowrap">
	        <a class="navbar-brand" href="#">
	            Sistema De Gesti&oacute;n De Intercambio
	        </a>
	    </div>
	</header>
	<section>
		<div class="container" style="margin-top: 3%;">
	
		    <div class="row">
		
			
				<div class="text-center col-sm-9 col-md-7 col-lg-5 mx-auto">
					<% if (request.getSession().getAttribute("usuarioLog") == null){ %>
					<div class="card card-signin shadow py-5 bg-white rounded">
						<h1 class="h3 mb-3">Iniciar Sesi&oacute;n</h1>
						<div class="card-body">
							
							<% if (request.getSession().getAttribute("error") != null){ %>
							<div class="alert alert-danger font-weight-bold mt-4" role="alert">
								<%=request.getSession().getAttribute("error").toString() %>
							</div>
							<% request.getSession().removeAttribute("error"); %>
						
							<% } %>
			
							<% }else{ %>
								<script>envio('inicio', '');</script><%
							} %>
							<form id="formLogin" method="POST" class="form-signin">
								<div class="form-group">
									<span id="usernameError" class="text-danger"></span>
									<label for="txtUserName" class="sr-only">RUT</label> 
									<input type="text" class="form-control py-4 rounded-pill" name="username" id="txtUserName" placeholder="RUT" required autofocus /> 
								</div>
							
								<div class="form-group">
									<label for="password" class="sr-only">Contrase&ntilde;a</label> 
									<input type="password" class="form-control py-4 rounded-pill" name="password" id="password" placeholder="Contrase�a" required autofocus />
								</div>
							
								<div class="form-group mt-4">
									<input type="submit" class="form-control btn btn-primary rounded-pill" value="Entrar" />
								</div>
							
								<a class="mt-4" href="#" id="restaurarContrasena">�Olvid&oacute; la contrase&ntilde;a?</a>
								<s:actionmessage />
							</form>
						</div>
					</div>	
				</div>
				
				<div id="error"></div>
			</div>
		</div>
	</section>
</body>
</html>