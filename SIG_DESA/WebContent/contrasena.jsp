<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="paginas/loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="js/jquery-1.10.0.min.js" type="text/javascript">

</script>
<script type="text/javascript" src="js/utils.js"></script>

<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>

<script type="text/javascript">
	$("#cambiar").click(
		function() {
			if ($("#passNueva1").val() == '' || $("#passNueva2").val() == '' || $("#passActual").val() == '') {
				alert("Porfavor ingrese todos los datos obligatorios .");
				return;
			}

			if ($("#passNueva1").val() == $("#passNueva2").val()) {
				$.ajax({
					url : "cambioContrasenaAction",
					type : 'POST',
					data : $("#formCont").serialize()
				}).done(function(resp){
					if(resp.cambioContrasena == null){
						alert(resp.error);
					}else{
						var resultado = resp.cambioContrasena.split(";");
						if(resultado[0] == 0){
							alert(resultado[1]);
							logout();
						}else{
							alert(resultado[1]);
						}
					}
				}).error(function(error, status, setting) {
					alert("Error1: " + error);
				});
			} else {
				alert("La nueva contraseña debe coincidir, favor de intentarlo nuevamente.");
			}
		});

	function clearView() {
		$("#passActual").val('');
		$("#passNueva1").val('');
		$("#passNueva2").val('');
	};
</script>
</head>

<body>
	<div id="mainContainer" style="width: 60%;">
	<table id="principal" style="width: 60%;">
		<tbody><tr>
			<th id="th2" class="celda" style="vertical-align: top; color: white;"><img src="img/login/logo.png" style="height: 62px; float: right;">

				<div id="titulo1" style="margin-top: 18px">
					<label style="color: white; text-transform: uppercase; font-weight: bold;">
						Sistema De Gestión De Intercambio</label>
				</div> <label style="color: white; color: white; font-size: 14px;"> Fecha : <script>
					var d = new Date();
					var dia = "";
					var mes = "";
					var ano = "";

					if (parseInt(d.getDate()) < 10) {
						dia = "0" + d.getDate();
					} else {
						dia = d.getDate();
					}

					if (parseInt(d.getMonth() + 1) < 10) {
						mes = "0" + parseInt(d.getMonth() + 1);
					} else {
						mes = d.getMonth() + 1;
					}

					ano = d.getFullYear();
					//$("#th2").append(d.getDate()+"/"+parseInt(d.getMonth()+1)+"/"+d.getFullYear());
					$("#th2").append(dia + "/" + mes + "/" + ano).css("color",
							"white");
						</script></label></th>
		</tr>
		<tr>
			<td id="contenidoTd" class="celda">
				<div id="bloqueo"></div>
				<div id="loading" style="display: none;">
					<img style="position: relative; left: 50%; top: 30%; margin-left: -50px; height: 75px; width: 75px" src="./img/loading.gif">
				</div>
				<div id="scroll">
					<div id="contenidoDin">
	<div class="cajaTitulo">Cambiar Contrase&ntilde;a</div>
	<div class="cajaContenido">
		<div class="cajaContenidoTitulo">Informaci&oacute;n de Cuenta</div>
		<div class="cajaFormulario">
			<form id="formCont">
				<input type="hidden" id="sidusuario" name="sidusuario"
					value="<%=usuarioLog.getSid()%>" />
				<div>
					Contrase&ntilde;a actual <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="password" name="passActual" id="passActual" maxlength="50" style="width: 210px;" required />
				</div>
				<div>
					Contrase&ntilde;a nueva <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="password" name="passNueva1" id="passNueva1" maxlength="50" style="width: 210px;" required />
				</div>
				<div>
					Confirmar contrase&ntilde;a <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="password" name="passNueva2" id="passNueva2" maxlength="50" style="width: 210px;" required />
				</div>
				<div class="botonera">
					<input type="button" id="salir" value="Salir" onclick="javascript:logout();" />
					<input type="button" id="cambiar" value="Cambiar" />
				</div>
			</form>
		</div>
	</div>
</div>
				</div>
			</td>
		</tr>
	</tbody></table>
</div>

</body></html>
