<%@page import = "Beans.Usuario" %>
<%@page import = "Beans.Menu" %>
<%@page import = "Beans.SubMenu" %>
<%@page import = "Beans.PermisosUsuario" %>
<%@page import = "java.util.ArrayList" %>
<%@page import = "java.util.List" %>
<style>
.active > label{
	background-image:url('img/minus.png');
	background-repeat:no-repeat;
	background-position:right;
	background-size:15px;
	display:inline-block;
	width: 95%;
	cursor: pointer;
}	
.passive > label{
	background-image:url('img/plus.png');
	background-repeat:no-repeat;
	background-position:right;
	background-size:15px;
	display:inline-block;
	width: 95%;
	cursor: pointer;
}
</style>
<%
Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");

if(usuarioLog.getSid() == 0){
	usuarioLog = null; %>
	location.reload();
	<%
}
if(usuarioLog.getPreguntas() == 1){ %>
	<script>envio('preguntaSecretaInicioAction', '');</script>
<% }else if(usuarioLog.getActualizar() == 1){ %>
	<script>envio('actualizacionContrasenaInicioAction', '');</script>
<% }else{
	List<Menu> listaMenu = usuarioLog.getListaMenu();
%>
<div id="mainContainer" class="table-responsive">
	<table id="principal" class="table table-bordered border-dark mx-auto">
		<thead class="text-white">
		<tr>
			<!-- class="celda" -->
			
			<th id="th1" class="border-dark align-center text-center">
				<!-- style="color: white; magin-top: 40px;" -->
				<label class="font-weight-bold">
					Usuario : <%=usuarioLog.getNombres() %> <%=usuarioLog.getApellidoPaterno() %> <%=usuarioLog.getApellidoMaterno() %>
				</label>
				<div>
					<a class="btn btn-light btn-sm" href="javascript:logout();"> Cerrar Sesi&oacute;n</a>
				</div>
			</th>
			
			<!--  class="celda" style="vertical-align: top;" -->
			<th id="th2" class="border-dark text-center">
				<!--  <img src="img/login/logo.png" style="height: 62px; float: right;" />  -->

				<div id="titulo1" style="margin-top: 18px">
					<!--  <label style="color: white; text-transform: uppercase; font-weight: bold;"> -->
					<label class="font-weight-bold text-uppercase">
						Sistema De Gesti&oacute;n De Intercambio
					</label>
					
				</div> 
				<!-- <label style="color: white; color: white; font-size: 14px;"> --> 
				<label>
					Fecha : 
					
					<script>
						var d = new Date();
						var dia = "";
						var mes = "";
						var ano = "";
	
						if (parseInt(d.getDate()) < 10) {
							dia = "0" + d.getDate();
						} else {
							dia = d.getDate();
						}
	
						if (parseInt(d.getMonth() + 1) < 10) {
							mes = "0" + parseInt(d.getMonth() + 1);
						} else {
							mes = d.getMonth() + 1;
						}
	
						ano = d.getFullYear();
						//$("#th2").append(d.getDate()+"/"+parseInt(d.getMonth()+1)+"/"+d.getFullYear());
						$("#th2").append(dia + "/" + mes + "/" + ano).css("color","white");
					</script>
				</label>
			</th>
		</tr>
		</thead>
		
		<tbody>
		<tr>
			<td id="menu" class="border-dark">
			
				<div style="border-top-style: none;">
				
					<% for (Menu menu: listaMenu) { %>
					
						
						<div id="main<%=menu.getSid()%>" style="cursor:pointer;"  <%if(menu.getSid() == 8){%> class="active" <%} else{%> class="passive" <%}%> onclick="abrirMenu(<%=menu.getSid() %>)">
							<label class="titulo2" ><%=menu.getNombre() %></label>
						</div>
						
						<div <%if(menu.getSid() == 8){%> class="activeSubm" <%} else{%> class="passiveSubm" <%}%>  style="all:unset;" id="subM<%=menu.getSid()%>" >
						
						<% List<SubMenu> listaSubMenu = menu.getListaSubMenus(); 
						for (SubMenu submenu: listaSubMenu) {
							List<PermisosUsuario> listaPermisos = submenu.getListaPermisos();
							for (PermisosUsuario permiso: listaPermisos) {
								if(permiso.getPermisoId() == 1){ %>
									<div class="selectHover">
										<a href="javascript:envio('<%=submenu.getLink() %>', <%=menu.getSid() %>, <%=submenu.getSid() %>);"><%=submenu.getNombre() %></a>
									</div>
								<% break;
								}
							}
						}%>
						
						</div>
						
					<%} %>
					
					<div>
						
					</div>
				</div>
				
			</td>
			
			<td id="contenidoTd" class="celda">
			
				<div id="bloqueo"></div>
				
				<div id="loading" style="display: none;">
					<img
						style="position: relative; left: 50%; top: 30%; margin-left: -50px; height: 75px; width: 75px"
						src="./img/loading.gif">
				</div>
				
				<div id="scroll">
					<div id="contenidoDin">
						<jsp:include page="paginas/inicioAplicacion.jsp" />
					</div>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
</div>
<script>
	
	function abrirMenu(sid){
		
		
		
		
		var x = document.getElementById('subM'+sid);
		
		
		
		if(window.getComputedStyle(x).display === "none"){
			$('.activeSubm').hide();
			$('.active').removeClass( "active" ).addClass( "passive" );
			$('.activeSubm').removeClass( "activeSubm").addClass( "passiveSubm" );
			$('#subM'+sid).show();
			$('#main'+sid).removeClass( "passive" ).addClass( "active" );
			$('#subM'+sid).removeClass( "passiveSubm").addClass( "activeSubm" );
			
		}
		else{
			$('#subM'+sid).hide();
			$('#main'+sid).removeClass( "active" ).addClass( "passive" );
			$('#subM'+sid).removeClass( "activeSubm").addClass( "passiveSubm" );
			
		}
		
		
	}
	
	$(document).ready(function(){
		$('#subM8 .selectHover').addClass('subMenuElegido');
		$('.passiveSubm').hide();
	});
	
	$('.selectHover a').on('click', function() {
		$(".selectHover").removeClass('subMenuElegido');
		$(this).parent().addClass('subMenuElegido');
	});
	
</script>
<% if (request.getSession().getAttribute("mensajeVencimiento") != null){ %>
	<script>alert('<%=request.getSession().getAttribute("mensajeVencimiento").toString() %>');</script>
<% }
} %>