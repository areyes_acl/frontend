<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       <script src="js/jquery.Rut.js" type="text/javascript"></script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script>

	$('.datepicker').datepicker();
	
	$(document).ready(function() {
		var f = new Date();

		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		if (mes < 10)
			mes = "0" + mes;
		if (dia < 10)
			dia = "0" + dia;
		var ano = f.getFullYear();
		$(".fecha").val(dia + "/" + mes + "/" + ano);
		
		
	});

	$("#buscar")
			.click(
					function() {
					
						
						limpiarTablas();
						
						$("#tablaObjRech").css("display", "none");
						var fechaTermino = "";
						if ($('#fechaInicio').val().length == 0) {
							alert("Debe ingresar fecha de inicio para realizar la busqueda...");
							$("#fechaInicio").focus();
							return;
						}

						if (validarFecha($('#fechaInicio').val()) == false) {
							alert("La fecha ingresada esta incorrecta, verifiquela...");
							$("#fechaInicio").focus();
							return;
						}

						if ($('#fechaTermino').val().length == 0) {
							f = new Date();

							fechaTermino = convertirFecha(f.getDate() + "/"
									+ (f.getMonth() + 1) + "/"
									+ f.getFullYear());
						} else {

							if (validarFecha($('#fechaTermino').val()) == false) {
								alert("La fecha ingresada esta incorrecta, verifiquela...");
								$("#fechaFin").focus();
								return;
							}

							fechaTermino = convertirFecha($('#fechaTermino').val());
						}

						var fechaInicio = convertirFecha($('#fechaInicio').val());

						if (fechaInicio > fechaTermino) {
							alert("La fecha de inicio es mayor a la fecha de termino, verifiquela...");
							$("#fechaInicio").focus();
							return;
						}
						//alert($("#form1").serialize());
						$.ajax({
							url : "listarObjecionRechazoAction",
							type : 'POST',
							data : $("#form1").serialize()
						}).done(function(data) {
							$("#tablaRes").empty().append(data);
						});
					});

	$("#tablaRes")
			.on(
					'click',
					'.seleccionTRX',
					function() {
						$("#microfil").val(
								$(this).parent().parent().find('td').eq('10').text());
						$("#codigoautorizacion").val(
								$(this).parent().parent().find('td').eq('11').text());
						$("#fechaefectiva").val(
								$(this).parent().parent().find('td').eq('12').text());
						$("#fechaproceso").val(
								$(this).parent().parent().find('td').eq('13').text());
						$("#binadquirente").val(
								$(this).parent().parent().find('td').eq('14').text());
						$("#leebanda").val(
								$(this).parent().parent().find('td').eq('15').text());
						$("#otrodato01").val(
								$(this).parent().parent().find('td').eq('16').text());
						$("#otrodato02").val(
								$(this).parent().parent().find('td').eq('17').text());
						$("#otrodato03").val(
								$(this).parent().parent().find('td').eq('18').text());
						$("#otrodato04").val(
								$(this).parent().parent().find('td').eq('19').text());
						$("#codigomonedatrx").val(
								$(this).parent().parent().find('td').eq('20').text());
						$("#rubrocomercio").val(
								$(this).parent().parent().find('td').eq('21').text());
					    $("#operador").val($(this).parent().parent().find('td').eq('28').text());

						var numeroTarjeta = $(this).parent().parent()
								.find('td').eq('1').text();

						//test
						//var numeroTarjeta = "9200610156088010";
						//test
						$
								.ajax({
									url : "llamadaWS",
									type : 'POST',
									data : "numeroTarjeta=" + numeroTarjeta
								})
								.done(
										function(resp) {
											var aux = resp.cadena.split("~");

											if (aux[0].split("|")[1] == 0) {
												var datos = aux[1].split("|");
												$("#nomcliente").val(datos[0]);
												var rut = datos[1];
												$("#rutcliente").val(rut);
												$("#numerocuenta").val(datos[2]);
												$("#cupo").val(datos[3]);
												$("#fechaexpiracion").val(datos[4]);
												$("#estado").val(datos[5]);
											} else {

												var mensaje = aux[1].split("|");

												if (mensaje == "Numero de tarjeta erroneo") {
													alert("No existen los datos de la cuenta del cliente para la tarjeta consultada...");
												}
												$("#nomcliente").val("");
												$("#rutcliente").val("");
												$("#numerocuenta").val("");
												$("#cupo").val("");
												$("#fechaexpiracion").val("");
												$("#estado").val("");

												//alert(aux[1]);
											}
										}).error(
										function(error, status, setting) {
											$("#error").empty().append(error);
										});
					});

	$("#exportar")
			.click(
					function() {

						if ($('#fechaInicio').val().length == 0) {
							alert("Debe ingresar fecha de inicio para exportar...");
							$("#fechaInicio").focus();
							return;
						}
						if ($('#fechaTermino').val().length == 0) {
							alert("Debe ingresar fecha de termino para exportar...");
							$("#fechaTermino").focus();
							return;
						}
						var fechaTermino = convertirFecha($('#fechaTermino').val());
						var fechaInicio = convertirFecha($('#fechaInicio').val());
						
						if (fechaInicio > fechaTermino) {
							alert("La fecha de inicio es mayor a la fecha de termino, verifiquela...");
							$("#fechaInicio").focus();
							return;
						}
						var parametros = "fechaInicio="
								+ $('#fechaInicio').val();
						if ($('#fechaTermino').val().length == 0) {
							f = new Date();
							parametros = parametros + "&fechaTermino="
									+ f.getDate() + "/" + (f.getMonth() + 1)
									+ "/" + f.getFullYear();
							fechaTermino = convertirFecha(f.getDate() + "/"
									+ (f.getMonth() + 1) + "/"
									+ f.getFullYear());
						} else {

							if (validarFecha($('#fechaTermino').val()) == false) {
								alert("La fecha ingresada esta incorrecta, verifiquela...");
								$("#fechaFin").focus();
								return;
							}
							parametros = parametros + "&fechaFin="
									+ $('#fechaTermino').val();
							fechaTermino = convertirFecha($('#fechaTermino')
									.val());
						}
						parametros += "&nombreArchivo=" + "export"
								+ "&componente="
								+ $('#tipoObjecionRechazo').val();

						location.href = "./exportarDatosExcelObjRechAction?"
								+ parametros;

					});
					
	function limpiarTablas() {
	
		// DATOS DE LA CUENTA DEL CLIENTE
		$("#nomcliente").val("");
		$("#rutcliente").val("");
		$("#numerocuenta").val("");
		$("#cupo").val("");
		$("#fechaexpiracion").val("");
		$("#estado").val("");
		
		// DATOS DE LA TRANSACCION SELECCIONADA
		$("#microfil").val("");
		$("#codigoautorizacion").val("");
		$("#otrodato01").val("");
		$("#fechaefectiva").val("");
		$("#fechaproceso").val("");
		$("#otrodato02").val("");
		$("#binadquirente").val("");
		$("#leebanda").val("");
		$("#otrodato03").val("");
		$("#codigomonedatrx").val("");
		$("#rubrocomercio").val("");
		$("#otrodato04").val("");
		$("#operador").val("");
	}
</script>
<form id="form1">

	<input type="hidden" id="componente" value="Rechazos Diarias" /> 
	
	<input type="hidden" id="idTabla" value="tablaRes" /> 
	
	<input type="hidden" value="15" name="cantReg" id="cantReg" />

	<div id="popup" style="display: none;">
		<div id="popUpContenido" class="content-popup">Hola Mundo</div>
	</div>

	<!-- <div class="titulo">RECHAZOS DIARIOS</div> -->
	<div class="col-12 mx-auto mt-4 text-align-center text-center font-weight-bold ">
		RECHAZOS DIARIOS
	</div>
	
	<hr/>
	
	<input type="hidden" name="tipoObjecionRechazo" value="REC"
		id="tipoObjecionRechazo" /> <!-- <br style="line-height: 5px" />  -->

	<div class="my-4">
		<!-- <table width="100%" class="contenedorInput">  -->
		<table width="100%" class="table my-0 table-sm mx-auto">
		<tbody>
		
			<tr class="text-center">
				<td class="text-right align-middle">
					<label class="my-0">Fecha Inicio:</label>
				</td>
				
				<td class="align-middle">
					<input type="text" name="fechaInicio" id="fechaInicio"
						size="10" maxlength="10" value="" class="campoObligatorio fecha form-control form-control-sm mx-auto text-center col-12 px-0 datepicker" placeholder="DD/MM/AAAA"/>
				</td>
				
				<td class="text-right align-middle">
					<label class="my-0">Fecha Hasta:</label>
				</td>
				
				<td class="align-middle">
					<input type="text" name="fechaTermino" id="fechaTermino"
						size="10" maxlength="10" value="" class="campoObligatorio fecha form-control form-control-sm mx-auto text-center col-12 px-0 datepicker" placeholder="DD/MM/AAAA" />
				</td>
				
				<td colspan="4" align="center" class="form-group align-middle">&nbsp; 
					<input type="button" class="btn btn-primary"
						value="Buscar" id="buscar" />
				</td>
			</tr>
			
		</tbody>
		</table>
	</div>

	<!-- <br style="line-height: 5px" /> -->

	<div class="cajaContenido">
		<div>
			<!-- <table width="100%" class="tablaContenido" id="tablaObjRech">  -->
			<!-- <table width="100%" class="tablaContenido" id="tabla1">
				<tr>
					<th style="width: 5%;">SEL</th>
					<th style="width: 15%;">NRO TC</th>
					<th style="width: 15%;">FECHA TRANSACC.</th>
					<th style="width: 20%;">COMERCIO</th>
					<th style="width: 5%;">PA&Iacute;S</th>
					<th style="width: 10%;">MONTO ORIG.</th>
					<th style="width: 10%;">MONTO</th>
					<th style="width: 10%;">COD. RAZON</th>
					<th style="width: 10%;">ESTATUS</th>
				</tr>
			</table>-->
		</div>
		<!--  class="tablaContenido -->
		<div style="width: 100%;" id="tablaRes" class="tablaContenido">

		</div>
	</div>
	<!-- <br style="line-height: 5px" />  -->
	<div>
		<!-- class="contenedorInput" -->
		<table width="100%" class="table-borderless">
			<tr>
				<td align="center"><input type="button" value="Exportar Tabla"
					id="exportar" name="exportar" class="btn btn-primary" /></td>
			</tr>
		</table>
	</div>
	<!-- <br style="line-height: 5px" /><br>  -->
	
	<hr size="2px">
	
	<!-- class="subTitulo -->
	<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
		font-weight-bold ">
		
		Datos De La Cuenta Del Cliente
	</div>

	<div class="mb-4">
		<!-- class="contenedorInput  -->
		<table width="100%" class="table table-sm small">
			<tr>
				<td>Cliente :</td>
				<td>
					<input type="text" name="nomcliente" id="nomcliente"
						class="campoSalida form-control form-control-sm" size="50" /></td>
				<td>Rut :</td>
				<td>
					<input type="text" name="rutcliente" id="rutcliente"
						class="campoSalida form-control form-control-sm" /></td>
			</tr>

			<tr>
				<td>N&ordm; De Cuenta :</td>
				<td><input type="text" class="campoSalida form-control form-control-sm" name="numerocuenta"
					id="numerocuenta" /></td>
				<td>Cupo :</td>
				<td><input type="text" class="campoSalida form-control form-control-sm" name="cupo"
					id="cupo" /></td>
			</tr>

			<tr>
				<td>Fecha Exp.:</td>
				<td><input type="text" class="campoSalida form-control form-control-sm"
					name="fechaexpiracion" id="fechaexpiracion" /></td>
				<td>Estado :</td>
				<td><input type="text" class="campoSalida form-control form-control-sm" name="estado"
					id="estado" /></td>
			</tr>

		</table>
	</div>

	<!-- <br style="line-height: 5px" />  -->


	<div class="mb-2 mx-auto col-6 small text-align-center text-center 
		font-weight-bold">
		
		Datos De La Transacci&oacute;n Seleccionada
	</div>

	<div>
		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm small">

			<tr>
				<td>Microfilm :</td>
				<td><input type="text" name="microfil" id="microfil"
					class="campoSalida form-control form-control-sm" /></td>
				<td>Cod. Autorizac. :</td>
				<td><input type="text" name="codigoautorizacion"
					id="codigoautorizacion" class="campoSalida form-control form-control-sm" /></td>
				<td>Cod.Proces:</td>
				<td><input type="text" name="otrodato01" id="otrodato01"
					class="campoSalida form-control form-control-sm" /></td>
			</tr>

			<tr>
				<td>Fecha Efectiva :</td>
				<td><input type="text" name="fechaefectiva" id="fechaefectiva"
					class="campoSalida form-control form-control-sm" /></td>
				<td>Fecha Proceso :</td>
				<td><input type="text" name="fechaproceso" id="fechaproceso"
					class="campoSalida form-control form-control-sm" /></td>
				<td>Conv.Conc. :</td>
				<td><input type="text" name="otrodato02" id="otrodato02"
					class="campoSalida form-control form-control-sm" /></td>
			</tr>

			<tr>
				<td>Bin Adquiriente :</td>
				<td><input type="text" name="binadquirente" id="binadquirente"
					class="campoSalida form-control form-control-sm" /></td>
				<td>Lee Banda :</td>
				<td><input type="text" name="leebanda" id="leebanda"
					class="campoSalida form-control form-control-sm" /></td>
				<td>Conv.Fact. :</td>
				<td><input type="text" name="otrodao03" id="otrodato03"
					class="campoSalida form-control form-control-sm" /></td>
			</tr>

			<tr>
				<td>C&oacute;digo Moneda Trx. :</td>
				<td><input type="text" name="codigomonedatrx"
					id="codigomonedatrx" class="campoSalida form-control form-control-sm" /></td>
				<td>Rubro Del Comercio:</td>
				<td ><input type="text" name="rubrocomercio" id="rubrocomercio"
					class="campoSalida form-control form-control-sm" /></td>
				<td>Pto. Servicio:</td>
				<td><input type="text" name="otrodao04" id="otrodato04"
					class="campoSalida form-control form-control-sm" /></td>
			</tr>
			
			<tr>
				<td>Operador :</td>
				<td class="border-right-0"><input type="text" name="operador" id="operador" class="campoSalida form-control form-control-sm" size="40"></td>
				<td colspan="4" class="border-left-0"></td>
			<!-- 
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			 -->
			</tr>

		</table>
	</div>

	<br style="line-height: 5px" />

	
</form>