<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Cobros de Cargo Visa</title>
<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       <script src="js/jquery.Rut.js" type="text/javascript"></script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
	   
	<script>
			$(".datepicker").datepicker();
			
	   		$(document).ready(function(){
	   			cargaOperador();
	   			cargaCodigosDeRazon();

				var f = new Date();
		
				var dia = f.getDate();
				var mes = (f.getMonth() + 1);
				if (mes < 10)
					mes = "0" + mes;
					
				if (dia < 10)
					dia = "0" + dia;
				var ano = f.getFullYear();
				$(".fecha").val(dia + "/" + mes + "/" + ano);			
	   			$('#otraGestion').prop("disabled", true);
				
	   		});
	   		
	   		function cargaCodigosDeRazon(){
	   			$.ajax({
	   				url: 'cargaCodigosRazonVisa',
	   				type: 'POST'
	   			}).done(function(data){
	   				data.listaCodigosRazonVisa;
	   				var datos = (data.listaCodigosRazon).split("~");
	   				
	   				var strCombo = "";
			   		strCombo += "<option disabled='disabled' selected='selected'>Seleccione ...</option>"
			   		for(var i=1;i<datos.length-1;i++){			
			   			var fila = datos[i].split("|");
			   			strCombo += "<option value='"+fila[0]+"' data-info='"+fila[4]+"' data-amount='"+fila[3]+"' >"+fila[2]+" - "+fila[1]+"</option>";
					}
			   			
			   		$("#codigosRazon").append(strCombo);
	   				
	   			});
	   			
	   			
	   		
	   		}
	   		
	   		function cargaOperador(){
				$.ajax({
			   			url:'cargarComboOperadores',
			   			type:'POST'
			   		}).done(function(data){
			   			data.listaOperadores;
			   			var datos = (data.listaOperadores).split("~");
			   			
			   			var strCombo = "";
			   			
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   				
			   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
							}
			   			
			   			$("#idOperador").append(strCombo);
			   		});
			}
	   </script>
	   <script>
	   		
	   		$('#buscar').on('click', function(){
	   		
	   			$("#idTrx").val("");					
				$("#mitTrx").val("");	
				$("#centralProcessingDate").val("");
				$("#eventDate").val("");
				$("#accountNumberExtension").val("");
				$("#destinationBin").val("");
				$("#settlementFlag").val("");
				$("#transactionIdentifier").val("");
				$("#reserved").val("");
				$("#reimbursementAttribute").val("");
				$("#messageText").val("");
				$("#amount").val("");
				$("#TransaccionConGestion").val("");
	   			$("#reverseDate").val("");
				$("#usuarioReverse").val("");
				$("#codeReverse").val("");
				$("#mitReverse").val("");
				$("#messageReverse").val("");
				$("#detallesReversa").css("display", "none");
				$("#tituloReversa").css("display", "none");
				
	   			var fechaDesde = $('#fechaDesde').val();
	   			var fechaHasta = $('#fechaHasta').val();
	   			var tipoTrx = $('#tipoTrx').val();
	   			var estado = $('#estado').val();
	   			var operador = $('#idOperador').val();
	   			var numPagina = 1;
	   			
	   			
	   			$('#otraGestion').prop("disabled", true);
				
		   		buscarTrxCobroCargoVisa(fechaDesde, fechaHasta, tipoTrx, numPagina, estado, operador);
	   	   			
	   		});
	   		
	   		function buscarTrxCobroCargoVisa(fechaDesde, fechaHasta, tipoTrx, numPagina, estado, operador){
	   		
	   			//alert(fechaDesde + ' - '+ fechaHasta + ' - ' + tipoTrx + ' - ' + numPagina + ' - ' + estado + ' - ' + operador);
	   		
	   			if(typeof numPagina == 'undefined'){
	   				numPagina = 1;
	   			}
	   		
	   			if (fechaDesde.length  == 0){
					alert("Debe ingresar una fecha de Inicio...");
					$("#fechaDesde").focus();
					return;
				}
				
				if (validarFecha(fechaDesde) == false){
					alert("La fecha ingresada es incorrecta, verifíquela...");
					$("#fechaDesde").focus();
					return;
				}
				
				if (fechaHasta.length  == 0){
					alert("Debe ingresar una fecha de término...");
					$("#fechaHasta").focus();
					return;
				}
				
				if (validarFecha(fechaHasta) == false){
					alert("La fecha ingresada es incorrecta, verifíquela...");
					$("#fechaHasta").focus();
					return;
				}
				
				//alert($('#tipoTrx').val());
				if(tipoTrx == ''){
					alert("Debe seleccionar un tipo de Transacción");
					return;
				}
				
				$.ajax({
				url:"listarCobroCargoVisaAction",
					type:'POST',
					data: {'fechaDesde': fechaDesde, 'fechaHasta' : fechaHasta, 'TipoTrx' : tipoTrx, 'numPagina' : numPagina, 'estadoTrx': estado, 'operador' : operador}
				}).done(function(data){
					//console.log(data);
				
					$("#tablaRes").empty().append(data);
					
					if($("#estado").val() === 'OU_OK'){
						$('#otraGestion').prop("disabled", true);
					}
					
					if($("#estado").val() === 'OU_PEN'){
						$('.select-accion').css('display', '');
					}else{
						$('.select-accion').css('display', 'none');
					}
				});
	   		
	   		}
	   		
	   </script>
	   <script>
	   
	   $("#tablaRes").on('click','.seleccionTRX',function(){	   		
	   		$("#idTrx").val($(this).parent().parent().find('td').eq('24').text());					
			$("#mitTrx").val($(this).parent().parent().find('td').eq('1').text());	
			$("#centralProcessingDate").val($(this).parent().parent().find('td').eq('10').text());
			$("#eventDate").val($(this).parent().parent().find('td').eq('11').text());
			$("#accountNumberExtension").val($(this).parent().parent().find('td').eq('12').text());
			$("#destinationBin").val($(this).parent().parent().find('td').eq('13').text());
			$("#settlementFlag").val($(this).parent().parent().find('td').eq('14').text());
			$("#transactionIdentifier").val($(this).parent().parent().find('td').eq('15').text());
			$("#reserved").val($(this).parent().parent().find('td').eq('16').text());
			$("#reimbursementAttribute").val($(this).parent().parent().find('td').eq('17').text());
			$("#messageText").val($(this).parent().parent().find('td').eq('18').text());
			$("#amount").val($(this).parent().parent().find('td').eq('6').text());
			$("#TransaccionConGestion").val($(this).parent().parent().find('td').eq('25').text());
			$("#reverseDate").val($(this).parent().parent().find('td').eq('19').text());
			$("#usuarioReverse").val($(this).parent().parent().find('td').eq('20').text());
			$("#codeReverse").val($(this).parent().parent().find('td').eq('21').text());
			$("#mitReverse").val($(this).parent().parent().find('td').eq('22').text());
			$("#messageReverse").val($(this).parent().parent().find('td').eq('23').text());
			
			
			if($("#TransaccionConGestion").val() == ''){
				$('#otraGestion').prop("disabled", false);
				$("#detallesReversa").css("display", "none");
				$("#tituloReversa").css("display", "none");
			}
			else{	
				$('#otraGestion').prop("disabled", true);
				$("#detallesReversa").css("display", "");
				$("#tituloReversa").css("display", "");
			}
			
		});
		
		$("#otraGestion").click(function() {
		
				$("#popup").css("height", $(document).height());
			
				$("#popup").css({
					display : 'block'
				});
				$("#popUpContenido").css({
					display : 'block'
				});
				centradoDiv('popUpContenido', 'big');
			
		});
	   $("#cancelarOtraGestion").click(function(){
       		
       		$("#popup").css("height", $(document).height());
       		
	       	$("#popup").css({
				display : 'none'
			});
			$("#popUpContenido").css({
				display : 'none'
			});
			$('#codigosRazon option').prop('selected', function() {
		        return this.defaultSelected;
		    });
		    document.getElementById("glosaInfo").value = '';
		    document.getElementById("glosa").value = '';
		    
		    
	       	//$('#popUpContenido').empty().css({display:'none'});
	       	//$("#buscarTransaccion").click();
       	
       });
       
       $("#guardarRvs").click(function(){
       
       var codigosRazon = $('#codigosRazon').val();
       if(codigosRazon == null){
       		alert("Debes seleccionar un código de Razón");
       		return;
       }
       
        var r = confirm("¿Estás seguro que quieres reversar esta transacción?");
		if (r == true) {
		
		  var amount = parseInt($("#amount").val());
			var maxamount = parseInt($("#maxamount").val());
			var codigosRazon = $("#codigosRazon :selected").val();
	       	var sidTrx = parseInt($("#idTrx").val());
	       	var mitTrx = $("#mitTrx").val();
	       	var glosa = $("#glosa").val();
	       	
	       	
	       	
	       	if(amount > maxamount && maxamount > 0){
	       	
	       		alert("El monto de origen no puede ser mayor al monto máximo");
	       		return;
	       	}
	       	
	       	
	       	var datos = {'sidTrx' : sidTrx, 'mitTrx' : mitTrx, 'codRazon' : codigosRazon, 'glosa' : glosa};
	       	console.log(datos);
	       	
	       	$.ajax({
	       		url: 'reversarCobroCargoVisaAction',
	       		data: datos,
	       		type: 'POST',
	       		success: function(data){
	       			alert("Se reversado correctamente la transacción");
	       			$("#popup").css({
						display : 'none'
					});
					$("#popUpContenido").css({
						display : 'none'
					});
					$('#codigosRazon option').prop('selected', function() {
				        return this.defaultSelected;
				    });
				    document.getElementById("glosaInfo").value = '';
			  		document.getElementById("glosa").value = '';
			  		var fechaDesde = $('#fechaDesde').val();
		   			var fechaHasta = $('#fechaHasta').val();
		   			var tipoTrx = $('#tipoTrx').val();
		   			var estado = $('#estado').val();
		   			var operador = $('#idOperador').val();
		   			var numPagina = 1;
		   			if(typeof numPagina == 'undefined'){
		   				numPagina = 1;
		   			}
		   			
		   			$('#otraGestion').prop("disabled", true);
			   		$("#detallesReversa").css("display", "none");
					$("#tituloReversa").css("display", "none");
					
					
					$("#idTrx").val("");					
					$("#mitTrx").val("");	
					$("#centralProcessingDate").val("");
					$("#eventDate").val("");
					$("#accountNumberExtension").val("");
					$("#destinationBin").val("");
					$("#settlementFlag").val("");
					$("#transactionIdentifier").val("");
					$("#reserved").val("");
					$("#reimbursementAttribute").val("");
					$("#messageText").val("");
					$("#amount").val("");
					$("#TransaccionConGestion").val("");
					$("#reverseDate").val("");
					$("#usuarioReverse").val("");
					$("#codeReverse").val("");
					$("#mitReverse").val("");
					$("#messageReverse").val("");
				   	buscarTrxCobroCargoVisa(fechaDesde, fechaHasta, tipoTrx,numPagina, estado, operador);
	       		},
	       		error: function(){
	       			alert("Se ha producido un error");
	       		}
	       	});
       	
		} else {
		  return;
		}
		        
       
		
       	
       });
       
		
		// muestra la descripción
		function showDescription(description) {
			if(description === null || description === "") {
				$('#descripcion').append(
					'<div id="page"> \ <p class="texto">Sin descripcion \ </div>');
				$("#tablaDescripcion").css("display", "");
			}
			else {
				$('#descripcion').append(
					'<div id="page"> \ <p class="texto">' + description
							+ ' \ </div>');
				$("#tablaDescripcion").css("display", "");
			}
		}
		
		// esconde la descripcion
		function hideDescription() {
			$('#descripcion').html("");
			$("#tablaDescripcion").css("display", "none");
		}
		
		function deleteReversa(sid){
		
			var r = confirm("¿Estás seguro que quieres cancelar la reversa de esta transacción?");
			if (r == true) {
			
				$.ajax({
					url: 'eliminarReversaAction',
					type: 'POST',
					data: {'sidTrx' : sid},
					success: function(data){
						if(data.codigoError == 0){
							alert("Se ha eliminado la gestión sobre esta transacción");
							var fechaDesde = $('#fechaDesde').val();
				   			var fechaHasta = $('#fechaHasta').val();
				   			var tipoTrx = $('#tipoTrx').val();
				   			var estado = $('#estado').val();
				   			var operador = $('#idOperador').val();
				   			var numPagina = 1;
				   			if(typeof numPagina == 'undefined'){
				   				numPagina = 1;
				   			}
				   			
				   			$('#otraGestion').prop("disabled", true);
				   			$("#detallesReversa").css("display", "none");
							$("#tituloReversa").css("display", "none");
					   		
							$("#idTrx").val("");					
							$("#mitTrx").val("");	
							$("#centralProcessingDate").val("");
							$("#eventDate").val("");
							$("#accountNumberExtension").val("");
							$("#destinationBin").val("");
							$("#settlementFlag").val("");
							$("#transactionIdentifier").val("");
							$("#reserved").val("");
							$("#reimbursementAttribute").val("");
							$("#messageText").val("");
							$("#amount").val("");
							$("#TransaccionConGestion").val("");
							$("#reverseDate").val("");
							$("#usuarioReverse").val("");
							$("#codeReverse").val("");
							$("#mitReverse").val("");
							$("#messageReverse").val("");
				   			buscarTrxCobroCargoVisa(fechaDesde, fechaHasta, tipoTrx,numPagina, estado, operador);
						}
						else{
							alert("Se ha producido un error");
						}
					},
					error: function(){
					}
				
				});
				
			}
			else{
				return;
			}	
			
		}
		
		function llenarGlosa(descr, amount){
			//alert(descr + ' - ' + amount)
			if(descr == 'null'){
				descr = '';
			}
			$("#maxamount").val("");
			$("#maxamount").val(amount);
			
			if(amount == '0'){
				amount = 'N/A';
			}
			
			document.getElementById("glosaInfo").value = '';
			document.getElementById("glosaInfo").value += 'Monto máximo: ' + amount + ' \n';
			document.getElementById("glosaInfo").value += 'Información: ' + descr;
		}
		
		
	   		$('#exportar').on('click', function(){
	   			//alert('ola ke ase');
	   			
	   			DataExportar = "";
				DatasExcel = "";
				OperadorExcel = document.getElementById('idOperador').options[document.getElementById('idOperador').selectedIndex].text.toUpperCase();
				
				
						idTablaExportar = "myTable";
						
						DataExportar += "TIT|| | | | | | | | | | | |"+OperadorExcel+"| | | | | | | | | | ~"
						DataExportar += "TIT|TIPO TRX|NÚMERO DE CUENTA|BIN ORIGEN|COD. RAZÓN|COD. PAÍS|MONTO ORIGEN|MONEDA ORIGEN|MONTO DESTINO|MONEDA DESTINO|FECHA DE PROCESAMIENTO|FECHA DEL EVENTO|EXT. NÚMERO DE CUENTA|BIN DESTINO|BANDERA|IDENTIFICADOR DE TRX|RESERVADO|ATTR. REEMBOLSO|MENSAJE|FECHA DE INGRESO (REVERSA)|USUARIO (REVERSA)|CODIGO MOTIVO (REVERSA)|TIPO TRX (REVERSA)|MENSAJE (REVERSA)~";
						//DataExportar += "DET|~"
						DataExportar += exportarExcelCobroCargo(idTablaExportar,
								DatasExcel);

						exportarTc46(DataExportar, "export");
				
				
	   			
	   		});
	   
	   </script>
	   
</head>
<body>
	<!-- <div class="titulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		COBROS DE CARGO VISA
	</div>
	
	<hr/>
	
	<div id="popUpContenido" class="content-popup-cobro-cargo" style="display: none;">
		<form id="formReversar">
			<!-- <div class="titulo">  -->
			<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
				REVERSAR 
			</div>
			<hr>
			<!-- <br style="line-height:5px"/>  -->
			
			<input type="hidden" id="idTrx" name="idTrx">
			<input type="hidden" id="mitTrx" name="mitTrx">
			<input type="hidden" id="amount" name="amount">
			<input type="hidden" id="maxamount" name="maxamount">
			<div>
				<!-- class="contenedorInput" -->
				<table width="100%" class="table table-sm small mx-auto">
			    	<tr>
			        	<td class="align-middle">COD. RAZ&Oacute;N:</td>
		        		<td class="align-middle">
		        			<select class="custom-select" onchange="llenarGlosa(this.options[this.selectedIndex].getAttribute('data-info'), this.options[this.selectedIndex].getAttribute('data-amount'));" name="codigosRazon" id="codigosRazon">
		        			</select>
		        		</td>
		 			</tr>
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/> -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table width="100%" class="table table-sm small mx-auto"> 			
					<tr>
			        	<td class="CuerpoNormal align-middle">GLOSA INFORMATIVA: </td>
			 		</tr>
			 		<tr>
			 			<td class="align-middle">
			 				<!-- style="width: 800px;" -->
			 				<textarea disabled id="glosaInfo" rows="5" cols="58" name="glosaInfo" data-maxsize="1999" class="form-control form-control-sm"></textarea>
			 			</td>
			 		</tr>
			 			
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/> -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table width="100%" class="table table-sm small mx-auto"> 			
					<tr>
			        	<td class="CuerpoNormal align-middle">MOTIVO DE LA REVERSA: </td>
			 		</tr>
			 		<tr>
			 			<td class="align-middle">
			 				<!-- style="width: 800px;" -->
			 				<textarea id="glosa" rows="5" cols="58" name="glosa" maxlength="70" class="form-control form-control-sm"></textarea>
			 			</td>
			 		</tr>
			 			
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/> -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table  width="100%" class="table table-sm table-borderless">
			       		<tr>
		 				<td class="align-middle" align="center">
		 				    <input id="guardarRvs" type="button"  value="Guardar" class="btn btn-primary"/>
							<input type="button"  value="Cancelar" name="cancelarOtraGestion" id="cancelarOtraGestion" class="btn btn-primary">
		 				</td>
		 			</tr>
				</table>
			</div>
	      </form>
	</div>
	<div id="popup" style="display: none;">
		
	</div>
	
	<!-- <br style="line-height:5px"/> -->
	
	<div>
			<!-- class="contenedorInput" -->
      		<table width="100%" class="table table-sm">
               <tr class="text-right">
           			<td class="align-middle">Desde:</td>
           			<td class="align-middle text-center" colspan="2">
           				<!--  size="10" placeholder="dd/mm/aaaa" -->
           				<input type="text" name="fechaDesde" id="fechaDesde"maxlength="10" class="campoObligatorio fecha form-control form-control-sm text-center col-5 mx-auto datepicker" placeholder="DD/MM/AAAA" />
             		</td>
             		<td class="align-middle">Hasta:</td>
             		<td class="align-middle text-center" colspan="2">
             			<!--  size="10" placeholder="dd/mm/aaaa" -->
           				<input type="text" name="fechaHasta" id="fechaHasta" maxlength="10" class="campoObligatorio fecha form-control form-control-sm text-center col-6 mx-auto datepicker" placeholder="DD/MM/AAAA" />
             		</td>
             		<td class="align-middle" rowspan="2">
             			<input type="button"  id="buscar" value="Buscar" class="btn btn-primary">
             		</td>
       			</tr>
       			<tr class="text-right">
       				<td class="align-middle">TIPO TRX:</td>
             		<td class="align-middle">
             			<!-- style="width:150px" -->
             			<select id="tipoTrx" class="custom-select">
             		 		<option value="">Seleccione...</option>
             		 		<option value="10">Cobros de Cargo</option>
             				<option value="20">Devoluci&oacute;n de Cobros de Cargo</option>
             			</select>
             		</td>
             		<td class="align-middle">Estado:</td>
             		<td class="align-middle">
             			<select id="estado" class="select-estado custom-select">
             		 		<option value="0">Todos</option>
             		 		<option value="1">Registrado</option>
             		 		<option value="OU_PEN">Pendiente</option>
             		 		<option value="OU_OK">Enviado</option>
             			</select>
             		</td>
             		<td class="align-middle">Operador:</td>
             		<td class="align-middle">
           				<select id="idOperador" class="custom-select">
           				</select>
             		</td>
       			</tr>
      		</table>
      </div>
	
	<!-- <br style="line-height:5px"/>  -->
	
	<div id="tablaRes"></div>
		
		<!-- <br style="line-height: 5px" />  -->
		
		<div class="mt-2 col-6 mx-auto">
			<!-- style="display:none;width: 500px;margin-left: 150px;" class="contenedorInput"-->
			<table id="tablaDescripcion"  class="table table-sm" style="display:none;">
				<thead>
					<tr class="text-center">
						<th class="align-middle">
							<!-- style="font-weight: bold;width: 500px;font-size: 14px;background: #a5bad1;" -->
							<div class="font-weight-bold">
								Descripci&oacute;n
							</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center">
						<!--  style="aling: center;width: 500px" -->
						<td id ="descripcion">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	
	<!-- <br style="line-height: 5px" />  -->
    
    <hr size="2px">
    
    <!-- class="subTitulo" -->
	<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center  font-weight-bold ">
    	Datos de la Transacción Seleccionada
    </div>
    <div>
    	<!-- class="contenedorInput" -->
	     <table class="table table-sm small" width="100%">
	    	
    		<tbody>
    		<tr>
    			<td class="align-middle">FECHA DE PROCESAMIENTO:</td> <!-- CENTRAL_PROCESSING_DATE  -->
    			<td class="align-middle"><input type="text" name="centralProcessingDate" id="centralProcessingDate" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    			<td class="align-middle">FECHA DEL EVENTO:</td><!-- EVENT_DATE -->
    			<td class="align-middle"><input type="text" name="eventDate" id="eventDate" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    			<td class="align-middle">EXT. N&Uacute;MERO DE CUENTA:</td><!-- ACCOUNT_NUMBER_EXTENSION -->
    			<td class="align-middle"><input type="text" name="accountNumberExtension" id="accountNumberExtension" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    		</tr>
    		
    		<tr>
    		    <td class="align-middle">BIN DESTINO:</td> <!-- DESTINATION_BIN -->
    			<td class="align-middle"><input type="text" name="destinationBin" id="destinationBin" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    			<td class="align-middle">BANDERA:</td><!-- SETTLEMENT_FLAG -->
    			<td class="align-middle"><input type="text" name="settlementFlag" id="settlementFlag" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    			<td class="align-middle">IDENTIFICADOR DE TRX:</td><!-- TRANSACTION_IDENTIFIER -->
    			<td class="align-middle"><input type="text" name="transactionIdentifier" id="transactionIdentifier" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    		</tr>
    		
    		<tr>
    			<td class="align-middle">RESERVADO:</td><!-- RESERVED -->
    			<td class="align-middle"><input type="text" name="reserved" id="reserved" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    			<td class="align-middle">ATTR. REEMBOLSO:</td><!-- REIMBURSEMENT_ATTRIBUTE -->
    			<td class="align-middle"><input type="text" name="reimbursementAttribute" id="reimbursementAttribute" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    		</tr>
    		 <tr>
    			<td class="align-middle">MENSAJE:</td><!-- MESSAGE_TEXT -->
    			<td class="align-middle" colspan="6"><input type="text" name="messageText" id="messageText" class="campoSalida form-control form-control-sm" size="107" readonly="readonly"></td>
    			<input type="hidden" id="TransaccionConGestion" name="TransaccionConGestion">
    		</tr>
    		
    	</tbody></table>
    </div>
    
    <!-- <br style="line-height: 5px" />  -->
    <!-- class="subTitulo" -->
    <div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center  font-weight-bold" id="tituloReversa" style="display:none;">
    	Datos de la Transacción Reversada
    </div>
    <div id="detallesReversa" style="display:none;">
    	<!-- contenedorInput -->
	     <table class="table table-sm small" width="100%">
	    	
    		<tbody><tr>
    			<td class="align-middle">FECHA DE INGRESO:</td> <!-- CENTRAL_PROCESSING_DATE  -->
    			<td class="align-middle"><input type="text" name="reverseDate" id="reverseDate" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    			<td class="align-middle">USUARIO:</td><!-- EVENT_DATE -->
    			<td class="align-middle"><input type="text" name="usuarioReverse" id="usuarioReverse" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    		</tr>
    		<tr>
    			<td class="align-middle">C&Oacute;DIGO MOTIVO REVERSA:</td><!-- ACCOUNT_NUMBER_EXTENSION -->
    			<td class="align-middle"><input type="text" name="codeReverse" id="codeReverse" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    			<td class="align-middle">TIPO TRX.:</td><!-- RESERVED -->
    			<td class="align-middle"><input type="text" name="mitReverse" id="mitReverse" class="campoSalida form-control form-control-sm" readonly="readonly"></td>
    		</tr>
    		 <tr>
    			<td class="align-middle">MENSAJE:</td><!-- MESSAGE_TEXT -->
    			<td class="align-middle" colspan="6"><input type="text" name="messageReverse" id="messageReverse" class="campoSalida form-control form-control-sm" size="107" readonly="readonly"></td>
    		</tr>
    		
    	</tbody></table>
    </div>
    
    <!-- <br style="line-height: 5px" />  -->
	
	<div>
		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm table-borderless">
			<tr>
			    <td class="align-middle" align="center"><input type="button" id="otraGestion" name="otraGestion"
					value="Reversar" class="btn btn-primary" />
				</td>
				<td class="align-middle" align="center"><input type="button"
					value="Exportar" id="exportar" name="exportar" class="btn btn-primary" />
				</td>
			</tr>
		</table>
	</div>
	
</body>
</html>