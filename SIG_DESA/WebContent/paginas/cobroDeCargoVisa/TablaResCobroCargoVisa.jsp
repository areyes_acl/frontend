<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cobro Cargo Visa</title>
<script>
	$(document).ready(function() {
	
		var parametros;
		
		
		
		if($("#numPagina").val()==1)		{	
			$("#anterior").bind("click",function(e){
			       e.preventDefault();
		     });
		}else{
			$("#anterior").unbind("click",false);					
		}
					
					
		if($("#ultimaPagina").val()<=1)
		{
			$("#botonesPaginacion").hide();
		}
				
		if($("#ultimaPagina").val()==0)
		{
			$("#anterior").hide();
			$("#siguiente").hide();
		}
					
					
		$("#siguiente").click(function(){
			
			if(parseInt($("#numPagina").val()) < parseInt($("#ultimaPagina").val()))
			{
				$("#popup").css({
						display : 'none'
					});
					$("#popUpContenido").css({
						display : 'none'
					});
					$('#codigosRazon option').prop('selected', function() {
				        return this.defaultSelected;
				    });
				    document.getElementById("glosaInfo").value = '';
			  		document.getElementById("glosa").value = '';
			  		var fechaDesde = $('#fechaDesde').val();
		   			var fechaHasta = $('#fechaHasta').val();
		   			var tipoTrx = $('#tipoTrx').val();
		   			var estado = $('#estado').val();
		   			var operador = $('#idOperador').val();
		   			var numPagina = parseInt($("#numPagina").val())+1;
		   			if(typeof numPagina == 'undefined'){
		   				numPagina = 1;
		   			}
		   			
		   			$('#otraGestion').prop("disabled", true);
					
			   		
					$("#idTrx").val("");					
					$("#mitTrx").val("");	
					$("#centralProcessingDate").val("");
					$("#eventDate").val("");
					$("#accountNumberExtension").val("");
					$("#destinationBin").val("");
					$("#settlementFlag").val("");
					$("#transactionIdentifier").val("");
					$("#reserved").val("");
					$("#reimbursementAttribute").val("");
					$("#messageText").val("");
					$("#amount").val("");
					$("#TransaccionConGestion").val("");
					
				   	buscarTrxCobroCargoVisa(fechaDesde, fechaHasta, tipoTrx,numPagina, estado, operador);
			}
		});
					
		$("#anterior").click(function(){
			
			if(parseInt($("#numPagina").val()) > 1)
			{
					$("#popup").css({
						display : 'none'
					});
					$("#popUpContenido").css({
						display : 'none'
					});
					$('#codigosRazon option').prop('selected', function() {
				        return this.defaultSelected;
				    });
				    document.getElementById("glosaInfo").value = '';
			  		document.getElementById("glosa").value = '';
			  		var fechaDesde = $('#fechaDesde').val();
		   			var fechaHasta = $('#fechaHasta').val();
		   			var tipoTrx = $('#tipoTrx').val();
		   			var estado = $('#estado').val();
		   			var operador = $('#idOperador').val();
		   			var numPagina = parseInt($("#numPagina").val())-1;
		   			if(typeof numPagina == 'undefined'){
		   				numPagina = 1;
		   			}
		   			
		   			$('#otraGestion').prop("disabled", true);
					
			   		
					$("#idTrx").val("");					
					$("#mitTrx").val("");	
					$("#centralProcessingDate").val("");
					$("#eventDate").val("");
					$("#accountNumberExtension").val("");
					$("#destinationBin").val("");
					$("#settlementFlag").val("");
					$("#transactionIdentifier").val("");
					$("#reserved").val("");
					$("#reimbursementAttribute").val("");
					$("#messageText").val("");
					$("#amount").val("");
					$("#TransaccionConGestion").val("");
					
				   	buscarTrxCobroCargoVisa(fechaDesde, fechaHasta, tipoTrx,numPagina, estado, operador);
			}
		});
					
		$("#ir").click(function(){
			
			var paginaIr;
			var ultimaPagina;
			
					$("#popup").css({
						display : 'none'
					});
					$("#popUpContenido").css({
						display : 'none'
					});
					$('#codigosRazon option').prop('selected', function() {
				        return this.defaultSelected;
				    });
				    document.getElementById("glosaInfo").value = '';
			  		document.getElementById("glosa").value = '';
			  		var fechaDesde = $('#fechaDesde').val();
		   			var fechaHasta = $('#fechaHasta').val();
		   			var tipoTrx = $('#tipoTrx').val();
		   			var estado = $('#estado').val();
		   			var operador = $('#idOperador').val();
		   			var paginaIr = parseInt($("#paginaIr").val());
		   			ultimaPagina = parseInt($("#ultimaPagina").val());
		   			if(typeof paginaIr == 'undefined'){
		   				paginaIr = 1;
		   			}
		   			
		   			$('#otraGestion').prop("disabled", true);
					
			   		
					$("#idTrx").val("");					
					$("#mitTrx").val("");	
					$("#centralProcessingDate").val("");
					$("#eventDate").val("");
					$("#accountNumberExtension").val("");
					$("#destinationBin").val("");
					$("#settlementFlag").val("");
					$("#transactionIdentifier").val("");
					$("#reserved").val("");
					$("#reimbursementAttribute").val("");
					$("#messageText").val("");
					$("#amount").val("");
					$("#TransaccionConGestion").val("");
					
				   	
			
			if( paginaIr == '' || paginaIr == null || typeof paginaIr == "undefined"){
				alert("La página no existe");
				$("#paginaIr").val("");
				return;
			}
						
			if(Number(paginaIr) > Number(ultimaPagina) || Number(paginaIr) < 0 )
			{
				alert("La página no existe");
				$("#paginaIr").val("");
			}else{
				
				buscarTrxCobroCargoVisa(fechaDesde, fechaHasta, tipoTrx, paginaIr, estado, operador);
				
			}
				
		});
	})
	
</script>
</head>
<body>
	
	<input id="ultimaPagina" name="ultimaPagina" type="hidden" value="<s:property value="%{#request.ultimaPagina}" />"/>
	<input type="hidden" value="<s:property value="%{#request.numPagina}" />" name="numPagina" id="numPagina"/>	
	<input type="hidden" value="<s:property value="%{#request.fechaInicioPag}" />" name="fechaInicioPag" id="fechaInicioPag"/> 
	<input type="hidden" value="<s:property value="%{#request.fechaTerminoPag}" />" name="fechaTerminoPag" id="fechaTerminoPag"/>
	<input type="hidden" value="<s:property value="%{#request.numeroTarjeta}" />" name="numeroTarjetaPag" id="numeroTarjetaPag"/> 
	
	<div style="width:800px;overflow-x:auto;overflow-y:auto;max-height:600px;" class="mx-auto">
			 <div style="margin:0px;padding:0px;">
			 	<!-- class="tablaContenido" -->
				<table width="100%" class="table table-sm small" id="myTable" >
					<thead>
						<tr class="text-center">
							<th class="align-middle">#</th>
							<th class="align-middle" style="text-align:center;">TIPO TRX</th><!-- MIT -->
							<th class="align-middle" style="text-align:center;">N&Uacute;MERO DE CUENTA</th><!-- ACCOUNT_NUMBER -->
							<th class="align-middle" style="text-align:center;">BIN ORIGEN</th><!-- SOURCE_BIN -->
							<th class="align-middle" style="text-align:center;">COD. RAZ&Oacute;N</th><!-- REASON_CODE -->
							<th class="align-middle" style="text-align:center;">COD. PA&Iacute;S</th><!-- COUNTRY_CODE -->
							<th class="align-middle" style="text-align:center;">MONTO ORIGEN</th><!-- SOURCE_AMOUNT -->
							<th class="align-middle" style="text-align:center;">MONEDA ORIGEN</th><!-- SOURCE_CURRENCY_CODE -->
							<th class="align-middle" style="text-align:center;">MONTO DESTINO</th><!-- DESTINATION_AMOUNT -->
							<th class="align-middle" style="text-align:center;">MONEDA DESTINO</th><!-- DESTINATION_CURRENCY_CODE -->
							<th style="text-align:center;display: none" class="select-accion align-middle">ACCION</th><!-- DESTINATION_CURRENCY_CODE -->
						</tr>
					</thead>
					<tbody>	
						<s:iterator id="data" value="%{#request.listaCobroCargoVisa}" status="elementoIndex">
					   		<tr class="text-center">
								<td class="align-middle">
									<div class="seleccionTRX custom-control custom-radio">
										<input type='radio' id='el<s:property value="%{#elementoIndex.index}"/>' name='seleccionTRX' value='C' class='custom-control-input' >
										<label class="custom-control-label" for='el<s:property value="%{#elementoIndex.index}"/>'></label>
									</div>
								</td>
								<td class="align-middle" style="text-align: center;"><s:property value="mit"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="accountNumber"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="sourceBin"/></td>
								<td class="align-middle" style="text-align: center;" onmouseover='showDescription("<s:property value="descripcionCodRazon"/>")' onmouseout="hideDescription()"><s:property value="reasonCode"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="countryCode"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="sourceAmount"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="sourceCurrencyCode"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="destinationAmount"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="destinationCurrencyCode"/></td>
								
								
								
								<td class="align-middle" tbl="centralProcessingDate" style="display:none"><s:property value="centralProcessingDate"/></td>
								<td class="align-middle" tbl="eventDate" style="display:none"><s:property value="eventDate"/></td>
								<td class="align-middle" tbl="accountNumberExtension" style="display:none"><s:property value="accountNumberExtension"/></td>
								<td class="align-middle" tbl="destinationBin" style="display:none"><s:property value="destinationBin"/></td>
								<td class="align-middle" tbl="settlementFlag" style="display:none"><s:property value="settlementFlag"/></td>
								<td class="align-middle" tbl="transactionIdentifier" style="display:none"><s:property value="transactionIdentifier"/></td>
								<td class="align-middle" tbl="reserved" style="display:none"><s:property value="reserved"/></td>
								<td class="align-middle" tbl="reimbursementAttribute" style="display:none"><s:property value="reimbursementAttribute"/></td>
								<td class="align-middle" tbl="messageText" style="display:none"><s:property value="messageText"/></td>
								<td class="align-middle" tbl="fechaReversa" style="display:none;"><s:property value="fechaReversa" /></td>
								<td class="align-middle" tbl="usuarioReversa" style="display:none;"><s:property value="usuarioReversa" /></td>
								<td class="tablaContenido" tbl="codigoMotivoReversa" style="display:none;"><s:property value="codigoMotivoReversa" /></td>
								<td class="align-middle" tbl="mitReversa" style="display:none;"><s:property value="mitReversa" /></td>
								<td class="align-middle" tbl="mensajeReversa" style="display:none;"><s:property value="mensajeReversa" /></td>
								<td class="align-middle" id="hidden" tbl="sid" style="display:none"><s:property value="sid"/></td>
								<td class="align-middle" id="hidden" tbl="TransaccionConGestion" style="display:none;"><s:property value="TransaccionConGestion" /></td>
								
								<s:if test="%{TransaccionConGestion != null}">
								 	<td id="actions" style="text-align: center; display: none" class="select-accion align-middle"><a title="Cancelar Reversa" style="cursor:pointer;" onclick='deleteReversa("<s:property value="sid"/>")'><img style="width:15px" src="img/trash.png"></a></td>
								</s:if>
								<s:else>
							        <td id="actions" style="text-align: center;display: none" class="select-accion align-middle"></td>
							    </s:else>
					   		</tr>	
					   	</s:iterator>
					</tbody>
			   	</table>
			</div>
		</div>
		<div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial,Helvetica,sans-serif; font-size: 8pt;">
		   	<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		   	<s:property value="%{#request.numPagina}" /> de <s:property value="%{#request.ultimaPagina}" /> p&aacute;gina(s)&nbsp;&nbsp;&nbsp;
		   	Ir a pagina <input type="text" id="paginaIr" style="width:50px;font-size: 8pt;" class="form-control form-control-sm d-inline py-0 align-middle" /> <input type="button" value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt" />
		</div>
</body>
</html>