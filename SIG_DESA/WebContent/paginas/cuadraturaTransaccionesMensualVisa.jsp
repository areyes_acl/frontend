<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<title>Cuadratura Mensual De Transacciones Visa</title>
<script src="js/jquery-1.10.0.min.js" type="text/javascript">
	
</script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>

<script>

	$(".datepicker").datepicker({
	   	format: "mm/yyyy",
	    minViewMode: 1	
	});
	
  	$(document).ready(function(){
	  		cargaOperador();
  	});
//funcion para cargar lista de operadores en combobox
	function cargaOperador(){
	$.ajax({
   			url:'cargarComboOperadores',
   			type:'POST'
   		}).done(function(data){
   			data.listaOperadores;
   			var datos = (data.listaOperadores).split("~");
   			
   			var strCombo = "";
   			
   			for(var i=1;i<datos.length-1;i++)
				{			
   				var fila = datos[i].split("|");
   				
   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
				}
   			
   			$("#idOperador").append(strCombo);
   		});
}
	  </script>

<script>
	var DataExportar = '';
	$('#fechaMensual').bind('keypress', function(event) {
		var fec = $("#fechaMensual").val();
		if (fec.length == 2)
			fec += '/';
		$("#fechaMensual").val(fec);
	});
	$("#buscar")
			.click(
					function() {

						if ($('#fechaMensual').val().length == 0) {
							alert("Debe ingresar fecha de proceso para realizar la busqueda...");
							$("#fechaProceso").focus();
							return;
						}
						
						var operador = $("#idOperador").val();

						var fechaProcesoTmp = $('#fechaMensual').val().split(
								"/"); //"20130619"//
						var mes = fechaProcesoTmp[0];
						var ano = fechaProcesoTmp[1];

						if (mes<1 || mes>12) {
							alert("La fecha de proceso ingresada es incorrecta, verifiquela...");
							$("#fechaMensual").focus();
							return;
						}



						var parametros = "fechaMensual="			

								+ $('#fechaMensual').val() + "&operador=" + operador;

						$
								.ajax({
									url : "cuadraturaTransaccionesMensualVisaAction",
									type : "POST",
									data : parametros,
									dataType : "json",
									error : function(XMLHttpRequest,
											textStatus, errorThrown) {
										alert('Error ' + textStatus);
										alert(errorThrown);
										alert(XMLHttpRequest.responseText);
									},
									success : function(data) {

										mostrarCuadraturaMensual(data.listaCuadraturaMensual);

									}
								});

					});

	$("#fechaMensual").focus(function() {
		$("#divCuadraturaTransaccion").css("display", "none");
		$("#exportar").attr("disabled", true);
	});

	$("#exportar")
			.click(
					function() {

						DataExportar = "";
						DatasExcel = "";
						OperadorExcel = document.getElementById('idOperador').options[document.getElementById('idOperador').selectedIndex].text.toUpperCase();
						

						idTablaExportar = "myTableIncoming";
						
						DataExportar += "TIT||"+OperadorExcel+"| | ||~"
						DataExportar += "TIT|TIPO TRANSACCION|NRO TRANSACCIONES|TIPO MONEDA|MONTO~";
						DataExportar += exportarExcelLogProcesos(idTablaExportar,
								DatasExcel);

						idTablaExportar = "myTableOutgoing";
						DatasExcel = "";
						DataExportar += "TIT|TIPO TRANSACCION|NRO TRANSACCIONES|TIPO MONEDA|MONTO~";
						DataExportar += exportarExcelLogProcesos(idTablaExportar,
								DatasExcel);

						exportarGenerico(DataExportar, "export");

					});
</script>

<script>
	function mostrarCuadraturaMensual(listaCuadraturaMensual) {

		var stringHtml = "";
		var lstcuadraturaMensual = listaCuadraturaMensual.split("~");
		var esttrx = lstcuadraturaMensual[0].split("~");
		var codtrx = esttrx[0].split("|");

		if (codtrx[1] == "0") {
			var idTablaocultar = document.getElementById('tablaOcultar');

			idTablaocultar.style.display = 'none';
			stringHtml = "";
			// class="tablaContenido"
			stringHtml = "<table width='100%' class='table table-sm small' id='myTableIncoming'>";
			stringHtml = stringHtml + "<tr class='text-center'>";
			stringHtml = stringHtml
					+ "<th class='align-middle' width='25%'>TIPO DE TRANSACCION</th>";
			stringHtml = stringHtml + "<th class='align-middle' width='25%'>NRO TRANSACCIONES</th>";
			stringHtml = stringHtml + "<th class='align-middle' width='25%'>TIPO MONEDA</th>";
			stringHtml = stringHtml + "<th class='align-middle' width='25%'>MONTO</th>";
			stringHtml = stringHtml + "</tr>";

			var detallecuadraturamensual = lstcuadraturaMensual[1].split("|");

			var l_compra_clp = detallecuadraturamensual[0];
			var l_qtrx_clp_1 = detallecuadraturamensual[1];
			var l_monto_1 = detallecuadraturamensual[2];

			var l_creditos_clp = detallecuadraturamensual[3];
			var l_qtrx_clp_2 = detallecuadraturamensual[4];
			var l_monto_2 = detallecuadraturamensual[5];

			var l_avances_clp = detallecuadraturamensual[6];
			var l_qtrx_clp_3 = detallecuadraturamensual[7];
			var l_monto_3 = detallecuadraturamensual[8];

			var l_representaciones_clp = detallecuadraturamensual[9];
			var l_qtrx_clp_4 = detallecuadraturamensual[10];
			var l_monto_4 = detallecuadraturamensual[11];

			var l_confirmvales_clp = detallecuadraturamensual[12];
			var l_qtrx_clp_5 = detallecuadraturamensual[13];

			var l_compra_us = detallecuadraturamensual[14];
			var l_qtrx_us_1 = detallecuadraturamensual[15];
			var l_monto_us_1 = detallecuadraturamensual[16];

			var l_creditos_us = detallecuadraturamensual[17];
			var l_qtrx_us_2 = detallecuadraturamensual[18];
			var l_monto_us_2 = detallecuadraturamensual[19];

			var l_avances_us = detallecuadraturamensual[20];
			var l_qtrx_us_3 = detallecuadraturamensual[21];
			var l_monto_us_3 = detallecuadraturamensual[22];

			var l_representaciones_us = detallecuadraturamensual[23];
			var l_qtrx_us_4 = detallecuadraturamensual[24];
			var l_monto_us_4 = detallecuadraturamensual[25];

			var l_miscelaneos_us = detallecuadraturamensual[26];
			var l_qtrx_us_5 = detallecuadraturamensual[27];
			var l_monto_us_5 = detallecuadraturamensual[28];

			var l_confirmvales_us = detallecuadraturamensual[29];
			var l_qtrx_us_6 = detallecuadraturamensual[30];

			var l_1contra_clp = detallecuadraturamensual[31];
			var l_qtrx_1con = detallecuadraturamensual[32];
			var l_outgoing_monto_1 = detallecuadraturamensual[33];

			var l_2contra_clp = detallecuadraturamensual[34];
			var l_qtrx_2con = detallecuadraturamensual[35];
			var l_outgoing_monto_2 = detallecuadraturamensual[36];

			var l_petval_clp = detallecuadraturamensual[37];
			var l_qtrx_petval = detallecuadraturamensual[38];

			var l_1contra_us = detallecuadraturamensual[39];
			var l_qtrx_1con_us = detallecuadraturamensual[40];
			var l_outgoing_monto_1_us = detallecuadraturamensual[41];

			var l_2contra_us = detallecuadraturamensual[42];
			var l_qtrx_2con_us = detallecuadraturamensual[43];
			var l_outgoing_monto_2_us = detallecuadraturamensual[44];

			var l_misc_us = detallecuadraturamensual[45];
			var l_qtrx_mis_us = detallecuadraturamensual[46];
			var l_outgoing_mis_us = detallecuadraturamensual[47];

			var l_petval_us = detallecuadraturamensual[48];
			var l_qtrx_petval_us = detallecuadraturamensual[49];

			/************************************/
			/* Variables de subtotales          */
			/************************************/
			var subtotal_inc_monto_clp = 0;
			var subtotal_inc_cantidad_clp = 0;

			var subtotal_inc_monto_usd = 0;
			var subtotal_inc_cantidad_usd = 0;

			var subtotal_out_monto_clp = 0;
			var subtotal_out_cantidad_clp = 0;

			var subtotal_out_monto_usd = 0;
			var subtotal_out_cantidad_usd = 0;

			/************************************/
			/* Transacciones de incoming        */
			/************************************/
			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_compra_clp
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_clp_1 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_monto_1)) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES CLP                  *//
			//**********************************//
			subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp)
					+ parseInt(l_qtrx_clp_1);
			subtotal_inc_monto_clp = parseInt(subtotal_inc_monto_clp)
					+ parseInt(l_monto_1);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_creditos_clp
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_clp_2 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_monto_2)) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES CLP                  *//
			//**********************************//
			subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp)
					+ parseInt(l_qtrx_clp_2);
			subtotal_inc_monto_clp = parseInt(subtotal_inc_monto_clp)
					+ parseInt(l_monto_2);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_avances_clp
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_clp_3 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_monto_3)) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES CLP                  *//
			//**********************************//
			subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp)
					+ parseInt(l_qtrx_clp_3);
			subtotal_inc_monto_clp = parseInt(subtotal_inc_monto_clp)
					+ parseInt(l_monto_3);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>"
					+ l_representaciones_clp + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_clp_4 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_monto_4)) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES CLP                  *//
			//**********************************//
			subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp)
					+ parseInt(l_qtrx_clp_4);
			subtotal_inc_monto_clp = parseInt(subtotal_inc_monto_clp)
					+ parseInt(l_monto_4);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_confirmvales_clp
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_clp_5 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero("0")) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES CLP                  *//
			//**********************************//
			subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp)
					+ parseInt(l_qtrx_clp_5);
			subtotal_inc_monto_clp = parseInt(subtotal_inc_monto_clp)
					+ parseInt(0);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ "SUBTOTAL " + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ subtotal_inc_cantidad_clp + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "   "
					+ "</td>";
			stringHtml = stringHtml
					+ "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(subtotal_inc_monto_clp
							.toString())) + "</td>";
			stringHtml = stringHtml + "</tr>";

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_compra_us
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_us_1 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_monto_us_1)) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES USD                  *//
			//**********************************//
			subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd)
					+ parseInt(l_qtrx_us_1);
			subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd)
					+ parseInt(l_monto_us_1);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_creditos_us
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_us_2 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_monto_us_2)) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES USD                  *//
			//**********************************//
			subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd)
					+ parseInt(l_qtrx_us_2);
			subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd)
					+ parseInt(l_monto_us_2);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_avances_us
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_us_3 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_monto_us_3)) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES USD                  *//
			//**********************************//
			subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd)
					+ parseInt(l_qtrx_us_3);
			subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd)
					+ parseInt(l_monto_us_3);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>"
					+ l_representaciones_us + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_us_4 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_monto_us_4)) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES USD                  *//
			//**********************************//
			subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd)
					+ parseInt(l_qtrx_us_4);
			subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd)
					+ parseInt(l_monto_us_4);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_miscelaneos_us
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_us_5 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_monto_us_5)) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES USD                  *//
			//**********************************//
			subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd)
					+ parseInt(l_qtrx_us_5);
			subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd)
					+ parseInt(l_monto_us_5);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_confirmvales_us
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_us_6 + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero("0") + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES USD                  *//
			//**********************************//
			subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd)
					+ parseInt(l_qtrx_us_6);
			subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd)
					+ parseInt(0);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ "SUBTOTAL " + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ subtotal_inc_cantidad_usd + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "   "
					+ "</td>";
			stringHtml = stringHtml
					+ "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(subtotal_inc_monto_usd
							.toString())) + "</td>";
			stringHtml = stringHtml + "</tr>";

			stringHtml = stringHtml + "</table>";

			stringHtml = stringHtml + "<br style='line-height:5px'/>";

			/************************************/
			/* Transacciones de outgoing        */
			/************************************/
			//stringHtml = stringHtml + "<table width='100%' class='tablaContenido'>";
			//stringHtml = stringHtml + "</table>";
			stringHtml = stringHtml
					// class='tablaContenido'
					+ "<table width='100%' class='table table-sm small' id='myTableOutgoing'>";
			stringHtml = stringHtml + "<tr class='text-center'>";
			stringHtml = stringHtml
					+ "<th class='align-middle' width='25%'>TIPO DE TRANSACCION</th>";
			stringHtml = stringHtml + "<th class='align-middle' width='25%'>NRO TRANSACCIONES</th>";
			stringHtml = stringHtml + "<th class='align-middle' width='25%'>TIPO MONEDA</th>";
			stringHtml = stringHtml + "<th class='align-middle' width='25%'>MONTO</th>";
			stringHtml = stringHtml + "</tr>";
			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_1contra_clp
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_1con + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_outgoing_monto_1))
					+ "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES CLP                  *//
			//**********************************//
			subtotal_out_cantidad_clp = parseInt(subtotal_out_cantidad_clp)
					+ parseInt(l_qtrx_1con);
			subtotal_out_monto_clp = parseInt(subtotal_out_monto_clp)
					+ parseInt(l_outgoing_monto_1);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_2contra_clp
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_2con + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_outgoing_monto_2))
					+ "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES CLP                  *//
			//**********************************//
			subtotal_out_cantidad_clp = parseInt(subtotal_out_cantidad_clp)
					+ parseInt(l_qtrx_2con);
			subtotal_out_monto_clp = parseInt(subtotal_out_monto_clp)
					+ parseInt(l_outgoing_monto_2);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_petval_clp
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_petval + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero("0")) + "</td>";
			stringHtml = stringHtml + "</tr>";

			//**********************************//
			// SUBTOTALES CLP                  *//
			//**********************************//
			subtotal_out_cantidad_clp = parseInt(subtotal_out_cantidad_clp)
					+ parseInt(l_qtrx_petval);
			subtotal_out_monto_clp = parseInt(subtotal_out_monto_clp)
					+ parseInt(0);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ "SUBTOTAL " + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ subtotal_out_cantidad_clp + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "   "
					+ "</td>";
			stringHtml = stringHtml
					+ "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(subtotal_out_monto_clp
							.toString())) + "</td>";
			stringHtml = stringHtml + "</tr>";

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_1contra_us
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_1con_us + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_outgoing_monto_1_us))
					+ "</td>";
			stringHtml = stringHtml + "</tr>";

			subtotal_out_cantidad_usd = parseInt(subtotal_out_cantidad_usd)
					+ parseInt(l_qtrx_1con_us);
			subtotal_out_monto_usd = parseInt(subtotal_out_monto_usd)
					+ parseInt(l_outgoing_monto_1_us);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_2contra_us
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_2con_us + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_outgoing_monto_2_us))
					+ "</td>";
			stringHtml = stringHtml + "</tr>";

			subtotal_out_cantidad_usd = parseInt(subtotal_out_cantidad_usd)
					+ parseInt(l_qtrx_2con_us);
			subtotal_out_monto_usd = parseInt(subtotal_out_monto_usd)
					+ parseInt(l_outgoing_monto_2_us);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_misc_us + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_mis_us + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(l_outgoing_mis_us))
					+ "</td>";
			stringHtml = stringHtml + "</tr>";

			subtotal_out_cantidad_usd = parseInt(subtotal_out_cantidad_usd)
					+ parseInt(l_qtrx_mis_us);
			subtotal_out_monto_usd = parseInt(subtotal_out_monto_usd)
					+ parseInt(l_outgoing_mis_us);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_petval_us
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ l_qtrx_petval_us + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD"
					+ "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero("0")) + "</td>";
			stringHtml = stringHtml + "</tr>";

			subtotal_out_cantidad_usd = parseInt(subtotal_out_cantidad_usd)
					+ parseInt(l_qtrx_petval_us);
			subtotal_out_monto_usd = parseInt(subtotal_out_monto_usd)
					+ parseInt(0);

			stringHtml = stringHtml + "<tr>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ "SUBTOTAL " + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>"
					+ subtotal_out_cantidad_usd + "</td>";
			stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "   "
					+ "</td>";
			stringHtml = stringHtml
					+ "<td class='align-middle' width='25%' align='right'>"
					+ formato_numero(colocaComaNumero(subtotal_out_monto_usd
							.toString())) + "</td>";
			stringHtml = stringHtml + "</tr>";

			stringHtml = stringHtml + "</table>";

			$('#divCuadraturaTransaccion').html(stringHtml);
			$("#divCuadraturaTransaccion").css("display", "");

			$("#exportar").attr("disabled", false);
		} else {
			var mensaje = lstcuadraturaMensual[1].split("|");
			alert(mensaje[1]);
		}

	}
</script>

</head>

<body>

	<!-- <div class="titulo"> -->
 	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
	  	CUADRATURA DE TRANSACIONES MENSUAL
  	</div>
<hr/>
	<!-- <br style="line-height: 5px" />  -->

	<div class="mb-4">
		<!-- <table width="100%" class="contenedorInput">  -->
	   	<table width="100%" class="table table-sm my-0 mx-auto">
			<tr class='text-center'>
				<td class="align-middle text-right">Fecha Proceso :</td>
				
 				<td class="align-middle"><input type="text" name="fechaMensual" id="fechaMensual"
 					size="10" maxlength="10" onKeypress="ingresoFecha();"
					class="campoObligatorio form-control form-control-sm mx-auto text-center col-12 datepicker" placeholder="MM/AAAA" />
				</td>
					
				<td class="align-middle text-right">Operador:</td>
				
				<td class="align-middle"><select id="idOperador" class="w-75 custom-select"></select></td>
				
				<td class="align-middle">
					 <input type="button"
					id="buscar" value="Buscar" class="btn btn-primary">
					</td>
			</tr>
		</table>
	</div>

	<!-- <br style="line-height: 5px" />  -->

	<div>
		<!-- class="tablaContenido" -->
		<table width="100%" class="table table-sm small" id="tablaOcultar">
			<tr class="text-center">
				<th class="align-middle" width='25%'>TIPO DE TRANSACCION</th>
				<th class="align-middle" width='25%'>NRO TRANSACCIONES</th>
				<th class="align-middle" width='25%'>TIPO MONEDA</th>
				<th class="align-middle" width='25%'>MONTO</th>
			</tr>
		</table>
		<div id="divCuadraturaTransaccion"></div>

	</div>

	<!-- <br style="line-height: 5px" />  -->


	<div>
		<!-- <table width="100%" class="contenedorInput">  -->
		<table  width="100%" class="table-borderless">
			<tr>
				<td align="center"><input type="button"
					value="Exportar Tabla" id="exportar" name="exportar" class="btn btn-primary" disabled />
				</td>
			</tr>
		</table>
	</div>

</body>
</html>