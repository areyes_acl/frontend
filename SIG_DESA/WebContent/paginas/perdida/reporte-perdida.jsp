<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Reporte de pérdidas</title>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script>

	$(".datepicker").datepicker();
	// INICIO DE PAGINA       
	$(document).ready(function() {
		$('#fecha-inicio').val(getYesterday());
		$('#fecha-termino').val(getToday());
	});

	// MODIFICACION DE FECHAS
	$('#fecha-inicio').change(function() {

	});

	$('#fecha-termino').change(function() {

	});

	// EXPORTAR ARCHIVO CSV
	$("#exportar")
			.click(
					function() {
						//implementar la funcion de exportar desde base de datos

						var parametros = "&fechaInicio=:ini:&fechaTermino=:fin:&numeroTarjeta=:tc:";
						parametros = parametros.replace(":ini:", $(
								"#fecha-inicio").val());
						parametros = parametros.replace(":fin:", $(
								"#fecha-termino").val());
						parametros = parametros.replace(":tc:", $(
								"#numero-tarjeta").val());

						location.href = "./exportarArchivoPerdidas?"
								+ parametros;

					});

	// BUSCAR PERDIDAS
	$("#buscar-perdidas").click(function() {

		var parametros = {
			fechaInicio : $("#fecha-inicio").val(),
			fechaTermino : $("#fecha-termino").val(),
			numeroTarjeta : $("#numero-tarjeta").val()
		};

		$.ajax({

			url : "consultarPerdidas",
			type : 'POST',
			data : parametros,

		}).done(function(data) {
			$("#popup").css({
				display : 'block'
			});
			$("#tituloTablaRes").css("display", "");
			$("#tablaRes").empty().append(data);

		});
	});
</script>


</head>

<body>
	<form id="form1">
		<!-- <div class="titulo">  -->
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
			CONSULTA DE P&Eacute;RDIDAS
		</div>
		<hr>
		<!-- <br style="line-height: 5px" />  -->

		<div>
			<!-- class="contenedorInput" -->
			<table width="100%" class="table table-sm">
				<tr class="text-center">
					<td class="align-middle text-right">Nro. TC:</td>
					<td class="align-middle"><input type="text" name="numerotarjeta"
						id="numero-tarjeta" maxlength="16" class="numericOnly form-control form-control-sm col-10 mx-auto text-center align-middle" /></td>
					<td class="align-middle">DESDE:</td>
					<td class="align-middle">
						<!-- size="10" -->
						<input type="text" name="fechaInicio" id="fecha-inicio"
							maxlength="10" class="campoObligatorio fecha form-control form-control-sm col-10 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td class="align-middle text-right">HASTA:</td>
					<td class="align-middle">
						<!-- size="10" -->
						<input type="text" name="fechaTermino" id="fecha-termino"
							maxlength="10" class="campoObligatorio fecha form-control form-control-sm col-10 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td class="align-middle">
						<input type="button" id="buscar-perdidas" name="buscarPerdidas"
						value="Buscar" class="btn btn-primary"></td>
					</td>	
				</tr>
			</table>
		</div>


		<!-- <br style="line-height: 5px" />  -->

		<!-- class="subTitulo" -->
		<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center font-weight-bold" 
				id="tituloTablaRes" style="display: none;">
			Lista de Transacciones Reportadas Como Pérdidas
		</div>

		<div class="cajaContenido">
			<div>
				<!-- class="tablaContenido" -->
				<table style="width: 100%;" class="table table-sm" id="tabla1">
				</table>
			</div>

			<div id="tablaRes"></div>
		</div>

		<!--  <br style="line-height: 5px" /> -->

		<!-- MUESTRA LA DESCRIPCION -->
		<div class="mt-3 col-6 mx-auto">
			<!-- class="contenedorInput" style="width: 500px; margin-left: 150px;" (Añadir) -->
			<table id="tablaDescripcion" style="display: none;" class="table table-sm">
				<thead>
					<tr class="text-center">
						<th class="align-middle">
							<!-- <div style="font-weight: bold; width: 500px; font-size: 14px; background: #a5bad1;"> -->
							<div class="font-weight-bold">	
									Descripci&oacute;n
							</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center">
						<!--  style="aling: center;width: 500px" -->
						<td id="descripcion" >
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<!-- <br style="line-height: 10px" />  -->

		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm table-borderless">
			<tr>
				<td align="center" width="33%"><input type="button"
					value="Exportar a Excel" name="exportar" id="exportar" class="btn btn-primary" /></td>
			</tr>
		</table>


	</form>
</body>
</html>