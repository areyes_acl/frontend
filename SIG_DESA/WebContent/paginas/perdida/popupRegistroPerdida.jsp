<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>
<html>
   <head>
     <title>Registro Perdida</title>
     <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
     <script type="text/javascript" src="js/utils.js"></script>
     <script type="text/javascript" src="js/maxlength.js"></script>
        
        
   <script>  
	$(document).ready(function(){
		
			// CARGA NUMERO DE INCIDENTE
			$("#numIncidente").val("<%=request.getParameter("numIncidente")%>");
       		
       		// CARGA COMBO TIPO
       		cargaComboTipoGestion();
       	});
       	
       	
       	// BOTON REGISTRAR PERDIDA
       	$("#btn-guardar-perdida").click(function(){
       		
       		if( camposObligatoriosValidos()){
	       			
	       		$.ajax({
		       			 url:'guardarPerdida',
		       			 type:'POST',
		       			 data:$("#form-registro-perdida").serialize()
		       		   
		       		   }).done(function(data){
		       				if( data.codigoError == 0){
		       					alert("Se ha generado una perdida correctamente en el sistema.");
				       			buscarTransaccionPresentacion();
				       			$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});
		       				}else{
		       					alert(data.mensaje);
				       			buscarTransaccionPresentacion();
				       			$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});
		       				}
			       			
			       	   });
       		}
    	});
       	
       	
       	// BONTON CANCELAR
       	$("#btn-cancelar-perdida").click(function(){
        	$('#popup').css({display: 'none'});
        	$('#popUpContenido').empty().css({display:'none'});
        	$("#buscarTransaccion").click();
        	
        });
        
        
        /**
         * Metodo que carga el combo box de tipo de gestión
         **/
        function cargaComboTipoGestion(){
       			
       			 $.ajax({
       						url  : 'cargarListaTipoPerdida',
       						type : 'POST'
       		  			
       		  			}).done(function(data){
			       			
			       			var datos = (data.listaComboPerdidas).split("~");
			       			var strCombo = "";
			       			
			       			for(var i=0;i<datos.length-1;i++)
			   				{			
			       				var fila = datos[i].split("|");
			       				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
			   				}
			       			
			       			$("#tpGestiones").append(strCombo);
       					});
        }
        
        
        /**
         *	Metodo que valida los campos obligatorios 
         **/
        function camposObligatoriosValidos(){
        
        	// VALIDACION DE LA GLOSA
        	if($("#glosa").val().trim() == ''){
        		alert("Advertencia : Campo glosa es obligatorio.");
        		return false;
        	}
        	
        	// VALIDACION DEL NUMERO DE INCIDENTE
        	if($("#numIncidente").val().trim() == ''){
        		alert("Advertencia : Campo numero de incidente es obligatorio.");
        		return false;
        	}
        	
        	
        	
        	return true;
        
        }
       
       </script>
   </head>
	
	<body>
		<form id="form-registro-perdida">
			<input type="hidden" name="sidTransaccion" value="<%=request.getParameter("sid")%>" />
			<input type="hidden" name="sidUsuario" value="<%=request.getSession().getAttribute("sidusuario")%>" />
			
			<!-- <div class="titulo">  --> 
			<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
				Registrar P&eacute;rdida 
			</div>
			<hr>
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table style="width:100%;" class="table table-sm">
			    	<tr>
			    		<!-- style="width:40%;" -->
			        	<td class="align-middle text-right w-50">Tipo Gesti&oacute;n:</td>
		        		<td class="align-middle" id="comboObjeciones">
		        			<select name="sidTipoGestion" id="tpGestiones" class="custom-select">
		        			</select>
		        		</td>
		 			</tr>
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/> -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table style="width:100%" class="table table-sm"> 			
					<tr>
			        	<td class="CuerpoNormal align-middle">(*) Motivo: </td>
			 		</tr>
			 		<tr>
			 			<td class="align-middle">
			 				<!--  style="width: 490px;" -->
			 				<textarea id="glosa"  rows="5" cols="58" name="glosa" data-maxsize="1999" class="form-control form-control-sm"></textarea>
			 			</td>
			 		</tr>
			 			
				</table>
			</div>
			<!-- <br style="line-height:5px"/>  -->
			<div>
				<!-- contenedorInput -->
				<table style="with:100%;" class="table table-ms">
			    	<tr>
			    		<!-- width="15%"  -->
			        	<td class="align-middle text-right w-50">Nº Incidente:</td>
		        		<td class="align-middle">
		        			<!-- size="50" -->
		        			<input id="numIncidente" name="numIncidente" class="alphanumeric form-control form-control-sm" maxlength="50" />
		        		</td>
		 			</tr>
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
				<!-- contenedorInput -->
				<table  style="width: 100%;" class="table table-sm table-borderless">
			       		<tr>
			       		<!-- align="center" -->
		 				<td class="align-middle text-center">
		 				    <input id="btn-guardar-perdida" type="button"  value="Guardar" class="btn btn-primary"/>
							<input id="btn-cancelar-perdida" type="button"  value="Cancelar" class="btn btn-primary" />
		 				</td>
		 			</tr>
				</table>
			</div>
	      </form>
	</body>
</html>

