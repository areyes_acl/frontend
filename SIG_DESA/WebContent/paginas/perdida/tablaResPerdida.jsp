<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		
		<script type="text/javascript">
			$(document).ready(function(){
								
				if($("#numPagina").val()==1)
				{	
					$("#anterior").bind("click",function(e){
				        e.preventDefault();
			        });
				}else{
					$("#anterior").unbind("click",false);					
				}
				
				if($("#ultimaPagina").val()<=1)
				{
					$("#botonesPaginacion").hide();
				}
				
				if($("#ultimaPagina").val()==0)
				{
					$("#anterior").hide();
					$("#siguiente").hide();
				}
				
				$("#siguiente").click(function(){
				
					if($("#numPagina").val() < $("#ultimaPagina").val())
					{
						var numPag = parseInt($("#numPagina").val())+1;
						var datos = 
							"fechaInicio="+$("#fechaInicioPag").val()+
							"&fechaTermino="+$("#fechaTerminoPag").val()+
							"&numPagina="+numPag;
						$.ajax({
							url:"consultarPerdidas",
							type:'POST',
							data:datos
						}).done(function(data){
							$("#tablaRes").empty().append(data);
						});
					}
				});
				
				$("#ir").click(function(){
					
					var paginaIr;
					var ultimaPagina;
					
					paginaIr = $("#paginaIr").val();
					
					ultimaPagina = $("#ultimaPagina").val();
					
					
					if( paginaIr == '' || paginaIr == null || typeof paginaIr == "undefined"){
						alert("La página no existe");
						$("#paginaIr").val("");
						return;
					}
					
					if(Number(paginaIr) > Number(ultimaPagina) || Number(paginaIr) < 0 )
					{
						alert("La página no existe");
						$("#paginaIr").val("");
					}else{
						var numPag = paginaIr;
						var datos = 
							"fechaInicio="+$("#fechaInicioPag").val()+
							"&fechaTermino="+$("#fechaTerminoPag").val()+
							"&numPagina="+numPag;
							
						$.ajax({
							url:'consultarPerdidas',
							type:'POST',
							data:datos
						}).done(function(data){
							$("#tablaRes").empty().append(data);
						});
					}
						
				});
				
				$("#anterior").click(function(){
					if($("#numPagina").val() > 1)
					{
						var numPag = parseInt($("#numPagina").val())-1;
						var datos = 
							"fechaInicio="+$("#fechaInicioPag").val()+
							"&fechaTermino="+$("#fechaTerminoPag").val()+
							"&numPagina="+numPag;
						$.ajax({
							url:"consultarPerdidas",
							type:'POST',
							data:datos
						}).done(function(data){
							$("#tablaRes").empty().append(data);
						});
					}
				});
			});
			
			
		// muestra la descripción	
		/* function showDescription(description){			
		
    					// $('#descripcion').append('<div id="page"> \ <p class="texto">'+description+' \ </div>' );
    					$('#descripcion').append('<div id="page"><p class="texto">'+description+'</p></div>' );
    					// css("display", "block")
    					$("#tablaDescripcion").css("display","");
				
			} */
		
		function showDescription(description) {
			if(description === null || description === "") {
				$('#descripcion').append(
					'<div id="page"> \ <p class="texto">Sin descripcion \ </div>');
				$("#tablaDescripcion").css("display", "");
			}
			else {
				$('#descripcion').append(
					'<div id="page"> \ <p class="texto">' + description
							+ ' \ </div>');
				$("#tablaDescripcion").css("display", "");
			}
		}
		
		// esconde la descripcion
		function hideDescription(){
			$('#descripcion').html("");
			$("#tablaDescripcion").css("display","none");
		}
		</script>
	</head>
	<body>
		<div></div>
		
		<input id="ultimaPagina" name="ultimaPagina" type="hidden" value="<s:property value="%{#request.ultimaPagina}" />"/>
		<input type="hidden" value="<s:property value="%{#request.numPagina}" />" name="numPagina" id="numPagina"/>
		<input type="hidden" value="<s:property value="%{#request.fechaInicioPag}" />" name="fechaInicioPag" id="fechaInicioPag"/> 
		<input type="hidden" value="<s:property value="%{#request.fechaTerminoPag}" />" name="fechaTerminoPag" id="fechaTerminoPag"/>
		<input type="hidden" value="<s:property value="%{#request.numeroTarjeta}" />" name="numeroTarjetaPag" id="numeroTarjetaPag"/>
		
		 
	 	<div style="width:817px;overflow-x:auto;overflow-y:auto;max-height:300px;">
		<div style="width:800px;margin:0px;padding:0px;">
		<!-- class="tablaContenido" -->
		<table style="width:100%;" class="table table-sm small" id="tabla-resultados" >
		
					<tr class="text-center">
						<th class="align-middle" style="width:110px;font-weigth:bold">FECHA REGISTRO</th>
						<th class="align-middle" style="width:100px;font-weigth:bold">MONTO</th>
						<th class="align-middle" style="width:100px;font-weigth:bold">NRO. INCIDENTE</th>
						<th class="align-middle" style="width:80px;font-weigth:bold">USUARIO</th>
						<th class="align-middle" style="width:80px;font-weigth:bold">TARJETA</th>
						<th class="align-middle" style="width:80px;font-weigth:bold">FECHA TRANSACCI&Oacute;N</th>
						<th class="align-middle" style="width:140px;font-weigth:bold">COMERCIO</th>
						<th class="align-middle" style="width:20px;font-weigth:bold">COD TRANSACCI&Oacute;N</th>
						<th class="align-middle" style="width:80px;font-weigth:bold">TIPO TRANSACCI&Oacute;N</th>
						<th class="align-middle" style="width:70px;font-weigth:bold">MOTIVO PERDIDA</th>
						<th class="align-middle" style="width:30px;font-weigth:bold">ESTADO<BR/>CONTABLE</th>
						<th class="align-middle" style="width:30px;font-weigth:bold">FECHA<BR/>CONTABLE</th>
					</tr>
				
			<s:iterator value="listaPerdidas">
	   		
		   		<tr class="text-center">
					<td class="align-middle" style="text-align:center;"><s:property value="fechaRegistro"/></td>
					<td class="align-middle" style="text-align:center;padding-right: 2px;"><s:property value="monto"/></td>
					<td class="align-middle" style="text-align:center;padding-left: 2px;"><s:property value="numeroIncidente"/></td>
					<td class="align-middle" style="text-align:center;padding-left: 2px;"><s:property value="usuario"/></td>
					<td class="align-middle" style="text-align:center;" ><s:property value="numeroTarjeta"/></td>
					<td class="align-middle" style="text-align:center;"><s:property value="fechaTransaccion"/></td>
					<td class="align-middle" style="text-align:center;"><s:property value="comercio"/></td>
					<td class="align-middle" style="text-align:center;"><s:property value="tipoVenta.codigo"/></td>
					<td class="align-middle" style="text-align:center;padding-left: 1px;"><s:property value="tipoVenta.descripcion"/></td>
					<!-- <td class="align-middle" style="text-align:center;" onmouseover="showDescription('<s:property value="largeDescription"/>');" onmouseout="hideDescription();"><s:property value="shortDescription"/></td> -->
					
					<s:if test="%{largeDescription == null || largeDescription.equals('')}">
						<td class="align-middle"></td>
					</s:if>
					<s:else>
						<td class="align-middle" style="text-align:center;" onmouseover="showDescription('<s:property value="largeDescription"/>');" onmouseout="hideDescription();">Ver Más</td>
					</s:else>
					
					<td class="align-middle" style="text-align:center;"><s:property value="log.estadoContable"/></td>
					<td class="align-middle" style="text-align:center;"><s:property value="log.fechaContable"/></td>												
		   		</tr>	
		   	</s:iterator>
		
	   		
	   	</table>
	   	</div>
		</div>
		
	   
	   <div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial,Helvetica,sans-serif; font-size: 8pt;">
		   	<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		   	<s:property value="%{#request.numPagina}" /> De <s:property value="%{#request.ultimaPagina}" /> P&aacute;gina(s)&nbsp;&nbsp;&nbsp;
		   	Ir a pagina <input type="text" id="paginaIr" style="width:50px;font-size: 8pt;" class="form-control form-control-sm d-inline py-0 align-middle" /> <input type="button" value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt;"/>
		</div>
		
	</body>
</html>