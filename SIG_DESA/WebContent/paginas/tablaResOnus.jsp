<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Transacciones On Us</title>
	    <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
		<script src="js/jquery.Rut.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/utils.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				var tipoTransaccion =  $("input[name='tipotransaccion']:checked").val();
				var url;
				var parametros;
			
				
				if(tipoTransaccion=='01_00'){
					url = "transaccionOnUsVisaAction";
					parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&op=01";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();
					parametros = parametros + "&codigoFuncion=00";
					
				}
				if(tipoTransaccion=='02_00'){
					url = "transaccionOnUsVisaAction";
					parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&op=02";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();;
					parametros = parametros + "&codigoFuncion=00";
				}
				
				if(tipoTransaccion=='03_00'){
					url = "transaccionOnUsVisaAction";
					parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&op=03";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();;
					parametros = parametros + "&codigoFuncion=00";
				}
				if(tipoTransaccion=='04_00'){
					url = "transaccionOnUsVisaAction";
					parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&op=04";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();;
					parametros = parametros + "&codigoFuncion=00";
				}
				if($("#numPagina").val()==1)
				{	
					$("#anterior").bind("click",function(e){
				        e.preventDefault();
			        });
				}else{
					$("#anterior").unbind("click",false);					
				}
				
				
				if($("#ultimaPagina").val()<=1)
				{
					$("#botonesPaginacion").hide();
				}
			
				if($("#ultimaPagina").val()==0)
				{
					$("#anterior").hide();
					$("#siguiente").hide();
				}

				
				$("#siguiente").click(function(){
					if(parseInt($("#numPagina").val()) < parseInt($("#ultimaPagina").val()))
					{
						var numPag = parseInt($("#numPagina").val())+1;
						parametros = parametros + "&numPagina="+numPag;
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
				});
				
				$("#anterior").click(function(){
					if(parseInt($("#numPagina").val()) > 1)
					{
						var numPag = parseInt($("#numPagina").val())-1;
						parametros = parametros + "&numPagina="+numPag;
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
				});
				
				$("#ir").click(function(){
					
					var paginaIr;
					var ultimaPagina;
					
					paginaIr = $("#paginaIr").val();
					
					ultimaPagina = $("#ultimaPagina").val();
					
					if( paginaIr == '' || paginaIr == null || typeof paginaIr == "undefined"){
						alert("La página no existe");
						$("#paginaIr").val("");
						return;
					}
					
					if(Number(paginaIr) > Number(ultimaPagina) || Number(paginaIr) < 0 )
					{
						alert("La página no existe");
						$("#paginaIr").val("");
					}else{
						var numPag = paginaIr;
						
						parametros = parametros + "&numPagina="+$("#paginaIr").val();
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
						
				});
				
				
			});
		</script>
	</head>
	<body>
		<div></div>
		<input id="ultimaPagina" name="ultimaPagina" type="hidden" value="<s:property value="%{#request.ultimaPagina}" />"/>
		<input type="hidden" value="<s:property value="%{#request.numPagina}" />" name="numPagina" id="numPagina"/>
		
		<input type="hidden" value="<s:property value="%{#request.fechaInicioPag}" />" name="fechaInicioPag" id="fechaInicioPag"/> 
		<input type="hidden" value="<s:property value="%{#request.fechaTerminoPag}" />" name="fechaTerminoPag" id="fechaTerminoPag"/>
		<input type="hidden" value="<s:property value="%{#request.numeroTarjeta}" />" name="numeroTarjetaPag" id="numeroTarjetaPag"/> 
		 
		 <!-- style="width:800px" -->
	 	<div style=";overflow-x:hidden;overflow-y:auto;max-height:200px;">
			 <div style="margin:0px;padding:0px;">
			 
			 	<!-- class="tablaContenido" -->
				<table width="100%" class="table table-sm small" id="myTable" >
				
				
				<tr class="text-center">
						<th class="align-middle">SEL</th>
						<th class="align-middle">NRO TC</th>
						<th class="align-middle">FECHA COMPRA</th>
						<th class="align-middle">FECHA AUTORIZACION</th>
						<th class="align-middle">TIPO VENTA</th>
						<th class="align-middle">NUM-CUOTAS</th>
						<th class="align-middle">MONTO TRANSACCION</th>
						<th class="align-middle">VALOR CUOTA</th>
						<th class="align-middle">COD AUTOR</th>
						<th class="align-middle">PATPASS</th>
						
						
					</tr>
				 
			   		<s:iterator id="data" value="%{#request.listaTransacciones}">
				   		<tr class="text-center">
							<td class="align-middle">
								<input type='radio' name='seleccionTRX' value='C' class='seleccionTRX' onclick='mostrarTransaccionOnUs(this);'>
							</td>
								<td class="align-middle"><s:property value="numeroTarjeta"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="fechaCompra"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="fechaAutorizacion"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="tipoVenta"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="numCuotas"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="montoTransaccion"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="valorCuota"/></td>
								<td class="align-middle"><s:property value="codAutor"/></td>
								<td class="align-middle"><!-- PROPIEDAD PATPASS --></td>
							
<%-- 							<td tbl="microfil" style="display:none"><s:property value="microFilm"/></td> --%>
<%-- 							<td tbl="codigo_autorizacion" style="display:none"><s:property value="codigoAutorizacion"/></td> --%>
<%-- 							<td tbl="fechaefectiva" style="display:none"><s:property value="fecjaEfectova"/></td> --%>
<%-- 							<td tbl="fechaproceso" style="display:none"><s:property value="fechaProceso"/></td> --%>
<%-- 							<td tbl="binadquirente" style="display:none"><s:property value="binAdquiriente"/></td> --%>
<%-- 							<td tbl="leebanda" style="display:none"><s:property value="leeBanda"/></td> --%>
<%-- 							<td tbl="otrodato1" style="display:none"><s:property value="otrosDatos1"/></td> --%>
<%-- 							<td tbl="otrodato2" style="display:none"><s:property value="otrosDatos2"/></td> --%>
<%-- 							<td tbl="otrodato3" style="display:none"><s:property value="otrosDatos3"/></td> --%>
<%-- 							<td tbl="otrodato4" style="display:none"><s:property value="otrosDatos4"/></td> --%>
<%-- 							<td tbl="codmonedatrx" style="display:none"><s:property value="codMonedaTrx"/></td> --%>
<%-- 							<td tbl="rubrocomercio" style="display:none"><s:property value="rubroComercio"/></td> --%>
<%-- 							<td tbl="sid" style="display:none"><s:property value="sid"/></td> --%>
<%-- 							<td tbl="mit" style="display:none"><s:property value="mit"/></td> --%>
<%-- 							<td tbl="codigo_funcion" style="display:none"><s:property value="codigoFuncion"/></td> --%>
<%-- 							<td tbl="glosageneral" style="display:none"><s:property value="glosaGeneral"/></td> --%>
<%-- 							<td tbl="referencia" style="display:none"><s:property value="referencia"/></td> --%>
<%-- 							<td tbl="montoconciliacion" style="display:none"><s:property value="montoConciliacion"/></td> --%>
							
							
				   		</tr>	
				   	</s:iterator>
			   	</table>
			</div>
		</div>
		
		<div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial,Helvetica,sans-serif; font-size: 8pt;">
		   	<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">siguiente</a>
		   	<s:property value="%{#request.numPagina}" /> de <s:property value="%{#request.ultimaPagina}" /> p&aacute;gina(s)&nbsp;&nbsp;&nbsp;
		   	Ir a pagina <input type="text" id="paginaIr" style="width:50px;font-size: 8pt;" class="form-control form-control-sm d-inline py-0 align-middle" /> <input type="button" value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt;" />
		</div>
	</body>
</html>