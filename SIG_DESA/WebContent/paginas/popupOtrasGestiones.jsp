<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

   <head>
       
       <title>Objeciones</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       <script type="text/javascript" src="js/utils.js"></script>
       <script type="text/javascript" src="js/maxlength.js"></script>
       
       <script>  
         
	$(document).ready(function(){
	
			$("#numIncidente").val("<%=request.getParameter("numIncidente")%>");
       		
       		$.ajax({
       			url:'tpOtrsGestionesAction',
       			type:'POST'
       		}).done(function(data){
       			data.listaTipoObjecion;
       			var datos = (data.listaTpOtrasGestiones).split("~");
       			var strCombo = "";
       			
       			for(var i=0;i<datos.length-1;i++)
   				{			
       				var fila = datos[i].split("|");
       				
       				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
   				}
       			
       			$("#tpGestiones").append(strCombo);
       		});
       		
       		
       		$("#guardarObj").click(function(){
       			//alert($("#formObjecion").serialize())
       			//if($('#glosa').val().replace(/\ /g,'') != ''){
       			if( camposObligatoriosValidos()){
	       			$.ajax({
		       			url:'crearNuevaGestionAction',
		       			type:'POST',
		       			data:$("#formObjecion").serialize()
		       		}).done(function(data){
		       			
		       			if(data.ejecutaObjecion == '1')
	       				{
	       					alert("Se ha guardado una nueva gestion, del tipo: OtraGestion,  correctamente");
		       				buscarTransaccionPresentacion();
		       				$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});
	       				}
		       		});
       			}
    		});
       	});
       	//onclick="$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});"
       	$("#cancelarOtraGestion").click(function(){
        	
        	$('#popup').css({display: 'none'});
        	$('#popUpContenido').empty().css({display:'none'});
        	//$("#buscarTransaccion").click();
        	
        });
        
        
        function camposObligatoriosValidos(){
        
        	if($("#glosa").val().trim() == ''){
        		alert("Error : Campo glosa es obligatorio para otras gestiones, debe ingresar un texto.");
        		return false;
        	}
        	
        	return true;
        
        }
       
       </script>
   </head>
	
	<body>
		<form id="formObjecion">
			<input type="hidden" name="sid" value="<%=request.getParameter("sid")%>" />
			<input type="hidden" name="numeroTarjeta" value="<%=request.getParameter("numeroTarjeta")%>" />
			<input type="hidden" name="sidusuario" value="<%=request.getSession().getAttribute("sidusuario")%>" />
			
			<!--  <div class="titulo"> -->
			<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
				Gestionar Transaccion
			</div>
			<hr>
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
				<!-- contenedorInput -->
				<table width="100%" class="table table-sm">
			    	<tr>
			        	<td class="text-right align-middle">&nbsp;Tipo Gesti&oacute;n: </td>
		        		<td id="comboObjeciones align-middle">
		        			<select name="tpGestiones" id="tpGestiones" class="custom-select">
		        			</select>
		        		</td>
		 			</tr>
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
				<!-- contenedorInput -->
				<table width="100%" class="table table-sm"> 			
					<tr>
			        	<td class="CuerpoNormal align-middle"><!-- &nbsp;  -->(*) GLOSA: </td>
			 		</tr>
			 		<tr>
			 			<td class="align-middle">
			 				<!-- style="width: 490px;" -->
			 				<textarea id="glosa" rows="5" cols="58" name="glosa" data-maxsize="1999" class="form-control form-control-sm w-100"></textarea>
			 			</td>
			 		</tr>
			 			
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/> -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table width="100%" class="table table-sm">
			    	<tr>
			        	<td class="align-middle" >&nbsp;Nº Incidente:</td>
		        		<td class="align-middle">
		        			<input id="numIncidente" name="numIncidente" class="alphanumeric form-control form-control-sm" maxlength="50" size="50"  />
		        		</td>
		 			</tr>
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table  width="100%" class="table table-sm table-borderless">
			       		<tr class="text-center">
			       		<!-- align="center" -->
		 				<td >
		 				    <input id="guardarObj" type="button"  value="Guardar" class="btn btn-primary"/>
							<input type="button"  value="Cancelar" name="cancelarOtraGestion" id="cancelarOtraGestion" class="btn btn-primary">
		 				</td>
		 			</tr>
				</table>
			</div>
	      </form>
	</body>
</html>

