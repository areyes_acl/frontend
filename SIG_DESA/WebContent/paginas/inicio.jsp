<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
<script type="text/javascript" src="js/jquery-ui.js"> </script>
<title></title>
</head>

<body>
	<div class="cajaTitulo">
    	Bienvenido <%= request.getSession().getAttribute("nombre") %>
    </div>
    <div class="cajaContenido">
    	Pagina de inicio
    </div>
</body>
</html>
