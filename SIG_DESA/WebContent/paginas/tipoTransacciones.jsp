<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <jsp:include page="loginCheck.jsp" flush="true" />
<%@page import = "Beans.Usuario" %>
<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	
	<script type="text/javascript">
		var jsonSalida = new Object();
		var lista = "";
		 var prefixBtn = "btn-";
	
		$(document).ready(function(){	
			$.ajax({
				url:'tipoTransacAction',
				type:'POST',
				dataType: "json"
			}).done(function(resp){
				var datos = (resp.listaTipoTransac).split("~");
				
				for(var i=1;i<datos.length-1;i++)
				{
					
					var fila = datos[i].split("|");		
					
					var $tabla = $("#tablaRes");
					
					if(fila[2]== "O")
					{
						fila[2]="Objecion";						
					}else if(fila[2]== "R"){
						fila[2]="Rechazo";
					}
					
					if(fila[3]== "MN")
					{
						fila[3]="Mastercard Nacional";
					}else if(fila[3]== "MI"){
						fila[3]="Mastercard Internacional";
					}else if(fila[3]== "M"){
						fila[3]="Mastercard";
					}else if(fila[3]== "V"){
						fila[3]="Visa";
					}else if(fila[3]== "VI"){
						fila[3]="Visa Internacional";
					}else if(fila[3]== "VN"){
						fila[3]="Visa Nacional";
					}

					var textoAppend = '<tr class="text-center" id="trFuncion">'
						+'<td class="align-middle" style="display:none;">'+fila[0]+'</td>'
						// style="width:340px;height:18px;"
						+'<td class="align-middle" >'+fila[1]+'</td>'
						// style="width:100px;"
						+'<td class="align-middle" >'+fila[2]+'</td>'
						// style="width:135px;"
						+'<td class="align-middle" >'+fila[3]+'</td>'
						//  style="width:*;"
						+'<td class="align-middle">'
						+'<select class="combo custom-select" id="select'+i+'"><option value="1">Activo</option><option value="0">Inactivo</option></td>';

					<% if(usuarioLog.getActualizacion() == 1){ %>
						textoAppend = textoAppend + '<td class="align-middle">'
						// style="display: none;"
						+ '<button id="btn-'+fila[0]+'" type="button" class="btn btn-primary" value="'+fila[4]+'" disabled>Confirmar</button>'
						+ '</td>';
					<% }else{ %>
						textoAppend = textoAppend + '<td class="align-middle"></td></tr>';
					<% } %>
					
					$tabla.append(textoAppend);

					$("#select"+i+" option[value='"+fila[4]+"']").prop("selected",true);
					
					// "height": "20px"
					$('#select'+i).css({
					    "font-size": "9pt"
					});
				}

				for(var x=0;x<20-(datos.length-2);x++)
				{
					$tabla.append('<tr class="text-center" id="trFuncion">'
						// style="display:none;"
						+'<td class="align-middle">&nbsp;</td>'
						// style="width:340px;height:18px;"
						+'<td class="align-middle">&nbsp;</td>'
						// style="width:100px;"
						+'<td class="align-middle">&nbsp;</td>'
						// style="width:130px;"
						+'<td class="align-middle">&nbsp;</td>'
						// style="width:*;"
						+'<td class="align-middle">&nbsp;</td>'
						+'</tr>'
					);
				}
			}).error(function(error, status, setting){
				alert("Error TipoTransac: "+error);
			});
			
			$("#tablaRes").on('change','.combo',function(){
				var sid = $(this).parent().parent().find('td').eq('0').text();
				var estado = $(this).val();
				
				mostrarBotonConfirmar(sid, estado);
				
			});
			
			
			function mostrarBotonConfirmar(id, estadoActual){					
					var estadoOriginal = $('#'+prefixBtn+id).val(); 
					
					// SI CAMBIO EL ESTADO
					if(estadoOriginal != estadoActual){
					    $('#'+prefixBtn+id).bind("click",function(e){actualizarEstado(id, estadoActual); });	
						// $('#'+prefixBtn+id).show();
						$('#'+prefixBtn+id).prop("disabled", false);
					}else{
						// $('#'+prefixBtn+id).hide();
						$('#'+prefixBtn+id).prop("disabled", true);
					}
					
				}
				
			
			function actualizarEstado(sid, estado){
				
				$.ajax({
					url:'cambioEstadoTipoTransacAction',
					type:'POST',
						data : "sid="+ sid
						     + "&estado=" + estado
					   }).done(
							   function(data) {
								  
								   
							    	$('#'+prefixBtn+data.sid).val(data.estado);
								   	// $('#'+prefixBtn+data.sid).hide();
								   	$('#'+prefixBtn+data.sid).prop("disabled", true);
							     	$('#'+prefixBtn+data.sid).unbind("click");
									
									alert("Tipo de transacci�n se ha actualizado correctamente");
									   
					  }).fail(
					  		 function(data){
							 	 alert("Tipo de transacci�n se ha actualizado correctamente");
			    	  }).always(
			    	  	function(data){
			    	  			$('#'+prefixBtn+data.sid).unbind("click");
			    	  	})
			    	  
			    	  ; 
				
				}
			
		});
	</script>
</head>

<body>
	<!-- <div class="titulo"> -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold"> 
    	PAR&Aacute;METROS DE TIPO DE TRANSACCIONES
    </div>
    
    <hr/>
    
    <!-- <div class="cajaContenido">  -->
    <div>
    	<!-- <div>
    		class="tablaContenido"
	    	<table width="100%" class="table table-sm small">
				<tr>
	                <th style="width:340px;">
	                	Descripci&oacute;n
		        	</th>
	                <th style="width:100px;">
	                	Tipo
		        	</th>
	                <th style="width:135px;">
	                	Producto
		        	</th>
	                <th style="width:*;">
	                	Estado
		        	</th>
		        </tr>
		    </table>
		</div> -->
	    <div id="tablaResDiv">
	    	<!-- class="tablaContenido" -->
			<table width="100%" class="table table-sm small" id="tablaRes">
				<!-- Copiado de arriba, sin tama�os -->
				<tr class="text-center">
	                <th class="align-middle">
	                	DESCRIPCI&Oacute;N
		        	</th>
	                <th class="align-middle">
	                	TIPO
		        	</th>
	                <th class="align-middle">
	                	PRODUCTO
		        	</th>
	                <th class="align-middle w-25">
	                	ESTADO
		        	</th>
		        	<th class="align-middle">
	                	ACCION
		        	</th>
		        </tr>
		    </table>
		</div>
    </div>
    
</body>
</html>
