<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import = "Beans.Usuario" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	<script type="text/javascript" src="js/utils.js"></script>
	<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
	 <% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>
	 
	<script type="text/javascript">
		var jsonSalida = new Object();
		var lista = "";
	
		$(document).ready(function(){	
		
			$("#tablaRes").on('change','.combo',function(){
				var sid = $(this).parent().parent().find('td').eq('0').text();
				var estado = $(this).val();
				$.ajax({
					url:'cambiarEstadoDiaInhabilAction',
					type:'POST',
					data:"sid="+sid+"&estado="+estado
				}).done(function(data){

				});
			});
			
			/* Clic botón nuevoDiaInhabil habilita formulario para agregar nuevo día inhábil */
			$("#nuevoDiaInhabil").click(function(event){
					$("#fecha").val("");
					$("#glosa").val("");
					$("#estadodiainhabilactivo").prop("checked", "checked");
					$("#estadodiainhabilinactivo").prop("checked", "");
					$("#idpantallaDatos").css("display","");
					$("#nuevoDiaInhabil").attr("disabled", true);
					$("#grabarDiaInhabil").attr("disabled", false);
					$("#cancelaDiaInhabil").attr("disabled", false);
			});
			
			/* Clic botón grabarDiaInhabil se guarda el registro en la base de datos */
			$("#grabarDiaInhabil").click(function(event){
					if ($('#fecha').val().length  == 0){
						alert("Debe ingresar la fecha...");
						$("#fecha").focus();
						return;
					}
					if ($('#glosa').val().length  == 0){
						alert("Debe ingresar la glosa...");
						$("#glosa").focus();
						return;
					}
					if (!confirm('Esta seguro que desea grabar la informacion ingresada?')) {
					    return;
					}
					
					var parametros = "";
					var valorChecked = $("input[name='estado']:checked").val();
					
					parametros = parametros + "&glosa=" + $('#glosa').val();
					parametros = parametros + "&fecha=" + $('#fecha').val();
					parametros = parametros + "&estado=" + valorChecked;
					$.ajax({
						url: "grabarDiasInhabilesAction",
    					type: "POST",
    					data:parametros,
    					dataType: "json",
    					error: function(XMLHttpRequest, textStatus, errorThrown){
        						alert('Error ' + textStatus);
        						alert(errorThrown);
        						alert(XMLHttpRequest.responseText);
        						
    				},
    				    success: function(data){
   				    	    if(data.codigoError == 0){
   				    	    	//Incorrecto -  Fecha repetida
   				    	    	alert("El día inhábil ya se encuentra registrado");
   				    	    }
   				    	    else if (data.codigoError == 1){
   				    	    	//Correcto - recargar página
   				    	    	$("#tablaRes").empty();	
   				    	    	cargarDiasInhabiles();
   				    	    	$("#idpantallaDatos").css("display","none");
								$("#nuevoDiaInhabil").attr("disabled", false);
								$("#grabarDiaInhabil").attr("disabled", true);
								$("#cancelaDiaInhabil").attr("disabled", true);
   				    	    }
   				    	    else{
	   				    	    //No controlado
								alert("Error no controlado");
   				    	    }
       					}
					}); 
			});
			
			/* Clic botón cancelarDiaInhabil desparece el formulario */
			$("#cancelaDiaInhabil").click(function(event){
					$("#grabarDiaInhabil").attr("disabled", true);
					$("#cancelaDiaInhabil").attr("disabled", true);
					$("#nuevoDiaInhabil").attr("disabled", false);
					$("#idpantallaDatos").css("display","none");
			});
			
			/* Código para buscar días inhábiles por año */
			$("#buscar").click(function(){
					if($('#year').val() != null && $('#year').val() != "") {
						cargarDiasInhabiles();
					}
					else
					{
						alert("Debe elegir un año");
					}
			});
		});
		
		function cargarDiasInhabiles(){
			var parametros = "&ano=" + $('#year').val();
			if(parametros == "&ano=null"){
				var f=new Date();
				parametros = "&ano=" + f.getFullYear();
			}
			$("#tablaRes").empty();	
    		$.ajax({
				url:'buscarDiasInhabilesAction',
				type:'POST',
				data: parametros,
				dataType: "json"
			}).done(function(resp){
				var $tabla = $("#tablaRes");
				
				$tabla.append('<tr class="text-center"><th class="align-middle">Glosa</th><th class="align-middle">Fecha</th><th class="align-middle w-25">Estado</th></tr>');
				var datos = (resp.listaDiasInhabiles).split("~");
				if(resp.codigoError != 0 ){
					//alert(datos[1]);
				}
				else{
				
				for(var i=1;i<datos.length-1;i++)
				{
					var fila = datos[i].split("|");		
					var filaHTML ="";
					
					<% if(usuarioLog.getActualizacion() == 1){ %>
    						filaHTML = '<tr class="text-center" id="trFuncion">'
						+'<td class="align-middle" style="display:none;">'+fila[0]+'</td>'
						+'<td class="align-middle">'+fila[1]+'</td>'
						+'<td class="align-middle" align="center">'+fila[2]+'</td>'
						+'<td class="align-middle" align="center">'
						+'<select class="combo custom-select" id="select'+i+'"><option value="1">Activo</option><option value="0">Inactivo</option></td>'
						+'</tr>';
						
    					<% }else{ %>
    						filaHTML = '<tr class="text-center" id="trFuncion">'
							+'<td class="align-middle" style="display:none;">'+fila[0]+'</td>'
							+'<tdclass="align-middle" >'+fila[1]+'</td>'
							+'<td class="align-middle" align="center">'+fila[2]+'</td>'
							+'<td class="align-middle" align="center">'
							+'<select class="combo custom-select" id="select'+i+'" disabled><option value="1">Activo</option><option value="0">Inactivo</option></td>'
							+'</tr>';
    				<% } %>
					
					$tabla.append(filaHTML);

					$("#select"+i+" option[value='"+fila[3]+"']").prop("selected",true);
					
					/* "height": "20px" */
					$('#select'+i).css({
					    "font-size": "9pt"
					});
				}

				// SE COMENTA ESTE for PARA EVITAR QUE APAREZCAN FILAS VACIAS
				/* for(var x=0;x<20-(datos.length-2);x++)
				{
					$tabla.append('<tr class="text-center" id="trFuncion">'
						+'<td class="align-middle" style="display:none;">&nbsp;</td>'
						+'<td class="align-middle">&nbsp;</td>'
						+'<td class="align-middle">&nbsp;</td>'
						//+'<td style="width:130px;">&nbsp;</td>'
						+'<td class="align-middle">&nbsp;</td>'
						+'</tr>'
					);
				} */
				}
				
			}).error(function(error, status, setting){
				alert("Error DiaInhabil: "+error);
			});
    	}
		    		
		function desabilitoBotones(){
    		$("#nuevoDiaInhabil").attr("disabled", true);
			$("#cancelaDiaInhabil").attr("disabled", true);
			$("#grabarDiaInhabil").attr("disabled", true);
    	}
    	
	</script>
	<script>
		// PREPARAR DATEPICKER
		var min = new Date(2000, 0, 1);
		var max = new Date(new Date().getFullYear()+5,11,31);
		$('#year').datepicker({
		    format: "yyyy",
		    startDate: min,
		    endDate: max,
		    minViewMode: 2,
		    maxViewMode: 3
		});
		$('#year').datepicker("setDate", new Date());
		
		$("#fecha").datepicker();
	</script>
</head>

<body>
	<!--  <div class="titulo"> -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold "> 
    	MANTENEDOR DE D&Iacute;AS INH&Aacute;BILES
    </div>
    <hr/>
    <script>cargarDiasInhabiles();</script>
    
    <!-- <br style="line-height:1px"/>  -->
   
      
      <div>
      	<!-- class="contenedorInput" -->
      		<table width="100%" class="table table-sm table-borderless">
               <tr align="center">
           			<td class="align-middle">
           				A&Ntilde;O
           				<%-- <select name="year" id="year" class="custom-select col-2"></select>
						  <script>
						  var j = new Date().getFullYear();
						  var i = new Date().getFullYear();
						  for (i = j + 5 ; j <= i ; i--)
							{
							    $('#year').append($('<option />').val(i).html(i));
							}
						  for (i = new Date().getFullYear()-1; i > 1999; i--)
							{
							    $('#year').append($('<option />').val(i).html(i));
							}
							$('#year').val(new Date().getFullYear());
						  </script> --%>
						
						<input name="year" id="year" type="text" maxlength="4" class="numericOnly datepicker form-control text-center d-inline col-1" placeholder="AAAA" required pattern="[0-9]+" >
              			<input type="button"  id="buscar" value="Buscar" class="btn btn-primary btn-sm">
             		</td>
       			</tr>
      		</table>
      </div>
    
    <!-- <br style="line-height:5px"/>  -->
    
    <!-- class="cajaContenido" -->
    <div>
    	
	    <div class="mb-3 w-75 mx-auto h-50" id="tablaResDiv">
	    	<!-- class="tablaContenido" -->
			<table width="100%" class="table table-sm small" id="tablaRes">
				
		    </table>
		</div>
    </div>
      <div class="w-50 mx-auto">
      		<!-- class="contenedorInput" -->
      		<table width="100%" style="display:none;" id="idpantallaDatos" class="table table-sm small">
      
       				<tr class="text-right">
               			<td class="align-middle">Fecha:</td>
              			<td class="align-middle text-left" >
              				<!-- size="10" -->
              				<input type="text"  name="fecha" id="fecha" maxlength="10" size="12" class="campoObligatorio fecha form-control form-control-sm col-5 text-center d-inline datepicker" placeholder="DD/MM/AAAA" />
               			</td>
       				</tr>
       		
       				<tr class="text-right">
       			    	<td class="align-middle">Glosa:</td>
       					<td class="align-middle text-left" >
       						<!-- size="60" -->
       						<input type="text"  name="glosa" id="glosa" maxlength="50" class="campoObligatorio form-control form-control-sm"/>
   						</td>
       				</tr>
       				
       				<tr class="text-right">
       			    	<td class="align-middle">Estado D&iacute;a Inh&aacute;bil:</td>
       					<td class="align-middle text-left" >
       						<div class="custom-control custom-radio custom-control-inline">
       							<input type="radio" name="estado"  id="estadodiainhabilactivo" value="1" class="campoObligatorio custom-control-input">
       							<label for="estadodiainhabilactivo" class="custom-control-label">Activo</label>
       						</div>
       						<div class="custom-control custom-radio custom-control-inline">
       							<input type="radio" name="estado"  id="estadodiainhabilinactivo" value="0" class="campoObligatorio custom-control-input">
       							<label for="estadodiainhabilinactivo" class="custom-control-label">Inactivo</label>
       						</div>
						</td>
       				</tr>
      				
        		</table>
        
        </div>
    
        <!-- <br style="line-height:5px"/> -->
        
      	<div>
      		<!-- class="contenedorInput" -->
     		<table width="100%" class="table table-sm table-borderless">
     	       		<tr>
     					<td align="center">
     					<% if(usuarioLog.getCreacion() == 1){ %>
    							<input type="button"  id="nuevoDiaInhabil" value="Nuevo" class="btn btn-primary">
    					<% }else{ %>
    							<input type="button"  id="nuevoDiaInhabil" value="Nuevo" class="btn btn-primary" disabled>
    					<% } %>
     				    	
     				    	<input type="button"  id="grabarDiaInhabil" value="Grabar" class="btn btn-primary" disabled>
     				    	<input type="button"  id="cancelaDiaInhabil" value="Cancelar" class="btn btn-primary" disabled>
     					</td>
     				</tr>
     		</table>
     	</div>
</body>
</html>
