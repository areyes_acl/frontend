<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Rechazos Diarios</title>
	    <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
		<script src="js/jquery.Rut.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/utils.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				var tipoTransaccion =  $("input[name='tipotransaccion']:checked").val();
				var url;
				var parametros;
				//alert(tipoTransaccion);
				
				if (tipoTransaccion==0){ //Transaccion De Presentacion.//
					url = "transaccionPresentacionAction";
					parametros = "numeroTarjeta=" + $('#numeroTarjetaPag').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&op=";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();
					
				}
				
				if (tipoTransaccion >= 1 && tipoTransaccion <= 8){

					var codigoTransaccion = "";
					var codigoFuncion = "";
					
					if (tipoTransaccion==1){//Primer Contracargo Total
						codigoTransaccion = "1442";
						codigoFuncion = "450";
					}else if (tipoTransaccion==2){//Primer Contracargo Parcial
						codigoTransaccion = "1442";
						codigoFuncion = "453";
					}else if (tipoTransaccion==3){//Segundo Contracargo Total
						codigoTransaccion = "1442";
						codigoFuncion = "451";
					}else if (tipoTransaccion==4){//Segundo Contracargo Parcial
						codigoTransaccion = "1442";
						codigoFuncion = "454";
					}else if (tipoTransaccion==5){//Peticion De Vale
						codigoTransaccion = "1644";
						codigoFuncion = "603";
					}else if (tipoTransaccion==6){//Confirmacion Peticion Vale
						codigoTransaccion = "1644";
						codigoFuncion = "605";
					}else if (tipoTransaccion==7){//Representacion Total
						codigoTransaccion = "1240";
						codigoFuncion = "205";
					}else if (tipoTransaccion==8){//Representacion Parcial
						codigoTransaccion = "1240";
						codigoFuncion = "282";
					}
					//buscarTxAccion
					
					url = "transaccionAccionesAction";
					
					parametros = "numeroTarjeta=" + $('#numeroTarjetaPag').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&op=";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();
					
					
					parametros = parametros + "&codigoTransaccion=" + codigoTransaccion;
					parametros = parametros + "&codigoFuncion=" + codigoFuncion;
					
					parametros = parametros + "&op=";
					
					
				}
				
				if (tipoTransaccion==9 || tipoTransaccion==10 ){ //Transaccion De Objeciones Rechazos.  // 
					
				    var tipoObjecionRechazo="";
				    url = "transaccionObjecionRechazoAction";
				    if (tipoTransaccion==9){ //Transaccion De Objeciones.  // 
				    	tipoObjecionRechazo = "OBJ";
					}				    
				    if (tipoTransaccion==10){ //Transaccion De Rechazos.  // 
				    	tipoObjecionRechazo = "REC";
					}
				    
  					parametros = "numeroTarjeta=" + $('#numeroTarjetaPag').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&tipoObjecionRechazo=" + tipoObjecionRechazo;
					parametros = parametros + "&op=";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();
				}
				
				if (tipoTransaccion==11){ //Transaccion Recuperacion De Cargos Incoming.  //
					url="transaccionRecuperacionCargosIncAction";
					codigoTransaccion = "1740";
					parametros = "numeroTarjeta=" + $('#numeroTarjetaPag').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&codigoTransaccion=" + codigoTransaccion;
					parametros = parametros + "&op=";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();
					
				}
				
				if (tipoTransaccion==12){ //Transaccion Recuperacion De Cargos Outgoing.  //
					codigoTransaccion = "1740";
					url = "transaccionRecuperacionCargosOutAction";
					
					parametros = "numeroTarjeta=" + $('#numeroTarjetaPag').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&codigoTransaccion=" + codigoTransaccion;
					parametros = parametros + "&op=";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();
						
					
					
				}
				
				if (tipoTransaccion==13){ //Transaccion Confirmacion de recepcion de vales.  //
					codigoTransaccion = "CONF_RVL"; 
					
					url="transaccionConfirmacionRecepcionPeticionValesAction";
					
					parametros = "numeroTarjeta=" + $('#numeroTarjetaPag').val();
					parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
					parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
					parametros = parametros + "&codigoTransaccion=" + codigoTransaccion;
					parametros = parametros + "&op=";
					parametros = parametros + "&cantReg=" + $('#cantReg').val();
					
				    
				}
				
				if($("#numPagina").val()==1)
				{	
					$("#anterior").bind("click",function(e){
				        e.preventDefault();
			        });
				}else{
					$("#anterior").unbind("click",false);					
				}
				
				
				if($("#ultimaPagina").val()<=1)
				{
					$("#botonesPaginacion").hide();
				}
			
				if($("#ultimaPagina").val()==0)
				{
					$("#anterior").hide();
					$("#siguiente").hide();
				}
				
				
				$("#siguiente").click(function(){
					if(parseInt($("#numPagina").val()) < parseInt($("#ultimaPagina").val()))
					{
						var numPag = parseInt($("#numPagina").val())+1;
						parametros = parametros + "&numPagina="+numPag;
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
				});
				
				$("#anterior").click(function(){
					if(parseInt($("#numPagina").val()) > 1)
					{
						var numPag = parseInt($("#numPagina").val())-1;
						parametros = parametros + "&numPagina="+numPag;
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
				});
				
				$("#ir").click(function(){
					
					var paginaIr;
					var ultimaPagina;
					
					paginaIr = $("#paginaIr").val();
					
					ultimaPagina = $("#ultimaPagina").val();
					
					if( paginaIr == '' || paginaIr == null || typeof paginaIr == "undefined"){
						alert("La página no existe");
						$("#paginaIr").val("");
						return;
					}
					
					if(Number(paginaIr) > Number(ultimaPagina) || Number(paginaIr) < 0 )
					{
						alert("La página no existe");
						$("#paginaIr").val("");
					}else{
						var numPag = paginaIr;
						
						parametros = parametros + "&numPagina="+$("#paginaIr").val();
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
						
				});
				
				
			});
		</script>
	</head>
	<body>
		<div></div>
		<input id="ultimaPagina" name="ultimaPagina" type="hidden" value="<s:property value="%{#request.ultimaPagina}" />"/>
		<input type="hidden" value="<s:property value="%{#request.numPagina}" />" name="numPagina" id="numPagina"/>
		
		<input type="hidden" value="<s:property value="%{#request.fechaInicioPag}" />" name="fechaInicioPag" id="fechaInicioPag"/> 
		<input type="hidden" value="<s:property value="%{#request.fechaTerminoPag}" />" name="fechaTerminoPag" id="fechaTerminoPag"/>
		<input type="hidden" value="<s:property value="%{#request.numeroTarjeta}" />" name="numeroTarjetaPag" id="numeroTarjetaPag"/> 
		 
		 
	 	<div style="width:800px;overflow-x:hidden;overflow-y:auto;max-height:105px;">
			 <div style="margin:0px;padding:0px;">
				<table width="100%" class="tablaContenido" id="myTable" >
				
				
				<tr>
						<th>SEL</th>
						<th>NRO TC</th>
						<th>FECHA TRANSACC.</th>
						<th>COMERCIO</th>
						<th>PAIS</th>
						<th>MONTO ORIG</th>
						<th>MONTO</th>
						<th>COD. RAZON</th>
						<th>ESTATUS</th>
						<th>Transacci&oacute;n</th>
						<th>PATPASS</th>
					</tr>
				 
			   		<s:iterator id="data" value="%{#request.listaTransacciones}">
				   		<tr>
							<td><input type='radio' name='seleccionTRX' value='C' class='seleccionTRX' onclick='mostrarTransaccion(this);'></td>
								<td><s:property value="numeroTarjeta"/></td>
								<td style="text-align: center;"><s:property value="fechaTransac"/></td>
								<td><s:property value="comercio"/></td>
								<td style="text-align: center;"><s:property value="pais"/></td>
								<td style="text-align: right;"><s:property value="montoTransac"/></td>
								<td style="text-align: right;"><s:property value="montoFacturacion"/></td>
								<td style="text-align: center;"><s:property value="codRazon"/></td>
								<td><s:property value="estadoTrx"/></td>
								
							
							<td tbl="microfil" style="display:none"><s:property value="microFilm"/></td>
							<td tbl="codigo_autorizacion" style="display:none"><s:property value="codigoAutorizacion"/></td>
							<td tbl="fechaefectiva" style="display:none"><s:property value="fecjaEfectova"/></td>
							<td tbl="fechaproceso" style="display:none"><s:property value="fechaProceso"/></td>
							<td tbl="binadquirente" style="display:none"><s:property value="binAdquiriente"/></td>
							<td tbl="leebanda" style="display:none"><s:property value="leeBanda"/></td>
							<td tbl="otrodato1" style="display:none"><s:property value="otrosDatos1"/></td>
							<td tbl="otrodato2" style="display:none"><s:property value="otrosDatos2"/></td>
							<td tbl="otrodato3" style="display:none"><s:property value="otrosDatos3"/></td>
							<td tbl="otrodato4" style="display:none"><s:property value="otrosDatos4"/></td>
							<td tbl="codmonedatrx" style="display:none"><s:property value="codMonedaTrx"/></td>
							<td tbl="rubrocomercio" style="display:none"><s:property value="rubroComercio"/></td>
							<td tbl="sid" style="display:none"><s:property value="sid"/></td>
							<td tbl="mit" style="display:none"><s:property value="mit"/></td>
							<td tbl="codigo_funcion" style="display:none"><s:property value="codigoFuncion"/></td>
							<td tbl="glosageneral" style="display:none"><s:property value="glosaGeneral"/></td>
							<td tbl="referencia" style="display:none"><s:property value="referencia"/></td>
							<td tbl="montoconciliacion" style="display:none"><s:property value="montoConciliacion"/></td>
							<td tbl="tipotrx" ><s:property value="tipoTrx"/></td>
				   		</tr>	
				   	</s:iterator>
			   	</table>
			</div>
		</div>
		
		<div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial,Helvetica,sans-serif; font-size: 8pt;">
		   	<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">siguiente</a>
		   	<s:property value="%{#request.numPagina}" /> de <s:property value="%{#request.ultimaPagina}" /> p&aacute;gina(s)&nbsp;&nbsp;&nbsp;
		   	Ir a pagina <input type="text" id="paginaIr" style="width:50px;font-size: 8pt;" class="form-control form-control-sm d-inline py-0 align-middle" /> <input type="button" value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt;"/>
		</div>
	</body>
</html>