<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>

<html>
<head>
<title>Solicitud 60 Masiva</title>
</head>
<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>
<body>
	<!-- POP UP DE CARGOS Y ABONOS MASIVOS -->
	<div id="popup" style="display: none;position:fixed;" ></div>
	<div id="popUpContenido" class="content-popup" style="display: none; min-height: 115px!important;"></div>

	<form id="form1">
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
			SOLICITUD 60 MASIVA
		</div>
<hr/>
		<!-- <br style="line-height: 5px" />  -->

		<div>
			<!-- class="contenedorInput" -->
			<table style="width: 100%" class="table table-sm">
				<tr class="text-right">
					<td class="align-middle">TC:</td>
					<td class="align-middle">
						<!-- style="width: 106px"  -->
						<input type="text" value="" name="numeroTarjeta"
							id="numeroTarjeta" class="numericOnly form-control form-control-sm" maxlength="16" />
					</td>
					<td class="align-middle">DESDE:</td>
					<td class="align-middle text-center">
					
						<!-- style="width: 57px;" size="10" -->
						<input type="text" maxlength="10" value="" name="fechaDesde" id="fecha-desde"
							class="campoObligatorio fecha form-control form-control-sm text-center col-9 mx-auto datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td class="align-middle">HASTA:</td>
					<td class="align-middle text-center">
					<!-- size="10" style="width: 57px;" -->
						<input type="text" maxlength="10" value="" name="fechaHasta" id="fecha-hasta"
							class="campoObligatorio fecha form-control form-control-sm text-center col-9 mx-auto datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td class="align-middle" align="center">
						<input type="button" value="Buscar"
							name="buscarTransaccion" id="buscar" class="btn btn-primary"/>
					</td>
				</tr>
			</table>
		</div>


		<!-- <br style="line-height: 5px" />  -->

		<!--  class="subTitulo" -->
	<div id="tablaResTitulo" style="display: none;"
			class="col-6 mx-auto my-4 small text-align-center text-center font-weight-bold">
			Listado de Solicitud Masiva
		</div>
		
		<!-- class="cajaContenido" -->
		<div>
			<div id="tablaRes"></div>
		</div>

		<!-- <br style="line-height: 5px" />  -->

		<!-- class="contenedorInput" -->
		<table class="table table-sm table-borderless mt-2" style="width: 100%;">
			<tr>
				<td align="center">
					<% if(usuarioLog.getCreacion() == 1){ %>
						<input type="button"value="Generar Abono Masivo" id="cargoabono" class="btn btn-primary" disabled />
					<% } %>
					<input type="button" value="Exportar" name="exportar" id="exportar" class="btn btn-primary" disabled /> 
				</td>
			</tr>
		</table>


	</form>
</body>
<script type="text/javascript" src="js/solicitud60/solicitud60Masiva.js"></script>
<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script>
	$(".datepicker").datepicker();
</script>
</html>