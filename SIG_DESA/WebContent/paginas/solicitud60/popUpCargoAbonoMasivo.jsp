
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<title>Cargos y Abonos Masivos</title>

<script>

var listaTrxSeleccionadas;

	$(document).ready(function() {
		
		var opcionesCargo = "";
		var opcionesAbono = "";
		listaTrxSeleccionadas =<%= request.getParameter("array")%>;	
		
		
		$.ajax({
				url:'listaCargoAbonoAction',
				type:'POST',
				dataType: "json"
			}).done(function(resp){
				var datos = (resp.listaCargoAbono).split("~");
				$("#serieData").val(datos);
				
				for(var i=1;i<datos.length-1;i++)
				{
					var fila = datos[i].split("|");
					
					if(fila[3]=='C')
					{
						opcionesCargo += "<option value='"+fila[0]+"'>"+fila[2]+"</option>";
					}else{
						opcionesAbono += "<option value='"+fila[0]+"'>"+fila[2]+"</option>";
					}
				}
				
				if($("#cargo").prop("checked"))
				{
					$("#tipoCargoAbono").empty().append(opcionesCargo);
				}else{
					$("#tipoCargoAbono").empty().append(optA);
				}
				
				
			});
			
			
			// SELECCION DE TIPO CARGO / ABONO
			$("input:radio").click(function(){
        		if($("#cargo").prop("checked"))
       			{
        			$("#tipoCargoAbono").empty();
        			$("#tipoCargoAbono").append(opcionesCargo);
       			}else{
       				$("#tipoCargoAbono").empty();
       				$("#tipoCargoAbono").append(opcionesAbono);
       			}
       		});
	});
	
	
	
	/**
	 * GUARDAR CARGO Y ABONO
	 **/ 	
	$("#guardar").click(function(){
	
		var tipoCA = $('input:radio[name=group1]:checked').val();
		var tipo = $( "#tipoCargoAbono" ).val();

		// CARGA LISTA DE CARGOS Y ABONOS
		$.ajax({
				url:'registrarMasivos',
				type:'POST',
				data : {
						listaSeleccionada : listaTrxSeleccionadas,
						tipoCargoAbono : tipoCA,
						sidTipoCA: tipo
						},
				dataType: "json",
				traditional:true,
			})
			.done(function(data){
				alert(data.mensajeError);
				$("#buscar").click();
			});
	
        	$('#popup').css({display: 'none'});
        	$('#popUpContenido').empty().css({display:'none'});
        	
        });
	
	
	
	/**
	 * CANCELAR CARGO/ABONO
	 **/ 	
	$("#cancelarCargarAbonar").click(function(){
        	
        	$('#popup').css({display: 'none'});
        	$('#popUpContenido').empty().css({display:'none'});
        	
        });
        
</script>

</head>

<body>
	<form id="formCA">
		<input type="hidden" id="serieData" /> 
		<input type="hidden" id="sid" name="sid" /> <input type="hidden" id="codrazon" name="codRazon" />
		<input type="hidden" id="listaSelected" /> 

		<!-- <div class="titulo">  -->
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold border border-dark">
			Cargo / Abono
		</div>

		<!-- <br style="line-height: 5px" />  -->
		
		<div>	
			<!-- class="contenedorInput" -->
			<table width="100%" class="table table-sm">
				<tr>
					<td class="text-right">Tipo:</td>
					<td>
						<div class="custom-control custom-radio">
							<input type="radio" id="cargo" name="group1" value="C" class="custom-control-input" checked>
							<label class="custom-control-label" for="cargo">Cargo</label>
						</div>
						
						<div class="custom-control custom-radio">
							<input type="radio" id="abono"name="group1" value="A" class="custom-control-input">
							<label class="custom-control-label" for="abono">Abono</label>
						</div>
					</td>
				</tr>
				<!-- <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr> -->
				<tr>
					<td class="text-right">Tipo cargo / abono:</td>
					<td><select name="tipoCargoAbono" id="tipoCargoAbono" style="" class="custom-select">
					</select></td>
				</tr>
				<!-- <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr> -->
			</table>

		</div>
		
		<!-- <br style="line-height: 5px" />  -->
		
		<!-- <br style="line-height: 5px" />  -->
	
		<div>
			<!-- class="contenedorInput" -->
			<table width="100%" class="table table-sm table-borderless">
				<tr>
					<td align="center"><input type="button"
						id="guardar" value="Guardar" class="btn btn-primary" /> <input
						type="button" value="Cancelar" id="cancelarCargarAbonar"
						name="cancelarCargarAbonar" class="btn btn-primary" /></td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>
