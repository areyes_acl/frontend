<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
			$(document).ready(function(){
			
				checkbox();
			
				desbloqueaExport();
											
				if($("#numPagina").val()==1)
				{	
					$("#anterior").bind("click",function(e){
				        e.preventDefault();
			        });
				}else{
					$("#anterior").unbind("click",false);					
				}
				
				if($("#ultimaPagina").val()<=1)
				{
					$("#botonesPaginacion").hide();
				}
				
				if($("#ultimaPagina").val()==0)
				{
					$("#anterior").hide();
					$("#siguiente").hide();
				}
				
				$("#siguiente").click(function(){
				
					if($("#numPagina").val() < $("#ultimaPagina").val())
					{
						var numPag = parseInt($("#numPagina").val())+1;
						buscaTransaccion($("#fecha-desde").val(),$("#fecha-hasta").val(),numPag);
					}
				});
				
				$("#ir").click(function(){
					
					var paginaIr;
					var ultimaPagina;
					
					paginaIr = $("#paginaIr").val();
					
					ultimaPagina = $("#ultimaPagina").val();
					
					 
					if( paginaIr == '' || paginaIr == null || typeof paginaIr == "undefined" || paginaIr == 0 || isNaN(paginaIr)){
						alert("La página no existe");
						$("#paginaIr").val("");
						return;
					}
					
					if(Number(paginaIr) > Number(ultimaPagina) || Number(paginaIr) < 0 )
					{
						alert("La página no existe");
						$("#paginaIr").val("");
					}else{
						var numPag = paginaIr;
						buscaTransaccion($("#fecha-desde").val(),$("#fecha-hasta").val(),numPag);
					}
						
				});
				
				$("#anterior").click(function(){
					if($("#numPagina").val() > 1)
					{
						var numPag = parseInt($("#numPagina").val())-1;
						buscaTransaccion($("#fecha-desde").val(),$("#fecha-hasta").val(),numPag);
					}
				});
			});
			
		
		// FUNCION DE CONTROL DE CHECK	
		function checkbox(){
		
			// SI TODOS ESTAN CHEQUEADOS, SELECCIONA EL ALL
			if($('.checkbox1:checked').length == $('.checkbox1').length){
			    $('input[name="select_all"]').prop('checked', true);
			}
		
			$('#select_all').click(function(event) {
   				 if(this.checked) { // check select status
          				 $('.checkbox1').each(function() { //loop through each checkbox
               				this.checked = true;  //select all checkboxes with class "checkbox1"               
           				});
       			}else{
           			$('.checkbox1').each(function() { //loop through each checkbox
               			this.checked = false; //deselect all checkboxes with class "checkbox1"                       
           			});         
       			}
   			});
    		
    		
   			$('input[name="check[]"]').change(function () {
   		
			   var selectAll = true;
			    // Confirm all checkboxes are checked (or not)
			    $('input[name="check[]"]').each(function (i, obj) {
			        if ($(obj).is(':checked') === false) {
			            // At least one checkbox isn't selected
			            selectAll = false;
			            return false;
			        }
			    });
			    // Update "Select All" checkbox appropriately
			    $('input[name="select_all"]').prop('checked', selectAll);
			    
			    // SI SE ELIGIO AL MENOS UN CHECKBOX, HABILITA ABONO MASIVO
				if($('input[name="check[]"]:checked').length > 0){
					$("#cargoabono").prop("disabled", false);
				}
				else {
					$("#cargoabono").prop("disabled", true);
				}
			});
		}
		
		
		// FUNCION PARA DESBLOQUEAR EL BOTON DE EXPORTAR
		function desbloqueaExport (){
			var cantidadElementos  = $("#cant-registros").val();
			
				if(cantidadElementos > 0){
					$("#exportar").prop("disabled", false);
				}else{
					$("#exportar").prop("disabled", true);
				} 
		}

</script>

<style type="text/css">
#btn-delete:active {
	opacity: 0.3;
}
</style>
</head>
<body>
	<div></div>
	<input type="hidden" value="<s:property value="%{#request.cantidadElementos}" />" name="cantidadRegistros" id="cant-registros" />
	<input type="hidden" value="<s:property value="%{#request.ultimaPagina}" />" name="ultimaPagina" id="ultimaPagina" />
	<input type="hidden" value="<s:property value="%{#request.numPagina}" />" name="numPagina" id="numPagina" />
	<input type="hidden" value="<s:property value="%{#request.fechaInicioPag}" />" name="fechaInicioPag" id="fechaInicioPag" />
	<input type="hidden" value="<s:property value="%{#request.fechaTerminoPag}" />" name="fechaTerminoPag" id="fechaTerminoPag" />
	<input type="hidden" value="<s:property value="%{#request.numeroTarjeta}" />" name="numeroTarjetaPag" id="numeroTarjetaPag" />

	<div style="width: 800px; overflow-x: auto; overflow-y: auto; max-height: 449px;">
		<div style="margin: 0px; padding: 0px;">
			<!-- class="tablaContenido" -->
			<table style="width: 100%" class="table table-sm small" id="tabla">

				<tr class="text-center">
					<th class="align-middle">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" id="select_all" name="select_all" class="custom-control-input"/>
							<label for="select_all" class="custom-control-label"></label>
						</div>
					</th>
					<th class="align-middle">NRO TC</th>
					<th class="align-middle">FECHA TRANSACC.</th>
					<th class="align-middle">COMERCIO</th>
					<th class="align-middle">PAIS</th>
					<th class="align-middle">MONTO ORIG</th>
					<th class="align-middle">MONTO</th>
					<th class="align-middle">COD. RAZ&Oacute;N</th>
					<th class="align-middle">ESTADO </th>
					<th class="align-middle">DESCRIPCION</th>
					<th class="align-middle">Nº INCIDENTE</th>
					<th class="align-middle">FECHA GESTION</th>
					<th class="align-middle">USUARIO</th>
					<th class="align-middle">PATPASS</th>
				</tr>

				<s:iterator value="listaTrx" status="checkPos">
					<tr class="text-center">
						<s:if test="%{isCheck == true}">
								<td class="align-middle">
									<div class="custom-control custom-checkbox">
										<input id='el<s:property value="%{#checkPos.index}"/>' type="checkbox" class="checkbox1 custom-control-input" name="check[]" value="<s:property value="sid" />" checked>
										<label for='el<s:property value="%{#checkPos.index}"/>' class="custom-control-label"></label>
									</div>
								</td>
						</s:if>
						<s:else>
							<td class="align-middle">
								<div class="custom-control custom-checkbox">
									<input id='el<s:property value="%{#checkPos.index}"/>' type="checkbox" class="checkbox1 custom-control-input" name="check[]" value="<s:property value="sid" />">
									<label for='el<s:property value="%{#checkPos.index}"/>' class="custom-control-label"></label>
								</div>
							</td>
						</s:else>
					
						<td class="align-middle" style="display: none;"><label id="sid"><s:property value="sid" /></label></td>
						<td class="align-middle"><s:property value="numeroTarjeta" /></td>
						<td class="align-middle" style="text-align: center;"><s:property value="fechaTransac" /></td>
						<td class="align-middle"><s:property value="comercio" /></td>
						<td class="align-middle" style="text-align: center;"><s:property value="pais" /></td>
						<td class="align-middle" style="text-align: center;"><s:property value="montoTransac" /></td>
						<td class="align-middle" style="text-align: center;"><s:property value="montoFacturacion" /></td>
						<td class="align-middle" style="text-align: center;"><s:property value="codRazon" /></td>
						<td class="align-middle"><s:property value="estadoTrx" /></td>
						<td class="align-middle"><s:property value="glosaGeneral" /></td>
						<td class="align-middle"><s:property value="numIncidente" /></td>
						<td class="align-middle"><s:property value="fechaGestion"/></td>
						<td class="align-middle"><s:property value="idUsuario" /></td>
<s:if test='%{patpass == " " or patpass == "-"}'>
								<td class="align-middle" style="width: 36.5px;text-align: center;">No</td>
							</s:if>
    						<s:else>
    						<td class="align-middle" style="width: 36.5px;text-align: center;"> Si</td>
    						</s:else>

					</tr>
				</s:iterator>
			</table>
		</div>
	</div>


	<div id="botonesPaginacion"
		style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial, Helvetica, sans-serif; font-size: 8pt;">
		<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		<s:property value="%{#request.numPagina}" />
		De
		<s:property value="%{#request.ultimaPagina}" />
		P&aacute;gina(s)&nbsp;&nbsp;&nbsp; Ir a pagina <input type="text"
			id="paginaIr" style="width:50px;font-size: 8pt;" class="form-control form-control-sm d-inline py-0 align-middle" /> <input type="button"
			value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt;" />
	</div>


</body>
</html>