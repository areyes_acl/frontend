

 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 

<html>

   <head>
       
       <title>Gestiones Por Cuenta</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       
       	<script>
       	
		$("#razones").change(function() {
			$("#causal").val("DESCRIPCION DE LA RAZON SELECCIONADA");
		});
		
		</script>
		
   </head>

<body>

	  <table width="100%" class="bordeSimple">
       		<tr class="bgceleste"><td class="EncabezadoTitulo">Generación De Cargos/Abonos</td></tr>
      </table>
      
      <br style="line-height:5px"/>
      
      <table width="100%" class="bordeSimple">
               
                <tr class="bgceleste">
               			<td class="CuerpoNormal">&nbsp;Cargo/Abono</td>
              			<td class="CuerpoNormal">
              				<input type="radio" name="group1" value="C" checked>&nbsp;Cargo&nbsp;
							<input type="radio" name="group1" value="A">Abono&nbsp;
              			</td>
       			</tr>
       			
               <tr class="bgceleste">
               			<td class="CuerpoNormal">&nbsp;Codigo Razon :</td>
              			<td class="CuerpoNormal">
              				<select name="razones" id="razones">
			    		    	<option value="0"> 6305 </option>
			    				<option value="1"> 6304 </option>
			    				<option value="2"> 6050 </option>
			    			</select>
              			</td>
       			</tr>
       			
       			<tr class="bgceleste">
               			<td class="CuerpoNormal">&nbsp;Causal :</td>
              			<td class="CuerpoNormal">
              				<input type="text" id="causal" name="causal" size="50" maxlength="50" disabled/>
              			</td>
       			</tr>

				<tr class="bgceleste">
               			<td class="CuerpoNormal">&nbsp;Referencia :</td>
              			<td class="CuerpoNormal">
              				<input type="text" id="referencia" name="referencia" size="50" maxlength="50"/>
              			</td>
       			</tr>
       			
       			<tr class="bgceleste">
       					<td class="CuerpoNormal">&nbsp;Moneda :</td>
              			<td class="CuerpoNormal">
              				<select name="moneda" id="moneda">
			    		    	<option value="0"> US$ </option>
			    				<option value="1"> USD </option>
			    			</select>
              			</td>
              	</tr>
       			
       			<tr class="bgceleste">
       					<td class="CuerpoNormal">&nbsp;Monto :</td>
              			<td class="CuerpoNormal">
              				<input type="text" id="monto" name="monto" size="10" maxlength="10"/>
              			</td>
              	</tr>
       			
       			<tr class="bgceleste">
               			<td class="CuerpoNormal">&nbsp;Fecha Tope :</td>
              			<td class="CuerpoNormal">
              				<input type="text" id="fechatope" name="fechatope" size="10" maxlength="10"/>
              			</td>
              			
              			
       			</tr>
       		
      </table>
      
      <br style="line-height:5px"/>
      
      <table  width="100%" class="bordeSimple">
       	       		<tr class="bgceleste">
       				<td class="CuerpoNormal" align="center">
       					<input type="button"  value="Guardar" onclick="$('#popup').css({display: 'none'});">
       				    <input type="button"  value="Cancelar" onclick="$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});">
       				</td>
       			</tr>
      </table>
      
</body>
</html>
