
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

   <head>
       
       <title>Objeciones</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       <script type="text/javascript" src="js/utils.js"></script>
       <script type="text/javascript" src="js/maxlength.js"></script>
       
       <script>
       	$(document).ready(function(){
       		
       		$.ajax({
       			url:'tipoObjecionesAction',
       			type:'POST'
       		}).done(function(data){
       			data.listaTipoObjecion;
       			var datos = (data.listaTipoObjecion).split("~");
       			
       			var strCombo = "";
       			
       			for(var i=1;i<datos.length-1;i++)
   				{			
       				var fila = datos[i].split("|");
       				
       				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
   				}
       			
       			$("#objeciones").append(strCombo);
       		});
       		
       		$("#guardarObj").click(function(){
       			var fecha = $("#fechaTrx").val().replace(/\//g,'').replace(/\:/g,'').replace(/\ /g,'');
       			$("#fechaTrx").val(fecha);
       			//alert($("#formObjecion").serialize())
       			
       			// valida campo requerido
       			  
       			 
       		   // VALIDA CAMPOS OBLIGATORIOS 
       		   if( !camposObligatoriosValidos()){
       		    	return;
       		   }
       			  
       			
       			$.ajax({
	       			url:'ejecutaObjecionesAction',
	       			type:'POST',
	       			data:$("#formObjecion").serialize()
	       		}).done(function(data){
	       			data.resEjecucion;
	       			alert(data.ejecutaObjecion.replace('esttrx:1|',"" ));
	       			if(data.ejecutaObjecion == 'Objeción realizada')
       				{
	       				buscarTransaccionPresentacion();
	       				//$('#popup').css({display: 'none'});$('#popUpContenido').empty();
	       				$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});
       				}
	       		});
    		});
       	});
       	
       	
       	function camposObligatoriosValidos(){
       	
       		 if($("#glosa").val() == ''){
       			alert("Advertencia: Debe ingresar un texto en el campo Glosa.");
       			  	return false;
          		}
       	
       		if($("#numIncidente").val() == ''){
       			alert("Advertencia: Debe ingresar un número de incidente.");
       			  	return false;
          	}
          	
          	return true;
       	}
       	
       	
       	
       	
       	
       	//onclick="$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});"
       	$("#cancelarObjetar").click(function(){
        	
        	$('#popup').css({display: 'none'});
        	$('#popUpContenido').empty().css({display:'none'});
        	
        });
       
       </script>
   </head>
	
	<body>
		<form id="formObjecion">
			<input type="hidden" name="sid" value="<%=request.getParameter("sid")%>" />
			<input type="hidden" name="numeroTarjeta" value="<%=request.getParameter("numeroTarjeta")%>" />
			<input type="hidden" name="fechaTrx" id="fechaTrx" value="<%=request.getParameter("fechaTrx")%>" />
			<input type="hidden" name="codigoAutorizacion" value="<%=request.getParameter("codigoAutorizacion")%>" />
			
			<input type="hidden" name="sidusuario" value="<%=request.getSession().getAttribute("sidusuario")%>" />
			
			<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
				Objetar Transacción
			</div>
			<hr>
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
				<!-- contenedorInput -->
				<table width="100%" class="table table-sm">
			    	<tr>
			        	<td class="align-middle text-left w-25">Tipo Objeción:</td>
		        		<td id="comboObjeciones">
		        			<select class="align-middle custom-select" name="objeciones" id="objeciones">
		        			</select>
		        		</td>
		 			</tr>
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/> -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table width="100%" class="table table-sm"> 			
					<tr>
			        	<td class="CuerpoNormal">&nbsp;(*) GLOSA: </td>
			 		</tr>
			 		<tr>
			 			<td>
			 				<textarea id="glosa" rows="5" cols="58" name="glosa" style="width:99%" data-maxsize="1999" class="align-middle form-control form-control-sm"></textarea>
			 			</td>
			 		</tr>
			 			
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
				<!-- contenedorInput -->
				<table width="100%" class=" table table-sm">
			    	<tr>
			    		<!-- width="20%" -->
			        	<td  class="align-middle text-left w-25">(*) Nro Incidente:</td>
		        		<td class="align-middle">
		        			<!-- size="50" -->
		        			<input maxlength="50" name="numIncidente" id="numIncidente" class="alphanumeric form-control form-control-sm"/>
		        		</td>
		 			</tr>
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
			<!-- contenedorInput -->
				<table  width="100%" class="table table-sm table-borderless">
			       		<tr>
		 				<td align="center">
		 				    <input id="guardarObj" type="button"  value="Guardar" class="btn btn-primary" />
							<input type="button"  value="Cancelar" name="cancelarObjetar" id="cancelarObjetar" class="btn btn-primary" />
		 				</td>
		 			</tr>
				</table>
			</div>
	      </form>
	</body>
</html>
