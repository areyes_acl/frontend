
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

   <head>
       
       <title>Confirmacion Recepcion De Vales</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       
       <script>
           
       		$(document).ready(function(){
       		
       			$("#guardar").click(function(){
       				
       				    if ($("#glosa").val().length == 0){
       							alert("Debe ingresar una glosa para realizar la operacion...");
       							$("#glosa").focus();
       							return;
       				    }

       				    //*****************************************************************//
       					//* Inicio grabacion de Confirmacion Recepcion De Vales           *//
       					//*****************************************************************//
       					var parametros = "xkey=" + "<%=request.getParameter("vxkey")%>";
     	    			parametros += "&sidtransaccion=" + "<%=request.getParameter("sidtransaccion")%>";
     					parametros += "&sidusuario=" + "<%=request.getParameter("sidusuario")%>";
     					parametros += "&glosadescriptiva=" + $("#glosa").val();
  
     					//alert(parametros);
       						
       					$.ajax({
       						url: "grabarConfirmacionRecepcionValeAction",
       						type: "POST",
       						data: parametros,
       						dataType: "json",
       						error: function(XMLHttpRequest, textStatus, errorThrown){
       							alert('Error ' + textStatus);
       							alert(errorThrown);
       							alert(XMLHttpRequest.responseText);
       						},
       		    			success: function(data){
       		    				
       		    				mostrarMensajeGrabacion(data.mensajegrabacion);
       		    				recargarConsulta();
       		    				recargarConsultaBuscar();
       		    				
       						}
       					});
       					
       					//************************************************//
       					//* Fin sección grabacion de cargos y abaonos    *//
       					//************************************************//
       					$('#popup').css({display: 'none'});
       					$('#popUpContenido').empty().css({display:'none'});
       					
       			});
   
       		});
       	
       </script>
       
       <script>
       
       			function mostrarMensajeGrabacion(mensajeGrabacion){
					
					var lstmensaje = mensajeGrabacion.split("~");
					var esttrx = lstmensaje[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var mensaje = lstmensaje[1].split("|");
					alert(mensaje[1]);
					
				}
       	
       	</script>
       
   </head>
	
	<body>
	
		<div class="titulo">
	  		    CONFIRMACION DE RECEPCION DE VALES
	  	</div>
	  
		<br style="line-height:5px"/>
			
		<div>
				<table width="100%" class="contenedorInput"> 			
					<tr>
			        	<td class="CuerpoNormal">&nbsp;GLOSA : </td>
			 		</tr>
			 		<tr>
			 			<td>
			 				<textarea rows="5" cols="58" name="glosa" id="glosa"></textarea>
			 			</td>
			 		</tr>		
				</table>
		</div>
			
		<br style="line-height:5px"/>
			
		<div>
				<table  width="100%" class="contenedorInput">
			       		<tr>
		 				<td align="center">
		 				    <input id="guardar" type="button"  value="Guardar" />
							<input type="button"  value="Cancelar" onclick="$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});">
		 				</td>
		 			</tr>
				</table>
			</div>

	</body>
	
</html>
