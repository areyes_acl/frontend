<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<title>Gestion De Transacciones</title>
<script src="js/jquery-1.10.0.min.js" type="text/javascript">
	
</script>
<script src="js/jquery.Rut.js" type="text/javascript"></script>
<script type="text/javascript" src="js/utils.js"></script>
<style type="text/css">
#tablaRes tbody tr th {
	font-weight: bold;
	text-align: left;
}
</style>
</head>

<body>
	<input type="hidden" value="100" name="cantReg" id="cantReg" />
	<div id="popup" style="display: none;"></div>

	<div id="popUpContenido" class="content-popup" style="display: none;"></div>

	<!-- <div class="titulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		DETALLE DE PROCESO: <b><s:property value="%{jobWork.proceso}" /></b>
	</div>
	<hr>
	<!-- <br /> -->
	
	<div>
		<!-- class="tablaContenido" -->
		<table width="100%" class="table table-sm small mx-auto" id="tablaRes">
			<tbody>
				<tr>
					<th>Nombre del Proceso</th>
					<td><s:property value="%{jobWork.proceso}" /></td>
				</tr>
				<tr>
					<th>Conexi&oacute;n Origen</th>
					<td><s:property value="%{jobWork.origenTipo.tipo}" /></td>
				</tr>
				<tr>
					<th>Conexi&oacute;n Origen: Path</th>
					<td><s:property value="%{jobWork.origenPath}" /></td>
				</tr>
				<s:if test="%{jobWork.origenTipo.tipo not eq 'LOCAL'}">
				<tr>
						<th>Conexi&oacute;n Origen: IP / Puerto</th>
						<td><s:property value="%{jobWork.origenIP}" /> / <s:property value="%{jobWork.origenPuerto}" /></td>
					</tr>
					<tr >
						<th>Conexi&oacute;n Origen: Usuario / Contrase&ntilde;a</th>
						<td><s:property value="%{jobWork.origenUsuario}" /> / <s:property value="%{jobWork.origenPass}" /></td>
					</tr>
				</s:if>
				<tr>
					<th>Conexi&oacute;n Destino</th>
					<td><s:property value="%{jobWork.destinoTipo.tipo}" /></td>
				</tr>
				<tr>
					<th>Conexi&oacute;n Destino: Path</th>
					<td><s:property value="%{jobWork.destinoPath}" /></td>
				</tr>
				<s:if test="%{jobWork.destinoTipo.tipo not eq 'LOCAL'}">
				<tr >
					<th>Conexi&oacute;n Destino: IP / Puerto</th>
						<td><s:property value="%{jobWork.destinoIP}" /> / <s:property value="%{jobWork.destinoPuerto}" /></td>
					</tr>
					<tr >
						<th>Conexi&oacute;n Destino: Usuario / Contrase&ntilde;a</th>
						<td><s:property value="%{jobWork.destinoUsuario}" /> / <s:property value="%{jobWork.destinoPass}" /></td>
					</tr>
				</s:if>
				<tr>
					<th>Archivo</th>
					<td><s:property value="%{jobWork.ArchivoReal}" /></td>
				</tr>
				<s:if test="%{jobWork.control == 1}">
				<tr>
					<th>Extensi&oacute;n Archivo Control</th>
					<td><s:property value="%{jobWork.controlExt}" /></td>
				</tr>
				</s:if>
				<s:iterator value="jobWork.validaciones" status="ind">
					<tr>
						<th><s:if test="%{#ind.index == 0}">Validaciones</s:if></th>
						<td><s:property value="validacionId.detalle" /><br>
						    Origen: <s:property value="origen" /> | Destino: <s:property value="destino" />
						</td>
					</tr>
				 </s:iterator>
				<tr>
					<th>Validaci&oacute;n de variabilidad</th>
					<td><s:property value="%{jobWork.VariabilidadShow}" /><s:if test="%{jobWork.variabilidad == 1}">, <s:property value="%{jobWork.variabilidadPorciento}" />%, <s:property value="%{jobWork.variabilidadLineas}" /></s:if></td>
				</tr>
				<tr>
					<th>Notificaci&oacute;n > Fallos de validaci&oacute;n</th>
					<td><s:property value="%{jobWork.Notificacion1Show}" /></td>
				</tr>
				<s:if test="%{jobWork.notificacion1 == 1}">
				<tr>
					<th>Cadena de correos > Fallos de validaci&oacute;n</th>
					<td><s:property value="%{jobWork.notificacion1Cadena}" /></td>
				</tr>
				</s:if>
				<tr>
					<th>Horario de ejecuci&oacute;n</th>
					<td><s:property value="%{jobWork.EjecucionShow}" /></td>
				</tr>
				<tr>
					<th>Nro. Reintentos / Minutos entre reintentos</th>
					<td><s:property value="%{jobWork.reintentos}" /> / <s:property value="%{jobWork.reintentosMin}" /></td>
				</tr>
				<tr>
					<th>Activado</th>
					<td><s:property value="%{jobWork.Activado}" /></td>
				</tr>
			</tbody>
		</table>
		<!-- <br />  -->
		<!-- color: rgb(0, 0, 0);  -->
		<div id="botonesPaginacion" class="text-center"
			style="font-style: normal; font-family: Arial, Helvetica, sans-serif; font-size: 8pt;">
			<a href="javascript:envio('gestionTDIAction', 10, 33);"><button class="btn btn-primary">Volver</button></a>
		</div>
	</div>
</body>
</html>