<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<title>Gestion De Transacciones</title>
<link rel="stylesheet" type="text/css" href="css/tdi.css" />
</head>

<body>
	<input type="hidden" value="100" name="cantReg" id="cantReg" />
	<div id="popup" style="display: none;"></div>

	<div id="popUpContenido" class="content-popup" style="display: none;"></div>

	<!-- <div class="titulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		INTERFACES DE TRANSPORTE DIN&Aacute;MICO
	</div>
	<hr>
	<!-- <br />  -->
	<!-- style="border: 1px" -->
	<div>
		<div>
			<!-- class="contenedorInput" -->
			<table class="table table-sm table-borderless" style="width: 100%;">
				<tr>
					<td class="align-middle" style="width: 100%;">
						<!-- style="width: 70%;" -->
						<input type="text" class="nombreArchivo form-control d-inline col-10"  placeholder="Proceso o Archivo" />
						<!-- <span style="float:right"> -->
						<!-- style="margin-right:10px" -->
							<input type="button" id="buscarTrx" name="buscar"
								value="Buscar" class="btn btn-primary btn-sm" /> 
							<a href="javascript:envio('crearTDIAction', 10, 33);">
								<button id="crearTrx" name="crear" value="Crear" class="btn btn-primary btn-sm">Crear</button>
							</a>
						<!-- </span>-->
						</td>
				</tr>

			</table>
		</div>
		
		<!-- <br /> -->
		
		<div class="cajaContenido">
		<div id="tablaResDiv">
		<div id="contenidoTabla">
		<s:if test="%{creado == 1}">
		<div class="alert alert-success font-weight-bold text-center">
			Se añadi&oacute; correctamente el Job
		</div>
		</s:if>
		<s:elseif test="%{creado == 3}">
		<div class="alert alert-success font-weight-bold text-center">
			Se actualiz&oacute; correctamente el Job
		</div>
		</s:elseif>
			<%@include file="indexGestionTDIResult.jsp" %>
	    
		</div>
			<div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial,Helvetica,sans-serif; font-size: 8pt;">
		   	<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		   	<span class="paginaActual"><s:property value="%{#request.page}" /></span> de <span class="paginaFinal"><s:property value="%{#request.ultimaPagina}" /></span> p&aacute;gina(s)&nbsp;&nbsp;&nbsp;
		   	Ir a p&aacute;gina <input type="text" id="paginaIr" style="width:50px;font-size: 8pt;" class="form-control form-control-sm d-inline py-0 align-middle" value=""> <input type="button" value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt;">
		</div>
		</div>
	</div>
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript">
$(function(){
	botonEliminar();
	function botonEliminar(){
			$('.btn-delete').click(function(){
				var dataid = $(this).attr('data-id');
				var dataproceso = $(this).attr('data-proceso');
				var opcion = confirm("Confirme si desea eliminar el JOB "+dataproceso);
			    if (opcion == true) {
					$.get("eliminarTDIAction","idJobWork="+dataid, function(data){
						if(!isNaN(data)){
							if(parseInt(data) == 8){
								alert("Job eliminado correctamente");
								$('.fullrow'+dataid).remove();
							}else{
								alert("Problema al eliminar el Job");
							}
						}else{
							alert("Problema al eliminar el Job");
						}
					});
				}
			});
	}
	
	$("#buscarTrx").click(function(){
		changePage(1, false);
	});
	
	$("#siguiente").click(function(){
		var pagina = parseInt($(".paginaActual").html());
		var pagfinal = parseInt($(".paginaFinal").html());
		if(pagina < pagfinal){
			changePage(pagina+1, true);
			$("#paginaIr").val("");
		}
	});
	
	$("#anterior").click(function(){
		var pagina = parseInt($(".paginaActual").html());
		var pagfinal = parseInt($(".paginaFinal").html());
		if(pagina > 1){
			changePage(pagina-1, true);
			$("#paginaIr").val("");
		}
	});
	
	$("#ir").click(function(){
		if($("#paginaIr").val().trim().length !== 0){
			var paginIr = parseInt($("#paginaIr").val());
			var pagfinal = parseInt($(".paginaFinal").html());
			if(paginIr >= 1 && paginIr <= pagfinal){
				changePage(paginIr, true);
			}else{
				alert("La p\u00E1gina no existe");
				$("#paginaIr").val("");
			}
		}
		
	});
	
	botonesPaginacion();
	
	function botonesPaginacion(){
		$(".paginaActual").html($(".resultPage").html());
		$(".paginaFinal").html($(".resultPageFinal").html());
		if(parseInt($(".resultadoAction").html()) != 0 || (parseInt($(".paginaActual").html()) == 1 && parseInt($(".paginaFinal").html()) == 1)){
			$("#botonesPaginacion").hide();
		}else{
			$("#botonesPaginacion").show();
		}
	}
	
	function changePage(page, sesion){
		var archivo = $(".nombreArchivo").val();
		
		if(sesion){
			archivo = $(".resultNombreArchivo").html();
		}
		
		$.get("gestionTDISearchResultAction", "page="+page+"&searchArchivo="+archivo, function(data){
				$("#contenidoTabla").html(data);
				botonesPaginacion();
				botonEliminar();
		});
	}
});
</script>
</body>
</html>