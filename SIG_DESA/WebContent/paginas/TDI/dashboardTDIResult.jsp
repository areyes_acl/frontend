<style>
	.tooltip2 {
    position: relative;
    display: inline-block;
	}
	
	.tooltip2 .tooltiptext {
	    visibility: hidden;
	    width: 400px;
	    background-color: black;opacity:0.8;
	    color: #fff;
	    text-align: center;
	    border-radius: 6px;
	    padding: 5px 0;
	    position: absolute;
	    z-index: 1;
	    top: -5px;
	    right: 130%;
	}
	
	.tooltip2 .tooltiptext::after {
	    content: "";
	    position: absolute;
	    top: 50%;
	    left: 100%;
	    margin-top: -5px;
	    border-width: 5px;
	    border-style: solid;
	    border-color: transparent transparent transparent black;
	}
	.tooltip2:hover .tooltiptext {
	    visibility: visible;
	}
</style>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
            <span style="display:none" class="resultPage"><s:property value="%{#request.page}" /></span>
            <span style="display:none" class="resultPageFinal"><s:property value="%{#request.ultimaPagina}" /></span>
            <span style="display:none" class="resultfechaInicio"><s:property value="%{#request.fechaInicio}" /></span>
            <span style="display:none" class="resultfechaTermino"><s:property value="%{#request.fechaTermino}" /></span>
            <span style="display:none" class="resultEstado"><s:property value="%{#request.estado}" /></span>
            <span style="display:none" class="resultadoAction"><s:property value="%{#request.resultadoAction}" /></span>
            <s:if test="%{resultadoAction == 0}">
			
			<!-- class="tablaContenido" -->
			<table width="100%" class="table table-sm small mx-auto" id="tablaRes">
			<thead>
			<tr class="text-center" style="text-transform: uppercase; font-weight: bold;">
			            <th class="align-middle">#</th>
						<th class="align-middle">Proceso</th>
						<th class="align-middle">Inicio</th>
						<th class="align-middle">Termino</th>
						<th class="align-middle">Ejecucion</th>
						<th class="align-middle">Archivo</th>
						<th class="align-middle">Estado</th>
					</tr></thead>
				<tbody>
				<s:iterator value="executionLogs" status="ind">
				   <tr class="text-center">
				   		<td class="align-middle text-right">
				   			<s:if test="%{estado != 'ERROR'}">
				   				<label><s:property value="%{#request.valorInicial + (#ind.index+1)}"/></label>
				   				<input id='check<s:property value="%{#request.valorInicial + (#ind.index+1)}"/>' type="checkbox" class="d-none custom-control-input elementoReprocesar elementoReprocesar<s:property value="sid" /> elementoEstado<s:property value="estado" />" style="float:right;" data-id="<s:property value="sid" />" >
					   		</s:if>
					   		<s:else>
					   			<div class="custom-control custom-checkbox">
					   				<!-- style="float:right; <s:if test="%{estado != 'ERROR'}">visibility:hidden</s:if> -->
					   				<input id='check<s:property value="%{#request.valorInicial + (#ind.index+1)}"/>' type="checkbox" class="custom-control-input elementoReprocesar elementoReprocesar<s:property value="sid" /> elementoEstado<s:property value="estado" />" style="float:right;" data-id="<s:property value="sid" />" >
					   				<label for='check<s:property value="%{#request.valorInicial + (#ind.index+1)}"/>' class="custom-control-label"><s:property value="%{#request.valorInicial + (#ind.index+1)}"/></label>
					   			</div> 
					   		</s:else>
			   			</td>
						<td class="align-middle"><s:property value="proceso" /></td>
						<td class="align-middle center"><s:property value="HoraInicioShow" /></td>
						<td class="align-middle center elementoTdhorafin<s:property value="sid" />"><s:property value="HoraTerminoShow" /></td>
						<td class="align-middle elementoTdEjec<s:property value="sid" />"><s:property value="DeltaEjecucionShow" /></td>
						<td class="align-middle elementoTdArchivo<s:property value="sid" />">
						<s:iterator value="ArchivoReal" status="ind" var="copiado">
						<s:property value="copiado" /><br>
						</s:iterator>
						</td>
						
						<td class="align-middle elementoTdEstado<s:property value="sid" /> <s:if test="%{estado == 'ERROR'}">error</s:if><s:elseif test="%{estado == 'ENTREGADO'}">info</s:elseif><s:elseif test="%{estado == 'REPROCESAR'}">warning</s:elseif><s:else>extra</s:else>">
<s:if test="%{estado == 'ERROR' || estado == 'REPROCESAR'}">
<div class="tooltip2">
<s:property value="estado" />
  <span class="tooltiptext"><s:property value="descripcionError" /></span>
</div> 
</s:if>
<s:elseif test="%{estado == 'PROCESANDO'}"><div class="loading" style="display: inline;"></div></s:elseif>
<s:else><s:property value="estado" /></s:else>
</td>
				  </tr>
				</s:iterator>
				</tbody>
			</table>
			</s:if>
			<s:elseif test="%{resultadoAction == 2}">
			<div class="alert alert-info">
			No se encontraron resultados
			</div>
            </s:elseif>
            <s:elseif test="%{resultadoAction == 3}">
			<div class="alert alert-info">
			La fecha de inicio no puede ser mayor a la de fin
			</div>
            </s:elseif>
			<s:else>
			<div class="alert alert-info">
			Favor, verificar que el formato de las fechas de inicio y fin es correcto
			</div>
           </s:else>