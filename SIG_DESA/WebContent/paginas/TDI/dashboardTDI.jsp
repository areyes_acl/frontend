<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<title>Gestion De Transacciones</title>
<link rel="stylesheet" type="text/css" href="css/tdi.css" />
</head>

<body>
	<input type="hidden" value="100" name="cantReg" id="cantReg" />
	<div id="popup" style="display: none;">
	</div>

	<div id="popUpContenido" class="content-popup" style="display: none;">
	</div>

	<!-- <div class="titulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		DASHBOARD
	</div>
	<hr>
<!-- <br />  -->
		
		<div>
		<!-- class="contenedorInput" -->
		<table class="table table-sm" id="example" style="width: 100%;">
				<tr class="text-right">
				<td class="align-middle">Fecha Inicio:</td>
					<!-- size="10" placeholder="DD/MM/AAAA" -->
			        <td class="align-middle text-center">
			        	<input type="text" maxlength="10" value="" name="fechaInicio" id="fechaInicio" onKeypress="ingresoFecha();" class="campoObligatorio fecha form-control form-control-sm mx-auto text-center col-10 datepicker" placeholder="DD/MM/AAAA" />
			        </td>
			        <td class="align-middle text-center">Fecha Final:</td>
			        <!-- size="10" placeholder="DD/MM/AAAA" -->
			        <td class="align-middle text-center">
			        	<input type="text" maxlength="10" value="" name="fechaTermino" id="fechaTermino" onKeypress="ingresoFecha();" class="campoObligatorio fecha form-control form-control-sm mx-auto text-center col-10 datepicker" placeholder="DD/MM/AAAA" />
					</td>
			        <td class="align-middle">ESTADO:</td>
					<td class="align-middle"><select id="estado" class="custom-select">
							<option value="-1">TODOS</option>
							<s:iterator value="logEstados">
									<option value="<s:property value="sid"/>"><s:property value="estado" /></option>
							</s:iterator>
					</select></td>
					<td class="align-middle text-center"><input type="button" id="buscarLog" name="buscar" value="Buscar" class="btn btn-primary"></td>
					
				</tr>
			</table>
		</div>
		
		<!--  <br /> -->
	<div class="cajaContenido mb-4">
		<div id="tablaResDiv" class="mb-2">
		
			<div id="contenidoTabla">
				<%@include file="dashboardTDIResult.jsp" %>
			</div>
		</div>
		
		<div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial,Helvetica,sans-serif; font-size: 8pt;">
		   		<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		   		<span class="paginaActual"><s:property value="%{#request.page}" /></span> de <span class="paginaFinal"><s:property value="%{#request.ultimaPagina}" /></span> p&aacute;gina(s)&nbsp;&nbsp;&nbsp;
		   		Ir a p&aacute;gina <input type="text" id="paginaIr" style="width:50px;font-size: 8pt;" value="" class="form-control form-control-sm d-inline py-0 align-middle"> <input type="button" value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt;">
		</div>
	</div>
	
	<div>
		<table class="table table-sm table-borderless">
			<tr class="text-center">
				<td class="align-middle">
					<input type="button" id="exportarLog" name="exportar"value="Exportar" class="btn btn-primary mr-2">
					<input type="button" id="reprocesarLog" name="reprocesar" value="Reprocesar" disabled="disabled" class="btn btn-primary">
				</td>
			</tr>
		</table>
	</div>
<script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/utils.js"></script>	
<script type="text/javascript" src="js/tdi.js"></script>	
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>

<script>
	$(".datepicker").datepicker();
</script>
<script>
$(function(){
	$("#exportarLog") .click(
			function() {
				var fechaInicio = $(".resultfechaInicio").html();
				var fechaTermino = $(".resultfechaTermino").html();
				var estado = $(".resultEstado").html();
				
				location.href = "./dashboardTDIExportarAction?&fechaInicio="+fechaInicio+"&fechaTermino="+fechaTermino+"&estado="+estado;
			});
})
</script>

</body>
</html>