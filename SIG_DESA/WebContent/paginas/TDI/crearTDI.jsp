<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<title>Gestion De Transacciones</title>

<style type="text/css">
#tablaRes tbody tr th {
	font-weight: bold;
	text-align: left;
	width: 40%;
}

#tablaRes tbody tr td input, #tablaRes tbody tr td select {
	width: 100%;
}


.tooltip2 {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip2 .tooltiptext2 {
    visibility: hidden;
    width: 250px;
    background-color: black;opacity:0.8;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 170%;
    left: -470%;
    margin-left: -60px;
}

.tooltip2 .tooltiptext2::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: black transparent transparent transparent;
}

.tooltip2:hover .tooltiptext2 {
    visibility: visible;
}
.tooltip3 {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip3 .tooltiptext {
    visibility: hidden;
    width: 260px;
    background-color: black;opacity:0.8;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    top: -110px;
    left: 160%;
}

.tooltip3 .tooltiptext::after {
    content: "";
    position: absolute;
    top: 50%;
    right: 100%;
    margin-top: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: transparent black transparent transparent;
}
.tooltip3:hover .tooltiptext {
    visibility: visible;
}
</style>
</head>

<body>
	<input type="hidden" value="100" name="cantReg" id="cantReg" />
	<div id="popup" style="display: none;"></div>

	<div id="popUpContenido" class="content-popup" style="display: none;"></div>

	
	<!-- <div class="titulo"> -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		<s:if test="%{idJobWork > 0}">EDITAR</s:if>
		<s:else>CREAR</s:else> 
		INTERFAZ DE TRANSPORTE DINÁMICO
	</div>
	<hr>
	<!-- <br />  -->
			<!-- class="tablaContenido" -->
			<table width="100%" class="table table-sm small" id="tablaRes">
				<tbody>
					<tr>
						<th class="align-middle">Nombre del Proceso</th>
						<td class="align-middle"><input type="text" class="nombreProceso form-control" value="<s:if test="%{idJobWork > 0}"><s:property value="%{jobWork.proceso}" /></s:if>" maxlength="49" /></td>
					</tr>
					<tr>
						<th class="align-middle">Conexi&oacute;n Origen</th>
						<td class="align-middle"><select class="origenTipo custom-select" id="estado">
								<option value="">Seleccione...</option>
								<s:iterator value="tiposConexion">
									<option value="<s:property value="sid"/>" <s:if test="%{jobWork.origenTipo.sid == sid}">selected="selected"</s:if> ><s:property value="tipo" /></option>
								</s:iterator>
						</select></td>
					</tr>
					<tr>
						<th class="align-middle">Conexi&oacute;n Origen: Path</th>
						<td class="align-middle"><input type="text" placeholder="Ex: /var/www" class="origenPath form-control" value="<s:property value="%{jobWork.origenPath}" />" maxlength="99" /></td>
					</tr>
					<tr class="origen-data" <s:if test="%{(idJobWork == 0) || (idJobWork > 0 && jobWork.origenTipo.tipo eq 'LOCAL')}">style="display:none"</s:if> >
						<th class="align-middle">Conexi&oacute;n Origen: IP / Puerto</th>
						<td class="align-middle"><input type="text" class="origenIP form-control d-inline" placeholder="Ex: 192.168.32.43" style="width: 48%" value="<s:property value="%{jobWork.origenIP}" />" maxlength="19"/> / <input type="text" style="width: 47%" class="origenPuerto form-control d-inline" placeholder="Ex: 22"  value="<s:property value="%{jobWork.origenPuerto}" />" /></td>
					</tr>
					<tr class="origen-data" <s:if test="%{(idJobWork == 0) || (idJobWork > 0 && jobWork.origenTipo.tipo eq 'LOCAL')}">style="display:none"</s:if> >
						<th class="align-middle">Conexi&oacute;n Origen: Usuario / Contrase&ntilde;a</th>
						<td class="align-middle"><input type="text" class="origenUsuario form-control d-inline" style="width: 48%" value="<s:property value="%{jobWork.origenUsuario}" />" maxlength="49"/> / <input type="text" class="origenPass form-control d-inline" style="width: 47%" value="<s:property value="%{jobWork.origenPass}" />" maxlength="49"/></td>
					</tr>
					<tr>
						<th class="align-middle">Conexi&oacute;n Destino</th>
						<td class="align-middle"><select class="destinoTipo custom-select">
								<option value="">Seleccione...</option>
								<s:iterator value="tiposConexion">
									<option value="<s:property value="sid"/>" <s:if test="%{jobWork.destinoTipo.sid == sid}">selected="selected"</s:if>><s:property value="tipo" /></option>
								</s:iterator>
						</select></td>
					</tr>
					<tr>
						<th class="align-middle">Conexi&oacute;n Destino: Path</th>
						<td class="align-middle"><input type="text" placeholder="Ex: /var/www" class="destinoPath form-control" value="<s:property value="%{jobWork.destinoPath}" />" maxlength="99"/></td>
					</tr>
					<tr class="destino-data" <s:if test="%{(idJobWork == 0) || (idJobWork > 0 && jobWork.destinoTipo.tipo eq 'LOCAL')}">style="display:none"</s:if>>
						<th class="align-middle">Conexi&oacute;n Destino: IP / Puerto</th>
						<td class="align-middle"><input type="text" class="destinoIP form-control d-inline" placeholder="Ex: 192.168.32.43" style="width: 48%" value="<s:property value="%{jobWork.destinoIP}" />" maxlength="19"/> / <input type="text" style="width: 47%" class="destinoPuerto form-control d-inline" placeholder="Ex: 22" value="<s:property value="%{jobWork.destinoPuerto}" />" /></td>
					</tr>
					<tr class="destino-data" <s:if test="%{(idJobWork == 0) || (idJobWork > 0 && jobWork.destinoTipo.tipo eq 'LOCAL')}">style="display:none"</s:if>>
						<th class="align-middle">Conexi&oacute;n Destino: Usuario / Contrase&ntilde;a</th>
						<td class="align-middle"><input type="text" class="destinoUsuario form-control d-inline"  style="width: 48%" value="<s:property value="%{jobWork.destinoUsuario}" />" maxlength="49"/> / <input type="text" class="destinoPass form-control d-inline"  style="width: 47%" value="<s:property value="%{jobWork.destinoPass}" />" maxlength="49"/></td>
					</tr>
					<tr>
						<th class="align-middle">Archivo / Extensi&oacute;n <span class="tooltip3">(?)<span class="tooltiptext">Tambi&eacute;n puede ocupar las cadenas siguientes:<br>[S]: Para designar secuencia entre 0..9<br>[YY]: A&ntilde;o actual en formato YY<br>[YYYY]: A&ntilde;o actual en formato YYYY<br>[MM]: Mes actual<br>[DD]: D&iacute;a actual<br>[DD-N]: D&iacute;a/s anterior/es al actual<br>[DD+N]: D&iacute;a/s posterior/es al actual<br><br> Ejemplos: FUS[MM][DD][YYYY]1, BIC[MM][DD-1][S], BIC[MM][DD+1][S]</span></span></th>
						<td class="align-middle"><input type="text" style="width: 48%" class="archivo form-control d-inline" value="<s:property value="%{jobWork.archivo}" />" maxlength="49"/> / <input type="text" style="width: 47%" class="archivo-ext form-control d-inline" value="<s:property value="%{jobWork.archivoExt}" />" maxlength="9" /></td>
					</tr>
					<tr>
						<th class="align-middle">Extensi&oacute;n Archivo Control</th>
						<td class="align-middle">
							<div class="custom-control custom-checkbox custom-control-inline align-middle mr-0">
								<input type="checkbox" id="extensionCheckbox" class="archivoDeControl custom-control-input" style="width: 3%;" value="<s:property value="%{jobWork.control}" />" <s:if test="%{jobWork.control == 1}">checked="checked"</s:if> >
								<label class="custom-control-label" for="extensionCheckbox"></label>
							</div>
							<span class="showArchivoDeControl custom-control-inline align-middle" <s:if test="%{jobWork.control == 0}">style="display:none"</s:if>><input maxlength="9" type="text"  class="control form-control" value="<s:property value="%{jobWork.controlExt}" />"/></span>
						</td>
					</tr>
					<tr>
						<th class="align-middle">Validaci&oacute;n <a title="Agregar" class="validacion-plus badge badge-danger" style="font-size: 15px;cursor:pointer; color:white">+</a></th>
						<td class="align-middle">
						</td>
					</tr>
					<s:iterator value="jobWork.validaciones" status="ind">
					<tr>
					<th class="align-middle"></th>
					<td class="align-middle">
					<div><img class="btn-delete btn-delete<s:property value="#ind.index" />" src="${pageContext.request.contextPath}/img/trash.png" style="width: 20px; padding: 2px; cursor: pointer;" title="Eliminar"> <b><s:property value="validacionId.detalle" /> <s:if test="%{validacionId.clasif == 7}"><span class="tooltip2">(?)<span class="tooltiptext2">Para el formato de la fecha puede ocupar (Y,M,D).<br> Ejemplos:YYMMDD, DDMMYYYY, MM/DD/YY</span></span></s:if></b></div><br>
					<select style="width:90%;display:none" class="custom-select tipoValidacion tipoValidacion<s:property value="#ind.index" />" data-index="<s:property value="#ind.index" />">
					<option value="<s:property value="validacionId.sid"/>" data-num="<s:property value="#ind.index" />" data-type="<s:property value="validacionId.clasif"/>" data-valid="<s:property value="validacionId.validaOD"/>" selected="selected"><s:property value="validacionId.detalle" /></option>
					</select>
					<s:if test="%{validacionId.clasif == 1 || validacionId.clasif == 3}">
					 <span class="datosOD<s:property value="#ind.index" />" ><span style="float:left;display:inline;width:50%">Origen: <input placeholder="línea" class="olínea form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="OrigenEdit[0]"/>">;X;X<input placeholder="posición" class="oposicion" value="X" data-visible="0" style="width: 25%; display:none"><input value="X" data-visible="0" placeholder="largo"  class="olargo" style="width: 25%;display:none"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="dlínea form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[0]" />"><input placeholder="posición" class="dposicion form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[1]" />"><input placeholder="largo" class="dlargo form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[2]" />"></span></span>	
					</s:if>
			        <s:elseif test="%{validacionId.clasif == 2 || validacionId.clasif == 4}">
			        <span class="datosOD<s:property value="#ind.index" />" > <span style="float:left;display:inline;width:50%">Origen: <input placeholder="línea" class="olínea form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="OrigenEdit[0]" />"><input placeholder="posición" class="oposicion form-control form-control-sm d-inline "  style="width: 25%;" value="<s:property value="OrigenEdit[1]" />"><input placeholder="largo"  class="olargo form-control form-control-sm d-inline " style="width: 25%;" value="<s:property value="OrigenEdit[2]" />"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="dlínea form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[0]" />"><input placeholder="posición" class="dposicion form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[1]" />"><input placeholder="largo" class="dlargo form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[2]" />"></span></span>			
			        </s:elseif>
			        <s:elseif test="%{validacionId.clasif == 5}">
			        <span class="datosOD<s:property value="#ind.index" />" > <span style="float:left;display:inline;width:50%">Origen: <input placeholder="línea" class="olínea form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="OrigenEdit[0]" />">;X;X<input placeholder="posición" class="oposicion" value="X" data-visible="0" style="width: 25%; display:none"><input placeholder="largo" value="X" data-visible="0" class="olargo" style="width: 25%;display:none"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="dlínea" value="X" data-visible="0" style="width: 25%; display:none">X;<input placeholder="posición" class="dposicion form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[1]" />"><input placeholder="largo" class="dlargo form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[2]" />"></span></span>
			        </s:elseif>
			        <s:elseif test="%{validacionId.clasif == 6}">
			        <span class="datosOD<s:property value="#ind.index" />" > <span style="float:left;display:inline;width:50%">Origen: <input placeholder="línea" class="olínea form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="OrigenEdit[0]" />"><input placeholder="posición" class="oposicion form-control form-control-sm d-inline "  style="width: 25%;" value="<s:property value="OrigenEdit[1]" />"><input placeholder="largo"  class="olargo form-control form-control-sm d-inline " style="width: 25%;" value="<s:property value="OrigenEdit[2]" />"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="dlínea" value="X" data-visible="0" style="width: 25%; display:none">X;<input placeholder="posición" class="dposicion form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[1]" />"><input placeholder="largo" class="dlargo form-control form-control-sm d-inline " style="width: 25%" value="<s:property value="DestinoEdit[2]" />"></span></span>		
			        </s:elseif>
			        <s:elseif test="%{validacionId.clasif == 7}">
			        <span class="datosOD<s:property value="#ind.index" />" > <span style="float:left;display:inline;width:50%">Origen: X;<input placeholder="línea" class="olínea" style="width: 25%;display:none" value="X" data-visible="0"><input placeholder="posición" class="oposicion form-control form-control-sm d-inline "  style="width: 24%;" value="<s:property value="OrigenEdit[1]" />"><input placeholder="largo"  class="olargo form-control form-control-sm d-inline " style="width: 24%;" value="<s:property value="OrigenEdit[2]" />"><input placeholder="fecha"  class="ofecha form-control form-control-sm d-inline " style="width: 24%;" title="Ej:YYMMDD" value="<s:property value="OrigenEdit[3]" />"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="dlínea form-control form-control-sm d-inline " style="width: 16%" value="<s:property value="DestinoEdit[0]" />"><input placeholder="posición" class="dposicion form-control form-control-sm d-inline " style="width: 20%" value="<s:property value="DestinoEdit[1]" />"><input placeholder="largo" class="dlargo form-control form-control-sm d-inline " style="width: 16%" value="<s:property value="DestinoEdit[2]" />"><input placeholder="fecha" class="dfecha form-control form-control-sm d-inline " style="width: 20%" title="Ej:YYMMDD" value="<s:property value="DestinoEdit[3]" />"></span></span>				
			        </s:elseif>
					</td>
					
					</tr>
				 </s:iterator>
					<tr class="validaciones">
						<th class="align-middle">Validaci&oacute;n de variabilidad / Nro. <s:if test="%{idJobWork == 0}">inicial</s:if> de l&iacute;neas</th>
						<td class="align-middle">
							<div class="custom-control custom-checkbox custom-control-inline align-middle mr-0">
								<input type="checkbox" id="validacionCheckbox" class="variabilidadCheck custom-control-input" style="width: 3%;" value="<s:property value="%{jobWork.variabilidad}" />" <s:if test="%{jobWork.variabilidad == 1}">checked="checked"</s:if>>
								<label for="validacionCheckbox" class="custom-control-label"></label>
							</div>
							<div class="custom-control-inline">
								<span class="variabilidad align-middle" <s:if test="%{jobWork.variabilidad == 0}">style="display:none"</s:if>> 
									<input type="number" class="variabilidad variabilidad-porciento form-control d-inline" style="width: 46%;" placeholder="%"  min="0" max="100" <s:if test="%{idJobWork > 0}">value="<s:property value="%{jobWork.variabilidadPorciento}" />"</s:if>/> 
									/ 
									<s:if test="%{idJobWork > 0}"><s:property value="%{jobWork.variabilidadLineas}" />
										<input type="hidden" class="variabilidad variabilidad-minlineas form-control d-inline" style="width: 47%;" min="1" value="<s:property value="%{jobWork.variabilidadLineas}" />" />
									</s:if>
									<s:else><input type="number" class="variabilidad variabilidad-minlineas form-control d-inline" style="width: 47%;" min="1" value="1" /></s:else> 
								</span>
							</div>
						</td>
					</tr>
					<tr>
						<th class="align-middle">Notificaci&oacute;n > Fallos de validaci&oacute;n</th>
						<td class="align-middle">
							<div class="custom-control custom-checkbox custom-control-inline align-middle mr-0">
								<input id="validacionVariedadCheckbox" type="checkbox" class="alerta custom-control-input" style="width: 3%;" value="<s:property value="%{jobWork.notificacion1}" />" <s:if test="%{jobWork.notificacion1 == 1}">checked="checked"</s:if>>
								<label for="validacionVariedadCheckbox" class="custom-control-label"></label>
							</div>
							<div class="custom-control-inline align-middle" style="width:90%">
								<input maxlength="254" type="text"  class="cadena form-control" style="<s:if test="%{jobWork.notificacion1 == 0}">display:none;</s:if>" placeholder="Ex: correo1@server.com;correo2@server.com;correo3@server.com" value="<s:property value="%{jobWork.notificacion1Cadena}" />"/>
							</div>
						</td>
					</tr>
					<tr>
						<th class="align-middle">Horario de ejecuci&oacute;n</th>
						<td class="align-middle">
						    <input type="number" style="width: 48%; display: inline;" placeholder="HH" min="0" max="23" class="ejecucion-hora form-control d-inline" value="<s:property value="%{jobWork.EjecucionHoraShow}" />"/> / 
						    <input min="0" max="59" placeholder="mm" type="number" style="width: 47%; display: inline;" class="ejecucion-minuto form-control d-inline" value="<s:property value="%{jobWork.EjecucionMinShow}" />"/>
						</td>
					</tr>
					<tr>
						<th class="align-middle">Nro. Reintentos / Minutos entre reintentos</th>
						<td class="align-middle"><input type="number" min="0" max="20" style="width: 48%" class="reintentos form-control d-inline" value="<s:property value="%{jobWork.reintentos}" />"> / <input style="width: 47%" type="number" min="0" max="59" class="reintentos-min form-control d-inline" value="<s:property value="%{jobWork.reintentosMin}" />"></td>
					</tr>
					<tr>
						<th class="align-middle">Activado</th>
						
							<td class="align-middle">
								<div class="custom-control custom-checkbox">
									<input id="activadoCheckbox" type="checkbox" class="procesoActivado custom-control-input" style="width: 3%;" value="<s:property value="%{jobWork.activo}" />" <s:if test="%{jobWork.activo == 1}">checked="checked"</s:if> >
									<label for="activadoCheckbox" class="custom-control-label">¿Activado?</label>
								</div>
							</td>
					</tr>
				</tbody>
			</table>
			<form action="" method="post" id="fullstack" style="display:none">
			  <input type="text" name="proceso"  class="nombreProceso2" >
			  <input type="text" name="origenPath" class="origenPath2" >
			  <input type="text" name="destinoPath" class="destinoPath2" >
			  <input type="text" name="origenTipo" class="origenTipo2" >
			  <input type="text" name="destinoTipo" class="destinoTipo2" >
			  <input type="text" name="origenIP" class="origenIP2" >
			  <input type="text" name="destinoIP" class="destinoIP2" >
			  <input type="text" name="origenPuerto" class="origenPuerto2" >
			  <input type="text" name="destinoPuerto" class="destinoPuerto2" >
			  <input type="text" name="origenUsuario" class="origenUsuario2" >
			  <input type="text" name="origenPass" class="origenPass2" >
			  <input type="text" name="destinoUsuario" class="destinoUsuario2" >
			  <input type="text" name="destinoPass" class="destinoPass2" >
			  <input type="text" name="archivo" class="archivo2" >
			  <input type="text" name="archivoExt" class="archivo-ext2" >
			  <input type="text" name="control" class="archivoDeControl2" >
			  <input type="text" name="controlExt" class="control2" >
			  <input type="text" name="alerta" class="alerta2" >
			  <input type="text" name="cadena" class="cadena2" >
			   <input type="text" name="reintentos" class="reintentos2" >
			   <input type="text" name="reintentosMin" class="reintentos-min2" >
			   <input type="text" name="ejecucionHora" class="ejecucion-hora2" >
			   <input type="text" name="ejecucionMinuto" class="ejecucion-minuto2" >
			   <input type="text" name="activo" class="procesoActivado2" >
			   <input type="text" name="validaciones" class="validaciones2" >
			   <input type="text" name="variabilidad" class="variabilidadCheck2" >
			   <input type="text" name="variabilidadPorciento" class="variabilidad-porciento2" >
			   <input type="text" name="variabilidadLineas" class="variabilidad-minlineas2" >
			   <input type="text" name="idJobWork" value="<s:property value="%{idJobWork}" />">
			</form>
			
			<!-- <br />  -->
			
			<!-- style="color: rgb(0, 0, 0);"  -->
			<div id="botonesPaginacion" class="text-center" style="font-style: normal; font-family: Arial, Helvetica, sans-serif; font-size: 8pt;">
				<div class="mr-2" style="display: inline">
					<a href="javascript:envio('gestionTDIAction', 10, 33);"><button class="btn btn-primary">Volver</button></a>
				</div>
				<!-- style="float: right; margin-top: -5px" -->
				<div class="d-inline">
					<button class="btn btn-primary" id="grabarTDI">Guardar</button>
				</div>
			</div>
			<script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
<script src="js/jquery.Rut.js" type="text/javascript"></script>
<script type="text/javascript" src="js/utils.js"></script>
			<script>
				$(function() {
					
					function validateEmail(email) {
						  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						  return re.test(email);
						}
					
					var contadorGlobal = $('.tipoValidacion').length;
					btnDelete(1);
					
					function btnDelete(valor){
						$('.btn-delete').click(function(){
							if(valor == 1){
								$(this).parent().parent().parent().remove();
							}else{
								$(this).parent().parent().remove();
							}
							
						});
					}
					//tipoValidacion(0);
					function tipoValidacion(num){
						btnDelete(0);
						$('.tipoValidacion'+num).change(function(){
							
							var seleccionado = $(this).val();
							var contadorS = 0;
							
							$('.tipoValidacion').each(function(){
								if ($(this).val() === seleccionado){
									contadorS++;
								}
							});
							if(contadorS === 1){
								var valorSeleccionado = $('.tipoValidacion'+num+' option:selected').html();
								$('.tipoValidacion'+num).hide();
								$('.btn-delete'+num).remove();
								var ayuda = "";
								var tipo = parseInt($('.tipoValidacion'+num+' option:selected').attr('data-type'));
								if(tipo === 7){
									ayuda = '<span class="tooltip2">(?)<span class="tooltiptext2">Para el formato de la fecha puede ocupar (Y,M,D).<br> Ejemplos:YYMMDD, DDMMYYYY, MM/DD/YY</span></span>';
								}
								$('.tipoValidacion'+num).before('<div><img class="btn-delete btn-delete'+num+'" src="${pageContext.request.contextPath}/img/trash.png" style="width: 20px; padding: 2px; cursor: pointer;" title="Eliminar"> <b>'+valorSeleccionado+" </b><span>"+ayuda+"</span></div><br>");
								btnDelete(1);
								if ($('.tipoValidacion'+num+' option:selected').attr('data-valid') === '1') {
									var inputs = "";
									if(tipo === 1 || tipo === 3){
										inputs = '<span class="datosOD'+num+'" ><span style="float:left;display:inline;width:50%">Origen: <input placeholder="línea" class="form-control form-control-sm d-inline olínea" style="width: 25%">;X;X<input placeholder="posición" class="oposicion" value="X" data-visible="0" style="width: 25%; display:none"><input value="X" data-visible="0" placeholder="largo"  class="olargo" style="width: 25%;display:none"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="form-control form-control-sm d-inline dlínea" style="width: 25%"><input placeholder="posición" class="form-control form-control-sm d-inline dposicion" style="width: 25%"><input placeholder="largo" class="form-control form-control-sm d-inline dlargo" style="width: 25%"></span></span>';
									}else if(tipo === 2 || tipo === 4){
										inputs = '<span class="datosOD'+num+'" > <span style="float:left;display:inline;width:50%">Origen: <input placeholder="línea" class="form-control form-control-sm d-inline olínea" style="width: 25%"><input placeholder="posición" class="form-control form-control-sm d-inline oposicion"  style="width: 25%;"><input placeholder="largo"  class="form-control form-control-sm d-inline olargo" style="width: 25%;"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="form-control form-control-sm d-inline dlínea" style="width: 25%"><input placeholder="posición" class="form-control form-control-sm d-inline dposicion" style="width: 25%"><input placeholder="largo" class="form-control form-control-sm d-inline dlargo" style="width: 25%"></span></span>';	
									}else if(tipo === 5){
										inputs = '<span class="datosOD'+num+'" > <span style="float:left;display:inline;width:50%">Origen: <input placeholder="línea" class="form-control form-control-sm d-inline olínea" style="width: 25%">;X;X<input placeholder="posición" class="oposicion" value="X" data-visible="0" style="width: 25%; display:none"><input placeholder="largo" value="X" data-visible="0" class="olargo" style="width: 25%;display:none"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="dlínea" value="X" data-visible="0" style="width: 25%; display:none">X;<input placeholder="posición" class="form-control form-control-sm d-inline dposicion" style="width: 25%"><input placeholder="largo" class="form-control form-control-sm d-inline form-control form-control-sm d-inline dlargo" style="width: 25%"></span></span>';
									}else if(tipo === 6){
										inputs = '<span class="datosOD'+num+'" > <span style="float:left;display:inline;width:50%">Origen: <input placeholder="línea" class="form-control form-control-sm d-inline olínea" style="width: 25%"><input placeholder="posición" class="form-control form-control-sm d-inline oposicion"  style="width: 25%;"><input placeholder="largo"  class="form-control form-control-sm d-inline olargo" style="width: 25%;"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="dlínea" value="X" data-visible="0" style="width: 25%; display:none">X;<input placeholder="posición" class="form-control form-control-sm d-inline dposicion" style="width: 25%"><input placeholder="largo" class="form-control form-control-sm d-inline dlargo" style="width: 25%"></span></span>';	
									}else if(tipo === 7){
										inputs = '<span class="datosOD'+num+'" > <span style="float:left;display:inline;width:50%">Origen: X;<input placeholder="línea" class="olínea" style="width: 25%;display:none" value="X" data-visible="0"><input placeholder="posición" class="form-control form-control-sm d-inline oposicion"  style="width: 24%;"><input placeholder="largo"  class="form-control form-control-sm d-inline olargo" style="width: 24%;"><input placeholder="fecha"  class="form-control form-control-sm d-inline ofecha" style="width: 24%;" title="Ej:YYMMDD"></span> <span style="float:right;display:inline;width:50%">Destino: <input placeholder="línea" class="form-control form-control-sm d-inline dlínea" style="width: 16%"><input placeholder="posición" class="form-control form-control-sm d-inline dposicion" style="width: 20%"><input placeholder="largo" class="form-control form-control-sm d-inline dlargo" style="width: 16%"><input placeholder="fecha" class="form-control form-control-sm d-inline dfecha" style="width: 20%" title="Ej:YYMMDD"></span></span>';	
									}
									
									$('.tipoValidacion'+num).after(inputs);
								}
							}else{
								//Ya fue seleccionado
								alert("La validaci\u00F3n se encuentra asignada");
								$('.tipoValidacion'+num).val("");
							}
							
						});
					}
					
					
					$('.validacion-plus').click(function(){
						var num = contadorGlobal;
						var bandera = true;
						$('.tipoValidacion').each(function(){
							if ($(this).val() === ""){
								bandera = false;
							}
						});
						if(bandera){
							$('.validaciones').before('<tr><th class="align-middle"></th><td class="align-middle"><img class="btn-delete btn-delete'+num+'" src="${pageContext.request.contextPath}/img/trash.png" style="width: 20px; padding: 2px; cursor: pointer;" title="Eliminar"> <select style="width:90%" class="custom-select tipoValidacion tipoValidacion'+num+'" data-index="'+num+'"><option value="">Seleccione...</option><s:iterator value="tiposValidacion"><option value="<s:property value="sid"/>" data-num="'+num+'" data-type="<s:property value="clasif"/>" data-valid="<s:property value="validaOD"/>"><s:property value="detalle" /></option></s:iterator></select></td></tr>')
						tipoValidacion(num);	
						}
						contadorGlobal++;
					});
					
					$('.alerta').change(function(){
					     visualizacion('.alerta', '.cadena');
					});
					$('.procesoActivado').change(function(){
					     $(this).val($(this).is(':checked') ? '1' : '0');
					});
					$('.archivoDeControl').change(function(){
						visualizacion('.archivoDeControl', '.showArchivoDeControl');
					});
					
					$('.origenTipo').change(
							function() {
								TipoConexion('origenTipo', 'origen-data');
							});
					$('.destinoTipo').change(
							function() {
								TipoConexion('destinoTipo', 'destino-data');
							});
					$('.variabilidadCheck').change(function() {
						visualizacion('.variabilidadCheck', '.variabilidad');
							});
					
					function visualizacion(clase1, clase2){
						 $(clase1).val($(clase1).is(':checked') ? '1' : '0');
							if($(clase1).is(':checked')){
								$(clase2).show();
							}else{
								$(clase2).hide();
							}
					}
					
					function TipoConexion(clase1, clase2){
						if ($('.'+clase1+' option:selected').html().trim() === 'SFTP' || $('.'+clase1+' option:selected').html().trim() === 'FTP') {
							$('.'+clase2).show();
						} else {
							$('.'+clase2).hide();
						}
					}
					
					function parseValidaciones(){
					//	[{"id":4, "origen":"1;3;4", "destino":"1;3;4"}]
					var json = '['
					var coma = '';
					$('.tipoValidacion').each(function(e){
						var id = $(this).val();
						var index = $(this).attr('data-index');
						var tipo = parseInt($('.tipoValidacion'+index+' option:selected').attr('data-type'));
						if(id !== ''){
							var olinea = $('.datosOD'+index).find(".olínea").val();
							var oposicion = $('.datosOD'+index).find(".oposicion").val();
							var olargo = $('.datosOD'+index).find(".olargo").val();
							var dfecha = "";
							var ofecha = "";
							if(tipo === 7){
								ofecha = ";"+$('.datosOD'+index).find(".ofecha").val();
								dfecha = ";"+$('.datosOD'+index).find(".dfecha").val();
							}
							
							var dlinea = $('.datosOD'+index).find(".dlínea").val();
							var dposicion = $('.datosOD'+index).find(".dposicion").val();
							var dlargo = $('.datosOD'+index).find(".dlargo").val();
							
							var origen = olinea+';'+oposicion+';'+olargo+ofecha;
							var destino = dlinea+';'+dposicion+';'+dlargo+dfecha;
							
							var obj = coma+'{ "id": '+id+', "origen": "'+origen+'", "destino":"'+destino+'"}';
							coma = ',';
							json += obj;
						}
					});
					 json += ']';
					return json;
					}
					
					function viewValidator(){
						var bandera = true;
						//Nombre Proceso
						
						if(bandera && $('.nombreProceso').val().trim() === ""){
							alert('El Nombre de Proceso es requerido');
							bandera = false;
						}
						if(bandera && $('.origenTipo').val() === ""){
							alert('Conexi\u00F3n Origen es requerido');
							bandera = false;
						}
						if(bandera && $('.origenPath').val() === ""){
							alert('Conexi\u00F3n Origen: Path es requerido');
							bandera = false;
						}
						if(bandera && $('.origenTipo').find('option:selected').html() !== "LOCAL"){
							
							if(bandera && ($('.origenIP').val().trim() === "" || $('.origenPuerto').val().trim() === "") ){
								alert('Conexi\u00F3n Origen: IP / Puerto es requerido');
								bandera = false;
							}
							if(bandera && (isNaN(Number($('.origenPuerto').val().trim()))) ){
								alert('El Puerto Origen debe ser num\u00E9rico');
								bandera = false;
							}
							if(bandera && ($('.origenUsuario').val().trim() === "" || $('.origenPass').val().trim() === "") ){
								alert('Conexi\u00F3n Origen: Usuario / Contrase\u00F1a es requerido');
								bandera = false;
							}
							
						}
						if(bandera && $('.destinoTipo').val() === ""){
							alert('Conexi\u00F3n Destino es requerido');
							bandera = false;
						}
						if(bandera && $('.destinoPath').val() === ""){
							alert('Conexi\u00F3n Destino: Path es requerido');
							bandera = false;
						}
						if(bandera && $('.destinoTipo').find('option:selected').html() !== "LOCAL"){
							
							if(bandera && ($('.destinoIP').val().trim() === "" || $('.destinoPuerto').val().trim() === "") ){
								alert('Conexi\u00F3n Destino: IP / Puerto es requerido');
								bandera = false;
							}
							if(bandera && (isNaN(Number($('.destinoPuerto').val().trim()))) ){
								alert('El Puerto Destino debe ser num\u00E9rico');
								bandera = false;
							}
							if(bandera && ($('.destinoUsuario').val().trim() === "" || $('.destinoPass').val().trim() === "") ){
								alert('Conexi\u00F3n Destino: Usuario / Contrase\u00F1a es requerido');
								bandera = false;
							}
							
						}
						if(bandera && ($('.archivo').val().trim() === "" || $('.archivo-ext').val().trim() === "") ){
							alert('Archivo / Extensi\u00F3n es requerido');
							bandera = false;
						}
						
						if(bandera && $('.archivoDeControl').is(':checked') && $('.control').val().trim() === ""){
							alert('Extensi\u00F3n Archivo Control es requerido');
							bandera = false;
						}
						if(bandera && $('.variabilidadCheck').is(':checked') && ($('.variabilidad-porciento').val().trim() === "" || $('.variabilidad-minlineas').val().trim() === "")){
							alert('Validaci\u00F3n de variabilidad / Nro. inicial de l\u00EDneas es requerido');
							bandera = false;
						}
						if(bandera && (parseInt($('.variabilidad-minlineas').val().trim()) < 1)){
							alert('Nro. inicial de l\u00EDneas debe ser mayor a 0');
							bandera = false;
						}
						if(bandera && $('.alerta').is(':checked') && $('.cadena').val().trim() === "" ){
							alert('Notificaci\u00F3n > Fallos de validaci\u00F3n es requerido');
							bandera = false;
						}
						
						if(bandera && $('.alerta').is(':checked') && $('.cadena').val().trim() !== "" ){
							var correos = $('.cadena').val().trim().split(";");
							for(var i = 0; i < correos.length; i++){
								if(!validateEmail(correos[i].trim())){
									alert('El Email ingresado ('+correos[i]+') no es v\u00E1lido');
									bandera = false;
									break;
								}
							}
						}
						
						if(bandera && ($('.ejecucion-hora').val().trim() === "" || $('.ejecucion-minuto').val().trim() === "")){
							alert('Horario de ejecuci\u00F3n es requerido');
							bandera = false;
						}
						
						if(bandera && ( parseInt($('.reintentos').val()) > 0 && parseInt($('.reintentos-min').val()) < 1)){
							alert('Minutos entre reintentos debe ser mayor o igual a 1');
							bandera = false;
						}
						
						if(bandera){
							$(".olínea").each(function(){
								if(bandera && $(this).attr('data-visible') === undefined && (isNaN(Number($(this).val())) || $(this).val().trim() === "" || Number($(this).val()) < 1 )){
									alert('En las validaciones el campo L\u00EDnea Origen debe ser num\u00E9rico: '+$(this).val());
									bandera = false;
								}
							});
							$(".oposicion").each(function(){
								if(bandera && $(this).attr('data-visible') === undefined && (isNaN(Number($(this).val())) || $(this).val().trim() === "")){
									alert('En las validaciones el campo Posici\u00F3n Origen debe ser num\u00E9rico: '+$(this).val());
									bandera = false;
								}
							});
							$(".olargo").each(function(){
								if(bandera && $(this).attr('data-visible') === undefined && (isNaN(Number($(this).val())) || $(this).val().trim() === "")){
									alert('En las validaciones el campo Largo Origen debe ser num\u00E9rico: '+$(this).val());
									bandera = false;
								}
							});
							$(".dlínea").each(function(){
								if(bandera && $(this).attr('data-visible') === undefined && (isNaN(Number($(this).val())) || $(this).val().trim() === "")){
									alert('En las validaciones el campo L\u00EDnea Destino debe ser num\u00E9rico: '+$(this).val());
									bandera = false;
								}
							});
							$(".dposicion").each(function(){
								if(bandera && $(this).attr('data-visible') === undefined && (isNaN(Number($(this).val())) || $(this).val().trim() === "")){
									alert('En las validaciones el campo Posici\u00F3n Destino debe ser num\u00E9rico: '+$(this).val());
									bandera = false;
								}
							});
							$(".dlargo").each(function(){
								if(bandera && $(this).attr('data-visible') === undefined && (isNaN(Number($(this).val())) || $(this).val().trim() === "")){
									alert('En las validaciones el campo Largo Destino debe ser num\u00E9rico: '+$(this).val());
									bandera = false;
								}
							});
							/*$(".ofecha").val();
							$(".dfecha").val();*/
						}
						
						return bandera;
					}
					
					/* Clic botón grabarDiaInhabil se guarda el registro en la base de datos */
					$("#grabarTDI").click(function() {
								if (viewValidator()){
									$('.nombreProceso2').val($('.nombreProceso').val());
									$('.origenPath2').val($('.origenPath').val());
									$('.destinoPath2').val($('.destinoPath').val());
									$('.origenTipo2').val($('.origenTipo').val());
									$('.destinoTipo2').val($('.destinoTipo').val());
									$('.origenIP2').val($('.origenIP').val());
									$('.destinoIP2').val($('.destinoIP').val());
									$('.origenPuerto2').val($('.origenPuerto').val());
									$('.destinoPuerto2').val($('.destinoPuerto').val());
									$('.origenUsuario2').val($('.origenUsuario').val());
									$('.origenPass2').val($('.origenPass').val());
									$('.destinoUsuario2').val($('.destinoUsuario').val());
									$('.destinoPass2').val($('.destinoPass').val());
									$('.archivo2').val($('.archivo').val());
									$('.archivo-ext2').val($('.archivo-ext').val());
									$('.archivoDeControl2').val($('.archivoDeControl').val());
									$('.control2').val($('.control').val());
									$('.alerta2').val($('.alerta').val());
									$('.cadena2').val($('.cadena').val());
									$('.reintentos2').val($('.reintentos').val());
									$('.reintentos-min2').val($('.reintentos-min').val());
									$('.ejecucion-hora2').val($('.ejecucion-hora').val());
									$('.ejecucion-minuto2').val($('.ejecucion-minuto').val());
									$('.procesoActivado2').val($('.procesoActivado').val());
									$('.validaciones2').val(parseValidaciones());
									$('.variabilidadCheck2').val($('.variabilidadCheck').val());
									$('.variabilidad-porciento2').val($('.variabilidad-porciento').val());
									$('.variabilidad-minlineas2').val($('.variabilidad-minlineas').val());
									
									$.ajax({
										url : "guardarTDIAction",
										type : "POST",
										data : $('#fullstack').serialize(),
										dataType : "json",
										success : function(data) {
											switch(data){
											case "1":envio('gestionTDIAction?creado=1', 10, 33);
												break;
											case "3":
												envio('gestionTDIAction?creado=3', 10, 33);
											break;
											case "2":
												alert("Existe job con el mismo nombre de proceso");
												break;
											 default:
													alert("Error en la inserci\u00F3n de datos");
													break;
											}
										}
									});
									
							}
								
							});
				});
			</script>
</body>
</html>