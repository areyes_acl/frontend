<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
            <span style="display:none" class="resultPage"><s:property value="%{#request.page}" /></span>
            <span style="display:none" class="resultPageFinal"><s:property value="%{#request.ultimaPagina}" /></span>
            <span style="display:none" class="resultadoAction"><s:property value="%{#request.resultadoAction}" /></span>
            <span style="display:none" class="resultNombreArchivo"><s:property value="%{#request.searchArchivo}" /></span>
			<s:if test="%{resultadoAction == 0}">
			
			<!-- class="tablaContenido" -->
			<table width="100%" class="table table-sm small" id="tablaRes">
			<thead>
			<tr class="text-center" style="text-transform: uppercase; font-weight: bold;">
						<th class="align-middle">#</th>
						<th class="align-middle">Proceso</th>
						<th class="align-middle">Archivo</th>
						<th class="align-middle">Ejecuci&oacute;n</th>
						<th class="align-middle">Activado</th>
						<th class="align-middle">Acciones</th>
					</tr>
					</thead>
				<tbody>
				<s:iterator value="jobs" status="ind">
					<tr class="text-center fullrow<s:property value="sid" />">
						<td class="align-middle"><s:property value="%{#request.valorInicial + (#ind.index+1)}"/></td>
						<td class="align-middle"><s:property value="proceso" /></td>
						<td class="align-middle"><s:property value="ArchivoReal" /></td>
						<td class="align-middle"><s:property value="EjecucionShow" /></td>
						<td class="align-middle"><s:property value="Activado" /></td>
						<td class="align-middle" style="text-align: center">	
									<img onclick="envio('detalleTDIAction?showId=<s:property value="sid" />', 10, 33);" src="${pageContext.request.contextPath}/img/lupe.png"
									style="width: 20px; padding: 2px; cursor: pointer;"
									title="Detalle">
									<img onclick="envio('crearTDIAction?idJobWork=<s:property value="sid" />', 10, 33);" src="${pageContext.request.contextPath}/img/pencil.png"
									style="width: 20px; padding: 2px; cursor: pointer;"
									title="Editar"> 
						<s:if test="%{cantidadDeLogs == 0}">
						<img class="btn-delete" data-id="<s:property value="sid" />" data-proceso="<s:property value="proceso" />" src="${pageContext.request.contextPath}/img/trash.png" style="width: 20px; padding: 2px; cursor: pointer;" title="Eliminar"> 
                        </s:if>
									</td>
					</tr>
				 </s:iterator>
				</tbody>
			</table>
			</s:if>
			<s:elseif test="%{resultadoAction == 2}">
			<div class="alert alert-info font-weight-bold text-center">
			No se encontraron resultados
			</div>
            </s:elseif>