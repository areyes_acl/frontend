<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Insert title here</title>
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	<script type="text/javascript" src="js/utils.js"></script>
	
	<% String estado = request.getParameter("estadoGestion");%>
	
	
 	<script type="text/javascript">
 	  $(document).ready(function(){        	
        	$("#id_sid").val( "<%=request.getParameter("sidConci")%>");	
        	$("#sidusuario").val( "<%=request.getParameter("idUser")%>");		
        	$("#estadoGestion").val( "<%=request.getParameter("estadoGestion")%>");
        	
        	        					
		});
		
			
	$("#salir").click(function(){
        	
       	$('#popup').css({display: 'none'});
       	$('#popUpContenido').empty().css({display:'none'});
        	
    });
    
    $("#guardarGestion").click(function(){
    	var obs = $("#obs").val();
    	var gestion = $("#checkgestion").prop("checked") ? 1 : 0; 
    	var sid = $("#id_sid").val();
    	var sidusuario = $("#sidusuario").val();
    	        	       
       	if(gestion == 1){
       	
       			if(obs== ''){
    				document.getElementById("ejemplo").innerHTML = "<font size='2' color='red'>* Favor ingresar Comentario</font>";
    				return false;
		    	}else{
		    		document.getElementById("ejemplo").innerHTML = "";
		    		valiadGestion(obs,gestion,sid,sidusuario);
		    	}
       	
      	}else if(gestion == 0){
      	
      	    	if(obs== ''){
    				document.getElementById("ejemplo").innerHTML = "<font size='2' color='red'>* Favor ingresar Comentario</font>";
    				return false;
		    	}else{
		    		document.getElementById("ejemplo").innerHTML = "";
		    		guardarGestion(obs,gestion,sid,sidusuario);
		    	}
      		
      	}
        	
    });
    
    function valiadGestion(obs,gestion,sid,sidusuario){
          var retVal = confirm("Seguro que desea dejar como gestionada la transacci\u00F3n ?");
    
    	if( retVal == true ){
              guardarGestion(obs,gestion,sid,sidusuario);
          }
          else{
             return false;
         }
    }
    
    function guardarGestion(obs,gestion,sid,sidusuario){
    		$.ajax({
	       			url:'ejecutaGestionAction',
	       			type:'POST',
	       			data:{
	       					'sidConci': sid, 
            				'comentario': obs,
            				'gestion': gestion,
            				'idUser':sidusuario
            			},
	       			
	       		}).done(function(data){
	       			data.resEjecucion;
	       			//alert(data.ejecutaInsert.replace('esttrx:1|',"" ));
	       			if(data.ejecutaInsert == 'ok')
       				{	       	
       						alert("Se ha guardado correctamente la gesti\u00F3n para la transacci\u00F3n seleccionada.");			
	       					$('#popup').css({display: 'none'});
        					$('#popUpContenido').empty().css({display:'none'});
        					reload();
        					
       				}else if(data.ejecutaInsert == 'EXISTE'){
       						alert("Para la transacci\u00F3n seleccionada ya se realiz\u00F3 el t\u00E9rmino de su gesti\u00F3n, favor reintentar nuevamente.");
       						$('#popup').css({display: 'none'});
        					$('#popUpContenido').empty().css({display:'none'});
        					reload();
       				}
	       		});
    }
    
    function reload(){
		var fechaInicioTmp = $('#fechaProceso').val().split("/");;
		var fechaTerminoTmp = $('#fechaFin').val().split("/");
		
		
		var estado	= $('#estado').val();
		var estadoGestion	= $('#gestionEstado').val();
		var estadoDetalle	= $('#detalleEstado').val();
		
		var diaInicio = fechaInicioTmp[0];
		var mesInicio = fechaInicioTmp[1];
		var anoInicio = fechaInicioTmp[2];
		
		var diaTermino = fechaTerminoTmp[0];
		var mesTermino  = fechaTerminoTmp[1];
		var anoTermino = fechaTerminoTmp[2];
		
		var fechaInicio  = anoInicio + mesInicio + diaInicio;
		var fechaTermino = anoTermino + mesTermino + diaTermino;
							
		$.ajax({
			url : "ConciliacionAvancesAction",
			type : "POST",
			data : {
					fechaInicio : fechaInicio,
					fechaFinal: fechaTermino,
					estado: estado,
					estadoGestion: estadoGestion,
					estadoDetalle: estadoDetalle
				},
			dataType : "json",
			error : function(XMLHttpRequest, textStatus,
					errorThrown) {
				alert('Error ' + textStatus);
				alert(errorThrown);
				alert(XMLHttpRequest.responseText);
			},
			success : function(data) {
				mostrarTransacciones(data);
				checkbox();
			}
		});
    }
    
    		// FUNCION DE CONTROL DE CHECK	
		function checkbox(){
			// SI TODOS ESTAN CHEQUEADOS, SELECCIONA EL ALL
			if($('.checkbox1:checked').length == $('.checkbox1').length){
			    $('input[name="select_all"]').prop('checked', true);
			  }
		
				$('#select_all').click(function(event) {
    				 if(this.checked) { // check select status
           				 $('.checkbox1').each(function() { //loop through each checkbox
                				this.checked = true;  //select all checkboxes with class "checkbox1"               
            				});
        			}else{
            			$('.checkbox1').each(function() { //loop through each checkbox
                			this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            			});         
        			}
    			});
    		
    		
    			$('input[name="check[]"]').change(function () {
				   var selectAll = true;
				    // Confirm all checkboxes are checked (or not)
				    $('input[name="check[]"]').each(function (i, obj) {
				        if ($(obj).is(':checked') === false) {
				            // At least one checkbox isn't selected
				            selectAll = false;
				            return false;
				        }
				    });
				    // Update "Select All" checkbox appropriately
				    $('input[name="select_all"]').prop('checked', selectAll);
				});
		}
    
    
 	</script>
 	
 	<style type="text/css">
 	#tableid .tr:first-child {
		  background-color: #A7A6A6;
		}
 	</style>
</head>
<body>

		<input type="hidden" id="id_sid" name="sid" />
		<input type="hidden" name="sidusuario" id="sidusuario" />
		<input type="hidden" name="estadoGestion" id="estadoGestion" />
		
		<!-- class="titulo" -->
		<div class="col-8 mx-auto my-4 text-align-center text-center font-weight-bold">
			GESTI&Oacute;N DE AVANCES CON TRANSFERENCIA
		</div>
		<hr>
		
		<% if(!estado.equals("")){ %>
			<!-- <br style="line-height:5px"/>  -->
			  <div>
	      		<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm small col-4 mx-auto">
	       	       		<tr>
	       					<td style="border:none !important" align="center" class="align-middle font-weight-bold py-0">
	       						Hist&oacute;rico de Gesti&oacute;n
	       					</td>
	       				</tr>
	      		</table>
     	 </div>
     	 
     	<!-- <br style="line-height:5px"/>  -->
     	
     	<!-- width:800px;height:150px; -->
     	<!-- style="border:1px solid black; overflow:scroll;overflow-y:scroll;overflow-x:hidden;"-->
	<div class="h-25 mb-4">
		
		<!-- class="tablaContenido" border="1" -->
		<table width="100%" id="tableid" class="table table-sm small col-8 mx-auto" style="overflow-y:scroll;overflow-x:hidden;">
		 <thead>
		<tr class="text-center">
			<th class="align-middle">Fecha</th> 
			<th class="align-middle">Usuario</th>
			<th class="align-middle">Comentario</th>
			<th class="align-middle">Estado</th>
		</tr>
		</thead>
		<tbody>
		<s:iterator id="data" value="%{#request.listaFinalGestion}" >
							
			<s:if test="%{#request.listaFinalGestion.size > 0}">
			<!-- class="tr" -->
			<tr class="text-center">
				<td class="align-middle" align="center"><s:property value="fecha"/></td>
				<!-- align="left" -->
				<td class="align-middle" align="center"><s:property value="sidUser"/></td>
				<!-- align="left" -->
				<td class="align-middle" align="center"><s:property value="comentario"/></td>
				<s:set name="estado_id" value="estado"/>
				
				
					 <s:if test="%{#estado_id == 0}">
			  	   		<td class="align-middle" align="center"><span title='En Curso'><input type='image' style='width:18px;height:18px;' src='img/amarillo.png' /></span></div></td>
			    	</s:if>
			    	 <s:elseif test="%{#estado_id == 1 }">
			  	   		<td class="align-middle" align="center"><span title='Gestinado'><input type='image' style='width:18px;height:18px;' src='img/verde.png' /></span></div></td>
				    </s:elseif>
				    
			</tr>
			</s:if>
			 <s:if test="%{#request.listaFinalGestion.size == 0}">
			 <tr>
				 <td class="align-middle alert alert-info text-center font-weight-bold" role="alert"  align="center" colspan="4">SIN REGISTRO</td>
				 
			 </tr>
			 </s:if>
		</s:iterator>
		
		</tbody>
		</table>	
		</div>
		<% }%>
		
	<!-- <br style="line-height:5px"/>  -->
	
	
	<% if(!estado.equals("1")){ %>
	<div id="divgestion">	    	
	  <div> 
	      		<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm small col-4 mx-auto">
	       	       		<tr>
	       					<td style="border:none !important" align="center" class="align-middle font-weight-bold py-0">
	       						Agregar Gesti&oacute;n
	       					</td>
	       				</tr>
	      		</table>
     	 </div>
    
    <!-- <br style="line-height:5px"/>  -->
    
    <p id="ejemplo"></p>
	
	 <div>
	 			<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm small mx-auto">
	       	       		<tr>
	       					<td align="left">
	       						<div class="custom-control custom-checkbox">
	       					 		<input type="checkbox" name="checkgestion" id="checkgestion" class="custom-control-input">
	       					 		<label class="custom-control-label" for="checkgestion">T&eacute;rminar Gesti&oacute;n</label>
	       					 	</div>
	       					</td>
	       				
	       				</tr>
	       				<tr>
	       					<td align="left">
	       					 	<textarea rows="4" cols="110" id="obs" class="form-control form-control-sm"></textarea>
	       					</td>
	       				</tr>
	      		</table>
   	 </div>
</div>
<%}%>
<% if(estado.equals("1") || estado.equals("")){%>
	<!-- <br style="line-height:5px"/>
	<br style="line-height:5px"/>
	<br style="line-height:5px"/>
	<br style="line-height:5px"/>
	<br style="line-height:5px"/> -->
<%} %>
		
		<!-- <br style="line-height:5px"/>  -->
	      <div>
	      		<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm table-borderless">
	       	       		<tr>
	       					<td align="center">
	       					<% if(!estado.equals("1")){%>
	       						<input id="guardarGestion" type="button"  value="Guardar" class="btn btn-primary" />
	       					<%} %>
	       				    	<input type="button"  value="Cancelar" id="salir" name="salirpopup" class="btn btn-primary" />
	       					</td>
	       				</tr>
	      		</table>
     	 </div>
</body>
</html>