<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "Beans.Usuario" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Gestiones Por Cuenta</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
       <script src="js/jquery.Rut.js" type="text/javascript"></script>
       <script type="text/javascript" src="js/utils.js"></script>
       <script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
       
       <script>
       $(".datepicker").datepicker();
       
       	<%
       	Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
       	%>
       	
        var origenmodulo   = "<%=request.getParameter("origenmodulo")%>";

        $(document).ready(function(){
        
        	var f = new Date();
			
			var dia = f.getDate();
			var mes = (f.getMonth() +1);
			if(mes < 10)
				mes = "0"+mes;
			if(dia < 10)
				dia = "0"+dia;
			var ano = f.getFullYear();
			$(".fecha").val(dia + "/" + mes + "/" + ano);
        
        	var numerotarjeta  = "";
        	var fechainicio    = getYesterday();
    		var fechatermino   = getToday();
    		
    		if(fechainicio != "")
    			$('#fechaInicio').val(fechainicio);
    		
    		if(fechatermino != "")
    			$('#fechaTermino').val(fechatermino);
    		
    		$('#numerotarjeta').val(numerotarjeta);
    		
    		//alert("Viene de pantalla = " +  origenmodulo);
    		
    		if(origenmodulo == 'G'){
    			$("#cancelarGestion").css("display", "block"); 
    		} else {
    			$("#cancelarGestion").css("display", "none"); 
    		}
	    });
        $("#cancelarGestion").click(function(){
        	
        	$('#popup').css({display: 'none'});
        	$('#popUpContenido').empty().css({display:'none'});
        	
        	$("#buscarPresentaciones").click();
        	
        	$("#divlistaPresentaciones").css("display","none");
        	$("#divlistaGestionesCuentas").css("display","none");
			$("#divlistaGestionCargosAbonos").css("display","none");
			$("#divlistaGestionCargosAbonosNoExiste").css("display","none");
			$("#divlistaGestionesCuentasNoExiste").css("display","none");
			$("#exportar").attr("disabled", true);
			$("#cerrartransaccion").attr("disabled", true);
			
			//**********************************//
			//* DATOS DE LA CUENTA DEL CLIENTE *//
			//**********************************//
			$('#nomcliente').val("");
    		$('#rutcliente').val("");
    		$('#numerocuenta').val("");
    		$('#cupo').val("");
    		$('#fechaexpiracion').val("");
    		$('#estado').val("");
        	
        });
        
        
        $("#buscarPresentaciones").click(function(){
        	
        	$("#tablaGestionCuenta").css("display","none");
        	if ($('#fechaInicio').val().length  == 0){
				alert("Debe ingresar fecha de inicio para la busqueda...");
				$("#fechaInicio").focus();
				return;
			}
			
			if ($('#fechaTermino').val().length  == 0){
				alert("Debe ingresar fecha final para la busqueda...");
				$("#fechaTermino").focus();
				return;
			}
			
			if (validarFecha($('#fechaInicio').val()) == false){
				alert("La fecha de inicio ingresada es incorrecta...");
				$("#fechaInicio").focus();
				return;
			}
			
			if (validarFecha($('#fechaTermino').val()) == false){
				alert("La fecha final ingresada es incorrecta...");
				$("#fechaTermino").focus();
				return;
			}
			
			// SE AGREGA VALIDACION DE QUE SEA OBLIGACION LOS 16 CARACTERES DE LA TARJETA
			if($('#numerotarjeta').val().length != 0 && $('#numerotarjeta').val().length != 16){
				alert("Se deben ingresar los 16 numeros de tarjeta o ninguno...");
				$('#numerotarjeta').focus();
				return;
			}    
			
        	buscarPresentacionesIncomming();
        });
        
        $("#numerotarjeta").focus(function(){
        	$("#divlistaPresentaciones").css("display","none");
        	$("#divlistaGestionesCuentas").css("display","none");
			$("#divlistaGestionCargosAbonos").css("display","none");
			$("#divlistaGestionCargosAbonosNoExiste").css("display","none");
			$("#divlistaGestionesCuentasNoExiste").css("display","none");
			$("#exportar").attr("disabled", true);
			$("#cerrartransaccion").attr("disabled", true);
			
			//**********************************//
			//* DATOS DE LA CUENTA DEL CLIENTE *//
			//**********************************//
			$('#nomcliente').val("");
    		$('#rutcliente').val("");
    		$('#numerocuenta').val("");
    		$('#cupo').val("");
    		$('#fechaexpiracion').val("");
    		$('#estado').val("");
    		
    		
    		// Detalles de la transaccion
    		
    		$("#microfil").val("");
	        $("#codigoautorizacion").val("");
	        $("#otrodato01").val("");
	        $("#fechaefectiva").val("");
	        $("#fechaproceso").val("");
	        $("#otrodato02").val("");
	        $("#binadquirente").val("");
	        $("#leebanda").val("");
	        $("#otrodato03").val("");
	        $("#rubrocomercio").val("");
	        $("#otrodato04").val("");
	        $("#operador").val("");
	        $("#codigomonedatrx").val("");
    		
    		
		});
		
		
		$("#fechaInicio").focus(function(){
        	$("#divlistaPresentaciones").css("display","none");
        	$("#divlistaGestionesCuentas").css("display","none");
			$("#divlistaGestionCargosAbonos").css("display","none");
			$("#divlistaGestionCargosAbonosNoExiste").css("display","none");
			$("#divlistaGestionesCuentasNoExiste").css("display","none");
			$("#exportar").attr("disabled", true);
			$("#cerrartransaccion").attr("disabled", true);
			
			
			// Detalles de la transaccion
    		
    		$("#microfil").val("");
	        $("#codigoautorizacion").val("");
	        $("#otrodato01").val("");
	        $("#fechaefectiva").val("");
	        $("#fechaproceso").val("");
	        $("#otrodato02").val("");
	        $("#binadquirente").val("");
	        $("#leebanda").val("");
	        $("#otrodato03").val("");
	        $("#rubrocomercio").val("");
	        $("#otrodato04").val("");
	        $("#operador").val("");
	        $("#codigomonedatrx").val("");
			
			//**********************************//
			//* DATOS DE LA CUENTA DEL CLIENTE *//
			//**********************************//
			$('#nomcliente').val("");
    		$('#rutcliente').val("");
    		$('#numerocuenta').val("");
    		$('#cupo').val("");
    		$('#fechaexpiracion').val("");
    		$('#estado').val("");
	        
		});
		
        
        $("#fechainicio").focus(function(){
        	$("#divlistaPresentaciones").css("display","none");
        	$("#divlistaGestionesCuentas").css("display","none");
			$("#divlistaGestionCargosAbonos").css("display","none");
			$("#divlistaGestionCargosAbonosNoExiste").css("display","none");
			$("#divlistaGestionesCuentasNoExiste").css("display","none");
			$("#exportar").attr("disabled", true);
			$("#cerrartransaccion").attr("disabled", true);
			
			
			// Detalles de la transaccion
    		
    		$("#microfil").val("");
	        $("#codigoautorizacion").val("");
	        $("#otrodato01").val("");
	        $("#fechaefectiva").val("");
	        $("#fechaproceso").val("");
	        $("#otrodato02").val("");
	        $("#binadquirente").val("");
	        $("#leebanda").val("");
	        $("#otrodato03").val("");
	        $("#rubrocomercio").val("");
	        $("#otrodato04").val("");
	        $("#operador").val("");
	        $("#codigomonedatrx").val("");
			
			//**********************************//
			//* DATOS DE LA CUENTA DEL CLIENTE *//
			//**********************************//
			$('#nomcliente').val("");
    		$('#rutcliente').val("");
    		$('#numerocuenta').val("");
    		$('#cupo').val("");
    		$('#fechaexpiracion').val("");
    		$('#estado').val("");
	        
		});
        
        $("#fechaTermino").focus(function(){
        	$("#divlistaPresentaciones").css("display","none");
        	$("#divlistaGestionesCuentas").css("display","none");
			$("#divlistaGestionCargosAbonos").css("display","none");
			$("#divlistaGestionCargosAbonosNoExiste").css("display","none");
			$("#divlistaGestionesCuentasNoExiste").css("display","none");
			$("#exportar").attr("disabled", true);
			$("#cerrartransaccion").attr("disabled", true);
			
			//**********************************//
			//* DATOS DE LA CUENTA DEL CLIENTE *//
			//**********************************//
			$('#nomcliente').val("");
    		$('#rutcliente').val("");
    		$('#numerocuenta').val("");
    		$('#cupo').val("");
    		$('#fechaexpiracion').val("");
    		$('#estado').val("");
    		
    		
    		// Detalles de la transaccion
    		
    		$("#microfil").val("");
	        $("#codigoautorizacion").val("");
	        $("#otrodato01").val("");
	        $("#fechaefectiva").val("");
	        $("#fechaproceso").val("");
	        $("#otrodato02").val("");
	        $("#binadquirente").val("");
	        $("#leebanda").val("");
	        $("#otrodato03").val("");
	        $("#rubrocomercio").val("");
	        $("#otrodato04").val("");
	        $("#operador").val("");
	        $("#codigomonedatrx").val("");
    		
    		
		});
        
        
        $("#exportar").click(function(){
			
			DataExportar="";
			DatasExcel="";
			
			var numerotarjeta = '';
			var $radio 					= $("#tablaRes input[name='seleccionPRE']:checked").parent();
			//alert("que tiene" + $radio);
			var opcion                  = $radio.parent().parent().find('td').eq('0').text(); 
    		var numerotarjetaAux        = $radio.parent().parent().find('td').eq('1').text(); 
    		
    		if(numerotarjetaAux.length == 16){
    			numerotarjeta += numerotarjetaAux.substring(0,4);
    			numerotarjeta += 'XXXXXXXX';
    			numerotarjeta += numerotarjetaAux.substring(12,16);
    		}
    		else
    			numerotarjeta = '';
    		
    		var fechaTransaccion        = $radio.parent().parent().find('td').eq('2').text();
    		var comercio                = $radio.parent().parent().find('td').eq('3').text();
    		var pais                    = $radio.parent().parent().find('td').eq('4').text();
    		var tipo                    = $radio.parent().parent().find('td').eq('5').text();
    		var montooriginal           = $radio.parent().parent().find('td').eq('6').text();
    		var monto                   = $radio.parent().parent().find('td').eq('7').text();
    		var estatus                   = $radio.parent().parent().find('td').eq('8').text();
    		var patpass                   = $radio.parent().parent().find('td').eq('9').text();
    		
    		var nomcliente      = $('#nomcliente').val();
    		var rutcliente      = $('#rutcliente').val();
    		var numerocuenta    = $('#numerocuenta').val();
    		var cupo            = $('#cupo').val();
    		var fechaexpiracion = $('#fechaexpiracion').val();
    		var estado          = $('#estado').val();
    		
    		
    		var microfilm 			= $("#microfil").val();
	        var codigoAutorizacion  = $("#codigoautorizacion").val();
	        var otrodato1 			= $("#otrodato01").val();
	        var fechaEfectiva 		= $("#fechaefectiva").val();
	        var fechaProceso 		= $("#fechaproceso").val();
	        var otrodato2 			= $("#otrodato02").val();
	        var binAdquiriente 		= $("#binadquirente").val();
	        var leeBanda 			= $("#leebanda").val();
	        var otrodato3 			= $("#otrodato03").val();
	        var rubroComercio 		= $("#rubrocomercio").val();
	        var otrodato4 			= $("#otrodato04").val();
	        var operador 			= $("#operador").val();
	        var codigomonedatrx 	= $("#codigomonedatrx").val();
    		
    		
    		//****************************************************//
    		//Datos de la transaccion seleccionada para mostrar  *//
    		//****************************************************//
			idTablaExportar = "tablaRes";
			DatasExcel="";
			DataExportar += "TIT|NRO TC|FECHA TRANSACCION|COMERCIO|PAIS|TIPO TRANSACCION|MONTO ORIGINAL|MONTO|ESTATUS|PATPASS~";
			DataExportar += "DET|" + numerotarjeta + "|" +  fechaTransaccion + "|" + comercio + "|" + pais + "|" + tipo + "|" + montooriginal + "|" + monto + "|" + estatus +  "|" + patpass+"~";

			DataExportar += "TIT|NOMBRE CLIENTE|RUT CLIENTE|NUMERO CUENTA|CUPO|FECHA EXPIRACION|ESTADO~";
			DataExportar += "DET|" + nomcliente + "|" +  rutcliente + "|" + numerocuenta + "|" + cupo + "|" + fechaexpiracion + "|" + estado + "~";

			
			idTablaExportar = "myTableGestion";
			DatasExcel="";
			DataExportar += "TIT|FECHA|ACCION|CAUSAL|RAZON|MONEDA|MONTO|USUARIO~";
			DataExportar += exportarExcelLogProcesos(idTablaExportar, DatasExcel);
			
			idTablaExportar = "myTableGestionCargoAbono";
			DatasExcel="";
			DataExportar += "TIT|FECHA|MONEDA|MONTO|TIPO|DESCRIPCION|USUARIO~";
			DataExportar += exportarExcelLogProcesos(idTablaExportar, DatasExcel);
			
			
			DataExportar += "TIT|MICROFILM|CODIGO AUTORIZACION|CODIGO PROCESAMIENTO|FECHA EFECTIVA|FECHA PROCESO|CONVENIO CONCILIACION~";
			DataExportar += "DET|" + microfilm + "|" +  codigoAutorizacion + "|" + otrodato1 + "|" + fechaEfectiva + "|" + fechaProceso + "|" + otrodato2 + "~";
			
			
			DataExportar += "TIT|BIN ADQUIRIENTE|LEE BANDA|CONVENIO FACTURACION|CODIGO MONEDA TRX|RUBRO COMERCIO|PTO SERVICIO|OPERADOR~";
			DataExportar += "DET|" + binAdquiriente + "|" +  leeBanda + "|" + otrodato3 + "|" + codigomonedatrx + "|" + rubroComercio + "|" + otrodato4 + "|" + operador + "~";
			
			
			exportarGenerico(DataExportar, "export");
			
			});
			
			
        
        $("#cerrartransaccion").click(function(){
        	
        	var $radio 					= $("#tablaRes input[name='seleccionPRE']:checked");
    		var sidIncoming             = $radio.parent().parent().find('td').eq('10').text();
        	
        	//alert("Insertrar codigo consid transaccion = " + sidIncoming);
        	var conf = confirm("Está seguro que desea cerrar la transacción?");
        	
        	if(conf)
       		{
	        	$.ajax({
					url: "cerrarTransaccionAction",
					type: "POST",
					data: "sidIncomming="+sidIncoming,
					dataType: "json"
				}).done(function(data){
					var dev = data.cerrarTransacResp.split("~");
					alert(dev[1]);	
					$("#exportar").attr("disabled", true);
					$("#cerrartransaccion").attr("disabled", true);
					buscarPresentacionesIncomming();
					
					if(origenmodulo == "G")
					{
						$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});					
					}
						
				});
       		}  	
        });
		
	   </script>
	   
	   <script>
	   
	        /*******************************************************************************************************/
	        /* Funcion que busca las presentaciones de incoming que tengan movimietos en gestion de transacciones. */
	        /*******************************************************************************************************/
	        function buscarPresentacionesIncomming(){
	    	   //$("#tablaGestionCuenta").css("display","none");
	    	   
	        	var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
			    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
			    
			    var diaInicio = fechaInicioTmp[0];
			    var mesInicio = fechaInicioTmp[1];
			    var anoInicio = fechaInicioTmp[2];
			    
			    var diaTermino = fechaTerminoTmp[0];
			    var mesTermino  = fechaTerminoTmp[1];
			    var anoTermino = fechaTerminoTmp[2];
			    
			    var fechaInicio  = anoInicio + mesInicio + diaInicio;
			    var fechaTermino = anoTermino + mesTermino + diaTermino;
			    
			    //alert("Fecha Inicio : " + fechaInicio + " Fecha Termino : " + fechaTermino);
			    
				var parametros = "numeroTarjeta=" + $('#numerotarjeta').val();
					parametros = parametros + "&fechaInicio=" + fechaInicio;
				parametros = parametros + "&fechaTermino=" + fechaTermino;
				
			
				//alert("parametroos "+ parametros);

				$.ajax({
					url: "gestionPorCuentasPresentacionesAction",
					type: "POST",
					data:$("#form1").serialize()
					//dataType: "json",
					
					}).done(function(data){
							//console.log(data);
							$("#tablaRes").empty().append(data);
						
				});
	        
	        	
	        }
	        
	        function mostrarDetalleTransaccion(x){
	        
	          /*$("#microfil")
	          $("#codigoautorizacion")
	          $("#otrodato01")
	          $("#fechaefectiva")
	          $("#fechaproceso")
	          $("#otrodato02")
	          $("#binadquirente")
	          $("#leebanda")
	          $("#otrodato03")
	          $("#codigomonedatrx")
	          $("#rubrocomercio")
	          $("#otrodato04")
	          $("#operador")*/
	        
	        
	      	  var $radio 					= $("#tablaRes input[name='seleccionPRE']:checked").parent();
	      	  var operador                  = $radio.parent().parent().find('td').eq('11').text();
	      	  var microfilm                 = $radio.parent().parent().find('td').eq('12').text();
	      	  var codigoAutorizacion        = $radio.parent().parent().find('td').eq('13').text();
	      	  var otrodato1                 = $radio.parent().parent().find('td').eq('14').text();
	      	  var fechaEfectiva             = $radio.parent().parent().find('td').eq('15').text();
	      	  var fechaProceso              = $radio.parent().parent().find('td').eq('16').text();
	      	  var otrodato2                 = $radio.parent().parent().find('td').eq('17').text();
	      	  var binAdquiriente            = $radio.parent().parent().find('td').eq('18').text();
	      	  var leeBanda                  = $radio.parent().parent().find('td').eq('19').text();
	      	  var otrodato3                 = $radio.parent().parent().find('td').eq('20').text();
	      	  var rubroComercio             = $radio.parent().parent().find('td').eq('21').text();
	      	  var otrodato4                 = $radio.parent().parent().find('td').eq('22').text();
	      	  var codigomonedatrx           = $radio.parent().parent().find('td').eq('23').text();
	      	  
	      	  
	      	  $("#microfil").val(microfilm);
	          $("#codigoautorizacion").val(codigoAutorizacion);
	          $("#otrodato01").val(otrodato1);
	          $("#fechaefectiva").val(fechaEfectiva);
	          $("#fechaproceso").val(fechaProceso);
	          $("#otrodato02").val(otrodato2);
	          $("#binadquirente").val(binAdquiriente);
	          $("#leebanda").val(leeBanda);
	          $("#otrodato03").val(otrodato3);
	          $("#rubrocomercio").val(rubroComercio);
	          $("#otrodato04").val(otrodato4);
	          $("#operador").val(operador);
	          $("#codigomonedatrx").val(codigomonedatrx);
	      	  
	      	  
	      	  
	      	  //alert(operador);
	        }
	        
	   
	        
	        
	        /*****************************************************************/
	        /* Funcion que busca las transaciones de la cuenta seleccionada. */
	        /*****************************************************************/
			function mostrarGestionPorCuenta(x){
	        		
				var $radio 					= $("#tablaRes input[name='seleccionPRE']:checked").parent();
	    		var estado        			= $radio.parent().parent().find('td').eq('8').text(); 
	    	
				$("#exportar").attr("disabled", false);
				if(estado != 'Ok')
					$("#cerrartransaccion").attr("disabled", false);
				else
					$("#cerrartransaccion").attr("disabled", true);
					
				$("#divlistaGestionesCuentas").css("display","none");
				$("#divlistaGestionCargosAbonos").css("display","none");
				$("#divlistaGestionCargosAbonosNoExiste").css("display","none");
				$("#divlistaGestionesCuentasNoExiste").css("display","none");
				
	        	var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
			    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
			    
			    var diaInicio = fechaInicioTmp[0];
			    var mesInicio = fechaInicioTmp[1];
			    var anoInicio = fechaInicioTmp[2];
			    
			    var diaTermino = fechaTerminoTmp[0];
			    var mesTermino  = fechaTerminoTmp[1];
			    var anoTermino = fechaTerminoTmp[2];
			    
			    var fechaInicio  = anoInicio + mesInicio + diaInicio;
			    var fechaTermino = anoTermino + mesTermino + diaTermino;
			    
			    //alert("Fecha Inicio : " + fechaInicio + " Fecha Termino : " + fechaTermino);
			    
				var opcion                  = $radio.parent().parent().find('td').eq('0').text(); 
	    		var numerotarjeta        	= $radio.parent().parent().find('td').eq('1').text(); 
	    		var sidIncoming             = $radio.parent().parent().find('td').eq('10').text();
				var parametros = "numeroTarjeta=" +numerotarjeta;
				parametros = parametros + "&fechaInicio=" + fechaInicio;
				parametros = parametros + "&fechaTermino=" + fechaTermino;
				parametros = parametros + "&sidIncomming=" + sidIncoming;
				
				//alert(parametros);		
				
				$.ajax({
					url: "gestionPorCuentasAction",
					type: "POST",
					data: parametros,
					dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
			    	success: function(data){
			    	    
			    		mostrarListaGestionesPorCuentas(data.listaGestionPorCuentas);
			    		
					}
				});
				
				
				mostrarGestionCargoAbono();
				
				/******************************************************/
				/* Mostrar las cuentas de la transaccion seleccionada */
				/******************************************************/
				
				consultarClienteWS(numerotarjeta);
				
  			}
	        
			/*******************************************************************************/
	        /* Funcion que muestra en pantalla las transaciones de la cuenta seleccionada. */
	        /*******************************************************************************/
	   		function mostrarListaGestionesPorCuentas(listaGestionPorCuentas)
  			{
  				var stringHtml = "";
				var lsttransaccion = listaGestionPorCuentas.split("~");
				var esttrx = lsttransaccion[0].split("~");
				var codtrx = esttrx[0].split("|");
				console.log(lsttransaccion);
				if (codtrx[1]=="0")
				{
					var tablaOriginal = document.getElementById("tablaContenidoGestion");
					tablaOriginal.style.display= "none";
					
					// class="tablaContenido"
					stringHtml = "<table width='100%' class='table table-sm small' id='myTableGestion'>";
					
					stringHtml = stringHtml + "<tr class='text-center'>";
					stringHtml = stringHtml + "<th class='align-middle' class='align-middle' width='18%'>FECHA</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='23%'>ACCION</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='27%'>CAUSAL</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='5%'>RAZON</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='7%'>MONEDA</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='10%'>MONTO</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='10%'>USUARIO</th>";
					stringHtml = stringHtml + "</tr>";
					
    				for (var i=1; i<lsttransaccion.length-1; i++)
    				{
    		
    					var detalletransaccion = lsttransaccion[i].split("|");
    					
    					var sidtransaccion          = detalletransaccion[0];
    					var numerotarjeta           = detalletransaccion[1];
    					var fechatransac            = detalletransaccion[2];
    					var comercio                = detalletransaccion[3];
    					var pais                    = detalletransaccion[4];
    					var codigoaccion            = detalletransaccion[5];
    					var xkey                    = detalletransaccion[6];
    					var descripcionaccion       = detalletransaccion[7];
    					var montotransaccion        = detalletransaccion[8];
    					var montofacturacion        = detalletransaccion[9];
    					var montoconciliacion       = detalletransaccion[10];
    					var codigorazon             = detalletransaccion[11];
    					var descripcioncausalobjrec = detalletransaccion[12] + " " +detalletransaccion[19];
    					var descripcioncausalaccion = detalletransaccion[13] + " " +detalletransaccion[19];
    					var referencia              = detalletransaccion[14];
    					var codigomonedatrx         = detalletransaccion[15];
    					var estadotrx               = detalletransaccion[16];
    					var glosaescrita            = detalletransaccion[17];
    					var usuario                 = detalletransaccion[18];
    					
    					//alert(detalletransaccion[19]);
    					
    					stringHtml = stringHtml + "<tr class='text-center'>";
    					stringHtml = stringHtml + "<td class='align-middle' width='18%' align='center'>" + fechatransac + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='20%'>" + descripcionaccion + "</td>";
    					
    					if (xkey=="OBJ" || xkey=="REC"){
    						stringHtml = stringHtml + "<td class='align-middle' width='30%'>" + descripcioncausalobjrec + "</td>";	
    					}else{
    						stringHtml = stringHtml + "<td class='align-middle' width='30%'>" + descripcioncausalaccion + "</td>";
    					}
    					
    					stringHtml = stringHtml + "<td class='align-middle' width='5%' align='center'>" + codigorazon + "</td>";
    					
    					//stringHtml = stringHtml + "<td class='align-middle' width='18%' align='center'>" + referencia + "</td>";
    					
    					stringHtml = stringHtml + "<td class='align-middle' width='7%' align='center'>" + codigomonedatrx + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='10%' align='center'>" + formato_numero(colocaComaNumero(montotransaccion)) +  "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='10%' align='center'>" + usuario +  "</td>";
    					
    					stringHtml = stringHtml + "</tr>";
           	
    				}//Fin For
    				
    				stringHtml = stringHtml + "</table>";
    				$('#divlistaGestionesCuentas').html(stringHtml);
    				$("#divlistaGestionesCuentas").css("display","");
    				
				}
				else{
					var mensaje = lsttransaccion[1].split("|");
					
					if (mensaje[1]=="No existe informacion de gestion de transacciones con los parametros seleccionados."){
						$("#divlistaGestionesCuentasNoExiste").css("display","");
					}else{
						alert(mensaje[1]);	
					}
				}
  			}
	   		
	   		/******************************************************************************/
	        /* Funcion que busca los cargos y abonos realizados a la cuenta seleccionada. */
	        /******************************************************************************/
	   		function mostrarGestionCargoAbono(){
	   			
	        	var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
			    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
			    
			    var diaInicio = fechaInicioTmp[0];
			    var mesInicio = fechaInicioTmp[1];
			    var anoInicio = fechaInicioTmp[2];
			    
			    var diaTermino = fechaTerminoTmp[0];
			    var mesTermino  = fechaTerminoTmp[1];
			    var anoTermino = fechaTerminoTmp[2];
			    
			    var fechaInicio  = anoInicio + mesInicio + diaInicio;
			    var fechaTermino = anoTermino + mesTermino + diaTermino;
			    
			    //alert("Fecha Inicio : " + fechaInicio + " Fecha Termino : " + fechaTermino);
			    
				var $radio 					= $("#tablaRes input[name='seleccionPRE']:checked").parent();
	    		var sidIncoming             = $radio.parent().parent().find('td').eq('10').text();
	    		
				var parametros = "&fechaInicio=" + fechaInicio;
				parametros = parametros + "&fechaTermino=" + fechaTermino;
				parametros = parametros + "&sidIncomming=" + sidIncoming;
				
				//alert(parametros);		
				
				$.ajax({
					url: "gestionPorCuentasCargoAbonoAction",
					type: "POST",
					data: parametros,
					dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
			    	success: function(data){
			    	    
			    		mostrarListaGestionesCargoAbono(data.listaGestionCargoAbono);
			    		
					}
				});
				
	   		}
	   		
	   		/*******************************************************************************/
	        /* Funcion que muestra en pantalla las transaciones de la cuenta seleccionada. */
	        /*******************************************************************************/
	   		function mostrarListaGestionesCargoAbono(listaGestionCargoAbono){
	   			
	   			var stringHtml = "";
				var lsttransaccion = listaGestionCargoAbono.split("~");
				var esttrx = lsttransaccion[0].split("~");
				var codtrx = esttrx[0].split("|");
				
				if (codtrx[1]=="0")
				{
                 
					var tablaOriginal = document.getElementById("tablaContenidoGestionCargoAbono");
					tablaOriginal.style.display= "none";
					
					// class="tablaContenido" 
					stringHtml = "<table width='100%' class='table table-sm small' id='myTableGestionCargoAbono'>";
					
					stringHtml = stringHtml + "<tr class='text-center'>";
					stringHtml = stringHtml + "<th class='align-middle' width='15%'>FECHA</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='7%'>MONEDA</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='13%'>MONTO</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='23%'>TIPO</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='27%'>DESCRIPCION</th>";
					stringHtml = stringHtml + "<th class='align-middle' width='10%'>USUARIO</th>";
					stringHtml = stringHtml + "</tr>";
					
					
    				for (var i=1; i<lsttransaccion.length-1; i++)
    				{
    		
    					var detallecargoabono = lsttransaccion[i].split("|");
    					
    					var sidtransaccion          = detallecargoabono[0];
    					var fechacargoabono         = detallecargoabono[1];
    					var codigomoneda            = detallecargoabono[2];
    					var montocargoabono         = detallecargoabono[3];
    					var sidcargoabono           = detallecargoabono[4];
    					var tipocargoabono          = detallecargoabono[5];
    					var descripcion             = detallecargoabono[6];
    					var usuario                 = detallecargoabono[7];
    					
    					stringHtml = stringHtml + "<tr class='text-center'>";
    					stringHtml = stringHtml + "<td class='align-middle' width='15%' align='center'>" + fechacargoabono + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='7%' align='center'>" + codigomoneda + "</td>";
    					// align="right"
    					stringHtml = stringHtml + "<td class='align-middle' width='13%' >" + formato_numero(colocaComaNumero(montocargoabono)) +  "</td>";

    					var tipo="ABONO";
    					if (tipocargoabono=="C"){
    						tipo = "CARGO";
    					}
    					
    					stringHtml = stringHtml + "<td class='align-middle' width='23%'>" + tipo + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='27%'>" + descripcion + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='10%'>" + usuario + "</td>";
    					stringHtml = stringHtml + "</tr>";
           	
    				}//Fin For
    				
    				stringHtml = stringHtml + "</table>";
    				$('#divlistaGestionCargosAbonos').html(stringHtml);
    				$("#divlistaGestionCargosAbonos").css("display","");
    				
    				
				}
				else{
					var mensaje = lsttransaccion[1].split("|");
					
					if (mensaje[1]=="No existe informacion de gestion de cargos y abonos con los parametros seleccionados."){
						$("#divlistaGestionCargosAbonosNoExiste").css("display","");
					}else{
						alert(mensaje[1]);	
					}
					
				}
	   			
	   		}
	   		
	   		
			function consultarClienteWS(numeroTarjeta){
  				
  				// var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
  				var parametros = "numeroTarjeta=" +numeroTarjeta;
  				//var parametros = "numeroTarjeta=4946110100055009";

				$.ajax({
					url: "llamadaWS",
					type: "POST",
					data: parametros,
					dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
		    		success: function(data){

		    			var lsttransaccion = data.cadena.split("~");
						var esttrx = lsttransaccion[0].split("~");
						var codtrx = esttrx[0].split("|");

						if (codtrx[1]=="0")
						{
								var datoscliente = lsttransaccion[1].split("|");
	
								var nombrecliente   = datoscliente[0];
		    					var rutcliente      = datoscliente[1];
		    					var numerocuenta    = datoscliente[2];
		    					var cupocliente     = datoscliente[3];
		    					var fechaexpiracion = datoscliente[4];
		    					var estadocliente   = datoscliente[5];

								$('#nomcliente').val(nombrecliente);
								$("#rutcliente").val(rutcliente);
					    		$('#numerocuenta').val(numerocuenta);
					    		$('#cupo').val(formato_numero(cupocliente));
					    		$('#fechaexpiracion').val(fechaexpiracion);
					    		$('#estado').val(estadocliente);
			    			
						}else{
								var mensaje = lsttransaccion[1].split("|");
								
								if (mensaje=="Numero de tarjeta erroneo"){
									alert("No existen los datos de la cuenta del cliente para la tarjeta consultada...");
								}
								
								$('#nomcliente').val("");
					    		$('#rutcliente').val("");
					    		$('#numerocuenta').val("");
					    		$('#cupo').val("");
					    		$('#fechaexpiracion').val("");
					    		$('#estado').val("");
						}
		    			
					}
				});
  			}
	   
	   </script>
	   
		
   </head>
   
   <body>
       <form id="form1">
      <!-- <div class="titulo"> -->
      <div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
       		GESTIONES DIARIAS
      </div>
      
      <!-- <br style="line-height: 5px" />  -->
      <hr/>
       
      <div>
      		<!-- <table width="100%" class="contenedorInput">  -->
		   	<table width="100%" class="table my-0 table-sm col-12 mx-auto">
               <tr class='text-center'>
           			<td class="align-middle" class="text-right">
           				<label class="my-0">Nro. TC :</label>
           			</td>
           			
           			<td class="align-middle">
      					<input type="text" name="numerotarjeta" id="numerotarjeta" maxlength="16" 
          					class="numericOnly form-control form-control-sm px-0 py-0 mx-auto w-75"/>
       				</td>
       				
           			<td class="align-middle">
           				<label class="my-0 text-right">DESDE :</label>
           			</td>
           			<td class="align-middle">
           				<input type="text" name="fechaInicio" id="fechaInicio" size="10" maxlength="10" 
           					class="campoObligatorio fecha form-control form-control-sm col-12 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
           			</td>
           			
           			<td class="align-middle">
           				<label class="my-0 text-right">HASTA :</label>
           			</td>
           			
           			<td class="align-middle">
           				<input type="text" name="fechaTermino" id="fechaTermino" size="10" maxlength="10" 
           					class="campoObligatorio fecha form-control form-control-sm col-12 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
           				
      				</td>
           			<td colspan="4" class="align-middle">
           				<input type="button"  id="buscarPresentaciones" name="buscarPresentaciones" 
           					value="Buscar" class="btn btn-primary" />
           			</td>
              			
       			</tr>
      		</table>
      </div>
      
     
      <!--  <br style="line-height:5px"/>  -->
      
     <!-- class="subTitulo" -->
	<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
			font-weight-bold ">
		Presentaciones Con Movimientos De Transacciones
     </div>
      
	 <div class="cajaContenido">
    	<div class="m-0 p-0">
		<!-- <table width="100%" class="tablaContenido" id="tablaGestionCuenta"> -->
   		<table width="100%" class="table table-sm small" id="tablaGestionCuenta">
			<tr class="text-center">
				<th class='align-middle' style="width: 29px;">SEL</th>
				<th class='align-middle' style="width: 104px;">NRO TC</th>
				<th class='align-middle' style="width: 96.5px;">FECHA TRANSACCION</th>
				<th class='align-middle' style="width: 126.5px;">COMERCIO</th>
				<th class='align-middle' style="width: 29px;">PAIS</th>
				<th class='align-middle' style="width: 126.5px;">TIPO TRANSACCION</th>
				<th class='align-middle' style="width: 59px;">MONTO ORIGINAL</th>
				<th class='align-middle' style="width: 59px;">MONTO</th>
				<th class='align-middle' style="width: 59px;">ESTATUS</th>
				<th class='align-middle' style="width: 36.5px;">PATPASS</th>
			</tr>
	    </table>
	   </div>
	    <div id="tablaRes">
	    </div>
        
    </div>
    
      
     <!--  <br style="line-height:5px"/> -->
     
     <hr size="2px">
     
     <!-- class="subTitulo" -->
		<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
			font-weight-bold">
			Datos De La Cuenta Del Cliente	    
	 </div>
	    
	 <div class="mb-0">
	    
    	<!-- class="contenedorInput  -->
		<table width="100%" class="table table-sm small">
	    	<tr>
    			<td>
    				Cliente :
				</td>
    			<td>
    				<input type="text" name="nomcliente" id="nomcliente" size="50" 
    					class="campoSalida form-control form-control-sm" />
   				</td>
    			<td>
    				Rut :
  				</td>
    			<td>
    				<input type="text" name="rutcliente" id="rutcliente" 
    					class="campoSalida form-control form-control-sm"/>
				</td>
    		</tr>
    		
    		<tr>
    			<td>
    				N° De Cuenta :
   				</td>
    			<td>
   					<input type="text" class="campoSalida form-control form-control-sm" 
   						name="numerocuenta" id="numerocuenta"/>
   				</td>
    			<td>
    				Cupo :
   				</td>
    			<td>
    				<input type="text" class="campoSalida form-control form-control-sm" 
    					name="cupo" id="cupo"/>
   				</td>
    		</tr>
    		
    		<tr>
    			<td>
    				Fecha Exp.:
   				</td>
    			<td>
    				<input type="text" class="campoSalida form-control form-control-sm" 
    					name="fechaexpiracion" id="fechaexpiracion" />
				</td>
    			<td>
    				Estado :
   				</td>
    			<td>
    				<input type="text" class="campoSalida form-control form-control-sm" 
    					name="estado" id="estado" />
   				</td>
    		</tr>
    
	    </table>
	
	  </div>
      
      <!-- <br style="line-height:5px"/>  -->
      
      <!-- class="subTitulo" -->
		<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
			font-weight-bold">
      		       Historial De Gestiones
      </div>
      
      <div style="width:100%;overflow-x:hidden;overflow-y:auto;max-height:200px;">
			 <div style="width:100%;margin:0px;padding:0px;">
      
      		<!--  class="tablaContenido" -->
      		<table width="100%" class="table table-sm small" id="tablaContenidoGestion">
       			
       				<tr class="text-center align-middle">
       					<th class='align-middle' width='18%'>FECHA</th>
       					<th class='align-middle' width='18%'>ACCI&Oacute;N</th>
       					<th class='align-middle' width='27%'>CAUSAL</th>
       					<th class='align-middle' width='5%'>RAZ&Oacute;N</th>
       					<th class='align-middle' width='7%'>MONEDA</th>
       					<th class='align-middle' width='10%'>MONTO</th>
       					<th class='align-middle' width='10%'>USUARIO</th>       					
       				</tr>
       			
       		</table>
       		
       		<div id="divlistaGestionesCuentas">

	        </div>
      
      		<div id="divlistaGestionesCuentasNoExiste" style="display:none;">
      
       				<br style="line-height:5px"/>
       				<!-- class="subTitulo" -->
       			 	<div class="text-center alert alert-info font-weight-bold" role="alert">
      					No Existe Informaci&oacute;n de historial de transacciones para la cuenta seleccionada.
      				</div>
	        </div>
	        
      </div>
      </div>
      
      <!-- <br style="line-height:5px"/>  -->
      
      <div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
			font-weight-bold">
      		Historial De Cargos y Abonos
      </div>
      
      <div>
      		<!-- class="tablaContenido" -->
      		<table width="100%" class="table table-sm small" id="tablaContenidoGestionCargoAbono">
       				
       				<tr class="text-center align-middle">
       					<th class='align-middle' width='15%'>FECHA</th>
       					<th class='align-middle' width='7%'>MONEDA</th>
       					<th class='align-middle' width='13%'>MONTO</th>
       					<th class='align-middle' width='23%'>TIPO</th>
       					<th class='align-middle' width='27%'>DESCRIPCI&Oacute;N</th>
       					<th width='10%'>USUARIO</th>       					
       					
       				</tr>
       				
       		</table>
       		
       		<div id="divlistaGestionCargosAbonos">
	        </div>
      
       		<div id="divlistaGestionCargosAbonosNoExiste" style="display:none;">
      
       				<!-- <br style="line-height:5px"/>  -->
       			 	<!-- <div class="subTitulo">  -->
       			 	<div class='text-center alert alert-info font-weight-bold' role='alert'>
      					No Existe Informaci&oacute;n de historial de cargos y abonos para la cuenta seleccionada.
      				</div>
	        </div>
      
      
      </div>
      <!-- <br style="line-height:5px"/>  -->
      	
      	<!-- <div class="subTitulo"> -->
      	<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
			font-weight-bold ">
  			Datos De La Transacci&oacute;n Seleccionada
  		</div>
  		
		<div>
			<!-- class="contenedorInput" -->
			<table width="100%" class="table table-sm small">
	    		<tr>
	    			<td>Microfilm :</td>
	    			<td><input type="text" name="microfil" id="microfil" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Cod. Autorizac. :</td>
	    			<td><input type="text" name="codigoautorizacion" id="codigoautorizacion" class="campoSalida form-control form-control-sm" /></td>
	    			<td>Cod.Proces:</td>
	    			<td><input type="text" name="otrodato01" id="otrodato01" class="campoSalida form-control form-control-sm"/></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Fecha Efectiva :</td>
	    			<td><input type="text" name="fechaefectiva" id="fechaefectiva" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Fecha Proceso :</td>
	    			<td><input type="text" name="fechaproceso" id="fechaproceso" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Conv.Conc. :</td>
	    			<td><input type="text" name="otrodato02" id="otrodato02" class="campoSalida form-control form-control-sm"/></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Bin Adquiriente :</td>
	    			<td><input type="text" name="binadquirente" id="binadquirente" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Lee Banda :</td>
	    			<td><input type="text" name="leebanda" id="leebanda" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Conv.Fact. :</td>
	    			<td><input type="text" name="otrodao03" id="otrodato03" class="campoSalida form-control form-control-sm"/></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>C&oacute;digo Moneda Trx. :</td>
	    			<td><input type="text" name="codigomonedatrx" id="codigomonedatrx" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Rubro Del Comercio :</td>
	    			<td><input type="text" name="rubrocomercio" id="rubrocomercio" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Pto. Servicio:</td>
	    			<td><input type="text" name="otrodao04" id="otrodato04" class="campoSalida form-control form-control-sm"/></td>
	    		</tr>
	    		<tr>
	    			<td>Operador:</td>
	    			<td class="border-right-0"><input type="text" name="operador" id="operador" class="campoSalida form-control form-control-sm" size="40"></td>
	    			<td colspan="4" class="border-left-0"></td>
	    		<!-- 
	    			<td></td>
	    			<td></td>
	    			<td></td>
	    			<td></td>
	    		 -->
	    		</tr>
	    	</table>
      </div>
      
      <!-- <br style="line-height:5px"/>  -->
      
			<!-- class="contenedorInput" -->
      		<table  width="100%" class="table table-sm small table-borderless">
       	       	<tr class="text-center">
       				<td align="center" width="33%" class="align-middle">
       					<input type="button"  value="Exportar Detalle Transacci&oacute;n" name="exportar" id="exportar" class="btn btn-primary mr-3 d-inline" disabled/>
       					
       					<%
       					try{
       					
						if(usuarioLog!= null && usuarioLog.getActualizacion() == 1){ %>
       						<input type="button"  value="Cerrar Transacci&oacute;n" name="cerrartransaccion" id="cerrartransaccion" class="btn btn-primary mr-3 d-inline" disabled/>
       					<% }
       					}catch(Exception e){
       						e.printStackTrace();
       					}
       					%>
       					
       					<input type="button"  value="Cancelar" name="cancelarGestion" id="cancelarGestion" class="btn btn-primary mr-3 d-inline" />
       				</td>
       				<%-- <td align="center" width="33%" class="align-middle">
       					<%
       					try{
       					
						if(usuarioLog!= null && usuarioLog.getActualizacion() == 1){ %>
       						<input type="button"  value="Cerrar Transacci&oacute;n" name="cerrartransaccion" id="cerrartransaccion" class="btn btn-primary" disabled/>
       					<% }
       					}catch(Exception e){
       						e.printStackTrace();
       					}
       					%>
					</td>
					<td align="center" width="33%" class="align-middle">
       				    <input type="button"  value="Cancelar" name="cancelarGestion" id="cancelarGestion" class="btn btn-primary" />
       				</td> --%>
       			</tr>
       		</table>
    

</form>
   </body>
</html>