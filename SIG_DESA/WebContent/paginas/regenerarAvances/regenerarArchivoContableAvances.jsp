<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.pagination {
  display: inline-block;
}

.pagination a {
  color: black;
  float: left;
  padding: 4px 4px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid #ddd;
}

.pagination a.active {
  background-color: #D3D5D5;
  color: white;
}

</style>
<title>Regenerar Interfaz Contable de Avances</title>
<script type="text/javascript">
<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");%>

	//CARGAR FECHAS INICIO DIA ANTERIOR Y HASTA CON ACTUAL
	var today = new Date();

		var dd = today.getDate();
		var mm = today.getMonth() + 1;//January is 0! 
		var yyyy = today.getFullYear();

		var dia = dd;
		var mes = mm;
		var ano = yyyy;

		if (dd < 10) {
			dd = '0' + dd;
		}

		if (mm < 10) {
			mm = '0' + mm;
		}
		var fechaActual = dd + "/" + mm + "/" + yyyy;

		if (dia == 1) {

			if (mes == 1) {
				mes = 12;
				dia = 31;
			} else if (mes == 2) {
				mes = 1;
				dia = 31;
			} else if (mes == 3) {
				mes = 2;
				dia = 28;
				if ((ano % 4 == 0) && (ano % 100 != 0) || (ano % 400 == 0)) {
					mes = 2;
					dia = 29;
				}
			} else if (mes == 4) {
				mes = 3;
				dia = 31;
			} else if (mes == 5) {
				mes = 4;
				dia = 30;
			} else if (mes == 6) {
				mes = 5;
				dia = 31;
			} else if (mes == 7) {
				mes = 6;
				dia = 30;
			} else if (mes == 8) {
				mes = 7;
				dia = 31;
			} else if (mes == 9) {
				mes = 8;
				dia = 31;
			} else if (mes == 10) {
				mes = 9;
				dia = 30;
			} else if (mes == 11) {
				mes = 10;
				dia = 31;
			} else if (mes == 12) {
				mes = 11;
				dia = 30;
			}
		} else {
			dia = dia - 1; //Dia anterior
		}

		var diaPaso = "";
		var mesPaso = "";
		var anoPaso = "";

		if (dia < 10) {
			diaPaso = "0" + dia;
		} else {
			diaPaso = dia;
		}

		if (mes < 10) {
			mesPaso = "0" + mes;
		} else {
			mesPaso = mes;
		}
		anoPaso = ano;

		var fechaAnterior = diaPaso + "/" + mesPaso + "/" + anoPaso;
		$('#fechaProceso').val(fechaAnterior);
		$('#fechaFin').val(fechaActual);
	/////////////////////////////////////////////
	

$("#buscar")
			.click(
					function() {

							if ($('#fechaProceso').val().length == 0) {
							alert("Debe ingresar fecha desde para realizar la b\u00fasqueda...");
							$("#fechaProceso").focus();
							return;
						}
						
						if ($('#fechaFin').val().length == 0) {
							alert("Debe ingresar fecha hasta para realizar la b\u00fasqueda...");
							$("#fechaFin").focus();
							return;
						}

						if (validarFecha($('#fechaProceso').val()) == false) {
							alert("La fecha desde ingresada esta incorrecta, verificar...");
							$("#fechaProceso").focus();
							return;
						}
												
						if (validarFecha($('#fechaFin').val()) == false) {
							alert("La fecha hasta ingresada esta incorrecta, verificar...");
							$("#fechaFin").focus();
							return;
						}
						
						var fechaInicio = convertirFecha($('#fechaProceso').val());
						var fechaTermino = convertirFecha($('#fechaFin').val());
						
						if (fechaInicio > fechaTermino) {
							alert("La fecha de inicio es mayor a la fecha de t\u00E9rmino, verificar...");
							$("#fechaInicio").focus();
							return;
						}
						
						var estado	= $('#estado').val();						

						var fechaInicioTmp = $('#fechaProceso').val().split("/");;
						var fechaTerminoTmp = $('#fechaFin').val().split("/");
												
						var diaInicio = fechaInicioTmp[0];
	  				    var mesInicio = fechaInicioTmp[1];
	  				    var anoInicio = fechaInicioTmp[2];
	  				    
	  				    var diaTermino = fechaTerminoTmp[0];
	  				    var mesTermino  = fechaTerminoTmp[1];
	  				    var anoTermino = fechaTerminoTmp[2];
	  				    
	  				    var fechaInicio  = anoInicio + mesInicio + diaInicio;
	  				    var fechaTermino = anoTermino + mesTermino + diaTermino; 
	  				    
	  				    
	  				    $.ajax({
							url : "RegenerarArchivoContableAvancesAction",
							type : "POST",
							data : {
								fechaInicio : fechaInicio,
								fechaFinal: fechaTermino,
								estado: estado,
							},
							dataType : "json",
							error : function(XMLHttpRequest, textStatus,
									errorThrown) {
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
							},
							success : function(data) {
							//alert(data.listaFinal.length);
								mostrarTabla(data);
							}
						});
	});
	
	function mostrarTabla(data) {
	
	var stringHtmlDetalle = "";	
	var cantidadFiltro = data.listaFinal.length;
		
		// DETALLE
		if (cantidadFiltro == 0) {
			$('#divLogCargaTrxExport').css("display", "none");
			// class='tablaContenido'
			stringHtmlDetalle = "<table width='100%' class='table table-sm' id='myTableAvancesDetalle'>";
			// stringHtmlDetalle += "<td width='100%' aling='center'> <center>NO SE HAN ENCONTRADO RESULTADOS.</center></td>";
			stringHtmlDetalle += "<td class='align-middle alert alert-info text-center font-weight-bold' role='alert' width='100%'>NO SE HAN ENCONTRADO RESULTADOS.</td>";
			stringHtmlDetalle += "</table>";
		} else {
				
			for ( var i = 0; i < cantidadFiltro; i++) {
				var sid = data.listaFinal[i].id;
				var fechaTs = data.listaFinal[i].fechaTs
				var fileName = data.listaFinal[i].fileName;
				var estado = data.listaFinal[i].estado;
				var fecha = data.listaFinal[i].fecha;
				var estadoName = "";
				
				var subFileName = fileName.substr(3,12);
				var fileNameCtr = "AR_"+subFileName;
				var archivos = fileName + '/'+ fileNameCtr;
				
				 var fileName2 = fileName.substr(3,8);
				
				if(estado == "0"){
					estadoName = "Creado";
				}else if(estado == "-1"){
							estadoName = "Error al porcesar";
				}else if(estado == "1"){
							estadoName = "Cargado OW";
				}else if(estado == "2"){
							estadoName = "Regenerado";
				}else if(estado == "3"){
							estadoName = "Regenerado";
				}
				
				if (i == 0) {
					// class="tablaContenido" width='810px'
					stringHtmlDetalle = "<table class='table table-sm small mx-auto' id='tbl-log-no-auth'>";
					// style='background-color: #B9B6B6; color: #0F0E0E;'
					stringHtmlDetalle += "<thead>";
					stringHtmlDetalle += "<tr class='text-center'>";
					// width='10px'
					stringHtmlDetalle += "<th class='align-middle' >FECHA CONTABLE";
					stringHtmlDetalle += "</th>";
					// width='10px'
					stringHtmlDetalle += "<th class='align-middle' >NOMBRE ARCHIVOS";
					stringHtmlDetalle += "</th>";
					// width='10px'
					stringHtmlDetalle += "<th class='align-middle' >Fecha/Hora Creaci&oacute;n";
					stringHtmlDetalle += "</th>";
					// width='5px'
					stringHtmlDetalle += "<th class='align-middle' >ESTADO";
					stringHtmlDetalle += "</th>";
					// width='10px'
					stringHtmlDetalle += "<th class='align-middle' >OPCIONES";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "</thead>";
				}
				
				stringHtmlDetalle += "<tr class='text-center'>";
				// width='10px' 
				stringHtmlDetalle += "<td class='align-middle' align='center'>" + fecha + "</td>";
				// width='10px
				stringHtmlDetalle += "<td class='align-middle' align='center'>" + archivos +"<input type='hidden' id='filename_id' value='"+fileName+"'/></td>";
				// width='10px
				stringHtmlDetalle += "<td class='align-middle' align='center'>" + fechaTs + "</td>";
				// width='10px
				stringHtmlDetalle += "<td class='align-middle' align='center'>" + estadoName + "</td>";
				// width='10px
				stringHtmlDetalle += "<td class='align-middle' align='center'><span title='Historico' class='mr-1'><input type='image' style='width:18px;height:18px;' src='img/paper.png' onClick='verHistoria("+sid+","+fileName2+")' /></span>";
				
				if(estado != "2" && estado != "3"){
					stringHtmlDetalle += "<span title='Regenerar Interfaz'><input type='image' style='width:18px;height:18px;' src='img/refresh.png' onClick='regenerar("+sid+","+fileName2+")'/></span>";
				}
				
				stringHtmlDetalle += "</td>"; 
				stringHtmlDetalle += "</tr>";
				
					// fin de ciclo
				if ((i + 1) == cantidadFiltro) {
					stringHtmlDetalle += "</table>";
				}
			}
			
		}
		
		$('#divLogCargaTitulo').css("display", "");
		$('#divLogCarga').html(stringHtmlDetalle);
		$('#divLogCarga').css("display", "");
		if (cantidadFiltro > 0) {
			pager(cantidadFiltro);
		}else{
			$("#nav").hide();
		}
	}
	
	function pager(rowsTotal) {
		$('#table-header-num').html(
				// '<div id="nav"></div><div  class="pagination" id="espacioPaginacion">');
				'<ul class="pagination pagination-sm justify-content-center" id="nav"></ul><div id="espacioPaginacion">')
		var rowsShown = 10;
		var numPages = rowsTotal / rowsShown;
		$('#nav').empty();
		for (i = 0; i < numPages; i++) {
			var pageNum = i + 1;
			$('#nav')
					.append(
							/* '<a href="#" style="font-size: 12px;align:center" class="enlaceboton" rel="' + i +'">'
									+ (i + 1) + '</a> '); */
							
							'<li class="page-item"><a href="#sin_mover" style="font-size: 12px;align:center" class="enlaceboton page-link" rel="' + i +'">'
									+ (i + 1) + '</a></li>')
		}
		$('#tbl-log-no-auth tbody tr').hide();
		$('#tbl-log-no-auth tbody tr').slice(0, rowsShown).show();
		// $('#nav a:first').addClass('active');
		$('#nav li:first').addClass('active');
		
		// $('#nav a').bind(
		$('#nav li').bind(
				'click',
				function() {

					// $('#nav a').removeClass('active');
					$('#nav li').removeClass('active');
					// $('#nav a').css('background-color', '');
					$(this).addClass('active');
					//$(this).css('background-color', 'gray');
					// var currPage = $(this).attr('rel');
					var currPage = $(this).children('a').attr('rel');
					var startItem = currPage * rowsShown;
					var endItem = startItem + rowsShown;
					$('#tbl-log-no-auth tbody tr').css('opacity', '0.0').hide()
							.slice(startItem, endItem).css('display',
									'table-row').animate({
								opacity : 1
							}, 300);
				});
	}
	
	function verHistoria(sid,fileName){
	
		$.ajax(
					{

						url : "regenerarArchivoContableAVTActionModalVer",
						type : 'POST',
						data: {'sid': sid,
								'fileName':fileName, 
             					},

						}).done(function(data) {
								$("#popup").css("height", $(document).height());
								
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'big');
			});

	}
	
		function regenerar(sid,fileName){
						
		$.ajax(
					{

						url : "regenerarArchivoContableAVTActionModal",
						type : 'POST',
						data: {'sid': sid, 
							   'fileName': fileName,
             				  },

						}).done(function(data) {
								$("#popup").css("height", $(document).height());
								
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'big');
			});

	}

</script>
</head>
<body>
<!-- <div class="titulo">  -->
<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
	REGENERAR INTERFAZ CONTABLE DE AVANCES CON TRANSFERENCIA
</div>
<hr>
<!-- <br style="line-height: 5px" />  -->

	<div>
		<!-- class="contenedorInput" -->
		<table width="75%" class="table table-sm">
			<tr class="text-right">
				<td class="align-middle">Fecha Desde:</td>
				<!-- placeholder="(dd/mm/aaaa) size="10" -->
				<td class="text-center align-middle"><input type="text" name="fechaInicio" id="fechaProceso" "
					maxlength="10" onKeypress="ingresoFecha();"
					class="campoObligatorio fecha form-control form-control-sm mx-auto text-center col-10 datepicker" placeholder="DD/MM/AAAA" />
				</td>
					
				<td class="align-middle">Fecha Hasta:</td>
				<!-- placeholder="(dd/mm/aaaa)" size="10" -->
				<td class="text-center align-middle"><input type="text" name="fechaFin" id="fechaFin" 
					maxlength="10" onKeypress="ingresoFecha();"
					class="campoObligatorio fecha form-control form-control-sm mx-auto text-center col-10 datepicker" placeholder="DD/MM/AAAA" />
				</td>
								
				<td class="align-middle">Estado:</td>
				<!-- style="width:120px" -->
				<td class="align-middle"> <select id="estado" name="estado" class="custom-select">
						<option value=99>-Todos-</option>
						<option value="0">Creado</option>
						<option value="-1">Error al Procesar</option>
						<option value="1">Cargado OW</option>
						<option value="2">Regenerado</option>
					</select>
				</td>
				<td class="align-middle">
					<input type="button" id="buscar" value="Buscar" class="btn btn-primary">
				</td>
			</tr>
		</table>
	</div>
	
	<!-- <br />  -->
	
	<div id="popup" style="display: none;">
		
	</div>
	
	<div id="popUpContenido" class="content-popup" style="display: none;"></div>
	
	<div id="divLogCargaTitulo" style="display: none;">
		<div class="col-6 mx-auto my-4 small text-center font-weight-bold">
			<!-- clas="contenedorInput" -->
			<table width="100%" class="table table-sm">
				<tr>
					<td style="border: none !important" align="center">ARCHIVOS CONTABLES DE AVANCES CON TRANSFERENCIA</td>
				</tr>
			</table>
		</div>
		
		<!-- <br /> -->
	</div>
		<div>
		<div id="divLogCarga"></div>
	</div>
	
	<!-- <center><b><div id="table-header-num" class="pagination" style="border: 1px solid #ddd;"></b></div>  -->
	<div id="table-header-num" class="mb-2 font-weight-bold text-center" ></div>
		
	<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>
	<script type="text/javascript" src="js/utils.js"></script>
	<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
	
	<script>
		$(".datepicker").datepicker();
	</script>
</body>
</html>