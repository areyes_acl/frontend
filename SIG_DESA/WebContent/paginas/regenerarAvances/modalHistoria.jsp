<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Historia</title>
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	<script type="text/javascript" src="js/utils.js"></script>
	
	
	<script type="text/javascript">
 	  $(document).ready(function(){        	
        	$("#id_sid").val( "<%=request.getParameter("sid")%>");	
        	        					
		});
		
	$("#salir").click(function(){
        	
       	$('#popup').css({display: 'none'});
       	$('#popUpContenido').empty().css({display:'none'});
        	
    });
	</script>
	
	<style type="text/css">
 	#tableid .tr:first-child {
		  background-color: #A7A6A6;
		}
 	</style>
 	
</head>
<body>

<input type="hidden" id="id_sid" name="sid" />
		<!-- <div class="titulo">  -->
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
			VISUALIZAR HISTORIA DE ARCHIVO CONTABLE
		</div>
		<hr>
		<!-- <br style="line-height:5px"/>  -->
		
			  <div>
			  	<!-- class="contenedorInput" -->
	      		<table width="100%" class="table table-sm small col-8 mx-auto">
	       	       		<tr class="text-center">
	       					<td style="border: none !important" class="align-middle font-weight-bold py-0" align="center">
	       						Hist&oacute;rico de  <font style="font-weight: bold;gg">AD_<%=request.getParameter("fileName")%>.TXT</font>
	       					</td>
	       				</tr>
	      		</table>
     	 </div>
     	 
     	 <!-- <br style="line-height:5px"/>  -->
     	 
     	 <!-- style="border:1px solid black;width:800px;height:150px;overflow:scroll;overflow-y:scroll;overflow-x:hidden;" -->
     	 <div >
		
		<!-- class="tablaContenido" -->
		<table border="1" width="100%" id="tableid" class="table table-sm small col-8 mx-auto" style="overflow-y:scroll;overflow-x:hidden;">
		<!-- style="background-color: #B9B6B6; color: #0F0E0E;" -->
		 <thead >
		<tr class="text-center">
			<th class="align-middle">Fecha</th> 
			<th class="align-middle">Usuario</th>
			<th class="align-middle">Motivo</th>
		</tr>
		</thead>
		<tbody>
							
			<s:iterator id="data" value="%{#request.listaFinalHContable}" >
			 <s:if test="#data.usuario != ''">		
				<!-- class="tr" -->
				<tr class="text-cemter">
					<td class="align-middle" align="center"><s:property value="fechaRegistro"/></td>
					<!-- align="left" -->
					<td class="align-middle" align="center"><s:property value="usuario"/></td>
					<!-- align="left" -->
					<td class="align-middle" align="center"><s:property value="motivo"/></td>		
				</tr>
			</s:if>
		</s:iterator>
		
		</tbody>
		</table>	
		</div>
		
		<!-- <br style="line-height:5px"/>  -->
		
	      <div>
	      		<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm table-borderless">
	       	       		<tr>
	       					<td align="center">
	       				    	<input type="button"  value="Cancelar" id="salir" name="salirpopup" class="btn btn-primary">
	       					</td>
	       				</tr>
	      		</table>
     	 </div>
</body>
</html>