<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
textarea {
    /* Para alinear campos de texto multilínea con sus labels */
    vertical-align: top;

    /* Para dar suficiente espacio para escribir texto */
    height: 5em;

    /* Para permitir a los usuarios cambiar el tamaño de cualquier textarea verticalmente
        No funciona en todos los navegadores */
    resize: vertical;
}
</style>
	<title>Historia</title>
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	<script type="text/javascript" src="js/utils.js"></script>
	
	
	<script type="text/javascript">
	<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");%>
	
	
 	$(document).ready(function(){        	
        	$("#id_sid").val( "<%=request.getParameter("sid")%>");	   	       	        	        					
	});
		
	$("#salir").click(function(){
        	
       	$('#popup').css({display: 'none'});
       	$('#popUpContenido').empty().css({display:'none'});
        	
    });
    
    $("#idRegenerar").click(function(){
		var motivo = $("#idMotivo").val();
		var idUser = <%= usuarioLog.getSid() %>;
		var sid = $("#id_sid").val();
	  	 
  		 if(motivo == ""){
			document.getElementById("validarMotivo").innerHTML = "<font size='2' color='red'>*Debe ingresar campo motivo para continuar</font>";
			document.getElementById("idMotivo").focus();
			return;
    	}else{
    		//gatilla regenerar
    		valiadReingreso(motivo,idUser,sid);
    		document.getElementById("validarMotivo").innerHTML = "";
    	}
        	
    });
    
     function valiadReingreso(motivo,idUser,sid){
          var retVal = confirm("Seguro que desea Regenerar archivo contable ?");
    
    	if( retVal == true ){
              guardarRegenerar(motivo,idUser,sid);
          }
          else{
             return false;
         }
    }
    
    function guardarRegenerar(motivo,idUser,sid){
    	
    	$.ajax({
	       			url:'ejecutaRegenerarAction',
	       			type:'POST',
	       			data:{
	       					'sid': sid, 
            				'idUser': idUser,
            				'motivo': motivo
            			},
	       			
	       		}).done(function(data){
	       			data.resEjecucion;
	       			if(data.ejecutaInsert == 'dupli')
       				{	
       					alert("La solicitud realizada ya se encuentra registrada anteriormente, favor revisar historia..");			
       					$('#popup').css({display: 'none'});
       					$('#popUpContenido').empty().css({display:'none'});
       					reload();
       				
       				}
	       			
	       			if(data.ejecutaInsert == 'ok')
       				{	       	
       						alert("Se ha registrado su petici\u00F3n para regenerar archivo contable...");			
	       					$('#popup').css({display: 'none'});
        					$('#popUpContenido').empty().css({display:'none'});
        					reload();
        					
       				}
	       		});
    	
    
    }
    
    function reload(){
		var fechaInicioTmp = $('#fechaProceso').val().split("/");;
		var fechaTerminoTmp = $('#fechaFin').val().split("/");
								
		var diaInicio = fechaInicioTmp[0];
	    var mesInicio = fechaInicioTmp[1];
	    var anoInicio = fechaInicioTmp[2];
	    
	    var diaTermino = fechaTerminoTmp[0];
	    var mesTermino  = fechaTerminoTmp[1];
	    var anoTermino = fechaTerminoTmp[2];
	    
	    var fechaInicio  = anoInicio + mesInicio + diaInicio;
	    var fechaTermino = anoTermino + mesTermino + diaTermino; 
		var estado = $("#estado").val();
				
		  $.ajax({
				url : "RegenerarArchivoContableAvancesAction",
				type : "POST",
				data : {
					fechaInicio : fechaInicio,
					fechaFinal: fechaTermino,
					estado: estado,
				},
				dataType : "json",
				error : function(XMLHttpRequest, textStatus,
						errorThrown) {
					alert('Error ' + textStatus);
					alert(errorThrown);
					alert(XMLHttpRequest.responseText);
				},
				success : function(data) {
				//alert(data.listaFinal.length);
					mostrarTabla(data);
				}
			});
		
		
	}
	</script>
</head>
<body>

<input type="hidden" id="id_sid" name="sid" />
		<!-- <div class="titulo">  -->
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
			REGENERAR ARCHIVO CONTABLE DE AVANCES AD_<%=request.getParameter("fileName")%>.TXT
		</div>
		<hr>
		<!-- <br style="line-height:5px"/>  -->
		       	
       	 <div> 
       	 		<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm small col-6 mx-auto">
	       	       		<tr class="text-center">
	       					<td style="border:none !important" class="align-middle font-weight-bold py-0" align="center">
	       						Regenerar Interfaz
	       					</td>
	       				</tr>
	      		</table>
     	 </div>
     	 
     	 <!-- <br style="line-height:5px"/> -->
     	  
     	  <div>
     	  		<p id="validarMotivo"></p>
     	  		<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm small col-6 mx-auto">
	       				<tr>
	       					<td align="center">
	       					 	Motivo:
	       					 	<textarea rows="4" cols="110" class="form-control form-control-sm mt-2" name="idMotivo" id="idMotivo"></textarea>
	       					</td>
	       				</tr>
	       				<!-- <tr><td>&nbsp;&nbsp;</td></tr>  -->
	      		</table>
   		 </div>
   	 		
		<!-- <br style="line-height:5px"/>
		<br style="line-height:5px"/>
		<br style="line-height:5px"/>
		<br style="line-height:5px"/>
		<br style="line-height:5px"/>
		<br style="line-height:5px"/> -->
		
	      <div>
	      		<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm table-borderless">
	       	       		<tr>
	       					<td align="center">
	       						<input id="idRegenerar" type="button"  value="Regenerar" class="btn btn-primary" />
	       				    	<input type="button"  value="Cancelar" id="salir" name="salirpopup" class="btn btn-primary" />
	       					</td>
	       				</tr>
	      		</table>
     	 </div>
</body>
</html>