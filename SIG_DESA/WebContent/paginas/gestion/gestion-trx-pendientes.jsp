<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>

<html>
<head>
<title>Gestiones Trx Pendientes</title>
</head>

<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>

<body>
	<form id="form1">
	<div id="opr"></div>
		
		<!-- <div class="titulo"> -->
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
			GESTI&Oacute;N DE TRANSACCIONES PENDIENTES INCOMING 
		</div>

		<!-- <br style="line-height: 5px" />  -->
<hr>
		<div>
			<!-- contenedorInput -->
			<table class="table table-sm" style="width: 100%;">
				<tr class="text-right">
					<td class="align-middle">Nro. TC:</td>
					<td class="align-middle">
						<input type="text" name="numerotarjeta" id="numerotarjeta"
							maxlength="16" class="numericOnly form-control form-control-sm text-center py-0 d-block" /></td>
					<td class="align-middle">DESDE:</td>
					<!-- size="10" -->
					<td class="align-middle text-center"><input type="text" name="fechaDesde" id="fechaDesde"
						 maxlength="10" class="campoObligatorio fecha form-control form-control-sm text-center col-8 mx-auto datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td class="align-middle">HASTA:</td>
					<!-- size="10" -->
					<td class="align-middle text-center"><input type="text" name="fechaHasta" id="fechaHasta"
						 maxlength="10" class="campoObligatorio fecha form-control form-control-sm text-center col-8 mx-auto datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td rowspan="2" class="align-middle"><input type="button" id="buscarTrx" name="buscar" value="Buscar" class="btn btn-primary"></td>
				</tr>
				<!-- <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td></td>
				</tr> -->
				<tr class="text-right">
					<td class="align-middle">Estado:</td>
					<td class="align-middle"><select id="estado" class="custom-select">
							<option value="TDO">Todos</option>
							<option value="OU_PEN">Pendiente</option>
							<option value="OU_OK">Enviado</option>
							<option value="ING">Ingresado</option> 
					</select></td>
					<td class="align-middle">Tipo:</td>
					<td class="align-middle"><select id="tipoTrx" class="custom-select">
							<option value="00_00" >Todos</option>
							<option value="15_00">Primer contracargo</option>
							<option value="52_00">Petici&oacute;n de vale</option>
							<option value="15_205">Segundo contracargo</option>
					</select></td>
					<td class="align-middle">Operador:</td>
					<td class="align-middle">
						<select id="idOperador" class="custom-select">
							<%//  <option value="0" >Todos</option> %>
						</select>
					</td>
				</tr>
				

			</table>
		</div>


		<!-- <br style="line-height: 5px" />  -->

   		<!-- <div class="subTitulo">  -->
 		<div class="col-6 small mx-auto mt-5 mb-3 text-align-center text-center font-weight-bold " 
 			style="display: none;" id="tablaResTitulo">
			Listado de Transacciones Pendientes 
		</div>

		<!-- cajaContenido -->
		<div>
			<div id="tablaRes"></div>
		</div>

		<!-- <br style="line-height: 5px" />  -->

		<!-- class="contenedorInput" -->
		<table class="table table-sm table-borderless mt-3" style="width: 100%;">
			<tr>
				<!-- width="33%" -->
				<td class="align-middle" align="center" ><input type="button"
					value="Exportar" name="exportar"
					id="exportar" disabled class="btn btn-primary"/></td>
			</tr>
		</table>


	</form>
</body>

<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="js/gestion/gestionarTrxPendientes.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script>
	$(".datepicker").datepicker();
</script>

</html>