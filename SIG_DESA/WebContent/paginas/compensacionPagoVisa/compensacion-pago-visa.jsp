<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Compensaciones de Pagos Visa</title>
<script src="js/jquery-1.10.0.min.js" type="text/javascript">
	
</script>
<script type="text/javascript" src="js/utils.js"></script>
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>

	<script>
			$(".datepicker").datepicker();
			
	   		$(document).ready(function(){
	   			cargaOperador();
	   		});
	   		
	   		function cargaOperador(){
				$.ajax({
			   			url:'cargarComboOperadores',
			   			type:'POST'
			   		}).done(function(data){
			   			data.listaOperadores;
			   			var datos = (data.listaOperadores).split("~");
			   			
			   			var strCombo = "";
			   			
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   					if(fila[0] > 1){
			   						strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
			   					}	
							}
			   			
			   			$("#idOperador").append(strCombo);
			   		});
			}
	   </script>
	<script>
	$("#buscar").click(function() {

		$("#exportar").attr("disabled", true);		
		var date = new Date();
		var year = date.getFullYear();
		var operador = $("#idOperador").val();
		
		//alert(operador);
		
		if($('#fecDesde').val().trim() == ''){
			alert("Debe ingresar la fecha de inicio para comenzar la busqueda");
			return;
		}
		
		if($('#fecHasta').val().trim() == ''){
			alert("Debe ingresar la fecha final para comenzar la busqueda");
			return;
		}

		if(validarFecha($('#fecDesde').val()) == true && validarFecha($('#fecHasta').val())==true){
			var parametros = "fecDesde=" + $('#fecDesde').val();
			parametros += "&fecHasta=" + $('#fecHasta').val();
			parametros += "&operador=" + operador;
			
			$.ajax({
				url : "cargarRptCmpnscnCPVAction",
				type : "POST",
				data : parametros,
				dataType : "json",
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert('Error ' + textStatus);
					alert(errorThrown);
					alert(XMLHttpRequest.responseText);
				},
				success : function(data) {
					mostrarCuadraturaDiaria(data.listaCPago);
				}
			});
		}else{
			// pone la fecha de hoy por defecto
			if(date.getDay().toString().length > 1)
		    	day = date.getDay();
		    else
		    	day = "0" + date.getDay();
			
			if(date.getMonth().toString().length > 1)
		    	month = (date.getMonth() + 1);
		    else
		    	month = "0" + (date.getMonth() + 1);

			document.getElementById("fecDesde").value = day + "/" + month + "/" + year;
			document.getElementById("fecHasta").value = day + "/" + month + "/" + year;
			alert('La fecha ingresada es invalida!');
		}

	});

	$("#exportar")
			.click(
					function() {
						var operador = document.getElementById('idOperador').options[document.getElementById('idOperador').selectedIndex].text.toUpperCase();
						DataExportar = "";
						DatasExcel = "";

						idTablaExportar = "tablaOcultar";
						DataExportar += "TIT|"+ operador +"~";
						DataExportar += "TIT|REPORTE DE COMPENSACIONES~~";
						DataExportar += exportarExcelCompensacion(idTablaExportar,
								DatasExcel);

						exportarGenerico(DataExportar, "export");

					});
</script>
<script>
	function mostrarCuadraturaDiaria(listaCPago) {
		
		var exprt = false;
		var stringHtml = "";
		var fechas = listaCPago.split("-");		
		var idTablaocultar = document.getElementById('tablaOcultar');
		var fechaDesde ;
		var fechaHasta ;
		
	
	if (fechas != null) {
			if (fechas.length == 5) {

				fechaDesde = fechas[3];
				fechaHasta = fechas[4];
			}
			if (fechas.length == 3) {
				fechaDesde = fechas[1];
				fechaHasta = fechas[2];
			}
			if (fechas.length == 4) {
				fechaDesde = fechas[2];
				fechaHasta = fechas[3];
			}
		}

		idTablaocultar.style.display = 'none';
		stringHtml = "";
		// class="tablaContenido"
		stringHtml += "<table width='100%' class='table table-sm small' id='tablaOcultar'>";
		stringHtml = stringHtml + "<thead>";
		stringHtml = stringHtml
				+ "<tr class='text-center'><th class='align-middle'  width='100%' colspan='2' align='center' id='titulo'><label>COMPENSACIONES DE PAGOS</label></th></tr>";
		stringHtml = stringHtml + "</thead><tbody>";
		stringHtml = stringHtml
				+ "<tr class='text-center'><td class='align-middle' width='100%' colspan='2' align='center' id='detalle'>FECHA DESDE "
				+ fechaDesde + " - FECHA HASTA " + fechaHasta + "</td></tr>";
		stringHtml = stringHtml
				+ "<tr class='text-center'><td class='align-middle' colspan='2' align='center' width='100%' id='titulo'><label>COMPENSACION DE PAGOS TC46</label></td></tr>";
		
		if (fechas[0].trim() != '' && listaCPago != '') {
			//CANTIDAD ON US
			var lstReporteMensual = listaCPago.split(";");
			//console.log(lstReporteMensual[0].split("|")[1]);
			if (lstReporteMensual[0].split("|")[0] != "null" && lstReporteMensual[0].split("|")[0] != 0) {
				stringHtml = stringHtml + "<tr class='text-center'>";
				stringHtml = stringHtml
						+ "<td class='align-middle' align='center' width='50%' id='detalle'> CANTIDAD </td>";
				stringHtml = stringHtml
						+ "<td class='align-middle' align='center' width='50%' id='detalle'>"
						+ lstReporteMensual[0].split("|")[0] + "</td>";
				stringHtml = stringHtml + "</tr>";
				stringHtml = stringHtml + "<tr class='text-center'>";
				stringHtml = stringHtml
						+ "<td class='align-middle' align='center' width='50%' id='detalle'> MONTO TOTAL </td>";
				stringHtml = stringHtml
						+ "<td class='align-middle' align='center' width='50%' id='detalle'>"
						+ formato_numero(lstReporteMensual[0].split("|")[1])
						+ "</td>";
				stringHtml = stringHtml + "</tr>";
				exprt = true;
			} else
				stringHtml = stringHtml
						+ "<tr class='text-center'><td class='align-middle' align='center' colspan='2' witdh='100%' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";

			stringHtml = stringHtml
					+ "<tr class='text-center'><td class='align-middle' colspan='2' align='center' id='titulo' id='detalle'><label>COMPENSACION DE PAGOS OUTGOING</label></td></tr>";

			if (lstReporteMensual[2] != null)
				if (lstReporteMensual[2].split("|")[0] != "null"
						&& lstReporteMensual[2].split("|")[0] != "0") {
					stringHtml = stringHtml + "<tr class='text-center'>";
					stringHtml = stringHtml
							+ "<td class='align-middle' align='center' width='50%' id='detalle'> CANTIDAD </td>";
					stringHtml = stringHtml
							+ "<td class='align-middle' align='center' width='50%' id='detalle'>"
							+ lstReporteMensual[2].split("|")[0] + "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml + "<tr class='text-center'>";
					stringHtml = stringHtml
							+ "<td class='align-middle' align='center' width='50%' id='detalle'> MONTO TOTAL </td>";
					stringHtml = stringHtml
							+ "<td class='align-middle' align='center' width='50%' id='detalle'>"
							+ formato_numero(lstReporteMensual[2].split("|")[1])
							+ "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml
							+ "<tr class='text-center'><td class='align-middle' align='center' width='100%' colspan='2' id=''></td></tr>";
					exprt = true;
				} else
					stringHtml = stringHtml
							+ "<tr class='text-center'><td class='align-middle' align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";

			stringHtml = stringHtml
					+ "<tr class='text-center'><td class='align-middle' colspan='2' align='center' id='titulo' id='detalle'><label>DESCUENTO DE PAGOS POR TC46</label></td></tr>";

			if (lstReporteMensual[1] != null)
				if (lstReporteMensual[1].split("|")[0] != "null"
						&& lstReporteMensual[1].split("|")[0] != "0") {
					stringHtml = stringHtml + "<tr class='text-center'>";
					stringHtml = stringHtml
							+ "<td class='align-middle' align='center' width='50%' id='detalle'> CANTIDAD </td>";
					stringHtml = stringHtml
							+ "<td class='align-middle' align='center' width='50%' id='detalle'>"
							+ lstReporteMensual[1].split("|")[0] + "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml + "<tr class='text-center'>";
					stringHtml = stringHtml
							+ "<td class='align-middle' align='center' width='50%' id='detalle'> MONTO TOTAL </td>";
					stringHtml = stringHtml
							+ "<td class='align-middle' align='center' width='50%' id='detalle'>"
							+ formato_numero(lstReporteMensual[1].split("|")[1])
							+ "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml
							+ "<tr class='text-center'><td class='align-middle' align='center' width='100%' colspan='2' id='titulo'></td></tr>";
					exprt = true;
				} else
					stringHtml = stringHtml
							+ "<tr class='text-center'><td class='align-middle' align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";

		} else {
			stringHtml = stringHtml
					+ "<tr class='text-center'><td class='align-middle' align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";
		}
		stringHtml = stringHtml + "</tbody>";
		$('#divCuadraturaTransaccion').html(stringHtml);
		$("#divCuadraturaTransaccion").css("display", "");

		if (exprt == true)
			$("#exportar").attr("disabled", false);
	}

	$(document).ready(function() {
		$("#fecDesde").val(getYesterday());
		$("#fecHasta").val(getToday());

	});
</script>	
<style type="text/css">
#detalle{
	/* background-color: #E8E8E8; */
	font: cursive;
}
label {
    font-weight: bolder;
}
/* #tablaOcultar tr, td, th{
	height: 20px;
	vertical-align: middle;
} */
</style>
</head>
<body>
<!-- <div class="titulo"> -->
<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold"> 
	INFORME COMPENSACIONES DE PAGOS VISA
</div>
	<hr>
	<!-- <br style="line-height: 5px" />  -->

	<div id="staticParent">
		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm">
			<tr class="text-right">
				<td class="align-middle px-1">Fecha Desde:</td>
				<td class="align-middle text-center">
					<!-- size="10" -->
					<input type="text" name="fecDesde" id="fecDesde" maxlength="10" onKeypress="ingresoFecha();" class="campoObligatorio fecha form-control col-10 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
				</td>
				<td class="align-middle">
				Fecha Hasta:
				</td>
				<td class="align-middle text-center">
					<!-- size="10" -->
					<input type="text" name="fecHasta" id="fecHasta" maxlength="10" onKeypress="ingresoFecha();" class="campoObligatorio fecha form-control form-control-sm col-10 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
				</td>
				
				<td class="align-middle">
				Operador:
				</td>
				<td class="align-middle">
					<select id="idOperador" class="custom-select">
           				</select>
				</td>
				<td class="align-middle">
					<input type="button" id="buscar" value="Buscar" class="btn btn-primary">
				</td>
			</tr>
		</table>
	</div>

	<!-- <br style="line-height: 5px" />  -->

	<div>
		<!-- class="tablaContenido" -->
		<table width="100%" class="table table-sm small" id="tablaOcultar">
		</table>
		<div id="divCuadraturaTransaccion">
		</div>
	</div>

	<!-- <br style="line-height: 5px" />  -->


	<div>
		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm table-borderless">
			<tr class="text-center">
				<td class="align-middle" align="center"><input type="button"
					value="Exportar Tabla" name="exportar" id="exportar" class="btn btn-primary" disabled />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>