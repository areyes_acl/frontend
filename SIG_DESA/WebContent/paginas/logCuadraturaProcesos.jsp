<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Cuadratura Log De Procesos</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
	   <script>
	   
	   		$(".datepicker").datepicker();
	   		$(document).ready(function(){
	   			cargaOperador();
	   		});
	   		
	   		function cargaOperador(){
				$.ajax({
			   			url:'cargarComboOperadores',
			   			type:'POST'
			   		}).done(function(data){
			   			data.listaOperadores;
			   			var datos = (data.listaOperadores).split("~");
			   			
			   			var strCombo = "";
			   			
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   				
			   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
							}
			   			
			   			$("#idOperador").append(strCombo);
			   		});
			}
	   </script>
	   <script>
	   
	   	$('#idOperador').on('change', function(){
	   	
	   		$('#divCuadraturaLogCargaIncomingPP').hide(); 
	   		$('#divCuadraturaLogCargaOutgoingPP').hide();
	   	
	   	});
	   
	   </script>
	   <script>
		
			$("#buscar").click(function(){
				
					if ($('#fechaProceso').val().length  == 0){
							alert("Debe ingresar fecha de proceso para realizar la busqueda...");
							$("#fechaProceso").focus();
							return;
					}
					
					if (validarFecha($('#fechaProceso').val()) == false){
							alert("La fecha ingresada esta incorrecta, verifiquela...");
							$("#fechaProceso").focus();
							return;
					}
						
				    var fechaProcesoTmp = $('#fechaProceso').val().split("/");   //"20130619"//
  				    
  				    var diaInicio = fechaProcesoTmp[0];
  				    var mesInicio = fechaProcesoTmp[1];
  				    var anoInicio = fechaProcesoTmp[2];
  				    
  				    var fechaProceso  = anoInicio + mesInicio + diaInicio;
  				    
  				    var operador = $("#idOperador").val();
  				    					
					var parametros = "fechaProceso=" + fechaProceso + "&operador=" + operador;

					var tipoTransaccion = $("input[name='tipotransaccion']:checked").val();
					
					//alert($('#myTableIncomingDetalle').val());
					//alert($('#myTableRechazosDetalle').val());
					//myTableIncomingTotal
					
					//myTableRechazos
					
					
					//alert(operador);
					
					//   Esconder todos los contenedores de tablas antes de construirlas
					//   Esto evita que se agreguen nuevas tablas no correspondientes 
					// al momento de realizar una nueva busqueda.
					//   divs obtenidos de funcion ' $("#idOperador").on('change', function() ' 
					$("#divCuadraturaLogCargaIncomingTotal").css("display","none");
					$("#divCuadraturaLogCargaIncomingTitulo").css("display","none");
					$("#divCuadraturaLogCargaIncoming").css("display","none");
					
					$("#divCuadraturaLogCargaRechazosTotal").css("display","none");
					$("#divCuadraturaLogCargaRechazosTitulo").css("display","none");
					$("#divCuadraturaLogCargaRechazos").css("display","none");
					
					$("#divCuadraturaLogOutgoing").css("display","none");
					$("#divCuadraturaLogCargaIncomingPP").css("display","none");
					$("#divCuadraturaLogCargaOutgoingPP").css("display","none");
					
					if (tipoTransaccion == 0){ //Log De Carga
						
							$.ajax({
								url: "cuadraturaLogProcesoIncomingAction",
								type: "POST",
								data: parametros,
								dataType: "json",
								error: function(XMLHttpRequest, textStatus, errorThrown){
									alert('Error ' + textStatus);
									alert(errorThrown);
									alert(XMLHttpRequest.responseText);
								},
					    		success: function(data){
					    			
					    			mostrarLogCuadraturaProcesoCargaIncoming(data.listaCuadraturaIncoming);
					    			
								}
							});
							
							$.ajax({
								url: "cuadraturaLogProcesoRechazosAction",
								type: "POST",
								data: parametros,
								dataType: "json",
								error: function(XMLHttpRequest, textStatus, errorThrown){
									alert('Error ' + textStatus);
									alert(errorThrown);
									alert(XMLHttpRequest.responseText);
								},
					    		success: function(data){
					    			
					    			mostrarLogCuadraturaProcesoCargaRechazos(data.listaCuadraturaRechazos);
					    			
								}
							});
							if(operador == 1){
							
								$('#divCuadraturaLogCargaIncomingPP').show(); 
		   						$('#divCuadraturaLogCargaOutgoingPP').show();
							
							
								$.ajax({
									url: "cuadraturaLogPatPassAction",
									type: "POST",
									data: parametros,
									dataType: "json",
									error: function(XMLHttpRequest, textStatus, errorThrown){
										alert('Error ' + textStatus);
										alert(errorThrown);
										alert(XMLHttpRequest.responseText);
									},
						    		success: function(data){
						    			
						    			console.log(data);
						    			mostrarLogCuadraturaProcesoCargaIncomingPP(data.listaCuadraturaInconmingPatpass)
						    			
						    			
									}
								});
							}
							else{
								$('#divCuadraturaLogCargaIncomingPP').hide(); 
		   						$('#divCuadraturaLogCargaOutgoingPP').hide();
							}	
							
					}else{
						
						$.ajax({
							url: "cuadraturaLogProcesosOutgoingAction",
							type: "POST",
							data: parametros,
							dataType: "json",
							error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
							},
				    		success: function(data){
				    			
				    			mostrarLogCuadraturaProcesoOutgoing(data.listaCuadraturaOutgoing);
				    			
							}
						});
						if(operador == 1){
							
								$('#divCuadraturaLogCargaIncomingPP').show(); 
		   						$('#divCuadraturaLogCargaOutgoingPP').show();
							$.ajax({
								url: "cuadraturaLogOutgoingPatPassAction",
								type: "POST",
								data: parametros,
								dataType: "json",
								error: function(XMLHttpRequest, textStatus, errorThrown){
									alert('Error ' + textStatus);
									alert(errorThrown);
									alert(XMLHttpRequest.responseText);
								},
					    		success: function(data){
					    			
					    			console.log(data);
					    			mostrarLogCuadraturaProcesoCargaOutgoingPP(data.listaCuadraturaOutgoingPatpass)
					    			
					    			
								}
							});
						}
						else{
							$('#divCuadraturaLogCargaIncomingPP').hide(); 
		   					$('#divCuadraturaLogCargaOutgoingPP').hide();
						}	
					}
	  				
			});
			
			$("#fechaProceso").focus(function(){
				$("#divCuadraturaLogCargaIncomingTotal").css("display","none");
				$("#divCuadraturaLogCargaIncomingTitulo").css("display","none");
				$("#divCuadraturaLogCargaIncoming").css("display","none");
				
				$("#divCuadraturaLogCargaRechazosTotal").css("display","none");
				$("#divCuadraturaLogCargaRechazosTitulo").css("display","none");
				$("#divCuadraturaLogCargaRechazos").css("display","none");
				
				$("#divCuadraturaLogOutgoing").css("display","none");
				$("#divCuadraturaLogCargaIncomingPP").css("display","none");
				$("#divCuadraturaLogCargaOutgoingPP").css("display","none");
				$("#exportar").attr("disabled", true);
				
			});
			
			$("#idOperador").on('change', function(){
				$("#divCuadraturaLogCargaIncomingTotal").css("display","none");
				$("#divCuadraturaLogCargaIncomingTitulo").css("display","none");
				$("#divCuadraturaLogCargaIncoming").css("display","none");
				
				$("#divCuadraturaLogCargaRechazosTotal").css("display","none");
				$("#divCuadraturaLogCargaRechazosTitulo").css("display","none");
				$("#divCuadraturaLogCargaRechazos").css("display","none");
				
				$("#divCuadraturaLogOutgoing").css("display","none");
				$("#divCuadraturaLogCargaIncomingPP").css("display","none");
				$("#divCuadraturaLogCargaOutgoingPP").css("display","none");
				$("#exportar").attr("disabled", true);			
			});
			
			$(".tipotransaccion").click(function(){
				$("#divCuadraturaLogCargaIncomingTotal").css("display","none");
				$("#divCuadraturaLogCargaIncomingTitulo").css("display","none");
				$("#divCuadraturaLogCargaIncoming").css("display","none");
				
				$("#divCuadraturaLogCargaRechazosTotal").css("display","none");
				$("#divCuadraturaLogCargaRechazosTitulo").css("display","none");
				$("#divCuadraturaLogCargaRechazos").css("display","none");
				
				$("#divCuadraturaLogOutgoing").css("display","none");
				$("#divCuadraturaLogCargaIncomingPP").css("display","none");
				$("#divCuadraturaLogCargaOutgoingPP").css("display","none");
				$("#exportar").attr("disabled", true);
				
			});
			
			
			$("#exportar").click(function(){
			
					var DataExportar;
					//var DatasExcel;
					
					DataExportar = "";
					//DataExcel = "";
					
					/*if(DataExportar){
						alert('1');
					}
					if(DatasExcel){
						alert('2');
					}*/
										
					//alert(DataExportar);
					//alert(DataExcel);
					var operador = document.getElementById('idOperador').options[document.getElementById('idOperador').selectedIndex].text.toUpperCase();
					var operadorNumero = $('#idOperador').val();
					var tipoTransaccion = $("input[name='tipotransaccion']:checked").val();
				
					if (tipoTransaccion == 0){ //Log De Carga
						 
								    var totalTrxIncoming      = $('#totalTrxIncoming').val();
								    var totalTrxIncomingError = $('#totalTrxIncomingError').val();
								    var totalTrxRechazos      = $('#totalTrxRechazos').val();
								    var totalTrxRechazosError = $('#totalTrxRechazosError').val();
								    var totalTrxIncomingPatpass  = $('#totalTrxIncomingPatpass').val();
								    var totalTrxIncomingNormales =  $('#totalTrxIncomingNormales').val();
								    
									DataExportar="";
									DatasExcel="";
									console.log(DatasExcel);
						    		//****************************************************//
						    		//Datos de la transaccion seleccionada para mostrar  *//
						    		//****************************************************//
									idTablaExportar = "myTableIncomingTotal";
									DatasExcel="";
									DataExportar += "TIT||"+operador+"| | | | | |~";
									DataExportar += "TIT|TOTAL TRX. CARGA CORRECTA OUTGOING|TOTAL TRX. CARGA ERRONEAS OUTGOING~";
									DataExportar += "DET|" + totalTrxIncoming + "|" + totalTrxIncomingError + "~";
									
									idTablaExportar = "myTableIncomingDetalle";
									DatasExcel="";
									DataExportar += "TIT|SID|GLOSA DESCRIPTIVA|NRO TARJETA|MIT|CODIGO FUNCION|CODIGO RAZON|MONTO TRANSACCION~";
									DataExportar += exportarExcelLogProcesos(idTablaExportar, DatasExcel);
									
									idTablaExportar = "myTableRechazos";
									DatasExcel="";
									DataExportar += "TIT|TOTAL TRX. CARGA CORRECTA RECHAZOS|TOTAL TRX. CARGA ERRONEAS RECHAZOS~";
									DataExportar += "DET|" + totalTrxRechazos + "|" + totalTrxRechazosError + "~";
									
									idTablaExportar = "myTableRechazosDetalle";
									DatasExcel="";
									DataExportar += "TIT|SID|GLOSA DESCRIPTIVA|NRO TARJETA|MIT|CODIGO FUNCION|CODIGO RAZON|MONTO TRANSACCION~";
									DataExportar += exportarExcelLogProcesos(idTablaExportar, DatasExcel);
									//console.log(DataExportar);
									
									if(operadorNumero == 1){
										DatasExcel="";
										DataExportar += "TIT|TOTAL TRX. CARGA OUTGOING NORMALES|TOTAL TRX. CARGA OUTGOING PATPASS~";
										DataExportar += "DET|" + totalTrxIncomingNormales + "|" + totalTrxIncomingPatpass + "~";
									}
									
									
									exportarGenerico(DataExportar, "export");
									console.log(DataExportar);
									
					}else{
						
						var operador = document.getElementById('idOperador').options[document.getElementById('idOperador').selectedIndex].text.toUpperCase();
					    var totalTrxOutgoingPen      = $('#totalTrxOutgoingPen').val();
					    var totalTrxOutgoingOk       = $('#totalTrxOutgoingOk').val();
					    var totalTrxCargoAbonoPen    = $('#totalTrxCargoAbonoPen').val();
					    var totalTrxCargoAbonoOk     = $('#totalTrxCargoAbonoOk').val();
					    var totalTrxOutgoingPatpass =   $('#totalTrxOutgoingPatpass').val();
					    var totalTrxOutgoingNormales =  $('#totalTrxOutgoingNormales').val();
						DataExportar="";
						DatasExcel="";
			    		//****************************************************//
			    		//Datos de la transaccion seleccionada para mostrar  *//
			    		//****************************************************//
						DatasExcel="";
						DataExportar += "TIT|"+operador+"| |~";
						DataExportar += "TIT|TOTAL TRX. INCOMING PENDIENTES|TOTAL TRX. INCOMING OK~";
						DataExportar += "DET|" + totalTrxOutgoingPen + "|" + totalTrxOutgoingOk + "~";
						
						DatasExcel="";
						DataExportar += "TIT|TOTAL TRX. CARGO/ABONO PENDIENTES|TOTAL TRX. CARGO/ABONO OK~";
						DataExportar += "DET|" + totalTrxCargoAbonoPen + "|" + totalTrxCargoAbonoOk + "~";
						
						if(operadorNumero == 1){
							DatasExcel="";
							DataExportar += "TIT|TOTAL TRX. CARGA INCOMING NORMALES |TOTAL TRX.  CARGA INCOMING PATPASS~";
							DataExportar += "DET|" + totalTrxOutgoingNormales + "|" + totalTrxOutgoingPatpass + "~";
						}
						
						//console.log(DataExportar);
						exportarGenerico(DataExportar, "export");
						DataExportar = "";
						
					}
			
			});
			
		</script>
		
		<script>
			
				function mostrarLogCuadraturaProcesoCargaIncoming(listaCuadratura){
				
					$('#divCuadraturaLogCargaIncoming').html("");
						
					var lstcuadratura = listaCuadratura.split("~");
					var esttrx = lstcuadratura[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var stringHtmlIncoming = "";
					var stringHtmlDetalle  = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var errorincoming = (lstcuadratura.length-2);
							var cantidadincoming = 0;
							
							for(var x=1;x<lstcuadratura.length-1;x++)
							{
								var detallecuadratura         = lstcuadratura[x].split("|");
								
			    				cantidadincoming              = detallecuadratura[0];
			    				var sid                       = detallecuadratura[1];
			    				var descripcion               = detallecuadratura[2];
			    				var numerotarjeta             = detallecuadratura[3];
			    				var mit                       = detallecuadratura[4];
			    				var codigofuncion             = detallecuadratura[5];
			    				var codigorazon               = detallecuadratura[6];
			    				var montotransaccion          = detallecuadratura[7];
			    				var montoconciliacion         = detallecuadratura[8];
			    				var montofacturacion          = detallecuadratura[9];
			    				
			    	    		if (x==1){
			    	    			// class="tablaContenido"
			    	    			stringHtmlDetalle = "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableIncomingDetalle'>";
			    	    		}	
			    					
			    	    		stringHtmlDetalle += "<tr class='text-center'>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='5%'>" + sid + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='27%'>" + descripcion + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='15%'>" + numerotarjeta + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='5%'>" + mit + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='8%'>" + codigofuncion + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='8%'>" + codigorazon + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='15%'>"+ formato_numero(colocaComaNumero(montotransaccion)) + "</td>";
			    	    		stringHtmlDetalle += "</tr>";
			    				
							}
							
							stringHtmlDetalle += "</table>";
							
							//stringHtmlIncoming  = "<br style='line-height:5px'/>";
							// class='tablaContenido'
							stringHtmlIncoming += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableIncomingTotal'>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Correcta Outgoing</th>";
	    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Err\u00f3neas Outgoing</th>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
	    					stringHtmlIncoming += "<input type='text' name='totalTrxIncoming' id='totalTrxIncoming' size='10' maxlength='10' value='" + cantidadincoming + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
	    					stringHtmlIncoming += "<input type='text' name='totalTrxIncomingError' id='totalTrxIncomingError' size='10' maxlength='10' value='" + errorincoming + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "</table>";
	    					
	    					//stringHtmlIncoming += "<br style='line-height:5px'/>";
	    					
	    					$('#divCuadraturaLogCargaIncomingTotal').html(stringHtmlIncoming);
	    					$('#divCuadraturaLogCargaIncomingTotal').css("display","");
	    					
	    					$('#divCuadraturaLogCargaIncomingTitulo').css("display","");
	    					$('#divCuadraturaLogCargaIncoming').html(stringHtmlDetalle);
	    					$('#divCuadraturaLogCargaIncoming').css("display","");
	    					
	    					$("#exportar").attr("disabled", false);
	    					
					} else if (codtrx[1]=="2"){
						
						var cantidad         = lstcuadratura[1].split("|");
						var cantidadincoming = cantidad[1];
						
						//stringHtmlIncoming  = "<br style='line-height:5px'/>";
						// class='tablaContenido'
						stringHtmlIncoming += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableIncomingTotal'>";
    					stringHtmlIncoming += "<tr class='text-center'>";
    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Correcta Outgoing</th>";
    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Err\u00f3neas Outgoing </th>";
    					stringHtmlIncoming += "</tr>";
    					stringHtmlIncoming += "<tr class='text-center'>";
    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
    					stringHtmlIncoming += "<input type='text' name='totalTrxIncoming' id='totalTrxIncoming' size='10' maxlength='10' value='" + cantidadincoming + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
    					stringHtmlIncoming += "</td>";
    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
    					stringHtmlIncoming += "<input type='text' name='totalTrxIncomingError' id='totalTrxIncomingError' size='10' maxlength='10' value='0' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
    					stringHtmlIncoming += "</td>";
    					stringHtmlIncoming += "</tr>";
    					stringHtmlIncoming += "</table>";
    					
    					//stringHtmlIncoming += "<br style='line-height:5px'/>";
    					
    					$('#divCuadraturaLogCargaIncomingTotal').html(stringHtmlIncoming);
    					$('#divCuadraturaLogCargaIncomingTotal').css("display","");
    					
    					$("#exportar").attr("disabled", false);
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						alert(mensaje[1]);
					}
				}


				function mostrarLogCuadraturaProcesoCargaRechazos(listaCuadraturaRechazos){
					
					$('#divCuadraturaLogCargaRechazos').html("");
					var lstcuadratura = listaCuadraturaRechazos.split("~");
					var esttrx = lstcuadratura[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var stringHtmlRechazos = "";
					var stringHtmlDetalle  = "";
					var stringHtml = ""; 
					
					var cantidadrechazos          = "";
			    	var sid                       = "";
			    	var descripcion               = "";
			    	var numerotarjeta             = "";
			    	var mit                       = "";
			    	var codigofuncion             = "";
			    	var codigorazon               = "";
			    	var montotransaccion          = "";
			    	var montoconciliacion         = "";
			    	var montofacturacion          = "";
										
					
					if (codtrx[1]=="0")
					{
							var errorrechazos = lstcuadratura.length-2;
							var cantidadrechazos = 0;
							for(var x=1;x<lstcuadratura.length-1;x++)
							{
								var detallecuadratura         = lstcuadratura[x].split("|");
								
								
			    				
			    				cantidadrechazos          = detallecuadratura[0];
			    				sid                       = detallecuadratura[1];
			    				descripcion               = detallecuadratura[2];
			    				numerotarjeta             = detallecuadratura[3];
			    				mit                       = detallecuadratura[4];
			    				codigofuncion             = detallecuadratura[5];
			    				codigorazon               = detallecuadratura[6];
			    				montotransaccion          = detallecuadratura[7];
			    				montoconciliacion         = detallecuadratura[8];
			    				montofacturacion          = detallecuadratura[9];
			    				
			    				
			    	    		if (x==1){
			    	    			//class='tablaContenido'
			    	    			stringHtmlDetalle = "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableRechazosDetalle'>";
			    	    		}	
			    					
			    	    		stringHtmlDetalle += "<tr class='text-center'>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='5%'>" + sid + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='27%'>" + descripcion + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='15%'>" + numerotarjeta + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='5%'>" + mit + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='8%'>" + codigofuncion + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='8%'>" + codigorazon + "</td>";
			    	    		stringHtmlDetalle += "<td class='align-middle' width='15%'>"+ formato_numero(colocaComaNumero(montotransaccion)) + "</td>";
			    	    		stringHtmlDetalle += "</tr>";
			    				
							}
							
							stringHtmlDetalle += "</table>";
							
							
							//alert(stringHtmlDetalle);
			    			//console.log(stringHtmlDetalle);
			    			
			    			
			    			
							//stringHtmlRechazos  = "<br style='line-height:5px'/>";
							// class='tablaContenido'
							stringHtmlRechazos += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableRechazos'>";
							stringHtmlRechazos += "<tr class='text-center'>";
							stringHtmlRechazos += "<th class='align-middle' width='25%'>Total Trx. Carga Correcta Rechazos</th>";
							stringHtmlRechazos += "<th class='align-middle' width='25%'>Total Trx. Carga Err\u00f3neas Rechazos </th>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "<tr class='text-center'>";
							stringHtmlRechazos += "<td class='align-middle' align='center'>";
							stringHtmlRechazos += "<input type='text' name='totalTrxRechazos' id='totalTrxRechazos' size='10' maxlength='10' value='" + cantidadrechazos + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "<td class='align-middle' align='center'>";
							stringHtmlRechazos += "<input type='text' name='totalTrxRechazosError' id='totalTrxRechazosError' size='10' maxlength='10' value='" + errorrechazos + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "</table>";
							//stringHtmlRechazos += "<br style='line-height:5px'/>";
	    					
	    					$('#divCuadraturaLogCargaRechazosTotal').html(stringHtmlRechazos);
	    					$('#divCuadraturaLogCargaRechazosTotal').css("display","");
	    					
	    					$('#divCuadraturaLogCargaRechazosTitulo').css("display","");
	    					$('#divCuadraturaLogCargaRechazos').html(stringHtmlDetalle);
	    					$('#divCuadraturaLogCargaRechazos').css("display","");
	    					
	    					$("#exportar").attr("disabled", false);
	    					
					} else if (codtrx[1]=="2"){
						
						var cantidad         = lstcuadratura[1].split("|");
						var cantidadrechazos = cantidad[1];
						
						//stringHtmlRechazos  = "<br style='line-height:5px'/>"; 
						// class='tablaContenido'
						stringHtmlRechazos += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableRechazos'>";
						stringHtmlRechazos += "<tr class='text-center'>";
						stringHtmlRechazos += "<th class='align-middle' width='25%'>Total Trx. Carga Correcta Rechazos</th>";
						stringHtmlRechazos += "<th class='align-middle' width='25%'>Total Trx. Carga Err\u00f3neas Rechazos</th>";
						stringHtmlRechazos += "</tr>";
						stringHtmlRechazos += "<tr class='text-center'>";
						stringHtmlRechazos += "<td class='align-middle' align='center'>";
						stringHtmlRechazos += "<input type='text' name='totalTrxRechazos' id='totalTrxRechazos' size='10' maxlength='10' value='" + cantidadrechazos + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
						stringHtmlRechazos += "</td>";
						stringHtmlRechazos += "<td class='align-middle' align='center'>";
						stringHtmlRechazos += "<input type='text' name='totalTrxRechazosError' id='totalTrxRechazosError' size='10' maxlength='10' value='0' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
						stringHtmlRechazos += "</td>";
						stringHtmlRechazos += "</tr>";
						stringHtmlRechazos += "</table>";
    					
						//stringHtmlRechazos += "<br style='line-height:5px'/>";
    					
    					$('#divCuadraturaLogCargaRechazosTotal').html(stringHtmlRechazos);
    					$('#divCuadraturaLogCargaRechazosTotal').css("display","");
    					
    					$("#exportar").attr("disabled", false);
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						alert(mensaje[1]);
					}
				}
				
				
				function mostrarLogCuadraturaProcesoOutgoing(listaCuadraturaOutgoing){
					
					var lstcuadratura = listaCuadraturaOutgoing.split("~");
					var esttrx = lstcuadratura[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var stringHtmlOutgoing = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var detallecuadratura    = lstcuadratura[1].split("|");
			    			var cantidadoutpen       = detallecuadratura[0];
			    			var cantidadoutok        = detallecuadratura[1];
			    			var cantidadcarabopen    = detallecuadratura[2];
			    			var cantidadcarabook     = detallecuadratura[3];
			    			
			    			//stringHtmlOutgoing  = "<br style='line-height:5px'/>";
			    			// class='tablaContenido'
							stringHtmlOutgoing += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableOutgoing'>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Incoming Pendientes</th>";
							stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Incoming Ok </th>";
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<td class='align-middle' align='center'>";
							stringHtmlOutgoing += "<input type='text' name='totalTrxOutgoingPen' id='totalTrxOutgoingPen' size='10' maxlength='10' value='" + cantidadoutpen + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "<td class='align-middle' align='center'>";
							stringHtmlOutgoing += "<input type='text' name='totalTrxOutgoingOk' id='totalTrxOutgoingOk' size='10' maxlength='10' value='" + cantidadoutok + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "</table>";
	    					
							//stringHtmlOutgoing += "<br style='line-height:5px'/>";
	    					
	    					// class='tablaContenido'
							stringHtmlOutgoing += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableCargoAbono'>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Cargo Abono Pendiente</th>";
							stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Carga Abono Ok </th>";
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<td class='align-middle' align='center'>";
							stringHtmlOutgoing += "<input type='text' name='totalTrxCargoAbonoPen' id='totalTrxCargoAbonoPen' size='10' maxlength='10' value='" + cantidadcarabopen + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "<td class='align-middle' align='center'>";
							stringHtmlOutgoing += "<input type='text' name='totalTrxCargoAbonoOk' id='totalTrxCargoAbonoOk' size='10' maxlength='10' value='" + cantidadcarabook + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "</table>";
	    					
	    					$('#divCuadraturaLogOutgoing').html(stringHtmlOutgoing);
	    					$("#divCuadraturaLogOutgoing").css("display","");
	    					
	    					$("#exportar").attr("disabled", false);
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						alert(mensaje[1]);
					}
					
				}
				
				
				//script tabla PATPASS
				
				function mostrarLogCuadraturaProcesoCargaIncomingPP(listaCuadratura){
						
					var lstcuadratura = listaCuadratura.split("~");
					console.log(lstcuadratura);
					console.log("1");
					var esttrx = lstcuadratura[0].split("~");
					console.log(esttrx);
					console.log("2");
					var codtrx = esttrx[0].split("|");
					console.log(codtrx);
					console.log("3");
					
					var stringHtmlIncoming = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var errorincoming       = (lstcuadratura.length-2);
							var detallecuadratura   = lstcuadratura[1].split("|");
							console.log(detallecuadratura);
			    			var cantidadincomingNormales    = detallecuadratura[0];
			    			var cantidadincomingPatpass    = detallecuadratura[1];
			    			
							
							//stringHtmlIncoming  = "<br style='line-height:5px'/>";
							// class='tablaContenido'
							stringHtmlIncoming += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableIncoming'>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Outgoing Normales</th>";
	    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Outgoing PATPASS</th>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
	    					stringHtmlIncoming += "<input type='text' name='totalTrxIncoming' id='totalTrxIncomingNormales' size='10' maxlength='10' value='" + cantidadincomingNormales + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
	    					stringHtmlIncoming += "<input type='text' name='totalTrxIncomingError' id='totalTrxIncomingPatpass' size='10' maxlength='10' value='" + cantidadincomingPatpass + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "</table>";
	    					
	    					stringHtml = stringHtmlIncoming;
	    					
	    					$('#divCuadraturaLogCargaIncomingPP').html(stringHtml);
	    					$("#divCuadraturaLogCargaIncomingPP").css("display","");
	    					
					} else if (codtrx[1]=="2"){
						
						var cantidad         = lstcuadratura[1].split("|");
						var cantidadincoming = cantidad[1];
						
						//stringHtmlIncoming  = "<br style='line-height:5px'/>";
						// class='tablaContenido'
						stringHtmlIncoming += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableIncoming'>";
    					stringHtmlIncoming += "<tr class='text-center'>";
    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Outgoing Normales</th>";
    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Outgoing PATPASS</th>";
    					stringHtmlIncoming += "</tr>";
    					stringHtmlIncoming += "<tr class='text-center'>";
    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
    					stringHtmlIncoming += "<input type='text' name='totalTrxIncoming' id='totalTrxIncomingNormales' size='10' maxlength='10' value='" + cantidadincoming + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
    					stringHtmlIncoming += "</td>";
    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
    					stringHtmlIncoming += "<input type='text' name='totalTrxIncomingError' id='totalTrxIncomingPatpass' size='10' maxlength='10' value='0' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
    					stringHtmlIncoming += "</td>";
    					stringHtmlIncoming += "</tr>";
    					stringHtmlIncoming += "</table>";
    					
    					stringHtml = stringHtmlIncoming;
    					
    					$('#divCuadraturaLogCargaIncoming').html(stringHtml);
    					$("#divCuadraturaLogCargaIncoming").css("display","");
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						if (mensaje[1] == 'No existe informacion con los parametros seleccionados para cuadratura de incoming.'){
							
							//stringHtmlIncoming  = "<br style='line-height:5px'/>";
							// class='tablaContenido'
							stringHtmlIncoming += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableIncoming'>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Incoming Normales</th>";
	    					stringHtmlIncoming += "<th class='align-middle' width='25%'>Total Trx. Carga Incoming PATPASS</th>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
	    					stringHtmlIncoming += "<input type='text' name='totalTrxIncoming' id='totalTrxIncomingNormales' size='10' maxlength='10' value='0' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "<td class='align-middle' align='center'>";
	    					stringHtmlIncoming += "<input type='text' name='totalTrxIncomingError' id='totalTrxIncomingPatpass' size='10' maxlength='10' value='0' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "</table>";
	    					
	    					stringHtml = stringHtmlIncoming;
	    					
	    					$('#divCuadraturaLogCargaIncomingPP').html(stringHtml);
	    					$("#divCuadraturaLogCargaIncomingPP").css("display","");
							
						}else{
							alert(mensaje[1]);	
						}
						
					}
				}
				
				
				function mostrarLogCuadraturaProcesoCargaOutgoingPP(listaCuadratura){
						
					var lstcuadratura = listaCuadratura.split("~");
					console.log(lstcuadratura);
					console.log("1");
					var esttrx = lstcuadratura[0].split("~");
					console.log(esttrx);
					console.log("2");
					var codtrx = esttrx[0].split("|");
					console.log(codtrx);
					console.log("3");
					
					var stringHtmlOutgoing = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var errorougoint       = (lstcuadratura.length-2);
							var detallecuadratura   = lstcuadratura[1].split("|");
							console.log(detallecuadratura);
			    			var cantidadOutgoingNormales    = detallecuadratura[0];
			    			var cantidadOutgoingPatpass    = detallecuadratura[1];
			    			
							
			    			//stringHtmlOutgoing  = "<br style='line-height:5px'/>";
			    			// class='tablaContenido'
			    			stringHtmlOutgoing += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableIncoming'>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Carga Incoming Normales</th>";
							stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Carga Incoming   PATPASS</th>";;
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<td class='align-middle' align='center'>";
							stringHtmlOutgoing += "<input type='text' name='totalTrxIncoming' id='totalTrxOutgoingNormales' size='10' maxlength='10' value='" + cantidadOutgoingNormales + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "<td class='align-middle' align='center'>";
							stringHtmlOutgoing += "<input type='text' name='totalTrxIncomingError' id='totalTrxOutgoingPatpass' size='10' maxlength='10' value='" + cantidadOutgoingPatpass + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "</table>";
	    					
	    					stringHtml = stringHtmlOutgoing;
	    					
	    					$('#divCuadraturaLogCargaIncomingPP').html(stringHtml);
	    					$("#divCuadraturaLogCargaIncomingPP").css("display","");
	    					
					} else if (codtrx[1]=="2"){
						
						var cantidad         = lstcuadratura[1].split("|");
						var cantidadoutgoing = cantidad[1];
						
						//stringHtmlOutgoing  = "<br style='line-height:5px'/>";
						// class='tablaContenido'
						stringHtmlOutgoing += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableIncoming'>";
						stringHtmlOutgoing += "<tr class='text-center'>";
						stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Carga Incoming Normales</th>";
						stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Carga Incoming   PATPASS</th>";;
						stringHtmlOutgoing += "</tr>";
    					stringHtmlOutgoing += "<tr class='text-center'>";
    					stringHtmlOutgoing += "<td class='align-middle' align='center'>";
    					stringHtmlOutgoing += "<input type='text' name='totalTrxIncoming' id='totalTrxIncoming' size='10' maxlength='10' value='" + cantidadoutgoing + "' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
    					stringHtmlOutgoing += "</td>";
    					stringHtmlOutgoing += "<td class='align-middle' align='center'>";
    					stringHtmlOutgoing += "<input type='text' name='totalTrxIncomingError' id='totalTrxIncomingError' size='10' maxlength='10' value='0' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
    					stringHtmlOutgoing += "</td>";
    					stringHtmlOutgoing += "</tr>";
    					stringHtmlOutgoing += "</table>";
    					
    					stringHtml = stringHtmlOutgoing;
    					
    					$('#divCuadraturaLogCargaIncoming').html(stringHtml);
    					$("#divCuadraturaLogCargaIncoming").css("display","");
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						if (mensaje[1] == 'No existe informacion con los parametros seleccionados para cuadratura de incoming.'){
							
							//stringHtmlOutgoing  = "<br style='line-height:5px'/>";
							// class='tablaContenido'
							stringHtmlOutgoing += "<table width='100%' class='table table-sm small col-8 mx-auto' id='myTableIncoming'>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Carga Incoming Normales</th>";
							stringHtmlOutgoing += "<th class='align-middle' width='25%'>Total Trx. Carga Incoming PATPASS</th>";;
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<td class='align-middle' align='center'>";
							stringHtmlOutgoing += "<input type='text' name='totalTrxIncoming' id='totalTrxIncoming' size='10' maxlength='10' value='0' class='campoSalida form-control form-control-sm mx-auto' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "<td class='align-middle' align='center'>";
							stringHtmlOutgoing += "<input type='text' name='totalTrxIncomingError' id='totalTrxIncomingError' size='10' maxlength='10' value='0' class='campoSalida form-control form-control-sm mx-auto text-center col-7' readonly/>";
	    					stringHtmlOutgoing += "</td>";
	    					stringHtmlOutgoing += "</tr>";
	    					stringHtmlOutgoing += "</table>";
	    					
	    					stringHtml = stringHtmlOutgoing;
	    					
	    					$('#divCuadraturaLogCargaOutgoingPP').html(stringHtml);
	    					$("#divCuadraturaLogCargaOutgoingPP").css("display","");
							
						}else{
							alert(mensaje[1]);	
						}
						
					}
				}

				
		</script>
	   
   </head>
   
   <body>
      <!-- <div class="titulo"> -->
	  <div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold ">
       		LOG DE PROCESOS DIARIO
  	  </div>
  	  
  	  <hr/>
      
      <!-- <br style="line-height:5px"/>  -->
      
       <div class="">
      	<!-- class="contenedorInput" -->
   		<table width="100%" class="table table-sm small">
               <tr class="text-center">
                    <td class="align-middle">
                    	<!-- Los dos input tenian class="tipotransaccion" -->
                    	<div class="custom-control custom-radio">
                    		<input type="radio" name="tipotransaccion" id="tipoTransaccion0" class="custom-control-input" value="0" checked>
                    		<label class="custom-control-label" for="tipoTransaccion0">Log De Carga</label>
                    	</div>
                    	
                    	<div class="custom-control custom-radio">
                    		<input type="radio" name="tipotransaccion" id="tipoTransaccion1" class="custom-control-input" value="1">
		        			<label class="custom-control-label" for="tipoTransaccion1">Log De Salida</label>
                    	</div>
                    	
		        	</td>
           			<td class="align-middle text-right">Fecha Proceso :</td>
           			<td class="align-middle">
           				<input type="text" name="fechaProceso" id="fechaProceso" size="10" maxlength="10" class="campoObligatorio fecha form-control form-control-sm text-center col-12 mx-auto datepicker"/ placeholder="DD/MM/AAAA">
           			
           			</td>
           			<td class="align-middle">
           				Operador:
           			</td>
           			<td class="align-middle px-0">
           				<select id="idOperador" class="custom-select w-75">
           				</select>
             		</td>
             		<td class="align-middle">
             			<input type="button"  id="buscar" value="Buscar" class="btn btn-primary">
             		</td>
       			</tr>
      		</table>
      </div>
     
     <div id="divCuadraturaLogCargaIncomingTotal" style="display: none;">
     </div>
     <div id="divCuadraturaLogCargaIncomingTitulo" style="display: none;">
     	<!-- class="tablaContenido" -->
		<table width="100%" class='table table-sm small'>
    		<tr class="text-center">
	    		<th class="align-middle" width="5%">SID</th>
	    	    <th class="align-middle" width="27%">GLOSA DESCRIPTIVA</th>
	    	    <th class="align-middle" width="15%">NRO TARJETA</th>
	    	    <th class="align-middle" width="5%">MIT</th>
	    	    <th class="align-middle" width="8%">CODIGO<br>FUNCION</th>
	    	    <th class="align-middle" width="8%">CODIGO<br>RAZON</th>
	    	    <th class="align-middle" width="15%">MONTO<br>TRANSACCION</th>
    	 	</tr>
    	 </table>
	  </div>
      <div>
      		<div id="divCuadraturaLogCargaIncoming">
	  		</div>
      </div>

	  
	  <div id="divCuadraturaLogCargaRechazosTotal" style="display: none;">
      </div>
	  
	  <div id="divCuadraturaLogCargaRechazosTitulo" style="display: none;">
	  	<!-- class="tablaContenido" -->
		<table width='100%' class='table table-sm small'>
   			<tr class="text-center">
	    	    <th class="align-middle" width="5%">SID</th>
	    	    <th class="align-middle" width="27%">GLOSA DESCRIPTIVA</th>
	    	    <th class="align-middle" width="15%">NRO TARJETA</th>
	    	    <th class="align-middle" width="5%">MIT</th>
	    	    <th class="align-middle" width="8%">CODIGO<br>FUNCION</th>
	    	    <th class="align-middle" width="8%">CODIGO<br>RAZON</th>
	    	    <th class="align-middle" width="15%">MONTO<br>TRANSACCION</th>
    	 	</tr>
   	 	</table>
	  </div>
	  
	  <div>
	  		<div id="divCuadraturaLogCargaRechazos">
	  		</div>
	  </div>

	  <div>
		  	<div id="divCuadraturaLogOutgoing">
	      	</div>
	  </div>
	   <div>
      		<div id="divCuadraturaLogCargaIncomingPP">
	  		</div>
      </div>
       <div>
      		<div id="divCuadraturaLogCargaOutgoingPP">
	  		</div>
      </div>

      <!-- <br style="line-height:5px"/>  -->
      
      <div>
      	<!-- class="contenedorInput" -->
    	<table  width="100%" class=" table-borderless">
    		<tr>
    		    <td align="center">
    				<input type="button" value="Exportar Tabla" name="exportar" id="exportar" class="btn btn-primary" disabled/>
    			</td>
    		</tr>
    	</table>
    </div>

   </body>
</html>