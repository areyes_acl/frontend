
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 

<html>

   <head>
       
       <title>Gestiones Por Cuenta</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       <script type="text/javascript" src="js/utils.js"></script>
       
       	<script>
       	
        $(document).ready(function(){
        	
        	var optC = "";
			var optA = "";
			var montoInit = 0;
			var montoAct = 0;

        	$("#sid").val( "<%=request.getParameter("sid")%>");	
        	$("#numerotarjeta").val( "<%=request.getParameter("numeroTarjeta")%>");
        	$("#fechatrx").val( "<%=request.getParameter("fechaTrx")%>");
        	$("#referencia").val( "<%=request.getParameter("referencia")%>");
        	$("#codrazon").val( "<%=request.getParameter("codrazon")%>");
        	$("#montoca").val( "<%=request.getParameter("montoca")%>");
        	
        	$.ajax({
				url:'listaCargoAbonoAction',
				type:'POST',
				dataType: "json"
			}).done(function(resp){
				var datos = (resp.listaCargoAbono).split("~");
				$("#serieData").val(datos);
				
				for(var i=1;i<datos.length-1;i++)
				{
					var fila = datos[i].split("|");
					
					if(fila[3]=='C')
					{
						optC += "<option value='"+fila[0]+"'>"+fila[2]+"</option>";
					}else{
						optA += "<option value='"+fila[0]+"'>"+fila[2]+"</option>";
					}
				}
				
				if($("#cargo").prop("checked"))
				{
					$("#tipoCargoAbono").empty().append(optC);
				}else{
					$("#tipoCargoAbono").empty().append(optA);
				}
				
				
			});
        	/*Cargo abono
        	$.ajax({
				url:'calculoFechaTopeAction',
				type:'POST',
				dataType: "json",
				data: $("#formPupUpCA").serialize()
			}).done(function(resp){
				var datos = (resp.fechaTope).split("~");
				
				if(datos[0]=="esttrx:0")
				{
					$("#fechaTope").val(datos[1]);
				}else{
					alert(datos[1]);
				}
			});*/
        	
        	$("input:radio").click(function(){
        		if($("#cargo").prop("checked"))
       			{
        			$("#tipoCargoAbono").empty();
        			$("#tipoCargoAbono").append(optC);
       			}else{
       				$("#tipoCargoAbono").empty();
       				$("#tipoCargoAbono").append(optA);
       				
       			}
        	});
        	
        	
        	$("#montoca").blur(function(){
        		if(montoInit==0){
	        		montoInit = "<%=request.getParameter("montoca")%>";
	        		montoInit = montoInit.replace(/\./g,"");
        		}
        		if(montoAct==0){
	        		montoAct = $("#montoca").val();
	        		montoAct = montoAct.replace(/\./g,"");
        		}
        		else{
      		 		montoAct = $("#montoca").val();
        		}
        		if(montoInit.replace(/\./g,"")!=montoAct.replace(/\,/g,"")){
	       			montoAct = montoAct.replace(/\./g,"");
	       			if(parseFloat(montoAct) > parseFloat(montoInit)){
	       				alert('Monto actual no puede superar al inicial');
	       				$("#montoca").val(formato_numero(reemplazarComaPunto(sacaPuntos(montoInit.replace(/\./g,",")))));
	       			}
	       			else{
	       				$("#montoca").val(formato_numero(reemplazarComaPunto(sacaPuntos($("#montoca").val()))));
	       			}
        		}
        	});
        	 		
        });
        
        
        $("#guardarPopUpCargoAbono").click(function(){ 
        
               	var montoActual = reemplazarComaPunto(sacaPuntos($("#montoca").val()));
               	var montoActualConFormato = $("#montoca").val();
               	var montoActualSinFormato = numeroSinComaPunto($("#montoca").val());
               	
               	if(montoActualSinFormato == 0 ){
               		alert('No se puede ingresar un monto 0, por favor corregir antes de realizar el CARGO/ABONO');
               		return ;
               	}
        		
        		
        		var montoOriginal = "<%=request.getParameter("montoca")%>";
	        		montoOriginal = reemplazarComaPunto(sacaPuntos(montoOriginal));
	        		
               	
               	$.ajax({
        			url:'obtenerSumaCargoAbono',
        			type:'post',
        			dataType:'json',
					data:$("#formPupUpCA").serialize()
        		}).done(function(data){
        			 var sumatoriaTotal=  0;
        			if(data.sumatoriaCargoAbono != null && data.sumatoriaCargoAbono)
       				{
        			  sumatoriaTotal= data.sumatoriaCargoAbono;      			 
       				}
       				
       				var montoOrig = parseFloat(montoOriginal);
       				var montoNuev = parseFloat(montoActual);
       				var sumatoria =parseFloat(sumatoriaTotal) ;
       				var total = 0;
       				
       				if($("#abono").prop("checked")){
       					total = montoOrig - montoNuev + sumatoria;
       				}else{
       					total = montoOrig + montoNuev + sumatoria;
       				}
       				
       				var montoMaximo = montoOrig + sumatoria;
       				               		
               		// ABONO
               		if($("#abono").prop("checked"))	{	        				
	        			if(total >= 0){
	        				  $("#montoca").val(montoActualSinFormato);
		        				guardaCargoYAbono();
		        			
        				}else{
        				
        					alert("No se puede ingresar un monto mayor al total de abonos existentes.  Monto Maximo =" + montoMaximo);
        					$("#montoca").val(montoActualConFormato);
        				};
	        		
	        		// CARGO
	        		}else{
	        			$("#montoca").val(montoActualSinFormato);
	        			guardaCargoYAbono();
	        		};     		
        	
        		});
        	});
        	
        	
        	
        	
        	function guardaCargoYAbono(){
        			$.ajax({
        							url:'insertCargoAbonoAction',
			        				type:'post',
			        				dataType:'json',
									data:$("#formPupUpCA").serialize()
        							}).done(function(data){
        				
        							if(data.insertCargoAbono.split("~")[0]=="esttrx:0")
       								{
	        							alert(data.insertCargoAbono.split("~")[1]);
	        							$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});
       								}else if(data.insertCargoAbono.split("~")[0]=="esttrx:1"){
	       									alert(data.insertCargoAbono.split("~")[1]);
	       							}
        					  }); 
        	}
        
        
		$("#cancelarCargarAbonar").click(function(){
        	
        	$('#popup').css({display: 'none'});
        	$('#popUpContenido').empty().css({display:'none'});
        	
        });
        
		
		</script>
		
   </head>

<body>
	<form id="formPupUpCA">
		<input type="hidden" id="serieData" />
		<input type="hidden" id="sid" name="sid" />
		<input type="hidden" id="codrazon" name="codRazon" />
		<input type="hidden" id="usuario" name="sidusuario" value="<%= request.getSession().getAttribute("sidusuario") %>"/>
		
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
			Cargo / Abono
		</div>
		<hr>
		<!-- <br style="line-height:5px"/>  -->
		  
		  	<!--  contenedorInput -->
	    	<div>
	      		<table width="100%" class="table table-sm">
	      			<tr>
	              		<td class="align-middle text-right">&nbsp;Tipo :</td>
	              		<td class="align-middle">
	              			<div class="form-check custom-control custom-radio">
	              				<input type="radio" id="cargo" name="group1" value="C" class="custom-control-input" checked>
	              				<label for="cargo" class="custom-control-label">Cargo&nbsp;</label>
							</div>
							<div class="form-check custom-control custom-radio">	
								<input type="radio" id="abono" name="group1" value="A" class="custom-control-input">
								<label for="abono" class="custom-control-label">Abono&nbsp;</label>
							</div>
	              		</td>
	       		    </tr>
	            	<tr>
	            		<td class="align-middle text-right">&nbsp;Numero Tarjeta :</td>
	              		<td class="align-middle">
	              			<!-- size="50" -->
	              			<input type="text" id="numerotarjeta" name="numerotarjeta" maxlength="50" value="" class="campoSalida form-control form-control-sm"/>
	              		</td>
	       			</tr>
	       			<tr>
	              		<td class="align-middle text-right">&nbsp;Fecha Transaccion :</td>
	              		<td class="align-middle">
	              			<!-- size="17" -->
	              			<input type="text" id="fechatrx" name="fechatrx"  maxlength="17" class="campoSalida form-control form-control-sm" value=""/>
	              		</td>
	       			</tr>

				<tr align="justify">
					<td class="align-middle text-right">&nbsp;Tipo cargo / abono :</td>
					<td class="align-middle"><select name="tipoCargoAbono"
						id="tipoCargoAbono" style="" class="custom-select">

					</select></td>
				</tr>

				<tr>
	               		<td class="align-middle text-right">&nbsp;Referencia :</td>
	              		<td class="align-middle">
	              			<!-- size="50" -->
	              			<input type="text" id="referencia" name="referencia"  maxlength="50" class="campoSalida form-control form-control-sm"/>
	              		</td>
	       			</tr>
	              	
	              	<tr>
						<td class="align-middle text-right">&nbsp;Moneda :</td>
						<td class="align-middle">
							<!-- style="width: 130px;" -->
							<select name="moneda" id="moneda"  class="custom-select">
								<option value="152"> CLP </option>
							<!--  	<option value="840"> USD </option> !-->
							</select>
						</td>
	              	</tr>
	       			
	       			<tr>
	       				<td class="align-middle text-right">&nbsp;Monto :</td>
	              		<td class="align-middle">
	              			<!-- size="14" -->
	              			<input type="text" id="montoca" name="montoca" maxlength="14" value="" class="campoObligatorio form-control form-control-sm"/>
	              		</td>
	              	</tr>  
	              	<!-- tr>
	       				<td>&nbsp;Fecha tope :</td>
	              		<td>
	              			<input type="text" id="fechaTope" name="fechaTope" size="10" maxlength="10" value="" class="campoSalida"/>
	              		</td>
	              	</tr-->     		
	      		</table>
	      
	      </div>
	      
	      
	      <!-- <br style="line-height:5px"/>  -->
	      <div>
	      		<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm table-borderless">
	       	       		<tr>
	       					<td align="center">
	       						<input type="button" id="guardarPopUpCargoAbono" value="Guardar" class="btn btn-primary"/>
	       				    	<input type="button"  value="Cancelar" id="cancelarCargarAbonar" name="cancelarCargarAbonar" class="btn btn-primary">
	       					</td>
	       				</tr>
	      		</table>
     	 </div>
      </form>
</body>
</html>
