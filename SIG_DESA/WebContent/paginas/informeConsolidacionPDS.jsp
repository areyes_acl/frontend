<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Informe de Compensaci&oacute;n</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
	  <script>
	  
	  	$(".datepicker").datepicker();
	  	
	  	$(document).ready(function(){
	  		cargaOperador();
	  	});
//funcion para cargar lista de operadores en combobox
	function cargaOperador(){
	$.ajax({
   			url:'cargarComboOperadores',
   			type:'POST'
   		}).done(function(data){
   			data.listaOperadores;
   			var datos = (data.listaOperadores).split("~");
   			
   			var strCombo = "";
   			var html = "";
   			
   			for(var i=1;i<datos.length-1;i++)
				{			
   				var fila = datos[i].split("|");
   				
   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
   				html += "<input type='hidden' id='oper"+fila[0]+"' value='"+fila[2]+"'>";
				}
   			
   			$("#idOperador").append(strCombo);
   			$("#opr").append(html);
   		});
}
	  </script>	 
	   <script>
	   
		
		var today = new Date();
		
		var dd = today.getDate(); 
		var mm = today.getMonth()+1;//January is 0! 
		var yyyy = today.getFullYear();
		
		var dia = dd; 
		var mes = mm; 
		var ano = yyyy;
		
		if(dd<10){
			dd='0'+dd;
		}
		
		if(mm<10){
			mm='0'+mm;
		}
		var fechaActual = dd + "/" + mm +"/" + yyyy;
		
		
		$('#fechaInicio').val(fechaActual);
		$('#fechaFin').val(fechaActual);
		
	        var DataExportar='';
			
			
			
			
			$("#exportar").click(function(){
				var operador = $("#idOperador").val();
				var opr = $("#oper"+operador).val();
				
				//alert(operadores);
				if ($('#fechaInicio').val().length == 0) {
					alert("Debe ingresar fecha de inicio para exportar...");
					$("#fechaInicio").focus();
					return;
				}
				if ($('#fechaFin').val().length == 0) {
					alert("Debe ingresar fecha de termino para exportar...");
					$("#fechaFin").focus();
					return;
				}
			
				var fechaTermino = convertirFecha($('#fechaFin').val());
				var fechaInicio = convertirFecha($('#fechaInicio').val());
							
				if (fechaInicio > fechaTermino) {
					alert("La fecha de inicio es mayor a la fecha de termino, verifiquela...");
					$("#fechaInicio").focus();
					return;
				}					
				var parametros = "fechaInicio=" + $('#fechaInicio').val();
				
				if ($('#fechaFin').val().length  == 0){
					f = new Date();
					parametros = parametros + "&fechaFin=" + f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
					fechaTermino = convertirFecha(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
				}else{
				
					if (validarFecha($('#fechaFin').val()) == false){
							alert("La fecha ingresada esta incorrecta, verifiquela...");
							$("#fechaFin").focus();
							return;
					}
					
					parametros = parametros + "&fechaFin=" + $('#fechaFin').val() + "&codOperador=" + opr;
					fechaTermino = convertirFecha($('#fechaFin').val());
				}
				
				parametros += "&nombreArchivo=" + "export"+ "&operador=" + operador;																		
				location.href="./exportarDatosExcelPDSAction?" + parametros;
				
			});
			
		</script>
		
		
   </head>
   
   <body>
       <div id="opr"></div>
      <!-- <div class="titulo"> -->
 	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
       		INFORME DE COMPENSACIÓN
 	</div>
 	
 	<hr/>
      
      <!-- <br style="line-height:5px"/> -->
      
      <div class="mb-3">
      	<!-- class="contenedorInput" -->
   		<table width="100%" class="table table-sm">
			<tr class="text-right">
				<td class="align-middle">
					Fecha desde:
				</td>		
				
				<td class="align-middle text-center">
					<input type="text" name="fechaInicio" id="fechaInicio"
						size="10" maxlength="10" onKeypress="ingresoFecha();"
						class="campoObligatorio fecha form-control form-control-sm mx-auto d-inline text-center col-12 datepicker" placeholder="DD/MM/AAAA"/>
				</td>
				
				<td class="align-middle">
					Fecha hasta:
				</td>
				
				<td class="align-middle text-center">
					<input type="text" name="fechaFin" id="fechaFin" size="10"
						maxlength="10" onKeypress="ingresoFecha();"
						class="campoObligatorio fecha form-control  form-control-sm  mx-auto d-inline text-center col-12 datepicker" placeholder="DD/MM/AAAA" />
				</td>
				
				<td class="align-middle">Operador:</td>
		
				<td class="align-middle"><select id="idOperador" class="custom-select"></select></td>
		
				<td class="align-middle"><input type="button" id="exportar" value="Exportar" size="5" class="btn btn-primary"></td>
			 </tr>				 
		</table>
      </div>
     
      <!-- <br style="line-height:5px"/>  -->
      
      
      <!-- <br style="line-height:5px"/>  -->
      
      
   

   </body>
</html>