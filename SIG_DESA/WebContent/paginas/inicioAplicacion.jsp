<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>RESUMEN GENERAL LOG DE CARGAS Y OUTGOING</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   <script>
	   		$(document).ready(function(){
	   			cargaOperador();
	   		});
	   		
	   		function cargaOperador(){
				$.ajax({
			   			url:'cargarComboOperadores',
			   			type:'POST'
			   		}).done(function(data){
			   			data.listaOperadores;
			   			var datos = (data.listaOperadores).split("~");
			   			
			   			var strCombo = "";
			   			
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   				
			   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
							}
			   			
			   			$("#idOperador").append(strCombo);
			   		});
			}
	   </script>
	   <script>
		
	   $(document).ready(function() {
			  inicio(1);
	   });
	   
	   $('#idOperador').on('change', function(){
	   		var operador = $('#idOperador').val();
	   		inicio(operador);
	   });
	   
	   function inicio(operador){
	    
	    
	    if(typeof operador == 'undefined'){
	    	operador = 1;
	    }
	    
	    
	   	var parametros = "fechaProceso=99999999&operador=" + operador;

					//alert(parametros);
					$.ajax({
								url: "cuadraturaLogProcesoIncomingAction",
								type: "POST",
								data: parametros,
								dataType: "json",
								error: function(XMLHttpRequest, textStatus, errorThrown){
									alert('Error ' + textStatus);
									alert(errorThrown);
									alert(XMLHttpRequest.responseText);
								},
					    		success: function(data){
					    		
					    			mostrarLogCuadraturaProcesoCargaIncoming(data.listaCuadraturaIncoming);
					    			
								}
					});
							
					$.ajax({
								url: "cuadraturaLogProcesoRechazosAction",
								type: "POST",
								data: parametros,
								dataType: "json",
								error: function(XMLHttpRequest, textStatus, errorThrown){
									alert('Error ' + textStatus);
									alert(errorThrown);
									alert(XMLHttpRequest.responseText);
								},
					    		success: function(data){
					    			
					    			mostrarLogCuadraturaProcesoCargaRechazos(data.listaCuadraturaRechazos);
					    			
								}
					});
							
					$.ajax({
							url: "cuadraturaLogProcesosOutgoingAction",
							type: "POST",
							data: parametros,
							dataType: "json",
							error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
							},
				    		success: function(data){
				    			
				    			mostrarLogCuadraturaProcesoOutgoing(data.listaCuadraturaOutgoing);
				    			
							}
					});
						
					if(operador == 1){
						
						$('#divCuadraturaLogCargaOutgoingPP').show();
						$('#divCuadraturaLogCargaIncomingPP').show();
						
						
						$.ajax({
							url: "cuadraturaLogPatPassAction",
							type: "POST",
							data: parametros,
							dataType: "json",
							error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
							},
				    		success: function(data){
				    			mostrarLogCuadraturaProcesoCargaIncomingPP(data.listaCuadraturaInconmingPatpass)
				    			
				    			
							}
						});	
				
									
				
						$.ajax({
							url: "cuadraturaLogOutgoingPatPassAction",
							type: "POST",
							data: parametros,
							dataType: "json",
							error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
							},
				    		success: function(data){
				    			mostrarLogCuadraturaProcesoOutgoingPP(data.listaCuadraturaOutgoingPatpass)
				    			
				    			
							}
						});
				
					}
					else{
						$('#divCuadraturaLogCargaOutgoingPP').hide();
						$('#divCuadraturaLogCargaIncomingPP').hide();
					}
	   }
	   
			
	 //Metodos
	   
			
				function mostrarLogCuadraturaProcesoCargaIncoming(listaCuadratura){
						
					var lstcuadratura = listaCuadratura.split("~");
					var esttrx = lstcuadratura[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var stringHtmlIncoming = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var errorincoming       = (lstcuadratura.length-2);
							var detallecuadratura   = lstcuadratura[1].split("|");
			    			var cantidadincoming    = detallecuadratura[0];
							
							stringHtmlIncoming  = "<br style='line-height:5px'/>";
							stringHtmlIncoming += "<table width='100%' class='table table-sm my-0' id='myTableIncoming'>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Correcta Outgoing</th>";
	    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Erroneas Ougoing</th>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "<tr>";
	    					stringHtmlIncoming += "<td align='center'>";
	    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control-sm text-center'  name='totalTrxIncoming' id='totalTrxIncoming' size='10' maxlength='10' value='" + cantidadincoming + "' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "<td align='center'>";
	    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control-sm text-center'  name='totalTrxIncomingError' id='totalTrxIncomingError' size='10' maxlength='10' value='" + errorincoming + "' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "</table>";
	    					
	    					stringHtml = stringHtmlIncoming;
	    					
	    					$('#divCuadraturaLogCargaIncoming').html(stringHtml);
	    					$("#divCuadraturaLogCargaIncoming").css("display","");
	    					
					} else if (codtrx[1]=="2"){
						
						var cantidad         = lstcuadratura[1].split("|");
						var cantidadincoming = cantidad[1];
						
						stringHtmlIncoming  = "<br style='line-height:5px'/>";
						stringHtmlIncoming += "<table width='100%' class='table table-sm my-0' id='myTableIncoming'>";
    					stringHtmlIncoming += "<tr class='text-center'>";
    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Correcta Outgoing</th>";
    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Err\u00f3neas Outgoing </th>";
    					stringHtmlIncoming += "</tr>";
    					stringHtmlIncoming += "<tr>";
    					stringHtmlIncoming += "<td align='center'>";
    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncoming' id='totalTrxIncoming' size='10' maxlength='10' value='" + cantidadincoming + "' readonly/>";
    					stringHtmlIncoming += "</td>";
    					stringHtmlIncoming += "<td align='center'>";
    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncomingError' id='totalTrxIncomingError' size='10' maxlength='10' value='0' readonly/>";
    					stringHtmlIncoming += "</td>";
    					stringHtmlIncoming += "</tr>";
    					stringHtmlIncoming += "</table>";
    					
    					stringHtml = stringHtmlIncoming;
    					
    					$('#divCuadraturaLogCargaIncoming').html(stringHtml);
    					$("#divCuadraturaLogCargaIncoming").css("display","");
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						if (mensaje[1] == 'No existe informacion con los parametros seleccionados para cuadratura de incoming.'){
							
							stringHtmlIncoming  = "<br style='line-height:5px'/>";
							stringHtmlIncoming += "<table width='100%' class='table table-sm my-0' id='myTableIncoming'>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Correcta Outgoing</th>";
	    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Err\u00f3neas Outgoing </th>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "<tr>";
	    					stringHtmlIncoming += "<td align='center'>";
	    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncoming' id='totalTrxIncoming' size='10' maxlength='10' value='0' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "<td align='center'>";
	    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncomingError' id='totalTrxIncomingError' size='10' maxlength='10' value='0' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "</table>";
	    					
	    					stringHtml = stringHtmlIncoming;
	    					
	    					$('#divCuadraturaLogCargaIncoming').html(stringHtml);
	    					$("#divCuadraturaLogCargaIncoming").css("display","");
							
						}else{
							alert(mensaje[1]);	
						}
						
					}
				}


				function mostrarLogCuadraturaProcesoCargaRechazos(listaCuadraturaRechazos){
					
					var lstcuadratura = listaCuadraturaRechazos.split("~");
					var esttrx = lstcuadratura[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var stringHtmlRechazos = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var errorrechazos       = lstcuadratura.length-2;
							var detallecuadratura   = lstcuadratura[1].split("|");
							var cantidadrechazos    = detallecuadratura[0];
			    				
							stringHtmlRechazos  = "<br style='line-height:5px'/>";
							stringHtmlRechazos += "<table width='100%' class='table table-sm my-0' id='myTableRechazos'>";
							stringHtmlRechazos += "<tr class='text-center'>";
							stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Correcta Rechazos</th>";
							stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Err\u00f3neas Rechazos </th>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "<tr>";
							stringHtmlRechazos += "<td align='center'>";
							stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazos' id='totalTrxRechazos' size='10' maxlength='10' value='" + cantidadrechazos + "' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "<td align='center'>";
							stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazosError' id='totalTrxRechazosError' size='10' maxlength='10' value='" + errorrechazos + "' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "</table>";
	    					
	    					stringHtml = stringHtmlRechazos;
	    					
	    					$('#divCuadraturaLogCargaRechazos').html(stringHtml);
	    					$("#divCuadraturaLogCargaRechazos").css("display","");
	    					
					} else if (codtrx[1]=="2"){
						
						var cantidad         = lstcuadratura[1].split("|");
						var cantidadrechazos = cantidad[1];
						
						stringHtmlRechazos  = "<br style='line-height:5px'/>"; 
						stringHtmlRechazos += "<table width='100%' class='table table-sm my-0' id='myTableRechazos'>";
						stringHtmlRechazos += "<tr class='text-center'>";
						stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Correcta Rechazos</th>";
						stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Err\u00f3neas Rechazos</th>";
						stringHtmlRechazos += "</tr>";
						stringHtmlRechazos += "<tr>";
						stringHtmlRechazos += "<td align='center'>";
						stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazos' id='totalTrxRechazos' size='10' maxlength='10' value='" + cantidadrechazos + "' readonly/>";
						stringHtmlRechazos += "</td>";
						stringHtmlRechazos += "<td align='center'>";
						stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazosError' id='totalTrxRechazosError' size='10' maxlength='10' value='0' readonly/>";
						stringHtmlRechazos += "</td>";
						stringHtmlRechazos += "</tr>";
						stringHtmlRechazos += "</table>";
    					
    					stringHtml = stringHtmlRechazos;
    					
    					$('#divCuadraturaLogCargaRechazos').html(stringHtml);
    					$("#divCuadraturaLogCargaRechazos").css("display","");
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						
						if (mensaje[1] == 'No existe informacion con los parametros seleccionados para cuadratura de rechazos.'){
						
							stringHtmlRechazos  = "<br style='line-height:5px'/>";
							stringHtmlRechazos += "<table width='100%' class='table table-sm my-0' id='myTableRechazos'>";
							stringHtmlRechazos += "<tr class='text-center'>";
							stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Correcta Rechazos</th>";
							stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Err\u00f3neas Rechazos </th>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "<tr>";
							stringHtmlRechazos += "<td align='center'>";
							stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazos' id='totalTrxRechazos' size='10' maxlength='10' value='0' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "<td align='center'>";
							stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazosError' id='totalTrxRechazosError' size='10' maxlength='10' value='0' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "</table>";
	    					
	    					stringHtml = stringHtmlRechazos;
	    					
	    					$('#divCuadraturaLogCargaRechazos').html(stringHtml);
	    					$("#divCuadraturaLogCargaRechazos").css("display","");
						}else{
							alert(mensaje[1]);
						}
						
					}
				}
				
				
				function mostrarLogCuadraturaProcesoOutgoing(listaCuadraturaOutgoing){
					
					var lstcuadratura = listaCuadraturaOutgoing.split("~");
					var esttrx = lstcuadratura[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var stringHtmlOutgoing = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var detallecuadratura    = lstcuadratura[1].split("|");
			    			var cantidadoutpen       = detallecuadratura[0];
			    			var cantidadoutok        = detallecuadratura[1];
			    			var cantidadcarabopen    = detallecuadratura[2];
			    			var cantidadcarabook     = detallecuadratura[3];
			    			
			    			stringHtmlOutgoing  = "<br style='line-height:5px'/>";
							stringHtmlOutgoing += "<table width='100%' class='table table-sm my-0' id='myTableOutgoing'>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<th width='25%'>Total Trx. Incoming Pendientes</th>";
							stringHtmlOutgoing += "<th width='25%'>Total Trx. Incoming Ok </th>";
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "<tr>";
							stringHtmlOutgoing += "<td align='center'>";
							stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxOutgoingPen' id='totalTrxOutgoingPen' size='10' maxlength='10' value='" + cantidadoutpen + "' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "<td align='center'>";
							stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxOutgoingOk' id='totalTrxOutgoingOk' size='10' maxlength='10' value='" + cantidadoutok + "' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "</table>";
	    					
							stringHtmlOutgoing += "<br style='line-height:5px'/>";
	    					
							stringHtmlOutgoing += "<table width='100%' class='table table-sm my-0' id='myTableCargoAbono'>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<th width='25%'>Total Trx. Cargo Abono Pendiente</th>";
							stringHtmlOutgoing += "<th width='25%'>Total Trx. Carga Abono Ok </th>";
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "<tr>";
							stringHtmlOutgoing += "<td align='center'>";
							stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxCargoAbonoPen' id='totalTrxCargoAbonoPen' size='10' maxlength='10' value='" + cantidadcarabopen + "' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "<td align='center'>";
							stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxCargoAbonoOk' id='totalTrxCargoAbonoOk' size='10' maxlength='10' value='" + cantidadcarabook + "' readonly/>";
							stringHtmlOutgoing += "</td>";
							stringHtmlOutgoing += "</tr>";
							stringHtmlOutgoing += "</table>";
	    					
	    					$('#divCuadraturaLogOutgoing').html(stringHtmlOutgoing);
	    					$("#divCuadraturaLogOutgoing").css("display","");
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						alert(mensaje[1]);
					}
					
				}
				
				//script tabla PATPASS
				
				function mostrarLogCuadraturaProcesoCargaIncomingPP(listaCuadratura){
						
					var lstcuadratura = listaCuadratura.split("~");
					var esttrx = lstcuadratura[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var stringHtmlIncoming = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var errorincoming       = (lstcuadratura.length-2);
							var detallecuadratura   = lstcuadratura[1].split("|");
							
			    			var cantidadincomingNormales    = detallecuadratura[0];
			    			var cantidadincomingPatpass    = detallecuadratura[1];
			    			
							stringHtmlIncoming  = "<br style='line-height:5px'/>";
							stringHtmlIncoming += "<table width='100%' class='table table-sm my-0' id='myTableIncomingPP'>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Outgoing Normales </th>";
	    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Outgoing PATPASS</th>";
	    					stringHtmlIncoming += "</tr>"; 
	    					stringHtmlIncoming += "<tr>";
	    					stringHtmlIncoming += "<td align='center'>";
	    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncoming' id='totalTrxIncoming' size='10' maxlength='10' value='" + cantidadincomingNormales + "' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "<td align='center'>";
	    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncomingError' id='totalTrxIncomingError' size='10' maxlength='10' value='" + cantidadincomingPatpass + "' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "</table>";
	    					
	    					stringHtml = stringHtmlIncoming;
	    					
	    					$('#divCuadraturaLogCargaIncomingPP').html(stringHtml);
	    					$("#divCuadraturaLogCargaIncomingPP").css("display","");
	    					
					} else if (codtrx[1]=="2"){
						
						var cantidad         = lstcuadratura[1].split("|");
						var cantidadincoming = cantidad[1];
						
						stringHtmlIncoming  = "<br style='line-height:5px'/>";
						stringHtmlIncoming += "<table width='100%' class='table table-sm my-0' id='myTableIncomingPP'>";
    					stringHtmlIncoming += "<tr class='text-center'>";
    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Outgoing Normales </th>";
    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Outgoing PATPASS</th>";
    					stringHtmlIncoming += "</tr>";
    					stringHtmlIncoming += "<tr>";
    					stringHtmlIncoming += "<td align='center'>";
    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncoming' id='totalTrxIncomiPP' size='10' maxlength='10' value='" + cantidadincoming + "' readonly/>";
    					stringHtmlIncoming += "</td>";
    					stringHtmlIncoming += "<td align='center'>";
    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncomingError' id='totalTrxIncomingErrorPP' size='10' maxlength='10' value='0' readonly/>";
    					stringHtmlIncoming += "</td>";
    					stringHtmlIncoming += "</tr>";
    					stringHtmlIncoming += "</table>";
    					
    					stringHtml = stringHtmlIncoming;
    					
    					$('#divCuadraturaLogCargaIncomingPP').html(stringHtml);
    					$("#divCuadraturaLogCargaIncomingPP").css("display","");
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						if (mensaje[1] == 'No existe informacion con los parametros seleccionados para cuadratura de incoming.'){
							
							stringHtmlIncoming  = "<br style='line-height:5px'/>";
							stringHtmlIncoming += "<table width='100%' class='table table-sm my-0' id='myTableIncomingPP'>";
	    					stringHtmlIncoming += "<tr class='text-center'>";
	    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Outgoing Normales </th>";
	    					stringHtmlIncoming += "<th width='25%'>Total Trx. Carga Outgoing PATPASS</th>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "<tr>";
	    					stringHtmlIncoming += "<td align='center'>";
	    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncoming' id='totalTrxIncomingPP' size='10' maxlength='10' value='0' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "<td align='center'>";
	    					stringHtmlIncoming += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxIncomingError' id='totalTrxIncomingErrorPP' size='10' maxlength='10' value='0' readonly/>";
	    					stringHtmlIncoming += "</td>";
	    					stringHtmlIncoming += "</tr>";
	    					stringHtmlIncoming += "</table>";
	    					
	    					stringHtml = stringHtmlIncoming;
	    					
	    					$('#divCuadraturaLogCargaIncomingPP').html(stringHtml);
	    					$("#divCuadraturaLogCargaIncomingPP").css("display","");
							
						}else{
							alert(mensaje[1]);	
						}
						
					}
				}


				function mostrarLogCuadraturaProcesoCargaRechazosPP(listaCuadraturaRechazos){
					
					var lstcuadratura = listaCuadraturaRechazos.split("~");
					var esttrx = lstcuadratura[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var stringHtmlRechazos = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var errorrechazos       = lstcuadratura.length-2;
							var detallecuadratura   = lstcuadratura[1].split("|");
							var cantidadrechazos    = detallecuadratura[0];
			    				
							stringHtmlRechazos  = "<br style='line-height:5px'/>";
							stringHtmlRechazos += "<table width='100%' class='table table-sm my-0' id='myTableRechazos'>";
							stringHtmlRechazos += "<tr class='text-center'>";
							stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Correcta Rechazos</th>";
							stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Err\u00f3neas Rechazos </th>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "<tr>";
							stringHtmlRechazos += "<td align='center'>";
							stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazos' id='totalTrxRechazos' size='10' maxlength='10' value='" + cantidadrechazos + "' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "<td align='center'>";
							stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazosError' id='totalTrxRechazosError' size='10' maxlength='10' value='" + errorrechazos + "' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "</table>";
	    					
	    					stringHtml = stringHtmlRechazos;
	    					
	    					$('#divCuadraturaLogCargaRechazos').html(stringHtml);
	    					$("#divCuadraturaLogCargaRechazos").css("display","");
	    					
					} else if (codtrx[1]=="2"){
						
						var cantidad         = lstcuadratura[1].split("|");
						var cantidadrechazos = cantidad[1];
						
						stringHtmlRechazos  = "<br style='line-height:5px'/>"; 
						stringHtmlRechazos += "<table width='100%' class='table table-sm my-0' id='myTableRechazos'>";
						stringHtmlRechazos += "<tr class='text-center'>";
						stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Correcta Rechazos</th>";
						stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Err\u00f3neas Rechazos</th>";
						stringHtmlRechazos += "</tr>";
						stringHtmlRechazos += "<tr>";
						stringHtmlRechazos += "<td align='center'>";
						stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazos' id='totalTrxRechazos' size='10' maxlength='10' value='" + cantidadrechazos + "' readonly/>";
						stringHtmlRechazos += "</td>";
						stringHtmlRechazos += "<td align='center'>";
						stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazosError' id='totalTrxRechazosError' size='10' maxlength='10' value='0' readonly/>";
						stringHtmlRechazos += "</td>";
						stringHtmlRechazos += "</tr>";
						stringHtmlRechazos += "</table>";
    					
    					stringHtml = stringHtmlRechazos;
    					
    					$('#divCuadraturaLogCargaRechazos').html(stringHtml);
    					$("#divCuadraturaLogCargaRechazos").css("display","");
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						
						if (mensaje[1] == 'No existe informacion con los parametros seleccionados para cuadratura de rechazos.'){
						
							stringHtmlRechazos  = "<br style='line-height:5px'/>";
							stringHtmlRechazos += "<table width='100%' class='table table-sm my-0' id='myTableRechazos'>";
							stringHtmlRechazos += "<tr class='text-center'>";
							stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Correcta Rechazos</th>";
							stringHtmlRechazos += "<th width='25%'>Total Trx. Carga Err\u00f3neas Rechazos </th>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "<tr>";
							stringHtmlRechazos += "<td align='center'>";
							stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazos' id='totalTrxRechazos' size='10' maxlength='10' value='0' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "<td align='center'>";
							stringHtmlRechazos += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxRechazosError' id='totalTrxRechazosError' size='10' maxlength='10' value='0' readonly/>";
							stringHtmlRechazos += "</td>";
							stringHtmlRechazos += "</tr>";
							stringHtmlRechazos += "</table>";
	    					
	    					stringHtml = stringHtmlRechazos;
	    					
	    					$('#divCuadraturaLogCargaRechazos').html(stringHtml);
	    					$("#divCuadraturaLogCargaRechazos").css("display","");
						}else{
							alert(mensaje[1]);
						}
						
					}
				}
				
				
				function mostrarLogCuadraturaProcesoOutgoingPP(listaCuadratura){
					
					
					
					var lstcuadratura = listaCuadratura.split("~");
				
					var esttrx = lstcuadratura[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var stringHtmlOutgoing = "";
					var stringHtml = ""; 
					
					if (codtrx[1]=="0")
					{
							var errorincoming       = (lstcuadratura.length-2);
							var detallecuadratura   = lstcuadratura[1].split("|");
							
			    			var cantidadOutgoingNormales    = detallecuadratura[0];
			    			var cantidadOutgoingPatpass    = detallecuadratura[1];
			    			
							
			    			stringHtmlOutgoing  = "<br style='line-height:5px'/>";
			    			stringHtmlOutgoing += "<table width='100%' class='table table-sm my-0' id='myTableOutgoingPP'>";
			    			stringHtmlOutgoing += "<tr class='text-center'>";
			    			stringHtmlOutgoing += "<th width='25%'>Total Trx. Carga Incoming Normales</th>";
			    			stringHtmlOutgoing += "<th width='25%'>Total Trx. Carga Incoming PATPASS</th>";
			    			stringHtmlOutgoing += "</tr>";
			    			stringHtmlOutgoing += "<tr>";
			    			stringHtmlOutgoing += "<td align='center'>";
			    			stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxOutgoingPP' id='totalTrxOutgoingPP' size='10' maxlength='10' value='" + cantidadOutgoingNormales + "' readonly/>";
			    			stringHtmlOutgoing += "</td>";
			    			stringHtmlOutgoing += "<td align='center'>";
			    			stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxOutgoingPPError' id='totalTrxOutgoingPPError' size='10' maxlength='10' value='" + cantidadOutgoingPatpass + "' readonly/>";
			    			stringHtmlOutgoing += "</td>";
			    			stringHtmlOutgoing += "</tr>";
			    			stringHtmlOutgoing += "</table>";
	    					
	    					stringHtml = stringHtmlOutgoing;
	    					
	    					$('#divCuadraturaLogCargaOutgoingPP').html(stringHtml);
	    					$("#divCuadraturaLogCargaOutgoingPP").css("display","");
	    					
					} else if (codtrx[1]=="2"){
						
						var cantidad         = lstcuadratura[1].split("|");
						var cantidadOutgoing = cantidad[1];
						
						stringHtmlOutgoing  = "<br style='line-height:5px'/>";
						stringHtmlOutgoing += "<table width='100%' class='table table-sm my-0' id='myTableIncoming'>";
						stringHtmlOutgoing += "<tr class='text-center'>";
						stringHtmlOutgoing += "<th width='25%'>Total Trx. Carga Incoming Normales</th>";
		    			stringHtmlOutgoing += "<th width='25%'>Total Trx. Carga Incoming PATPASS</th>";
						stringHtmlOutgoing += "</tr>";
						stringHtmlOutgoing += "<tr>";
						stringHtmlOutgoing += "<td align='center'>";
						stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxOutgoingPP' id='totalTrxOutgoingPP' size='10' maxlength='10' value='" + cantidadOutgoing + "' readonly/>";
						stringHtmlOutgoing += "</td>";
						stringHtmlOutgoing += "<td align='center'>";
						stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxOutgoingPPError' id='totalTrxOutgoingPPError' size='10' maxlength='10' value='0' readonly/>";
						stringHtmlOutgoing += "</td>";
						stringHtmlOutgoing += "</tr>";
						stringHtmlOutgoing += "</table>";
    					
    					stringHtml = stringHtmlOutgoing;
    					
    					$('#divCuadraturaLogCargaOutgoingPP').html(stringHtml);
    					$("#divCuadraturaLogCargaOutgoingPP").css("display","");
						
					} else {
						var mensaje = lstcuadratura[1].split("|");
						if (mensaje[1] == 'No existe informacion con los parametros seleccionados para cuadratura de incoming.'){
							
							stringHtmlOutgoing  = "<br style='line-height:5px'/>";
							stringHtmlOutgoing += "<table width='100%' class='table table-sm my-0' id='myTableOutgoingPP'>";
							stringHtmlOutgoing += "<tr class='text-center'>";
							stringHtmlOutgoing += "<th width='25%'>Total Trx. Carga Incoming Normales</th>";
			    			stringHtmlOutgoing += "<th width='25%'>Total Trx. Carga Incoming PATPASS</th>";
	    					stringHtmlOutgoing += "</tr>";
	    					stringHtmlOutgoing += "<tr>";
	    					stringHtmlOutgoing += "<td align='center'>";
	    					stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxOutgoingPP' id='totalTrxOutgoingPP' size='10' maxlength='10' value='0' readonly/>";
	    					stringHtmlOutgoing += "</td>";
	    					stringHtmlOutgoing += "<td align='center'>";
	    					stringHtmlOutgoing += "<input type='text' class='campoSalida form-control col-6 text-center'  name='totalTrxOutgoingPPError' id='totalTrxOutgoingPPError' size='10' maxlength='10' value='0' readonly/>";
	    					stringHtmlOutgoing += "</td>";
	    					stringHtmlOutgoing += "</tr>";
	    					stringHtmlOutgoing += "</table>";
	    					
	    					stringHtml = stringHtmlOutgoing;
	    					
	    					$('#divCuadraturaLogCargaOutgoingPP').html(stringHtml);
	    					$("#divCuadraturaLogCargaOutgoingPP").css("display","");
							
						}else{
							alert(mensaje[1]);	
						}
						
					}
					
				}
				
		</script>   
   </head>
   
   <body>
       
      <!-- <div class="titulo"> -->
      <div class="col-12 mx-auto mt-4 text-align-center text-center font-weight-bold">
       		RESUMEN GENERAL LOG DE CARGAS Y OUTGOING
  	  </div>
  	  
  	  <hr>
      
	  <div id="test" class="col-3" style="padding-left:0px">
     		<select id="idOperador" class="custom-select">
      		</select>
    	</div>
     
      <div>
      		<div id="divCuadraturaLogCargaIncoming" class="mx-auto">
	  		</div>
      </div>
	     
	  <div>
		  	<div id="divCuadraturaLogCargaRechazos" class="mx-auto">
	      	</div>
	  </div>

	  <div>
		  	<div id="divCuadraturaLogOutgoing" class="mx-auto">
	      	</div>
	  </div>
	   <div>
		  	<div id="divCuadraturaLogCargaOutgoingPP" class="mx-auto">
	      	</div>
	  </div>
	    <div>
      		<div id="divCuadraturaLogCargaIncomingPP" class="mx-auto">
	  		</div>
      </div>
	     
	 

      <br style="line-height:5px"/>
      
   </body>
</html>