<jsp:include page="loginCheck.jsp" flush="true" />
<%@page import = "Beans.Usuario" %>
<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>
<script type="text/javascript">
	var jsonSalida = new Object();
	var lista = "";

	$(document)
			.ready(	
					function() {
						$
								.ajax({
									url : 'listaCargoAbonoMntAction',
									type : 'POST',
									dataType : "json",
								})
								.done(
										function(resp) {

											var datos = (resp.listaCargoAbono)
													.split("~");
											if (datos[0].split(":")[1] == 0) {
												for (var i = 1; i < datos.length - 1; i++) {
													var fila = datos[i]
															.split("|");

													var $tabla = $("#tablaRes");

													if (fila[3] == "C") {
														fila[3] = "Cargo";
													} else if (fila[3] == "A") {
														fila[3] = "Abono";
													}
													
													var textoAppend = '<tr class="text-center" id="trFuncion">'
																	+ '<td class="align-middle" style="display:none;">'
																	+ fila[0]
																	+ '</td>'
																	// style="width:50px;"
																	+ '<td class="align-middle">'
																	+ fila[1]
																	+ '</td>'
																	// style="width:300px;height:19px;"
																	+ '<td class="align-middle">'
																	+ fila[2]
																	+ '</td>'
																	// style="width:100px;"
																	+ '<td class="align-middle">'
																	+ fila[3]
																	+ '</td>'
																	// style="width:135px;"
																	+ '<td class="align-middle">'
																	+ fila[4]
																	+ '</td><td class="align-middle">'
																	+ '<select id="select'+i+'" class="combo custom-select"><option value="1">Activo</option><option value="0">Inactivo</option>'
																	+ '</td>';
													<% if(usuarioLog.getActualizacion() == 1){ %>
														textoAppend = textoAppend + '<td class="align-middle">'
																	// style="display: none;"
																	+ '<button id="btn-'+i+'" type="button" value="'+fila[5]+'" class="btn btn-primary" disabled>Confirmar</button>'
																	+ '</td>'
																	+ '</tr>';
													<% }else{ %>
														textoAppend = textoAppend + '<td class="align-middle"></td></tr>';
													<% } %>

													$tabla.append(textoAppend);

													$("#select"+ i+ " option[value='"+ fila[5]+ "']")
															.prop("selected",
																	true);
													
													/* "height" : "19px" */
													$('#select' + i).css({
														"font-size" : "9pt"
													});
												}
												// SE COMENTA ESTE for PARA EVITAR QUE APAREZCAN FILAS VACIAS
												/* for (var x = 0; x < 20 - (datos.length - 2); x++) {
													$tabla
															.append('<tr class="text-center" id="trFuncion">'
																	+ '<td class="align-middle" style="display:none;">&nbsp;</td>'
																	// style="width:50px;height:19px;"
																	+ '<td class="align-middle" >&nbsp;</td>'
																	// style="width:300px;"
																	+ '<td class="align-middle" >&nbsp;</td>'
																	// style="width:100px;"
																	+ '<td class="align-middle" >&nbsp;</td>'
																	// style="width:135px;"
																	+ '<td class="align-middle" >&nbsp;</td>'
																	// style="width:*;"
																	+ '<td class="align-middle">&nbsp;</td>'
																	// style="width:*;"
																	+ '<td class="align-middle">&nbsp;</td>'
																	+ '</tr>');
												} */
											}
										})
								.error(function(error, status, setting) {
									alert("Error TipoTransac: " + error);
								});



					
						$("#tablaRes").on('change', '.combo', function() {
											
								var sid = $(this).parent().parent().find('td').eq('0').text();
								
								var estado = $(this).val();
							
							    mostrarBotonConfirmar(sid, estado);
								
																			
						});
						
				
					
				
				
				function mostrarBotonConfirmar(id, estadoActual){
					var prefixBtn = "btn-";
					
					var estadoOriginal = $('#'+prefixBtn+id).val(); 
					
					// SI CAMBIO EL ESTADO
					if(estadoOriginal != estadoActual){
					    $('#'+prefixBtn+id).bind("click",function(e){actualizarEstado(id, estadoActual); });	
						// $('#'+prefixBtn+id).show();
						$('#'+prefixBtn+id).prop("disabled", false);
					}else{
						//$('#'+prefixBtn+id).hide();
						$('#'+prefixBtn+id).prop("disabled", true);
					}
					
				}
								
				
				
				function actualizarEstado(sid, estado){
				
						$.ajax({
								url : 'cambioEstadoCargoAbonoAction',
								type : 'POST',
								data : "sid="+ sid
								     + "&estado=" + estado
							  }).done(
									   function(data) {
									   var prefixBtn = "btn-";

									   	$('#'+prefixBtn+data.sid).val(data.estado);
									   	//$('#'+prefixBtn+data.sid).hide();
									   	$('#'+prefixBtn+data.sid).prop("disabled", true);
									   	$('#'+prefixBtn+sid).unbind("click");
									   	
									   	alert("Cargo y abono se ha actualizado correctamente");
									   
									   
							  }).error({
							  
							  }); 
				
				}
				
				
					
						
			});
</script>

<!-- div class="titulo">  -->
<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
	PAR&Aacute;METROS DE CARGOS Y ABONO
</div>
<hr/>
<!-- <div class="cajaContenido">  -->
	<div>
	<!-- <div>
		class="tablaContenido"
		<table width="100%" class="table table-sm small">
			<tr class="text-center">
				<th class="align-middle" style="width: 50px;">CODIGO</th>
				<th class="align-middle" style="width: 300px;">DESCRIPCION</th>
				<th class="align-middle" style="width: 100px;">TIPO</th>
				<th class="align-middle" style="width: 135px;">PRODUCTO</th>
				<th class="align-middle" style="width: *;">ESTADO</th>
			</tr>
		</table>
	</div> -->
	<div id="tablaResDiv">
		<!-- class="tablaContenido" -->
		<table width="100%" class="table table-sm small" id="tablaRes">
			<tr class="text-center">
				<th class="align-middle" >CODIGO</th>
				<th class="align-middle" >DESCRIPCION</th>
				<th class="align-middle" >TIPO</th>
				<th class="align-middle" >PRODUCTO</th>
				<th class="align-middle" >ESTADO</th>
				<th class="align-middle" >ACCION</th>
			</tr>
		</table>
	</div>
</div>

<!--div class="botoneraExterna">
        <div>
        	<input type="button" value="Salir"/>
          	<input type="button" value="Ingresar" />
          	<input type="button" value="Modificar" />
          	<input type="button" value="Eliminar" />
        </div>
    </div-->
