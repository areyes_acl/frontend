<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>

<html>
<head>
<title>Cargos/Abonos Pendientes</title>
</head>

<body>
	<form id="form1">
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
			GESTI&Oacute;N DE CARGOS Y ABONOS PENDIENTES
		</div>

		<!-- <br style="line-height: 5px" />  -->
<hr/>
		<div>
			<!-- contenedorInput -->
			<table class="table table-sm" style="width: 100%;">
				<tr class="text-right">
					<td class="align-middle">NRO. TC :</td>
					<td class="align-middle "><input type="text" name="numerotarjeta" id="numerotarjeta"
						maxlength="16" class="numericOnly form-control form-control-sm text-center py-0" /></td>
					<td class="align-middle">DESDE :</td>
					<td class="align-middle text-center">
						<!-- size="10" -->
						<input type="text" name="fechaDesde" id="fecha-desde"
							maxlength="10" class="campoObligatorio fecha form-control form-control-sm text-center col-8 mx-auto datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td class="align-middle">HASTA :</td>
					<td class="align-middle text-center">
						<!-- size="10" -->
						<input type="text" name="fechaHasta" id="fecha-hasta"
							maxlength="10" class="campoObligatorio fecha form-control form-control-sm text-center col-8 mx-auto datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td rowspan="2" class="align-middle text-center"><input type="button" id="buscar-cargoabono" name="buscar"
						value="Buscar" class="btn btn-primary p-2"></td>
				</tr>
				<!-- <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr> -->
				<tr class="text-right">
					<td class="align-middle">TIPO :</td>
					<td colspan="2"class="align-middle"><select id="tipo" class="custom-select">
							<option value="TDO">Todos</option>
							<option value="CARGO">Cargos</option>
							<option value="ABONO">Abonos</option>
					</select></td>
					<td class="align-middle">ESTADO:</td>
					<td colspan="2" class="align-middle"><select id="estado" class="custom-select">
							<option value="TDO">Todos</option>
							<option value="CA_PEN">Pendiente</option>
							<option value="CA_OK">Enviado</option>

					</select></td>
				</tr>


			</table>
		</div>


		<!-- <br style="line-height: 5px" />  -->

		<!-- <div class="subTitulo">  -->
 		<div id="tablaResTitulo" style="display: none;"
				class="col-6 small mx-auto mt-5 mb-3 text-align-center text-center font-weight-bold">
			Listado de Cargos/Abonos Pendientes de SOL60
		</div>

		<!-- cajaContenido -->
		<div>
			<div id="tablaRes"></div>

		</div>

		<!-- <br style="line-height: 5px" />  -->


		<!-- class="contenedorInput" -->
		<table class="table table-sm table-borderless mt-3" style="width: 100%;">
			<tr>
				<td class="align-middle" align="center" width="33%"><input type="button"
					value="Exportar" name="exportar"
					id="exportar" disabled class="btn btn-primary"/></td>
			</tr>
		</table>


	</form>
</body>
<script type="text/javascript" src="js/cargoabono/cargoabonoPend.js"></script>
<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script>
	$(".datepicker").datepicker();
</script>
</html>