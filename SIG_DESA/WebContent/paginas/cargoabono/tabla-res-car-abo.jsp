<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>
<html>
<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
			$(document).ready(function(){
			
			var cantidadElementos  = $("#cant-registros").val();
			
			if(cantidadElementos > 0){
				$("#exportar").prop("disabled", false);
			}else{
				$("#exportar").prop("disabled", true);
			}
											
				if($("#numPagina").val()==1)
				{	
					$("#anterior").bind("click",function(e){
				        e.preventDefault();
			        });
				}else{
					$("#anterior").unbind("click",false);					
				}
				
				if($("#ultimaPagina").val()<=1)
				{
					$("#botonesPaginacion").hide();
				}
				
				if($("#ultimaPagina").val()==0)
				{
					$("#anterior").hide();
					$("#siguiente").hide();
				}
				
				$("#siguiente").click(function(){
				
					if($("#numPagina").val() < $("#ultimaPagina").val())
					{
						var numPag = parseInt($("#numPagina").val())+1;
						buscaCargosAbonos($("#fecha-desde").val(),$("#fecha-hasta").val(),numPag);
					}
				});
				
				$("#ir").click(function(){
					
					var paginaIr;
					var ultimaPagina;
					
					paginaIr = $("#paginaIr").val();
					
					ultimaPagina = $("#ultimaPagina").val();
					
					 
					if( paginaIr == '' || paginaIr == null || typeof paginaIr == "undefined" || paginaIr == 0 || isNaN(paginaIr)){
						alert("La página no existe");
						$("#paginaIr").val("");
						return;
					}
					
					if(Number(paginaIr) > Number(ultimaPagina) || Number(paginaIr) < 0 )
					{
						alert("La página no existe");
						$("#paginaIr").val("");
					}else{
						var numPag = paginaIr;
						buscaCargosAbonos($("#fecha-desde").val(),$("#fecha-hasta").val(),numPag);
					}
						
				});
				
				$("#anterior").click(function(){
					if($("#numPagina").val() > 1)
					{
						var numPag = parseInt($("#numPagina").val())-1;
						buscaCargosAbonos($("#fecha-desde").val(),$("#fecha-hasta").val(),numPag);
					}
				});
			});


</script>

<style type="text/css">
#btn-delete:active {
	opacity: 0.3;
}
</style>
</head>
<body>
	<div></div>
	<input type="hidden"
		value="<s:property value="%{#request.cantidadElementos}" />"
		name="cantidadRegistros" id="cant-registros" />
	<input type="hidden"
		value="<s:property value="%{#request.ultimaPagina}" />"
		name="ultimaPagina" id="ultimaPagina" />
	<input type="hidden"
		value="<s:property value="%{#request.numPagina}" />" name="numPagina"
		id="numPagina" />
	<input type="hidden"
		value="<s:property value="%{#request.fechaInicioPag}" />"
		name="fechaInicioPag" id="fechaInicioPag" />
	<input type="hidden"
		value="<s:property value="%{#request.fechaTerminoPag}" />"
		name="fechaTerminoPag" id="fechaTerminoPag" />
	<input type="hidden"
		value="<s:property value="%{#request.numeroTarjeta}" />"
		name="numeroTarjetaPag" id="numeroTarjetaPag" />


	<div
		style="width: 800px; overflow-x: auto; overflow-y: auto; max-height: 400px;">
		<div style="margin: 0px; padding: 0px;">
			<!-- class="tablaContenido" -->
			<table style="width: 100%" class="table table-sm small" id="myTable">


				<tr class="text-center">
					<th class="align-middle">NRO TC</th>
					<th class="align-middle">FECHA TRANSACC.</th>
					<th class="align-middle">COMERCIO</th>
					<th class="align-middle">MONTO TRANSACC.</th>
					<th class="align-middle" title="Monto cargo y abono">MONTO CA.</th>
					<th class="align-middle">ESTADO</th>
					<th class="align-middle">COD. RAZ&Oacute;N</th>
					<th class="align-middle">Nº INCIDENTE</th>
					<th class="align-middle">TIPO</th>
					<th class="align-middle">USUARIO</th>
					<th class="align-middle" title="Fecha cargo y abono">FECHA CA.</th>
					<th class="align-middle">ACCION</th>
				</tr>

				<s:iterator value="listaCargoAbono">
					<tr class="text-center">
						<td class="align-middle" style="display: none;"><label id="sid"><s:property
									value="sid" /></label></td>
						<td class="align-middle"><s:property value="numeroTarjeta" /></td>
						<td class="align-middle" style="text-align: center;"><s:property
								value="fechaTransaccion" /></td>
						<td class="align-middle"><s:property value="comercio" /></td>
						<td class="align-middle" style="text-align: center;"><s:property
								value="montoOriginal" /></td>
						<td class="align-middle" style="text-align: center;"><s:property
								value="montoCargoAbono" /></td>
						<td class="align-middle"><s:property value="estado.description" /></td>
						<td class="align-middle" style="text-align: center;"><s:property value="codRazon" /></td>
						<td class="align-middle"><s:property value="numIncidente" /></td>
						<td class="align-middle"><s:property value="tipo.xkey" /></td>
						<td class="align-middle"><s:property value="username" /></td>
						<td class="align-middle"><s:property value="fechaCargoAbono" /></td>
						<% if(usuarioLog.getEliminacion() == 1){ %>
							<s:if test="%{estado.sid == 6 }">
								<td class="align-middle"><img id="btn-delete"
									src="${pageContext.request.contextPath}/img/trash.png"
									style="width: 15px; padding: 2px; cursor: pointer;"
									onclick="eliminarCargoAbono(<s:property value="sid" />)">
								</td>
							</s:if>
							<s:else>
<!-- 								<td class="align-middle"></td> -->
							</s:else>
						<% } %>
					</tr>
				</s:iterator>
			</table>
		</div>
	</div>


	<div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial, Helvetica, sans-serif; font-size: 8pt;">
		<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		<s:property value="%{#request.numPagina}" />
		De
		<s:property value="%{#request.ultimaPagina}" />
		P&aacute;gina(s)&nbsp;&nbsp;&nbsp; Ir a pagina <input type="text"
			id="paginaIr" style="width:50px;font-size: 8pt;" class="form-control form-control-sm d-inline py-0 align-middle" /> <input type="button"
			value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt;" />
	</div>


</body>
</html>