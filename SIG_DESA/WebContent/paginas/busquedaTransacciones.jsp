<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>B&Uacute;SQUEDA DE TRANSACCIONES</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       <script src="js/jquery.Rut.js" type="text/javascript"></script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
	   
	<script>
		$(".datepicker").datepicker();
	
		
		$(document).ready(function (){

		
			// MODIFICACION PARA ADAPTACION DE SISTEMA DE INTECAMBIO VISA 
				$.ajax({
					url:"cargaTipoDeBusquedaTransac",
					type:'POST'
				}).done(function(data){
					var dinamicRadioButtonTable = "";
					for(var i = 0 ; i < data.listaTipoTransacciones.length ; i++){
						// SI EL RESTO DE LA DIVISION DA 0 SE ABRE UN TR
						if( i % 4 == 0){
							dinamicRadioButtonTable = dinamicRadioButtonTable + '<tr>';
						}
						// SI ES EL PRIMER ELEMENTO CREA UN TD CON EL RADIO BUTTON CHECKEADO
						if( i  == 0 ){
							// <input class="tipotransaccion" />
							dinamicRadioButtonTable = dinamicRadioButtonTable + '<td><div class="form-check custom-control custom-radio"><input type="radio" name="tipotransaccion" onclick="limpiartTablaResultados();" class="custom-control-input" id="radio'+i+'" value="'+data.listaTipoTransacciones[i].codigoxKey+'" checked="checked" ><label for="radio'+i+'" class="custom-control-label">'+data.listaTipoTransacciones[i].descripcion+'</label></div></td>';
						// DE CASO CONTRARIO CREA UN TD CON UN RADIO BUTTON SIN CHECKEAR
						}else{
							dinamicRadioButtonTable = dinamicRadioButtonTable + '<td><div class="form-check custom-control custom-radio"><input type="radio" name="tipotransaccion" onclick="limpiartTablaResultados();" class="custom-control-input" id="radio'+i+'" value="'+data.listaTipoTransacciones[i].codigoxKey+'"><label for="radio'+i+'" class="custom-control-label">'+data.listaTipoTransacciones[i].descripcion+'</label></div></td>';
						}
						// SI ES MULTIPLO DE 3 CIERRA EL TR
    					if( i % 4 == 3){ 
    						dinamicRadioButtonTable = dinamicRadioButtonTable + '</tr>';
    					};
					};
					
						//dinamicRadioButtonTable = dinamicRadioButtonTable +"<tr><td colspan='4'>&nbsp; <td></tr>";	
					
					// ADJUNTA A LA TABLA LOS RADIO BUTTON FORMADOS
					$("#tabla-tipo-busqueda").append(dinamicRadioButtonTable);
		
				});
		
		
			var f = new Date();
		
			var dia = f.getDate();
			var mes = (f.getMonth() +1);
			if(mes < 10)
				mes = "0"+mes;
			if(dia < 10){
				dia = "0"+dia;
			}
			var ano = f.getFullYear();
			$(".fecha").val(dia + "/" + mes + "/" + ano);
			
			$(".fecha").click(function(){
				 $(this).val("");
				 $(this).addClass("campoObligatorio");
				 limpiarDatosDeTransaccion();
			});
			
			$("#componente").val("Primer Contracargo Total");	
			
			//MOD
			$("#buscarTransaccion").click(function(){
				
				limpiarDatosCliente();
				limpiarDatosDeTransaccion();
				
				$("#tablaRes").css("display","none");
				var fechaTermino = "";
				if ($('#fechaInicio').val().length  == 0){
					alert("Debe ingresar fecha de inicio para realizar la busqueda...");
					$("#fechaInicio").focus();
					return;
				}
				
				if (validarFecha($('#fechaInicio').val()) == false){
						alert("La fecha ingresada esta incorrecta, verifiquela...");
						$("#fechaInicio").focus();
						return;
				}
					
				
				
				if ($('#fechaTermino').val().length  == 0){
					f = new Date();
					
					fechaTermino = convertirFecha(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
				}else{
				
					if (validarFecha($('#fechaTermino').val()) == false){
							alert("La fecha ingresada esta incorrecta, verifiquela...");
							$("#fechaFin").focus();
							return;
					}
					
					fechaTermino = convertirFecha($('#fechaTermino').val());
				}
				
					
				
			
				var fechaInicio = convertirFecha($('#fechaInicio').val());
				
				
				if(fechaInicio > fechaTermino){
					alert("La fecha de inicio es mayor a la fecha de termino, verifiquela...");
					$("#fechaInicio").focus();
					return;
				}
				var valor = $("input[name='tipotransaccion']:checked").val();			
				$("#tipoObjecionRechazo").val(valor);
				//alert($("#form1").serialize());
				//Alberto Contreras	
				$.ajax({
					url:"listarObjecionRechazoAction",
					//url:"paginas/tablaTest.jsp",
					type:'POST',
					data:$("#form1").serialize()
				}).done(function(data){
					//alert(data);
					$("#tablaRes").empty().append(data);
					$("#tablaRes").css("display","");
				});
				
				
				
			});
			//MOD
			
			
			
			
			
			
			$("#tablaRes").on('click','.seleccionTRX',function(){	
			
				$("#microfil").val($(this).parent().parent().find('td').eq('10').text());
				$("#codigoautorizacion").val($(this).parent().parent().find('td').eq('11').text());
				$("#fechaefectiva").val($(this).parent().parent().find('td').eq('12').text());
				$("#fechaproceso").val($(this).parent().parent().find('td').eq('13').text());
				$("#binadquirente").val($(this).parent().parent().find('td').eq('14').text());
				$("#leebanda").val($(this).parent().parent().find('td').eq('15').text());
				$("#otrodato01").val($(this).parent().parent().find('td').eq('16').text());
				$("#otrodato02").val($(this).parent().parent().find('td').eq('17').text());
				$("#otrodato03").val($(this).parent().parent().find('td').eq('18').text());
				$("#otrodato04").val($(this).parent().parent().find('td').eq('19').text());
				$("#codigomonedatrx").val($(this).parent().parent().find('td').eq('20').text());
				$("#rubrocomercio").val($(this).parent().parent().find('td').eq('21').text());
				$("#operador").val($(this).parent().parent().find('td').eq('28').text());
				
				var numeroTarjeta = $(this).parent().parent().find('td').eq('1').text();
				
				//test
				//var numeroTarjeta = "9200610156088010";
				//test
				$.ajax({
					url:"llamadaWS",
					type:'POST',
					data:"numeroTarjeta="+numeroTarjeta
				}).done(function(resp){
					var aux = resp.cadena.split("~");

					if(aux[0].split("|")[1] == 0)
					{
						var datos = aux[1].split("|");
						$("#nomcliente").val(datos[0]);
						var rut = datos[1];
						$("#rutcliente").val(rut);
						$("#numerocuenta").val(datos[2]);
						$("#cupo").val(datos[3]);
						$("#fechaexpiracion").val(datos[4]);
						$("#estado").val(datos[5]);
					}else{
						
						var mensaje = aux[1].split("|");
						
						if (mensaje=="Numero de tarjeta erroneo"){
							alert("No existen los datos de la cuenta del cliente para la tarjeta consultada...");
						}
						limpiarDatosCliente();
						
						
						//alert(aux[1]);
					}		
				}).error(function(error, status, setting){
					$("#error").empty().append(error);
				});
			});
			
			var valor = $("input:checked").val();
			
			$("#tipoObjecionRechazo").val(valor);
			
			$("input:radio").click(function(){

				limpiarDatosDeTransaccion();
				
				valor = $(this).val();
				$("#tipoObjecionRechazo").val(valor);
				
				$("#componente").val($(this).parent().text());
			});
			
			/*$("body").on('click','.fecha',function(){
				$('.fecha').datepicker({
					showOn: "button",
					buttonImage: "img/calendarblue.png",
					buttonImageOnly:true,
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
					monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
					dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
					dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
					dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
					firstDay: 1,
					dateFormat:'dd/mm/yy'
				});
			});	*/
			
			
		});
		
		function limpiarDatosCliente(){
						$("#nomcliente").val("");
						$("#rutcliente").val("");
						$("#numerocuenta").val("");
						$("#cupo").val("");
						$("#fechaexpiracion").val("");
						$("#estado").val("");
						
		};
		
		
		function limpiarDatosDeTransaccion(){
			
			 		$("#microfil").val("");
					$("#codigoautorizacion").val("");
					$("#fechaefectiva").val("");
					$("#fechaproceso").val("");
					$("#binadquirente").val("");
					$("#leebanda").val("");
					$("#otrodato01").val("");
					$("#otrodato02").val("");
					$("#otrodato03").val("");
					$("#otrodato04").val("");
					$("#codigomonedatrx").val("");
					$("#rubrocomercio").val("");
					$("#operador").val("");
		
		};
		
		
		function limpiartTablaResultados(){
				$("#tablaRes").empty();
				$("#tablaRes").css("display","none");
				limpiarDatosCliente();
				limpiarDatosDeTransaccion();
				
		};
		
		$("#exportar").click(function(){
			
			var parametros = "fechaInicio=" + $('#fechaInicio').val();
			if ($('#fechaInicio').val().length == 0) {
				alert("Debe ingresar fecha de inicio para exportar...");
				$("#fechaInicio").focus();
				return;
			}
			if ($('#fechaTermino').val().length == 0) {
				alert("Debe ingresar fecha de termino para exportar...");
				$("#fechaTermino").focus();
				return;
			}
			
			var fechaTermino = convertirFecha($('#fechaTermino').val());
			var fechaInicio = convertirFecha($('#fechaInicio').val());
						
			if (fechaInicio > fechaTermino) {
				alert("La fecha de inicio es mayor a la fecha de termino, verifiquela...");
				$("#fechaInicio").focus();
				return;
			}			
			if ($('#fechaTermino').val().length  == 0){
				f = new Date();
				parametros = parametros + "&fechaTermino=" + f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
				fechaTermino = convertirFecha(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
			}else{
			
				if (validarFecha($('#fechaTermino').val()) == false){
						alert("La fecha ingresada esta incorrecta, verifiquela...");
						$("#fechaFin").focus();
						return;
				}
				parametros = parametros + "&fechaFin=" + $('#fechaTermino').val();
				fechaTermino = convertirFecha($('#fechaTermino').val());
			}
			parametros += "&nombreArchivo=" + "export"+"&componente="+$('#tipoObjecionRechazo').val();
		    
		    location.href="./exportarDatosExcelObjRechAction?" + parametros;
			
		});

				
	</script>
</head>

<body>
	<form id="form1">
		<input type="hidden" value="15" name="cantReg" id="cantReg"/>
		<div id="popup" style="display: none;">
			<div id="popUpContenido" class="content-popup"></div>
		</div>
	    
	    <!-- <div class="titulo"> -->
	    <div class="col-12 mx-auto mt-4 text-align-center text-center font-weight-bold">
       		B&Uacute;SQUEDA DE GESTIONES
	    </div>
	    
	    <hr/>
	    
	    <div>
	    
	    <!-- class="contenedorInput" -->
    	<table width="100%" class="table table-sm mt-4" id="tabla-tipo-busqueda">
    	
    	
    
    	</table>
    </div>
	    
	    <input type="hidden" name="tipoObjecionRechazo" id="tipoObjecionRechazo" value="REP" />
	    <input type="hidden" id="serie" />
	    <input type="hidden" id="idTabla" value="tablaRes" />
	    
	    <input type="hidden" id="componente" name="componente" />
	    
	    <!-- <br style="line-height:5px"/>  -->
	    
	    <div class="my-4">
	    	<!-- class="contenedorInput" -->
	    	<table width="100%" class="table my-0 table-sm mx-auto">
	    	<tbody>
	    		<tr class="text-center">
			        <!--td>&nbsp;N&deg; TC :&nbsp;</td>
		        	<td><input type="text" value="" name="numeroTarjeta" id="numeroTarjeta"  class="campoObligatorio"/></td-->
			        <td class="text-right align-middle">
			        	<label class="my-0">Fecha Inicio :</label>
			        </td>
			        
			        <td class="align-middle">
			        	<input type="text" size="10" maxlength="10" value="" name="fechaInicio" 
			        		id="fechaInicio" class="campoObligatorio fecha form-control form-control-sm px-0 col-12 text-center mx-auto datepicker" placeholder="DD/MM/AAAA" />
			        </td>
			        
			        <td class="text-right align-middle">
			        	<label class="my-0">Fecha Final :</label>
			        </td>
			        
			        <td class="align-middle">
			        	<input type="text" size="10" maxlength="10" value="" name="fechaTermino" 
			        		id="fechaTermino" class="campoObligatorio fecha form-control form-control-sm col-12 text-center px-0 mx-auto datepicker" placeholder="DD/MM/AAAA" />
			        </td>
			        
			        <td colspan="4" align="center" class="align-middle"> 
			        	<input type="button" value="Buscar" name="buscarTransaccion" 
			        		id="buscarTransaccion"/ class="btn btn-primary"/>
			        </td>
			       
		      	</tr>
		     </tbody>
	    	</table>
	    </div>
	    
	    <!-- <br style="line-height:5px"/> -->
	
	     <div class="cajaContenido">
    	  <div>
			<!-- <table width="100%" class="tablaContenido" id="tablaBusqueda" > -->
			<!--<table width="736" class="tablaContenido" id="tabla1" > 
		    	<tr>
			        <th width='5%'>Sel</th>
		        	<th width='15%'>NRO TC</th>
			        <th width='15%'>FECHA TRANSACC.</th>
			        <th width='20%'>COMERCIO</th>
			        <th width='5%'>PA&Iacute;S</th>
			        <th width='10%'>MONTO ORIG.</th>
			        <th width='10%'>MONTO</th>
			        <th width='10%'>COD. RAZON</th>
			        <th width='10%'>ESTATUS</th>
			  	</tr>
		    </table>-->
		    </div>
	    	<div id="tablaRes">
	    		
	    	</div>
		    
	   </div>
		  <!-- <br style="line-height:5px"/>  -->
		    
	    <div>
	    	<!-- class="contenedorInput" -->
	    	<table  width="100%" class="table-borderless">
	    		<tr>
	    		    <td align="center" class="align-middle">
	    				<input type="button" id="exportar" id="exportar" value="Exportar Tabla" class="btn btn-primary"/>
	    			</td>
	    		</tr>
	    	</table>
	    </div>  
	   <!-- <br style="line-height:5px"/> -->
		
		<hr size="2px">
	
	   <!-- class="subTitulo" -->
		<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
			font-weight-bold">
	   		Datos De La Cuenta Del Cliente
	   </div>
				    	
	   <div class="mb-4">
	 	    <!-- class="contenedorInput  -->
			<table width="100%" class="table table-sm small">
		    	<tr>
	    			<td>Cliente :</td>
	    			<td>
	    				<input type="text" name="nomcliente" id="nomcliente" 
	    					class="campoSalida form-control form-control-sm" size="50"/>
   					</td>
	    			
	    			<td>Rut :</td>
	    			<td>
	    				<input type="text" name="rutcliente" id="rutcliente" 
	    					class="campoSalida form-control form-control-sm"/>
   					</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>N° De Cuenta :</td>
	    			<td><input type="text" class="campoSalida form-control form-control-sm" name="numerocuenta" id="numerocuenta"/></td>
	    			<td>Cupo :</td>
	    			<td><input type="text" class="campoSalida form-control form-control-sm" name="cupo" id="cupo"/></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Fecha Exp.:</td>
	    			<td><input type="text" class="campoSalida form-control form-control-sm" name="fechaexpiracion" id="fechaexpiracion" /></td>
	    			<td>Estado :</td>
	    			<td><input type="text" class="campoSalida form-control form-control-sm" name="estado" id="estado" /></td>
	    		</tr>
	    
		    </table>
		</div>
	
		<!-- <br style="line-height:5px"/> -->
		
		<!-- class="subTitulo" -->
		<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
			font-weight-bold">
		  		Datos De La Transacci&oacute;n Seleccionada
		  	</div>
		  	
		<div>
			<!-- class="contenedorInput" -->
			<table width="100%" class="table table-sm small">
	    	
	    		<tr>
	    			<td>Microfilm:</td>
	    			<td><input type="text" name="microfil" id="microfil" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Cod. Autorizac:</td>
	    			<td><input type="text" name="codigoautorizacion" id="codigoautorizacion" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Cod.Proces:</td>
	    			<td><input type="text" name="otrodato01" id="otrodato01" class="campoSalida form-control form-control-sm"/></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Fecha Efectiva:</td>
	    			<td><input type="text" name="fechaefectiva" id="fechaefectiva" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Fecha Proceso:</td>
	    			<td><input type="text" name="fechaproceso" id="fechaproceso" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Conv.Conc:</td>
	    			<td><input type="text" name="otrodato02" id="otrodato02" class="campoSalida form-control form-control-sm"/></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Bin Adquiriente:</td>
	    			<td><input type="text" name="binadquirente" id="binadquirente" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Lee Banda:</td>
	    			<td><input type="text" name="leebanda" id="leebanda" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Conv.Fact:</td>
	    			<td><input type="text" name="otrodao03" id="otrodato03" class="campoSalida form-control form-control-sm"/></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>C&oacute;digo Moneda Trx:</td>
	    			<td><input type="text" name="codigomonedatrx" id="codigomonedatrx" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Rubro Del Comercio:</td>
	    			<td><input type="text" name="rubrocomercio" id="rubrocomercio" class="campoSalida form-control form-control-sm"/></td>
	    			<td>Pto. Servicio:</td>
	    			<td><input type="text" name="otrodao04" id="otrodato04" class="campoSalida form-control form-control-sm"/></td>
	    		</tr>
	    		<tr>
	    			<td>Operador:</td>
	    			<td class="border-right-0"><input type="text" name="operador" id="operador" class="campoSalida form-control form-control-sm" size="40"></td>
	    			<td class="border-left-0" colspan="4"></td>
	    			
    			<!-- 
	    			<td></td>
	    			<td></td>
	    			<td></td>
	    			<td></td>
	    		 -->
	    		</tr>
	    	</table>
		</div>
	    	
	    	
 	</form>
</body>
</html>