<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Gestion de transaccion</title>
	    <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
		<script src="js/jquery.Rut.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/utils.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				var tipoTransaccion =  $("input[name='tipotransaccion']:checked").val();
				var url;
				var parametros;			
				var xKey = tipoTransaccion.split("_", 2);
				var tipoBusqueda = $("#tipoFechaBusqueda").val();
			
				
				// Transaccion De Presentacion.//
				if (xKey[0] == '1240' && xKey[1] == '200') { 
					url = "transaccionPresentacionAction";
				}
				
				if (xKey[0] == '1442' || xKey[0] == '1644' || xKey[0] == '1240') {
					url = "transaccionAccionesAction";
				}
				
				// Transaccion De Objeciones Rechazos.  // 
				if (xKey[0] == 'REC' || xKey[0] == 'OBJ') { 
					url = "transaccionObjecionRechazoAction";
				}
				
				//Transaccion Recuperacion De Cargos Incoming.  //
				if (xKey[0] == '1740') { 
					url = "transaccionRecuperacionCargosIncAction";
				}
				
				//Transaccion Confirmacion de recepcion de vales.  //
				if (xKey[0] == 'CONF_RVL') { 
						url = "transaccionConfirmacionRecepcionPeticionValesAction";
				}
				
				
				//LOGICA TARJETA VISA
				if((xKey[0] == '05' || xKey[0] == '06' || xKey[0] == '07') && xKey[1] == '00'){
						url = "transaccionPresentacionVisaAction";
				}
				
				if((xKey[0] == '05' || xKey[0] == '06' || xKey[0] == '07') && xKey[1] == '205'){
						url = "transaccionPresentacionVisaAction";
				}
				
				if((xKey[0] == '25' || xKey[0] == '26' || xKey[0] == '27') && xKey[1] == '00'){
						url = "transaccionPresentacionVisaAction";
				}
				
				if(xKey[0] == '15' && xKey[1] == '00'){
						url= "transaccionAccionesAction";
 				}
				
				if(xKey[0] == '15' && xKey[1] == '205'){
						url= "transaccionAccionesAction";
				}
							
				if(xKey[0] == '52' && xKey[1] == '00'){
					url= "transaccionConfirmacionRecepcionPeticionValesAction";
				}
				
				if(xKey[0] == '00' && xKey[1] == '00'){
						url= "transaccionPresentacionVisaAction";
				}
				
				if(xKey[0] == '01' && xKey[1] == '00'){
					url= "transaccionOnUsVisaAction";
				}
				
				if(xKey[0] == '02' && xKey[1] == '00'){
						url= "transaccionOnUsVisaAction";
				}
							
				// AVANCE ONUS
				if(xKey[0] == '03' && xKey[1] == '00'){
					url= "transaccionOnUsVisaAction";
				}
				
				// AVANCE ONUS
				if(xKey[0] == '04' && xKey[1] == '00'){
						url= "transaccionOnUsVisaAction";
				}

				parametros = "numeroTarjeta=" + $('#numeroTarjetaPag').val();
				parametros = parametros + "&fechaInicio=" + $('#fechaInicioPag').val();
				parametros = parametros + "&fechaTermino=" + $('#fechaTerminoPag').val();
				parametros = parametros + "&op=" + xKey[0];
				parametros = parametros + "&codigoFuncion=" + xKey[1];
				parametros = parametros + "&codigoTransaccion=" + xKey[0];
				parametros = parametros + "&cantReg=" + $('#cantReg').val();
				parametros = parametros + "&tipoBusqueda=" + tipoBusqueda;
			
				
				if($("#numPagina").val()==1)
				{	
					$("#anterior").bind("click",function(e){
				        e.preventDefault();
			        });
				}else{
					$("#anterior").unbind("click",false);					
				}
				
				
				if($("#ultimaPagina").val()<=1)
				{
					$("#botonesPaginacion").hide();
				}
			
				if($("#ultimaPagina").val()==0)
				{
					$("#anterior").hide();
					$("#siguiente").hide();
				}
				
				
				$("#siguiente").click(function(){
					if(parseInt($("#numPagina").val()) < parseInt($("#ultimaPagina").val()))
					{
						var numPag = parseInt($("#numPagina").val())+1;
						parametros = parametros + "&numPagina="+numPag;
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
				});
				
				$("#anterior").click(function(){
					if(parseInt($("#numPagina").val()) > 1)
					{
						var numPag = parseInt($("#numPagina").val())-1;
						parametros = parametros + "&numPagina="+numPag;
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
				});
				
				$("#ir").click(function(){
					
					var paginaIr;
					var ultimaPagina;
					
					paginaIr = $("#paginaIr").val();
					
					ultimaPagina = $("#ultimaPagina").val();
					
					if( paginaIr == '' || paginaIr == null || typeof paginaIr == "undefined"){
						alert("La página no existe");
						$("#paginaIr").val("");
						return;
					}
					
					if(Number(paginaIr) > Number(ultimaPagina) || Number(paginaIr) < 0 )
					{
						alert("La página no existe");
						$("#paginaIr").val("");
					}else{
						var numPag = paginaIr;
						
						parametros = parametros + "&numPagina="+$("#paginaIr").val();
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
						
				});
				
			
			});
			
			
			/* function showDescription(description){			
			
				//var desc =  description.replace(/\r?\n/g, "<br>");
    					$('#descripcion').append('<div id="page"> \ <p class="texto">'+description+' \ </div>' );
    					// css("display","block");
    					$("#tablaDescripcion").css("display","");
				
			} */
			
			function showDescription(description) {
				if(description === null || description === "") {
					$('#descripcion').append(
						'<div id="page"> \ <p class="texto">Sin descripcion \ </div>');
					$("#tablaDescripcion").css("display", "");
				}
				else {
					$('#descripcion').append(
						'<div id="page"> \ <p class="texto">' + description
								+ ' \ </div>');
					$("#tablaDescripcion").css("display", "");
				}
			}
			
			function hideDescription(){
				$('#descripcion').html("");
				$("#tablaDescripcion").css("display","none");
			}
		</script>
	</head>
	<body>
		<div></div>
		<input id="ultimaPagina" name="ultimaPagina" type="hidden" value="<s:property value="%{#request.ultimaPagina}" />"/>
		<input type="hidden" value="<s:property value="%{#request.numPagina}" />" name="numPagina" id="numPagina"/>
		
		<input type="hidden" value="<s:property value="%{#request.fechaInicioPag}" />" name="fechaInicioPag" id="fechaInicioPag"/> 
		<input type="hidden" value="<s:property value="%{#request.fechaTerminoPag}" />" name="fechaTerminoPag" id="fechaTerminoPag"/>
		<input type="hidden" value="<s:property value="%{#request.numeroTarjeta}" />" name="numeroTarjetaPag" id="numeroTarjetaPag"/> 
		 
		 
	 	<div style="overflow-x:auto;overflow-y:auto;max-height:200px;">
			 <div style="margin:0px;padding:0px;">
			 	<!-- tablaContenido -->
				<table width="100%" class="table table-sm small" id="myTable" >
				
				
				<tr class="text-center">
						<th class="align-middle">SEL</th>
						<th class="align-middle">NRO TC</th>
						<th class="align-middle">FECHA TRANSACC.</th>
						<th class="align-middle">COMERCIO</th>
						<th class="align-middle">PAIS</th>
						<th class="align-middle">MONTO ORIG</th>
						<th class="align-middle">MONTO</th>
						<th class="align-middle">COD. RAZON</th>
						<th class="align-middle">ESTATUS</th>
						<th class="align-middle">DESCRIPCION</th>
						<th class="align-middle">Nº INCIDENTE</th>
						<th class="align-middle">USUARIO</th>
						<th class="align-middle">FECHA GESTION</th>
						<th class="align-middle">PATPASS</th>
					</tr>
				 
			   		<s:iterator id="data" value="%{#request.listaTransacciones}" status="radioPos">
				   		<tr class="text-center">
							<td class="align-middle">
								<div class="align-middle custom-control custom-radio">
									<input id='el<s:property value="%{#radioPos.index}"/>' class="custom-control-input" type='radio' name='seleccionTRX' value='C' class='seleccionTRX' onclick='mostrarTransaccion(this);'>
									<label for='el<s:property value="%{#radioPos.index}"/>' class="custom-control-label"></label>
								</div>
							</td>
							<td class="align-middle"><s:property value="numeroTarjeta"/></td>
							<td class="align-middle" style="text-align: center;"><s:property value="fechaTransac"/></td>
							<td class="align-middle"><s:property value="comercio"/></td>
							<td class="align-middle" style="text-align: center;"><s:property value="pais"/></td>
							<td class="align-middle" style="text-align: center;"><s:property value="montoTransac"/></td>
							<td class="align-middle" style="text-align: center;"><s:property value="montoFacturacion"/></td>
							<td class="align-middle" style="text-align: center;"><s:property value="codRazon"/></td>
							<td class="align-middle"><s:property value="estadoTrx"/></td>
							<td class="align-middle" tbl="microfil" style="display:none"><s:property value="microFilm"/></td>
							<td class="align-middle" tbl="codigo_autorizacion" style="display:none"><s:property value="codigoAutorizacion"/></td>
							<td class="align-middle" tbl="fechaefectiva" style="display:none"><s:property value="fecjaEfectova"/></td>
							<td class="align-middle" tbl="fechaproceso" style="display:none"><s:property value="fechaProceso"/></td>
							<td class="align-middle" tbl="binadquirente" style="display:none"><s:property value="binAdquiriente"/></td>
							<td class="align-middle" tbl="leebanda" style="display:none"><s:property value="leeBanda"/></td>
							<td class="align-middle" tbl="otrodato1" style="display:none"><s:property value="otrosDatos1"/></td>
							<td class="align-middle" tbl="otrodato2" style="display:none"><s:property value="otrosDatos2"/></td>
							<td class="align-middle" tbl="otrodato3" style="display:none"><s:property value="otrosDatos3"/></td>
							<td class="align-middle" tbl="otrodato4" style="display:none"><s:property value="otrosDatos4"/></td>
							<td class="align-middle" tbl="codmonedatrx" style="display:none"><s:property value="codMonedaTrx"/></td>
							<td class="align-middle" tbl="rubrocomercio" style="display:none"><s:property value="rubroComercio"/></td>
							<td class="align-middle" tbl="sid" style="display:none"><s:property value="sid"/></td>
							<td class="align-middle" tbl="mit" style="display:none"><s:property value="mit"/></td>
							<td class="align-middle" tbl="codigo_funcion" style="display:none"><s:property value="codigoFuncion"/></td>
							<td class="align-middle" tbl="glosageneral" style="display:none"><s:property value="glosaGeneral"/></td>
							<td class="align-middle" tbl="referencia" style="display:none"><s:property value="referencia"/></td>
							<td class="align-middle" tbl="montoconciliacion" style="display:none"><s:property value="montoConciliacion"/></td>
							<!-- <td class="align-middle text-info" onmouseover="showDescription('<s:property value="largeDescription"/>');" onmouseout="hideDescription();"><s:property value="shortDescription"/> </td> -->
							<td class="align-middle" onmouseover="showDescription('<s:property value="largeDescription"/>');" onmouseout="hideDescription();">VER MÁS...</td>
							<td class="align-middle" tbl="numIncidente"><s:property value="numIncidente"/></td>
							<td class="align-middle" tbl="usuario"><s:property value="idUsuario"/></td>
							<td class="align-middle" tbl="fechaGestion"><s:property value="fechaGestion"/></td>
							<td class="align-middle" tbl="datosAdicionales5" style="display:none"><s:property value="datosAdicionales5"/></td>
							<s:if test='%{patpass == " " or patpass == "-" or operador == "VI" or operador == "VN"}'>
								<td class="align-middle" style="width: 36.5px;text-align: center;">No</td>
							</s:if>

    						<s:else>
    						<td class="align-middle" style="width: 36.5px;text-align: center;"> Si</td>
    						</s:else>
    						<td class="align-middle" tbl="operador" style="display:none"><s:property value="operador"/></td>
				   		</tr>	
				   	</s:iterator>
			   	</table>
			</div>
		</div>
		
		<div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial,Helvetica,sans-serif; font-size: 8pt;">
		   	<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		   	<s:property value="%{#request.numPagina}" /> de <s:property value="%{#request.ultimaPagina}" /> p&aacute;gina(s)&nbsp;&nbsp;&nbsp;
		   	Ir a pagina <input type="text" class="form-control form-control-sm d-inline py-0 align-middle" id="paginaIr" style="width:50px;font-size: 8pt;" /> <input type="button" class="align-middle btn btn-primary py-0" style="font-size: 8pt;" value="Ir" id="ir"/>
		</div>
	</body>
</html>