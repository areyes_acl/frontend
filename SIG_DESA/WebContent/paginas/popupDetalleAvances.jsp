<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%-- <style type="text/css">
#contenedor {
float: left;
width: 804px;
}

#tabla1 {
float: left;
width: 1000px;
}

#tabla1 table {
text-align: left;
}

#tabla1 table tr td {
width: 50px
}


</style> --%>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	<script type="text/javascript" src="js/utils.js"></script>
 	<script>
       	
        $(document).ready(function(){
        	
        	$("#sid").val( "<%=request.getParameter("sid")%>");	
				
		});
		
			
	$("#salir").click(function(){
        	
       	$('#popup').css({display: 'none'});
       	$('#popUpContenido').empty().css({display:'none'});
        	
    });
	</script>
</head>
<body>
		<input type="hidden" id="sid" name="sid" />
		
		<!-- <div class="titulo">  -->
		<div class="col-12 mx-auto my-4 text-center font-weight-bold">
			DETALLE DE INTERFACES PARA CONCILIACI&Oacute;N DE AVANCES CON TRANSFERENCIA
		</div>
		<hr>
		<!-- <br style="line-height:5px"/>  -->
		  
	     <div id="contenedor">
			<div id="tabla1">
				<!-- class="border="1" -->
				<table class="table table-sm table-boderless small" >
					<tr>
					
						<s:if test="%{#request.listaFinalBice.size() > 0}" >
						<td class="border-0">
							<!-- class="tablaContenido" -->
							<!--  border=1 -->
							<table style="width: 100%" id="tablaBice" class="mx-auto col-4">
							<s:iterator id="data" value="%{#request.listaFinalBice}">
							 <tr>
							 	<!-- align="center" -->
							    <th colspan="2" class="text-center font-weight-bold">Datos de Interfaz de BICE</th>
							  </tr>
							  <tr>
							    <td>Tipo Registro</td>
							    <td><s:property value="tipoRegistro"/></td>
							  </tr>
							  <tr>
							    <td>N&uacute;mero Operaci&oacute;n</td>
							    <td><s:property value="numOperPro"/></td>
							  </tr>
							   <tr>
							    <td>C&oacute;digo Servicio</td>
							    <td><s:property value="codServicio"/></td>
							  </tr>
							   <tr>
							    <td>Rut Ordenante</td>
							    <td><s:property value="rutOrdenante"/></td>
							  </tr>
							   <tr>
							    <td>N&uacute;mero Cta.Ordenante</td>
							    <td><s:property value="numCtaOrdenante"/></td>
							  </tr>
							   <tr>
							    <td>Estado</td>
							    <td><s:property value="codEstado"/></td>
							  </tr>
							   <tr>
							    <td>Glosa estado</td>
							    <td><s:property value="glosaEstado"/></td>
							  </tr>
							   <tr>
							    <td>Fecha Instrucci&oacute;n</td>
							    <td><s:property value="fechaInstruccion"/></td>
							  </tr>
							   <tr>
							    <td>Fecha Contable</td>
							    <td><s:property value="fechaContable"/></td>
							  </tr>
							   <tr>
							    <td>Banco Beneficiario</td>
							    <td><s:property value="codBancoBenefi"/></td>
							  </tr>
							   <tr>
							    <td>Tipo Cta.Beneficiario</td>
							    <td><s:property value="tipoCtaBenefi"/></td>
							  </tr>
							   <tr>
							    <td>Cta. Beneficiario</td>
							    <td><s:property value="ctaBenefi"/></td>
							  </tr>
							   <tr>
							    <td>Rut Beneficiario</td>
							    <td><s:property value="rutBenefi"/></td>
							  </tr>
							   <tr>
							    <td>Nombre Beneficiario</td>
							    <td><s:property value="nomBenefi"/></td>
							  </tr>
							     <tr>
							    <td>Monto</td>
							    <td><s:property value="monto"/></td>
							  </tr>
							</s:iterator>
							</table> 
						</td>
						</s:if>
						
						<s:if test="%{#request.listaFinalIC.size() > 0}" >
						<td class="border-0">
						<!-- class="tablaContenido" -->
						<table  style="width: 100%" class="mx-auto col-4" id="tablaIC" >
						<s:iterator id="data" value="%{#request.listaFinalIC}">
						 <tr>
						 	<!-- align="center" -->
							<th colspan="2" class="text-center font-weight-bold" >Datos de interfaz de IC</th>
						</tr>
						  <tr>
						    <td>C&oacute;digo Transacci&oacute;n</td>
						    <td><s:property value="codTransac"/></td>
						  </tr>
						   <tr>
						    <td>Num Tarjeta</td>
						    <td><s:property value="numTrajeta"/></td>
						  </tr>
						   <tr>
						    <td>Fecha Compra</td>
						    <td><s:property value="fechaCompra"/></td>
						  </tr>
						   <tr>
						    <td>Fecha Autorizaci&oacute;n</td>
						    <td><s:property value="fechaAutoriza"/></td>
						  </tr>
						   <tr>
						    <td>Fecha Posteo</td>
						    <td><s:property value="fechaPosteo"/></td>
						  </tr>
						   <tr>
						    <td>Tipo Venta</td>
						    <td><s:property value="tipoVenta"/></td>
						  </tr>
						   <tr>
						    <td>N&uacute;mero Cuotas</td>
						    <td><s:property value="numCuotas"/></td>
						  </tr>
						   <tr>
						    <td>N&uacute;mero Microfilm</td>
						    <td><s:property value="numMicroFilm"/></td>
						  </tr>
						   <tr>
						    <td>N&uacute;mero Comercio</td>
						    <td><s:property value="numComercio"/></td>
						  </tr>
						   <tr>
						    <td>Monto Transacci&oacute;n</td>
						    <td><s:property value="montoTransac"/></td>
						  </tr>
						   <tr>
						    <td>Valor Cuota</td>
						    <td><s:property value="valorCuota"/></td>
						  </tr>
						   <tr>
						    <td>Nombre Comercio</td>
						    <td><s:property value="nombreComercio"/></td>
						  </tr>
						   <tr>
						    <td width= "14px">C&oacute;digo Autorizaci&oacute;n</td>
						    <td><s:property value="codAutor"/></td>
						  </tr>
						  </s:iterator>
						</table> 
						</td>
						</s:if>
						
						<s:if test="%{#request.listaFinalAvance.size() > 0}" >
						<td class="border-0">
						<!-- clas="tablaContenido" -->
						<table  style="width: 100%" id="tablaAvances" class="mx-auto col-4">
						<s:iterator id="data" value="%{#request.listaFinalAvance}">
						 <tr class="text-center font-weight-bold">
							<th colspan="2" align="center" >Datos de interfaz de Avances</th>
						</tr>
						  <tr>
						    <td>SID</td>
						    <td><s:property value="sid"/></td>
						  </tr>
						  <tr>
						    <td>Fecha Transaferencia</td>
						    <td><s:property value="fechaTransafer"/></td>
						  </tr>
						  <tr>
						    <td>C&oacute;digo Autorizaci&oacute;n</td>
						    <td><s:property value="codAutor"/></td>
						  </tr>
						   <tr>
						    <td>N&uacute;mero Transacci&oacute;n</td>
						    <td><s:property value="numTrx"/></td>
						  </tr>
						   <tr>
						    <td>N&uacute;mero Tarjeta</td>
						    <td><s:property value="numTrajeta"/></td>
						  </tr>
						   <tr>
						    <td>Monto</td>
						    <td><s:property value="monto"/></td>
						  </tr>
						  <tr>
						    <td>Monto Seguro</td>
						    <td><s:property value="montoSeguro"/></td>
						  </tr>
						   <tr>
						    <td>Estado</td>
						    <td><s:property value="estado"/></td>
						  </tr>
						  </s:iterator>
						</table> 
						</td>
						</s:if>
					</tr>
				</table>
			</div>
		</div>

	      <!-- <br style="line-height:5px"/>  -->
	      
	      <div>
	      		<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm table-borderless">
	       	       		<tr>
	       					<td align="center">
	       				    	<input type="button"  value="Cerrar" id="salir" name="salirpopup" class="btn btn-primary">
	       					</td>
	       				</tr>
	      		</table>
     	 </div>

</body>
</html>