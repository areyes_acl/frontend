<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/utils.js"></script>

<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>

<script type="text/javascript">
	var jsonSalida = new Object();
	var lista = "";

	$(document).ready(function(){
		cargar();
	});

	function cargar(){
		var respuestas = [];
		var sidusuario = $("#sidusuario").val();

		$.ajax({
			url : 'listaPreguntasSecretasAction',
			type : 'POST',
			dataType : "json",
			data : "sidusuario="+ sidusuario
		})
		.done(
			function(resp){
				var datos = (resp.listaPreguntas).split("~");

				$('#ps1').append($('<option>', {value: "", text: "Seleccione pregunta secreta"}));
				$('#ps2').append($('<option>', {value: "", text: "Seleccione pregunta secreta"}));
				$('#ps3').append($('<option>', {value: "", text: "Seleccione pregunta secreta"}));

				if (datos[0].split(":")[1] == 0){
					for (var i = 1; i < datos.length - 1; i++) {
						var fila = datos[i].split("|");
						$('#ps1').append($('<option>', {value: fila[0], text: fila[1]}));
						$('#ps2').append($('<option>', {value: fila[0], text: fila[1]}));
						$('#ps3').append($('<option>', {value: fila[0], text: fila[1]}));
					}
				}
			}
		)
		.error(function(error, status, setting) {
			alert("Error TipoTransac: " + error);
		});

		$.ajax({
			url : 'listaPreguntasRespuestasSecretasUsuarioAction',
			type : 'POST',
			dataType : "json",
			data : "sidusuario="+ sidusuario
		})
		.done(
			function(resp){
				var datos = (resp.listaPreguntasRespuestasUsuario).split("~");

				if (datos[1] != ""){
					for (var i = 1; i < datos.length - 1; i++){
						var fila = datos[i].split("|");
						respuestas[fila[1]] = fila[3];
					}
				}

				var cont = 1;
				respuestas.forEach(function myFunction(item, index){
					if(cont == 1){
						var valor1 = $("#ps1").val();
						if(valor1 == null){
							cargar();
							return;
						}
						$("#ps1").val(index);
					}else if(cont == 2){
						$("#ps2").val(index);
					}else if(cont == 3){
						$("#ps3").val(index);
					}
    				
    				//$("#ps"+cont+" option[value=" + index + "]").attr('selected','selected');
    				$("#rp"+cont).val(item);
    				cont = cont + 1;
				});
			}
		)
		.error(function(error, status, setting) {
			alert("Error TipoTransac: " + error);
		});

		$('#formPreguntas').submit(function(){
			var p1 = $("#ps1").val();
			var p2 = $("#ps2").val();
			var p3 = $("#ps3").val();
			if(p1 == p2 || p2 == p3 || p1 == p3){
				alert("Las preguntas no pueden repetirse.");
				return false;
			}

			var r1 = $("#rp1").val();
			var r2 = $("#rp2").val();
			var r3 = $("#rp3").val();

			$.ajax({
				url : 'guardarPreguntasRespuestasAction',
				type : 'POST',
				data : "p1="+ p1 + "&r1=" + r1 + "&p2="+ p2 + "&r2=" + r2 + "&p3="+ p3 + "&r3=" + r3 + "&sidusuario=" + sidusuario
			}).done(function(resp){
				var datos = (resp.guardarPreguntasRespuestas).split("~");
				if (datos[0].split(":")[1] == 0){
					alert("Preguntas secretas y respuestas guardadas correctamente");
					location.reload();
				}else{
					alert("Error al guardar preguntas y respuestas secretas.")
				}
			})
			.error(function(error, status, setting){
				alert("Error TipoTransac: " + error);
			});

			return false;
		});
	};
</script>
</head>

<body>
	<!-- <div class="cajaTitulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		Preguntas Secretas
	</div>
	<hr>
	<!-- class="cajaContenido" -->
	<div>
		<!-- class="cajaFormulario" -->
		<div class="col-5 mx-auto">
			<form id="formPreguntas">
				<input type="hidden" id="sidusuario" name="sidusuario" value="<%=usuarioLog.getSid()%>" />
				<%-- <div>
					Pregunta Secreta nº 1 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<select name="ps1" id="ps1" required></select>
				</div>
				<div>
					Respuesta Pregunta nº 1 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="text" name="rp1" id="rp1" maxlength="50" style="width: 210px;" required />
				</div> --%>
				<div class="form-group mb-5">
					<label class="font-weight-bold" for="ps1">Pregunta Secreta nº 1</label>
					<select name="ps1" id="ps1" class="custom-select custom-select-sm" required></select>
					<label class="font-weight-bold" for="rp1">Respuesta Pregunta nº 1</label>
					<input type="text" name="rp1" id="rp1" maxlength="50" class="form-control" placeholder="Respuesta nª1" required />
				</div>
				
				<!-- <br>  -->
				
				<%-- <div>
					Pregunta Secreta nº 2 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<select name="ps2" id="ps2" required></select>
				</div>
				<div>
					Respuesta Pregunta nº 2 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="text" name="rp2" id="rp2" maxlength="50" style="width: 210px;" required />
				</div> --%>
				<div class="form-group mb-5">
					<label class="font-weight-bold" for="ps2">Pregunta Secreta nº 2</label>
					<select name="ps2" id="ps2" class="custom-select custom-select-sm" required></select>
					<label class="font-weight-bold" for="rp2">Respuesta Pregunta nº 2</label>
					<input type="text" name="rp2" id="rp2" maxlength="50" placeholder="Respuesta nª2" class="form-control" required />
				</div>
				
				<!-- <br> -->
				
				<%-- <div>
					Pregunta Secreta nº 3 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<select name="ps3" id="ps3" required></select>
				</div>
				<div>
					Respuesta Pregunta nº 3 <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="text" name="rp3" id="rp3" maxlength="50" style="width: 210px;" required />
				</div> --%>
				<div class="form-group mb-5">
					<label class="font-weight-bold" for="ps3">Pregunta Secreta nº 3</label>
					<select name="ps3" id="ps3" class="custom-select custom-select-sm" required></select>
					<label class="font-weight-bold" for="rp3">Respuesta Pregunta nº 3</label>
					<input type="text" name="rp3" id="rp3" maxlength="50" class="form-control" placeholder="Respuesta nª3" required />
				</div>
				
				<!-- <br> -->
				
				<div class="botonera text-center">
					<input type="submit" id="cambiar" value="Guardar" class="btn btn-primary" />
				</div>
			</form>
		</div>
	</div>
</body>
</html>
