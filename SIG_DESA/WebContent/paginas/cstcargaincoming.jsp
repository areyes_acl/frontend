<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Gestiones Por Cuenta</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
		
   </head>
   
   <body>
       
      <div class="titulo">
       		GARGA INCOMING
  	  </div>
      
      <br style="line-height:5px"/>
      
      <div>
      		<table width="100%" class="contenedorInput">
               <tr>
           			<td>&nbsp;Fecha Proceso :</td>
           			<td>
           				<input type="text" name="fechaProceso" id="fechainicio" size="10" maxlength="10" class="campoObligatorio"/>&nbsp;
              			<input type="button"  id="nuevoUsuario" value="Consultar">
             		</td>
       			</tr>
      		</table>
      </div>
     
      <br style="line-height:5px"/>
      
      <div>
      		<table width="100%" class="tablaContenido">
      
       				<tr>
       					<th>TIPO DE TRANSACCION</th>
       					<th>NRO TRANSACCIONES</th>
       					<th>MONEDA</th>
       					<th>MONTO</th>
       				</tr>
       				
       				<tr>
       					<td align="center" width="25%">1240</td>
       					<td align="right" width="25%">60</td>
       					<td align="right" width="25%">USD</td>
       					<td align="right" width="25%">168.900</td>
       				</tr>
       				
       				<tr>
       					<td align="center" width="25%">1442</td>
       					<td align="right" width="25%">10</td>
       					<td align="right" width="25%">USD</td>
       					<td align="right" width="25%">65.000</td>
       				</tr>
       				
       				<tr>
       					<td align="center" width="25%">1644</td>
       					<td align="right" width="25%">8</td>
       					<td align="right" width="25%">USD</td>
       					<td align="right" width="25%">13.000</td>
       				</tr>
       		
       				<tr>
       					<td align="center" width="25%">1740</td>
       					<td align="right" width="25%">3</td>
       					<td align="right" width="25%">USD</td>
       					<td align="right" width="25%">5.000</td>
       				</tr>
      		</table>
      </div>
      
      <br style="line-height:5px"/>
      
      <div>
      		<table width="100%" class="tablaContenido">
      
       				<tr>
       					<td width="25%">TOTAL :</td>
       					<td width="25%" align="right">81</td>
       					<td width="25%">&nbsp;</td>
       					<td align="right" width="25%">251.900</td>
       				</tr>
      		</table>
      </div>
      
      <br style="line-height:5px"/>
      
     <div>
    	<table  width="100%" class="contenedorInput">
    		<tr>
    		    <td align="center">
    				<input type="button" value="Exportar a Excel" />
    			</td>
    		</tr>
    	</table>
    </div>

   </body>
</html>