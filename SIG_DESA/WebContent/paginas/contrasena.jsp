<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="js/jquery-1.10.0.min.js" type="text/javascript">

</script>
<script type="text/javascript" src="js/utils.js"></script>

<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>

<script type="text/javascript">
	$("#cambiar").click(
		function() {
			if ($("#passNueva1").val() == '' || $("#passNueva2").val() == '' || $("#passActual").val() == '') {
				alert("Por favor ingrese todos los datos obligatorios.");
				return;
			}

			if ($("#passNueva1").val() == $("#passNueva2").val()) {
				$.ajax({
					url : "cambioContrasenaAction",
					type : 'POST',
					data : $("#formCont").serialize()
				}).done(function(resp){
					if(resp.cambioContrasena == null){
						alert(resp.error);
					}else{
						var resultado = resp.cambioContrasena.split(";");
						if(resultado[0] == 0){
							alert(resultado[1]);
							logout();
						}else{
							alert(resultado[1]);
						}
					}
				}).error(function(error, status, setting) {
					alert("Error1: " + error);
				});
			} else {
				alert("La nueva contraseña debe coincidir, favor de intentarlo nuevamente.");
			}
		});

	function clearView() {
		$("#passActual").val('');
		$("#passNueva1").val('');
		$("#passNueva2").val('');
	};
</script>
</head>

<body>
	<!-- <div class="cajaTitulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		CAMBIAR CONTRASE&Ntilde;A
	</div>
	<hr>
	<div class="cajaContenido">
		<!-- <div class="cajaContenidoTitulo">Informaci&oacute;n de Cuenta</div>  -->
		<!-- cajaFormulario -->
		<div class="col-5 mx-auto">
			<form id="formCont">
				<input type="hidden" id="sidusuario" name="sidusuario"
					value="<%=usuarioLog.getSid()%>" />
					
				<!-- <div>
					Contrase&ntilde;a actual <label style="color: red;" title="Requerido">*</label>
					<input type="password" name="passActual" id="passActual" maxlength="50" style="width: 210px;" required />
				</div> -->
				<div class="form-group mb-4">
					<!-- style="width: 210px;" -->
					<label for="passActual" class="font-weight-bold">Contraseña Actual</label>
					<input type="password" name="passActual" id="passActual" maxlength="50" class="form-control rounded-0 py-4" placeholder="Contraseña Actual" required />
				</div>
				
				<!-- <div>
					Contrase&ntilde;a nueva <label style="color: red;" title="Requerido">*</label>
				</div> 
				<div>
					<input type="password" name="passNueva1" id="passNueva1" maxlength="50" style="width: 210px;" required />
				</div> 
				<div>
					Confirmar contrase&ntilde;a <label style="color: red;" title="Requerido">*</label>
				</div>
				<div>
					<input type="password" name="passNueva2" id="passNueva2" maxlength="50" style="width: 210px;" required />
				</div> -->
				<div class="form-group mb-1">
					<!-- style="width: 210px;" -->
					<label class="font-weight-bold">Nueva Contraseña</label>
					<input type="password" name="passNueva1" id="passNueva1" maxlength="50"  class="form-control rounded-0 py-4 mb-1" placeholder="Contraseña Nueva" required />
					<input type="password" name="passNueva2" id="passNueva2" maxlength="50" class="form-control rounded-0 py-4" placeholder="Repetir Contraseña Nueva" required />
				</div>
				
				<div class="botonera">
					<input type="button" id="cambiar" value="Cambiar" class="btn btn-primary px-4" />
				</div>
			</form>
		</div>
	</div>
</body>
</html>
