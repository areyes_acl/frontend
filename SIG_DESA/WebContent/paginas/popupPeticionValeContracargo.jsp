
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 

<html>

   <head>
       
       <title>Gestiones Por Cuenta</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       <script type="text/javascript" src="js/utils.js"></script>       
       	<script>
       	
       	var optC = "";
		var optA = "";
		
        $(document).ready(function(){
        	
        	var sidTransaccion      = "<%=request.getParameter("sidtransaccion")%>";
            var codigorazon         = "<%=request.getParameter("codigorazon")%>";
            var vxkey               = "<%=request.getParameter("vxkey")%>";
            var mit                 = "<%=request.getParameter("mit")%>";
            var vmontoconciliacion  = "<%=request.getParameter("vmontoconciliacion")%>";
            
            
            //alert("sidtransaccion = " + sidTransaccion + "  codigorazon = " + codigorazon + " xkey = " + vxkey + " mit = " + mit + " vmontoconciliacion = " + vmontoconciliacion);
      
            buscarRazonesPeticionContraCargos(vxkey, codigorazon);
             
             //******************************************//
             //* Seccion Cargo y Abono                  *//
             //******************************************//
         	 $.ajax({
 						url:'listaCargoAbonoAction',
 						type:'POST',
 						dataType: "json",
 						}).done(function(resp){
 						var datos = (resp.listaCargoAbono).split("~");
 						$("#serieData").val(datos);
 				
 						for(var i=1;i<datos.length-1;i++)
 						{
 							var fila = datos[i].split("|");
 							if(fila[3]=='C')
 							{
 								optC += "<option value='"+fila[0]+"'>"+fila[2]+"</option>";
 							}else{
 								optA += "<option value='"+fila[0]+"'>"+fila[2]+"</option>";
 							}
 						}
 				
 						if($("#cargo").prop("checked"))
 						{
 							$("#tipoCargoAbono").empty().append(optC);
 						}else{
 							$("#tipoCargoAbono").empty().append(optA);
 						}
 						
 			 });
             
         	 //******************************************//
             //* Fin Parte Uno Seccion Cargo y Abono    *//
             //******************************************//
         	
             //******************************************//
             //* Obtener fecha tope                     *//
             //******************************************//
             var parametros = "sidtransaccion=" + "<%=request.getParameter("sidtransaccion")%>";
             parametros += "&vxkey=" + "<%=request.getParameter("vxkey")%>";
         	 parametros += "&codigorazon=" + "<%=request.getParameter("codigorazon")%>";
         	 
         	 //alert("Parametros feche tope = " + parametros);
         	 
             
             //********************************************//
             //* Llena al campo referencia cargo y abono  *//
             //********************************************//
         	 $("#referencia").val("<%=request.getParameter("referencia")%>");
         	 
         	 //*********************************************************************//
             //* Llena al campo monto para cuado sea contracargos y formatea valor *//
             //*********************************************************************//
             
         	 $("#montoconciliacion").val("<%=request.getParameter("vmontoconciliacion")%>");
         	
         	 var newMonto = sacaPuntos($("#montoconciliacion").val());
			 var newValorMonto = reemplazarComaPunto(newMonto);
			 var newMontoFormato = formato_numero(colocaComaNumero(newValorMonto));
			 
//			 $("#montoconciliacion").val(newMontoFormato);
         	 
             if (vxkey=="1644_603" || vxkey=="1442_450" || vxkey=="1442_451" || vxkey=="52_00" || vxkey=="15_00" || vxkey=="15_205"){ 
             		//Peticion De Vale - Primer Contracargo Total - Segundo Contracargo Total.
            	 	$("#montoconciliacion").attr("disabled", true);
             }
             
             $("#montoca").val("<%=request.getParameter("vmontoconciliacion")%>");
        });
        
        
        //**************************************//
        //* Parte Dos Seccion Cargo y Abono    *//
        //**************************************//
        $("input:radio").click(function(){
    	    
    		if($("#cargo").prop("checked"))
   			{
    			$("#tipoCargoAbono").empty();
    			$("#tipoCargoAbono").append(optC);
   			}else{
   				$("#tipoCargoAbono").empty();
   				$("#tipoCargoAbono").append(optA);
   			}
    	 });
        //*****************//
        //* Fin Parte Dos *//
        //*****************//
     	
		$("#cargoabono").click(function(){
			
			  if($("#cargoabono").is(':checked')) {  
					$("#seccionCargoAbono").css("display","");
			  }else{  
					$("#seccionCargoAbono").css("display","none");
			  } 
		});
		
		
		$("#montoca").blur(function(){

			var newMonto = sacaPuntos($("#montoca").val());
			var newValorMonto = reemplazarComaPunto(newMonto);
			var newMontoFormato = formato_numero(newValorMonto);
			var num = "<%=request.getParameter("vmontoconciliacion")%>";
        	if(num!=$("#montoca").val()){
        		if(parseFloat(num.replace(/\./g,"")) < parseFloat(newMontoFormato.replace(/\./g,"").replace(/\,/g,"."))){
      		        alert('Monto actual no puede superar al inicial');
					$("#montoca").val(num);
        		}else{
					$("#montoca").val(newMontoFormato);
        		}
        	}
			
		});

		$("#montoconciliacion").blur(function(){

			var newMonto = sacaPuntos($("#montoconciliacion").val());
			var newValorMonto = reemplazarComaPunto(newMonto);
			var newMontoFormato = formato_numero(newValorMonto);
			$("#montoconciliacion").val(newMontoFormato);
			
		});

		
		$("#guardar").click(function(){
			
			//***************************************************//
			//* Inicio sección grabacion de cargos y abaonos    *//
			//***************************************************//
			if($("#cargoabono").is(':checked')) {  
				
				//************************************************************//
				//* Grabo cargo y abono al seleccionar incluir cargo / abono *//
				//* Logica Alberto.                                          *//
				//************************************************************//
				var sidCargoAbono =  $("#tipoCargoAbono").val();
				var montoca       = reemplazarComaPunto(sacaPuntos($("#montoca").val()));
				var montoActualConFormato = $("#montoca").val();
				var montoActualSinFormato = numeroSinComaPunto($("#montoca").val());
				
				if(montoca == 0 ){
               		alert('No se puede ingresar un monto 0, por favor corregir antes de realizar el CARGO/ABONO');
               		return ;
               	}
               	
               	var montoOriginal = "<%=request.getParameter("vmontoconciliacion")%>";
	        		montoOriginal = reemplazarComaPunto(sacaPuntos(montoOriginal));
               	              	
				
				var parametros = "sid=" + "<%=request.getParameter("sidtransaccion")%>";
				parametros += "&tipoCargoAbono=" + sidCargoAbono;
				parametros += "&moneda=" + $("#monedaca").val();
				parametros += "&montoca=" + montoActualSinFormato;
				parametros += "&sidusuario=" + "<%=request.getParameter("sidusuario")%>";
				
				
				$.ajax({
        			url:'obtenerSumaCargoAbono',
        			type:'post',
        			dataType:'json',
					data: parametros,
        		}).done(function(data){
        		 	var sumatoriaTotal=  0;
        		 	
        		 	if(data.sumatoriaCargoAbono != null && data.sumatoriaCargoAbono)
       				{
        			  sumatoriaTotal= data.sumatoriaCargoAbono;      			 
       				}
        		 	
        		 	var montoOrig = parseFloat(montoOriginal);
       				var montoNuev = parseFloat(montoca);
       				var sumatoria =parseFloat(sumatoriaTotal);
       				var total = 0;
       				
       				if($("#abono").prop("checked")){
       					total = montoOrig - montoNuev + sumatoria;
       				}else{
       					total = montoOrig + montoNuev + sumatoria;
       				}
        		 	
        		 	var montoMaximo = montoOrig + sumatoria;
        		 	
        		 	        		 	
        		 	// ABONO
               		if($("#abono").prop("checked"))	{	        				
	        			if(total >= 0){
		        			  guardaCargoYAbono(parametros);
		        			
        				}else{
        				
        					alert("No se puede ingresar un monto mayor al total de abonos existentes.  Monto Maximo =" + montoMaximo);
        				};
	        		
	        		// CARGO
	        		}else{
	        			guardaCargoYAbono(parametros);
	        		};     		
        		
        		});
				
				
			  }else{
			  	guardarPeticionVale();
			  }
				
		});
		
		
		function guardarPeticionVale(){
		
		//***************************************************//
			 //* Fin sección grabacion de cargos y abaonos       *//
			 //***************************************************//
			  
			 //*****************************************************************//
			 //* Inicio sección grabacion de Peticion de vales y contracargos  *//
			 //*****************************************************************//
	    	   var codigorazon   = $("#razones option:selected").text().split("-")[0];
			   
	    	    if (codigorazon=="Seleccione opcion"){
	    	    	alert("Debe seleccionar un código de razón para la operación...");
	    	    	$("#razones").focus();
					return;
	    	    }

	    	    var vmontoconciliacion =  numeroSinComaPunto($("#montoconciliacion").val());
	    	    
	    	    var parametros = "&xkey=" + "<%=request.getParameter("vxkey")%>";
	    	    parametros += "&vmit=" + "<%=request.getParameter("mit")%>";
	    		parametros += "&sidtransaccion=" + "<%=request.getParameter("sidtransaccion")%>";
				parametros += "&codigorazon=" + codigorazon;
				parametros += "&vmontoconciliacion=" + vmontoconciliacion;
				parametros += "&sidusuario=" + "<%=request.getParameter("sidusuario")%>";
				
				
				
				$.ajax({
					url: "grabarPeticionValeContraCargosAction",
					type: "POST",
					data: parametros,
					dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
	    			success: function(data){
	    				mostrarMensajeGrabacion(data.mensajegrabacion);
	    				recargarConsulta();
	    				recargarConsultaBuscar();
					}
				});
				
				//************************************************//
				//* Fin sección grabacion de cargos y abaonos    *//
				//************************************************//
				$('#popup').css({display: 'none'});
				$('#popUpContenido').empty().css({display:'none'});
		}
		
		</script>
		
		<script>
					    	
		    	function buscarRazonesPeticionContraCargos(xkey, codigorazon){
		    		
			    	var parametros = "xkey=" + xkey;
			    			    	
					$.ajax({
						url: "buscarRazonesPeticionContraCargosAction",
						type: "POST",
						data: parametros,
						dataType: "json",
						error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
						},
						success: function(data){
						    		llenaSelectRazones(data.listaRazones, codigorazon);
						}
					});
			 
		    	}
		    	
		    	
				function llenaSelectRazones(listaRazones, codigorazon){
		    		
		    		var itemsHtml = "";
			   		var opcionesHtml = "";
		    		var razones = listaRazones.split("~");
		    		var esttrx = razones[0].split("~");
		    		var codtrx = esttrx[0].split("|");

		    		if ( codtrx[1]  == "0"){
		    			
			    		opcionesHTML = "";
			    		opcionesHtml = "<OPTION VALUE='" + "0" + "'>" + "Seleccione opcion"  + "</OPTION>";
			    		
			    		for (var i=1; i<razones.length-1; i++){
			    		
			    			var lstrazones = razones[i].split("|");
			    			var sidrazon       = lstrazones[0];
			    			var cod_motivo_ini = lstrazones[1];
			    			var descripcion    = lstrazones[3];
			    			
							if (codigorazon==cod_motivo_ini){
								opcionesHtml = opcionesHtml + "<OPTION VALUE='" + sidrazon + "-" + descripcion + "' SELECTED>" + cod_motivo_ini + "-" + descripcion.substring(0,55) + "</OPTION>";
								$("#causal" ).val(descripcion);
							}else{
								opcionesHtml = opcionesHtml + "<OPTION VALUE='" + sidrazon + "-" + descripcion + "'>" + cod_motivo_ini + "-" + descripcion.substring(0,55) + "</OPTION>";	
							}
			    			
			    		}
			    		
			    		// style='width: 400px;'
			    		itemsHtml = "<SELECT name='razones' id='razones' onchange='mostrarDescripcionRazon();' class='custom-select'>";
					    itemsHtml = itemsHtml + opcionesHtml;
					    itemsHtml = itemsHtml + "</SELECT>";
					    $('#divlistaRazones').html(itemsHtml);
		    			
		    		}else{
		    		
		    			//style='width: 400px;'
			    		opcionesHTML = "";
			    		opcionesHtml = "<OPTION VALUE='" + "" + "'>" + "PROBLEMAS AL CARGAR RAZONES" + "</OPTION>";
			    		itemsHtml = "<SELECT name='razones' id='razones' class='custom-select'>";
					    itemsHtml = itemsHtml + opcionesHtml;
					    itemsHtml = itemsHtml + "</SELECT>";
					    $('#divlistaRazones').html(itemsHtml);
		    		}
		    		
		    	}

				function mostrarDescripcionRazon(){
					var razon = $("#razones" ).val();
					 
					if(razon != '0' ){
					var detalle = razon.split("-");
					var sid = detalle[0];
					var descripcion = detalle[1];;
					
					$("#causal" ).val(descripcion);
					
					buscarFechaTope();
					}else{
					 // Borrar tabla
					}
					
				}
				
				function mostrarMensajeGrabacion(mensaje){
					
					var lstmensaje = mensaje.split("~");
					var esttrx = lstmensaje[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var mensaje = lstmensaje[1].split("|");
					alert(mensaje[1]);
					
				}
			
				//******************************************//
	            //* Obtener fecha tope                     *//
	            //******************************************//
		    	function buscarFechaTope(){
					
		    		 var codigorazon = $("#razones option:selected").text().split("-")[0];
		    		 
		             var parametros = "sidtransaccion=" + "<%=request.getParameter("sidtransaccion")%>";
		             parametros += "&vxkey=" + "<%=request.getParameter("vxkey")%>";
		         	 parametros += "&codigorazon=" + codigorazon;
		         	 
		         	 //alert("Parametros feche tope = " + parametros);
		         	 
		         	 $.ajax({
							url: "obtenerFechaTopeAction",
							type: "POST",
							data: parametros,
							dataType: "json",
							error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
							},
			    			success: function(data){
			    				
			    				var listafechatope = data.listafechatope.split("~");
								var esttrx = listafechatope[0].split("~");
								var codtrx = esttrx[0].split("|");
								
								if (codtrx[1]==0){
									
									var datafechatope = listafechatope[1].split("|");
									var fechatope = datafechatope[0];
									var flag      = datafechatope[1];
									
									if (flag==1){
										$("#guardar").attr("disabled", true);
										alert("Numero de dias supera al plazo determinado para calculo de fecha tope...");
										
									}else{
										$("#guardar").attr("disabled", false);
									}
									$("#fechatope").val(fechatope);
									
								}else{
									
									$("#guardar").attr("disabled", true);
									var mensaje = listafechatope[1].split("|");
									alert(mensaje[1]);
									
								}
								
							}
					 });
					
				}
				
				
				
		function guardaCargoYAbono(parametros) {
				$.ajax({
					url : "insertCargoAbonoAction",
					type : "POST",
					data : parametros,
					dataType : "json",
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
					success : function(data) {
						var esterror = data.insertCargoAbono.split("~")[0];
						
						if (esterror == "esttrx:0") {
							alert("Exito -  " +data.insertCargoAbono.split("~")[1]);
						}else{
							alert("Error  - " +data.insertCargoAbono.split("~")[1]);
						}
					},
					complete : function (data){
						guardarPeticionVale();
						
					}
					
				});
			}
		</script>
		
   </head>

<body>

	<!-- <div class="titulo">  --> 
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold ">
	  		
	  			<%
	  				if(request.getParameter("vxkey").equals("1644_603"))
	  				{
	  						out.print("PETICION DE VALES");
	  				}
	  			
	  				if(request.getParameter("vxkey").equals("1442_450"))
  					{
  						out.print("PRIMER CONTRACARGO TOTAL");
  					}
  			
	  				if(request.getParameter("vxkey").equals("1442_453"))
  					{
  						out.print("PRIMER CONTRACARGO PARCIAL");
  					}
	  				
	  				if(request.getParameter("vxkey").equals("1442_451"))
  					{
  						out.print("SEGUNDO CONTRACARGO TOTAL");
  					}
	  				
	  				if(request.getParameter("vxkey").equals("1442_454"))
  					{
  						out.print("SEGUNDO CONTRACARGO PARCIAL");
  					}
  					
  					// RECONOCIMIENTO DE CODIGOS VISA DEFINIDOS EN mostrarPantallaAcciones(), /paginas/consulta-transacciones.jsp
  					if(request.getParameter("vxkey").equals("52_00"))
  					{
  						out.print("PETICIÓN DE VALE");
  					}
  					
  					if(request.getParameter("vxkey").equals("15_00"))
  					{
  						out.print("PRIMER CONTRACARGO");
  					}
  					
  					if(request.getParameter("vxkey").equals("15_205"))
  					{
  						out.print("SEGUNDO CONTRACARGO");
  					}
	  			%>
  							
	  </div>
	  
	  <hr/>
	  
      <!-- <br style="line-height:5px"/>  -->
      
      <div>
      	<!-- class="contenedorInput" -->
      		<table width="100%" class="table table-sm">
       			
               <tr>
               			<td class="align-middle text-right w-25">Código Razón:</td>
              			<td class="align-middle"><div id="divlistaRazones"></div>
              			</td>
       			</tr>
       			
       			<tr>
               			<td class="align-middle text-right">Descripción Razón:</td>
              			<td class="align-middle">
              				<!-- size="77" -->
              				<input type="text" id="causal" name="causal"  maxlength="200" value="" class="campoSalida form-control form-control-sm"/>
              			</td>
       			</tr>
       			
       			<tr>
               			<td class="align-middle text-right">Fecha Tope:</td>
              			<td class="align-middle">
              				<input type="text" id="fechatope" name="fechatope" size="10" maxlength="10" value="" class="campoSalida form-control form-control-sm"/>
              			</td>
       			</tr>
       			
       			<tr>
               			<td class="align-middle text-right">Monto Conciliacion:</td>
              			<td class="align-middle">
              				<input type="text" id="montoconciliacion" name="montoconciliacion" maxlength="12" value="" onKeypress="ingresoMonto();" class="campoObligatorio form-control form-control-sm"/>
              			</td>
       			</tr>
      		</table>
      </div>
      
      
      <!-- <br style="line-height:5px"/>  -->
      
	<div class="border custom-control custom-checkbox">
		<input class="align-middle custom-control-input" type="checkbox" name="cargoabono" id="cargoabono" value="CA"/>
		<label for="cargoabono" class="align-middle custom-control-label ml-2">&nbsp;Incluir Cargos y Abonos</label> 
	</div>
	
     <div id="seccionCargoAbono" style="display:none;">
     		<hr size="2px">
            <input type="hidden" id="serieData" />
    	  	
    	  	<!-- <div class="subTitulo">  -->
    	  	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
	       				Sección Ingreso De Cargos / Abonos 
      		</div>
      		
      		<!-- class="contenedorInput" -->
      		<table width="100%" class="table table-sm">
               
                 <tr>
               			<td class="align-middle w-25 text-right">Tipo :</td>
              			<td class="align-middle">
	              			<div class="form-check custom-control custom-radio">
	              				<input type="radio" name="group1" id="cargo" value="C" class="custom-control-input" checked>
	              				<label for="cargo" class="custom-control-label">Cargo&nbsp;</label>
	              			</div>
	              			
	              			<div class="form-check custom-control custom-radio">
								<input type="radio" name="group1" id="abono" value="A" class="custom-control-input">
								<label for="abono" class="custom-control-label">Abono&nbsp;</label>
							</div>
						</td>
       		    </tr>
       		
       			<tr>
               			<td class="align-middle text-right">Codigo Cargo/Abono :</td>
              			<td class="align-middle">
			    			<select name="tipoCargoAbono" id="tipoCargoAbono" class="custom-select"></select>
              			</td>
       			</tr>
       			
				<tr>
               			<td class="align-middle text-right">Referencia :</td>
              			<td class="align-middle">
              				<!-- size="50" -->
              				<input type="text" id="referencia" name="referencia"  maxlength="50" class="campoSalida form-control form-control-sm"/>
              			</td>
       			</tr>
       			
       			<tr>
       					<td class="align-middle text-right">Moneda :</td>
              			<td class="align-middle">
              				<select name="monedaca" id="monedaca" class="custom-select">
			    		    	<option value="152">CLP</option>
<!-- 			    				<option value="840">USD</option> -->
			    			</select>
              			</td>
              	</tr>
       			
       			<tr>
       					<td class="align-middle text-right">Monto :</td>
              			<td class="align-middle">
              				<input type="text" id="montoca" name="montoca" size="12" maxlength="12" value="" onKeypress="ingresoMonto();" class="campoObligatorio form-control form-control-sm"/>
              			</td>
              	</tr>
              	
      </table>
      
      </div>
      
      <!-- <br style="line-height:5px"/>  -->
      
      <div>
      	<!-- contenedorInput -->
      		<table  width="100%" class="table table-sm table-borderless">
       	       		<tr>
       					<td align="center">
       						<input type="button"  value="Guardar" name="guardar" id="guardar" class="btn btn-primary">
       				    	<input type="button"  value="Cancelar" onclick="$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});$('#idacciones').prop('selectedIndex',0);" class="btn btn-primary">
       					</td>
       				</tr>
      		</table>
      </div>
      
</body>
</html>