<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import = "Beans.Usuario" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Gestion De Transacciones</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       <script src="js/jquery.Rut.js" type="text/javascript"></script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
	   
	   <% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>
	<script>
		
		$(".datepicker").datepicker();
		
		//
		$(document).ready(function() {
			$("#comienzolinea:first tr").css("border-bottom", "0px");
			$("#tipoFechaBusqueda").val("T");
  			$('#tipoFechaBusqueda').attr('disabled','disabled');
		});

		//Alberto

		var today = new Date();

		var dd = today.getDate();
		var mm = today.getMonth() + 1;//January is 0! 
		var yyyy = today.getFullYear();

		var dia = dd;
		var mes = mm;
		var ano = yyyy;

		if (dd < 10) {
			dd = '0' + dd;
		}

		if (mm < 10) {
			mm = '0' + mm;
		}
		var fechaActual = dd + "/" + mm + "/" + yyyy;

		if (dia == 1) {

			if (mes == 1) {
				mes = 12;
				dia = 31;
			} else if (mes == 2) {
				mes = 1;
				dia = 31;
			} else if (mes == 3) {
				mes = 2;
				dia = 28;
				if ((ano % 4 == 0) && (ano % 100 != 0) || (ano % 400 == 0)) {
					mes = 2;
					dia = 29;
				}
			} else if (mes == 4) {
				mes = 3;
				dia = 31;
			} else if (mes == 5) {
				mes = 4;
				dia = 30;
			} else if (mes == 6) {
				mes = 5;
				dia = 31;
			} else if (mes == 7) {
				mes = 6;
				dia = 30;
			} else if (mes == 8) {
				mes = 7;
				dia = 31;
			} else if (mes == 9) {
				mes = 8;
				dia = 31;
			} else if (mes == 10) {
				mes = 9;
				dia = 30;
			} else if (mes == 11) {
				mes = 10;
				dia = 31;
			} else if (mes == 12) {
				mes = 11;
				dia = 30;
			}
		} else {
			dia = dia - 1; //Dia anterior
		}

		var diaPaso = "";
		var mesPaso = "";
		var anoPaso = "";

		if (dia < 10) {
			diaPaso = "0" + dia;
		} else {
			diaPaso = dia;
		}

		if (mes < 10) {
			mesPaso = "0" + mes;
		} else {
			mesPaso = mes;
		}
		anoPaso = ano;

		var fechaAnterior = diaPaso + "/" + mesPaso + "/" + anoPaso;
		$('#fechaInicio').val(fechaAnterior);
		$('#fechaTermino').val(fechaActual);

		$(".fecha").click(function() {
			$(this).val("");
			$(this).addClass("campoObligatorio");
			$("#microfil").val("");
			$("#codigoautorizacion").val("");
			$("#fechaefectiva").val("");
			$("#fechaproceso").val("");
			$("#binadquirente").val("");
			$("#leebanda").val("");
			$("#otrodato01").val("");
			$("#otrodato02").val("");
			$("#otrodato03").val("");
			$("#otrodato04").val("");
			$("#codigomonedatrx").val("");
			$("#datosAdicionales5").val("");
			$("#rubrocomercio").val("");
			$("#operador").val("");
		});

		$("#cargoabono")
				.click(
						function() {

							var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();

							var nrotarjeta = $radio.parent().parent()
									.find('td').eq('1').text();
							var fechatransac = $radio.parent().parent().find(
									'td').eq('2').text();
							var sid = $radio.parent().parent().find('td').eq(
									'21').text();
							var referencia = $radio.parent().parent()
									.find('td').eq('25').text();
							var montoca = $radio.parent().parent().find('td')
									.eq('6').text();		
							var codrazon = $radio.parent().parent().find('td')
									.eq('7').text();

							$.ajax(
									{

										url : "paginas/popupCargoAbono.jsp",
										type : 'POST',
										data : "sid=" + sid + "&numeroTarjeta="
												+ nrotarjeta + "&fechaTrx="
												+ fechatransac + "&referencia="
												+ referencia + "&montoca="
												+ montoca + "&codrazon="
												+ codrazon,
									/*success: 
										function(data)
										{
											alert(data);	
										}*/
									}).done(function(data) {
									
								$("#popup").css("height", $(document).height());
									
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'small');
							});
						});
		//Alberto
		$("#objetar")
				.click(
						function() {

							var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();

							var nrotarjeta = $radio.parent().parent()
									.find('td').eq('1').text();
							var fechatransac = $radio.parent().parent().find(
									'td').eq('2').text();
							var codigoautorizacion = $radio.parent().parent()
									.find('td').eq('10').text();
							var sid = $radio.parent().parent().find('td').eq(
									'21').text();

							//alert("Sid =" + sid + " N.TC =" + nrotarjeta + " fechatransac=" + fechatransac + " codigoautorizacion=" + codigoautorizacion);

							$.ajax(
									{

										url : "paginas/popup-objeciones.jsp",
										type : 'POST',
										data : "sid=" + sid + "&numeroTarjeta="
												+ nrotarjeta + "&fechaTrx="
												+ fechatransac
												+ "&codigoAutorizacion="
												+ codigoautorizacion
									/*success: 
										function(data)
										{
											alert(data);	
										}*/
									}).done(function(data) {
									
								$("#popup").css("height", $(document).height());
									
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'big');
							});
						});
						
	$("#otraGestion").click(
						function() {

							var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();

							var nrotarjeta = $radio.parent().parent()
									.find('td').eq('1').text();
							var fechatransac = $radio.parent().parent().find(
									'td').eq('2').text();
							var codigoautorizacion = $radio.parent().parent()
									.find('td').eq('10').text();
							var sid = $radio.parent().parent().find('td').eq(
									'21').text();

							var numIncidente = $radio.parent().parent().find('td').eq('28').text();
									
									
							//alert("Sid =" + sid + " N.TC =" + nrotarjeta + " fechatransac=" + fechatransac + " codigoautorizacion=" + codigoautorizacion);

							$.ajax(
									{

										url : "paginas/popupOtrasGestiones.jsp",
										type : 'POST',
										data : "sid=" + sid + "&numeroTarjeta="
												+ nrotarjeta + "&fechaTrx="
												+ fechatransac
												+ "&codigoAutorizacion="
												+ codigoautorizacion
												+ "&numIncidente="
												+ numIncidente
									/*success: 
										function(data)
										{
											alert(data);	
										}*/
									}).done(function(data) {
								
								$("#popup").css("height", $(document).height());
								
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'small');
							});
						});

		$("#gestionPorCuenta")
				.click(
						function() {

							var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();
							var numerotarjeta = $radio.parent().parent().find(
									'td').eq('1').text();
							var fechainicio = $('#fechaInicio').val();
							var fechatermino = $('#fechaTermino').val();
							var origenmodulo = "G";

							var parametros = "numerotarjeta=" + numerotarjeta
									+ "&fechaInicio=" + fechainicio
									+ "&fechaTermino=" + fechatermino
									+ "&origenmodulo=" + origenmodulo;

							$.ajax({
								url : "paginas/gestionCuenta.jsp",
								type : 'POST',
								data : parametros,
							/*success: 
								function(data)
								{
									alert(data);	
								}*/
							}).done(function(data) {
							
								$("#popup").css("height", $(document).height());
								
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'big');
							});
						});

						
		// REGISTRAR UNA PERDIDA					
		$("#btn-regitrar-perdida").click(
				function() {
							var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();
							
							var nrotarjeta = $radio.parent().parent().find('td').eq('1').text();
							
							var fechatransac = $radio.parent().parent().find('td').eq('2').text();
							
							var codigoautorizacion = $radio.parent().parent().find('td').eq('10').text();
							
							var sid = $radio.parent().parent().find('td').eq('21').text();

							var numIncidente = $radio.parent().parent().find('td').eq('28').text();

							$.ajax({
									url : "paginas/perdida/popupRegistroPerdida.jsp",
									type : 'POST',
									data : "sid=" + sid + "&numeroTarjeta="
												+ nrotarjeta + "&fechaTrx="
												+ fechatransac
												+ "&codigoAutorizacion="
												+ codigoautorizacion
												+ "&numIncidente="
												+ numIncidente
								}).done(function(data) {
										
										$("#popup").css("height", $(document).height());
										
										$("#popup").css({	display : 'block'});
										$("#popUpContenido").css({ display : 'block' });
										$("#popUpContenido").empty().append(data);
										centradoDiv('popUpContenido', 'small');
									});
						});
						
						

		$("#buscarTransaccion")
				.click(
						function() {
							desabilitarAcciones();
							
							$("#comienzolinea").css("display", "none");
							$("#accionesDisponibles").css("display", "none");
							$("#microfil").val("");
							$("#codigoautorizacion").val("");
							$("#otrodato01").val("");
							$("#fechaefectiva").val("");
							$("#fechaproceso").val("");
							$("#otrodato02").val("");
							$("#binadquirente").val("");
							$("#leebanda").val("");
							$("#otrodato03").val("");
							$("#codigomonedatrx").val("");
							$("#datosAdicionales5").val("");
							$("#rubrocomercio").val("");
							$("#otrodato04").val("");
							$("#operador").val("");

							//**********************************//
							//* DATOS DE LA CUENTA DEL CLIENTE *//
							//**********************************//
							$('#nomcliente').val("");
							$('#rutcliente').val("");
							$('#numerocuenta').val("");
							$('#cupo').val("");
							$('#fechaexpiracion').val("");
							$('#estado').val("");

							$("#divlistaTransacciones").css("display", "none");
							$("#accionesDisponibles").css("display", "none");

							var tipoTransaccion = $(
									"input[name='tipotransaccion']:checked")
									.val();
							//alert(tipoTransaccion);
							if ($('#fechaInicio').val().length == 0) {
								alert("Debe ingresar fecha de inicio para la busqueda...");
								$("#fechaInicio").focus();
								return;
							}
							
							// SE AGREGA VALIDACION DE QUE SEA OBLIGACION LOS 16 CARACTERES DE LA TARJETA
							if($('#numeroTarjeta').val().length != 0 && $('#numeroTarjeta').val().length != 16){
								alert("Se deben ingresar los 16 numeros de tarjeta o ninguno...");
								$('#numeroTarjeta').focus();
								return;
							}    
							
							if ($('#numeroTarjeta').val().length == 0 && tipoTransaccion=='00_00' ) {
								alert("Debe ingresar un numero de tarjeta para la busqueda...");
								$("#numeroTarjeta").focus();
								$("#exportarTxs").attr("disabled", true);
								return;
							}else{
								$("#exportarTxs").attr("disabled", false);
							}

							if ($('#fechaTermino').val().length == 0) {
								alert("Debe ingresar fecha final para la busqueda...");
								$("#fechaTermino").focus();
								return;
							}

							if (validarFecha($('#fechaInicio').val()) == false) {
								alert("La fecha de inicio ingresada es incorrecta...");
								$("#fechaInicio").focus();
								return;
							}

							if (validarFecha($('#fechaTermino').val()) == false) {
								alert("La fecha de termino ingresada es incorrecta...");
								$("#fechaTermino").focus();
								return;
							}
							
							var fechaInicio = convertirFecha($('#fechaInicio')
								.val());
							var fechaTermino = convertirFecha($('#fechaTermino')
								.val());
							if (fechaInicio > fechaTermino) {
								alert("La fecha de inicio es mayor a la fecha de termino, verifiquela...");
								$("#fechaInicio").focus();
								return;
							}
							
							
							//alert("tipoTransaccion xKey " +  tipoTransaccion);
							var xKey = tipoTransaccion.split("_", 2);
							
							//Paso1
							if (xKey[0] == '1240' && xKey[1] == '200') { //Transaccion De Presentacion.//
								console.log("1");
								buscarTransaccionPresentacion("");
							}
							if (xKey[0] == '1442' || xKey[0] == '1644' || xKey[0] == '1240') {
							
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								//Primer Contracargo Total: 1442_450
								//Primer Contracargo Parcial: 1442_453
								//Segundo Contracargo Total: 1442_451
								//Segundo Contracargo Parcial: 1442_454
								//Peticion De Vale: 1644_603
								//Confirmacion Peticion Vale:1644_605
								//Representacion Total:1240_205
								//Representacion Parcial:1240_282
								if(xKey[0] != '200'){
									buscarTransaccionAccion(codigoTransaccion,
											codigoFuncion, "");
									console.log("2");
								}
								console.log("3");
							}

							if (xKey[0] == 'REC' || xKey[0] == 'OBJ') { //Transaccion De Objeciones Rechazos.  // 
								buscarTransaccionObjecionRechazo('');
								console.log("4");
							}

							if (xKey[0] == '1740') { //Transaccion Recuperacion De Cargos Incoming.  //
								var codigoTransaccion = xKey[0];
								buscarTransaccionRecuperacionCargosIncoming(
										codigoTransaccion, "");
								console.log("5");
							}

							if (xKey[0] == 'CONF_RVL') { //Transaccion Confirmacion de recepcion de vales.  //
								var codigoTransaccion = xKey[0];
								buscarTransaccionConfirmacionRecepcionPeticionVales(
										codigoTransaccion, "");
								console.log("6");
							}
										
							//LOGICA TARJETA VISA
							if((xKey[0] == '05' || xKey[0] == '06' || xKey[0] == '07') && xKey[1] == '00'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								buscarTransaccionPresentacionVisa(codigoTransaccion,codigoFuncion);
								console.log("7");
							}
							if((xKey[0] == '05' || xKey[0] == '06' || xKey[0] == '07') && xKey[1] == '205'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								buscarTransaccionPresentacionVisa(codigoTransaccion,
											codigoFuncion, "");
								console.log("8");
							}
							if((xKey[0] == '25' || xKey[0] == '26' || xKey[0] == '27') && xKey[1] == '00'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								buscarTransaccionPresentacionVisa(codigoTransaccion,
											codigoFuncion, "");
								console.log("9");
							}
							if(xKey[0] == '15' && xKey[1] == '00'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								buscarTransaccionAccion(codigoTransaccion,
											codigoFuncion, "");
								console.log("10");
							}
							if(xKey[0] == '15' && xKey[1] == '205'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								buscarTransaccionAccion(codigoTransaccion,
											codigoFuncion, "");
								console.log("11");
							}
							if(xKey[0] == '52' && xKey[1] == '00'){
// 								var codigoTransaccion = xKey[0];
// 								var codigoFuncion = xKey[1];
// 								buscarTransaccionPresentacionVisa(codigoTransaccion,
// 											codigoFuncion, "");
								buscarTransaccionConfirmacionRecepcionPeticionVales(tipoTransaccion,"");	
								console.log("12");
							}
							if(xKey[0] == '00' && xKey[1] == '00'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								buscarTransaccionPresentacionVisa(codigoTransaccion,
											codigoFuncion, "");
								console.log("13");
											
							}
							if(xKey[0] == '01' && xKey[1] == '00'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];

								buscarTransaccionOnUsVisa(codigoTransaccion,codigoFuncion);
// 								buscarTransaccionPresentacionVisa(codigoTransaccion,
// 											codigoFuncion, "");
								console.log("14");
							}
							if(xKey[0] == '02' && xKey[1] == '00'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								buscarTransaccionOnUsVisa(codigoTransaccion,codigoFuncion);
// 								buscarTransaccionPresentacionVisa(codigoTransaccion,
// 											codigoFuncion, "");
								console.log("15");
							}
							// AVANCE ONUS
							if(xKey[0] == '03' && xKey[1] == '00'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								buscarTransaccionOnUsVisa(codigoTransaccion,codigoFuncion);
								console.log("13");
								console.log("16");
							}
							// AVANCE ONUS
							if(xKey[0] == '04' && xKey[1] == '00'){
								var codigoTransaccion = xKey[0];
								var codigoFuncion = xKey[1];
								buscarTransaccionOnUsVisa(codigoTransaccion,codigoFuncion);
								console.log("14");
							}
							

						});

		$(".tipotransaccion").click(function() {

			$("#comienzolinea").css("display", "");
			$("#divlistaTransacciones").css("display", "none");
			$("#accionesDisponibles").css("display", "none");
			$("#microfil").val("");
			$("#codigoautorizacion").val("");
			$("#otrodato01").val("");
			$("#fechaefectiva").val("");
			$("#fechaproceso").val("");
			$("#otrodato02").val("");
			$("#binadquirente").val("");
			$("#leebanda").val("");
			$("#otrodato03").val("");
			$("#codigomonedatrx").val("");
			$("#datosAdicionales5").val("");
			$("#rubrocomercio").val("");
			$("#otrodato04").val("");
			$("#operador").val("");

			//**********************************//
			//* DATOS DE LA CUENTA DEL CLIENTE *//
			//**********************************//
			$('#nomcliente').val("");
			$('#rutcliente').val("");
			$('#numerocuenta').val("");
			$('#cupo').val("");
			$('#fechaexpiracion').val("");
			$('#estado').val("");

			desabilitarAcciones();
		});

		$("#numeroTarjeta").focus(function() {
			desabilitarAcciones();
			$("#divlistaTransacciones").css("display", "none");
			$("#accionesDisponibles").css("display", "none");

			//**********************************//
			//* DATOS DE LA CUENTA DEL CLIENTE *//
			//**********************************//
			$('#nomcliente').val("");
			$('#rutcliente').val("");
			$('#numerocuenta').val("");
			$('#cupo').val("");
			$('#fechaexpiracion').val("");
			$('#estado').val("");

			$("#microfil").val("");
			$("#codigoautorizacion").val("");
			$("#otrodato01").val("");
			$("#fechaefectiva").val("");
			$("#fechaproceso").val("");
			$("#otrodato02").val("");
			$("#binadquirente").val("");
			$("#leebanda").val("");
			$("#otrodato03").val("");
			$("#codigomonedatrx").val("");
			$("#datosAdicionales5").val("");
			$("#rubrocomercio").val("");
			$("#otrodato04").val("");
			$("#operador").val("");

		});

		$("#fechaInicio").focus(function() {
			$("#comienzolinea").css("display", "");
			desabilitarAcciones();
			$("#divlistaTransacciones").css("display", "none");
			$("#accionesDisponibles").css("display", "none");

			//**********************************//
			//* DATOS DE LA CUENTA DEL CLIENTE *//
			//**********************************//
			$('#nomcliente').val("");
			$('#rutcliente').val("");
			$('#numerocuenta').val("");
			$('#cupo').val("");
			$('#fechaexpiracion').val("");
			$('#estado').val("");

			$("#microfil").val("");
			$("#codigoautorizacion").val("");
			$("#otrodato01").val("");
			$("#fechaefectiva").val("");
			$("#fechaproceso").val("");
			$("#otrodato02").val("");
			$("#binadquirente").val("");
			$("#leebanda").val("");
			$("#otrodato03").val("");
			$("#codigomonedatrx").val("");
			$("#datosAdicionales5").val("");
			$("#rubrocomercio").val("");
			$("#otrodato04").val("");
			$("#operador").val("");

		});

		$("#fechaTermino").focus(function() {
			$("#comienzolinea").css("display", "");
			desabilitarAcciones();
			$("#divlistaTransacciones").css("display", "none");
			$("#accionesDisponibles").css("display", "none");

			//**********************************//
			//* DATOS DE LA CUENTA DEL CLIENTE *//
			//**********************************//
			$('#nomcliente').val("");
			$('#rutcliente').val("");
			$('#numerocuenta').val("");
			$('#cupo').val("");
			$('#fechaexpiracion').val("");
			$('#estado').val("");

			$("#microfil").val("");
			$("#codigoautorizacion").val("");
			$("#otrodato01").val("");
			$("#fechaefectiva").val("");
			$("#fechaproceso").val("");
			$("#otrodato02").val("");
			$("#binadquirente").val("");
			$("#leebanda").val("");
			$("#otrodato03").val("");
			$("#codigomonedatrx").val("");
			$("#datosAdicionales5").val("");
			$("#rubrocomercio").val("");
			$("#otrodato04").val("");
			$("#operador").val("");

		});

		

		$("#exportarTxs")
				.click(
						function() {
						
							// SE AGREGA VALIDACION DE QUE SEA OBLIGACION LOS 16 CARACTERES DE LA TARJETA
							if($('#numeroTarjeta').val().length != 0 && $('#numeroTarjeta').val().length != 16){
								alert("Se deben ingresar los 16 numeros de tarjeta o ninguno...");
								$('#numerotarjeta').focus();
								return;
							} 
					
							if ($('#fechaInicio').val().length == 0) {
								alert("Debe ingresar fecha de inicio para exportar ...");
								$("#fechaInicio").focus();
								return;
							}
							if ($('#fechaTermino').val().length == 0) {
								alert("Debe ingresar fecha final para exportar ...");
								$("#fechaTermino").focus();
								return;
							}
							if (validarFecha($('#fechaInicio').val()) == false) {
								alert("La fecha de inicio ingresada es incorrecta...");
								$("#fechaInicio").focus();
								return;
							}

							if (validarFecha($('#fechaTermino').val()) == false) {
								alert("La fecha de termino ingresada es incorrecta...");
								$("#fechaTermino").focus();
								return;
							}							
							var fechaTermino = convertirFecha($('#fechaTermino').val());
							var fechaInicio = convertirFecha($('#fechaInicio').val());
							if (fechaInicio > fechaTermino) {
								alert("La fecha de inicio es mayor a la fecha de termino, verifiquela...");
								$("#fechaInicio").focus();
								return;
							}
							var parametros = "fechaInicio="
									+ $('#fechaInicio').val();

							if ($('#fechaTermino').val().length == 0) {
								f = new Date();
								parametros = parametros + "&fechaFin="
										+ f.getDate() + "/"
										+ (f.getMonth() + 1) + "/"
										+ f.getFullYear();
								fechaTermino = convertirFecha(f.getDate() + "/"
										+ (f.getMonth() + 1) + "/"
										+ f.getFullYear());
							} else {

								if (validarFecha($('#fechaTermino').val()) == false) {
									alert("La fecha ingresada esta incorrecta, verifiquela...");
									$("#fechaTermino").focus();
									return;
								}
								parametros = parametros + "&fechaFin="
										+ $('#fechaTermino').val();
								fechaTermino = convertirFecha($('#fechaTermino')
										.val());
							}

							var tipoTransaccion = $(
									"input[name='tipotransaccion']:checked")
									.val();

							var tipoBusqueda = $("#tipoFechaBusqueda").val(); 

							parametros += "&numeroTarjeta="
									+ $('#numeroTarjeta').val();
							parametros += "&tipoTransaccion=" + tipoTransaccion;
							parametros += "&nombreArchivo=" + "export";
							parametros += "&tipoBusqueda=" + tipoBusqueda;
							//alert("parametros " + parametros);

							location.href = "./exportarDatosExcelTxsAction?"
									+ parametros;

						});

		$("#exportar")
				.click(
						function() {

							DataExportar = "";
							DatasExcel = "";

							var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();

							var opcion = $radio.parent().parent().find('td')
									.eq('0').text();
							var numerotarjeta = $radio.parent().parent().find(
									'td').eq('1').text();
							if( numerotarjeta.length == 16 ) {
								numerotarjeta = numerotarjeta.substring(0, 4) + "XXXXXXXX" + numerotarjeta.substring(12, 16);
							}
							var fechaTransaccion = $radio.parent().parent()
									.find('td').eq('2').text();
							var comercio = $radio.parent().parent().find('td')
									.eq('3').text();
							var pais = $radio.parent().parent().find('td').eq(
									'4').text();
							var montooriginal = $radio.parent().parent().find(
									'td').eq('5').text();
							var monto = $radio.parent().parent().find('td').eq(
									'6').text();
							var codrazon = $radio.parent().parent().find('td')
									.eq('7').text();
							var status = $radio.parent().parent().find('td')
									.eq('8').text();

							var nomcliente = $('#nomcliente').val();
							var rutcliente = $('#rutcliente').val();
							var numerocuenta = $('#numerocuenta').val();
							var cupo = $('#cupo').val();
							var fechaexpiracion = $('#fechaexpiracion').val();
							var estado = $('#estado').val();

							var microfil = $('#microfil').val();
							var codigoautorizacion = $('#codigoautorizacion')
									.val();
							var otrodato01 = $('#otrodato01').val();

							var fechaefectiva = $('#fechaefectiva').val();
							var fechaproceso = $('#fechaproceso').val();
							var otrodato02 = $('#otrodato02').val();

							var binadquirente = $('#binadquirente').val();
							var leebanda = $('#leebanda').val();
							var otrodato03 = $('#otrodato03').val();

							var codigomonedatrx = $('#codigomonedatrx').val();
							var datosAdicionales5 = $('#datosAdicionales5').val();
							var rubrocomercio = $('#rubrocomercio').val();
							var otrodato04 = $('#otrodato04').val();
							var operador = $("#operador").val();

							DatasExcel = "";
							DataExportar = "TIT|NRO TC|FECHA TRANSACCION|COMERCIO|PAIS|MONTO ORIGINAL|MONTO|CODIGO RAZON|STATUS|OPERADOR~";
							DataExportar += "DET|" + numerotarjeta + "|"
									+ fechaTransaccion + "|" + comercio + "|"
									+ pais + "|" + montooriginal + "|" + monto
									+ "|" + codrazon + "|" + status + "|" + operador + "~";

							DataExportar += "TIT|NOMBRE CLIENTE|RUT CLIENTE|NUMERO CUENTA|CUPO|FECHA EXPERICACION|ESTADO~";
							DataExportar += "DET|" + nomcliente + "|"
									+ rutcliente + "|" + numerocuenta + "|"
									+ cupo + "|" + fechaexpiracion + "|"
									+ estado + "~";

							DataExportar += "TIT|MICROFIL|CODIGO AUTORIZACION|COD. PROCESO~";
							DataExportar += "DET|" + microfil + "|"
									+ codigoautorizacion + "|" + otrodato01
									+ "~";

							DataExportar += "TIT|FECHA EFECTIVA|FECHA PROCESO|CONV. CONCILIACION~";
							DataExportar += "DET|" + fechaefectiva + "|"
									+ fechaproceso + "|" + otrodato02 + "~";

							DataExportar += "TIT|BIN ADQUIRENTE|LEE BANDA|CONV. FACTURACION~";
							DataExportar += "DET|" + binadquirente + "|"
									+ leebanda + "|" + otrodato03 + "~";

							DataExportar += "TIT|CODIGO TRANSACCION|RUBRO COMERCIO|PTO. SERVICIO~";
							DataExportar += "DET|" + codigomonedatrx + "|"
									+ rubrocomercio + "|" + otrodato04 + "~";

							var parametros = "dataExcel=" + DataExportar;
							parametros += "&nombreArchivo=" + "export";

							location.href = "./exportarDatosExcelAction?"
									+ parametros;

						});
	</script>
	
	<script>
			//inicioarreglos
  			function buscarTransaccionPresentacion(op){
  			//alert("10");
  					$("#objetar").attr("disabled", true);
  				    var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
  				    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
  				    
  				    var diaInicio = fechaInicioTmp[0];
  				    var mesInicio = fechaInicioTmp[1];
  				    var anoInicio = fechaInicioTmp[2];
  				    
  				    var diaTermino = fechaTerminoTmp[0];
  				    var mesTermino  = fechaTerminoTmp[1];
  				    var anoTermino = fechaTerminoTmp[2];
  				    
  				    var fechaInicio  = anoInicio + mesInicio + diaInicio;
  				    var fechaTermino = anoTermino + mesTermino + diaTermino;
  				    
  				    var cantReg = $('#cantReg').val();
  				    
  				    //alert("Fecha Inicio : " + fechaInicio + " Fecha Termino : " + fechaTermino);
  				    
	  				var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
					parametros = parametros + "&fechaInicio=" + fechaInicio;
					parametros = parametros + "&fechaTermino=" + fechaTermino;
					parametros = parametros + "&op=" + op;
					parametros = parametros + "&cantReg=" + cantReg;
					//alert(parametros);

					$.ajax({
						url: "transaccionPresentacionAction",
						type: "POST",
						data: parametros,
						error: function(XMLHttpRequest, textStatus, errorThrown){
							alert('Error ' + textStatus);
							alert(errorThrown);
							alert(XMLHttpRequest.responseText);
						},
				    	success: function(data){
							//Paso2
							//mostrarListaTransacciones(data.listaTransacciones);
				    		$("#divlistaTransacciones").empty().append(data);
				    		
				    		// Evitar que la tabla empequeñezca el menu de navegación.
				    		$("#divlistaTransacciones").css("width", $("#divlistaTransacciones").parent().width());
		    				
		    				$("#divlistaTransacciones").css("display","");
						}
					});
			}
  			
  			function buscarTransaccionPresentacionVisa(op,codigoFuncion){
  			//alert("9");
  					$("#objetar").attr("disabled", true);
  				    var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
  				    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
  				    
  				    var diaInicio = fechaInicioTmp[0];
  				    var mesInicio = fechaInicioTmp[1];
  				    var anoInicio = fechaInicioTmp[2];
  				    
  				    var diaTermino = fechaTerminoTmp[0];
  				    var mesTermino  = fechaTerminoTmp[1];
  				    var anoTermino = fechaTerminoTmp[2];
  				    
  				    var fechaInicio  = anoInicio + mesInicio + diaInicio;
  				    var fechaTermino = anoTermino + mesTermino + diaTermino;
  				    
  				    var cantReg = $('#cantReg').val();
  				    
  				    //alert("Fecha Inicio : " + fechaInicio + " Fecha Termino : " + fechaTermino);
  				    
	  				var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
					parametros = parametros + "&fechaInicio=" + fechaInicio;
					parametros = parametros + "&fechaTermino=" + fechaTermino;
					parametros = parametros + "&op=" + op;
					parametros = parametros + "&cantReg=" + cantReg;
					parametros = parametros + "&codigoFuncion=" + codigoFuncion;

					$.ajax({
						url: "transaccionPresentacionVisaAction",
						type: "POST",
						data: parametros,
						error: function(XMLHttpRequest, textStatus, errorThrown){
							alert('Error ' + textStatus);
							alert(errorThrown);
							alert(XMLHttpRequest.responseText);
						},
				    	success: function(data){
				    		//Paso2
							//mostrarListaTransacciones(data.listaTransacciones);
				    		$("#divlistaTransacciones").empty().append(data);
				    		
				    		// Evitar que la tabla empequeñezca el menu de navegación.
				    		$("#divlistaTransacciones").css("width", $("#divlistaTransacciones").parent().width());
				    		
		    				$("#divlistaTransacciones").css("display","");
						}
					});
			}
  			
  			function buscarTransaccionOnUsVisa(op,codigoFuncion){
  			//alert("8");
  					$("#objetar").attr("disabled", true);
  				    var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
  				    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
  				    
  				    var diaInicio = fechaInicioTmp[0];
  				    var mesInicio = fechaInicioTmp[1];
  				    var anoInicio = fechaInicioTmp[2];
  				    
  				    var diaTermino = fechaTerminoTmp[0];
  				    var mesTermino  = fechaTerminoTmp[1];
  				    var anoTermino = fechaTerminoTmp[2];
  				    
  				    var fechaInicio  = anoInicio + mesInicio + diaInicio;
  				    var fechaTermino = anoTermino + mesTermino + diaTermino;
  				    
  				    var cantReg = $('#cantReg').val();
  				    //$('#operador').hide();
  				    //$('#operadortxt').hide();
  				    
  				    
	  				var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
					parametros = parametros + "&fechaInicio=" + fechaInicio;
					parametros = parametros + "&fechaTermino=" + fechaTermino;
					parametros = parametros + "&op=" + op;
					parametros = parametros + "&cantReg=" + cantReg;
					parametros = parametros + "&codigoFuncion=" + codigoFuncion;
					

					$.ajax({
						url: "transaccionOnUsVisaAction",
						type: "POST",
						data: parametros,
						error: function(XMLHttpRequest, textStatus, errorThrown){
							alert('Error ' + textStatus);
							alert(errorThrown);
							alert(XMLHttpRequest.responseText);
						},
				    	success: function(data){
							//Paso2
							//mostrarListaTransacciones(data.listaTransacciones);
				    		$("#divlistaTransacciones").empty().append(data);
				    		
				    		// Evitar que la tabla empequeñezca el menu de navegación.
				    		$("#divlistaTransacciones").css("width", $("#divlistaTransacciones").parent().width());
				    		
		    				$("#divlistaTransacciones").css("display","");
						}
					});
			}
  			function buscarTransaccionObjecionRechazo(op){
  				//alert("7");
  					var tipoBusqueda = $("#tipoFechaBusqueda").val();
				    var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
				    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
				    
				    var diaInicio = fechaInicioTmp[0];
				    var mesInicio = fechaInicioTmp[1];
				    var anoInicio = fechaInicioTmp[2];
				    
				    var diaTermino = fechaTerminoTmp[0];
				    var mesTermino  = fechaTerminoTmp[1];
				    var anoTermino = fechaTerminoTmp[2];
				    
				    var fechaInicio  = anoInicio + mesInicio + diaInicio;
				    var fechaTermino = anoTermino + mesTermino + diaTermino;
				    
				     var cantReg = $('#cantReg').val();
				    //alert("Consulata objeciones rechazos Fecha Inicio : " + fechaInicio + " Fecha Termino : " + fechaTermino);
				    
				    var tipoTransaccion = $("input[name='tipotransaccion']:checked").val();
			
				    var tipoObjecionRechazo="";
										
					tipoObjecionRechazo = tipoTransaccion;
				    
  					var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
					parametros = parametros + "&fechaInicio=" + fechaInicio;
					parametros = parametros + "&fechaTermino=" + fechaTermino;
					parametros = parametros + "&tipoObjecionRechazo=" + tipoObjecionRechazo;
					parametros = parametros + "&op=" + op;
					parametros = parametros + "&cantReg=" + cantReg;
					parametros = parametros + "&tipoBusqueda=" + tipoBusqueda;
					
					//alert(parametros);

					$.ajax({
						url: "transaccionObjecionRechazoAction",
						type: "POST",
						data: parametros,
						
						error: function(XMLHttpRequest, textStatus, errorThrown){
							alert('Error ' + textStatus);
							alert(errorThrown);
							alert(XMLHttpRequest.responseText);
						},
			    		success: function(data){
			    			//mostrarListaTransacciones(data.listaTransacciones);
				    		$("#divlistaTransacciones").empty().append(data);
				    		
				    		// Evitar que la tabla empequeñezca el menu de navegación.
				    		$("#divlistaTransacciones").css("width", $("#divlistaTransacciones").parent().width());
				    		
		    				$("#divlistaTransacciones").css("display","");
					}
				});
			}
  			
  			
  			function buscarTransaccionAccion(codigoTransaccion, codigoFuncion, op){
  				//alert("6");
//   				alert("Codigo Transaccion = " + codigoTransaccion + " CodigoFuncion = " + codigoFuncion);
  				
  				var tipoBusqueda  = $("#tipoFechaBusqueda").val();
  			    var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
			    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
			    
			    var diaInicio = fechaInicioTmp[0];
			    var mesInicio = fechaInicioTmp[1];
			    var anoInicio = fechaInicioTmp[2];
			    
			    var diaTermino = fechaTerminoTmp[0];
			    var mesTermino  = fechaTerminoTmp[1];
			    var anoTermino = fechaTerminoTmp[2];
			    
			    var fechaInicio  = anoInicio + mesInicio + diaInicio;
			    var fechaTermino = anoTermino + mesTermino + diaTermino;
			    
			     var cantReg = $('#cantReg').val();
			    
				var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
				parametros = parametros + "&fechaInicio=" + fechaInicio;
				parametros = parametros + "&fechaTermino=" + fechaTermino;
				parametros = parametros + "&codigoTransaccion=" + codigoTransaccion;
				parametros = parametros + "&codigoFuncion=" + codigoFuncion;
				parametros = parametros + "&op=" + op;
				parametros = parametros + "&cantReg=" + cantReg;
				parametros = parametros + "&tipoBusqueda=" + tipoBusqueda;
				
// 				alert(parametros);

				$.ajax({
					url: "transaccionAccionesAction",
					type: "POST",
					data: parametros,
					//dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
		    		success: function(data){
		    			//mostrarListaTransacciones(data.listaTransacciones);
				    		$("#divlistaTransacciones").empty().append(data);
				    		
				    		// Evitar que la tabla empequeñezca el menu de navegación.
				    		$("#divlistaTransacciones").css("width", $("#divlistaTransacciones").parent().width());
				    		
		    				$("#divlistaTransacciones").css("display","");
					}
				});
  				
  			}
  			
  			
  			
  			function buscarTransaccionRecuperacionCargosIncoming(codigoTransaccion,op){
  				//alert("5");
  				//alert("Codigo Transaccion = " + codigoTransaccion);
  				
  				
  			    var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
			    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
			    
			    var diaInicio = fechaInicioTmp[0];
			    var mesInicio = fechaInicioTmp[1];
			    var anoInicio = fechaInicioTmp[2];
			    
			    var diaTermino = fechaTerminoTmp[0];
			    var mesTermino  = fechaTerminoTmp[1];
			    var anoTermino = fechaTerminoTmp[2];
			    
			     var cantReg = $('#cantReg').val();
			    
			    var fechaInicio  = anoInicio + mesInicio + diaInicio;
			    var fechaTermino = anoTermino + mesTermino + diaTermino;
			    
				var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
				parametros = parametros + "&fechaInicio=" + fechaInicio;
				parametros = parametros + "&fechaTermino=" + fechaTermino;
				parametros = parametros + "&codigoTransaccion=" + codigoTransaccion;
				parametros = parametros + "&op=" + op;
				parametros = parametros + "&cantReg=" + cantReg;
				
				//alert(parametros);

				$.ajax({
					url: "transaccionRecuperacionCargosIncAction",
					type: "POST",
					data: parametros,
					//dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
		    		success: function(data){
		    			//mostrarListaTransacciones(data.listaTransacciones);
				    		$("#divlistaTransacciones").empty().append(data);
				    		
				    		// Evitar que la tabla empequeñezca el menu de navegación.
				    		$("#divlistaTransacciones").css("width", $("#divlistaTransacciones").parent().width());
				    		
		    				$("#divlistaTransacciones").css("display","");
					}
				});
  				
  			}
  			

  			function buscarTransaccionRecuperacionCargosOutgoing(codigoTransaccion,op){
  				//alert("4");
  				//alert("Codigo Transaccion = " + codigoTransaccion);
  			    var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
			    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
			    
			    var diaInicio = fechaInicioTmp[0];
			    var mesInicio = fechaInicioTmp[1];
			    var anoInicio = fechaInicioTmp[2];
			    
			    var diaTermino = fechaTerminoTmp[0];
			    var mesTermino  = fechaTerminoTmp[1];
			    var anoTermino = fechaTerminoTmp[2];
			    
			     var cantReg = $('#cantReg').val();
			    
			    var fechaInicio  = anoInicio + mesInicio + diaInicio;
			    var fechaTermino = anoTermino + mesTermino + diaTermino;
			    
				var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
				parametros = parametros + "&fechaInicio=" + fechaInicio;
				parametros = parametros + "&fechaTermino=" + fechaTermino;
				parametros = parametros + "&codigoTransaccion=" + codigoTransaccion;
				parametros = parametros + "&op=" + op;
				parametros = parametros + "&cantReg=" + cantReg;
				
				//alert(parametros);

				$.ajax({
					url: "transaccionRecuperacionCargosOutAction",
					type: "POST",
					data: parametros,
					//dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
		    		success: function(data){
		    			//mostrarListaTransacciones(data.listaTransacciones);
				    		$("#divlistaTransacciones").empty().append(data);
				    		
				    		// Evitar que la tabla empequeñezca el menu de navegación.
				    		$("#divlistaTransacciones").css("width", $("#divlistaTransacciones").parent().width());
				    		
		    				$("#divlistaTransacciones").css("display","");
					}
				});
  				
  			}
  			
  			/*******************************************************************************/
  			/* Funcion que busca las transacciones de confirmacion de recepcion de vales   */
  			/*******************************************************************************/
  			function buscarTransaccionConfirmacionRecepcionPeticionVales(codigoTransaccion,op){
  				//alert("3");
  				//alert("Codigo Transaccion = " + codigoTransaccion);
  			    var fechaInicioTmp = $('#fechaInicio').val().split("/");   //"20130619"//
			    var fechaTerminoTmp = $('#fechaTermino').val().split("/"); //"20130619"//
			    var tipoBusqueda  = $("#tipoFechaBusqueda").val();
			    var diaInicio = fechaInicioTmp[0];
			    var mesInicio = fechaInicioTmp[1];
			    var anoInicio = fechaInicioTmp[2];
			    
			    var diaTermino = fechaTerminoTmp[0];
			    var mesTermino  = fechaTerminoTmp[1];
			    var anoTermino = fechaTerminoTmp[2];
			    
			     var cantReg = $('#cantReg').val();
			    
			    var fechaInicio  = anoInicio + mesInicio + diaInicio;
			    var fechaTermino = anoTermino + mesTermino + diaTermino;
			    
				var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
				parametros = parametros + "&fechaInicio=" + fechaInicio;
				parametros = parametros + "&fechaTermino=" + fechaTermino;
				parametros = parametros + "&codigoTransaccion=" + codigoTransaccion;
				parametros = parametros + "&op=" + op;
				parametros = parametros + "&cantReg=" + cantReg;
				parametros = parametros + "&tipoBusqueda=" + tipoBusqueda;
				
				//alert(parametros);

				$.ajax({
					url: "transaccionConfirmacionRecepcionPeticionValesAction",
					type: "POST",
					data: parametros,
					//dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
		    		success: function(data){
		    			//mostrarListaTransacciones(data.listaTransacciones);
				    		$("#divlistaTransacciones").empty().append(data);
				    		
				    		// Evitar que la tabla empequeñezca el menu de navegación.
				    		$("#divlistaTransacciones").css("width", $("#divlistaTransacciones").parent().width());
				    		
		    				$("#divlistaTransacciones").css("display","");
					}
				});
  			}
  			
  			//paso3
  			function mostrarListaTransacciones(listaTransacciones)
  			{
  			//alert("2");
  				var stringHtml = "";
				var lsttransaccion = listaTransacciones.split("~");
				var esttrx = lsttransaccion[0].split("~");
				var codtrx = esttrx[0].split("|");
				
				if (codtrx[1]=="0")
				{
					
					stringHtml = "";
					// class="tablaContenido"
					stringHtml = "<table width='100%' class='table table-sm' id='myTable'>";
					
    				for (var i=1; i<lsttransaccion.length-1; i++)
    				{
    		
    					var detalletransaccion = lsttransaccion[i].split("|");
    					
    					var mit = detalletransaccion[0];
    					var codigo_funcion = detalletransaccion[1];
    					var nro_tarjeta = detalletransaccion[2];
    					var fechatransac = detalletransaccion[3];
    					var comercio = detalletransaccion[4];
    				    var pais = detalletransaccion[5];
    				    var monto_transac = detalletransaccion[6];
    				    var monto_facturacion = detalletransaccion[7];
				        var codrazon = detalletransaccion[8];
				        var microfil = detalletransaccion[9];
				        var codigo_autorizacion = detalletransaccion[10];
				        var fechaefectiva = detalletransaccion[11];
				        var fechaproceso = detalletransaccion[12];
				        var binadquirente = detalletransaccion[13];
				        var leebanda = detalletransaccion[14];
				        var estadotrx = detalletransaccion[15];
				        var otrodato1 = detalletransaccion[16];
				        var otrodato2 = detalletransaccion[17];
				        var otrodato3 = detalletransaccion[18];
				        var otrodato4 = detalletransaccion[19];
				        var codmonedatrx = detalletransaccion[20];
				        var rubrocomercio = detalletransaccion[21];
				        var sid           = detalletransaccion[22];
				        var glosageneral  = detalletransaccion[23];
				        var referencia    = detalletransaccion[24];
				        var montoconciliacion = detalletransaccion[25];
				        var datosAdicionales5 = detalletransaccion[26];
				        
				        
				     	stringHtml = stringHtml + "<tr class='text-center'>";
    					stringHtml = stringHtml + "<td class='align-middle' width='5%'>" + "<input type='radio' name='seleccionTRX' value='C' id='seleccionTRX' onclick='mostrarTransaccion(this);'>" + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='15%'>" + nro_tarjeta + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='15%'>" + fechatransac + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='20%'>" + comercio + "</td>";
   						stringHtml = stringHtml + "<td class='align-middle' width='5%'>" + pais + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='10%' align='right'>" + formato_numero(colocaComaNumero(monto_transac)) + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='10%' align='right'>" + formato_numero(colocaComaNumero(monto_facturacion)) + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='10%' align='right'>" + codrazon + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' width='10%'>" + estadotrx + "</td>";
    					
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + microfil + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + codigo_autorizacion + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + fechaefectiva + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + fechaproceso + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + binadquirente + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + leebanda + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + otrodato1 + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + otrodato2 + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + otrodato3 + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + otrodato4 + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + codmonedatrx + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + rubrocomercio + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + sid + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + mit + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + codigo_funcion + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + glosageneral + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + referencia + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + montoconciliacion + "</td>";
    					stringHtml = stringHtml + "<td class='align-middle' style='display:none;'>" + datosAdicionales5 + "</td>";
    					
    					stringHtml = stringHtml + "</tr>";
    					
    				}
  					stringHtml = stringHtml + "</table>";
    				$('#divlistaTransacciones').html(stringHtml);
    				
    				// Evitar que la tabla empequeñezca el menu de navegación.
		    		$("#divlistaTransacciones").css("width", $("#divlistaTransacciones").parent().width());
		    		
    				$("#divlistaTransacciones").css("display","");
    				$("#exportarTxs").attr("disabled", false);
				}
				else{
					var mensaje = lsttransaccion[1].split("|");
					if(mensaje[1] != 'op')
						alert(mensaje[1]);
				}
  			}
			
  			
  			function mostrarTransaccion(x){
				//alert("1");
								
				limpiarDatosCliente();
				var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();
				var opcion              = $radio.parent().parent().find('td').eq('0').text(); 
	    		var nro_tarjeta        	= $radio.parent().parent().find('td').eq('1').text(); 
	    		var fechatransac    	= $radio.parent().parent().find('td').eq('2').text();
	    		var comercio  			= $radio.parent().parent().find('td').eq('3').text();
	    		var pais            	= $radio.parent().parent().find('td').eq('4').text();
	    		var monto_transac     	= $radio.parent().parent().find('td').eq('5').text();
	    		var monto_facturacion  	= $radio.parent().parent().find('td').eq('6').text(); 
	    		var codrazon          	= $radio.parent().parent().find('td').eq('7').text();
	    		var estatus    			= $radio.parent().parent().find('td').eq('8').text(); 
	    		var microfil            = $radio.parent().parent().find('td').eq('9').text(); 
				var codigo_autorizacion = $radio.parent().parent().find('td').eq('10').text(); 
				var fechaefectiva       = $radio.parent().parent().find('td').eq('11').text(); 
				var fechaproceso        = $radio.parent().parent().find('td').eq('12').text(); 
				var binadquirente       = $radio.parent().parent().find('td').eq('13').text(); 
				var leebanda            = $radio.parent().parent().find('td').eq('14').text(); 
				var otrodato1           = $radio.parent().parent().find('td').eq('15').text(); 
				var otrodato2           = $radio.parent().parent().find('td').eq('16').text(); 
				var otrodato3           = $radio.parent().parent().find('td').eq('17').text(); 
				var otrodato4           = $radio.parent().parent().find('td').eq('18').text(); 
				var codmonedatrx        = $radio.parent().parent().find('td').eq('19').text(); 
				var rubrocomercio       = $radio.parent().parent().find('td').eq('20').text(); 
				var sid                 = $radio.parent().parent().find('td').eq('21').text();
				var mit                 = $radio.parent().parent().find('td').eq('22').text();
				var codigofuncion       = $radio.parent().parent().find('td').eq('23').text();
				var glosageneral        = $radio.parent().parent().find('td').eq('24').text();
				var referencia          = $radio.parent().parent().find('td').eq('25').text();
				var montoconciliacion   = $radio.parent().parent().find('td').eq('26').text();
				var datosAdicionales5   = $radio.parent().parent().find('td').eq('31').text();
				var operador			= $radio.parent().parent().find('td').eq('33').text();
								
				//var fechaProcesoGregoriano = fechaJulianaCregoriana("2455815");
				//alert("Valor Juliano : " + fechaproceso);
				
				var fechaProcesoGregoriano = "";
				if (fechaproceso != "0000"){
					fechaProcesoGregoriano = fechaJulianaCregoriana(fechaproceso);	
				}
				
				$("#microfil").val(microfil);
				$("#codigoautorizacion").val(codigo_autorizacion);
				$("#otrodato01").val(otrodato1);
				$("#fechaefectiva").val(fechaefectiva);
				$("#fechaproceso").val(fechaproceso);
				$("#otrodato02").val(otrodato2);
				$("#binadquirente").val(binadquirente);
				$("#leebanda").val(leebanda);
				$("#otrodato03").val(otrodato3);
				$("#codigomonedatrx").val(codmonedatrx);
				$("#datosAdicionales5").val(datosAdicionales5);
				$("#rubrocomercio").val(rubrocomercio);
				$("#otrodato04").val(otrodato4);
				$("#operador").val(operador);
				
				<% if(usuarioLog.getActualizacion() == 1){ %>
					habilitarAcciones();
				<% } %>
				
				consultarClienteWS(nro_tarjeta);
				
  			}
  			
  			function mostrarTransaccionOnUs(x){
				
				limpiarDatosCliente();
				var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();
				
				var opcion              = $radio.parent().parent().find('td').eq('0').text(); 
	    		var nro_tarjeta        	= $radio.parent().parent().find('td').eq('1').text(); 
	    
				
				consultarClienteWS(nro_tarjeta);
				
  			}
  			
  			
  			function consultarClienteWS(nro_tarjeta){
  				
  				//var parametros = "numeroTarjeta=" + $('#numeroTarjeta').val();
  				var parametros = "numeroTarjeta=" +nro_tarjeta;
  				//var parametros = "numeroTarjeta=9200610156088010";
				//alert(parametros);

				$.ajax({
					url: "llamadaWS",
					type: "POST",
					data: parametros,
					dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
		    		success: function(data){

		    			var lsttransaccion = data.cadena.split("~");
						var esttrx = lsttransaccion[0].split("~");
						var codtrx = esttrx[0].split("|");
						
						if (codtrx[1]=="0")
						{
							var datoscliente = lsttransaccion[1].split("|");

							var nombrecliente   = datoscliente[0];
	    					var rutcliente      = datoscliente[1];
	    					var numerocuenta    = datoscliente[2];
	    					var cupocliente     = datoscliente[3];
	    					var fechaexpiracion = datoscliente[4];
	    					var estadocliente   = datoscliente[5];
	    					
							$('#nomcliente').val(nombrecliente);
				    		$('#rutcliente').val(rutcliente);
				    		//$("#rutcliente").val($.Rut.formatear(rutcliente)+"-"+$.Rut.getDigito(rutcliente));
				    		$('#numerocuenta').val(numerocuenta);
				    		$('#cupo').val(formato_numero(cupocliente));
				    		$('#fechaexpiracion').val(fechaexpiracion);
				    		$('#estado').val(estadocliente);
		    			
						}else{
							var mensaje = lsttransaccion[1].split("|");
							
							if (mensaje=="Numero de tarjeta erroneo"){
								alert("No existen los datos de la cuenta del cliente para la tarjeta consultada...");
							}
							
							limpiarDatosCliente();
						}
		    			
					}
				});
  			}
  			
  			function limpiarDatosCliente(){
  			
  					$('#nomcliente').val("");
					$('#rutcliente').val("");
				    $('#numerocuenta').val("");
				    $('#cupo').val("");
				    $('#fechaexpiracion').val("");
				    $('#estado').val("");
  			};

  			
  			function habilitarAcciones(){
  				
  				var tipoTransaccion = $("input[name='tipotransaccion']:checked").val();
  				var xKey = $("input[name='tipotransaccion']:checked").val();
  				//alert("xKey "+xKey);		
  				$("#objetar").attr("disabled", true);
  				$("#cargoabono").attr("disabled", true);
  				// class="contenedorInput"
  				var stringHtml = "<table width='100%' class='table table-borderless'>";
  				//stringHtml+= "<tr onchange='mostrarPantallaAcciones(this);'>";
  				stringHtml+= "<tr class='text-right'>";
  				stringHtml+= "<td class='font-weight-bold'> Acciones : ";
			    stringHtml+= "<select name='idacciones' id='idacciones' class='custom-select w-25 d-inline' onchange='mostrarPantallaAcciones(this);'>";
			    stringHtml+= "<option value='0'> Seleccione opcion</option>";
		
				//LOGICA MASTER ACCIONES
				//HABLITAR OBJECIONES
				
				
				if (xKey == '1240_200') {
					$("#objetar").attr("disabled", false);
	  	    		
				}
				//HABILITAR CARGO ABONOS
				if (xKey == '1442_450' || xKey == '1442_451' 
					|| xKey == '1442_453' || xKey == '1442_454' || xKey == '1644_603'
					|| xKey == '1644_605' || xKey == 'REC' || xKey == 'OBJ') {
					$("#cargoabono").attr("disabled", false);
				
				}
				
				if(xKey == '1240_205' || xKey == '1240_282'){
					stringHtml+= "<option value='1'> Peticion de vale</option>";
				}
				
				if(xKey == '1644_603' || xKey == '1644_605'){
					stringHtml+= "<option value='2'> Primer contracargo total</option>";
				}
				
				if(xKey == '1644_603' || xKey == '1644_605' ){
				//if(xKey == '1644_603' || xKey == '1644_605' || xKey == 'REC' || xKey == 'OBJ'){//Habilitar Master
					stringHtml+= "<option value='3'> Primer contracargo parcial</option>";
				}
				
				if(xKey == '1240_205'){
					stringHtml+= "<option value='4'> Segundo contracargo total</option>";
				}
				
				if(xKey == '1240_282'){
					stringHtml+= "<option value='5'> Segundo contracargo parcial</option>";
				}
				
				if(xKey == '1644_603'){
					stringHtml+= "<option value='7'> Confirmacion de recepcion de vales</option>";
				}
				
				if(xKey == '1442_450' || xKey == '1442_451' || xKey == '1442_453' 
				//|| xKey == '1442_454' || xKey == '1644_605' || xKey == 'REC' || xKey == 'OBJ'){ Habilitar linea para master 
				|| xKey == '1442_454' || xKey == '1644_605' ){
					
					stringHtml+= "<option value='6'> Rec. de cobro de cargos</option>";
				}				
				//LOGICA VISA ACCIONES
				if (xKey == '05_00' || xKey == '06_00' || xKey == '07_00') {
					$("#objetar").attr("disabled", false);
	  	    		
				}
				if (xKey == '05_205' || xKey == '06_205' 
					|| xKey == '07_205'  || xKey == '26_00'
					|| xKey == '27_00'  || xKey == '15_00' || xKey == '15_205'
					|| xKey == '52_00') {
					$("#cargoabono").attr("disabled", false);
				}
				
				// DESBLOQUEAR EL BTN REGISTRAR PERDIDA.
				if(xKey == '05_205' || xKey=='OBJ' || xKey == 'REC' || xKey =='52_00'){
					$("#btn-regitrar-perdida").attr("disabled", false);
				}
				
				
				// SE PUEDEN REALIZAR OTRAS GESTIONES MENOS EN LAS PRESENTACIONES C-GUTIERREZ : 17/03/2016
				if( xKey != '05_00'){
					$("#otraGestion").attr("disabled", false);
				}
				
				
				if(xKey == 'REC' || xKey == 'OBJ'){
					stringHtml+= "<option value='8'> Peticion de vale</option>";
				}
								
				if(xKey == '52_00' || xKey == 'REC' || xKey == 'OBJ'){
					stringHtml+= "<option value='9'> Primer contracargo total</option>";
				}
				
				if(xKey == '05_205' || xKey == '06_205' || xKey == '07_205' ){
					stringHtml+= "<option value='10'> Segundo contracargo total</option>";
				}
				
				
				if (xKey == '00_00') {
					$("#accionesDisponibles").css("display","none");
					desabilitarAcciones();
					$("#exportarTxs").attr("disabled", true);
	  	    		
				}else{
					$("#accionesDisponibles").html(stringHtml);
	  	    		$("#accionesDisponibles").css("display","");
	  	    		
	  	    		$("#gestionPorCuenta").attr("disabled", false);
	  	    		$("#exportar").attr("disabled", false);
				}				
							
				
	            stringHtml+= "</select>";
	        	stringHtml+= "</td>";
  				stringHtml+= "</tr>";
  				stringHtml+= "</table>";
  				
  	    		
  	    		
  	    		

  			}
			
  			function desabilitarAcciones(){
  				
  				$("#gestionPorCuenta").attr("disabled", true);
  	    		$("#objetar").attr("disabled", true);
  	    		$("#cargoabono").attr("disabled", true);
  	    		$("#exportar").attr("disabled", true);
  	    		$("#otraGestion").attr("disabled", true);
  	    		$("#btn-regitrar-perdida").attr("disabled", true);
  	    		//$("#exportarTxs").attr("disabled", true);
  			}
  			
  			
  			
  			function mostrarPantallaAcciones(){
  				//alert("11");
  				var valorOpcion = $("#idacciones" ).val();
  				//alert("Mostrando opcion = " + valorOpcion);
  				var tipo = "";
  				var url  = "";
  				
  				//alert("Valor opcion aqui : " + valorOpcion);
  				
  				var sidusuario = "<%=request.getSession().getAttribute("sidusuario")%>";
  				//alert("Sid usuario = " + sidusuario);
  				
  				if (valorOpcion==1 || valorOpcion==2 || valorOpcion==3 || 
  					valorOpcion==4 || valorOpcion==5 || valorOpcion==8 ||
  					valorOpcion==9 || valorOpcion==10){
  						
  							var xkey = "";
  							var mit  = "";
  							
  							if (valorOpcion==1){
  								xkey = "1644_603"; //Peticion De Vale
  								mit = "1644";
  							} else if (valorOpcion==2){
			    				xkey = "1442_450"; //Primer ContraCargo Total
			    				mit = "1442";
  							} else if (valorOpcion==3){
  								xkey = "1442_453"; //Primer ContraCargo Parcial
  								mit = "1442";
  							} else if (valorOpcion==4){
  								xkey = "1442_451"; //Segundo ContraCargo Total
  								mit = "1442";
  							} else if (valorOpcion==5){
  								xkey = "1442_454"; //Segundo contraCargo Parcial
  								mit = "1442";
  							} else {
  								// LOGICA VISA.
  								if(valorOpcion==8){
  									xkey = "52_00"; //Peticion de vale 
  									mit = "52";
  								} else if(valorOpcion==9){
  									xkey = "15_00"; //Primer Contracargo
  									mit = "15";
  								} else if (valorOpcion==10){
  									xkey = "15_205"; //Segundo contracargo
  									mit = "15";
  								}
  							}
  					
  							var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();
							var opcion              = $radio.parent().parent().find('td').eq('0').text(); 
	    					var nro_tarjeta        	= $radio.parent().parent().find('td').eq('1').text(); 
	    					var fechatransac    	= $radio.parent().parent().find('td').eq('2').text(); 
	    					var comercio  			= $radio.parent().parent().find('td').eq('3').text(); 
	    					var pais            	= $radio.parent().parent().find('td').eq('4').text(); 
	    					var monto_transac     	= $radio.parent().parent().find('td').eq('5').text(); 
	    					var monto_facturacion  	= $radio.parent().parent().find('td').eq('6').text(); 
	    					var codrazon          	= $radio.parent().parent().find('td').eq('7').text(); 
	    					var estatus    			= $radio.parent().parent().find('td').eq('8').text(); 
	    					var microfil            = $radio.parent().parent().find('td').eq('9').text(); 
							var codigo_autorizacion = $radio.parent().parent().find('td').eq('10').text(); 
							var fechaefectiva       = $radio.parent().parent().find('td').eq('11').text(); 
							var fechaproceso        = $radio.parent().parent().find('td').eq('12').text(); 
							var binadquirente       = $radio.parent().parent().find('td').eq('13').text(); 
							var leebanda            = $radio.parent().parent().find('td').eq('14').text(); 
							var otrodato1           = $radio.parent().parent().find('td').eq('15').text(); 
							var otrodato2           = $radio.parent().parent().find('td').eq('16').text(); 
							var otrodato3           = $radio.parent().parent().find('td').eq('17').text(); 
							var otrodato4           = $radio.parent().parent().find('td').eq('18').text(); 
							var codmonedatrx        = $radio.parent().parent().find('td').eq('19').text(); 
							var rubrocomercio       = $radio.parent().parent().find('td').eq('20').text(); 
							var sid                 = $radio.parent().parent().find('td').eq('21').text();
							var codigofuncion       = $radio.parent().parent().find('td').eq('23').text();
							var glosageneral        = $radio.parent().parent().find('td').eq('24').text();
							var referencia          = $radio.parent().parent().find('td').eq('25').text();
							var montoconciliacion   = $radio.parent().parent().find('td').eq('26').text();
							var operador 			= $radio.parent().parent().find('td').eq('33').text();
							//alert("Se va con = " + "vxkey="+xkey+"&mit="+mit+"&sidtransaccion="+sid+"&codigorazon="+codrazon+"&vmontoconciliacion="+montoconciliacion+"&referencia="+referencia+"&sidusuario="+sidusuario);
						
							url = "paginas/popupPeticionValeContracargo.jsp";
  							$.ajax({
  								url:url,
  								type:'POST',
  								data:"vxkey="+xkey+"&mit="+mit+"&sidtransaccion="+sid+"&codigorazon="+codrazon+"&vmontoconciliacion="+montoconciliacion+"&referencia="+referencia+"&sidusuario="+sidusuario,
  						
  							}).done(function(data){
  								
  								$("#popup").css("height", $(document).height());
  								
  								$("#popup").css({
  						    		display: 'block'
  								});
  						
  								$("#popUpContenido").css({
  								    display: 'block'
  								});
  							
  								$("#popUpContenido").empty().append(data);
  								centradoDiv('popUpContenido', 'small');
  							});
  				}
  							
  				if (valorOpcion==6){//Recuperacion y Cobro De Cargo
  					
  							var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();
  					
  							var opcion              = $radio.parent().parent().find('td').eq('0').text(); 
  		    				var nro_tarjeta        	= $radio.parent().parent().find('td').eq('1').text(); 
  		    				var fechatransac    	= $radio.parent().parent().find('td').eq('2').text(); 
  		    				var comercio  			= $radio.parent().parent().find('td').eq('3').text(); 
  		    				var pais            	= $radio.parent().parent().find('td').eq('4').text(); 
  		    				var monto_transac     	= $radio.parent().parent().find('td').eq('5').text(); 
  		    				var monto_facturacion  	= $radio.parent().parent().find('td').eq('6').text(); 
  		    				var codrazon          	= $radio.parent().parent().find('td').eq('7').text(); 
  		    				var estatus    			= $radio.parent().parent().find('td').eq('8').text(); 
  		    				var microfil            = $radio.parent().parent().find('td').eq('9').text(); 
  							var codigo_autorizacion = $radio.parent().parent().find('td').eq('10').text(); 
  							var fechaefectiva       = $radio.parent().parent().find('td').eq('11').text(); 
  							var fechaproceso        = $radio.parent().parent().find('td').eq('12').text(); 
  							var binadquirente       = $radio.parent().parent().find('td').eq('13').text(); 
  							var leebanda            = $radio.parent().parent().find('td').eq('14').text(); 
  							var otrodato1           = $radio.parent().parent().find('td').eq('15').text(); 
  							var otrodato2           = $radio.parent().parent().find('td').eq('16').text(); 
  							var otrodato3           = $radio.parent().parent().find('td').eq('17').text(); 
  							var otrodato4           = $radio.parent().parent().find('td').eq('18').text(); 
  							var codmonedatrx        = $radio.parent().parent().find('td').eq('19').text(); 
  							var rubrocomercio       = $radio.parent().parent().find('td').eq('20').text(); 
  							var sid                 = $radio.parent().parent().find('td').eq('21').text();
  							var mit                 = $radio.parent().parent().find('td').eq('22').text();
  							var codigofuncion       = $radio.parent().parent().find('td').eq('23').text();
  							var glosageneral        = $radio.parent().parent().find('td').eq('24').text();
  							var referencia          = $radio.parent().parent().find('td').eq('25').text();
  							var montoconciliacion   = $radio.parent().parent().find('td').eq('26').text();
  					        var operador 			= $radio.parent().parent().find('td').eq('33').text();
  					        
  							//alert("sidtransaccion="+sid+"&mit="+mit+"&numeroTarjeta="+nro_tarjeta+"&fechaTransaccion="+fechatransac+"&microfil="+microfil+"&codigofuncion="+codigofuncion+"&codigorazon="+codrazon+"&codmonedatrx="+codmonedatrx+"&montotransac="+monto_transac+"&glosageneral="+glosageneral+"&referencia="+referencia);
  							
  							var sidusuario = "<%=request.getSession().getAttribute("sidusuario")%>";
  			  				//alert("Sid usuario = " + sidusuario);
  			  				
  			  			    var xkey = "";
  			  			
		  					url = "paginas/popupRecuperacionCobroCargo.jsp";
		  					$.ajax({
		  						url:url,
		  						type:'POST',
		  						data:"sidtransaccion="+sid+"&mit="+mit+"&numeroTarjeta="+nro_tarjeta+"&fechaTransaccion="+fechatransac+"&microfil="+microfil+"&codigofuncion="+codigofuncion+"&codigorazon="+codrazon+"&codmonedatrx="+codmonedatrx+"&montotransac="+monto_transac+"&glosageneral="+glosageneral+"&referencia="+referencia+"&sidusuario="+sidusuario,
		  						
		  					}).done(function(data){
		  						
		  						$("#popup").css("height", $(document).height());
		  						
		  						$("#popup").css({
		  						    display: 'block'
		  						});
		  						
		  						$("#popUpContenido").css({
  								    display: 'block'
  								});
		  						
		  						$("#popUpContenido").empty().append(data);
		  						centradoDiv('popUpContenido', 'small');
		  					});
  				}
  				
  				
  				if (valorOpcion==7){//Confirmacion de recepcion de vales
						
						var xkey = "CONF_RVL";
						
						var $radio = $("#myTable input[name='seleccionTRX']:checked").parent();
					    var opcion              = $radio.parent().parent().find('td').eq('0').text(); 
					    var nro_tarjeta        	= $radio.parent().parent().find('td').eq('1').text(); 
					    var fechatransac    	= $radio.parent().parent().find('td').eq('2').text(); 
					    var comercio  			= $radio.parent().parent().find('td').eq('3').text(); 
					    var pais            	= $radio.parent().parent().find('td').eq('4').text(); 
					    var monto_transac     	= $radio.parent().parent().find('td').eq('5').text(); 
					    var monto_facturacion  	= $radio.parent().parent().find('td').eq('6').text(); 
					    var codrazon          	= $radio.parent().parent().find('td').eq('7').text(); 
					    var estatus    			= $radio.parent().parent().find('td').eq('8').text(); 
					    var microfil            = $radio.parent().parent().find('td').eq('9').text(); 
					    var codigo_autorizacion = $radio.parent().parent().find('td').eq('10').text(); 
					    var fechaefectiva       = $radio.parent().parent().find('td').eq('11').text(); 
					    var fechaproceso        = $radio.parent().parent().find('td').eq('12').text(); 
					    var binadquirente       = $radio.parent().parent().find('td').eq('13').text(); 
					    var leebanda            = $radio.parent().parent().find('td').eq('14').text(); 
					    var otrodato1           = $radio.parent().parent().find('td').eq('15').text(); 
					    var otrodato2           = $radio.parent().parent().find('td').eq('16').text(); 
					    var otrodato3           = $radio.parent().parent().find('td').eq('17').text(); 
					    var otrodato4           = $radio.parent().parent().find('td').eq('18').text(); 
					    var codmonedatrx        = $radio.parent().parent().find('td').eq('19').text(); 
					    var rubrocomercio       = $radio.parent().parent().find('td').eq('20').text(); 
					    var sid                 = $radio.parent().parent().find('td').eq('21').text();
					    var codigofuncion       = $radio.parent().parent().find('td').eq('23').text();
					    var glosageneral        = $radio.parent().parent().find('td').eq('24').text();
					    var referencia          = $radio.parent().parent().find('td').eq('25').text();
					    var montoconciliacion   = $radio.parent().parent().find('td').eq('26').text();
					    var operador 			= $radio.parent().parent().find('td').eq('33').text();
				
					    //alert("Se va con = " + "vxkey="+xkey+"&sidtransaccion="+sid+"&sidusuario="+sidusuario);
				
					    url = "paginas/popupConfirmacionRecepcionVales.jsp";
						$.ajax({
							url:url,
							type:'POST',
							data:"vxkey="+xkey+"&sidtransaccion="+sid+"&sidusuario="+sidusuario,
					
						}).done(function(data){
						
							$("#popup").css("height", $(document).height());
												
							$("#popup").css({
					    		display: 'block'
							});
					
							$("#popUpContenido").css({
							    display: 'block'
							});
						
							
							$("#popUpContenido").empty().append(data);
							centradoDiv('popUpContenido', 'small');
						});
			       }//Fin opcion
  				
  			}//Fin Funcion
  			
  			
  			function fechaJulianaCregoriana(fechaJuliana)
  			{
  			    J=eval(fechaJuliana);
  			    with (Math)
  			    {  
  					if (J>=2299160.5){
  						G=1;
  					} 
  					else{
  						G=0;
  					}
  					
  					F= J-floor(J);
  					J=floor(J);
  					F=F +0.5;
  					if (F>=1) 
  					{
  						F= F - 1;
  						J= J + 1;
  					}
  					if (G==0)
  					{
  						A=J;
  					} 
  					else
  					{
  						A1=floor((J/36524.25)-51.12264);
  						A=J + 1 + A1 - floor(A1 / 4);
  					}
  					B = A + 1524;
  					C = floor((B / 365.25) - 0.3343);
  					D = floor(365.25 * C);
  					E1 = floor((B - D) / 30.61);
  					D = B - D - floor(30.61 * E1) + F;
  					M = E1 - 1;
  					Y = C - 4716;
  					
  					if (E1>13.5)
  					{
  						M=M-12;
  					}
  					
  					if (M<2.5)
  					{
  						Y = Y + 1;
  					}
  					H = (D - floor(D)) * 24;
  					H1 = (H - floor(H)) * 60;
  					H2 = (H1 - floor(H1)) * 60;
  					
  					if (Y < 0) {
  						era="A.C.";
  						Y=-Y;
  					} 
  					else 
  					{
  						era="D.C.";
  					}
  					
  					D=floor(D);
  					H=floor(H);
  					H1=floor(H1);
  					H2=floor(H2+0.5);
  			 	}
  				
  			    var dia = D;
  			    if (D < 10){
  			    	dia = "0" + D;
  			    }
  			    
  			    var mes = M;
  			    if (M < 10){
  			    	mes = "0" + M;
  			    }
  			    
  			    var ano = Y;
  			    var hora = H;
  			    
  			    if (H < 10){
  			    	hora = "0" + H;
  			    }
  			    
  			    var min = H1;
  			    if (H1 < 10){
  			    	min = "0" + H1;
  			    }
  			    
  			    var seg= H2;
  			    if (H2 < 10){
  			    	seg = "0" + H2;
  			    }
  			    
  			    //var fechaGregoriana = D + "/" + M + "/" + Y + " " + H + ":" + H1 + ":" + H2;
  			    var fechaGregoriana = dia + "/" + mes + "/" + ano + " " + hora + ":" + min + ":" + seg;
  			    return fechaGregoriana;
  			    //form.nday.value =D;
  				//form.nmonth.value =M;
  				//form.nyear.value = Y;
  				//form.era.value = era;
  				//form.nhour.value = H;
  				//form.nminute.value =H1;
  				//form.nsec.value =H2;
  			}
  			
  
  			function recargarConsulta()
  			{
  	  			$("#accionesDisponibles").css("display","none");
  				$("#microfil").val("");
  				$("#codigoautorizacion").val("");
  				$("#otrodato01").val("");
  				$("#fechaefectiva").val("");
  				$("#fechaproceso").val("");
  				$("#otrodato02").val("");
  				$("#binadquirente").val("");
  				$("#leebanda").val("");
  				$("#otrodato03").val("");
  				$("#codigomonedatrx").val("");
  				$("#datosAdicionales5").val("");
  				$("#rubrocomercio").val("");
  				$("#otrodato04").val("");
  				$("#operador").val("");
  				
  				desabilitarAcciones();
  				
  			}
  			function recargarConsultaBuscar()
  			{
  				desabilitarAcciones();
  				
				$("#divlistaTransacciones").css("display","none");
				$("#accionesDisponibles").css("display","none");    
			
				var tipoTransaccion = $("input[name='tipotransaccion']:checked").val();
				if ($('#fechaInicio').val().length  == 0){
					alert("Debe ingresar fecha de inicio para la busqueda...");
					$("#fechaInicio").focus();
					return;
				}
				
				if ($('#fechaTermino').val().length  == 0){
					alert("Debe ingresar fecha final para la busqueda...");
					$("#fechaTermino").focus();
					return;
				}
				
				if (validarFecha($('#fechaInicio').val()) == false){
					alert("La fecha de inicio ingresada es incorrecta...");
					$("#fechaInicio").focus();
					return;
				}
				
				if (validarFecha($('#fechaTermino').val()) == false){
					alert("La fecha de termino ingresada es incorrecta...");
					$("#fechaTermino").focus();
					return;
				}
				
				if (tipoTransaccion==0){ //Transaccion De Presentacion.//
					buscarTransaccionPresentacion("operacion");
				}
				
				if (tipoTransaccion >= 1 && tipoTransaccion <= 8){

					var codigoTransaccion = "";
					var codigoFuncion = "";
					
					if (tipoTransaccion==1){//Primer Contracargo Total
						codigoTransaccion = "1442";
						codigoFuncion = "450";
					}else if (tipoTransaccion==2){//Primer Contracargo Parcial
						codigoTransaccion = "1442";
						codigoFuncion = "453";
					}else if (tipoTransaccion==3){//Segundo Contracargo Total
						codigoTransaccion = "1442";
						codigoFuncion = "451";
					}else if (tipoTransaccion==4){//Segundo Contracargo Parcial
						codigoTransaccion = "1442";
						codigoFuncion = "454";
					}else if (tipoTransaccion==5){//Peticion De Vale
						codigoTransaccion = "1644";
						codigoFuncion = "603";
					}else if (tipoTransaccion==6){//Confirmacion Peticion Vale
						codigoTransaccion = "1644";
						codigoFuncion = "605";
					}else if (tipoTransaccion==7){//Representacion Total
						codigoTransaccion = "1240";
						codigoFuncion = "205";
					}else if (tipoTransaccion==8){//Representacion Parcial
						codigoTransaccion = "1240";
						codigoFuncion = "282";
					}
					
					buscarTransaccionAccion(codigoTransaccion, codigoFuncion,"operacion");
				}
				
				if (tipoTransaccion==9 || tipoTransaccion==10 ){ //Transaccion De Objeciones Rechazos.  // 
					buscarTransaccionObjecionRechazo();
				}
				
				if (tipoTransaccion==11){ //Transaccion Recuperacion De Cargos Incoming.  //
					var codigoTransaccion = "1740";
					buscarTransaccionRecuperacionCargosIncoming(codigoTransaccion,"operacion");
				}
				
				if (tipoTransaccion==12){ //Transaccion Recuperacion De Cargos Outgoing.  //
					var codigoTransaccion = "1740";
					buscarTransaccionRecuperacionCargosOutgoing(codigoTransaccion,"operacion");
				}
				
				if (tipoTransaccion==13){ //Transaccion Confirmacion de recepcion de vales.  //
					var codigoTransaccion = "CONF_RVL"; 
				    buscarTransaccionConfirmacionRecepcionPeticionVales(codigoTransaccion,"operacion");
				}
  				
  			};
  			
  			
  			function bloquearTipoBusqueda(xkey){
  					
  				var codigosSinGestion =['03_00','25_00','05_00','15_205','01_00','02_00','00_00','04_00','05_205']; 	
  				  				
  				if(codigosSinGestion.indexOf( xkey ) >= 0){
  					$("#tipoFechaBusqueda").val("T");
  					$('#tipoFechaBusqueda').attr('disabled','disabled');
  				}else{
  					$('#tipoFechaBusqueda').removeAttr('disabled');
  					$("#tipoFechaBusqueda").val("G");
  					
  			}
  			};
  			

	</script>
	
</head>

<body>
	<input type="hidden" value="100" name="cantReg" id="cantReg"/>
	
	<div id="popup" style="display: none;">
		
	</div>
	
	<div id="popUpContenido" class="content-popup" style="display: none;"></div>
	
    <div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
       		GESTI&Oacute;N DE TRANSACCIONES
    </div>
	<hr>
    <div style="border:1px">
    <!-- contenedorInput -->
    	<table width="100%" class="table table-sm small">
    		
    		<s:iterator status="stat" value="listaTipoTransacciones">
    			  
    			 <s:if test="%{((#stat.index) % 4) == 0 }"> 	
    			 	<tr>
    			 </s:if> 
    				 <s:if test="%{#stat.index == 0 }"> 	
    				 	<td>
    				 		<div class="form-check custom-control custom-radio">
    				 			<input type="radio" name="tipotransaccion" id='radio<s:property value="#stat.index"/>' class="tipotransaccion custom-control-input" value='<s:property value="codigoxKey"/>' checked="checked" onclick="bloquearTipoBusqueda(this.value);" >
    				 			<label for='radio<s:property value="#stat.index"/>' class="custom-control-label"><s:property value="descripcion" escapeHtml="true" /></label> 
    				 		</div>
    				 	</td>
    				</s:if>
    				<s:else>
    					<td>
    						<div class="form-check custom-control custom-radio">
    							<input type="radio" name="tipotransaccion" id='radio<s:property value="#stat.index"/>' class="tipotransaccion custom-control-input" value='<s:property value="codigoxKey"/>' onclick="bloquearTipoBusqueda(this.value);">
    							<label for='radio<s:property value="#stat.index"/>' class="custom-control-label"><s:property value="descripcion" escapeHtml="true"/></label>
    						</div>
   						</td>
    				</s:else>
    			<s:if test="%{((#stat.index) % 4) == 3 }"> 	
    			 	</tr>
    			 </s:if> 

    		</s:iterator>
<!-- 	      	<tr> -->
<!-- 	      		<td colspan="4">&nbsp; <td> -->
<!-- 	      	</tr>	 -->
    	</table>
   	
    </div>
    
    <!-- <br style="line-height:5px"/>  -->
    
    <div>
    	<!-- contenedorInput -->
    	<table width="100%" class="table table-sm small">
    		<tr class="text-center">
		        <td class="align-middle">TC:</td>
	        	<td class="align-middle"><input type="text" value="" name="numeroTarjeta" id="numeroTarjeta"  class="numericOnly form-control form-control-sm mx-auto" maxlength="16" style="width:106px" /></td>
	        	<td class="align-middle">FECHA:</td>
	        	<td class="align-middle"><select id="tipoFechaBusqueda" class="custom-select"><option value="G">Gesti&oacute;n</option><option value="T">Transacci&oacute;n</option></select></td>
		        <td class="align-middle">DESDE:</td>
		        <td class="align-middle"><input type="text" size="10" maxlength="10" value="" name="fechaInicio" id="fechaInicio" class="campoObligatorio fecha form-control form-control-sm mx-auto text-center datepicker" placeholder="DD/MM/AAAA" /></td>
		        <td class="align-middle">HASTA:</td>
		        <td class="align-middle"><input type="text" size="10" maxlength="10" value="" name="fechaTermino" id="fechaTermino" class="campoObligatorio fecha form-control form-control-sm mx-auto text-center datepicker" placeholder="DD/MM/AAAA" /></td>
		        <td class="align-middle"> 
		        	<input type="button" value="Buscar" name="buscarTransaccion" id="buscarTransaccion" class="btn btn-primary"/>
		        </td>
	      	</tr>
    	</table>
    </div>
    
    <!-- <br style="line-height:5px"/>  -->

	<!-- class="cajaContenido" -->
    <div>
    	<div>
		<!--<table width="100%" class="tablaContenido"  id="comienzolinea"> -->
		<!--<table width="736" class="tablaContenido"  id="tabla1">
	    	<tr>
				<th width='10px'>SEL</th>
				<th width='15%'>NRO TC</th>
				<th width='15%'>FECHA TRANSACC.</th>
				<th width='20%'>COMERCIO</th>
				<th width='5%'>PAIS</th>
				<th width='10%'>MONTO ORIG</th>
				<th width='10%'>MONTO</th>
				<th width='10%'>COD. RAZON</th>
				<th width='10%'>ESTATUS</th>
				<th width='10%'>Nº INCIDENTE</th>
			</tr>
			
	    </table>
	    </div>-->
	    <!-- Paso4 -->
	    <div id="divlistaTransacciones"></div>
		</div> 
    </div>
    
    <!-- <br>  -->
    
	     <div>
	     
	     <!-- contenedorInput -->
    	<table  width="100%" class="table-borderless">
    		<tr>
    		    <td align="center">
    				<input type="button" value="Exportar a Excel" id="exportarTxs" name="exportarTxs" class="btn btn-primary"/>
    			</td>
    		</tr>
    	</table>
    </div>
	
	<!-- <br style="line-height:5px"/>  -->
	
	<div id="accionesDisponibles" style="display:none;" class="table table-sm">
		
	</div>
	    
	<!--  <br style="line-height:5px"/> -->
	 
		<div class="mt-2 col-6 mx-auto">
			<!-- style="display:none;width: 500px;margin-left: 150px;" class="contenedorInput"-->
			<table id="tablaDescripcion"  class="table table-sm" style="display:none;">
				<thead>
					<tr class="text-center">
						<th class="align-middle">
							<!-- style="font-weight: bold;width: 500px;font-size: 14px;background: #a5bad1;" -->
							<div class="font-weight-bold">
								Descripci&oacute;n
							</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center">
						<!--  style="aling: center;width: 500px" -->
						<td id ="descripcion" >&nbsp;
						</td>
					</tr>
				</tbody>
			</table>

		</div>

	<hr size="2px">

		<!-- <br style="line-height:5px"/> -->
	<!--  class="subTitulo" -->
	<div class="col-6 mx-auto my-4 small text-align-center text-center font-weight-bold ">
			Datos de la Cuenta del Cliente    
	</div>
	    
	<div>
	    <!-- class="contenedorInput" -->
    	<table width="100%" class="table table-sm small">
	    	<tr>
    			<td>CLIENTE:</td>
    			<!-- size="50" -->
    			<td><input type="text" name="nomcliente" id="nomcliente" class="campoSalida form-control form-control-sm" /></td>
    			<td>RUT:</td>
    			<td><input type="text" name="rutcliente" id="rutcliente" class="campoSalida form-control form-control-sm"/></td>
    		</tr>
    		
    		<tr>
    			<td>NRO CUENTA:</td>
    			<td><input type="text" class="campoSalida form-control form-control-sm" name="numerocuenta" id="numerocuenta"/></td>
    			<td>CUPO:</td>
    			<td><input type="text" class="campoSalida form-control form-control-sm" name="cupo" id="cupo"/></td>
    		</tr>
    		
    		<tr>
    			<td>FECHA EXP.</td>
    			<td><input type="text" class="campoSalida form-control form-control-sm" name="fechaexpiracion" id="fechaexpiracion" /></td>
    			<td>ESTADO:</td>
    			<td><input type="text" class="campoSalida form-control form-control-sm" name="estado" id="estado" /></td>
    		</tr>
    
	    </table>
	
	</div>
	
	<!-- <br style="line-height:5px"/>  -->

    <!-- <div class="subTitulo">  -->
    <div class="col-6 small mx-auto my-4 text-align-center text-center font-weight-bold">
    	Datos de la Transacción Seleccionada
    </div>
	
	<div>    
		<!-- contenedorInput -->
	     <table width="100%" class="table table-sm small">
	    	
    		<tr>
    			<td>MICROFILM:</td>
    			<td><input type="text" name="microfil" id="microfil" class="campoSalida form-control form-control-sm"/></td>
    			<td>COD. AUTORIZAC.:</td>
    			<td><input type="text" name="codigoautorizacion" id="codigoautorizacion" class="campoSalida form-control form-control-sm"/></td>
    			<td>COD. PROCES.:</td>
    			<td><input type="text" name="otrodato01" id="otrodato01" class="campoSalida form-control form-control-sm"/></td>
    		</tr>
    		
    		<tr>
    			<td>FECHA EFECTIVA:</td>
    			<td><input type="text" name="fechaefectiva" id="fechaefectiva" class="campoSalida form-control form-control-sm"/></td>
    			<td>FECHA PROCESO:</td>
    			<td><input type="text" name="fechaproceso" id="fechaproceso" class="campoSalida form-control form-control-sm"/></td>
    			<td>CONV. CONC.:</td>
    			<td><input type="text" name="otrodato02" id="otrodato02" class="campoSalida form-control form-control-sm"/></td>
    		</tr>
    		
    		<tr>
    			<td>BIN ADQUIRIENTE:</td>
    			<td><input type="text" name="binadquirente" id="binadquirente" class="campoSalida form-control form-control-sm"/></td>
    			<td>LEE BANDA:</td>
    			<td><input type="text" name="leebanda" id="leebanda" class="campoSalida form-control form-control-sm"/></td>
    			<td>CONV. FACT.:</td>
    			<td><input type="text" name="otrodao03" id="otrodato03" class="campoSalida form-control form-control-sm"/></td>
    		</tr>
    		
    		<tr>
    			<td>CODIGO MONEDA TX.:</td>
    			<td><input type="text" name="codigomonedatrx" id="codigomonedatrx" class="campoSalida form-control form-control-sm"/></td>
    			<td>RUBRO DEL COMERCIO:</td>
    			<td><input type="text" name="rubrocomercio" id="rubrocomercio" class="campoSalida form-control form-control-sm"/></td>
    			<td>PTO. SERVICIO:</td>
    			<td><input type="text" name="otrodao04" id="otrodato04" class="campoSalida form-control form-control-sm"/></td>
    		</tr>
    		<tr>
    			<td><p id="operadortxt">OPERADOR :</p></td>
    			<td><input type="text" name="operador" id="operador" class="campoSalida form-control form-control-sm"></td>
    			<td>OBSERVACIONES:</td>
    			<td colspan="3"><input type="text" name="datosAdicionales5" id="datosAdicionales5" class="campoSalida form-control form-control-sm" size="61"/></td>
    		</tr>
    		
    	</table>
    </div>
    
    <!-- <br style="line-height:5px"/>  -->
    
    <div>
    <!-- contenedorInput -->
    	<table  width="100%" class="table-borderless">
    	<tr><td align="center">
    		<%
			if(usuarioLog.getActualizacion() == 1){ %>
    			<input type="button" value="Gestion Por Tarjeta" id="gestionPorCuenta" name="gestionPorCuenta" class="btn btn-primary" disabled/>
    		<% } %>
    		<% if(usuarioLog.getCreacion() == 1){ %>
    			<input type="button" value="Otras Gestiones" id="otraGestion" name="otraGestion" class="btn btn-primary" disabled/>
    		<% } %>
    		<% if(usuarioLog.getActualizacion() == 1){ %>
    			<input type="button" value="Objetar" id="objetar" class="btn btn-primary" disabled/>
    		<% } %>
    		<% if(usuarioLog.getCreacion() == 1){ %>
    			<input type="button" value="Cargar / Abonar" id="cargoabono" class="btn btn-primary" disabled/>
    		<% } %>
    		<% if(usuarioLog.getCreacion() == 1){ %>
    			<input type="button" value="Registrar P&eacute;rdida" id="btn-regitrar-perdida" class="btn btn-primary" disabled/>
    		<% } %>
    	</td></tr>
    	<tr class="text-center">
    		<td>
    			<input type="button" value="Exportar a Excel" id="exportar" name="exportar" class="btn btn-primary" disabled/>
    		</td>
    	</tr>
    	</table>
    </div>
 
</body>
</html>