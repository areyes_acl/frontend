<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Reporte TC46</title>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script>
			$(".datepicker").datepicker();

	   		$(document).ready(function(){
	   			cargaOperador();

				var f = new Date();
		
				var dia = f.getDate();
				var mes = (f.getMonth() + 1);
				if (mes < 10)
					mes = "0" + mes;
					
				if (dia < 10)
					dia = "0" + dia;
				var ano = f.getFullYear();
				$(".fecha").val(dia + "/" + mes + "/" + ano);			
	   			
	   			
	   		});
	   		
	   		$('#fechaInicio').on('click', function(){
	   			$(this).val("");
	   		});
	   		$('#fechaTermino').on('click', function(){
	   			$(this).val("");
	   		});
	   		
	   		function cargaOperador(){
				$.ajax({
			   			url:'cargarComboSinOperador',
			   			type:'POST'
			   		}).done(function(data){
			   			data.listaOperadores;
			   			var datos = (data.listaOperadores).split("~");
			   			
			   			var strCombo = "";
			   			
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   				
			   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
							}
			   			
			   			$("#idOperador").append(strCombo);
			   		});
			}
	   </script>
	   
	   <script type="text/javascript">
	   	$('#buscar').on('click', function(){
	   		var operador = $('#idOperador').val();
	   		var fechaDesde = $('#fechaInicio').val();
	   		var fechaHasta = $('#fechaTermino').val();
	   		
	   		var f1 = Date.parse(fechaDesde);
	   		var f2 = Date.parse(fechaHasta);
	   		
	   		if(fechaDesde == ''){
	   			alert("La fecha de inicio no puede estar vacía");
	   			return;
	   		}
	   		
	   		if(fechaHasta == ''){
	   			alert("La fecha de término no puede estar vacía");
	   			return;
	   		}
	   		
	   		if (validarFecha($('#fechaInicio').val()) == false) {
				alert("La fecha de inicio ingresada es incorrecta...");
				$("#fechaInicio").focus();
				return;
			}

			if (validarFecha($('#fechaTermino').val()) == false) {
				alert("La fecha de termino ingresada es incorrecta...");
				$("#fechaTermino").focus();
				return;
			}
			
			var fecha1 = convertirFecha($('#fechaInicio').val());
			var fecha2 = convertirFecha($('#fechaTermino').val());
			if (fecha1 > fecha2) {
				alert("La fecha de inicio es mayor a la fecha de termino, verifiquela...");
				return;
			}
	   		
	   		$.ajax({
				url:'listaTC46Action?fechaDesde='+fechaDesde+'&fechaHasta='+fechaHasta+'&operador='+operador,
				type:'GET',
			}).done(function(resp){
				//var datos = (resp.listaTC46).split("~");
				var table = document.getElementById('myTable');
   				table.style.display = "";
   				
				var datos = (resp.listaTC46).split("~");
				
				console.log(datos[1]);
				var tabla = "";
				var total_count = 0;
				var monto_intercambio = 0;
				var intercambio_sign = "";
				var monto_reembolso = 0;
				var reembolso_sign = "";
				var monto_cargo_visa = 0;
				var visa_sign = "";
				var monto_a_pagar = 0;
				var monto_a_pagar_sign = "";
				var totales = "";
				
				//console.log(datos.length);
				if(datos[1] == 'No hay datos'){
					var exportBlock = document.getElementById('exportBlock');
   					exportBlock.style.display = "none";
   					var totalesResultados = document.getElementById('totalesResultados');
   					totalesResultados.style.display = "none";
   					tabla += '<tr>'
							+'<td colspan="6" style="text-align: center;">No se han encontrado resultados</td>'
							+'</tr>';
				}
				else{
					var exportBlock = document.getElementById('exportBlock');
   					exportBlock.style.display = "";
					var totalesResultados = document.getElementById('totalesResultados');
   					totalesResultados.style.display = "";
   					
					for(var i=1;i<datos.length-1;i++){
						var fila = datos[i].split("|");		
						
						tabla += '<tr class="text-center">'
							+'<td class="align-middle" style="text-align: center;">'+fila[0]+'</td>'
							+'<td class="align-middle" style="text-align: center;">'+fila[1]+'</td>'
							+'<td class="align-middle" style="text-align: center;">'+fila[2]+' '+fila[3]+'</td>'
							+'<td class="align-middle" style="text-align: center;">'+fila[4]+' '+fila[5]+'</td>'
							+'<td class="align-middle" style="text-align: center;">'+fila[6]+' '+fila[7]+'</td>'
							+'<td class="align-middle" style="text-align: center;">'+fila[8]+' '+fila[9]+'</td>'
							+'</tr>';
						
						total_count = total_count + parseInt(fila[1]);
						
						if(fila[3] == 'CR'){
							monto_intercambio = monto_intercambio - parseInt(fila[2]);
						}
						else{
							monto_intercambio = monto_intercambio + parseInt(fila[2]);
						}
						
						if(fila[5] == 'CR'){
							monto_reembolso = monto_reembolso - parseInt(fila[4]);
						}
						else{
							monto_reembolso = monto_reembolso + parseInt(fila[4]);
						}
				
						if(fila[7] == 'CR'){
							monto_cargo_visa = monto_cargo_visa - parseInt(fila[6]);
						}
						else{
							monto_cargo_visa = monto_cargo_visa + parseInt(fila[6]);
						}
						
						if(fila[9] == 'CR'){
							monto_a_pagar = monto_a_pagar - parseInt(fila[8]);
						}
						else{
							monto_a_pagar = monto_a_pagar + parseInt(fila[8]);
						}	
						
					}
					if(monto_intercambio < 0){
					monto_intercambio = monto_intercambio * -1;
					intercambio_sign = 'CR';
				}
				else{
					intercambio_sign = 'DB';
				}
					
				if(monto_reembolso < 0){
					monto_reembolso = monto_reembolso * -1;
					reembolso_sign = 'CR';
				}
				else{
					reembolso_sign = 'DB';
				}
					
				if(monto_cargo_visa < 0){
					monto_cargo_visa = monto_cargo_visa * -1;
					visa_sign = 'CR';
				}
				else{
					visa_sign = 'DB';
				}
					
				if(monto_a_pagar < 0){
					monto_a_pagar = monto_a_pagar * -1;
					monto_a_pagar_sign = 'CR';
				}
				else{
					monto_a_pagar_sign = 'DB';
				}
				
				tabla += '<tr class="text-center" style="font-weight: bold" id="totales">';
				tabla += '<td class="align-middle" style="text-align: center;" >TOTAL</td>';
			 	tabla += '<td class="align-middle" style="text-align: center;">'+total_count+'</td>';
				tabla += '<td class="align-middle" style="text-align: center;">'+monto_intercambio+' '+intercambio_sign+'</td>';
				tabla += '<td class="align-middle" style="text-align: center;">'+monto_reembolso+' '+reembolso_sign+'</td>';
				tabla += '<td class="align-middle" style="text-align: center;">'+monto_cargo_visa+' '+visa_sign+'</td>';
				tabla += '<td class="align-middle" style="text-align: center;">'+monto_a_pagar+' '+monto_a_pagar_sign+'</td>';
				tabla += '</tr>';
					
				}	
				
				//console.log(tabla);
				
				
				$('#tablaRessultados').html(tabla);
				//$('#tablaRessultados').html(totales);
				
			}).error(function(error, status, setting){
				alert("Error Parametros: "+error);
			});
	   	});
	   	
	   		
	   </script>
	   <script>
	   
	   		$('#exportar').on('click', function(){
	   			
	   			DataExportar = "";
				DatasExcel = "";
				OperadorExcel = document.getElementById('idOperador').options[document.getElementById('idOperador').selectedIndex].text.toUpperCase();
				var total_count = $('#total_count').html();
				//alert(total_count);
				
						idTablaExportar = "myTable";
						
						DataExportar += "TIT|| |"+OperadorExcel+"| | | ||~";
						DataExportar += "TIT|FECHA PAGO|CANTIDAD|MONTO INTERCAMBIO|MONTO COMISIÓN|CARGOS VISA|MONTO TOTAL|~";
						//agregar linea previa a los registros
						//DataExportar += "DET| |~";
						DataExportar += exportarExcelTC46(idTablaExportar,
								DatasExcel);
						//console.log(DataExportar);
						exportarTc46(DataExportar, "export");
				
				
	   			
	   		});
	   
	   </script>
	   
</head>
<body>
	<!-- <div class="titulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		RESUMEN REPORTE TC46 VISA
	</div>
	<hr>
	<!-- <br style="line-height:5px"/>  -->
	
	<div>
		<!-- class="contenedorInput" -->
      		<table width="100%" class="table table-sm">
               <tr class="text-right">
           			<td class="align-middle">DESDE:</td>
			        <td class="align-middle text-center">
			        	<!-- style="width: 57px;" size="10" -->
			        	<input type="text" maxlength="10" value="" name="fechaInicio" id="fechaInicio" class="campoObligatorio fecha form-control form-control-sm col-8 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
					</td>
			        <td class="align-middle">HASTA:</td>
			        <td class="align-middle text-center">
			        	<!-- style="width: 57px;" size="10" -->
			        	<input type="text" maxlength="10" value="" name="fechaTermino" id="fechaTermino" class="campoObligatorio fecha form-control form-control-sm col-8 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
		        	</td>
	           		
	           		<td class="align-middle text-center">Operador:</td>
	           		<td class="align-middle">
	       				<select id="idOperador" class="custom-select">
	       				</select>
	          		</td>
	           		<td class="align-middle">
	          			<input type="button"  id="buscar" value="Buscar" class="btn btn-primary">
	           		</td>
       			</tr>
      		</table>
      </div>
	
	<!-- <br style="line-height:5px"/>  -->
	
	<div style="margin:0px;padding:0px;">
		<!-- class="tablaContenido" -->
		<table width="100%" class="table table-sm small" id="myTable" style="display:none;">
			<thead>
				<tr class="text-center">
				    <th class="align-middle" style="text-align:center;">FECHA PAGO</th>
					<th class="align-middle" style="text-align:center;">CANTIDAD</th>
					<th class="align-middle" style="text-align:center;">MONTO INTERCAMBIO</th>
					<th class="align-middle" style="text-align:center;">MONTO COMISI&Oacute;N</th>
					<th class="align-middle" style="text-align:center;">CARGOS VISA</th>
					<th class="align-middle" style="text-align:center;">MONTO TOTAL</th>
				</tr>
			</thead>
			<tbody id="tablaRessultados">
			</tbody>
			<tfoot id="totalesResultados">
					
			</tfoot>
		</table>
	</div>
		
	<!-- <br style="line-height: 5px" />  -->


	<div id="exportBlock" style="display:none">
		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm table-borderless">
			<tr class="text-center">
				<td align="center"><input type="button"
					value="Exportar Tabla" id="exportar" name="exportar" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>