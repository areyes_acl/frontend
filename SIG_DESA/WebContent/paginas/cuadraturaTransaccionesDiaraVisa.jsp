<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Cuadratura Diaria De Transacciones Visa</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
	   <script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
	   
	   <script>
	   
	   		$(".datepicker").datepicker();
	   		
	   		$(document).ready(function(){
	   			cargaOperador();
	   		});
	   		
	   		function cargaOperador(){
				$.ajax({
			   			url:'cargarComboOperadores',
			   			type:'POST'
			   		}).done(function(data){
			   			data.listaOperadores;
			   			var datos = (data.listaOperadores).split("~");
			   			
			   			var strCombo = "";
			   			
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   				
			   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
							}
			   			
			   			$("#idOperador").append(strCombo);
			   		});
			}
	   </script>
	   <script>
		
			$("#buscar").click(function(){
				
					if ($('#fechaProceso').val().length  == 0){
							alert("Debe ingresar fecha de proceso para realizar la busqueda...");
							$("#fechaProceso").focus();
							return;
					}
					
					if (validarFecha($('#fechaProceso').val()) == false){
							alert("La fecha ingresada esta incorrecta, verifiquela...");
							$("#fechaProceso").focus();
							return;
					}
						
					var operador = $('#idOperador').val();
						
					var parametros = "fechaProceso=" + $('#fechaProceso').val() + "&operador=" + operador;
					//alert('cuadraturaTransaccionesDiariaVisaAction');
					$.ajax({
						url: "cuadraturaTransaccionesDiariaVisaAction",
						type: "POST",
						data: parametros,
						dataType: "json",
						error: function(XMLHttpRequest, textStatus, errorThrown){
							alert('Error ' + textStatus);
							alert(errorThrown);
							alert(XMLHttpRequest.responseText);
						},
			    		success: function(data){
			    			mostrarCuadraturaDiaria(data.listaCuadraturaDiaria);
			    			
						}
					});
	  				
			});
			
			$("#fechaProceso").focus(function(){
				$("#divCuadraturaTransaccion").css("display","none");
				$("#exportar").attr("disabled", true);
			});
			
			
			$("#exportar").click(function(){
				
				var operador = document.getElementById('idOperador').options[document.getElementById('idOperador').selectedIndex].text.toUpperCase();
				
				DataExportar="";
				DatasExcel="";
				
				DataExportar += "TIT||"+operador+"| | ||~";
				idTablaExportar = "myTableIncoming";
				DataExportar += "TIT|TIPO TRANSACCION|NRO TRANSACCIONES|TIPO MONEDA|MONTO~";
				DataExportar += exportarExcelLogProcesos(idTablaExportar, DatasExcel);
				
				idTablaExportar = "myTableOutgoing";
				DatasExcel="";
				DataExportar += "TIT|TIPO TRANSACCION|NRO TRANSACCIONES|TIPO MONEDA|MONTO~";
				DataExportar += exportarExcelLogProcesos(idTablaExportar, DatasExcel);
				
				exportarGenerico(DataExportar, "export");
				
			});
			
		</script>
		
		<script>
			
				function mostrarCuadraturaDiaria(listaCuadraturaDiaria){
						
					var stringHtml = "";
					var lstcuadraturaDiaria = listaCuadraturaDiaria.split("~");
					var esttrx = lstcuadraturaDiaria[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					if (codtrx[1]=="0")
					{
							var idTablaocultar = document.getElementById('tablaOcultar'); 

							idTablaocultar.style.display='none'; 
							stringHtml = "";
							//class="tablaContenido"
							stringHtml = "<table width='100%' class='table table-sm small' id='myTableIncoming'>";
							stringHtml = stringHtml + "<tr class='text-center'>";
	    					stringHtml = stringHtml + "<th class='align-middle' width='25%'>TIPO DE TRANSACCION</th>";
	    					stringHtml = stringHtml + "<th class='align-middle' width='25%'>NRO TRANSACCIONES</th>";
	    					stringHtml = stringHtml + "<th class='align-middle' width='25%'>TIPO MONEDA</th>";
	    					stringHtml = stringHtml + "<th class='align-middle' width='25%'>MONTO</th>";
	    					stringHtml = stringHtml + "</tr>";
	    					var detallecuadraturadiaria = lstcuadraturaDiaria[1].split("|");
	    					
	    					var l_compra_clp           = detallecuadraturadiaria[0];
	    					var l_qtrx_clp_1           = detallecuadraturadiaria[1];
	    					var l_monto_1              = detallecuadraturadiaria[2];
	    					
	    					var l_creditos_clp         = detallecuadraturadiaria[3];
	    					var l_qtrx_clp_2           = detallecuadraturadiaria[4];
	    					var l_monto_2              = detallecuadraturadiaria[5];
	    			        
	    					var l_avances_clp          = detallecuadraturadiaria[6];
	    					var l_qtrx_clp_3           = detallecuadraturadiaria[7];
	    					var l_monto_3              = detallecuadraturadiaria[8];
	    				
	    					var l_representaciones_clp = detallecuadraturadiaria[9];
	    					var l_qtrx_clp_4           = detallecuadraturadiaria[10];
	    					var l_monto_4              = detallecuadraturadiaria[11];
	    				
	    					var l_confirmvales_clp     = detallecuadraturadiaria[12];
	    					var l_qtrx_clp_5           = detallecuadraturadiaria[13];
	    			          
	    					var l_compra_us            = detallecuadraturadiaria[14];
	    					var l_qtrx_us_1            = detallecuadraturadiaria[15];
	    					var l_monto_us_1           = detallecuadraturadiaria[16];
	    					  
	    					var l_creditos_us          = detallecuadraturadiaria[17];
	    					var l_qtrx_us_2            = detallecuadraturadiaria[18];
	    					var l_monto_us_2           = detallecuadraturadiaria[19];
	    					
	    					var l_avances_us           = detallecuadraturadiaria[20];
	    					var l_qtrx_us_3            = detallecuadraturadiaria[21];
	    					var l_monto_us_3           = detallecuadraturadiaria[22];
	    					
	    					var l_representaciones_us  = detallecuadraturadiaria[23];
	    					var l_qtrx_us_4            = detallecuadraturadiaria[24];
	    					var l_monto_us_4           = detallecuadraturadiaria[25];
	    			        
	    					var l_miscelaneos_us       = detallecuadraturadiaria[26];   
	    					var l_qtrx_us_5            = detallecuadraturadiaria[27];
	    					var l_monto_us_5           = detallecuadraturadiaria[28];
	    			        
	    					var l_confirmvales_us      = detallecuadraturadiaria[29];
	    					var l_qtrx_us_6            = detallecuadraturadiaria[30];
	    					
	    					var l_1contra_clp          = detallecuadraturadiaria[31];
	    					var l_qtrx_1con            = detallecuadraturadiaria[32];
	    					var l_outgoing_monto_1     = detallecuadraturadiaria[33];
	    				          
	    					var l_2contra_clp          = detallecuadraturadiaria[34];
	    					var l_qtrx_2con            = detallecuadraturadiaria[35];
	    					var l_outgoing_monto_2     = detallecuadraturadiaria[36];
	    			  
	    					var l_petval_clp           = detallecuadraturadiaria[37];
	    					var l_qtrx_petval          = detallecuadraturadiaria[38];
	    						     
	    					var l_1contra_us           = detallecuadraturadiaria[39];
	    					var l_qtrx_1con_us         = detallecuadraturadiaria[40];
	    					var l_outgoing_monto_1_us  = detallecuadraturadiaria[41];
	    					
	    					var l_2contra_us           = detallecuadraturadiaria[42];
	    					var l_qtrx_2con_us         = detallecuadraturadiaria[43];
	    					var l_outgoing_monto_2_us  = detallecuadraturadiaria[44];

	    					var l_misc_us              = detallecuadraturadiaria[45];
	    					var l_qtrx_mis_us          = detallecuadraturadiaria[46];
	    					var l_outgoing_mis_us      = detallecuadraturadiaria[47];
	    						
	    					var l_petval_us            = detallecuadraturadiaria[48];
	    					var l_qtrx_petval_us       = detallecuadraturadiaria[49];
	    				
	    					/************************************/
	    					/* Variables de subtotales          */           
	    					/************************************/
	    					var subtotal_inc_monto_clp = 0;
	    					var subtotal_inc_cantidad_clp = 0;
	    					
	    					var subtotal_inc_monto_usd = 0;
	    					var subtotal_inc_cantidad_usd = 0;
	    					
	    					var subtotal_out_monto_clp = 0;
	    					var subtotal_out_cantidad_clp = 0;
	    					
	    					var subtotal_out_monto_usd = 0;
	    					var subtotal_out_cantidad_usd = 0;
	    					
	    				
	    					/************************************/
	    					/* Transacciones de incoming        */           
	    					/************************************/
	       					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_compra_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_clp_1 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_monto_1)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	       					
	    					//**********************************//
	    					// SUBTOTALES CLP                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp) + parseInt(l_qtrx_clp_1);
	    					subtotal_inc_monto_clp    = parseInt(subtotal_inc_monto_clp)  + parseInt(l_monto_1);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_creditos_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_clp_2 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_monto_2)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    					
	    					//**********************************//
	    					// SUBTOTALES CLP                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp) + parseInt(l_qtrx_clp_2);
	    					subtotal_inc_monto_clp    = parseInt(subtotal_inc_monto_clp)  + parseInt(l_monto_2);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_avances_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_clp_3 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_monto_3)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    			        
	    					//**********************************//
	    					// SUBTOTALES CLP                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp) + parseInt(l_qtrx_clp_3);
	    					subtotal_inc_monto_clp    = parseInt(subtotal_inc_monto_clp)  + parseInt(l_monto_3);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_representaciones_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_clp_4 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_monto_4)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    				
	    					//**********************************//
	    					// SUBTOTALES CLP                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp) + parseInt(l_qtrx_clp_4);
	    					subtotal_inc_monto_clp    = parseInt(subtotal_inc_monto_clp)  + parseInt(l_monto_4);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_confirmvales_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_clp_5 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero("0")) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    				
	    					//**********************************//
	    					// SUBTOTALES CLP                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_clp = parseInt(subtotal_inc_cantidad_clp) + parseInt(l_qtrx_clp_5);
	    					subtotal_inc_monto_clp    = parseInt(subtotal_inc_monto_clp)  + parseInt(0);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + "SUBTOTAL " + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + subtotal_inc_cantidad_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "   " + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(subtotal_inc_monto_clp.toString())) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    					
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_compra_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_us_1 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_monto_us_1)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    				
	    					//**********************************//
	    					// SUBTOTALES USD                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd) + parseInt(l_qtrx_us_1);
	    					subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd) + parseInt(l_monto_us_1);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_creditos_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_us_2 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_monto_us_2)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    			
	    					//**********************************//
	    					// SUBTOTALES USD                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd) + parseInt(l_qtrx_us_2);
	    					subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd) + parseInt(l_monto_us_2);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_avances_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_us_3 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_monto_us_3)) + "</td>";
	    					stringHtml = stringHtml + "</tr>"; 
	    			
	    					//**********************************//
	    					// SUBTOTALES USD                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd) + parseInt(l_qtrx_us_3);
	    					subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd) + parseInt(l_monto_us_3);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_representaciones_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_us_4 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_monto_us_4)) + "</td>";
	    					stringHtml = stringHtml + "</tr>"; 
	    					
	    					//**********************************//
	    					// SUBTOTALES USD                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd) + parseInt(l_qtrx_us_4);
	    					subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd) + parseInt(l_monto_us_4);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_miscelaneos_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_us_5 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_monto_us_5)) + "</td>";
	    					stringHtml = stringHtml + "</tr>"; 
	    				
	    					//**********************************//
	    					// SUBTOTALES USD                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd) + parseInt(l_qtrx_us_5);
	    					subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd) + parseInt(l_monto_us_5);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_confirmvales_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_us_6 + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero("0")) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    				
	    					//**********************************//
	    					// SUBTOTALES USD                  *//
	    					//**********************************//
	    					subtotal_inc_cantidad_usd = parseInt(subtotal_inc_cantidad_usd) + parseInt(l_qtrx_us_6);
	    					subtotal_inc_monto_usd = parseInt(subtotal_inc_monto_usd) + parseInt(0);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + "SUBTOTAL " + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + subtotal_inc_cantidad_usd + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "   " + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(subtotal_inc_monto_usd.toString())) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    					
	    					stringHtml = stringHtml + "</table>";
	    					
	    					stringHtml = stringHtml + "<br style='line-height:5px'/>";
	    					
	    					/************************************/
	    					/* Transacciones de outgoing        */           
	    					/************************************/
	    					//stringHtml = stringHtml + "<table width='100%' class='tablaContenido'>";
	    					
	    					//stringHtml = stringHtml + "</table>";
	    					
	    					// class="tablaContenido"
	    					stringHtml = stringHtml + "<table width='100%' class='table table-sm small' id='myTableOutgoing'>";
	    					stringHtml = stringHtml + "<tr class='text-center'>";
	    					stringHtml = stringHtml + "<th class='align-middle' width='25%'>TIPO DE TRANSACCION</th>";
	    					stringHtml = stringHtml + "<th class='align-middle' width='25%'>NRO TRANSACCIONES</th>";
	    					stringHtml = stringHtml + "<th class='align-middle' width='25%'>TIPO MONEDA</th>";
	    					stringHtml = stringHtml + "<th class='align-middle' width='25%'>MONTO</th>";
	    					stringHtml = stringHtml + "</tr>";
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_1contra_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_1con + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_outgoing_monto_1)) + "</td>";
	    					stringHtml = stringHtml + "</tr>"; 
	    					
	    					//**********************************//
	    					// SUBTOTALES CLP                  *//
	    					//**********************************//
	    					subtotal_out_cantidad_clp = parseInt(subtotal_out_cantidad_clp) + parseInt(l_qtrx_1con);
	    					subtotal_out_monto_clp    = parseInt(subtotal_out_monto_clp) + parseInt(l_outgoing_monto_1);
	    					
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_2contra_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_2con + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_outgoing_monto_2)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    				    
	    					//**********************************//
	    					// SUBTOTALES CLP                  *//
	    					//**********************************//
	    					subtotal_out_cantidad_clp = parseInt(subtotal_out_cantidad_clp) + parseInt(l_qtrx_2con);
	    					subtotal_out_monto_clp    = parseInt(subtotal_out_monto_clp) + parseInt(l_outgoing_monto_2);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_petval_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_petval + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "CLP" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero("0")) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    			  
	    					//**********************************//
	    					// SUBTOTALES CLP                  *//
	    					//**********************************//
	    					subtotal_out_cantidad_clp = parseInt(subtotal_out_cantidad_clp) + parseInt(l_qtrx_petval);
	    					subtotal_out_monto_clp    = parseInt(subtotal_out_monto_clp) + parseInt(0);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + "SUBTOTAL " + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + subtotal_out_cantidad_clp + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "   " + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(subtotal_out_monto_clp.toString())) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    					
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_1contra_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_1con_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_outgoing_monto_1_us)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    					
	    					subtotal_out_cantidad_usd = parseInt(subtotal_out_cantidad_usd) + parseInt(l_qtrx_1con_us);
	    					subtotal_out_monto_usd    = parseInt(subtotal_out_monto_usd) + parseInt(l_outgoing_monto_1_us);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_2contra_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_2con_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_outgoing_monto_2_us)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    					
	    					subtotal_out_cantidad_usd = parseInt(subtotal_out_cantidad_usd) + parseInt(l_qtrx_2con_us);
	    					subtotal_out_monto_usd    = parseInt(subtotal_out_monto_usd) + parseInt(l_outgoing_monto_2_us);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_misc_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_mis_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(l_outgoing_mis_us)) + "</td>";
	    					stringHtml = stringHtml + "</tr>";

	    					subtotal_out_cantidad_usd = parseInt(subtotal_out_cantidad_usd) + parseInt(l_qtrx_mis_us);
	    					subtotal_out_monto_usd    = parseInt(subtotal_out_monto_usd) + parseInt(l_outgoing_mis_us);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%'>" + l_petval_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + l_qtrx_petval_us + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "USD" + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero("0")) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    				
	    					subtotal_out_cantidad_usd = parseInt(subtotal_out_cantidad_usd) + parseInt(l_qtrx_petval_us);
	    					subtotal_out_monto_usd    = parseInt(subtotal_out_monto_usd) + parseInt(0);
	    					
	    					stringHtml = stringHtml + "<tr>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + "SUBTOTAL " + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + subtotal_out_cantidad_usd + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='center'>" + "   " + "</td>";
	    					stringHtml = stringHtml + "<td class='align-middle' width='25%' align='right'>" + formato_numero(colocaComaNumero(subtotal_out_monto_usd.toString())) + "</td>";
	    					stringHtml = stringHtml + "</tr>";
	    					
	  						stringHtml = stringHtml + "</table>";
	  						
	    					$('#divCuadraturaTransaccion').html(stringHtml);
	    					$("#divCuadraturaTransaccion").css("display","");
	    					
	    					$("#exportar").attr("disabled", false);
	    				
					}
					else{
						var mensaje = lstcuadraturaDiaria[1].split("|");
						alert(mensaje[1]);
					}
				}
		
		</script>
	   
   </head>
   
   <body>
       
      <!-- <div class="titulo"> -->
	  <div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
       		CUADRATURA DE TRANSACIONES DIARIA
      </div>
      
      <!-- <br style="line-height:5px"/>  -->
      <hr/>
      
      <div class="mb-4">
      		<!-- <table width="100%" class="contenedorInput">  -->
		   	<table width="100%" class="table table-sm my-0 mx-auto">
               <tr class='text-center'>
           			<td class="align-middle text-right">Fecha Proceso :</td>
           			<td class="align-middle">
           				<input type="text" name="fechaProceso" id="fechaProceso" size="10" maxlength="10" class="campoObligatorio fecha form-control form-control-sm c mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
              			
             		</td>
             		<td class="align-middle text-right">Operador :</td>
             		<td class="align-middle">
           				<select id="idOperador" class="custom-select w-75">
           				</select>
             		</td>
             		<td class="align-middle">
             			<input type="button"  id="buscar" value="Buscar" class="btn btn-primary">
             		</td>
       			</tr>
      		</table>
      </div>
     
      <!-- <br style="line-height:5px"/>  -->
      
      <div>
      		<!-- class="tablaContenido" -->
			<table width="100%" class="table table-sm small" id="tablaOcultar">
	    	<tr class="text-center">
				<th class="align-middle" width='25%'>TIPO DE TRANSACCION</th>
       			<th class="align-middle" width='25%'>NRO TRANSACCIONES</th>
       			<th class="align-middle" width='25%'>TIPO MONEDA</th>
       			<th class="align-middle" width='25%'>MONTO</th>
       			
			</tr>
	    	</table>
	    	<div id="divCuadraturaTransaccion">
	    	</div>
	    
      </div>
      
      <!-- <br style="line-height:5px"/>  -->
      
      
    <div>
    	<!-- <table width="100%" class="contenedorInput">  -->
    	<table  width="100%" class="table-borderless">
    		<tr>
    		    <td align="center">
    				<input type="button" value="Exportar Tabla" name="exportar" id="exportar" class="btn btn-primary" disabled/>
    			</td>
    		</tr>
    	</table>
    </div>

   </body>
</html>