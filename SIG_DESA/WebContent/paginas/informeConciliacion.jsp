<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<title>Informe de conciliaci&oacute;n</title>
<script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script type="text/javascript">
	// LOAD INIT PAGE
	
	$(".datepicker").datepicker();

	$("#buscar")
			.click(
					function() {

						if ($('#fechaProceso').val().length == 0) {
							alert("Debe ingresar fecha de proceso para realizar la busqueda...");
							$("#fechaProceso").focus();
							return;
						}

						if (validarFecha($('#fechaProceso').val()) == false) {
							alert("La fecha ingresada esta incorrecta, verifiquela...");
							$("#fechaProceso").focus();
							return;
						}

						var fechaProceso = $('#fechaProceso').val();

						//var parametros = "fechaProceso=" + fechaProceso;

						$.ajax({
							url : "informeConciliacionAction",
							type : "POST",
							data : {
								fechaInicio : fechaProceso,
							},
							dataType : "json",
							error : function(XMLHttpRequest, textStatus,
									errorThrown) {
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
							},
							success : function(data) {
								mostrarTransacciones(data);
							}
						});

					});

	function mostrarTransacciones(data) {

		var stringHtmlTrxPar = "";
		var stringHtml = "";
		var stringHtmlDetalle = "";
		var stringHtmlDetalleIC = "";
		var cantidadTrxNoPareadas = data.listaFinalNoAutorizadas.length;
		var cantidadTrxPareadas = data.listaFinalAutorizadas.length;
		var cantidadTrxNoPareadasIC = data.listaFinalNoAutorizadasIC.length;
		var montoTotalTrxNoPar = formato_numero(data.sumaMontoTrxNoPar);
		var montoTotalTrxPar = formato_numero(data.sumaMontoTrxPar);
		var montoTotalTrxNoParIC = formato_numero(data.sumaMontoTrxNoParIC);

		// TABLA DE TOTALES
		
		// stringHtmlTrxPar = "<br style='line-height:5px'/>";
		
		// class='tablaContenido'
		
		// class='tablaContenido'
		stringHtmlTrxPar += "<table class='table table-sm small' width='100%' id='trx-table'>";
		stringHtmlTrxPar += "<tr class='text-center'>";
		stringHtmlTrxPar += "<th class='align-middle' width='25%'>Total Trx. Pareadas</th>";
		stringHtmlTrxPar += "<th class='align-middle' width='25%'>Total Trx. No Pareadas Transbank</th>";
		stringHtmlTrxPar += "<th class='align-middle' width='25%'>Total Trx. No Pareadas IC</th>";
		stringHtmlTrxPar += "</tr>";
		stringHtmlTrxPar += "<tr class='text-center'>";
		stringHtmlTrxPar += "<td class='align-middle' align='center'>";
		stringHtmlTrxPar += "<input type='text' name='totalTrxPareadas' id='totalTrxPareadas' size='10' maxlength='10' value='" + cantidadTrxPareadas + "' class='campoSalidaNum form-control form-control-sm text-center col-5 mx-auto' disabled/>";
		stringHtmlTrxPar += "</td>";
		stringHtmlTrxPar += "<td class='align-middle' align='center'>";
		stringHtmlTrxPar += "<input type='text' name='totalTrxNoPareadas' id='totalTrxNoPareadas' size='10' maxlength='10' value='" + cantidadTrxNoPareadas + "' class='campoSalidaNum form-control form-control-sm text-center col-5 mx-auto' disabled/>";
		stringHtmlTrxPar += "</td>";
		stringHtmlTrxPar += "<td class='align-middle' align='center'>";
		stringHtmlTrxPar += "<input type='text' name='totalTrxNoPareadasIC' id='totalTrxNoPareadasIC' size='10' maxlength='10' value='" + cantidadTrxNoPareadasIC + "' class='campoSalidaNum form-control form-control-sm text-center col-5 mx-auto' disabled/>";
		stringHtmlTrxPar += "</td>";
		stringHtmlTrxPar += "</tr>";
		stringHtmlTrxPar += "</table>";

		// TABLA DE MONTOS
		
		//stringHtmlTrxPar += "<br style='line-height:5px'/>";
		
		stringHtmlTrxPar += "<table class='table table-sm small' width='100%' id='trx-table-monto'>";
		stringHtmlTrxPar += "<tr class='text-center'>";
		stringHtmlTrxPar += "<th class='align-middle' width='25%'>Monto Total Trx. Pareadas</th>";
		stringHtmlTrxPar += "<th class='align-middle' width='25%'>Monto Total Trx. No Pareadas Transbank</th>";
		stringHtmlTrxPar += "<th class='align-middle' width='25%'>Monto Total Trx. No Pareadas IC</th>";
		stringHtmlTrxPar += "</tr>";
		stringHtmlTrxPar += "<tr class='text-center'>";
		stringHtmlTrxPar += "<td class='align-middle' align='center'>";
		stringHtmlTrxPar += "<input type='text' name='montoTotalTrxPareadas' id='montoTotalTrxPareadas' size='10' maxlength='10' value='" + montoTotalTrxPar + "' class='campoSalidaNum form-control form-control-sm text-center col-5 mx-auto' disabled/>";
		stringHtmlTrxPar += "</td>";
		stringHtmlTrxPar += "<td class='align-middle' align='center'>";
		stringHtmlTrxPar += "<input type='text' name='montoTotalTrxNoPareadas' id='montoTotalTrxNoPareadas' size='10' maxlength='10' value='" + montoTotalTrxNoPar + "' class='campoSalidaNum form-control form-control-sm text-center col-5 mx-auto' disabled/>";
		stringHtmlTrxPar += "</td>";
		stringHtmlTrxPar += "<td class='align-middle' align='center'>";
		stringHtmlTrxPar += "<input type='text' name='montoTotalTrxNoPareadasIC' id='montoTotalTrxNoPareadasIC' size='10' maxlength='10' value='" + montoTotalTrxNoParIC + "' class='campoSalidaNum form-control form-control-sm text-center col-5 mx-auto' disabled/>";
		stringHtmlTrxPar += "</td>";
		stringHtmlTrxPar += "</tr>";
		stringHtmlTrxPar += "</table>";

		stringHtml = stringHtmlTrxPar;

		$("#div-informe-trx-pareadas").html(stringHtml);
		$("#div-informe-trx-pareadas").css("display", "");

		// DETALLE DE LISTA NO AUTORIZADAS
		if (cantidadTrxNoPareadas == 0) {
			// class='tablaContenido'
			stringHtmlDetalle = "<table class='table table-sm small' width='100%'  id='myTableIncomingDetalle'>";
			// stringHtmlDetalle += "<td width='100%' aling='center'> NO SE HAN ENCONTRADO RESULTADOS.</td>";
			stringHtmlDetalle += "<div class='text-center alert alert-info font-weight-bold' role='alert'>NO SE HAN ENCONTRADO RESULTADOS.</div>"
			stringHtmlDetalle += "</table>";
		} else {

			for ( var i = 0; i < cantidadTrxNoPareadas; i++) {
				var sid = data.listaFinalNoAutorizadas[i].sid;
				var codTrx = data.listaFinalNoAutorizadas[i].codigoTransaccion;
				var numerotarjeta = data.listaFinalNoAutorizadas[i].numeroTarjeta;
				var tipoVta = data.listaFinalNoAutorizadas[i].tipoVenta;
				var codigoAut = data.listaFinalNoAutorizadas[i].codAutorizacion;
				var nomComer = data.listaFinalNoAutorizadas[i].nombreComercio;
				var montotransaccion = formato_numero(data.listaFinalNoAutorizadas[i].montoTransac);
				var fechaCompra = data.listaFinalNoAutorizadas[i].fechaCompra;
				var tipoAccion = data.listaFinalNoAutorizadas[i].tipoAccion;
// 				if( numerotarjeta.length == 16 ) {
// 					numerotarjeta = numerotarjeta.substring(0, 4) + "XXXXXXXX" + numerotarjeta.substring(12, 16);
// 				}

				// inicio
				if (i == 0) {
					// class='tablaContenido'
					stringHtmlDetalle = "<table class='table table-sm small' width='100%' id='tbl-log-no-auth'>";
				}

				stringHtmlDetalle += "<tr class='text-center'>";
				stringHtmlDetalle += "<td class='align-middle' width='5%'>" + sid + "</td>";
				stringHtmlDetalle += "<td class='align-middle' width='10%'>" + tipoAccion + "</td>";
				stringHtmlDetalle += "<td class='align-middle' width='15%' class='alineacionTablaCenter'>" + numerotarjeta
						+ "</td>";
				stringHtmlDetalle += "<td class='align-middle' width='10%' class='alineacionTablaRight'>" + codigoAut + "</td>";
				stringHtmlDetalle += "<td class='align-middle' width='10%'>" + nomComer + "</td>";
				stringHtmlDetalle += "<td class='align-middle' width='10%' class='alineacionTablaRight'>" + montotransaccion
						+ "</td>";
				stringHtmlDetalle += "<td class='align-middle' width='10%' class='alineacionTablaCenter'>" + fechaCompra + "</td>";
				stringHtmlDetalle += "</tr>";

				// fin de ciclo
				if ((i + 1) == cantidadTrxNoPareadas) {
					stringHtmlDetalle += "</table>";
				}

			}
			$('#divLogCargaTrxNoAutExport').css("display", "");
		}
		
		$('#divLogCargaTrxNoAutTitulo').css("display", "");
		$('#divLogCargaTrxNoAut').html(stringHtmlDetalle);
		$('#divLogCargaTrxNoAut').css("display", "");
		if (cantidadTrxNoPareadas > 0) {
			pager(cantidadTrxNoPareadas);
		}else{
			$("#nav").hide();
		}
		
		// DETALLE DE LISTA NO AUTORIZADAS IC
		if (cantidadTrxNoPareadasIC == 0) {
			// class='tablaContenido'
			stringHtmlDetalleIC = "<table class='table table-sm small' width='100%' id='myTableIncomingDetalleIC'>";
			// stringHtmlDetalleIC += "<td width='100%' aling='center'> NO SE HAN ENCONTRADO RESULTADOS.</td>";
			stringHtmlDetalleIC += "<div class='text-center alert alert-info font-weight-bold' role='alert'>NO SE HAN ENCONTRADO RESULTADOS.</div>"
			stringHtmlDetalleIC += "</table>";
		} else {

			for ( var i = 0; i < cantidadTrxNoPareadasIC; i++) {
				var sid = data.listaFinalNoAutorizadasIC[i].sid;
				var codTrx = data.listaFinalNoAutorizadasIC[i].codigoTransaccion;
				var numerotarjeta = data.listaFinalNoAutorizadasIC[i].numeroTarjeta;
				var tipoVta = data.listaFinalNoAutorizadasIC[i].tipoVenta;
				var codigoAut = data.listaFinalNoAutorizadasIC[i].codAutorizacion;
				var nomComer = data.listaFinalNoAutorizadasIC[i].nombreComercio;
				var montotransaccion = formato_numero(data.listaFinalNoAutorizadasIC[i].montoTransac);
				var fechaCompra = data.listaFinalNoAutorizadasIC[i].fechaCompra;
				var tipoAccion = data.listaFinalNoAutorizadasIC[i].tipoAccion;
// 				if( numerotarjeta.length == 16 ) {
// 					numerotarjeta = numerotarjeta.substring(0, 4) + "XXXXXXXX" + numerotarjeta.substring(12, 16);
// 				}

				// inicio
				if (i == 0) {
					// class='tablaContenido'
					stringHtmlDetalleIC = "<table class='table table-sm small' width='100%' id='tbl-log-no-auth-ic'>";
				}

				stringHtmlDetalleIC += "<tr class='text-center'>";
				stringHtmlDetalleIC += "<td class='align-middle' width='5%'>" + sid + "</td>";
				stringHtmlDetalleIC += "<td class='align-middle' width='15%' class='alineacionTablaCenter'>" + numerotarjeta
						+ "</td>";
				stringHtmlDetalleIC += "<td class='align-middle' width='10%' class='alineacionTablaRight'>" + codigoAut + "</td>";
				stringHtmlDetalleIC += "<td class='align-middle' width='10%'>" + nomComer + "</td>";
				stringHtmlDetalleIC += "<td class='align-middle' width='10%' class='alineacionTablaRight'>" + montotransaccion
						+ "</td>";
				stringHtmlDetalleIC += "<td class='align-middle' width='10%' class='alineacionTablaCenter'>" + fechaCompra + "</td>";
				stringHtmlDetalleIC += "</tr>";

				// fin de ciclo
				if ((i + 1) == cantidadTrxNoPareadasIC) {
					stringHtmlDetalleIC += "</table>";
				}

			}
			$('#divLogCargaTrxNoAutICExport').css("display", "");
		}		
		$('#divLogCargaTrxNoAutTituloIC').css("display", "");
		$('#divLogCargaTrxNoAutIC').html(stringHtmlDetalleIC);
		$('#divLogCargaTrxNoAutIC').css("display", "");
		if (cantidadTrxNoPareadasIC > 0) {
			pagerIC(cantidadTrxNoPareadasIC);
		}else{
			$("#navIC").hide();
		}

	}

	function pager(rowsTotal) {
		// #table-header
		$('#divLogCargaTrxNoAut').after(
				'<ul class="pagination pagination-sm justify-content-center" id="nav"></ul><div id="espacioPaginacion">');
		var rowsShown = 10;
		var numPages = rowsTotal / rowsShown;
		$('#nav').empty();
		for (i = 0; i < numPages; i++) {
			var pageNum = i + 1;
			$('#nav')
					.append(
							'<li class="page-item"><a href="#sin_mover" style="font-size: 12px;align:center" class="enlaceboton page-link" rel="' + i +'">'
									+ (i + 1) + '</a></li>');
		}
		$('#tbl-log-no-auth tbody tr').hide();
		$('#tbl-log-no-auth tbody tr').slice(0, rowsShown).show();
		//$('#nav a:first').addClass('active');
		$('#nav li:first').addClass('active');
		$('#nav li').bind(
				'click',
				function() {

					// $('#nav a').removeClass('active');
					$('#nav li').removeClass('active');
					//$('#nav a').css('background-color', '');
					$(this).addClass('active');
					//$(this).css('background-color', 'gray');
					var currPage = $(this).children('a').attr('rel');
					var startItem = currPage * rowsShown;
					var endItem = startItem + rowsShown;
					$('#tbl-log-no-auth tbody tr').css('opacity', '0.0').hide()
							.slice(startItem, endItem).css('display',
									'table-row').animate({
								opacity : 1
							}, 300);
				});
	}

	function pagerIC(rowsTotal) {
		//#table-header-ic
		$('#divLogCargaTrxNoAutIC').after(
				'<ul class="pagination pagination-sm justify-content-center" id="navIC"></ul><div id="espacioPaginacionIC">');
		var rowsShown = 10;
		var numPages = rowsTotal / rowsShown;
		$('#navIC').empty();
		for (i = 0; i < numPages; i++) {
			var pageNum = i + 1;
			$('#navIC')
					.append(
							// Como el elemento de id "sin_mover" no existe, al hacer click en el boton de pagina, el navegador no se ira al inicio de la pagina
							'<li class="page-item"><a href="#sin_mover" style="font-size: 12px;align:center" class="enlaceboton page-link" rel="' + i +'">'
									+ (i + 1) + '</a></li> ');
		}
		$('#tbl-log-no-auth-ic tbody tr').hide();
		$('#tbl-log-no-auth-ic tbody tr').slice(0, rowsShown).show();
		//(#navIC a:first)
		$('#navIC li:first').addClass('active');
		// (#navIC a)
		$('#navIC li').bind(
				'click',
				function() {
									
					//$('#navIC a').removeClass('active');
					$('#navIC li').removeClass('active');
					//$('#navIC a').css('background-color', '');
					$(this).addClass('active');
					//$(this).css('background-color', 'gray');
					
					var currPage = $(this).children('a').attr('rel');
					var startItem = currPage * rowsShown;
					var endItem = startItem + rowsShown;
					$('#tbl-log-no-auth-ic tbody tr').css('opacity', '0.0').hide()
							.slice(startItem, endItem).css('display',
									'table-row').animate({
								opacity : 1
							}, 300);
				});
	}
	var today = new Date();

	var dd = today.getDate();
	var mm = today.getMonth() + 1;//January is 0! 
	var yyyy = today.getFullYear();

	var dia = dd;
	var mes = mm;
	var ano = yyyy;

	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}
	var fechaActual = dd + "/" + mm + "/" + yyyy;

	$('#fechaInicio').val(fechaActual);
	
	$("#exportarIC")
			.click(
					function() {
							location.href = "./exportarExcelConciliacionTrxNoAutorizadasIC";
					});
					
		$("#exportarAut")
			.click(
					function() {
							//DataExportar += "TIT|SID|TIPO TRANSACCION|NUMERO TARJETA|CODIGO AUTORIZACION|NOMBRE COMERCIO|MONTO TRANSACCION|FECHA COMPRA~";
				
						location.href = "./exportarExcelConciliacionTrxTransbank";
								
								

					});
					
</script>



</head>

<body>
	<!-- <div class="titulo"> -->
	 <div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold ">
		INFORME DE CONCILIACI&Oacute;N
	</div>
	
	<hr>

	<!--  <br style="line-height: 5px" />  -->
	<div class="col-12 mb-4">
		<!-- class="contenedorInput" -->
   		<table width="100%" class="table table-sm col-8 mx-auto">
			<tr class="text-center">
				<td class="align-middle text-right">
					Fecha:
				</td>
				<td class="align-middle">
					<input type="text" name="fechaInicio" id="fechaProceso"
						size="10" maxlength="10" onKeypress="ingresoFecha();"
						class="campoObligatorio fecha form-control form-control-sm text-center col-8 mx-auto datepicker" placeholder="DD/MM/AAAA" />
				</td>
				<td class="align-middle">
					<input type="button" id="buscar" value="Buscar" class="btn btn-primary">
				</td>


			</tr>
		</table>
	</div>

	<!-- INFORME DE TRX -->
	<div class="mb-4">
		<div id="div-informe-trx-pareadas" style="display: none;"></div>
	</div>
	
	<!-- <br />  -->

	<!-- DETALLE DE TRX NO AUTORIZADAS -->
	<div id="divLogCargaTrxNoAutTitulo" style="display: none;">
		<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
			font-weight-bold">
			DETALLE TRANSACCCIONES NO PAREADAS TRANSBANK
			
<!-- 			<!-- class="contenedorInput" -->
<!-- 			<table width="100%" class="table table-sm small"> -->
<!-- 				<tr class="text-center"> -->
<!-- 					<td class="align-middle">DETALLE TRANSACCCIONES NO PAREADAS TRANSBANK</td> -->
<!-- 				</tr> -->
<!-- 			</table> -->

				
		</div>
		
		<!-- <br /> -->

		<!--   class='tablaContenido' -->
		<table width='100%' class="table table-sm small" id="table-header">
			<tr class="text-center">
				<th class='align-middle' width="5%">SID</th>
				<th class='align-middle' width="10%">TIPO<br>TRANSACCI&Oacute;N
				</th>
				<th class='align-middle' width="15%">N&Uacute;MERO<br>TARJETA
				</th>
<!-- 				<th class='align-middle' width="10%">N&Uacute;MERO<br>TRANSACCI&Oacute;N -->
<!-- 				</th> -->
<!-- 				<th class='align-middle' width="10%">TIPO<br>VENTA -->
<!-- 				</th> -->
				<th class='align-middle' width="10%">C&Oacute;DIGO<br>AUTORIZACI&Oacute;N
				</th>
				<th class='align-middle' width="10%">NOMBRE<br>COMERCIO
				</th>
				<th class='align-middle' width="10%">MONTO<br>TRANSACI&Oacute;N
				</th>
				<th class='align-middle' width="10%">FECHA<br>COMPRA
				</th>
			</tr>
		</table>
	</div>
	<div>
		<div id="divLogCargaTrxNoAut"></div>
	</div>
	
	<!-- <br />  -->
	<div id="divLogCargaTrxNoAutExport" style="display: none;">
		<!--  class="contenedorInput"> -->
		<table width="100%" class="table-borderless">
			<tr class="text-center">
				<td class='align-middle' align="center">
					<input type="button" value="Exportar Tabla" name="exportar" id="exportarAut" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
	</div>
	
	<!-- <br /> -->
	
	<!-- DETALLE DE TRX NO AUTORIZADAS IC-->
	<div id="divLogCargaTrxNoAutTituloIC" style="display: none;">
		
		<div class="mt-4 mb-2 mx-auto col-6 small text-align-center text-center 
			font-weight-bold ">
		
			DETALLE TRANSACCCIONES NO PAREADAS IC
		</div>
<!-- 		<div> -->
<!-- 			<!-- class="contenedorInput" --> 
<!-- 			<table width="100%" class="table table"> -->
<!-- 				<tr> -->
<!-- 					<td align="center">DETALLE TRANSACCCIONES NO PAREADAS IC</td> -->
<!-- 				</tr> -->
<!-- 			</table> -->
<!-- 		</div> -->
		
		<!-- <br /> -->
		
		<!--  class='tablaContenido' -->
		<table width='100%' id="table-header-ic" class="table table-sm small">
			<tr class='text-center'>
				<th class="align-middle" width="5%">SID</th>
<!-- 				<th class="align-middle" width="10%">TIPO<br>TRANSACCI&Oacute;N -->
<!-- 				</th> -->
				<th class="align-middle" width="15%">N&Uacute;MERO<br>TARJETA
				</th>
<!-- 				<th class="align-middle" width="10%">N&Uacute;MERO<br>TRANSACCI&Oacute;N -->
<!-- 				</th> -->
<!-- 				<th class="align-middle" width="10%">TIPO<br>VENTA -->
<!-- 				</th> -->
				<th class="align-middle" width="10%">C&Oacute;DIGO<br>AUTORIZACI&Oacute;N
				</th>
				<th class="align-middle" width="10%">NOMBRE<br>COMERCIO
				</th>
				<th class="align-middle" width="10%">MONTO<br>TRANSACI&Oacute;N
				</th>
				<th class="align-middle" width="10%">FECHA<br>COMPRA
				</th>
			</tr>
		</table>
	</div>
	<div>
		<div id="divLogCargaTrxNoAutIC"></div>
	</div>
	<!-- <br/>  -->
	<div id="divLogCargaTrxNoAutICExport" style="display: none;">
		<!-- class="contenedorInput" -->
		<table width="100%" class="table-borderless">
			<tr class='text-center'>
				<td class='align-middle' align="center">
					<input type="button" value="Exportar Tabla" name="exportar" id="exportarIC" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
	</div>

</body>
</html>