<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Rechazos Diarios</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   
	<script>
	
	</script>
	
</head>

<body>

  <div id="popup" style="display: none;">
		<div id="popUpContenido" class="content-popup">Hola Mundo</div>
  </div>
    
    
  <div class="titulo">
       	REPRESENTACIONES DIARIAS
  </div>
    
  <br style="line-height:5px"/>
  
  <div>
    	<table width="100%" class="contenedorInput">
    		<tr>
		        <td>&nbsp;Fecha Inicio :&nbsp;</td>
		        <td><input type="text" size="10" maxlength="10" value="26-03-2013" class="campoObligatorio"/></td>
		        <td>&nbsp;Fecha Final :&nbsp;</td>
		        <td><input type="text" size="10" maxlength="10" value="26-03-2013" class="campoObligatorio"/>&nbsp;</td>
		        <td colspan="4" align="center">&nbsp; 
		        	<input type="button" value="Buscar"/>&nbsp;&nbsp;&nbsp;
		        </td>
	      	</tr>
   	 </table>
  </div>
    
  <br style="line-height:5px"/>

  <div>
  		<table width="100%" class="tablaContenido">
	    
	    	<tr>
		        <th>SEL</th>
	        	<th>NRO TC</th>
		        <th>FECHA TRANSACC.</th>
		        <th>COMERCIO</th>
		        <th>PAIS</th>
		        <th>MONTO ORIG.</th>
		        <th>MONTO</th>
		        <th>COD. RAZON</th>
		        <th>ESTATUS</th>
		        <th>PATPASS</th>
		        
		       
		        
	      	</tr>
	      	<%
	      		for(int i=0;i<10;i++)
	      		{
	      	%>
      			<tr>
			        <td width="0px">
			        	<input type="radio" name="seleccionTRX" value="C" id="seleccionTRX">
			        </td>
		        	<td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        
		      	</tr>
	      	<%		
	      		}
      		%>
	    </table>
	
	</div>
	    
	<br style="line-height:5px"/>
	
	<div class="subTitulo">
   		Datos De La Cuenta Del Cliente
   	</div>
   	
	<div>
			<table width="100%" class="contenedorInput">
        	<tr>
    			<td>Cliente :</td>
    			<td><input type="text" class="campoSalida"/></td>
    			<td>Fecha Vencto :</td>
    			<td><input type="text" class="campoSalida"/></td>
    		</tr>
    		
    		<tr>
    			<td>Estado De Cuenta :</td>
    			<td><input type="text" class="campoSalida"/></td>
    			<td>Fecha Bloqueo :</td>
    			<td><input type="text" class="campoSalida"/></td>
    		</tr>
    		
    		<tr>
    			<td>Adicional :</td>
    			<td><input type="text" class="campoSalida"/></td>
    			<td colspan="2"></td>
    		</tr>
	    	</table>
	</div>
	
	<br style="line-height:5px"/>
	
	<div class="subTitulo">
   		Datos De La Transaccion Seleccionada
   	</div>
	<div>
	
	     <table width="100%" class="contenedorInput">
    		<tr>
    			<td>Microfilm :</td>
    			<td><input type="text" class="campoSalida"/></td>
    			<td>Cod. Autorizac. :</td>
    			<td><input type="text" class="campoSalida"/></td>
    			<td>Otros Datos :</td>
    			<td><input type="text" class="campoSalida"/></td>
    		</tr>
    		
    		<tr>
    			<td>Fecha Efectiva :</td>
    			<td><input type="text" class="campoSalida"/></td>
    			<td>Fecha Proceso :</td>
    			<td><input type="text" class="campoSalida"/></td>
    			<td>Otros Datos :</td>
    			<td><input type="text" class="campoSalida"/></td>
    		</tr>
    		
    		<tr>
    			<td>Bin Adquiriente :</td>
    			<td><input type="text" class="campoSalida"/></td>
    			<td>Lee Banda</td>
    			<td><input type="text" class="campoSalida"/></td>
    			<td>Otros datos</td>
    			<td><input type="text" class="campoSalida"/></td>
    		</tr>
    	</table>
    </div>
    
    <br style="line-height:5px"/>
    <div>
    	<table  width="100%" class="contenedorInput">
    		<tr>
    		    <td align="center">
    				<input type="button" value="Exportar a Excel" />
    			</td>
    		</tr>
    	</table>
    </div>
 
</body>
</html>