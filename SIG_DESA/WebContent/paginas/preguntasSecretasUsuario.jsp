<%@page import = "Beans.Usuario" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Sistema De Gesti�n de Intercambio</title>
<link rel="stylesheet" type="text/css" href="./css/reset.css" />
<link rel="stylesheet" type="text/css" href="./css/estilo.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/custom.css" />

<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>

<script type="text/javascript">
	function envio(action, id) {
		$.ajax({
			url : action,
			type : 'POST',
			data : $("#" + id).serialize(),
			beforeSend : function() {
				$('#loading').show();
			}
		}).done(function(resp) {
			$("body").empty().append(resp);
		}).error(function(error, status, setting) {
			$("#error").empty().append(error);

		}).always(function(error, status, setting) {
			$('#loading').hide();
		});
	}
</script>

<script type="text/javascript">
	$(document).ready(function(){
	
		$("#alertErrors").toggle(false);
		
		var sidusuario = $("#sidusuario").val();
		$.ajax({
			url : 'listaPreguntasSecretasUsuarioAction',
			type : 'POST',
			dataType : "json",
			data : "sidusuario="+ sidusuario
		})
		.done(
			function(resp){
				var datos = (resp.listaPreguntasUsuario).split("~");
				if (datos[0].split(":")[1] == 0){
					for (var i = 1; i < datos.length - 1; i++) {
						var fila = datos[i].split("|");
						var id = "#idPregunta" + i;
						$(id).val(fila[0]);
						id = "#pregunta" + i;
						$(id).text(fila[2]);
					}
				}else if(datos[0].split(":")[1] == 2){
					$("#contenidoPagina").html("<h1>Su usuario no contiene las preguntas secretas necesarias para poder continuar, favor contacte al administrador de sistema</h1>");
				}
			}
		)
		.error(function(error, status, setting) {
			console.log("Error TipoTransac: " + error);
		});

		$(".irLogin").click(function(){
			envio("logout", "");
			return false;
		});

		$('#formPreguntas').submit(function(){
		
			$("#alertErrors").html("")
			
			var p1 = $("#idPregunta1").val();
			var p2 = $("#idPregunta2").val();
			var p3 = $("#idPregunta3").val();

			if(p1 == p2 || p2 == p3 || p1 == p3){
				//alert("Las preguntas no pueden repetirse.");
				$("#alertErrors").append("<span>Las preguntas no pueden repetirse.</span><br>");
				$("#alertErrors").toggle(true);
				return false;
			}

			if(p1 == 0 || p2 == 0 || p3 == 0){
				//alert("Faltan preguntas en el formulario.");
				$("#alertErrors").append("<span>Faltan preguntas en el formulario.</span><br>");
				$("#alertErrors").toggle(true);
				return false;
			}

			var r1 = $("#respuesta1").val();
			var r2 = $("#respuesta2").val();
			var r3 = $("#respuesta3").val();

			var cantOk = 0;

			$.ajax({
				url : 'checkPreguntasRespuestasAction',
				type : 'POST',
				data : "p1="+ p1 + "&r1=" + r1 + "&p2="+ p2 + "&r2=" + r2 + "&p3="+ p3 + "&r3=" + r3 + "&sidusuario=" + sidusuario
			}).done(function(resp){
				var datos = (resp.checkPreguntasRespuestas).split("~");
				if (datos[0].split(":")[1] == 0){
					if (datos[1] != ""){
						for (var i = 1; i < datos.length - 1; i++){
							var fila = datos[i].split("|");
							if(fila[0] > 0){
								cantOk = cantOk + 1;
							}
						}
					}

					if(cantOk == 3){
						alert("Las respuestas fueron correctas.");
						envio("reestablecerContrasenaIndexAction", "");
					}else{
						//alert("Respuestas incorrectas");
						$("#alertErrors").html("Respuestas Incorrectas.");
						$("#alertErrors").toggle(true);
					}
				}else{
					//alert("Error al chequear preguntas y respuestas secretas.")
					$("#alertErrors").html("Error al chequear preguntas y respuestas secretas.");
					$("#alertErrors").toggle(true);
				}
			})
			.error(function(error, status, setting){
				//alert("Error TipoTransac: " + error);
				$("#alertErrors").html("Error TipoTransac: " + error +"<br>");
				$("#alertErrors").toggle(true);
			});

			return false;
		});
		
	});
</script>

</head>

<body>
	<div id="loading" style="display: none;">
		<img style="position: relative; left: 50%; top: 30%; margin-left: -50px; height: 75px; width: 75px" src="./img/loading.gif">
	</div>

	<header class="navbar navbar-expand-sm navbar-dark py-4" data-toggle="affix">
		<!--
		Imagen de fondo roja reemplazada por color de fondo rojo
		<img src="img/login/logo.png" />
		-->
	    <div class="mx-auto d-sm-flex d-block flex-sm-nowrap">
	        <a class="navbar-brand" href="#">
	            Sistema De Gesti&oacute;n De Intercambio
	        </a>
	    </div>
	</header>
	
	<section class="my-5">
	
		<div class="text-center shadow py-5 mb-5 bg-white rounded col-5 mx-auto">
		
			<% if (usuarioLog != null){ %>
				<div id="contenidoPagina">
					
					<h1 class="h3 mb-3 text-center">Responda las siguientes preguntas</h1>
					
					<form id="formPreguntas" class="mt-3 col-12 mx-auto">
					
					
						<input type="hidden" id="sidusuario" name="sidusuario" value="<%=usuarioLog.getSid()%>" />

						<div class="form-group col-6 mx-auto">
							<label id="pregunta1"></label> 
							<input type="text" class="form-control rounded-pill py-4" name="respuesta1" id="respuesta1" placeholder="Respuesta" required />
							
							<input type="hidden" class="form-control rounded-0 py-4" id="idPregunta1" name="idPregunta1" value="0" />
						</div>
						
						<div class="form-group col-6 mx-auto">
							<label id="pregunta2"></label> 
							<input class="form-control rounded-pill py-4" type="text" name="respuesta2" id="respuesta2" placeholder="Respuesta" required />
							
							<input type="hidden" class="form-control rounded-0 py-4" id="idPregunta2" name="idPregunta2" value="0" />
						</div>
						
						<div class="form-group col-6 mx-auto">
							<label id="pregunta3"></label>
							<input type="text" class="form-control rounded-pill py-4" name="respuesta3" id="respuesta3" placeholder="Respuesta" required />
							
							<input type="hidden" class="form-control rounded-0 py-4" id="idPregunta3" name="idPregunta3" value="0" />
						</div>
						
						<div class="form-group mt-4 col-6 mx-auto">
							<input type="submit" class="form-control btn btn-primary rounded-pill" id="enviar" value="Enviar" />
						</div>
						
					</form>
					
					<div id="alertErrors" class="alert alert-danger font-weight-bold mt-4 col-8 mx-auto">
					</div>
					
				</div>
				<a href="#" class="irLogin">Volver</a>
			<% }else{ %>
				<h1 class="h1 mb-3 mb-auto">No hay usuario</h1><%
			} %>

			<div id="error"></div>
			
		</div>
		
	</section>
</body>
</html>
