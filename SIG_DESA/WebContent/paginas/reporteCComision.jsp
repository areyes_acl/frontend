<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="loginCheck.jsp" flush="true" />
<!-- PATPASS -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<title>Cuadratura Diaria De Transacciones Visa</title>
<script src="js/jquery-1.10.0.min.js" type="text/javascript">
	
</script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>

<script>
	$(".datepicker").datepicker();

	$("#buscar").click(function() {

		$("#exportar").attr("disabled", false);		
	    var date = new Date();
	    var day;
	    var month;
		var year = date.getFullYear();
		
		if($('#fecDesde').val().trim() == ''){
			alert("Debe ingresar la fecha de inicio para comenzar la busqueda");
			return;
		}
		
		if($('#fecHasta').val().trim() == ''){
			alert("Debe ingresar la fecha final para comenzar la busqueda");
			return;
		}
	    
		if(validarFecha($('#fecDesde').val()) == true && validarFecha($('#fecHasta').val())==true){
			var parametros = "fecDesde=" + $('#fecDesde').val();
			parametros += "&fecHasta=" + $('#fecHasta').val();
			//alert('cuadraturaTransaccionesDiariaVisaAction');
			$.ajax({
				url : "cargarRptConciliacionCAction",
				type : "POST",
				data : parametros,
				dataType : "json",
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert('Error ' + textStatus);
					alert(errorThrown);
					alert(XMLHttpRequest.responseText);
				},
				success : function(data) {
					mostrarCuadraturaDiaria(data.listaConciliacionC);
				}
			});
		}else{
			if(date.getDay().toString().length > 1)
		    	day = date.getDay();
		    else
		    	day = "0" + date.getDay();
			
			if(date.getMonth().toString().length > 1)
		    	month = (date.getMonth() + 1);
		    else
		    	month = "0" + (date.getMonth() + 1);

			document.getElementById("fecDesde").value = day + "/" + month + "/" + year;
			document.getElementById("fecHasta").value = day + "/" + month + "/" + year;
			alert('La fecha ingresada es invalida!');
		}

	});

	$("#exportar")
			.click(
					function() {

						DataExportar = "";
						DatasExcel = "";

						idTablaExportar = "tablaOcultar";
						DataExportar += "TIT|TRANSBANK~";
						DataExportar += "TIT|REPORTE DE CONCILIACIONES~";
						DataExportar += exportarExcelConComisiones(idTablaExportar,
								DatasExcel);

						exportarGenerico(DataExportar, "export");

					});
</script>

<script>
	function mostrarCuadraturaDiaria(listaCC) {
		console.log("LOL");
		console.log(listaCC);
		var exprt = false;
		var stringHtml = "";
		var fechas = listaCC.split("-");
		var fechaHasta;
		var fechaDesde;
		
		if (fechas != null) {
			if (fechas.length == 5) {

				fechaDesde = fechas[3];
				fechaHasta = fechas[4];
			}
			if (fechas.length == 3) {
				fechaDesde = fechas[1];
				fechaHasta = fechas[2];
			}
			if (fechas.length == 4) {
				fechaDesde = fechas[2];
				fechaHasta = fechas[3];
			}
		}
		
		var idTablaocultar = document.getElementById('tablaOcultar');
	
		idTablaocultar.style.display = 'none';
		stringHtml = "";
		// class='tablaContenido'
		stringHtml += "<table width='100%' class='table table-sm small' id='tablaOcultar'>";
		stringHtml = stringHtml + "<thead>";
		stringHtml = stringHtml + "<tr><th class='text-center' width='100%' colspan='2' align='center' id='titulo'><label>CONCILIACION DE COMISIONES</label></th></tr>";
		stringHtml = stringHtml + "</thead><tbody>";
		stringHtml = stringHtml + "<tr><td width='100%' colspan='2' align='center' id='detalle'>FECHA DESDE " + fechaDesde+ " - FECHA HASTA " + fechaHasta + "</td></tr>";
		stringHtml = stringHtml + "<tr><td colspan='2' align='center' width='100%' id='titulo'><label>CONCILIACION DE COMISIONES POR CEC</label></td></tr>";

		if(fechas[0].trim() != '' && listaCC!=''){
				//CANTIDAD ON US
			var lstReporteMensual = listaCC.split(";");
			
			if(lstReporteMensual[0].split("|")[0]!="null"  && lstReporteMensual[0].split("|")[0]!="0")
			{
				stringHtml = stringHtml + "<tr>";
				stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'> CANTIDAD DE L&Iacute;NEAS CEC </td>";
				stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'>" + lstReporteMensual[0].split("|")[0] + "</td>";
				stringHtml = stringHtml + "</tr>";
				stringHtml = stringHtml + "<tr>";
				stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'> MONTO TOTAL </td>";
				stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'>" + formato_numero(lstReporteMensual[0].split("|")[1]) + "</td>";
				stringHtml = stringHtml + "</tr>";
				stringHtml = stringHtml + "<tr id='breakrow'><td colspan='2'></td></tr>";
				exprt = true;
			}
			else{
				stringHtml = stringHtml + "<tr><td align='center' colspan='2' witdh='100%' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";
				
			}	
			stringHtml = stringHtml + "<tr><td colspan='2' align='center' id='titulo' id='detalle'><label>CONCILIACION DE COMISIONES POR OUTGOING</label></td></tr>";
			
			if(lstReporteMensual[1]!=null)
				if(lstReporteMensual[1].split("|")[0]!="null" && lstReporteMensual[1].split("|")[0]!="0")
				{
					stringHtml = stringHtml + "<tr>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'> CANTIDAD DE REGISTROS </td>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'>" + lstReporteMensual[1].split("|")[0] + "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml + "<tr>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'> MONTO TOTAL </td>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'>" + formato_numero(lstReporteMensual[1].split("|")[1]) + "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml + "<tr id='breakrow'><td align='center' width='100%' colspan='2' id='titulo'></td></tr>";
					exprt = true;
				}
				else{
					stringHtml = stringHtml + "<tr><td align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";
					
				}		
			else{
				stringHtml = stringHtml + "<tr><td align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";
				
			}	
							stringHtml = stringHtml + "<tr><td colspan='2' align='center' id='titulo' id='detalle'><label>CONCILIACION DE COMISIONES POR OUTGOING PATPASS</label></td></tr>";
			
							console.log(lstReporteMensual);
			if(lstReporteMensual[2]!=null)
				if(lstReporteMensual[2].split("|")[0]!="null" && lstReporteMensual[2].split("|")[0]!="0")
				{
					stringHtml = stringHtml + "<tr>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'> NORMALES </td>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'>" + lstReporteMensual[2].split("|")[0] + "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml + "<tr>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'> PATPASS </td>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'>" + lstReporteMensual[2].split("|")[1] + "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml + "<tr id='breakrow'><td align='center' width='100%' colspan='2' id='titulo'></td></tr>";
					exprt = true;
				}
				else
					stringHtml = stringHtml + "<tr><td align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";	
			else
				stringHtml = stringHtml + "<tr><td align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";
				
				stringHtml = stringHtml + "<tr><td colspan='2' align='center' id='titulo' id='detalle'><label>CONCILIACION DE COMISIONES POR NCL</label></td></tr>";
			
			if(lstReporteMensual[3]!=null)
				if(lstReporteMensual[3].split("|")[0]!="null" && lstReporteMensual[3].split("|")[0]!="0")
				{
					stringHtml = stringHtml + "<tr>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'> CANTIDAD DE REGISTROS </td>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'>" + lstReporteMensual[3].split("|")[0] + "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml + "<tr>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'> MONTO TOTAL </td>";
					stringHtml = stringHtml + "<td align='center' width='50%' id='detalle'>" + formato_numero(lstReporteMensual[3].split("|")[1]) + "</td>";
					stringHtml = stringHtml + "</tr>";
					stringHtml = stringHtml + "<tr><td align='center' width='100%' colspan='2' id='titulo'></td></tr>";
					exprt = true;
				}
				else
					stringHtml = stringHtml + "<tr><td align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";	
			else
				stringHtml = stringHtml + "<tr><td align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";	
				stringHtml = stringHtml + "<tr><td colspan='2'></td></tr>";
				
					
			
		}else{
			stringHtml = stringHtml + "<tr><td align='center' colspan='2' id='detalle'>NO SE ENCONTRARON REGISTROS</td></tr>";
		}
		
		
		stringHtml = stringHtml + "</tbody>";
		$('#divCuadraturaTransaccion').html(stringHtml);
		$("#divCuadraturaTransaccion").css("display", "");
		if(exprt==true)
			$("#exportar").attr("disabled", false);		
	}

$(document).ready(function() {
   $("#fecDesde").val(getYesterday());
    $("#fecHasta").val(getToday());
});


</script>
<style type="text/css">
#detalle{
	/* background-color: #E8E8E8; */
	font: cursive;
}
label {
    font-weight: bolder;
}
#tablaOcultar tr, td, th{
	/* height: 20px; */
	vertical-align: middle;
}
</style>
</head>
<body>

	<!-- <div class="titulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		INFORME CONCILIACI&Oacute;N DE COMISIONES TRANSBANK
	</div>
	<hr>
	<!-- <br style="line-height: 5px" />  -->

	<div id="staticParent">
		<!-- class="contenedorInput"  -->
		<table width="100%" class="table table-sm">
			<tr class="text-center">
				<td class="align-middle text-right">Fecha Desde :</td>
				<td class="align-middle">
					<!-- size="10" -->
					<input type="text" name="fecDesde" id="fecDesde" maxlength="10" onKeypress="ingresoFecha();" class="campoObligatorio fecha form-control form-control-sm col-9 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
				</td>
				<td class="align-middle text-right">
				Fecha Hasta:
				</td>
				<td class="align-middle">
					<!-- size="10" -->
					<input type="text" name="fecHasta" id="fecHasta" maxlength="10" onKeypress="ingresoFecha();" class="campoObligatorio fecha form-control form-control-sm col-9 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
				</td>
				<td class="align-middle">
					<input type="button" id="buscar" value="Buscar" class="btn btn-primary">
				</td>
			</tr>
		</table>
	</div>

	<!-- <br style="line-height: 5px" />  -->

	<div>
		<table width="100%" class="tablaContenido" id="tablaOcultar">
		</table>
		<div id="divCuadraturaTransaccion"></div>
	</div>

	<br style="line-height: 5px" />


	<div>
		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm table-borderless">
			<tr>
				<td align="center"><input type="button"
					value="Exportar Tabla" name="exportar" id="exportar" class="btn btn-primary" disabled />
				</td>
			</tr>
		</table>
	</div>

</body>
</html>