<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Gestion por cuenta</title>
	    <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
		<script src="js/jquery.Rut.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/utils.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
					
				if($("#numPagina").val()==1)
				{	
					$("#anterior").bind("click",function(e){
				        e.preventDefault();
			        });
				}else{
					$("#anterior").unbind("click",false);					
				}
				
				if($("#ultimaPagina").val()<=1)
				{
					$("#botonesPaginacion").hide();
				}
				
				if($("#ultimaPagina").val()==0)
				{
					$("#anterior").hide();
					$("#siguiente").hide();
				}
				
				$("#siguiente").click(function(){
					
					if(Number($("#numPagina").val()) < Number($("#ultimaPagina").val()))
					{
						var numPag = parseInt($("#numPagina").val())+1;
						var datos = 
							"fechaInicio="+$("#fechaInicioPag").val()+
							"&fechaTermino="+$("#fechaTerminoPag").val()+
							"&numPagina="+numPag+
							"&cantReg="+$("#cantReg").val();
						$.ajax({
							url:"gestionPorCuentasPresentacionesAction",
							type:'POST',
							data:datos
						}).done(function(data){
							$("#tablaRes").empty().append(data);
						});
					}
				});
				
				$("#ir").click(function(){
					
					var paginaIr;
					var ultimaPagina;
					
					paginaIr = $("#paginaIr").val();
					
					ultimaPagina = $("#ultimaPagina").val();
					
					
					if( paginaIr == '' || paginaIr == null || typeof paginaIr == "undefined"){
						alert("La página no existe");
						$("#paginaIr").val("");
						return;
					}
					
					if(Number(paginaIr) > Number(ultimaPagina) || Number(paginaIr) < 0 )
					{
						alert("La página no existe");
						$("#paginaIr").val("");
					}else{
						var numPag = paginaIr;
						var datos = 
							"fechaInicio="+$("#fechaInicioPag").val()+
							"&fechaTermino="+$("#fechaTerminoPag").val()+
							"&numPagina="+numPag+
							"&cantReg="+$("#cantReg").val();
							
						$.ajax({
							url:'gestionPorCuentasPresentacionesAction',
							type:'POST',
							data:datos
						}).done(function(data){
							$("#tablaRes").empty().append(data);
						});
					}
						
				});
				
				$("#anterior").click(function(){
					if($("#numPagina").val() > 1)
					{
						var numPag = parseInt($("#numPagina").val())-1;
						var datos = 
							"fechaInicio="+$("#fechaInicioPag").val()+
							"&fechaTermino="+$("#fechaTerminoPag").val()+
							
							"&numPagina="+numPag+
							"&cantReg="+$("#cantReg").val();
						$.ajax({
							url:"gestionPorCuentasPresentacionesAction",
							type:'POST',
							data:datos
						}).done(function(data){
							$("#tablaRes").empty().append(data);
						});
					}
				});
			});
		</script>
	</head>
	<body>
		<div></div>
		
		<input id="ultimaPagina" name="ultimaPagina" type="hidden" value="<s:property value="%{#request.ultimaPagina}" />"/>
		<input type="hidden" value="<s:property value="%{#request.numPagina}" />" name="numPagina" id="numPagina"/>
		
		<input type="hidden" value="<s:property value="%{#request.fechaInicioPag}" />" name="fechaInicioPag" id="fechaInicioPag"/> 
		<input type="hidden" value="<s:property value="%{#request.fechaTerminoPag}" />" name="fechaTerminoPag" id="fechaTerminoPag"/>
		<input type="hidden" value="<s:property value="%{#request.numeroTarjeta}" />" name="numeroTarjetaPag" id="numeroTarjetaPag"/>
		
		<!--  <div style="width:780px;overflow-x:hidden;overflow-y:auto;max-height:105px;"> -->
		<div style="overflow-x:hidden;overflow-y:auto;max-height:200px;"> 
		<!-- <div style="width:764px;margin:0px;padding:0px;"> -->
		<div style="margin:0px;padding:0px;">
		
		<!-- class="tablaContenido" -->
		<table width="100%" class="table table-sm small" id="tablaRes">
		
			<tr class="text-center">
				<th class="align-middle" style="width: 29px;">SEL</th>
				<th class="align-middle" style="width: 104px;">NRO TC</th>
				<th class="align-middle" style="width: 96.5px;">FECHA TRANSACCION</th>
				<th class="align-middle" style="width: 126.5px;">COMERCIO</th>
				<th class="align-middle" style="width: 29px;">PAIS</th>
				<th class="align-middle" style="width: 126.5px;">TIPO TRANSACCION</th>
				<th class="align-middle" style="width: 59px;">MONTO ORIGINAL</th>
				<th class="align-middle" style="width: 59px;">MONTO</th>
				<th class="align-middle" style="width: 59px;">ESTATUS</th>
				<th class="align-middle" style="width: 36.5px;">PATPASS</th>
			</tr>
		
				
					<s:iterator id="data" value="%{#request.listaTransacciones}" status="radioPos">
	   		
		   		<tr class="text-center">
					<td class="align-middle" style="width: 29px;">
						<div class="text-center custom-control custom-radio">
							<input id='el<s:property value="%{#radioPos.index}"/>' type='radio' name='seleccionPRE' value='C' class='seleccionPRE custom-control-input' onclick='mostrarGestionPorCuenta(this);mostrarDetalleTransaccion(this);'>
							<label for='el<s:property value="%{#radioPos.index}"/>' class="custom-control-label"></label>
						</div>
					</td>
					<td class="align-middle" style="width: 104px;"><s:property value="numeroTarjeta"/></td>
					<td class="align-middle" style="width: 96.5px;"><s:property value="fechaTransac"/></td>
					<td class="align-middle" style="width: 126.5px;"><s:property value="comercio"/></td>
					<td class="align-middle" style="width: 29px;"><s:property value="pais"/></td>
					<td class="align-middle" style="width: 126.5px;"><s:property value="glosaTipo"/><s:property value="glosaTipoMoneda"/></td>
					<td class="align-middle" style="width: 59px;"><s:property value="montoTransac"/></td>
					<td class="align-middle" style="width: 59px;"><s:property value="montoFacturacion"/></td>					
					<td class="align-middle" style="width: 59px;"><s:property value="estatus"/></td>
					<s:if test='%{patpass == ("-") or patpass == (" ") or operador == ("VI") or operador == ("VN") }'>
					<td class="align-middle" style="width: 36.5px;text-align: center;">No</td>
					</s:if>

    				<s:else>
    				<td class="align-middle" style="width: 36.5px;text-align: center;"> Si</td>
    				</s:else>
					
					<td class="align-middle" style="display:none"><s:property value="sidIncoming"/></td>
					<td class="align-middle" style="display:none"><s:property value="operador"/></td>
					<td class="align-middle" style="display:none"><s:property value="microfilm"/></td>
					<td class="align-middle" style="display:none"><s:property value="codigoAutorizacion"/></td>
					<td class="align-middle" style="display:none"><s:property value="otrodato1"/></td>
					<td class="align-middle" style="display:none"><s:property value="fechaEfectiva"/></td>
					<td class="align-middle" style="display:none"><s:property value="fechaProceso"/></td>
					<td class="align-middle" style="display:none"><s:property value="otrodato2"/></td>
					<td class="align-middle" style="display:none"><s:property value="binAdquiriente"/></td>
					<td class="align-middle" style="display:none"><s:property value="leeBanda"/></td>
					<td class="align-middle" style="display:none"><s:property value="otrodato3"/></td>
					<td class="align-middle" style="display:none"><s:property value="rubroComercio"/></td>
					<td class="align-middle" style="display:none"><s:property value="otrodato4"/></td>
					<td class="align-middle" style="display:none"><s:property value="codMonedaTrx"/></td>
		   		</tr>	
		   	</s:iterator>
		
	   		
	   	</table>
	   	</div>
		</div>
		
	   
	   <div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial,Helvetica,sans-serif; font-size: 8pt;">
		   	<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		   	<s:property value="%{#request.numPagina}" /> de <s:property value="%{#request.ultimaPagina}" /> p&aacute;gina(s)&nbsp;&nbsp;&nbsp;
		   	Ir a pagina <input type="text" id="paginaIr" class="form-control form-control-sm d-inline py-0 align-middle" style="width:50px;font-size: 8pt;" /> <input type="button" class=" align-middle btn btn-primary py-0" value="Ir" id="ir" style="font-size: 8pt" />
		</div>
		
	</body>
</html>