<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE >

<html>

<head>

<title>Reporte Trimestral</title>

<script>
	
</script>
<style type="text/css">
enlaceboton {
	font-family: verdana, arial, sans-serif;
	font-size: 10pt;
	font-weight: bold;
	padding: 4px;
	background-color: #ffffcc;
	color: #666666;
	text-decoration: none;
}

.enlaceboton:link,.enlaceboton:visited {
	border-top: 1px solid #cccccc;
	border-bottom: 2px solid #666666;
	border-left: 1px solid #cccccc;
	border-right: 2px solid #666666;
}

.enlaceboton:hover {
	border-bottom: 1px solid #cccccc;
	border-top: 2px solid #666666;
	border-right: 1px solid #cccccc;
	border-left: 2px solid #666666;
}

/* .active {
	background: gray;
} */

</style>
</head>

<body>

	<!-- <div class="titulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		REPORTE TRIMESTRAL VISA
	</div>
	
	<hr/>
	
	<!-- <br style="line-height: 5px" />  -->

	<div id="staticParent">
		<!-- class="contenedorInput" -->
		<table style="width: 100%" class="table table-sm mx-auto">
			<tr class="text-right">
				<td class="align-middle">Fecha Proceso:</td>
				<!-- size="4" -->
				<td class="align-middle text-center">
					
					<input type="text" name="ano" id="ano"
						maxlength="4" class="number-only form-control form-control-sm py-3 col-2 mr-1 d-inline text-center datepicker" placeholder="AAAA" /> (Año)
					
					<select
						name="trimestre" id="trimestre" class="custom-select ml-3 mr-1 col-5">
							<option value="1" selected="selected">Primer</option>
							<option value="2">Segundo</option>
							<option value="3">Tercer</option>
							<option value="4">Cuarto</option>
					</select>(Trimestre) 
				</td>
				<td class="align-middle text-center">
					<input type="button" id="buscar" value="Buscar" class="btn btn-primary"> 
				</td>
			</tr>
		</table>
	</div>

	<!-- <br style="line-height: 5px" />  -->

	<div>
		<!-- class="tablaContenido" -->
		<table id="tabla-trimestral" style="width: 100%;" class="table table-sm small" >
			<tbody>
			</tbody>
		</table>
	</div>

	<!-- <br style="line-height: 5px" />  -->


	<div>
		<!-- class="contenedorInput" -->
		<table style="width: 100%" class="table table-sm table-borderless">
			<tr>
				<td align="center"><input type="button" value="Exportar Tabla"
					name="exportar" id="exportar" disabled class="btn btn-primary" /></td>
			</tr>
		</table>
	</div>

</body>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/trimestral/reporteTrimestral.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script>
	$(".datepicker").datepicker({
	    format: "yyyy",
    	minViewMode: 2
	});
</script>
</html>