
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 

<html>

   <head>
       
       <title>Gestiones Por Cuenta</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
       <script type="text/javascript" src="js/utils.js"></script>
       
       	<script>
       	
       	var optC = "";
		var optA = "";
		var sidfunciones=0;
		
        $(document).ready(function(){
        	
        	var mit = "<%=request.getParameter("mit")%>";
        	
        	$("#numerotarjeta").val( "<%=request.getParameter("numeroTarjeta")%>");
        	$("#fechatrx").val( "<%=request.getParameter("fechaTransaccion")%>");
        	$("#microfilm").val( "<%=request.getParameter("microfil")%>");
        	$("#monto").val(formato_numero(colocaComaNumero(numeroSinComaPunto("<%=request.getParameter("montotransac")%>"))));
        	$("#glosa").val("<%=request.getParameter("glosageneral")%>");
        	
        	
        	
        	var codmonedatrx =  "<%=request.getParameter("codmonedatrx")%>";
        	if (codmonedatrx == "CLP"){
        		//alert("CLP");
        		$('#moneda option:eq(0)').prop('selected', true);
        	}else{
        		//alert("USD");
        		$('#moneda option:eq(1)').prop('selected', true);
        	}
        	
        	 var sidTransaccion  = "<%=request.getParameter("sidtransaccion")%>";
        	 var codigofuncion   = "<%=request.getParameter("codigofuncion")%>";
             var codigorazon     = "<%=request.getParameter("codigorazon")%>";
            
             cargarFunciones("<%=request.getParameter("codigofuncion")%>");
             
             //******************************************//
             //* Seccion Cargo y Abono                  *//
             //******************************************//
         	
         	 $.ajax({
 						url:'listaCargoAbonoAction',
 						type:'POST',
 						dataType: "json",
 						}).done(function(resp){
 						var datos = (resp.listaCargoAbono).split("~");
 						$("#serieData").val(datos);
 				
 						for(var i=1;i<datos.length-1;i++)
 						{
 							var fila = datos[i].split("|");
 							if(fila[3]=='C')
 							{
 								optC += "<option value='"+fila[0]+"'>"+fila[2]+"</option>";
 							}else{
 								optA += "<option value='"+fila[0]+"'>"+fila[2]+"</option>";
 							}
 						}
 				
 						if($("#cargo").prop("checked"))
 						{
 							$("#tipoCargoAbono").empty().append(optC);
 						}else{
 							$("#tipoCargoAbono").empty().append(optA);
 						}
 						
 					});
             
         	 //******************************************//
             //* Fin Parte Uno Seccion Cargo y Abono    *//
             //******************************************//
         	
             
             //*************************************************//
             //* Llena al campo referencia para cargo y abono  *//
             //*************************************************//
         	 $("#referencia").val( "<%=request.getParameter("referencia")%>");
         	
         	 
        });
        
        
        //**************************************//
        //* Parte Dos Seccion Cargo y Abono    *//
        //**************************************//
        $("input:radio").click(function(){
    	    
    		if($("#cargo").prop("checked"))
   			{
    			$("#tipoCargoAbono").empty();
    			$("#tipoCargoAbono").append(optC);
   			}else{
   				$("#tipoCargoAbono").empty();
   				$("#tipoCargoAbono").append(optA);
   			}
    	 });
        //*****************//
        //* Fin Parte Dos *//
        //*****************//
     	
		$("#cargoabono").click(function(){
			
			  if($("#cargoabono").is(':checked')) {  
					$("#seccionCargoAbono").css("display","");
			  }else{  
					$("#seccionCargoAbono").css("display","none");
			  } 
		});
		
		
		
		$("#monto").blur(function(){

			var newMonto = sacaPuntos($("#monto").val());
			var newValorMonto = reemplazarComaPunto(newMonto);
			var newMontoFormato = formato_numero(newValorMonto);
			$("#monto").val(newMontoFormato);
			
		});
		
		$("#montoca").blur(function(){

			var newMonto = sacaPuntos($("#montoca").val());
			var newValorMonto = reemplazarComaPunto(newMonto);
			var newMontoFormato = formato_numero(newValorMonto);
			$("#montoca").val(newMontoFormato);
			
		});

		$("#guardar").click(function(){
			
			//***************************************************//
			//* Inicio sección grabacion de cargos y abaonos    *//
			//***************************************************//
			if($("#cargoabono").is(':checked')) {  
				
				//************************************************************//
				//* Grabo cargo y abono al seleccionar incluir cargo / abono *//
				//* Logica Alberto.                                          *//
				//************************************************************//
				var sidCargoAbono =  $("#tipoCargoAbono").val();
				var montoca       = numeroSinComaPunto($("#montoca").val());
				
				var parametros = "sid=" + "<%=request.getParameter("sidtransaccion")%>";
				parametros += "&tipoCargoAbono=" + sidCargoAbono;
				parametros += "&moneda=" + $("#monedaca").val();
				parametros += "&montoca=" + montoca;
				parametros += "&sidusuario=" + "<%=request.getParameter("sidusuario")%>";
				
				//alert(parametros);
				
				$.ajax({
						url: "insertCargoAbonoAction",
						type: "POST",
						data: parametros,
						dataType: "json",
						error: function(XMLHttpRequest, textStatus, errorThrown){
							alert('Error ' + textStatus);
							alert(errorThrown);
							alert(XMLHttpRequest.responseText);
					},
    				success: function(data){
    					var esterror = data.insertCargoAbono.split("|")[0];
    					if (esterror == "esttrx:1"){
    						alert(data.insertCargoAbono.split("|")[1]);
    					}
    				}
				});
			  }
			
			 //***************************************************//
			 //* Fin sección grabacion de cargos y abaonos       *//
			 //***************************************************//
			  
			 //**********************************************************//
			 //* Inicio sección grabacion de Recuperacion ContraCargos  *//
			 //**********************************************************//
				var sidacciontransaccion = $("#funciones").val();
	    		var codigofuncion = $("#funciones option:selected").text().split("-")[0];  
	    	    var codigorazon   = $("#razones option:selected").text().split("-")[0];
	    	    
	    	    if (codigofuncion=="Seleccione opcion"){
	    	    	alert("Debe seleccionar un código de función para la operación...");
	    	    	$("#funciones").focus();
					return;
	    	    }
	    	    
	    	    if (codigorazon=="Seleccione opcion"){
	    	    	alert("Debe seleccionar un código de razón para la operación...");
	    	    	$("#razones").focus();
					return;
	    	    }
	    	    
				
	    	    var codigomoneda  = $("#moneda").val(); 
	    	    var montotransaccion = numeroSinComaPunto($("#monto").val());
	    	    
	    	    
	    	    var glosadescriptiva = $("#glosa").val();
	    	    var montooriginaltrx = numeroSinComaPunto("<%=request.getParameter("montotransac")%>");
	    		
				var parametros = "vmit=1740";
				parametros += "&sidtransaccion=" + "<%=request.getParameter("sidtransaccion")%>";
				parametros += "&codigofuncion=" + codigofuncion;
				parametros += "&codigorazon=" + codigorazon;
				parametros += "&sidacciontransac=" + sidacciontransaccion;
				parametros += "&codigomoneda=" + codigomoneda;
				parametros += "&montotransaccion=" + montotransaccion;
				parametros += "&montooriginaltrx=" + montooriginaltrx;
				parametros += "&glosadescriptiva=" + glosadescriptiva;
				parametros += "&sidusuario=" + "<%=request.getParameter("sidusuario")%>";
				
				//alert(parametros);
				
				$.ajax({
					url: "grabarRecuperacionCobroCargoAction",
					type: "POST",
					data: parametros,
					dataType: "json",
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('Error ' + textStatus);
						alert(errorThrown);
						alert(XMLHttpRequest.responseText);
					},
	    			success: function(data){
	    				mostrarMensajeGrabacion(data.mensajegrabacion);
	    				
	    				recargarConsultaBuscar();
	    				
	    				recargarConsulta();
	    				
	    				
					}
				});
				
				//************************************************//
				//* Fin sección grabacion de cargos y abaonos    *//
				//************************************************//
				$('#popup').css({display: 'none'});
				$('#popUpContenido').empty().css({display:'none'});

		});//Fin Guardar
		
		</script>
		
		<script>
		
				function cargarFunciones(codigofuncion){
					
				 	var codigoTransaccion = "1740";
		    		var parametros = "codigoTransaccion=" + codigoTransaccion;
		    		
					$.ajax({
						url: "buscarFuncionesRecuperacionCobroAction",
						type: "POST",
						data: parametros,
						dataType: "json",
						error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
								
						},
					    success: function(data){
					    	
					    		llenaSelectFunciones(data.listaFunciones, codigofuncion);
					    		
						}
					});
		    	}
		    	
		    	function llenaSelectFunciones(listaFunciones, codigofuncion){
		    		
		    		var itemsHtml = "";
			   		var opcionesHtml = "";
		    		var funciones = listaFunciones.split("~");
		    		var esttrx = funciones[0].split("~");
		    		var codtrx = esttrx[0].split("|");

		    		if ( codtrx[1]  == "0"){
		    			
			    		opcionesHtml = "<OPTION VALUE='" + "0" + "' SELECTED>" + "Seleccione opcion"  + "</OPTION>";
			    		for (var i=1; i<funciones.length-1; i++){
			    		
			    			var lstfunciones = funciones[i].split("|");
			    			var sidtrx = lstfunciones[0];
			    			var valfuncion = lstfunciones[3];
			    			var descripcion = lstfunciones[4];
			    			
			    			if (codigofuncion == valfuncion){
			    				opcionesHtml = opcionesHtml + "<OPTION VALUE='" + sidtrx + "'>" + valfuncion + "-" + descripcion + "</OPTION>";
			    				sidfunciones = sidtrx;
			    				
			    			}else{
			    				opcionesHtml = opcionesHtml + "<OPTION VALUE='" + sidtrx + "'>" + valfuncion + "-" + descripcion + "</OPTION>";	
			    			}
			    			
			    		}
			    		
					    itemsHtml = opcionesHtml;
					    $('#funciones').append(itemsHtml);
					    
					 
		    		}else{
			    		opcionesHTML = "";
			    		opcionesHtml = "<OPTION VALUE='" + "" + "'>" + "PROBLEMAS AL CARGAR FUNCIONES" + "</OPTION>";
			    		itemsHtml = "<SELECT name='funciones' id='funciones' style='width: 140px;'>";
					    itemsHtml = itemsHtml + opcionesHtml;
					    itemsHtml = itemsHtml + "</SELECT>";
					    $('#divlistaFunciones').html(itemsHtml);
		    		}
		    		
		    	}
		    	
		    	
		    	function buscarRazonesFuncion(opcionRazon){
		    		
		    		var sid = $("#funciones option:selected").val();
		    			
		    		var parametros = "sid=" + sid;
			    	if (sid==0){
			    		return;
			    	}
			    	
					$.ajax({
						url: "buscarRazonesFuncionRecuperacionCobroAction",
						type: "POST",
						data: parametros,
						dataType: "json",
						error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
									
						},
						success: function(data){
						    		llenaSelectRazones(data.listaRazones, opcionRazon);
						}
					});
		    	}
		    	
		    	
				
		    	function buscarRazonesFuncionCargaInicial(opcionRazon){
		    		
		    		var parametros = "sid=" + sidfunciones;
			    	
					$.ajax({
						url: "buscarRazonesFuncionRecuperacionCobroAction",
						type: "POST",
						data: parametros,
						dataType: "json",
						error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
									
						},
						success: function(data){
							        alert(data.listaRazones);
						    		llenaSelectRazones(data.listaRazones, opcionRazon);
						}
					});
		    	}

		    	
		    	
				function llenaSelectRazones(listaRazones, codigoRazon){
		    		
		    		var itemsHtml = "";
			   		var opcionesHtml = "";
		    		var razones = listaRazones.split("~");
		    		var esttrx = razones[0].split("~");
		    		var codtrx = esttrx[0].split("|");

		    		if ( codtrx[1]  == "0"){
		    			
			    		opcionesHTML = "";
			    		opcionesHtml = "<OPTION VALUE='" + "0" + "-" + "  " + "'>" + "Seleccione opcion"  + "</OPTION>";
			    		
			    		for (var i=1; i<razones.length-1; i++){
			    		
			    			var lstrazones = razones[i].split("|");
			    			var sidrazon       = lstrazones[0];
			    			var cod_motivo_ini = lstrazones[1];
			    			var descripcion    = lstrazones[3];
			    			
							if (codigoRazon==cod_motivo_ini){
								opcionesHtml = opcionesHtml + "<OPTION VALUE='" + sidrazon + "-" + descripcion + "' SELECTED>" + cod_motivo_ini + "-" + descripcion.substring(0,55) + "</OPTION>";
								$("#causal" ).val(descripcion);
							}else{
								opcionesHtml = opcionesHtml + "<OPTION VALUE='" + sidrazon + "-" + descripcion + "'>" + cod_motivo_ini + "-" + descripcion.substring(0,55) + "</OPTION>";	
							}
			    			
			    		}
			    		
			    		itemsHtml = "<SELECT name='razones' id='razones' style='width: 400px;' onchange='mostrarDescripcionRazon();'>";
					    itemsHtml = itemsHtml + opcionesHtml;
					    itemsHtml = itemsHtml + "</SELECT>";
					    $('#divlistaRazones').html(itemsHtml);
		    			
		    		}else{
			    		opcionesHTML = "";
			    		opcionesHtml = "<OPTION VALUE='" + "" + "'>" + "PROBLEMAS AL CARGAR RAZONES" + "</OPTION>";
			    		itemsHtml = "<SELECT name='razones' id='razones' style='width: 400px;'>";
					    itemsHtml = itemsHtml + opcionesHtml;
					    itemsHtml = itemsHtml + "</SELECT>";
					    $('#divlistaRazones').html(itemsHtml);
		    		}
		    		
		    	}

				function mostrarDescripcionRazon(){
					
					var razon = $("#razones option:selected").val();
					
					var detalle = razon.split("-");
					var sid = detalle[0];
					var descripcion = detalle[1];;
					
					$("#causal" ).val(descripcion);
					
					/********************************************************************/
					/* Busca la fecha tope para el nuevo codigo de funcion seleccionado */
					/********************************************************************/
					buscarFechaTope();
					
				}
				
				function mostrarMensajeGrabacion(mensaje){
					
					var lstmensaje = mensaje.split("~");
					var esttrx = lstmensaje[0].split("~");
					var codtrx = esttrx[0].split("|");
					
					var mensaje = lstmensaje[1].split("|");
					alert(mensaje[1]);
					
				}
				
				//******************************************//
	            //* Obtener fecha tope                     *//
	            //******************************************//
		    	function buscarFechaTope(){
				
					 var codigorazon = $("#razones option:selected").text().split("-")[0];
					 
		             var parametros = "sidtransaccion=" + "<%=request.getParameter("sidtransaccion")%>";
		             parametros += "&vxkey=1740_" + $("#funciones option:selected").text().split("-")[0];
		             parametros += "&codigorazon=" + codigorazon;
		         	 
		             if (codigorazon=="Seleccione opcion"){
			    	    	alert("Debe seleccionar un código de función para la operación...");
			    	    	$("#razones").focus();
							return;
			    	 }
		             
		             //alert("Parametros para fecha tope = " + parametros);
		             
		         	 if ($("#razones option:selected").text()!=""){
		         		 
			         	 	$.ajax({
								url: "obtenerFechaTopeAction",
								type: "POST",
								data: parametros,
								dataType: "json",
								error: function(XMLHttpRequest, textStatus, errorThrown){
									alert('Error ' + textStatus);
									alert(errorThrown);
									alert(XMLHttpRequest.responseText);
								},
				    			success: function(data){
				    				
				    				var listafechatope = data.listafechatope.split("~");
									var esttrx = listafechatope[0].split("~");
									var codtrx = esttrx[0].split("|");
									
									if (codtrx[1]==0){
										
										var datafechatope = listafechatope[1].split("|");
										var fechatope = datafechatope[0];
										var flag      = datafechatope[1];

										$("#fechatope").val(fechatope);
										
									}else{
										
										var mensaje = listafechatope[1].split("|");
										alert(mensaje[1]);
									}
									
								}
						 	});
		         	 }
		    	}
			
		</script>
		
   </head>

<body>

	  <div class="titulo">
	  		RECUPERACIÓN Y COBRO DE CARGOS
	  </div>
	  
      <br style="line-height:5px"/>
      
      <div>
      		<table width="100%" class="contenedorInput">
            	
       			<tr>
               			<td>&nbsp;Numero Tarjeta :</td>
              			<td>
              				<input type="text" id="numerotarjeta" name="numerotarjeta" size="50" maxlength="50" value="" class="campoSalida"/>
              			</td>
       			</tr>
       			
       			<tr>
               			<td>&nbsp;Fecha Transaccion :</td>
              			<td>
              				<input type="text" id="fechatrx" name="fechatrx" size="17" maxlength="17" class="campoSalida" value=""/>
              			</td>
       			</tr>
       			
       			<tr>
               			<td>&nbsp;Microfilm :</td>
              			<td>
              				<input type="text" id="microfilm" name="microfilm" size="17" maxlength="17" class="campoSalida" value=""/>
              			</td>
       			</tr>
       			
       			<tr>
               			<td>&nbsp;Código Función :</td>
              			<td>
              				<SELECT name="funciones" id="funciones" style="width: 400px;" onchange="buscarRazonesFuncion(0);">
					    			
					    	</SELECT>
              			</td>
       			</tr>
       			
               <tr>
               			<td>&nbsp;Código Razón :</td>
              			<td><div id="divlistaRazones">
              				</div>
              			</td>
       			</tr>
       			
       			<tr>
               			<td>&nbsp;</td>
              			<td>
              				<input type="text" id="causal" name="causal" size="77" maxlength="200" value="" class="campoSalida"/>
              			</td>
       			</tr>
       			
       			
       			<tr>
       					<td>&nbsp;Moneda :</td>
              			<td>
              				<select name="moneda" id="moneda" style="width: 130px;">
			    		    	<option value="152">CLP</option>
			    				<option value="840">USD</option>
			    			</select>
              			</td>
              	</tr>
       			
       			<tr>
       					<td>&nbsp;Monto :</td>
              			<td>
              				<input type="text" id="monto" name="monto" size="12" maxlength="12" value="" onKeypress="ingresoMonto();" class="campoObligatorio"/>
              			</td>
              	</tr>
       			
       			<tr>
               			<td>&nbsp;Glosa :</td>
              			<td>
              				<input type="text" id="glosa" name="glosa" size="77" maxlength="100" class="campoObligatorio"/>
              			</td>
       			</tr>
       			
       			<tr>
               			<td>&nbsp;Fecha Tope :</td>
              			<td>
              				<input type="text" id="fechatope" name="fechatope" size="10" maxlength="10" value="" class="campoSalida"/>
              			</td>
       			</tr>
       			
       		    <tr>
       				<td colspan="2">&nbsp;Incluir Cargos y Abonos
 						<input type="checkbox" name="cargoabono" id="cargoabono" value="CA"/> 
 					</td>
       			</tr>
       		
      		</table>
      
      </div>
      
      
      <br style="line-height:5px"/>
      
     <div id="seccionCargoAbono" style="display:none;">
            <input type="hidden" id="serieData" />
    	  	<div class="subTitulo">
	       				Sección Ingreso De Cargos / Abonos 
      		</div>
      
      		<table width="100%" class="contenedorInput">
               
                 <tr>
               			<td>&nbsp;Tipo :</td>
              			<td>
              				<input type="radio" name="group1" id="cargo" value="C" checked>&nbsp;Cargo&nbsp;
							<input type="radio" name="group1" id="abono" value="A">Abono&nbsp;
						<td>
       		    </tr>
       		
       			<tr>
               			<td>&nbsp;Codigo Cargo/Abono :</td>
              			<td>
			    			<select name="tipoCargoAbono" id="tipoCargoAbono"></select>
              			</td>
       			</tr>
       			
				<tr>
               			<td>&nbsp;Referencia :</td>
              			<td>
              				<input type="text" id="referencia" name="referencia" size="50" maxlength="50" class="campoSalida"/>
              			</td>
       			</tr>
       			
       			<tr>
       					<td>&nbsp;Moneda :</td>
              			<td>
              				<select name="monedaca" id="monedaca">
			    		    	<option value="152">CLP</option>
			    				<option value="840">USD</option>
			    			</select>
              			</td>
              	</tr>
       			
       			<tr>
       					<td>&nbsp;Monto :</td>
              			<td>
              				<input type="text" id="montoca" name="montoca" size="12" maxlength="12" value="" onKeypress="ingresoMonto();" class="campoObligatorio"/>
              			</td>
              	</tr>
              	
      </table>
      
      </div>
      
      <br style="line-height:5px"/>
      
      <div>
      		<table  width="100%" class="contenedorInput">
       	       		<tr>
       					<td align="center">
       						<input type="button"  value="Guardar" name="guardar" id="guardar">
       				    	<input type="button"  value="Cancelar" onclick="$('#popup').css({display: 'none'});$('#popUpContenido').empty().css({display:'none'});">
       					</td>
       				</tr>
      		</table>
      </div>
      
</body>
</html>
