<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 0px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    
    /* Position the tooltip */
    position: absolute;
    z-index: 1;
    bottom: 100%;
    left: 50%;
    margin-left: -60px;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}
</style>

<head>
<title>Reporte de conciliaci&oacute;n de Avances</title>
<script src="js/jquery-1.10.0.min.js" type="text/javascript">
	
</script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script type="text/javascript">
	$(".datepicker").datepicker();
	
 <% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");%>
 
	// LOAD INIT PAGE
	$(document).ready(function(){
			cargaTipoEstado();
			 $("#detalleEstado").prop('disabled', 'disabled');
       		
	});
	
	function cargaTipoEstado(){
		$.ajax({
       			url:'tipoEstadoAction',
       			type:'POST'
       		}).done(function(data){
       			data.listaTipoEstado;
       			var datos = (data.listaTipoEstado).split("~");
       			
       			var strCombo = "";
       			
       			for(var i=1;i<datos.length-1;i++)
   				{			
       				var fila = datos[i].split("|");
       				
       				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
   				}
       			
       			$("#estado").append(strCombo);
       		});
	}
	
		// FUNCION DE CONTROL DE CHECK	
		function checkbox(){
			// SI TODOS ESTAN CHEQUEADOS, SELECCIONA EL ALL
			if($('.checkbox1:checked').length == $('.checkbox1').length){
			    $('input[name="select_all"]').prop('checked', true);
			  }
		
				$('#select_all').click(function(event) {
    				 if(this.checked) { // check select status
           				 $('.checkbox1').each(function() { //loop through each checkbox
                				this.checked = true;  //select all checkboxes with class "checkbox1"               
            				});
        			}else{
            			$('.checkbox1').each(function() { //loop through each checkbox
                			this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            			});         
        			}
    			});
    		
    		
    			$('input[name="check[]"]').change(function () {
				   var selectAll = true;
				    // Confirm all checkboxes are checked (or not)
				    $('input[name="check[]"]').each(function (i, obj) {
				        if ($(obj).is(':checked') === false) {
				            // At least one checkbox isn't selected
				            selectAll = false;
				            return false;
				        }
				    });
				    // Update "Select All" checkbox appropriately
				    $('input[name="select_all"]').prop('checked', selectAll);
				});
		}
		
	// gestion masica y exportar
	$("#exportarTrx").click( function(){
	
		var array = $.map($('input[name="check[]"]:checked'), function(c){return c.value; });
		var idUser = <%= usuarioLog.getSid() %>;
		
			
		if(array != null && array.length == 0){
			alert("No se ha seleccionado ninguna transacci\u00F3n para gestionar.");
			return;
		}else{
					
		$.ajax(
					{

						url : "ConciliacionAvancesIndexActionModalGestionMasiva",
						type : 'POST',
						data: { 'idUser':idUser,
								'array':JSON.stringify(array)
             					},

						}).done(function(data) {
								$("#popup").css("height", $(document).height());
								
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'big');
			});
		}
		
	   
				
	});
					
	//LLENAR CAMPO DETALLE DE ESTADO
	$("#estado").change(function() {
		var id = this.id;
		var valor = $("#"+id).val();

		if(valor == 0){
			 $("#detalleEstado").prop('disabled', 'disabled');
			 removeCombo();
			 strCombo = "<option value=0>-Todos-</option>";
			 $("#detalleEstado").append(strCombo);
		}else{
			cargaDetalleEstado(valor);
		}
	});
	
	function cargaDetalleEstado(valor){
		$.ajax({
       			url:'detalleEstadoAction',
       			type:'POST',
       			data: {'sidEstado': valor  },
       		}).done(function(data){
       			data.listaDetalleEstado;
       			var datos = (data.listaDetalleEstado).split("~");
       			
       			var strCombo = "";
       			removeCombo();
       			
       			strCombo += "<option value=0>-Todos-</option>";
       			for(var i=1;i<datos.length-1;i++)
   				{			
       				var fila = datos[i].split("|");       				
       				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
   				}
       			$("#detalleEstado").removeAttr("disabled");
       			$("#detalleEstado").append(strCombo);
       		});
	}
	
	//LIMPIA COMBO DETALLE
	function removeCombo(){
		$("#detalleEstado option").each(function() {
		    $(this).remove();
		});
	}
	
	function obtenerId(sidAvances,sidBice,sidOnus){
	
		$.ajax(
					{

						url : "ConciliacionAvancesIndexActionModal",
						type : 'POST',
						data: {'sidAvances': sidAvances, 
             					'sidBice': sidBice,
             					'sidOnus': sidOnus
             					},

						}).done(function(data) {
								$("#popup").css("height", $(document).height());
								
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'big');
			});
	}
	
		function obtenerIdGestion(sid,estadoGestion){
		var idUser = <%= usuarioLog.getSid() %>;
			
		$.ajax(
					{

						url : "ConciliacionAvancesIndexActionModalGestion",
						type : 'POST',
						data: {'sidConci': sid,
								'idUser':idUser,
								'estadoGestion':estadoGestion
             					},

						}).done(function(data) {
								$("#popup").css("height", $(document).height());
								
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'big');
			});
	}
	


	$("#buscar")
			.click(
					function() {

						if ($('#fechaProceso').val().length == 0) {
							alert("Debe ingresar fecha desde para realizar la b\u00fasqueda...");
							$("#fechaProceso").focus();
							return;
						}
						
						if ($('#fechaFin').val().length == 0) {
							alert("Debe ingresar fecha hasta para realizar la b\u00fasqueda...");
							$("#fechaFin").focus();
							return;
						}

						if (validarFecha($('#fechaProceso').val()) == false) {
							alert("La fecha desde ingresada esta incorrecta, verificar...");
							$("#fechaProceso").focus();
							return;
						}
												
						if (validarFecha($('#fechaFin').val()) == false) {
							alert("La fecha hasta ingresada esta incorrecta, verificar...");
							$("#fechaFin").focus();
							return;
						}
						
						var fechaInicio = convertirFecha($('#fechaProceso').val());
						var fechaTermino = convertirFecha($('#fechaFin').val());
						
						if (fechaInicio > fechaTermino) {
							alert("La fecha de inicio es mayor a la fecha de t\u00E9rmino, verificar...");
							$("#fechaInicio").focus();
							return;
						}
						
						

						var fechaInicioTmp = $('#fechaProceso').val().split("/");;
						var fechaTerminoTmp = $('#fechaFin').val().split("/");
						
						
						var estado	= $('#estado').val();
						var estadoGestion	= $('#gestionEstado').val();
						var estadoDetalle	= $('#detalleEstado').val();
						
						var diaInicio = fechaInicioTmp[0];
	  				    var mesInicio = fechaInicioTmp[1];
	  				    var anoInicio = fechaInicioTmp[2];
	  				    
	  				    var diaTermino = fechaTerminoTmp[0];
	  				    var mesTermino  = fechaTerminoTmp[1];
	  				    var anoTermino = fechaTerminoTmp[2];
	  				    
	  				    var fechaInicio  = anoInicio + mesInicio + diaInicio;
	  				    var fechaTermino = anoTermino + mesTermino + diaTermino;
	  				    	

						$.ajax({
							url : "ConciliacionAvancesAction",
							type : "POST",
							data : {
								fechaInicio : fechaInicio,
								fechaFinal: fechaTermino,
								estado: estado,
								estadoGestion: estadoGestion,
								estadoDetalle: estadoDetalle
							},
							dataType : "json",
							error : function(XMLHttpRequest, textStatus,
									errorThrown) {
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
							},
							success : function(data) {
								mostrarTransacciones(data);
								checkbox();
							}
						});
						
					});

	function mostrarTransacciones(data) {
		
	
		var stringHtmlTrx = "";
		var stringHtml = "";
		var stringHtmlDetalle = "";
		var cantidadFiltro = data.listaFinalAvances.length;
		
		
		for ( var i = 0; i < cantidadFiltro; i++) {
				var cantidadTrxNoPareadas = data.listaFinalAvances[i].cantNoConci;
				var cantidadTrxPareadas = data.listaFinalAvances[i].cantConci
				var montoTotalTrxNoPar = data.listaFinalAvances[i].montoNoConci;
				var montoTotalTrxPar = data.listaFinalAvances[i].montoConci;
			}
			
		if(typeof montoTotalTrxPar === 'undefined'){
			montoTotalTrxPar = 0;
		}
		
		if(typeof montoTotalTrxNoPar === 'undefined'){
			montoTotalTrxNoPar = 0;
		}
		
		if(typeof cantidadTrxNoPareadas === 'undefined'){
			cantidadTrxNoPareadas = 0;
		}
		
		if(typeof cantidadTrxPareadas === 'undefined'){
			cantidadTrxPareadas = 0;
		}

		// TABLA DE TOTALES
		// stringHtmlTrx = "<br style='line-height:5px'/>";
		// class='tablaContenido'
		stringHtmlTrx += "<table width='100%' class='table table-sm small' id='trx-table'>";
		stringHtmlTrx += "<tr class='text-center'>";
		stringHtmlTrx += "<th class='align-middle' width='25%'>Total Trx. Conciliadas</th>";
		stringHtmlTrx += "<th class='align-middle' width='25%'>Total Trx. No Conciliadas</th>";
		stringHtmlTrx += "</tr>";
		stringHtmlTrx += "<tr class='text-center'>";
		stringHtmlTrx += "<td class='align-middle' align='center'>";
		stringHtmlTrx += "<input class='form-control form-control-sm col-4 text-center' type='text' name='totalTrxConciliadas' id='totalTrxPareadas' size='10' maxlength='10' value='" + cantidadTrxPareadas + "' class='campoSalidaNum' disabled/>";
		stringHtmlTrx += "</td>";
		stringHtmlTrx += "<td class='align-middle' align='center'>";
		stringHtmlTrx += "<input class='form-control form-control-sm col-4 text-center' type='text' name='totalTrxNoConciliadas' id='totalTrxNoPareadas' size='10' maxlength='10' value='" + cantidadTrxNoPareadas + "' class='campoSalidaNum' disabled/>";
		stringHtmlTrx += "</td>";
		stringHtmlTrx += "</tr>";
		stringHtmlTrx += "</table>";

		// TABLA DE MONTOS
		// stringHtmlTrx += "<br style='line-height:5px'/>";
		// class='tablaContenido'
		stringHtmlTrx += "<table width='100%' class='table table-sm small' id='trx-table-monto'>";
		stringHtmlTrx += "<tr class='text-center'>";
		stringHtmlTrx += "<th class='align-middle' width='25%'>Monto Total Trx. Conciliadas</th>";
		stringHtmlTrx += "<th class='align-middle' width='25%'>Monto Total Trx. No Conciliadas</th>";
		stringHtmlTrx += "</tr>";
		stringHtmlTrx += "<tr class='text-center'>";
		stringHtmlTrx += "<td class='align-middle' align='center'>";
		stringHtmlTrx += "<input class='form-control form-control-sm col-4 text-center' type='text' name='montoTotalTrxConciliadas' id='montoTotalTrxPareadas' size='10' maxlength='10' value='" + formato_numero(montoTotalTrxPar) + "' class='campoSalidaNum' disabled/>";
		stringHtmlTrx += "</td>";
		stringHtmlTrx += "<td class='align-middle' align='center'>";
		stringHtmlTrx += "<input class='form-control form-control-sm col-4 text-center' type='text' name='montoTotalTrxNoConciliadas' id='montoTotalTrxNoPareadas' size='10' maxlength='10' value='" + formato_numero(montoTotalTrxNoPar) + "' class='campoSalidaNum' disabled/>";
		stringHtmlTrx += "</td>";
		stringHtmlTrx += "</tr>";
		stringHtmlTrx += "</table>";

		stringHtml = stringHtmlTrx;

		$("#div-informe-trx").html(stringHtml);
		$("#div-informe-trx").css("display", "");
		
		// DETALLE
		if (cantidadFiltro == 0) {
			$('#divLogCargaTrxExport').css("display", "none");
			// class='tablaContenido'
			stringHtmlDetalle = "<table width='100%' class='table table-borderless' id='myTableAvancesDetalle'>";
			// stringHtmlDetalle += "<td class='align-middle' width='100%' aling='center'> <center>NO SE HAN ENCONTRADO RESULTADOS.</center></td>";
			stringHtmlDetalle += "<td class='align-middle alert alert-info text-center font-weight-bold' role='alert' width='100%'>NO SE HAN ENCONTRADO RESULTADOS.</td>";
			stringHtmlDetalle += "</table>";
		} else {
		

			for ( var i = 0; i < cantidadFiltro; i++) {
				var sid = data.listaFinalAvances[i].sid;
				var rutCliente = data.listaFinalAvances[i].rutCliente
				var fechaFiltro = data.listaFinalAvances[i].fechaFiltro;
				var montoAvance = data.listaFinalAvances[i].montoAvance;
				var numeroTarjeta = data.listaFinalAvances[i].numeroTarjeta;
				var estadoConc = data.listaFinalAvances[i].estadoConc;
				var detalleEstado = data.listaFinalAvances[i].detalleEstado;
				var sidAvances = data.listaFinalAvances[i].sidAvances;
				var sidBice = data.listaFinalAvances[i].sidBice;
				var sidOnus = data.listaFinalAvances[i].sidOnus;
				var estadoGestion = data.listaFinalAvances[i].estadoGestion;			
				var valorCuota = data.listaFinalAvances[i].valorCuota;
				var numCuota = data.listaFinalAvances[i].numCuota;

				if (i == 0) {
					// class='tablaContenido'
					stringHtmlDetalle = "<table width='800px' class='table table-sm small' id='tbl-log-no-auth'>";
					stringHtmlDetalle += "<thead>";
					stringHtmlDetalle += "<tr class='text-center'>";
					stringHtmlDetalle += "<th class='align-middle' width='1px' align='center'><div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='select_all' name='select_all'/><label for='select_all' class='custom-control-label'></label></div></th>";
					stringHtmlDetalle += "<th class='align-middle' width='10px'>RUT CLIENTE";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "<th class='align-middle' width='10px'>FECHA";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "<th class='align-middle' width='10px'>MONTO AVANCE";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "<th class='align-middle' width='10px'>N&Uacute;MERO TARJETA";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "<th class='align-middle' width='10px'>VALOR CUOTAS";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "<th class='align-middle' width='10px'>N&Uacute;MERO CUOTAS";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "<th class='align-middle' width='10px'>ESTADO";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "<th class='align-middle' width='5px'>VER";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "<th class='align-middle' width='10px'>OPCIONES";
					stringHtmlDetalle += "</th>";
					stringHtmlDetalle += "</tr>";
					stringHtmlDetalle += "</thead>";
				}	
							
				stringHtmlDetalle += "<tr class='text-center'>";
				
				if(estadoGestion == "1" ){
					stringHtmlDetalle += "<td class='align-middle' width='1px' align='center'></td>";
				}else if(estadoGestion == "0"){
					stringHtmlDetalle += "<td class='align-middle' width='1px' align='center'><div class='custom-control custom-checkbox'><input id='el"+i+"' class='checkbox1 custom-control-input' type='checkbox' name='check[]' value='"+sid+"'><label class='custom-control-label' for='el"+i+"'></label></div></td>";
				}else if(estadoConc == "3" || estadoConc == "4" || estadoConc == "5" || estadoConc == "6" ||  estadoConc == "1" || estadoConc == "7"){
					stringHtmlDetalle += "<td class='align-middle' width='1px' align='center'><div class='custom-control custom-checkbox'><input id='el"+i+"' class='checkbox1 custom-control-input' type='checkbox' name='check[]' value='"+sid+"'><label class='custom-control-label' for='el"+i+"'></label></div></td>";
				}else{
					stringHtmlDetalle += "<td class='align-middle' width='1px' align='center'></td>";
				}
				
				// align='right'
				stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'>" + rutCliente + "</td>";
				stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'>" + fechaFiltro
						+ "</td>";
				// align='right'
				stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'>" + formato_numero(montoAvance) + "</td>";
				stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'>" + numeroTarjeta
						+ "</td>";
				// align='right'
				stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'>" + formato_numero(valorCuota)
						+ "</td>";
				// align='right'
				stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'>" + numCuota
						+ "</td>";
				if(estadoConc == "1" || estadoConc == "2"){
					stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'><span title='"+detalleEstado+"'><img src='img/checked.png'  style='width:18px;height:18px;'></span></td>";
					
				}else if(estadoConc == "3" || estadoConc == "4" || estadoConc == "5" || estadoConc == "6" || estadoConc == "7"){
						stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'><span title='"+detalleEstado+"'><img src='img/cancel.png' style='width:18px;height:18px;'></span></td>";
						
				}
				stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'><span title='Detalle Interfaces'><input type='image' style='width:18px;height:18px;' src='img/paper.png' onClick='obtenerId("+sidAvances+","+sidBice+","+sidOnus+")' /></span></td>";
				
				stringHtmlDetalle += "<td class='align-middle' width='10px' align='center'><span title='Gestiones'><input type='image' style='width:18px;height:18px;' src='img/mano.png' onClick='obtenerIdGestion("+sid+","+estadoGestion+")'/></span>";
				
				if(estadoGestion == "1" ){
					stringHtmlDetalle += "<span title='Gestionado'><input type='image' style='width:18px;height:18px; pointer-events: none;' src='img/verde.png' /></span>";
				}else if(estadoGestion == "0"){
					stringHtmlDetalle += "<span title='En Curso'><input type='image' style='width:18px;height:18px; pointer-events: none;' src='img/amarillo.png' /></span> ";
				}else if(estadoConc == "3" || estadoConc == "4" || estadoConc == "5" || estadoConc == "6" || estadoConc == "7"){
					stringHtmlDetalle += "<span title='Pendiente'><input type='image' style='width:18px;height:18px; pointer-events: none;' src='img/rojo.png' /></span>";
				}else{
					stringHtmlDetalle += "<div class='tooltip'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span></span></div> ";
				}
				
				stringHtmlDetalle += "</td>";

				stringHtmlDetalle += "</tr>";

				// fin de ciclo
				if ((i + 1) == cantidadFiltro) {
					stringHtmlDetalle += "</table>";
				}

			}
									
			$('#divLogCargaTrxExport').css("display", "");
		}
		
		$('#divLogCargaTrxTitulo').css("display", "");
		$('#divLogCargaTrx').html(stringHtmlDetalle);
		$('#divLogCargaTrx').css("display", "");
		if (cantidadFiltro > 0) {
			pager(cantidadFiltro);
		}else{
			$("#nav").hide();
		}

	}

	function pager(rowsTotal) {
		$('#table-header-num').html(
				// '<div id="nav"></div><div id="espacioPaginacion">');
				'<ul class="pagination pagination-sm justify-content-center" id="nav"></ul><div id="espacioPaginacion">')
		var rowsShown = 10;
		var numPages = rowsTotal / rowsShown;
		$('#nav').empty();
		for (i = 0; i < numPages; i++) {
			var pageNum = i + 1;
			$('#nav')
					.append(
							/* '<a href="#" style="font-size: 12px;align:center" class="enlaceboton" rel="' + i +'">'
									+ (i + 1) + '</a> '); */
							'<li class="page-item"><a href="#sin_mover" style="font-size: 12px;align:center" class="enlaceboton page-link" rel="' + i +'">'
									+ (i + 1) + '</a></li>');
		}
		$('#tbl-log-no-auth tbody tr').hide();
		$('#tbl-log-no-auth tbody tr').slice(0, rowsShown).show();
		//$('#nav a:first').addClass('active');
		$('#nav li:first').addClass('active');
		
		// $('#nav a').bind(
		$('#nav li').bind(
				'click',
				function() {

					
					// $('#nav a').removeClass('active');
					$('#nav li').removeClass('active');
					// $('#nav a').css('background-color', '');
					$(this).addClass('active');
					//$(this).css('background-color', 'gray');
					// var currPage = $(this).attr('rel');
					var currPage = $(this).children('a').attr('rel');
					var startItem = currPage * rowsShown;
					var endItem = startItem + rowsShown;
					$('#tbl-log-no-auth tbody tr').css('opacity', '0.0').hide()
							.slice(startItem, endItem).css('display',
									'table-row').animate({
								opacity : 1
							}, 300);
				});
	}

	//CARGAR FECHAS INICIO DIA ANTERIOR Y HASTA CON ACTUAL
	var today = new Date();

		var dd = today.getDate();
		var mm = today.getMonth() + 1;//January is 0! 
		var yyyy = today.getFullYear();

		var dia = dd;
		var mes = mm;
		var ano = yyyy;

		if (dd < 10) {
			dd = '0' + dd;
		}

		if (mm < 10) {
			mm = '0' + mm;
		}
		var fechaActual = dd + "/" + mm + "/" + yyyy;

		if (dia == 1) {

			if (mes == 1) {
				mes = 12;
				dia = 31;
			} else if (mes == 2) {
				mes = 1;
				dia = 31;
			} else if (mes == 3) {
				mes = 2;
				dia = 28;
				if ((ano % 4 == 0) && (ano % 100 != 0) || (ano % 400 == 0)) {
					mes = 2;
					dia = 29;
				}
			} else if (mes == 4) {
				mes = 3;
				dia = 31;
			} else if (mes == 5) {
				mes = 4;
				dia = 30;
			} else if (mes == 6) {
				mes = 5;
				dia = 31;
			} else if (mes == 7) {
				mes = 6;
				dia = 30;
			} else if (mes == 8) {
				mes = 7;
				dia = 31;
			} else if (mes == 9) {
				mes = 8;
				dia = 31;
			} else if (mes == 10) {
				mes = 9;
				dia = 30;
			} else if (mes == 11) {
				mes = 10;
				dia = 31;
			} else if (mes == 12) {
				mes = 11;
				dia = 30;
			}
		} else {
			dia = dia - 1; //Dia anterior
		}

		var diaPaso = "";
		var mesPaso = "";
		var anoPaso = "";

		if (dia < 10) {
			diaPaso = "0" + dia;
		} else {
			diaPaso = dia;
		}

		if (mes < 10) {
			mesPaso = "0" + mes;
		} else {
			mesPaso = mes;
		}
		anoPaso = ano;

		var fechaAnterior = diaPaso + "/" + mesPaso + "/" + anoPaso;
		$('#fechaProceso').val(fechaAnterior);
		$('#fechaFin').val(fechaActual);
	/////////////////////////////////////////////
	
	


	
	//EXPORTAR
		
		$("#exportar")
				.click(
						function() {
						
						if ($('#fechaProceso').val().length == 0) {
							alert("Debe ingresar fecha desde para realizar la b\u00fasqueda...");
							$("#fechaProceso").focus();
							return;
						}
						
						if ($('#fechaFin').val().length == 0) {
							alert("Debe ingresar fecha hasta para realizar la b\u00fasqueda...");
							$("#fechaFin").focus();
							return;
						}

						if (validarFecha($('#fechaProceso').val()) == false) {
							alert("La fecha desde ingresada esta incorrecta, verificar...");
							$("#fechaProceso").focus();
							return;
						}
												
						if (validarFecha($('#fechaFin').val()) == false) {
							alert("La fecha hasta ingresada esta incorrecta, verificar...");
							$("#fechaFin").focus();
							return;
						}
						
						var fechaInicio1 = convertirFecha($('#fechaProceso').val());
						var fechaTermino2 = convertirFecha($('#fechaFin').val());
						
						if (fechaInicio1 > fechaTermino2) {
							alert("La fecha de inicio es mayor a la fecha de t\u00E9rmino, verificar...");
							$("#fechaInicio").focus();
							return;
						}
						
						

						var fechaInicioTmp = $('#fechaProceso').val().split("/");;
						var fechaTerminoTmp = $('#fechaFin').val().split("/");
						
						
						var estado	= $('#estado').val();
						var estadoGestion	= $('#gestionEstado').val();
						var estadoDetalle	= $('#detalleEstado').val();
						
						var diaInicio = fechaInicioTmp[0];
	  				    var mesInicio = fechaInicioTmp[1];
	  				    var anoInicio = fechaInicioTmp[2];
	  				    
	  				    var diaTermino = fechaTerminoTmp[0];
	  				    var mesTermino  = fechaTerminoTmp[1];
	  				    var anoTermino = fechaTerminoTmp[2];
	  				    
	  				    var fechaInicio  = anoInicio + mesInicio + diaInicio;
	  				    var fechaTermino = anoTermino + mesTermino + diaTermino;
	  				    
	  				    var parametrosEx = "";
	  				    
	  				    //carga parametros
	  				    parametrosEx += "fechaInicio="+fechaInicio;
	  				    parametrosEx += "&fechaFin="+fechaTermino;
	  				    parametrosEx += "&estado="+estado;
	  				    parametrosEx += "&estadoGestion="+estadoGestion;
	  				    parametrosEx += "&estadoDetalle="+estadoDetalle;
	  				    
	  				    
	  				   location.href = "./exportarDatosExcelDetallesAction?"
									+ parametrosEx;
	  				    
		});
</script>



</head>

<body>


	<!-- <div class="titulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
		CONCILIACI&Oacute;N DE AVANCES CON TRANSFERENCIA
	</div>
	<hr>
	<!-- <br style="line-height: 5px" />  -->
	
	<div>
		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm">
			<tr class="text-center">
				<td class="align-middle text-right">Desde:</td>
				<!-- placeholder="(dd/mm/aaaa) " -->
				<td class="align-middle" colspan="2">
					<!-- size="10"  -->
					<input type="text" name="fechaInicio" id="fechaProceso" maxlength="10" onKeypress="ingresoFecha();"
						class="campoObligatorio fecha form-control form-control-sm col-7 mx-auto text-center d-inline datepicker" placeholder="DD/MM/AAAA" />
				</td>
					
				<td class="align-middle text-right">Hasta:</td>
				<td class="align-middle" colspan="2">
					<!-- size="10"  -->
					<input type="text" name="fechaFin" id="fechaFin" maxlength="10" onKeypress="ingresoFecha();"
						class="campoObligatorio fecha form-control form-control-sm col-7 mx-auto text-center d-inline datepicker" placeholder="DD/MM/AAAA" />
				</td>
				<td class="align-middle" rowspan="2">
					<input type="button" id="buscar" value="Buscar" class="btn btn-primary">
				</td>
			</tr>
			<tr class="text-center">
				<td class="align-middle text-right">Estado :</td>
				<td class="align-middle"> 				
					<select name="estado" id="estado" class="custom-select">
						<option value=0>-Todos-</option>
		        	</select>
				</td>
				<td class="align-middle text-right">Detalle Estado :</td>
				<td class="align-middle"> <select id="detalleEstado" name="detalleEstado" style="width:120px" class="custom-select">
						<option value=0>-Todos-</option>
					</select>
				</td>
				<td class="align-middle text-right">Estado Gesti&oacute;n:</td>
				<td class="align-middle"> <select id="gestionEstado" name="gestionEstado" class="custom-select">
						<option value=99>-Todos-</option>
						<option value=0>En Curso</option>
						<option value=88>Pendiente</option>
						<option value=1>Gestionado</option>
					</select>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="popup" style="display: none;">
		
	</div>
	
	<div id="popUpContenido" class="content-popup" style="display: none;"></div>
	
	

	<!-- INFORME DE TRX -->
	<div>
		<div id="div-informe-trx" style="display: none;"></div>
	</div>
	
	<!-- <br />  -->

	<!-- DETALLE DE TRX NO AUTORIZADAS -->
	<div id="divLogCargaTrxTitulo" style="display: none;">
		<div>
			<!-- class="contenedorInput" -->
			<table  width="100%" class="table table-sm small col-6 mx-auto">
				<tr  class="text-center">
					<td style="border: none !important" class="align-middle font-weight-bold py-0" align="center">
						Detalle Conciliaci&oacute;n De Avances Con Transferencia
					</td>
				</tr>
			</table>
		</div>
		
		<!-- <br />  -->
	
	</div>
		<div>
		<div id="divLogCargaTrx"></div>
	</div>
	<!-- <center><b><div id="table-header-num"></b></div>  -->
	<div id="table-header-num" class="mb-2 font-weight-bold">
	</div>
	
	<!-- <br />  -->
	
	<div id="divLogCargaTrxExport" style="display: none;">
		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm table-borderless">
			<tr class="text-center">
				<td class="align-middle" align="center">
					<input type="button" value="Exportar Detalle" name="exportar" id="exportar" class="btn btn-primary"/>
					<input type="button" value="Gestionar seleccionadas y exportar " name="exportar" id="exportarTrx" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
	</div>
	<br />
</body>
</html>