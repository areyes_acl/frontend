<%@page import = "Beans.Usuario" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Sistema De Gestión de Intercambio</title>
<link rel="stylesheet" type="text/css" href="./css/reset.css" />
<link rel="stylesheet" type="text/css" href="./css/estilo.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/custom.css" />

<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="js/utils.js"></script>

<script type="text/javascript">
	function envio(action, id) {
		$.ajax({
			url : action,
			type : 'POST',
			data : $("#" + id).serialize(),
			beforeSend : function() {
				$('#loading').show();
			}
		}).done(function(resp) {
			$("body").empty().append(resp);
		}).error(function(error, status, setting) {
			$("#error").empty().append(error);

		}).always(function(error, status, setting) {
			$('#loading').hide();
		});
	}
</script>

<script type="text/javascript">
	$(document).ready(function(){
	
		$("#contrasena1Error").toggle(false);
		$("#contrasena2Error").toggle(false);
		$("#alertErrors").toggle(false);
		
		var sidusuario = $("#sidusuario").val();

		$(".irLogin").click(function(){
			envio("logout", "");
			return false;
		});

		$('#formPreguntas').submit(function(){
			var contrasena1 = $("#contrasena1").val();
			var contrasena2 = $("#contrasena2").val();

			if(contrasena1 == "" ){
				// alert("Debe ingresar una clave.");
				$("#contrasena1Error").html("Debe ingresar una contraseña");
				$("#contrasena1Error").toggle(true);
				return false;
			}

			if(contrasena2 == ""){
				// alert("Faltan preguntas en el formulario.");
				$("#contrasena2Error").html("Debe repetir su contraseña");
				$("#contrasena2Error").toggle(true);
				return false;
			}

			if(contrasena1 != contrasena2){
				//alert("Las contraseñas deben coincidir.");
				$("#alertErrors").html("Las contraseñas deben coincidir");
				$("#alertErrors").toggle(true);
				return false;
			}

			var cantOk = 0;

			$.ajax({
				url : 'crearNuevaContrasenaAction',
				type : 'POST',
				data : "passNueva1="+ contrasena1 + "&passNueva2=" + contrasena2 + "&sidusuario=" + sidusuario
			}).done(function(resp){
				if(resp.cambioContrasena == null){
					//alert(resp.error);
					$("#alertErrors").html("Error: " + resp.error);
					$("#alertErrors").toggle(true);
				}else{
					var resultado = resp.cambioContrasena.split(";");
					if(resultado[0] == 0){
						alert(resultado[1]);
						logout();
					}else{
						alert(resultado[1]);
					}
				}
			})
			.error(function(error, status, setting){
				//alert("Error TipoTransac: " + error);
				$("#alertErrors").html("Error TipoTransac: " + error);
				$("#alertErrors").toggle(true);
			});

			return false;
		});
		
	});
</script>

</head>

<body>
	<div id="loading" style="display: none;">
		<img style="position: relative; left: 50%; top: 30%; margin-left: -50px; height: 75px; width: 75px" src="./img/loading.gif">
	</div>

	<header class="navbar navbar-expand-sm navbar-dark py-4">
		<!--
		Imagen de fondo roja reemplazada por color de fondo rojo
		<img src="img/login/logo.png" />
		-->
		<div class="mx-auto d-sm-flex d-block flex-sm-nowrap">
	        <a class="navbar-brand" href="#">
	            Sistema De Gesti&oacute;n De Intercambio
	        </a>
	    </div>
	</header>
	
	<!-- class="login-block" -->
	<section class="my-5">
		
		<div class="text-center shadow py-5 mb-5 bg-white rounded col-4 mx-auto">
		
			<% if (usuarioLog != null){ %>
				<div>
				
					<h1 class="h3 mb-3">Reestablecer Contraseña</h1>

					<form id="formPreguntas" class="mt-3 col-7 mx-auto">
					
						<input type="hidden" id="sidusuario" name="sidusuario" value="<%=usuarioLog.getSid()%>" />
						
						<div class="form-group">
							<label for="contrasena1" class="sr-only">Ingrese Contraseña</label> 
							<input class="form-control rounded-pill py-4" type="password" name="contrasena1" id="contrasena1" placeholder="Ingrese Contraseña" required />
							<span id="contrasena1Error" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="contrasena2" class="sr-only" >Repita Contraseña</label> 
							<input class="form-control rounded-pill py-4" type="password" name="contrasena2" id="contrasena2" placeholder="Repita Contraseña" required />
							<span id="contrasena2Error" class="text-danger"></span>
						</div>
						
						<div class="form-group">
							<input class="form-control rounded-pill btn btn-primary" type="submit" id="guardar" value="Guardar" />
						</div>
						
					</form>
					
					<div id="alertErrors" class="alert alert-danger font-weight-bold mt-4" role="alert">
					</div>
					
				</div>
				
				<a class="mt-4" href="" id="irLogin">Volver</a>
				
			<% }else{ %>
				<h1 class="h1 mb-3">No hay usuario</h1><%
			} %>

			<div id="error"></div>
			
		</div>
		
	</section>
</body>
</html>
