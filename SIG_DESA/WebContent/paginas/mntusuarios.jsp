<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Gestion De Transacciones</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	   <script type="text/javascript" src="js/utils.js"></script>
       
       <script>
       
	   $(document).ready(function(){
		   
				$("#buscarUsuarios").click(function(event){
					
					var valorChecked = $("input[name='estadousuario']:checked").val();
					
					var parametros = "idusuario=" + $('#idusuario').val();
					parametros = parametros + "&nombreusuario=" + $('#nombreusuario').val();
					parametros = parametros + "&apellidosusuario=" + $('#apellidosusuario').val();
					parametros = parametros + "&claveusuario=" + $('#claveusuario').val();
					parametros = parametros + "&estadousuario=" + valorChecked;
					
					//alert(parametros);
					$.ajax({
						url: "buscarUsuarioAction",
    					type: "POST",
    					data: parametros,
    					dataType: "json",
    					error: function(XMLHttpRequest, textStatus, errorThrown){
        						alert('Error ' + textStatus);
        						alert(errorThrown);
        						alert(XMLHttpRequest.responseText);
        						
    				},
    				    success: function(data){
    				    	    buscarUsuarioRespuesta(data);
       					}
					}); 

					$("#idpantallaDatos").css("display","");
				});
				
				
				$("#grabarUsuarios").click(function(event){
					
					if ($('#idusuario').val().length  == 0){
						alert("Debe ingresar identificador de usuario...");
						$("#idusuario").focus();
						return;
					}
					
					if ($('#nombreusuario').val().length  == 0){
						alert("Debe ingresar el nombre del usuario...");
						$("#nombreusuario").focus();
						return;
					}
					
					if ($('#apellidosusuario').val().length  == 0){
						alert("Debe ingresar el apellido del usuario...");
						$("#apellidosusuario").focus();
						return;
					}
					
					if ($('#claveusuario').val().length == 0 && $('#claveusuario').disabled == true ){
						alert("Debe ingresar la clave del usuario...");
						$("#claveusuario").focus();
						return;
					}
					
					if (!confirm('Esta seguro que desea grabar la informacion ingresada?')) {
					    return;
					} 
					
					var parametros = "";
					var valorChecked = $("input[name='estadousuario']:checked").val();
					var valorRol = $("#rolesusuarios").val(); 
					
					parametros = parametros + "&idusuario=" + $('#idusuario').val();
					parametros = parametros + "&nombreusuario=" + $('#nombreusuario').val();
					parametros = parametros + "&apellidosusuario=" + $('#apellidosusuario').val();
					parametros = parametros + "&claveusuario=" + $('#claveusuario').val();
					parametros = parametros + "&estadousuario=" + valorChecked;
					parametros = parametros + "&rolusuario=" + valorRol;
					parametros = parametros + "&sidusuariorol=" + $('#sidusuariorol').val();
			        
					//alert("Parametros = " + parametros);
					
					$.ajax({
						url: "grabarUsuarioAction",
    					type: "POST",
    					data:parametros,
    					dataType: "json",
    					error: function(XMLHttpRequest, textStatus, errorThrown){
        						alert('Error ' + textStatus);
        						alert(errorThrown);
        						alert(XMLHttpRequest.responseText);
        						
    				},
    				    success: function(data){  
    				    	    grabarUsuarioRespuesta(data.resultado);
       					}
					}); 

				});
				
				$("#nuevoUsuario").click(function(event){
									
					$("#idusuario").val("");
					$("#nombreusuario").val("");
					$("#apellidosusuario").val("");
					$("#claveusuario").val("");
					$("#claveusuario").attr("disabled", false);
					$("#estadousuarioactivo").prop("checked", "checked");
					$("#estadousuarioinactivo").prop("checked", "");
					$('#sidusuariorol').val("");
					//$("#nuevoUsuario").attr("disabled", true);
					//$("#cancelaUsuarios").attr("disabled", false);
					//$("#grabarUsuarios").attr("disabled", false);
					$("#idusuario").attr("disabled", false);
					$("#idpantallaDatos").css("display","");
					desabilitoBotones();
					permisoUsuario();
					$("#nuevoUsuario").attr("disabled", true);
					
				});
				
				$("#cancelaUsuarios").click(function(){
					
					$("#idusuario").val("");
					$("#nombreusuario").val("");
					$("#apellidosusuario").val("");
					$("#claveusuario").val("");
					$('#sidusuariorol').val("");
					$("#estadousuarioactivo").prop("checked", "checked");
					$("#estadousuarioinactivo").prop("checked", "");
					//$("#nuevoUsuario").attr("disabled", false);
					//$("#cancelaUsuarios").attr("disabled", true);
					//$("#grabarUsuarios").attr("disabled", true);
					$("#idpantallaDatos").css("display","none");
					desabilitoBotones();
					verificoAdministrador();
					
				});
				
			});
	   
		</script>
		     
		     
		<script>
		
			function  buscarUsuarioRespuesta(data){
				
				$("#nombreusuario").val(data.nombreusuario);
				$("#apellidosusuario").val(data.apellidosusuario);
				$("#claveusuario").val(data.claveusuario);
				
				if (data.estadousuario=="1"){
					 $("#estadousuarioactivo").prop("checked", "checked");
					 $("#estadousuarioinactivo").prop("checked", "");
				}else{
					 $("#estadousuarioactivo").prop("checked", "");
					 $("#estadousuarioinactivo").prop("checked", "checked");
				}
				
			
				//$("input[name='nombre del grupo']:checked").val();
				//alert(data);
			    //alert('data : ' + data);
			    //alert('nombre : ' + data.nombreusuario01);
			    //alert('resultado : ' + data.resultado);
		    					    						    
			    //alert('map       : ' + data.map.nombreusuarioMAP);
			    //alert('map       : ' + data.map.apellidosusuarioMAP);
			    //alert('map       : ' + data.map.claveusuarioMAP);
			    //alert('map       : ' + data.map.estadousuarioMAP);
			    
			    //alert("nombreusuario : " + data.nombreusuario);
				//alert("apellidosusuario : " + data.nombreusuario);
				//alert("claveusuario  : " + data.claveusuario);
				//alert("estadousuario : " + data.estadousuario);
				//alert("listaClientes  Largo: " + data.listaClientes.length);
			 
				
				//alert("listaClientes  : " + data.listaClientes[0].nombreusuarioMAP);
				//alert("listaClientes  : " + data.listaClientes[0].apellidosusuarioMAP);
				//alert("listaClientes  : " + data.listaClientes[0].claveusuarioMAP);
				//alert("listaClientes  : " + data.listaClientes[0].estadousuarioMAP);
				
				//alert("listaClientes  : " + data.listaClientes[1].nombreusuarioMAP);
				//alert("listaClientes  : " + data.listaClientes[1].apellidosusuarioMAP);
				//alert("listaClientes  : " + data.listaClientes[1].claveusuarioMAP);
				//alert("listaClientes  : " + data.listaClientes[1].estadousuarioMAP);
				
				//alert("listaClientes  : " + data.listaClientes[2].nombreusuarioMAP);
				//alert("listaClientes  : " + data.listaClientes[2].apellidosusuarioMAP);
				//alert("listaClientes  : " + data.listaClientes[2].claveusuarioMAP);
				//alert("listaClientes  : " + data.listaClientes[2].estadousuarioMAP);
				
			}
			
			function grabarUsuarioRespuesta(respuesta){
		    		var resultado = respuesta.split("~");
		    		var coderr    = resultado[0].split(":");
		    		var esttrx = resultado[1].split(":");
		    		
		    		if (coderr[1]=='0'){
		    			alert(esttrx[1]);
		    			cargarUsuarios();
		    		}else{
		    			alert(esttrx[1]);
		    			cargarUsuarios();
		    		}
		    		
		    		$("#idusuario").val("");
					$("#nombreusuario").val("");
					$("#apellidosusuario").val("");
					$("#claveusuario").val("");
					$('#sidusuariorol').val("");
					$("#estadousuarioactivo").prop("checked", "checked");
					$("#estadousuarioinactivo").prop("checked", "");
					//$("#nuevoUsuario").attr("disabled", false);
					//$("#cancelaUsuarios").attr("disabled", true);
					//$("#grabarUsuarios").attr("disabled", true);
					$("#idpantallaDatos").css("display","none");
					
					permisoUsuario();
		    		
			}
			
			function cargarRoles(){
		    		
		    		var parametros = "";
					$.ajax({
						url: "cargarRolesAction",
						type: "POST",
						data: parametros,
						dataType: "json",
						error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
								
						},
					    success: function(data){
					    	
					    		llenaSelectRolesUsuario(data.listaRoles);
					    		
						}
					});
		    }
		    	
		    function llenaSelectRolesUsuario(listaRoles){
		    		
		    		var itemsHtml = "";
			   		var opcionesHtml = "";
		    		var roles = listaRoles.split("~");
		    		var esttrx = roles[0].split("~");
		    		var codtrx = esttrx[0].split(":");

		    		if ( codtrx[1]  == "0"){
		    			
			    		opcionesHTML = "";
			    		for (var i=1; i<roles.length-1; i++){
			    		
			    			var rolusuario = roles[i].split(":");
			    			opcionesHtml = opcionesHtml + "<OPTION VALUE='" + rolusuario[0]+ "'>" + rolusuario[1] + "</OPTION>";
			    		}
			    		itemsHtml = "<SELECT NAME='rolesusuarios' id='rolesusuarios'>";
					    itemsHtml = itemsHtml + opcionesHtml;
					    itemsHtml = itemsHtml + "</SELECT>";
					    $('#divlistaRoles').html(itemsHtml);
		    			
		    		}else{
			    		opcionesHTML = "";
			    		opcionesHtml = "<OPTION VALUE='" + ""+ "'>" + "PROBLEMAS AL CARGAR ROLES" + "</OPTION>";
			    		itemsHtml = "<SELECT NAME='rolesusuarios' id='rolesusuarios'>";
					    itemsHtml = itemsHtml + opcionesHtml;
					    itemsHtml = itemsHtml + "</SELECT>";
					    $('#divlistaRoles').html(itemsHtml);
		    		}
		    		
		    	}
		    	
		    	function cargarUsuarios(){
		    		
		    		var parametros = "";
					$.ajax({
						url: "cargarUsuariosAction",
						type: "POST",
						data: parametros,
						dataType: "json",
						error: function(XMLHttpRequest, textStatus, errorThrown){
								alert('Error ' + textStatus);
								alert(errorThrown);
								alert(XMLHttpRequest.responseText);
								
						},
					    success: function(data){
					    	    
					    		mostrarListaUsuarios(data.listaUsuarios);
					    		desabilitoBotones();
					    		verificoAdministrador();
					    		
					    		//alert("pase");
					    		
						}
					});
		    	}
		    	
		    	
		    	function mostrarListaUsuarios(listaUsuarios){
		    		
		   				var stringHtml = "";
	    				var lstusuarios = listaUsuarios.split("~");
	    				var esttrx = lstusuarios[0].split("~");
	    				var codtrx = esttrx[0].split(":");
	    				
	    				if ( codtrx[1]  == "0"){
	    					
	    					stringHtml = "";
	    					stringHtml = "<table width='100%' class='tablaContenido' id='myTable'>";
	    			
		    				for (var i=1; i<lstusuarios.length-1; i++){
		    		
		    					var detalleusuario = lstusuarios[i].split("|");
		    					var estadoUsuario = "";
		    					
		    					if (detalleusuario[5]=='1'){
		    						estadoUsuario = "ACTIVO";
			    				}else{ 
			    					estadoUsuario = "INACTIVO";
			    				}
			    				
		    					stringHtml = stringHtml + "<tr onclick='mostrarUsuario(this);' class='tblUrsuarios'>";
		    					stringHtml = stringHtml + "<td style='display:none;'>" + detalleusuario[0] + "</td>";
		    					stringHtml = stringHtml + "<td width='20%'>" + detalleusuario[1] + "</td>";
		    					stringHtml = stringHtml + "<td width='20%'>" + detalleusuario[2] + "</td>";
		    					stringHtml = stringHtml + "<td width='20%'>" + detalleusuario[3] + "</td>";
		    					stringHtml = stringHtml + "<td style='display:none;'>" + detalleusuario[4] + "</td>";
		    					stringHtml = stringHtml + "<td width='10%'>" + estadoUsuario + "</td>";
		    					stringHtml = stringHtml + "<td width='30%'>" + detalleusuario[9] + "</td>";
		    					stringHtml = stringHtml + "<td style='display:none;'>" + detalleusuario[8] + "</td>";
		    					stringHtml = stringHtml + "<td style='display:none;'>" + detalleusuario[6] + "</td>";
		    					stringHtml = stringHtml + "</tr>";
		    					
		    				}
		    				
		    				stringHtml = stringHtml + "</table>";
				    		$('#divlistaUsuarios').html(stringHtml);
				    		
	    				}else{
	    					alert(lstusuarios[1]);
	    				}
		    	}
		    	
		    	function mostrarUsuario(x){
					$("#idpantallaDatos").css("display","");
		    		var numfila = x.rowIndex;
		    		var fila = document.getElementById("myTable").rows[numfila].cells;
		    		var sidusuario       = fila[0].innerHTML;
		    		var idusuario        = fila[1].innerHTML;
		    		var nombreusuario    = fila[2].innerHTML;
		    		var apellidousuario  = fila[3].innerHTML;
		    		var clave            = fila[4].innerHTML;
		    		var estado           = fila[5].innerHTML;
		    		var rolDescripcion   = fila[6].innerHTML;
		    		var rolxKey          = fila[7].innerHTML;
		    		var sidusuariorol    = fila[8].innerHTML;
		    		
		    		$("#idusuario").val(idusuario);
					$("#nombreusuario").val(nombreusuario);
					$("#apellidosusuario").val(apellidousuario);
					$("#claveusuario").val(clave);
					
					//$("#claveusuario").attr("disabled", true);
					
					if (estado == 'ACTIVO'){
						$("#estadousuarioactivo").prop("checked", "checked");
						$("#estadousuarioinactivo").prop("checked", "");
					}else{
						$("#estadousuarioactivo").prop("checked", "");
						$("#estadousuarioinactivo").prop("checked", "checked");
					}
					
					$('#sidusuariorol').val(sidusuariorol); 
					
					document.getElementById('rolesusuarios').value = rolxKey;
					$("#nuevoUsuario").attr("disabled", true);
					$("#cancelaUsuarios").attr("disabled", false);
					$("#grabarUsuarios").attr("disabled", false);
					
					$("#idusuario").attr("disabled", true);
					$("#idpantallaDatos").css("display","");
					
					permisoUsuario();
		    	}
		    	
		    	function permisoUsuario(){
		    		var xkeyrol = "<%=request.getSession().getAttribute("xkeyrol")%>";
		    		
		    		desabilitoBotones();
		    		
		    		if (xkeyrol == 'USU_CONS'){
		    			$("#nuevoUsuario").attr("disabled", true);
						$("#cancelaUsuarios").attr("disabled", true);
						$("#grabarUsuarios").attr("disabled", true);
		    		}
		    		
		    		if (xkeyrol == 'USU_MOD'){
		    			$("#nuevoUsuario").attr("disabled", true);
						$("#cancelaUsuarios").attr("disabled", false);
						$("#grabarUsuarios").attr("disabled", false);
		    		}
		    		
		    		if (xkeyrol == 'ADM'){
		    			$("#nuevoUsuario").attr("disabled", false);
						$("#cancelaUsuarios").attr("disabled", false);
						$("#grabarUsuarios").attr("disabled", false);
		    		}
		    		
		    		//alert(xkeyrol);
		    	}
		    	
		    	function desabilitoBotones(){
		    		$("#nuevoUsuario").attr("disabled", true);
					$("#cancelaUsuarios").attr("disabled", true);
					$("#grabarUsuarios").attr("disabled", true);
		    	}
		    	
		    	function verificoAdministrador(){
		    		var xkeyrol = "<%=request.getSession().getAttribute("xkeyrol")%>";
		    		if (xkeyrol == 'ADM'){
		    			$("#nuevoUsuario").attr("disabled", false);
		    		}
		    	}

		</script>
	<style type="text/css">
	.tblUrsuarios:hover {
	border-bottom: 1px solid #cccccc;
	border-top: 2px solid #666666;
	border-right: 1px solid #cccccc;
	border-left: 2px solid #666666;
	background: #E8E8E8;
	}
	</style>	
   </head>
   
   <body>
      
      <div class="titulo">
      		ADMINISTRACI&Oacute;N DE USUARIOS
      </div>
      
      <br style="line-height:5px" />
       
      <div>
      		<table width="100%" class="tablaContenido">
       		<tr>
               <th width="20%">ID USUARIO</th>
               <th width="20%">NOMBRE USUARIO</th>
               <th width="20%">APELLIDO USUARIO</th>
               <th width="10%">ESTADO</th>
               <th width="30%">TIPO ROL</th>
       		</tr>
      		</table>
      		
      		
      </div>
      
       
    
      
      <div id="divlistaUsuarios"></div>
      
      <script>cargarUsuarios();</script>
      
      <br style="line-height:5px"/>
      <div>
      		<table width="100%" style="display:none;" id="idpantallaDatos" class="contenedorInput">
      
       				<tr>
               			<td>Identificador De Usuario :</td>
              			<td colspan="2">
               				<input type="text" name="idusuario" id="idusuario" size="10" maxlength="10" class="campoObligatorio"/>&nbsp;
               				<input type="text" style="visibility:hidden" size="10" maxlength="10" name="sidusuariorol" id="sidusuariorol"/>
               			</td>
       				</tr>
       		
       				<tr>
       			    	<td>Nombres : </td>
       					<td colspan="2"><input type="text"  name="nombreusuario" id="nombreusuario" size="60" maxlength="50" class="campoObligatorio"/></td>
       				</tr>
       				
       				<tr>
       			    	<td>Apellidos : </td>
       					<td colspan="2"><input type="text" name="apellidosusuario" id="apellidosusuario" size="60" maxlength="50" class="campoObligatorio"/></td>
       				</tr>
       				
       				<tr>
       			    	<td>Clave : </td>
       					<td colspan="2"><input type="password" size="10" maxlength="10" name="claveusuario" id="claveusuario" class="campoObligatorio"/></td>
       				</tr>
       				
       				<tr>
       			    	<td>Estado usuario : </td>
       					<td colspan="2">
       						<input type="radio" name="estadousuario"  id="estadousuarioactivo" value="1" class="campoObligatorio"> Activo
       						<input type="radio" name="estadousuario"  id="estadousuarioinactivo" value="0" class="campoObligatorio"> Inactivo
						</td>
       				</tr>
       				
       				<tr>
       			    	<td>Rol Usuario : </td>
       					<td>
       					    <div id="divlistaRoles"></div>
       						<script>     					  
       					        cargarRoles();
		        		    </script>
						</td>
       				</tr>
       				
        		</table>
        
        </div>
       
        <br style="line-height:5px"/>
        
        <div>
       		<table  width="100%" class="contenedorInput">
       	       		<tr>
       					<td align="center">
       				    	<input type="button"  id="nuevoUsuario" value="Nuevo" disabled>
       				    	<input type="button"  id="grabarUsuarios" value="Grabar" disabled>
       				    	<input type="button"  id="cancelaUsuarios" value="Cancelar" disabled>
       					</td>
       				</tr>
       		</table>
       	</div>
       
   </body>
</html>