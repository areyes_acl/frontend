<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Gestion Masiva</title>
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	<script type="text/javascript" src="js/utils.js"></script>	
	
 	<script type="text/javascript">
 	  $(document).ready(function(){        	
        	$("#sidusuario").val( "<%=request.getParameter("idUser")%>");	
        	
        	listaTrxSeleccionadas =<%= request.getParameter("array")%>;	        	
        	        					
		});
		
			
	$("#salir").click(function(){
        	
       	$('#popup').css({display: 'none'});
       	$('#popUpContenido').empty().css({display:'none'});
        	
    });
    
     $("#guardarGestionMasiva").click(function(){
    	var obs = $("#obs").val();
    	var gestion = $("#checkgestion").prop("checked") ? 1 : 0; 
    	var listaTrxSeleccionadas =<%= request.getParameter("array")%>;	
    	var sidusuario = $("#sidusuario").val();
    	        	       
       	if(gestion == 1){
       	
       			if(obs== ''){
    				document.getElementById("ejemplo").innerHTML = "<font size='2' color='red'>* Favor ingresar Comentario</font>";
    				return false;
		    	}else{
		    		document.getElementById("ejemplo").innerHTML = "";
		    		valiadGestion(obs,gestion,listaTrxSeleccionadas,sidusuario);
		    	}
       	
      	}else if(gestion == 0){
      	
      	    	if(obs== ''){
    				document.getElementById("ejemplo").innerHTML = "<font size='2' color='red'>* Favor ingresar Comentario</font>";
    				return false;
		    	}else{
		    		document.getElementById("ejemplo").innerHTML = "";
		    		guardarGestion(obs,gestion,listaTrxSeleccionadas,sidusuario);
		    	}
      		
      	}
        	
    });
    
    function valiadGestion(obs,gestion,sid,sidusuario){
          var retVal = confirm("Seguro que desea dejar como gestionada la transacci\u00F3n ?");
    
    	if( retVal == true ){
              guardarGestion(obs,gestion,listaTrxSeleccionadas,sidusuario)
          }
          else{
             return false;
         }
    }
    
    
     function guardarGestion(obs,gestion,listaTrxSeleccionadas,sidusuario){
    		$.ajax({
	       			url:'ejecutaGestionMasivaAction',
	       			type:'POST',
	       			data:{
	       					listaSeleccionada: listaTrxSeleccionadas, 
            				comentario: obs,
            				gestionMasivo: gestion,
            				idUserMasivo: sidusuario
            			},
            		dataType: "json",
					traditional:true,
	       			
	       		}).done(function(data){
	       			alert(data.mensajeError);  
	       			exportarSeleccionadas();
       				reload();
	       		});
	       		$('#popup').css({display: 'none'});
  				$('#popUpContenido').empty().css({display:'none'});	
  					
    }
     
    function exportarSeleccionadas(){
    	var listaTrxSeleccionadas =<%= request.getParameter("array")%>;	
    	    	
    	if ($('#fechaProceso').val().length == 0) {
			alert("Debe ingresar fecha desde para realizar la b\u00fasqueda...");
			$("#fechaProceso").focus();
			return;
		}
	
		if ($('#fechaFin').val().length == 0) {
		alert("Debe ingresar fecha hasta para realizar la b\u00fasqueda...");
		$("#fechaFin").focus();
		return;
		}
	
		if (validarFecha($('#fechaProceso').val()) == false) {
			alert("La fecha desde ingresada esta incorrecta, verificar...");
			$("#fechaProceso").focus();
			return;
		}
								
		if (validarFecha($('#fechaFin').val()) == false) {
			alert("La fecha hasta ingresada esta incorrecta, verificar...");
			$("#fechaFin").focus();
			return;
		}
		
		var fechaInicio1 = convertirFecha($('#fechaProceso').val());
		var fechaTermino2 = convertirFecha($('#fechaFin').val());
		
		if (fechaInicio1 > fechaTermino2) {
			alert("La fecha de inicio es mayor a la fecha de t\u00E9rmino, verificar...");
			$("#fechaInicio").focus();
			return;
		}
		
		var fechaInicioTmp = $('#fechaProceso').val().split("/");;
		var fechaTerminoTmp = $('#fechaFin').val().split("/");
		
		
		var estado	= $('#estado').val();
		var estadoGestion	= $('#gestionEstado').val();
		var estadoDetalle	= $('#detalleEstado').val();
		
		var diaInicio = fechaInicioTmp[0];
	    var mesInicio = fechaInicioTmp[1];
	    var anoInicio = fechaInicioTmp[2];
	    
	    var diaTermino = fechaTerminoTmp[0];
	    var mesTermino  = fechaTerminoTmp[1];
	    var anoTermino = fechaTerminoTmp[2];
	    
	    var fechaInicio  = anoInicio + mesInicio + diaInicio;
	    var fechaTermino = anoTermino + mesTermino + diaTermino;
	    
	    var parametrosEx = "";
	    
	    //carga parametros
	    parametrosEx += "fechaInicio="+fechaInicio;
	    parametrosEx += "&fechaFin="+fechaTermino;
	    parametrosEx += "&estado="+estado;
	    parametrosEx += "&estadoGestion="+estadoGestion;
	    parametrosEx += "&estadoDetalle="+estadoDetalle;
	  	parametrosEx += "&listaTodos="+listaTrxSeleccionadas;	  				    
	  				    
	   location.href = "./exportarDatosExcelDetallesMasivoAction?"
			+ parametrosEx;
    }
    
    
    function reload(){
		var fechaInicioTmp = $('#fechaProceso').val().split("/");;
		var fechaTerminoTmp = $('#fechaFin').val().split("/");
		
		
		var estado	= $('#estado').val();
		var estadoGestion	= $('#gestionEstado').val();
		var estadoDetalle	= $('#detalleEstado').val();
		
		var diaInicio = fechaInicioTmp[0];
		var mesInicio = fechaInicioTmp[1];
		var anoInicio = fechaInicioTmp[2];
		
		var diaTermino = fechaTerminoTmp[0];
		var mesTermino  = fechaTerminoTmp[1];
		var anoTermino = fechaTerminoTmp[2];
		
		var fechaInicio  = anoInicio + mesInicio + diaInicio;
		var fechaTermino = anoTermino + mesTermino + diaTermino;
							
		$.ajax({
			url : "ConciliacionAvancesAction",
			type : "POST",
			data : {
					fechaInicio : fechaInicio,
					fechaFinal: fechaTermino,
					estado: estado,
					estadoGestion: estadoGestion,
					estadoDetalle: estadoDetalle
				},
			dataType : "json",
			error : function(XMLHttpRequest, textStatus,
					errorThrown) {
				alert('Error ' + textStatus);
				alert(errorThrown);
				alert(XMLHttpRequest.responseText);
			},
			success : function(data) {
				mostrarTransacciones(data);
				checkbox();
			}
		});
    }
    
    
    // FUNCION DE CONTROL DE CHECK	
		function checkbox(){
			// SI TODOS ESTAN CHEQUEADOS, SELECCIONA EL ALL
			if($('.checkbox1:checked').length == $('.checkbox1').length){
			    $('input[name="select_all"]').prop('checked', true);
			  }
		
				$('#select_all').click(function(event) {
    				 if(this.checked) { // check select status
           				 $('.checkbox1').each(function() { //loop through each checkbox
                				this.checked = true;  //select all checkboxes with class "checkbox1"               
            				});
        			}else{
            			$('.checkbox1').each(function() { //loop through each checkbox
                			this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            			});         
        			}
    			});
    		
    		
    			$('input[name="check[]"]').change(function () {
				   var selectAll = true;
				    // Confirm all checkboxes are checked (or not)
				    $('input[name="check[]"]').each(function (i, obj) {
				        if ($(obj).is(':checked') === false) {
				            // At least one checkbox isn't selected
				            selectAll = false;
				            return false;
				        }
				    });
				    // Update "Select All" checkbox appropriately
				    $('input[name="select_all"]').prop('checked', selectAll);
				});
		}
  
 	</script>
 	
 	<style type="text/css">
 	#tableid .tr:first-child {
		  background-color: #A7A6A6;
		}
 	</style>
</head>
<body>

		<input type="hidden" name="sidusuario" id="sidusuario" />
		
		<!-- <div class="titulo">  -->
		<div class="col-8 mx-auto my-4 text-align-center text-center font-weight-bold">
			GESTI&Oacute;N DE AVANCES CON TRANSFERENCIA MASIVA
		</div>
		
	<!-- <br style="line-height:5px"/>  -->
	<hr/>
	
	<div id="divgestion">	    	
	  <div> 
	  			<!-- class="contenedorInput" -->
	      		<div class="small mx-auto text-center font-weight-bold">
	       	        Agregar Gesti&oacute;n
	      		</div>
     	 </div>
    
    <!-- <br style="line-height:5px"/>  -->
    
    <p id="ejemplo"></p>
	
	 <div>
	 			<!-- class="contenedorInput" -->
	      		<table  width="100%" class="table table-sm small mx-auto">
	       	       		<tr>
	       					<td align="left">
	       						<div class="custom-control custom-checkbox">
	       							<input type="checkbox" name="checkgestion" id="checkgestion" class="custom-control-input">
	    							<label class="custom-control-label" for="checkgestion">T&eacute;rminar Gesti&oacute;n</label>
       					 		</div>
	       					</td>
	       				
	       				</tr>
	       				<tr>
	       					<td align="left">
	       					 	<textarea rows="4" cols="110" id="obs" class="form-control form-control-sm"></textarea>
	       					</td>
	       				</tr>
	      		</table>
   	 </div>
</div>
	
	<!-- <br style="line-height:5px"/>
	<br style="line-height:5px"/>
	<br style="line-height:5px"/>
	<br style="line-height:5px"/>
	<br style="line-height:5px"/>
	<br style="line-height:5px"/>  -->
	      
	      <div>
	      		<!-- class="contenedorInput"  -->
	      		<table  width="100%" class="table table-sm table-borderless">
	       	       		<tr>
	       					<td align="center">
	       						<input id="guardarGestionMasiva" type="button"  value="Guardar" class="btn btn-primary"/>
	       				    	<input type="button"  value="Cancelar" id="salir" name="salirpopup" class="btn btn-primary">
	       					</td>
	       				</tr>
	      		</table>
     	 </div>
</body>
</html>