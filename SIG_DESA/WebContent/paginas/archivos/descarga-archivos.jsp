<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import = "Beans.Usuario" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>

<html>
<head>
<title>Gestiones Trx Pendientes</title>
</head>

<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>

<body>
	<form id="form1">
		<!-- <div class="titulo">  -->
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
			B&Uacute;SQUEDA DE ARCHIVOS
		</div>
		<hr>
		<!-- <br style="line-height: 5px" />  -->

		<div>
			<!-- class="contenedorInput" -->
			<table class="table table-sm" style="width: 100%;">
				<tr class="text-center">
					<td class="align-middle text-right">Fecha Archivo:</td>
					<!-- class="size="10" -->
					<td class="align-middle">
						<input type="text" name="fechaDesde" id="fechaDesde"
							maxlength="10" class="campoObligatorio fecha form-control form-control-sm d-inline mx-auto text-center col-9 datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td class="align-middle text-right">TIPO ARCHIVO:</td>
					<td class="align-middle"><select id="tipo-archivo" class="custom-select">
							<option value="">SELECCIONE...</option>
							<option value="OUTGOING">OUTGOING_TBK</option>
							<option value="OUTGOING_VN">OUTGOING_VN</option>
							<option value="OUTGOING_VI">OUTGOING_VI</option>
							<option value="RECHAZOS">RECHAZOS</option>
							<option value="ONUS">ONUS</option>
							<option value="LOG_AUTORIZACION">LOG_AUTORIZACION</option>
							<option value="CARGO_ABONO">CARGO_ABONO</option>
							<option value="CONTABILIDAD">CONTABILIDAD</option>
							<option value="INCOMING">INCOMING_TBK</option>
							<option value="INCOMING_VN">INCOMING_VN</option>
							<option value="INCOMING_VI">INCOMING_VI</option>
							<option value="COMISIONES">COMISIONES</option>
							<option value="NCL">NCL</option>
							<option value="BOL">BOL</option>
					</select></td>
					<td class="align-middle"><input type="button" id="buscarArchivos" name="buscar" value="Buscar" class="btn btn-primary"></td>	
				</tr>
			</table>
		</div>


		<!-- <br style="line-height: 5px" />  -->

		<!-- <div class="subTitulo">  -->
		<div id="tituloTablaResultado" class="col-6 small mx-auto my-4 text-align-center text-center font-weight-bold" style="display: none;">
			Listado de Archivos
		</div>

		<div class="cajaContenido">
			<div id="tablaRes"></div>

		</div>

		<!-- <br style="line-height: 5px" />  -->

	</form>
</body>

<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/descargarArchivo/descargarArchivo.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>

<script>
	$(".datepicker").datepicker();
</script>
</html>