<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>

	<div style="width: 800px; overflow-x: auto; overflow-y: auto; max-height: 500px;">
		<div style="margin: 0px; padding: 0px;">
			<!-- class="tablaContenido" -->
			<table style="width: 100%" class="table table-sm small" id="tabla">

				<tr class="text-center">
					<th class="align-middle">NOMBRE ARCHIVO</th>
				</tr>

				<s:iterator value="listaArchivos">
					<tr>
						<td class="align-middle" ><a href="descargarArchivos?fileName=<s:property value="name"/>"><s:property value="name"/></a> <label class="oprdr"></label></td>
					</tr>
				</s:iterator>
			</table>
		</div>
	</div>

</body>
</html>