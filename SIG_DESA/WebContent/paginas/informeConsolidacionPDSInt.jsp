<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Informe de Compensaci&oacute;n Internacional</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   
	   <script>
	   
		
		var today = new Date();
		
		var dd = today.getDate(); 
		var mm = today.getMonth()+1;//January is 0! 
		var yyyy = today.getFullYear();
		
		var dia = dd; 
		var mes = mm; 
		var ano = yyyy;
		
		if(dd<10){
			dd='0'+dd;
		}
		
		if(mm<10){
			mm='0'+mm;
		}
		var fechaActual = dd + "/" + mm +"/" + yyyy;
		
		
		$('#fechaInicio').val(fechaActual);
		$('#fechaFin').val(fechaActual);
		
	        var DataExportar='';
	        
		
			
			$("#fechaInicio").focus(function(){
				$("#divInformePDS").css("display","none");
				var tablaOriginal = document.getElementById("tablaPDSInforme");
				tablaOriginal.style.display= "";
				
				$("#exportar").attr("disabled", true);
				
			});
			
			$("#exportar").click(function(){
				
				var parametros = "fechaInicio=" + $('#fechaInicio').val();
				
				if ($('#fechaFin').val().length  == 0){
					f = new Date();
					parametros = parametros + "&fechaFin=" + f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
					fechaTermino = convertirFecha(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
				}else{
				
					if (validarFecha($('#fechaFin').val()) == false){
							alert("La fecha ingresada esta incorrecta, verifiquela...");
							$("#fechaFin").focus();
							return;
					}
					parametros = parametros + "&fechaFin=" + $('#fechaFin').val();
					fechaTermino = convertirFecha($('#fechaFin').val());
				}
				parametros += "&nombreArchivo=" + "export";
								
				location.href="./exportarDatosExcelPDSIntAction?" + parametros;
				
			});
			
		</script>
		
		
	   
   </head>
   
   <body>
       
      <div class="titulo">
       		INFORME DE COMPENSACION INTERNACIONAL
  	  </div>
      
      <br style="line-height:5px"/>
      
      <div>
      		<table width="100%" class="contenedorInput">
               <tr>
           			<td>&nbsp;Fecha Proceso desde:</td>
           			<td>
           				<input type="text" name="fechaInicio" id="fechaInicio" size="10" maxlength="10" onKeypress="ingresoFecha();" class="campoObligatorio fecha"/>&nbsp;(DD/MM/AAAA)
              			
             		</td>
             		<td>&nbsp;Fecha Proceso hasta:</td>
           			<td>
           				<input type="text" name="fechaFin" id="fechaFin" size="10" maxlength="10" onKeypress="ingresoFecha();" class="campoObligatorio fecha"/>&nbsp;(DD/MM/AAAA)
              			<input type="button"  id="exportar" value="Exportar">
             		</td>
       			</tr>
      		</table>
      </div>
     
      <br style="line-height:5px"/>
      
     
      
      <br style="line-height:5px"/>
      
      
    

   </body>
</html>