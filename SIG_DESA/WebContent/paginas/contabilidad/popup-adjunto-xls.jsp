
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<title>Gestiones Por Cuenta</title>

<script type="text/javascript" src="js/utils.js"></script>
<script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/custom/custom.js"></script>
<script type="text/javascript">

	function cerrarPopUp(){
		$('#popup').css({
			display : 'none'
		});
		$('#popUpContenido').empty().css({
			display : 'none'
		});
	}

	// CANCELAR 
	$("#cancelar").click(function() {
		cerrarPopUp();
	});

	// Actualizar pago	
	function actualizarPago() {
		// ARCHIVOS
		var file_data_excel = $("#file-to-upload-excel").prop("files")[0];
		var sidPago = $("#sid").val();
		
		

		// CREA EL ARCHIVO FORM DATA
		var form_data = new FormData();

		if (file_data_excel) {
			if (!validarExtensionArchivoExcel(file_data_excel.name)) {
				alert("Tipo de archivo adjunto 'Detalle Mail XLS' debe ser un archivo Excel");
				return false;
			}
		} else {
			file_data_excel = "";
			alert("Debe seleccionar un archivo del formato permitido, para adjuntar al pago.");
			return false;
		}

		form_data.append("adjuntoExcel", file_data_excel);
		form_data.append("sidPago",sidPago);
		
		console.log("adjuntoExcel :  " + file_data_excel);

		if (file_data_excel != "") {
			form_data.append("filenameExcel", file_data_excel.name);
		} else {
			form_data.append("filenameExcel", "");
		}

		$.ajax({
			url : 'actualizarPagoConAdjuntoXLS',
			type : 'POST',
			data : form_data,
			processData : false,
			contentType : false,
			enctype : 'multipart/form-data',
			success : function(datos, textStatus, xhr) {
				if (datos.codigoError == 0) {
					alert(" Se ha agregado el archivo xls al pago correctamente ");
					$("#buscar").click();
					cerrarPopUp();
										
				} else {
					$("#buscar").click();
					alert(datos.mensajeError);
					cerrarPopUp();
				}

			},
		});
	}
</script>
</head>

<body>
	<form id="formulario-">
		<input type="hidden" name="sid" id="sid"
			value="<%=request.getParameter("sid")%>" />

		<!-- <div class="titulo">  -->
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">Adjuntar Detalle XLS</div>

		<!-- <br style="line-height: 5px" /> -->
		<hr>
		
		<div class="text-center mb-3">
			<label>
				Se adjuntara archivo excel al pago registrado en la fecha:
			</label>
			<label>
				<%=request.getParameter("fecha")%>
			</label>
		</div>
		
		<!-- <br style="line-height: 5px" />  -->

		<div class="mb-4">
			<!-- class="contenedorInput" -->
			<table width="100%" class="table table-sm small">
				<tr>
					<td class="align-middle">Detalle XLS:</td>
					<td class="align-middle w-75">
						<!-- <input type="file" id="file-to-upload-excel">  -->
						<div class="custom-file">
							<input class="custom-file-input" type="file"  id="file-to-upload-excel" onchange="cambiarTextoInputFile('file-to-upload-excel', 'file-to-upload-excel-label')" >
							<label class="custom-file-label" for="file-to-upload-excel" id="file-to-upload-excel-label">Examinar...</label>
						</div>
					</td>
				</tr>
			</table>
		</div>

		<!-- <br style="line-height: 5px" />  -->
		
		<div>
			<!-- class="contenedorInput" -->
			<table width="100%" class="table table-sm table-borderless">
				<tr>
					<td align="center"><input type="button" id="guardar"
						name="guardar" value="Guardar" onclick="actualizarPago();" class="btn btn-primary" /> <input
						type="button" id="cancelar" name="cancelar" value="Cancelar" class="btn btn-primary" ></td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>
