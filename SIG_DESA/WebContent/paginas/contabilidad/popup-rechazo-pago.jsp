<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
   <head>
       <title>Rechazo de pago</title>
       <script>
       	$(document).ready(function(){

       	});

       	// RECHAZAR PAGO
       	$("#rechazar-pago").click(function(){
       		var fechaDesde = <%=request.getParameter("fechaDesde")%>;
       		var fechaHasta = <%=request.getParameter("fechaHasta")%>;
       		var operador = <%=request.getParameter("operador")%>;

			var textoConfirmacion = "\u00BF Est\u00e1 seguro que desea rechazar el pago?. \n\n"+ 
       										"**IMPORTANTE: Si cancela el pago, no puede volver a realizar ninguna accion sobre este.";

			if(confirm(textoConfirmacion)){		
				
				var parametros = {
					fechaPago : $("#fecha-pago").val(), motivoRechazo : $("#motivo-rechazo").val(), operador: operador
				};

				$.ajax({
					url:'rechazarPagoContable',
					type:'POST',
					data: parametros
				}).done(function(data){
					if(data.codigoError == 0){
						alert("Se ha rechazo el pago correctamente.");
						buscaPagos(fechaDesde, fechaHasta, operador);
					}else{
						alert("Ha ocurrido un error y no se ha podido rechazar el pago");
					}

					$('#popup').css({display: 'none'});
					$('#popUpContenido').empty().css({display:'none'});
				});
			}
		});

       	// CERRAR POP UP 
       	$("#cancelar").click(function(){
			$('#popup').css({display: 'none'});
			$('#popUpContenido').empty().css({display:'none'});
        });

       </script>
   </head>
	
	<body>
		<form id="formObjecion">
						
			<!-- <div class="titulo">  -->
			<div class="col-12 mx-auto my-2 text-align-center text-center font-weight-bold bg-white">
				Rechazar Pago 
			</div>
			<hr/>
			
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table style="width:100%;" class="table table-sm small mx-auto">
			    	<tr>
			        	<td class="align-middle">&nbsp;Fecha Pago:</td>
		        		<td class="align-middle" id="comboObjeciones"> <input class="form-control form-control-sm" type="text" id="fecha-pago" value="<%=request.getParameter("fechaPago")%>"  readonly> </td>
		 			</tr>
		 			<tr>
			        	<td class="align-middle">&nbsp;Monto pago ($):</td>
		        		<td class="align-middle" id="comboObjeciones"> <input class="form-control form-control-sm" type="text" value="<%=request.getParameter("montoPago")%>" readonly> </td>
		 			</tr>
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/>  -->		
						
			<div>
				<!-- class="contenedorInput" -->
				<table style="width:100%" class="table table-sm small mx-auto"> 			
					<tr>
						<!-- class="CuerpoNormal" -->
			        	<td class="align-middle">&nbsp;Motivo del rechazo:</td>
			 		</tr>
			 		<tr>
			 			<td class="align-middle">
			 				<textarea class="form-control form-control-sm" rows="5" cols="58" style="width:99%" id="motivo-rechazo"></textarea>
			 			</td>
			 		</tr>
				</table>
			</div>
			
			<!-- <br style="line-height:5px"/>  -->
			
			<div>
				<!-- class="contenedorInput" -->
				<table  style="width:100%" class="table table-sm table-borderless">
			       		<tr>
		 				<td align="center">
		 				    <input type="button" id="rechazar-pago"  value="Rechazar" class="btn btn-primary" />
							<input type="button" id="cancelar" value="Cancelar" class="btn btn-primary" />
		 				</td>
		 			</tr>
				</table>
			</div>
	      </form>
	</body>
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	<script type="text/javascript" src="js/utils.js"></script>
	<script type="text/javascript" src="js/contabilidad/autorizacionPago.js"></script>
</html>
