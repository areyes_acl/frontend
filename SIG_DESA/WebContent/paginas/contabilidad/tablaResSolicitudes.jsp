<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
<script src="js/jquery.Rut.js" type="text/javascript"></script>
<script type="text/javascript" src="js/utils.js"></script>
<style>
	.loading {
	  font-size: 17px;
	  font-weight: bold;
	  vertical-align:middle !important;
	}
	
	.loading:after {
	  overflow: hidden;
	  display: inline-block;
	  -webkit-animation: ellipsis steps(4,end) 1000ms infinite;      
	  animation: ellipsis steps(4,end) 2000ms infinite;
	  content: "\2026";
	  width: 0px;
	}
	@keyframes ellipsis {
	  to {
	    width: 1.25em;    
	  }
	}
	
	@-webkit-keyframes ellipsis {
	  to {
	    width: 1.25em;    
	  }
	}
</style>
</head>
<body>
		<div style="width:800px;overflow-x:auto;overflow-y:auto;max-height:600px;">
			 <div style="margin:0px;padding:0px;">
				<table width="100%" class="table table-sm small" id="myTable" >
				
				
				<tr>
						<th style="text-align:center;">OPERADOR</th>
						<th style="text-align:center;">PRODUCTO<BR/>BIN</th>
						<th style="text-align:center;">TIPO<BR/>MOVIMIENTO</th>
						<th style="text-align:center;">DESDE</th>
						<th style="text-align:center;">HASTA</th>
						<th style="text-align:center;">ESTADO</th>
						
					</tr>

					<s:if test="%{getListaSolicitudes().isEmpty()}">
						<tr>
							<td colspan="6" style="text-align:center;">No se encontraron solicitudes pendientes</td>
						</tr>
					</s:if>
					<s:else>
				   		<s:iterator value="listaSolicitudes" status="solicitud">
					   		<tr>
									<td style="text-align: center ;"><s:property value="operador"/></td>
									<td style="text-align: center ;"><s:property value="bin"/></td>
									<td style="text-align: center;"><s:property value="tipoMov"/></td>
									<td style="text-align: center;"><s:property value="fechaInicio"/></td>
									<td style="text-align: center ;"><s:property value="fechaFin"/></td>
									<s:if test="%{listaSolicitudes[#solicitud.index].estado == 'PENDIENTE'}">
										<td style="text-align: center;"><span title="<s:property value="estado"/>"><div class="loading" style="display: inline;"></div></span></td>
									</s:if>
									<s:else>
										<td style="text-align: center;"><span title="<s:property value="estado"/>"><img src='img/checked.png'  style='width:18px;height:18px;'></span></td>
									</s:else>
									
									<td style="display:none;"><s:property value="momento"/></td>
									<td style="display:none;"><s:property value="sid"/></td>
									<td style="display:none;"><s:property value="fichero"/></td>
									<td style="display:none;"><s:property value="usuario"/></td>
									<td style="display:none;"><s:property value="estado"/></td>
									<td style="display:none;"><s:property value="total"/></td>
						</tr>	
					   	</s:iterator>
					</s:else>
			   	</table>
			</div>
		</div>
</body>
</html>