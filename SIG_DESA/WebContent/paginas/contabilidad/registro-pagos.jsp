<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />

<!DOCTYPE html>
<html>
<head>
<title>Contabilidad - Registro de pagos transbank</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/contabilidad/contabilidad.css">
</head>

<body>
	<div id="app-container">
		<!--  HEADER -->
		<header id="app-header">
			<!-- <div class="titulo">  -->
			<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold bg-white">
				REGISTRO DE PAGOS TRANSBANK
			</div>
		</header>
		<hr>
		<!-- BODY -->
		<section id="app-body">

			<div id="div-fecha-pago" class="text-center">
				<label>Fecha de pago:</label> 
					<!-- size="10" -->
					<input type="text" maxlength="10" value="" name="fecha-pago" id="fecha-pago"
						class="campoObligatorio fecha form-control col-2 d-inline text-center datepicker" placeholder="DD/MM/AAAA" />
					
					<input type="button" value="Buscar" id ="btn-buscar-pago" class="btn btn-primary btn-sm" />
						<select id="idOperador" style="visibility:hidden" class="d-block" >
							
						</select>
					
			</div>
			<%-- <div class="mb-4">
				<span>(DD/MM/AAAA)</span>
			</div> --%>
			
			<!-- class="contenedorInput" -->
			<table id="info-contable" class="table table-sm small col-7 mx-auto">
				<tr>
					<td class="align-middle"><label for="monto-sugerido">Monto Outgoing </label></td>
					<td class="align-middle"><span>$ </span><input type="text"
						name="montoSugerido" id="monto-sugerido-ic" class="soloLectura form-control form-control-sm col-11 d-inline"
						readonly="readonly"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Monto NCL</label></td>
					<td class="align-middle"><span>$ </span><input type="text" name="montoCPago"
						id="monto-cpago" class="soloLectura form-control form-control-sm col-11 d-inline" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Descuento Pago NCL</label></td>
					<td class="align-middle"><span>$ </span><input type="text" name="descuentoCPago"
						id="dscto-pago" class="soloLectura form-control form-control-sm col-11 d-inline" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Monto informado + descuento en mail TBK </label></td>
					<td class="align-middle"><span>$ </span><input type="text"
						name="montoIngresado" id="monto-ingresado" maxlength="15" value="0" class="numericOnly form-control form-control-sm col-11 d-inline"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Descuento en mail TBK</label></td>
					<td class="align-middle"><span>$ </span><input type="text"
						name="montoDescuento" id="monto-descuento" maxlength="15" value="0" class="numericOnly form-control form-control-sm col-11 d-inline"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Monto final pago TBK</label></td>
					<td class="align-middle"><span>$ </span><input type="text"
						name="montoFinal" id="monto-final" maxlength="15" value="0" class="numericOnly form-control form-control-sm col-11 d-inline" readonly></td>
				</tr>
			</table>

			<div class="text-center">
				<div class="custom-file col-4 d-block mx-auto" style="padding-top: 20px; padding-left: 230px;">
					<input class="custom-file-input" type="file"  id="file-to-upload-imagen" onchange="cambiarTextoInputFile('file-to-upload-imagen', 'file-to-upload-imagen-label')" >
					<label class="custom-file-label" for="file-to-upload-imagen" id="file-to-upload-imagen-label">Imagen Mail</label>
				</div>
	
				<div class="custom-file col-4 d-block mx-auto" style="padding-top: 20px; padding-left: 230px;">
					<input class="custom-file-input" type="file" id="file-to-upload-excel" onchange="cambiarTextoInputFile('file-to-upload-excel', 'file-to-upload-excel-label')">
					<label class="custom-file-label" for="file-to-upload-excel" id="file-to-upload-excel-label">Detalle Mail XLS</label>
				</div>
			</div>

			<div id="div-btn-registrar" class="pt-0">
				<input type="button" class="btn btn-primary" id="btn-registrar" value="Registrar">
			</div>

		</section>

		<!-- FOOTER -->
		<footer id="app-footer" class="text-center">
			<p>NOTA: El pago ingresado debe ser autorizado por un usuario con esos privilegios antes de ser registrado como válido en el sistema.</p>
			<hr>
		</footer>
	</div>
</body>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/contabilidad/registroPago.js"></script>
<script type="text/javascript" src="js/custom/custom.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>

<script>
	$(".datepicker").datepicker();
</script>

</html>
