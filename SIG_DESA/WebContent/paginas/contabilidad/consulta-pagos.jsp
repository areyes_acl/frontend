<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Consulta Diarias</title>
<script src="js/jquery-1.10.0.min.js" type="text/javascript">
	
</script>
<script src="js/jquery.Rut.js" type="text/javascript"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script>
			$(".datepicker").datepicker();

	   		$(document).ready(function(){
	   			cargaOperador();
	   		});
	   		
	   		function cargaOperador(){
				$.ajax({
			   			url:'cargarComboOperadores',
			   			type:'POST'
			   		}).done(function(data){
			   			data.listaOperadores;
			   			var datos = (data.listaOperadores).split("~");
			   			
			   			var strCombo = "";
			   			
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   				
			   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
							}
			   			
			   			$("#idOperador").append(strCombo);
			   		});
			}
	   </script>
<script>
	$(document).ready(function() {
		var f = new Date();

		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		if (mes < 10)
			mes = "0" + mes;
		if (dia < 10)
			dia = "0" + dia;
		var ano = f.getFullYear();
		$(".fecha").val(dia + "/" + mes + "/" + ano);
	});

	$("#buscar")
			.click(
					function() {
						var fechaTermino = "";
						if ($('#fechaInicio').val().length == 0) {
							alert("Debe ingresar fecha de inicio para realizar la busqueda...");
							$("#fechaInicio").focus();
							return;
						}

						if (validarFecha($('#fechaInicio').val()) == false) {
							alert("La fecha ingresada esta incorrecta, verifiquela...");
							$("#fechaInicio").focus();
							return;
						}

						if ($('#fechaTermino').val().length == 0) {
							f = new Date();
							fechaTermino = convertirFecha(f.getDate() + "/"
									+ (f.getMonth() + 1) + "/"
									+ f.getFullYear());
						} else {
							if (validarFecha($('#fechaTermino').val()) == false) {
								alert("La fecha ingresada esta incorrecta, verifiquela...");
								$("#fechaFin").focus();
								return;
							}

							fechaTermino = convertirFecha($('#fechaTermino')
									.val());
						}

						var fechaInicio = convertirFecha($('#fechaInicio')
								.val());

						if (fechaInicio > fechaTermino) {
							alert("La fecha de inicio es mayor a la fecha de termino, verifiquela... ");
							$("#fechaInicio").focus();
							return;
						}

						$.ajax({
							url : "buscarPagoContableAction",
							type : 'POST',
							data : $("#form1").serialize()
						}).done(function(data) {
							$("#tablaRes").empty().append(data);
						});

					});

	$("#exportar")
			.click(
					function() {
						var operador = document.getElementById('idOperador').options[document.getElementById('idOperador').selectedIndex].text;
						DataExportar = "";
						DatasExcel = "";

						idTablaExportar = "myTable";
							
						if(operador == "Transbank"){
							DataExportar += "TIT| | | | | | | | " + operador + " | | | | | | |~";
							DataExportar += "TIT|FECHA PAGO|MONTO INFORMADO|DESCUENTO|MONTO FINAL|USUARIO INGRESO|FECHA INGRESO|USUARIO AUTORIZA|FECHA AUTORIZA|ESTADO PAGO|MOTIVO RECHAZO|ESTADO CONTABLE|FECHA CONTABLE|ADJUNTO|EXCEL|~";
						}
						else{
							DataExportar += "TIT| | | | | | |" + operador + " | | | | | | |~";
							DataExportar += "TIT|FECHA PAGO|MONTO INFORMADO|DESCUENTO|MONTO FINAL|USUARIO INGRESO|FECHA INGRESO|USUARIO AUTORIZA|FECHA AUTORIZA|ESTADO PAGO|MOTIVO RECHAZO|ESTADO CONTABLE|FECHA CONTABLE|ADJUNTO|~";
						}
							
						
						DataExportar += exportarExcelConciliacion(
								idTablaExportar, DatasExcel);
								

						var datos = replaceCaracterEspecial(DataExportar);
						var parametros = "dataExcel=" + datos;
						parametros += "&nombreArchivo=export";

						location.href = "./exportarDatosExcelAction?"
								+ parametros;
					});

	function replaceCaracterEspecial(cadena) {
		var cadenaAux = cadena;
		cadenaAux = cadenaAux.replace(/á/g, 'a');
		cadenaAux = cadenaAux.replace(/é/g, 'e');
		cadenaAux = cadenaAux.replace(/í/g, 'i');
		cadenaAux = cadenaAux.replace(/ó/g, 'o');
		cadenaAux = cadenaAux.replace(/ú/g, 'u');
		cadenaAux = cadenaAux.replace(/Á/g, 'A');
		cadenaAux = cadenaAux.replace(/É/g, 'E');
		cadenaAux = cadenaAux.replace(/Í/g, 'I');
		cadenaAux = cadenaAux.replace(/Ó/g, 'O');
		cadenaAux = cadenaAux.replace(/Ú/g, 'U');
		return cadenaAux;
	}

	/**
	 * SE ADJUNTA EL XLS DE PAGO 
	 */
	function adjuntarXLS(sidPago,fechaPago) {
	
		$.ajax({
			url : "paginas/contabilidad/popup-adjunto-xls.jsp",
			type : 'POST',
			data : "sid=" + sidPago + "&fecha="
					+ fechaPago
		}).done(function(data) {
			$("#popup").css({
				display : 'block'
			});

			$("#popUpContenido").css({
				display : 'block'
			});

			$("#popUpContenido").empty().append(data);
			centradoDiv('popUpContenido', 'small');
		});

	};
</script>
</head>

<body>
	<div id="popup" style="display: none;">
		
	</div>
	
	<div id="popUpContenido" class="content-popup" style="display: none;"></div>

	<form id="form1">
		<input type="hidden" value="100" name="cantReg" id="cantReg" />
		
		<!-- <div class="titulo">  -->
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold bg-white">
			CONSULTA DE PAGOS
		</div>
		<hr>
		<!-- <br style="line-height: 5px" />  -->

		<div>
			<!-- class="contenedorInput" -->
			<table width="100%" class="table table-sm">
				<tr class="text-right">
					<td class="align-middle">Fecha Inicio:</td>
					<td class="align-middle text-center">
						<!-- size="10"  placeholder="DD/MM/AAAA"  -->
						<input type="text" name="fechaInicio" id="fechaInicio"
							maxlength="10" value="" class="campoObligatorio fecha form-control form-control-sm col-7 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td class="align-middle">Fecha Final:</td>
					<td class="align-middle text-center">
					
						<input type="text" name="fechaTermino" id="fechaTermino"
							size="10" maxlength="10" value="" class="campoObligatorio fecha form-control form-control-sm col-6 mx-auto text-center datepicker" placeholder="DD/MM/AAAA" />
					</td>
					<td rowspan="2" class="align-middle" colspan="2" align="center"> <input type="button"
						value="Buscar" id="buscar" class="btn btn-primary" />
					</td>
				</tr>
				<tr class="text-right">
				<td class="align-middle">Estado :</td>
					<td class="align-middle text-center">
						<select name="estadoPago" id="estadoPago" class="custom-select col-9 mx-auto">
							<option value="TDO">TODOS</option>
							<option value="ING">INGRESADO</option>
							<option value="RCH">RECHAZADO</option>
							<option value="AUT">AUTORIZADO</option>
						</select>
					</td>
					<td class="align-middle">Operador :</td>
					
					<td class="align-middle text-center">
						<select id="idOperador" name="operador" class="custom-select col-9 mx-auto">
           				</select>
      				</td>
				</tr>
			</table>
		</div>

		<!-- <br style="line-height: 5px" />  -->

		<!-- class="cajaContenido" -->
		<div class="">
			<div style="width: 100%;" id="tablaRes"></div>
		</div>

		<!-- <br style="line-height: 5px" />  -->

		<!-- MUESTRA LA DESCRIPCION -->
		<div class="my-2 col-6 mx-auto">
			<!-- style="display:none;width: 500px;margin-left: 150px;" class="contenedorInput"-->
			<table id="tablaDescripcion"  class="table table-sm small" style="display:none;">
				<thead>
					<tr class="text-center">
						<th class="align-middle">
							<!-- style="font-weight: bold;width: 500px;font-size: 14px;background: #a5bad1;" -->
							<div class="font-weight-bold">
								Descripci&oacute;n
							</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<tr class="text-center">
						<!--  style="aling: center;width: 500px" -->
						<td id ="descripcion" >
					</tr>
				</tbody>
			</table>
		</div>

		<!-- <br style="line-height: 5px" />  -->

		<div>
			<!-- class="contenedorInput" -->
			<table width="100%" class="table table-sm table-borderless">
				<tr>
					<td class="align-middle" align="center"><input type="button" value="Exportar Tabla" class="btn btn-primary"
						id="exportar" name="exportar" /></td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>