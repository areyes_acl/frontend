<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Consulta Pagos</title>
	    <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
		<script src="js/jquery.Rut.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/utils.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		
		
	});

	// muestra la descripción	
	function showDescription(description) {
		if(description === null || description === "") {
			$('#descripcion').append(
				'<div id="page"> \ <p class="texto">Sin descripcion \ </div>');
			$("#tablaDescripcion").css("display", "");
		}
		else {
			$('#descripcion').append(
				'<div id="page"> \ <p class="texto">' + description
						+ ' \ </div>');
			$("#tablaDescripcion").css("display", "");
		}
	}

	// esconde la descripcion
	function hideDescription() {
		$('#descripcion').html("");
		$("#tablaDescripcion").css("display", "none");
	}
	
	

	/**
	 * 
	 * 
	 * SE ENVIA A GUARDAR PAGO
	 * 
	 * 
	 */
	function guardarPago() {
		// ARCHIVOS
		var file_data_excel = $("#file-to-upload-excel").prop("files")[0];

		// CREA EL ARCHIVO FORM DATA
		var form_data = new FormData();

		if (file_data_excel) {
			if (!validarExtensionArchivoExcel(file_data_excel.name)) {
				alert("Tipo de archivo adjunto 'Detalle Mail XLS' debe ser un archivo Excel");
				return false;
			}
		} else {
			file_data_excel = "";
		}

		form_data.append("adjuntoExcel", file_data_excel);
		
		if (file_data_excel != "") {
			form_data.append("filenameExcel", file_data_excel.name);
		} else {
			form_data.append("filenameExcel", "");
		}

	
	$.ajax({
			url : 'adjuntarArchivo',
			type : 'POST',
			data : form_data,
			processData : false,
			contentType : false,
			enctype : 'multipart/form-data',
			success : function(datos, textStatus, xhr) {
				if (datos.codigoError == 0) {
					alert(" Se ha guardado el pago correctamente. ");
				} else {
					alert(datos.mensajeError);
				}
			},
		});
	}
</script>
</head>
	<body>
		<div></div>
	
		 
	 	<div style="width:800px;overflow-x:auto;overflow-y:auto;max-height:600px;">
			 <div style="margin:0px;padding:0px;">
			 	<!-- class="tablaContenido" -->
				<table width="100%" class="table table-sm small" id="myTable" >
				
				
				<tr class="text-center">
						<th class="align-middle">FECHA<BR/>PAGO</th>
						<th class="align-middle">MONTO INFORMADO </th>
						<th class="align-middle">DESCUENTO</th>
						<th class="align-middle">MONTO FINAL</th>
						<th class="align-middle">USUARIO<BR/>INGRESO</th>
						<th class="align-middle">FECHA<BR/>INGRESO</th>
						<th class="align-middle">USUARIO<BR/>AUTORIZA</th>
						<th class="align-middle">FECHA<BR/>AUTORIZA</th>
						<th class="align-middle">ESTADO<BR/>PAGO</th>
						<th class="align-middle" style="display:none;">MOTIVO<BR/>RECHAZO</th>
						<th class="align-middle">ESTADO<BR/>CONTABLE</th>
						<th class="align-middle">FECHA<BR/>CONTABLE</th>
						<th class="align-middle">ADJUNTO</th>
					<s:if test="%{operador == 1}">
						<th class="align-middle">EXCEL</th>
					</s:if>
					</tr>
			   		<s:iterator value="listaPagos" status="pago">
				   		<tr class="text-center">
								<td class="align-middle" style="text-align: center;"><s:property value="fechaPago"/></td>
								<td class="align-middle" style="text-align: center ;"><s:property value="montoFormateado"/></td>
								<td class="align-middle" style="text-align: center ;"><s:property value="descuentoFormateado"/></td>
								<td class="align-middle" style="text-align: center ;"><s:property value="montoFinalFormateado"/></td>
								<td class="align-middle" style="text-align: center;"> 	<s:property value="log.nombreUsuarioIngreso"/> </td>
								<td class="align-middle" style="text-align: center;"><s:property value="log.fechaIngreso"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="log.nombreUsuarioAutorizador"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="log.fechaAutorizacion"/></td>
								<td class="align-middle" style="text-align: center;"  onmouseover="showDescription('<s:property value="largeDescription"/>');" onmouseout="hideDescription();"><s:property value="estadoPago.description"/></td>
								<td class="align-middle" style="text-align: center; display:none;"><s:property value="largeDescription"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="log.estadoContable"/></td>
								<td class="align-middle" style="text-align: center;"><s:property value="log.fechaContable"/></td>
								<td class="align-middle" style="text-align: center;"><a href="download?fileName=<s:property value="filenameAdjuntoImagen"/>"><s:property value="filenameAdjuntoImagen"/></a></td>
							
								<s:if test="%{operador == 1}">			
									<s:if test="%{listaPagos[#pago.index].filenameAdjuntoExcel != null && listaPagos[#pago.index].filenameAdjuntoExcel != ''}">
										<td class="align-middle" style="text-align: center;"><a href="download?fileName=<s:property value="filenameAdjuntoExcel"/>"><s:property value="filenameAdjuntoExcel"/></a></td>
									</s:if>
									<s:elseif
										test="%{(listaPagos[#pago.index].filenameAdjuntoExcel == null || listaPagos[#pago.index].filenameAdjuntoExcel == '') && listaPagos[#pago.index].estadoPago.xkey == 'AUT'}">
										<td class="align-middle" style="text-align: center;">
											<img id="btn-upload-xls" src="${pageContext.request.contextPath}/img/upload.png" style="width: 15px; padding: 2px; cursor: pointer;" onclick="adjuntarXLS(<s:property value="sid" /> , '<s:property value="log.fechaIngreso" />' )"/>
										</td>
									</s:elseif>
									<s:else>
										<td class="align-middle" style="text-align: center;">Sin excel adjunto.</td>
									</s:else>
								</s:if>
					</tr>	
				   	</s:iterator>
			   	</table>
			</div>
		</div>
	</body>
</html>