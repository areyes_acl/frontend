<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Autorizaci&oacute;</title>
</head>

<body>
	
	<!-- <div class="titulo">  -->
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold bg-white">
		AUTORIZACI&Oacute;N DE PAGOS
	</div>
	<hr>
	<!-- <br style="line-height: 5px" /> -->
	
	<select class="custom-select col-3 mb-3" id="idOperador" name="operador">
           				</select>
	<!-- DIV DE POP UPS -->
	<div id="popup" style="display: none;"></div>
	<div id="popUpContenido" class="content-popup" style="display: none;"></div>
	
	<!-- FILTROS FECHAS -->
	<div id="staticParent" style="display:none;">
	<div class="titulo" style="font-size:smaller">Buscar Pagos Pendientes</div>
		<table style="width: 100%;" class="contenedorInput">
			<tr>
				<td>&nbsp;Fecha desde :</td>
				<td><input type="text" size="10" maxlength="10" value=""
					name="fechadesde" id="fecha-pago-desde"
					class="campoObligatorio fecha" /> <span>(DD/MM/AAAA)</span></td>
				<td>&nbsp;Fecha hasta :</td>
				<td><input type="text" size="10" maxlength="10" value=""
					name="fechahasta" id="fecha-pago-hasta"
					class="campoObligatorio fecha" /> <span>(DD/MM/AAAA)</span> <input
					type="button" id="buscar-pagos" value="Buscar"
					style="margin-left: 100px"></td>
			</tr>
		</table>
	</div>

	<!-- <br style="line-height: 5px" />  -->

	<!-- class="cajaContenido" -->
	<div>

		<div id="divlistaPagos"></div>

	</div>
</body>

<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/contabilidad/autorizacionPago.js"></script>
<script>
	   		$(document).ready(function(){
	   			cargaOperador();
	   		});
	   		
	   		function cargaOperador(){
				$.ajax({
			   			url:'cargarComboOperadores',
			   			type:'POST'
			   		}).done(function(data){
			   			data.listaOperadores;
			   			var datos = (data.listaOperadores).split("~");
			   			
			   			var strCombo = "";
			   			
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   				
			   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
							}
			   			
			   			$("#idOperador").append(strCombo);
			   		});
			}
	   </script>
	   <script>
	   		$("#idOperador").on('change', function(){
	   			
	   			var operador = $("#idOperador").val();
	   			cargaDatosInicial();
	   			buscaPagos("", "", operador);
	   			
	   		
	   		});
	   </script>

</html>