<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Detalle Contable Diario</title>
<link rel="stylesheet" type="text/css" href="css/contabilidad/contabilidad.css">
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/detalleContable/detalleContable.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script>
			$(".datepicker").datepicker();

	   		$(document).ready(function(){
	   			cargaOperador();
	   			$("#exportarDC").prop("disabled", true);
	   			buscarSolicitudes();
	   			
	   		});
	   		
	   		$('#idOperador').on('change', function(){
	   			cargaProducto();
	   		});
	   		
	   		$('#idProducto').on('change', function(){
	   			var operador = $("#idOperador").val();
	   			var producto = $("#idProducto").val();
	   			cargarTipoMovimiento(operador, producto);
	   		});
	   		
	   		function buscarSolicitudes(){
	   		
	   			$.ajax({
	   				url: 'buscarSolicitudes',
	   				type: 'POST',
	   			}).done(function(data){
	   				$("#resultadoSolicitudes").empty().append(data);
	   				$("#sidSolicitud").val($('#myTable').find('td').eq('7').text());
	   				$("#fichero").val($('#myTable').find('td').eq('8').text());
	   				
	   				
	   				
	   				var estado = $('#myTable').find('td').eq('10').text();

	   				if(estado != ''){
	   				
		   				if(estado == 'PENDIENTE'){
		   					$("#solicitar-registros-contables").prop("disabled", true);
		   					$("#exportarDC").prop("disabled", true);
		   					document.getElementById("solicitar-registros-contables").setAttribute('title', 'No es posible generar una solicitud, ya que tiene una pendiente');
		   				}
		   				else{
		   					$("#solicitar-registros-contables").prop("disabled", false);
		   					$("#exportarDC").prop("disabled", false);
		   					document.getElementById("solicitar-registros-contables").setAttribute('title', '');
		   					if($('#myTable').find('td').eq('11').text() != ''){
		   						$("#cantidad-registros").empty().append("Cantidad de Registros: " + $('#myTable').find('td').eq('11').text());
		   					}
		   					
		   				}
	   				
	   				}
	   				
	   				
	   				
	   			});
	   		}
	   		
	   		
	   		
	   		function cargaOperador(){
				$.ajax({
			   			url:'cargarComboOperadores',
			   			type:'POST'
			   		}).done(function(data){
			   			data.listaOperadores;
			   			var datos = (data.listaOperadores).split("~");
			   			
			   			var strCombo = "";
			   			//strCombo += "<option selected disabled>-Seleccionar-</option>";
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   				
			   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
							}
			   			
			   			$("#idOperador").append(strCombo);
			   			cargaProducto();
			   		});
			}
	   		
	   		function cargaProducto(){
	   			$("#idProducto").html("");
	   			var operador = $("#idOperador").val();
				$.ajax({
			   			url:'cargarComboProductos',
			   			type:'POST',
			   			data: 'operador='+operador
			   		}).done(function(data){
			   			data.listaProductos;
			   			var datos = (data.listaProductos).split("~");
			   			
			   			var strCombo = "";
			   			
			   			//strCombo += "<option selected disabled>-Seleccionar-</option>";
			   			for(var i=1;i<datos.length-1;i++)
							{			
			   				var fila = datos[i].split("|");
			   				
			   				strCombo += "<option value='"+fila[0]+"'>"+fila[2]+"</option>";
							}
			   			
			   			$("#idProducto").append(strCombo);
			   			var operador = $("#idOperador").val();
			   			var producto = $("#idProducto").val();
			   			cargarTipoMovimiento(operador, producto);
			   		});
			}
	   </script>
</head>

<body>
	<div id="app-container">
		<!--  HEADER -->
		<header id="app-header">
			<!-- <div class="titulo"> -->
			<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold bg-white">
				EXPORTAR DETALLE CONTABLE
			</div>
		</header>
		<hr>
		<!-- <br>  -->
		
		<div>
			<!-- class="contenedorInput" -->
	    	<table width="100%" class="table table-sm">
	    		<tr class="text-right">
	    			<td class="align-middle">OPERADOR:</td>
			        <td class="align-middle">
			        <!-- style="width:100px" -->
			        <select id="idOperador" class="custom-select"></select>
			        </td>
			        <td class="align-middle">PRODUCTO:</td>
			        <td class="align-middle">
			        <select id="idProducto" class="custom-select"></select>
			        </td>
			        <td class="align-middle">TIPO MOV:</td>
		        	<td class="align-middle">
		        	<select id="tipoMovBusqueda" class="custom-select"></select>
		        	</td>
			        
			        <!-- <td  align="center"> </td>  -->
		        	<td class="align-middle" rowspan="2"><input type="button" id="solicitar-registros-contables" value="Solicitar" class="btn btn-primary"/></td>
		      	</tr>
		      	<tr class="text-right">
			        <td class="align-middle">DESDE:</td>
			        <!-- size="10"  style="width: 57px;" placeholder="(dd/mm/aaaa)" -->
			        <td class="align-middle text-center" colspan="2">
			        <input type="text" maxlength="10" value="" name="fechaInicio" id="fechaInicio" class="campoObligatorio fecha form-control form-control-sm mx-auto text-center col-6 d-inline datepicker" placeholder="DD/MM/AAAA" />
			        </td>
			        <td class="align-middle">HASTA:</td>
			        <!-- size="10"  style="width: 57px;" placeholder="(dd/mm/aaaa)" -->
			        <td class="align-middle text-center" colspan="2">
			        <input type="text" maxlength="10" value="" name="fechaTermino" id="fechaTermino" class="campoObligatorio fecha form-control form-control-sm mx-auto text-center col-8 d-inline datepicker" placeholder="DD/MM/AAAA" />
			        </td>
		      	</tr>
	    	</table>
	    	<table id="tablaExportar" style="display:none;"></table>
	    	
	   
	    </div>
	</div>

	<br style="line-height: 5px" />
		<div id="solicitudRespuesta" style="display:none;"></div>
	<br style="line-height: 5px" />
		<div id="resultadoSolicitudes"></div>
		<input type="hidden" id="sidSolicitud">
		<input type="hidden" id="fichero">
	<br style="line-height: 5px" />
	<br>
	
	<div id="divLogCargaTrxNoAutICExport">
		<!-- class="contenedorInput" -->
		<table width="100%" class="table table-sm table-borderless">
			<tr>
				<td align="center">
					<label id="cantidad-registros" class="d-block font-weight-bold mb-4"></label>
					<!-- <br>
					<br> -->
					<input type="button" value="Exportar Detalle" name="exportar" id="exportarDC" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
	</div>
</body>

</html>