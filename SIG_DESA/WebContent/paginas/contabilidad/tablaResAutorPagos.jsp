<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import = "Beans.Usuario" %>
<% Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog"); %>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Consulta Pagos</title>

		<script type="text/javascript">
			$(document).ready(function(){
				var url;
				var parametros;			

				if($("#numPagina").val()==1){	
					$("#anterior").bind("click",function(e){
				        e.preventDefault();
			        });
				}else{
					$("#anterior").unbind("click",false);					
				}
				
				
				if($("#ultimaPagina").val()<=1){
					$("#botonesPaginacion").hide();
				}
			
				if($("#ultimaPagina").val()==0){
					$("#anterior").hide();
					$("#siguiente").hide();
				}

				
				$("#siguiente").click(function(){
					if(parseInt($("#numPagina").val()) < parseInt($("#ultimaPagina").val()))
					{
						var numPag = parseInt($("#numPagina").val())+1;
						parametros = parametros + "&numPagina="+numPag;
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
				});

				$("#anterior").click(function(){
					if(parseInt($("#numPagina").val()) > 1)
					{
						var numPag = parseInt($("#numPagina").val())-1;
						parametros = parametros + "&numPagina="+numPag;
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
				});

				$("#ir").click(function(){
					var paginaIr;
					var ultimaPagina;

					paginaIr = $("#paginaIr").val();

					ultimaPagina = $("#ultimaPagina").val();

					if(Number(paginaIr) > Number(ultimaPagina)){
						alert("La página no existe");
						$("#paginaIr").val("");
					}else{
						var numPag = paginaIr;

						parametros = parametros + "&numPagina="+$("#paginaIr").val();
						var datos = parametros;
						$.ajax({
							url:url,
							type:'POST',
							data:datos
						}).done(function(data){
							$("#divlistaTransacciones").empty().append(data);
						});
					}
				});
			});
		</script>
	</head>
	<body>
		<div></div>
		<input id="ultimaPagina" name="ultimaPagina" type="hidden" value="<s:property value="%{#request.ultimaPagina}" />"/>
		<input type="hidden" value="<s:property value="%{#request.numPagina}" />" name="numPagina" id="numPagina"/>
		<input type="hidden" value="<s:property value="%{#request.fechaInicioPag}" />" name="fechaInicioPag" id="fechaInicioPag"/> 
		<input type="hidden" value="<s:property value="%{#request.fechaTerminoPag}" />" name="fechaTerminoPag" id="fechaTerminoPag"/>
		<input type="hidden" value="<s:property value="%{#request.numeroTarjeta}" />" name="numeroTarjetaPag" id="numeroTarjetaPag"/> 

	 	<div style="width:810px;overflow-x:hidden;overflow-y:auto;max-height:200px;">
			 <div style="margin:0px;padding:0px;">
			 
			 	<!-- class="tablaContenido" -->
				<table width="800px" class="table table-sm small">
				<tr class="text-center">
					<th class="align-middle" style="display:none">SID</th>
					<th class="align-middle" style="width: 100px; padding-top:2px;font-weight: bold;font-size: 12px;">FECHA PAGO</th>
					<th class="align-middle" style="width: 100px; padding-top:2px;font-weight: bold;font-size: 12px;">MONTO INFORMADO</th>
					<th class="align-middle" style="width: 100px; padding-top:2px;font-weight: bold;font-size: 12px;">DESCUENTO</th>
					<th class="align-middle" style="width: 100px; padding-top:2px;font-weight: bold;font-size: 12px;">MONTO FINAL</th>
					<th class="align-middle" style="width: 140px; padding-top:2px;font-weight: bold;font-size: 12px;">USUARIO INGRESO</th>
					<th class="align-middle" style="width: 140px; padding-top:2px;font-weight: bold;font-size: 12px;">FECHA INGRESO</th>
					<th class="align-middle" style="width: 140px; padding-top:2px;font-weight: bold;font-size: 12px;">ADJUNTO</th>
				<s:if test="%{operador == 1}">
					<th class="align-middle" style="width: 140px; padding-top:2px;font-weight: bold;font-size: 12px;">XLS</th>
				</s:if>	
					<th class="align-middle" style="width: 90px; padding-top:2px;font-weight: bold;font-size: 12px;">AUTORIZAR</th>
					<th class="align-middle" style="width: 90px; padding-top:2px;font-weight: bold;font-size: 12px;">RECHAZAR</th>
				</tr>
			   		<s:iterator value="listaPagos" status="pago"  >
				   		<tr class="text-center">
							<td style="display:none"><s:property value="sid"/></td>
							<td style="width:100px; padding-top:1px;text-align: center;"><s:property value="fechaPago"/></td>
							<td style="width:100px; padding-top:1px;text-align: center;"><s:property value="montoFormateado"/></td>
							<td style="width:100px; padding-top:1px;text-align: center;"><s:property value="descuentoFormateado"/></td>
							<td style="width:100px; padding-top:1px;text-align: center;"><s:property value="montoFinalFormateado"/></td>
							<td style="width:140px; padding-top:1px;text-align: center;"><s:property value="log.nombreUsuarioIngreso"/></td>
							<td style="width:140px; padding-top:1px;text-align: center;"><s:property value="log.fechaIngreso"/></td>
							
							<td style="text-align: center;"><a href="download?fileName=<s:property value="filenameAdjuntoImagen"/>"><s:property value="filenameAdjuntoImagen"/></a></td>
					<s:if test="%{listaPagos[#pago.index].operador == 1}">
						<s:if
							test="%{listaPagos[#pago.index].filenameAdjuntoExcel != null && listaPagos[#pago.index].filenameAdjuntoExcel != ''}">
							<td
								style="text-align: center;"><a
								href="download?fileName=<s:property value="filenameAdjuntoExcel"/>"><s:property
										value="filenameAdjuntoExcel" /></a></td>
						</s:if>

						<s:else>
							<td
								style="text-align: center;">Sin excel adjunto</td>
						</s:else>
					</s:if>

						<td style="width:75px; padding-top:1px;">
								<% if(usuarioLog.getActualizacion() == 1){ %>
									<button style="" title="Autorizar" onclick='autorizarPago(<s:property value="sid"/>,"<s:property value="fechaPago"/> ");' class="btn btn-primary">A</button>
								<% } %>
							</td>
							<td style="width:75px; padding-top:1px;">
								<% if(usuarioLog.getActualizacion() == 1){ %>
									<button style="" value="" title="Rechazar" onclick='rechazarPago(<s:property value="sid"/>,"<s:property value="fechaPago"/>",<s:property value="montoBruto"/>);' class="btn btn-primary">R</button>
								<% } %>
							</td>
				   		</tr>	
				   	</s:iterator>
			   	</table>
			</div>
		</div>
		
		<div id="botonesPaginacion" style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial,Helvetica,sans-serif; font-size: 8pt;">
		   	<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		   	<s:property value="%{#request.numPagina}" /> de <s:property value="%{#request.ultimaPagina}" /> p&aacute;gina(s)&nbsp;&nbsp;&nbsp;
		   	Ir a pagina <input type="text" id="paginaIr" style="width:50px;font-size: 8pt;" class="form-control form-control-sm d-inline py-0 align-middle" /> <input type="button" value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt;"/>
		</div>
	</body>
</html>