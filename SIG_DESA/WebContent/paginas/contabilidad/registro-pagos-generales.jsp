<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />

<!DOCTYPE html>
<html>
<head>
<title>Contabilidad - Registro de pagos Visa</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/contabilidad/contabilidad.css">
</head>

<body>
	<div id="app-container">
		<!--  HEADER -->
		<header id="app-header">
			<!-- <div class="titulo">  -->
			<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold bg-white">
				REGISTRO DE PAGOS VISA
			</div>
		</header>
		<hr>
		<!-- BODY -->
		<section id="app-body">
			<div id="div-fecha-pago" class="text-center mb-4 ml-0">
				<div class="d-inline"><label class="align-middle" >Operador:</label></div>
			
				<!-- style="height: 30px;"  -->
				<div class="align-middle d-inline mr-3"><select id="idOperador" class="custom-select custom-select-sm col-4 mb-2 align-middle"></select></div>
			
				<!-- <br>
				<br> -->
			
				<div class="d-inline mr-3">
					<label class="align-middle">Fecha de pago:</label>
					<!--  size="10" -->
					<input type="text" maxlength="10" value="" name="fecha-pago" id="fecha-pago"
						class="campoObligatorio fecha form-control col-2 text-center d-inline align-middle mb-1 datepicker" placeholder="DD/MM/AAAA" />
				</div>
			
				<!-- <br>  -->
			
				<div class="d-inline">
					<div class="d-inline">					
						<input type="button" value="Buscar" id ="btn-buscar-pago" class="btn btn-primary btn-sm mt-2 align-middle mb-2">
					</div>
				</div>
			
			</div>
			
			<!-- class="contenedorInput" -->
			<table id="info-contable" class="table table-sm small col-5 mx-auto">
				<tr>
					<td class="align-middle"><label for="monto-sugerido">Monto Outgoing </label></td>
					<td class="align-middle"><span>$ </span>&nbsp;<input type="text"
						name="montoSugerido" id="monto-sugerido-ic" class="soloLectura form-control form-control-sm col-10 d-inline"
						readonly="readonly"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Monto TC46</label></td>
					<td class="align-middle"><span>$ </span>&nbsp;<input type="text" name="montoCPago"
						id="monto-cpago" class="soloLectura form-control form-control-sm col-10 d-inline" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Descuento Pago TC46</label></td>
					<td class="align-middle"><span>$ </span>&nbsp;<input type="text" name="descuentoCPago"
						id="dscto-pago" class="soloLectura form-control form-control-sm col-10 d-inline" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Monto informado</label></td>
					<td class="align-middle"><span>$ </span>&nbsp;<input type="text"
						name="montoIngresado" id="monto-ingresado" maxlength="15" value="0" class="numericOnly form-control form-control-sm col-10 d-inline"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Descuento</label></td>
					<td class="align-middle"><span>$ </span>&nbsp;<input type="text"
						name="montoDescuento" id="monto-descuento" maxlength="15" value="0" class="numericOnly form-control form-control-sm col-10 d-inline"></td>
				</tr>
				<tr>
					<td class="align-middle"><label>Monto final</label></td>
					<td class="align-middle"><span>$ </span>&nbsp;<input type="text"
						name="montoFinal" id="monto-final" maxlength="15" value="0" class="numericOnly form-control form-control-sm col-10 d-inline" readonly></td>
				</tr>
			</table>
			
			<div class="text-center">
				<div class="custom-file col-4" style="padding-top: 20px; padding-left: 230px;">
					<input class="custom-file-input" type="file"  id="file-to-upload-imagen" onchange="cambiarTextoInputFile('file-to-upload-imagen', 'file-to-upload-imagen-label')" >
					<label class="custom-file-label text-left" id="file-to-upload-imagen-label" for="file-to-upload-imagen">PDF</label>
				</div>
			</div>
				
			<div id="div-btn-registrar" class="pt-0">
				<input type="button" id="btn-registrar" value="Registrar" class="btn btn-primary">
			</div>

		</section>

		<!-- FOOTER -->
		<footer id="app-footer" class="text-center">
			<p>NOTA: El pago ingresado debe ser autorizado por un usuario con esos privilegios antes de ser registrado como válido en el sistema.</p>
			<hr>
		</footer>
	</div>
</body>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/contabilidad/registroPagoGeneral.js"></script>
<script type="text/javascript" src="js/custom/custom.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>

<script>
	$(".datepicker").datepicker();
</script>
</html>