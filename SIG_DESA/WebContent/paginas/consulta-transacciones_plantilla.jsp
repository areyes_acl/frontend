<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>

   <head>
       
       <title>Gestion De Transacciones</title>
       <script src="js/jquery-1.10.0.min.js" type="text/javascript"> </script>
	   <script type="text/javascript" src="js/utils.js"></script>
	   
	
	<script>
	
		$("#objetar").click(function(){
			$.ajax({
				url:"paginas/popup-objeciones.jsp",
				type:'POST',
				/*success: 
					function(data)
					{
						alert(data);	
					}*/
			}).done(function(data){
				$("#popup").css({
				    display: 'block'
				});
				
				$("#popUpContenido").empty().append(data);
				centradoDiv('popUpContenido');
			});
		});
		
		
		$("#gestionPorCuenta").click(function(){
			
			url = "paginas/gestioncuenta.jsp";
			$.ajax({
				url:url,
				type:'POST',
				/*success: 
					function(data)
					{
						alert(data);	
					}*/
			}).done(function(data){
				$("#popup").css({
				    display: 'block'
				});
				
				$("#popUpContenido").empty().append(data);
				centradoDiv('popUpContenido');
			});
		});
		
		
		
		$("#seleccionTRX").click(function(){
			$("#accionesDisponibles").css("display","");
		});
		
		$("#idacciones").change(function() {
			var valorOpcion = $("#idacciones" ).val();
			var tipo = "";
			var url  = "";
			
			if (valorOpcion==1 || valorOpcion==2 || valorOpcion==3 || valorOpcion==4 || valorOpcion==5){
				//alert("opcion uno");
				
				if (valorOpcion==1){
					tipo="PE"; //Peticion de vale
					url = "paginas/popupPeticionValeContracargo.jsp?tipoAccion=" + tipo;
				}
				
				if (valorOpcion==2){
					tipo="CT"; //Contracargo
					url = "paginas/popupPeticionValeContracargo.jsp?tipoAccion=" + tipo;
				}
				

				if (valorOpcion==3){
					tipo="CP"; //Contracargo Parcial
					url = "paginas/popupPeticionValeContracargo.jsp?tipoAccion=" + tipo;
				}

				if (valorOpcion==4){
					tipo="ST"; //Segundo Contracargo Total
					url = "paginas/popupPeticionValeContracargo.jsp?tipoAccion=" + tipo;
				}
				
				if (valorOpcion==5){
					tipo="SP"; //Segundo Contracargo Parcial
					url = "paginas/popupPeticionValeContracargo.jsp?tipoAccion=" + tipo;
				}
				
				$.ajax({
					url:url,
					type:'POST',
					/*success: 
						function(data)
						{
							alert(data);	
						}*/
				}).done(function(data){
					$("#popup").css({
					    display: 'block'
					});
					
					$("#popUpContenido").empty().append(data);
				});
				
			}
			
			if (valorOpcion==6){//Generar Cargo/Abono
				
				url = "paginas/popupGenerarCargoAbono.jsp";
			
				$.ajax({
					url:url,
					type:'POST',
					/*success: 
						function(data)
						{
							alert(data);	
						}*/
				}).done(function(data){
					$("#popup").css({
					    display: 'block'
					});
					
					$("#popUpContenido").empty().append(data);
				});
			
			}
			
			if (valorOpcion==7){//Recuperacion y Cobro De Cargo
				
				url = "paginas/popupRecuperacionCobroCargo.jsp";
			
				$.ajax({
					url:url,
					type:'POST',
					/*success: 
						function(data)
						{
							alert(data);	
						}*/
				}).done(function(data){
					$("#popup").css({
					    display: 'block'
					});
					
					$("#popUpContenido").empty().append(data);
				});
			
			}
			
			
			
			
		});
		
	</script>
</head>

<body>
	<div id="popup" style="display: none;">
		<div id="popUpContenido" class="content-popup">Hola Mundo</div>
	</div>
	
	<div class="titulo">
		GESTI&Oacute;N DE TRANSACCIONES
	</div>
    
    <table width="100%" class="contenedorInput">
    
    	    <tr class="">
		        <td class=""><input type="radio" name="group1" value="C">&nbsp;Presentaci&oacute;n</td>
		        <td class=""><input type="radio" name="group1" value="C">&nbsp;Primer Contracargo Total</td>
		        <td class=""><input type="radio" name="group1" value="C">&nbsp;Primer Contracargo Parcial</td>
	      	</tr>
	      	
	      	<tr class="">
	      		<td class=""><input type="radio" name="group1" value="C">&nbsp;Segundo Contracargo Total</td>
	      		<td class=""><input type="radio" name="group1" value="C">&nbsp;Segundo Contracargo Parcial</td>
		        <td class=""><input type="radio" name="group1" value="C">&nbsp;Petici&oacute;n De Vale</td>
	      	</tr>
	      	
	      	<tr class="">
	      		<td class=""><input type="radio" name="group1" value="C">&nbsp;Confirmación De Petici&oacute;n De Vale</td>
	      		<td class=""><input type="radio" name="group1" value="C">&nbsp;Representaci&oacute;n</td>
		        <td class=""><input type="radio" name="group1" value="C">&nbsp;Recuperai&oacute;n De Cobro De Cargos</td>
	      	</tr>
	      	
		    <tr class="">
		        <td class=""><input type="radio" name="group1" value="C">&nbsp;Objeciones</td>
		        <td class=""><input type="radio" name="group1" value="C">&nbsp;Rechazos</td>
		    	<td class="">&nbsp;</td>
		       
	      	</tr>
    </table>
    
    <br style="line-height:5px"/>
    
    <table width="100%" class="contenedorInput">
    	<tr class="">
		        <td class="">&nbsp;N&deg; TC :&nbsp;</td>
	        	<td class=""><input type="text" value="5xxx xxxx xxxx 1235"/></td>
		        <td class="">&nbsp;Fecha Inicio :&nbsp;</td>
		        <td class=""><input type="text" size="10" maxlength="10" value="26-03-2013"/></td>
		        <td class="">&nbsp;Fecha Final :&nbsp;</td>
		        <td class=""><input type="text" size="10" maxlength="10" value="26-03-2013"/>&nbsp;</td>
		        <td class="" colspan="4" align="center">&nbsp; 
	        		<input type="button" value="Buscar"/>&nbsp;&nbsp;&nbsp;
		        </td>
	      </tr>
    </table>
    
    <br style="line-height:5px"/>

	<table width="100%" class="tablaContenido">
	    
	    	<tr class="">
	    	  
		        <th class="">SEL</th>
	        	<th class="">NRO TC</th>
		        <th class="">FECHA TRANSACC.</th>
		        <th class="">COMERCIO</th>
		        <th class="">PAIS</th>
		        <th class="">MONTO ORIG.</th>
		        <th class="">MONTO</th>
		        <th class="">COD. RAZON</th>
		        <th class="">ESTATUS</th>
		        
		        
		        
	      	</tr>
	      	<%
	      		for(int i=0;i<10;i++)
	      		{
	      	%>
      			<tr class="">
			        <td width="0px">
			        	<input type="radio" name="seleccionTRX" value="C" id="seleccionTRX">
			        </td>
		        	<td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        <td width="0px">&nbsp;</td>
			        
		      	</tr>
	      	<%		
	      		}
      		%>
	    </table>
	    
	    <br style="line-height:5px"/>
	    
	    <div id="accionesDisponibles" style="display:none;">
	    
    		<table width="100%" class="">
    	    <tr class="bgceleste">
    	    
    	     		<td class="">Acccion :
			    		<select name="acciones" id="idacciones">
			    		    <option value="0"> Seleccione Opcion</option>
			    			<option value="1"> Peticion De Vale</option>
			    			<option value="2"> Primer Contracargo Total</option>
			    			<option value="3"> Primer Contracargo Parcial</option>
			    			<option value="4"> Segundo Contracargo Total</option>
			    			<option value="5"> Segundo Contracargo Parcial</option>
			    			<option value="6"> Generar Cargo / Abono</option>
			    			<option value="7"> Rec. De Cobro De Cargos</option>
			    		</select>
			    	</td>
    	    </tr>
    		</table>
	    </div>
	    
	    <br style="line-height:5px"/>
	    
	    
	    <div class="subTitulo">
			Datos De La Cuenta Del Cliente	    
	    </div>
    	<table width="100%" class="contenedorInput">
	    	
	    	<tr class="">
    			<td  class="">Cliente :</td>
    			<td  class=""><input type="text"/></td>
    			<td  class="">Fecha Vencto :</td>
    			<td  class=""><input type="text"/></td>
    		</tr>
    		
    		<tr class="bgceleste">
    			<td  class="">Estado De Cuenta :</td>
    			<td  class=""><input type="text" ></input></td>
    			<td  class="">Fecha Bloqueo :</td>
    			<td  class=""><input type="text" ></input></td>
    		</tr>
    		
    		<tr class="bgceleste">
    			<td  class="">Adicional :</td>
    			<td  class=""><input type="text" ></input></td>
    			<td  class="" colspan="2"></td>
    		</tr>
    
	    </table>

		<br style="line-height:5px"/>

	    
	    
		<div class="subTitulo">
			Datos De La Transaccion Seleccionada
		</div>
    	
	     <table width="100%" class="contenedorInput">
	    	
    		<tr class="bgceleste">
    			<td  class="">Microfilm :</td>
    			<td  class=""><input type="text"/></td>
    			<td  class="">Cod. Autorizac. :</td>
    			<td  class=""><input type="text"/></td>
    			<td  class="">Otros Datos :</td>
    			<td  class=""><input type="text"/></td>
    		</tr>
    		
    		<tr class="">
    			<td class="">Fecha Efectiva :</td>
    			<td class=""><input type="text"/></td>
    			<td class="">Fecha Proceso :</td>
    			<td class=""><input type="text"/></td>
    			<td class="">Otros Datos :</td>
    			<td class=""><input type="text"/></td>
    		</tr>
    		
    		<tr class="">
    			<td class="">Bin Adquiriente :</td>
    			<td class=""><input type="text"/></td>
    			<td class="">Lee Banda</td>
    			<td class=""><input type="text"/></td>
    			<td class="">Otros datos</td>
    			<td class=""><input type="text"/></td>
    		</tr>
    	</table>
    	<br/>
    	<div>
    		<input type="button" value="Gestión Por Cuenta" id="gestionPorCuenta" name="gestionPorCuenta"/>
    		<input type="button" value="Objetar" id="objetar"/>
    		<input type="button" value="Exportar a Excel" />
    	
    	</div>
 
</body>
</html>