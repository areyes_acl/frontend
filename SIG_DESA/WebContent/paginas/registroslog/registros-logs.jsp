<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="Beans.Usuario"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>

<html>
<head>
<title>Registros Logs</title>
</head>
<%
	Usuario usuarioLog = (Usuario) request.getSession().getAttribute(
			"usuarioLog");
%>
<body>
	<form id="form1">
		<!-- <div class="titulo">  -->
		<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold">
			REGISTROS DE LOGS
		</div>
		<hr>
		<!-- <br style="line-height: 5px" />  -->

		<div>
			<!-- class="contenedorInput" -->
			<table style="width: 100%" class="table table-sm">
				<tr class="text-center">
					<td class="text-right align-middle">Fecha:</td>
					<!-- size="10"  style="width: 57px;" -->
					<td class="align-middle">
					
						<input type="text"
							maxlength="10" value="" name="fecha-busqueda" id="fecha-busqueda"
							class="campoObligatorio fecha form-control form-control-sm mx-auto text-center w-50 px-4 datepicker" placeholder="DD/MM/AAAA" />
					</td>


					<td class="text-right align-middle">Tabla:</td>
					<td class="align-middle"><select id="select-tabla-log" class="custom-select">
							<option value="">SELECCIONE...</option>
							<option value="LOG_ERROR">LOG_ERROR</option>
							<option value="TBL_EXCEPT_INCOMING">TBL_EXCEPT_INCOMING</option>
							<option value="TBL_LOG_BOL">TBL_LOG_BOL</option>
							<option value="TBL_LOG_COMISION">TBL_LOG_COMISION</option>
							<option value="TBL_LOG_CONTABLE">TBL_LOG_CONTABLE</option>
							<option value="TBL_LOG_CPAGO">TBL_LOG_CPAGO</option>
							<option value="TBL_LOG_ONUS">TBL_LOG_ONUS</option>
							<option value="TBL_LOG_OUT_ONUS">TBL_LOG_OUT_ONUS</option>
							<option value="TBL_LOG_OUT">TBL_LOG_OUT_TBK</option>
							<option value="TBL_LOG_OUT_VISA">TBL_LOG_OUT_VN</option>
							<option value="TBL_LOG_OUT_VISA_INT">TBL_LOG_OUT_VI</option>
							<option value=TBL_LOG_RCH>TBL_LOG_RCH</option>
							<option value=TBL_LOG_TRANSAC_AUTOR>TBL_LOG_TRANSAC_AUTOR</option>
							<option value="TBL_LOG_INC">TBL_LOG_OUTGOING_TBK</option>
							<option value=TBL_LOG_VISA>TBL_LOG_OUTGOING_VN</option>
							<option value="TBL_LOG_VISA_INT">TBL_LOG_OUTGOING_VI</option>
					</select></td>
					<td class="align-middle" align="center"> <input type="button" value="Buscar"
						name="buscarTransaccion" id="buscar" class="btn btn-primary"/>
					</td>
				</tr>
			</table>
		</div>


		<!-- <br style="line-height: 5px" />  -->

		<!-- class="subTitulo" -->
		<div id="tituloTablaResultado" class="col-6 small mx-auto my-4 text-align-center text-center font-weight-bold" style="display: none;">
			Lista de Registros de Log
		</div>

		<div class="cajaContenido">
			<div class="table table-sm small" id="tablaRes"></div>

		</div>

		<!-- <br style="line-height: 5px" /> -->

		<!-- class="contenedorInput" -->
		<table class="table table-sm table-borderless" style="width: 100%;">
			<tr class="text-center">
				<td class="align-middle" align="center"><input type="button" value="Exportar"
					name="exportar" id="exportar" class="btn btn-primary" disabled /></td>
			</tr>
		</table>


	</form>
</body>
<script type="text/javascript" src="js/registrosLog/registros-logs.js"></script>
<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/custom/bootstrap-datepicker.es.min.js"></script>

<script>
	$(".datepicker").datepicker();
</script>
</html>