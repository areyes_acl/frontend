<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../loginCheck.jsp" flush="true" />
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
	$(document).ready(
			function() {

				desbloqueaExport();

				if ($("#numPagina").val() == 1) {
					$("#anterior").bind("click", function(e) {
						e.preventDefault();
					});
				} else {
					$("#anterior").unbind("click", false);
				}

				if ($("#ultimaPagina").val() <= 1) {
					$("#botonesPaginacion").hide();
				}

				if ($("#ultimaPagina").val() == 0) {
					$("#anterior").hide();
					$("#siguiente").hide();
				}

				$("#siguiente")
						.click(
								function() {

									if ($("#numPagina").val() < $(
											"#ultimaPagina").val()) {
										var numPag = parseInt($("#numPagina")
												.val()) + 1;
										buscarRegistrosLog($("#fecha-busqueda")
												.val(), $("#select-tabla-log")
												.val(), numPag);
									}
								});

				$("#ir").click(
						function() {

							var paginaIr;
							var ultimaPagina;

							paginaIr = $("#paginaIr").val();

							ultimaPagina = $("#ultimaPagina").val();

							if (paginaIr == '' || paginaIr == null
									|| typeof paginaIr == "undefined"
									|| paginaIr == 0 || isNaN(paginaIr)) {
								alert("La página no existe");
								$("#paginaIr").val("");
								return;
							}

							if (Number(paginaIr) > Number(ultimaPagina)
									|| Number(paginaIr) < 0) {
								alert("La página no existe");
								$("#paginaIr").val("");
							} else {
								var numPag = paginaIr;
								buscarRegistrosLog($("#fecha-busqueda").val(),
										$("#select-tabla-log").val(), numPag);
							}

						});

				$("#anterior")
						.click(
								function() {
									if ($("#numPagina").val() > 1) {
										var numPag = parseInt($("#numPagina")
												.val()) - 1;
										buscarRegistrosLog($("#fecha-busqueda")
												.val(), $("#select-tabla-log")
												.val(), numPag);
									}
								});
			});

	// FUNCION PARA DESBLOQUEAR EL BOTON DE EXPORTAR
	function desbloqueaExport() {
		var cantidadElementos = $("#cant-registros").val();

		if (cantidadElementos > 0) {
			$("#exportar").prop("disabled", false);
		} else {
			$("#exportar").prop("disabled", true);
		}
	}
</script>

<style type="text/css">
#btn-delete:active {
	opacity: 0.3;
}
</style>
</head>
<body>
	<div></div>
	<input type="hidden"
		value="<s:property value="%{#request.cantidadElementos}" />"
		name="cantidadRegistros" id="cant-registros" />
	<input type="hidden"
		value="<s:property value="%{#request.ultimaPagina}" />"
		name="ultimaPagina" id="ultimaPagina" />
	<input type="hidden"
		value="<s:property value="%{#request.numPagina}" />" name="numPagina"
		id="numPagina" />
	<input type="hidden"
		value="<s:property value="%{#request.fechaInicioPag}" />"
		name="fechaInicioPag" id="fechaInicioPag" />
	<input type="hidden"
		value="<s:property value="%{#request.fechaTerminoPag}" />"
		name="fechaTerminoPag" id="fechaTerminoPag" />
	<input type="hidden"
		value="<s:property value="%{#request.numeroTarjeta}" />"
		name="numeroTarjetaPag" id="numeroTarjetaPag" />

	<div
		style="width: 800px; overflow-x: auto; overflow-y: auto; max-height: 500px;">
		<div style="margin: 0px; padding: 0px;">
			<!-- class="tablaContenido" -->
			<table style="width: 100%" class="table table-sm" id="tabla">


				<s:if test="%{filtro.logTableType.tableName.equals('LOG_ERROR')}">
					<tr class="text-center">
						<th class="align-middle">SID</th>
						<th class="align-middle">FECHA INSERCION</th>
						<th class="align-middle">NOMBRE_SP</th>
						<th class="align-middle">MSG_ERROR</th>
					</tr>
				</s:if>
				<s:elseif
					test="%{filtro.logTableType.tableName.equals('TBL_EXCEPT_INCOMING')}">
					<tr class="text-center">
						<th class="align-middle">SID</th>
						<th class="align-middle">LOG CARGA</th>
						<th class="align-middle">FECHA</th>
						<th class="align-middle">MIT</th>
						<th class="align-middle">COD. FUNCION</th>
						<th class="align-middle">NRO. TARJETA</th>
						<th class="align-middle">COD. AUTORIZACION</th>
					</tr>
				</s:elseif>
				<s:elseif
					test="%{filtro.logTableType.tableName.equals('TBL_LOG_BOL')}">
					<tr class="text-center">
						<th class="align-middle">SID</th>
						<th class="align-middle">FECHA</th>
						<th class="align-middle">FILENAME</th>
						<th class="align-middle">FILE_FLAG</th>
						<th class="align-middle">FILE_TS_DOWNLOAD</th>
						<th class="align-middle">FILE_TS_UPLOAD</th>
					</tr>
				</s:elseif>
				<s:else>
					<tr class="text-center">
						<th class="align-middle">SID</th>
						<th class="align-middle">FECHA</th>
						<th class="align-middle">FILENAME</th>
						<th class="align-middle">FILE_FLAG</th>
						<th class="align-middle">FILE_TS</th>
					</tr>
				</s:else>


				<s:iterator value="listaRegistrosLog">
					<s:if test="%{filtro.logTableType.tableName.equals('LOG_ERROR')}">
						<tr class="text-center">
							<td class="align-middle" style="display: none;"><label id="sid"><s:property
										value="sid" /></label></td>
							<td class="align-middle"><s:property value="sid" /></td>
							<td class="align-middle" style="text-align: center;"><s:property
									value="fechaInsercion" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="nombreSp" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="msgError" /></td>
						</tr>
					</s:if>
					<s:elseif
						test="%{filtro.logTableType.tableName.equals('TBL_EXCEPT_INCOMING')}">
						<tr class="text-center">
							<td class="align-middle" style="display: none;"><label id="sid"><s:property
										value="sid" /></label></td>
							<td class="align-middle"><s:property value="sid" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="logCarga" /></td>
							<td class="align-middle" style="text-align: center;"><s:property
									value="fechaIncoming" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="mit" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="codigoFuncion" /></td>
							<td class="align-middle" style="text-align: center;"><s:property
									value="numeroTarjeta" /></td>
							<td class="align-middle" style="text-align: center;"><s:property
									value="codigoAutorizacion" /></td>
						</tr>
					</s:elseif>
					<s:elseif
						test="%{filtro.logTableType.tableName.equals('TBL_LOG_BOL')}">
						<tr class="text-center">
							<td class="align-middle" style="display: none;"><label id="sid"><s:property
										value="sid" /></label></td>
							<td class="align-middle"><s:property value="sid" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="fecha" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="filename" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="fileFlag" /></td>
							<td class="align-middle" style="text-align: center;"><s:property
									value="fileTsDownload" /></td>
							<td class="align-middle" style="text-align: center;"><s:property
									value="fileTsUpload" /></td>
						</tr>
					</s:elseif>
					<s:else>
						<tr class="text-center">
							<td class="align-middle" style="display: none;"><label id="sid"><s:property
										value="sid" /></label></td>
							<td class="align-middle"><s:property value="sid" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="fecha" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="filename" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="fileFlag" /></td>
							<td class="align-middle" style="text-align: center;"><s:property value="fileTs" /></td>
						</tr>
					</s:else>


				</s:iterator>
			</table>
		</div>
	</div>


	<div id="botonesPaginacion"
		style="color: rgb(0, 0, 0); font-style: normal; font-family: Arial, Helvetica, sans-serif; font-size: 8pt;">
		<a id="anterior" href="#">Anterior</a> <a id="siguiente" href="#">Siguiente</a>
		<s:property value="%{#request.numPagina}" />
		De
		<s:property value="%{#request.ultimaPagina}" />
		P&aacute;gina(s)&nbsp;&nbsp;&nbsp; Ir a pagina <input type="text"
			id="paginaIr" style="width:50px;font-size: 8pt;" class="form-control form-control-sm d-inline py-0 align-middle" /> <input type="button"
			value="Ir" id="ir" class="align-middle btn btn-primary py-0" style="font-size: 8pt;" />
	</div>


</body>
</html>