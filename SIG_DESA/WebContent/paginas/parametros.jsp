<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <jsp:include page="loginCheck.jsp" flush="true" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Documento sin título</title>	
	<script type="text/javascript">
	
				document.querySelector("#buscar").onkeyup = function(){
		        $TableFilter("#tablaRes", this.value);
		    }
		    
		    $TableFilter = function(id, value){
		        var rows = document.querySelectorAll(id + ' tbody tr');
		        
		        for(var i = 0; i < rows.length; i++){
		            var showRow = false;		            
		            
		            var row = rows[i];
		            row.style.display = 'none';
		            
		            for(var x = 1; x < row.childElementCount; x++){
		                if(row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1){
		                	console.log(row.children[x]);
		                    showRow = true;
		                    break;
	                 }
		            }
		            
		            if(showRow){
		                row.style.display = null;
		            }
		        }
		    }
	
		var jsonSalida = new Object();
		var lista = "";
	
		$(document).ready(function(){	
			$.ajax({
				url:'listaParametrosAction',
				type:'POST',
				dataType: "json",
			}).done(function(resp){
				var datos = (resp.listaParametros).split("~");
				console.log(datos.length);
				for(var i=1;i<datos.length-1;i++)
				{
					var fila = datos[i].split("|");		
					
					var $tabla = $("#tablaRes");
					
					if(fila[5] == 'null'){
						fila[5] = '';
					}
					
					$tabla.append('<tr class="text-center" id="trFuncion">'							
						+'<td id="td1" style="display:none;">'+fila[0]+'</td>'
						// style="width:60px;"
						+'<td class="align-middle" >'+fila[1]+'</td>'
						// style="width:75px;"
						+'<td class="align-middle" >'+fila[2]+'</td>'
						// style="width:250px;"
						+'<td class="align-middle text-left" id="tdDesc" titulo="'+fila[3]+'"><span id="t'+i+'">'+fila[3]+'</span></td>'
						// style="width:55px;"
						+'<td class="tdPlazo align-middle" style="-moz-user-select: none;-khtml-user-select: none;-webkit-user-select: none;user-select: none;" onkeypress="return ingresoMonto();">'+fila[4]+'</td>'
						// style="width:60px;"
						+'<td class="align-middle" id="td2" >'+fila[5]+'</td>'
						// style="width:*;"
						+'<td class="align-middle text-left" >'+fila[6]+'</td>'
						+'</tr>'
					);
					substringTooltip(i);
				}
			}).error(function(error, status, setting){
				alert("Error Parametros: "+error);
			});
			
			$('#tablaRes').on('click', 'td', function() {
				$("#tablaRes tr").removeClass('filaSel');
				$(this).parent().addClass('filaSel');
				jsonSalida.tipo = $(this).parent().find('td').eq('1').text();
				jsonSalida.codigo = $(this).parent().find('td').eq('2').text();
				jsonSalida.transa = $(this).parent().find('td').eq('3').text();
				jsonSalida.glosa = $(this).parent().find('td').eq('4').text();
				jsonSalida.montoMaximo = $(this).parent().find('td').eq('5').text()
				jsonSalida.plazo = $(this).parent().find('td').eq('6').text();
				
			});
			
			$("#tablaRes").on("dblclick",".tdPlazo",function(){
				var valPlazo = $.trim($(this).text());
				var sid = $(this).parent().find('td').eq('0').text();
				$(this).empty();
				// style="height:14px; width:18px;font-size:8pt;" (ELIMINADOS)
				$(this).append("<input id='txtTemp' class='txtTemp form-control form-control-sm' style='-moz-user-select: none;-khtml-user-select: none;-webkit-user-select: none;user-select: none;' maxlength='6' type='text' value='"+ valPlazo +"'>");
				$(".txtTemp").focus().focusout(function(){
					var newValue = $.trim($(".txtTemp").val());
					$(this).parent().empty().append(newValue);
					
					$.ajax({
						url:"updatePlazoParametrosAction",
						type:"post",
						data:"sid="+sid+"&plazo="+newValue
					}).done(function(data){
						//alert(data.updateParametros);
					});
				});
			});
			
			/*$("#tablaRes").on("mouseover",".flecha",function(){
				alert($(this).parent().attr('titulo'));
			});*/
		});
	</script>
	<style>
		.cabeceraEstatica thead tr th { position: sticky; top: 0; border: 0px; box-shadow: 1px -1px 1px 1px;}
	</style>
</head>

<body>
	<!-- <div class="titulo">  --> 
	<div class="col-12 mx-auto my-4 text-align-center text-center font-weight-bold"> 
    	C&Oacute;DIGOS DE RAZ&Oacute;N
    </div>
    <hr/>
     <div class="mt-2">
            <input id="buscar" type="text" class="form-control w-25" placeholder="Filtrar por..." />
  </div>
    <!-- class="cajaContenido" -->
    <div class="mt-2">
    	<!-- <div>
    		class="tablaContenido"
	    	<table width="100%" class="table table-sm small" id="tabla1">
				<tr>
	                <th style="width:60px;">
	                	C&oacute;digo
		        	</th>
	                <th style="width:75px;">
	                	Transacci&oacute;n
		        	</th>
	                <th style="width:250px;">
	                	Glosa
		        	</th>
	                <th style="width:55px;">
	                	Plazo
		        	</th>
		        	<th style="width:60px;">
	                	Monto maximo
		        	</th>
	                <th style="width:*;">
	                	Acci&oacute;n
		        	</th>
		        </tr>
		    </table>
		</div> -->
	    <div id="tablaResDiv" class="mt-2 cabeceraEstatica border-top">
	    	<!-- class="tablaContenido" -->
			<table width="100%"  class="table table-sm small" id="tablaRes">
				<thead>
				<tr class="text-center ">
	                <th class="align-middle bg-white " >
	                	C&Oacute;DIGO
		        	</th>
	                <th class="align-middle bg-white" >
	                	TRANSACCI&Oacute;N
		        	</th>
	                <th class="align-middle bg-white" >
	                	GLOSA
		        	</th>
	                <th class="align-middle bg-white" >
	                	PLAZO
		        	</th>
		        	<th class="align-middle bg-white" >
	                	MONTO M�XIMO
		        	</th>
	                <th class="align-middle bg-white" >
	                	ACCI&Oacute;N
		        	</th>
		        </tr>
		        </thead>
		    </table>
		</div>
    </div>
    
    <!--div class="botoneraExterna">
        <div>
        	<input type="button" value="Salir"/>
          	<input type="button" value="Ingresar" />
          	<input type="button" value="Modificar" />
          	<input type="button" value="Eliminar" />
        </div>
    </div-->
</body>
</html>
