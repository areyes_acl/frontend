$(function(){
	// Reprocesamiento
	var seleccion = new Array();
	bloquearExportar();
	function bloquearExportar(){
		if(parseInt($(".resultadoAction").html()) != 0){
			$("#exportarLog").attr("disabled", "disabled");
		}else{
			$("#exportarLog").removeAttr("disabled");
		}
	}
	
	$("#buscarLog").click(function(){
		if (validarFechas()){
			changePage(1, false);
		}
	});
	
	$("#reprocesarLog").click(function(){
		
		$.get("dashboardTDIReprocesarAction", "reprocesar="+seleccion, function(data){
			if(parseInt(data) == 1){
				resetFilter();
				changePage(1, false);
				$('.elementoReprocesar').removeAttr("checked");	
				seleccion = new Array();
			}
		});
		
	});
	
	function resetFilter(){
		$('#fechaInicio').val("");
		$("#fechaTermino").val("");
		$("#estado").val(-1);
	}
	
	function validarFechas(){
        var validador = true;
        if($('#fechaInicio').val().trim().length > 0 || $('#fechaTermino').val().trim().length >0 ){
        	
		if (!validarFecha($('#fechaInicio').val().trim()) && $('#fechaInicio').val().trim().length > 0) {
			alert("La fecha de inicio ingresada es incorrecta");
			$("#fechaInicio").focus();
			validador = false;
		}
		
		if (!validarFecha($('#fechaTermino').val().trim()) && $('#fechaTermino').val().trim().length >0 && validador) {
			alert("La fecha de fin ingresada es incorrecta");
			$("#fechaFin").focus();
			validador = false;
		}

		var fechaTermino = convertirFecha($('#fechaTermino').val().trim());
		var fechaInicio = convertirFecha($('#fechaInicio').val().trim());

		if (fechaInicio > fechaTermino && validador) {
			alert("La fecha de inicio no debe ser mayor a la fecha de fin ");
			$("#fechaInicio").focus();
			validador = false;
		}
        }
		return validador;
	}
	
	$("#siguiente").click(function(){
		var pagina = parseInt($(".paginaActual").html());
		var pagfinal = parseInt($(".paginaFinal").html());
		if(pagina < pagfinal){
			changePage(pagina+1, true);
			$("#paginaIr").val("");
		}
	});
	
	$("#anterior").click(function(){
		var pagina = parseInt($(".paginaActual").html());
		var pagfinal = parseInt($(".paginaFinal").html());
		if(pagina > 1){
			changePage(pagina-1, true);
			$("#paginaIr").val("");
		}
	});
	
	$("#ir").click(function(){
		if($("#paginaIr").val().trim().length !== 0){
			var paginIr = parseInt($("#paginaIr").val());
			var pagfinal = parseInt($(".paginaFinal").html());
			if(paginIr >= 1 && paginIr <= pagfinal){
				changePage(paginIr, true);
			}else{
				alert("La p\u00E1gina no existe");
				$("#paginaIr").val("");
			}
		}
		
	});
	
	botonesPaginacion();
	
	function botonesPaginacion(){
		$(".paginaActual").html($(".resultPage").html());
		$(".paginaFinal").html($(".resultPageFinal").html());
		if(parseInt($(".resultadoAction").html()) != 0 || (parseInt($(".paginaActual").html()) == 1 && parseInt($(".paginaFinal").html()) == 1)){
			$("#botonesPaginacion").hide();
		}else{
			$("#botonesPaginacion").show();
		}
	}
	
	function changePage(page, sesion){
		activarCargando();
		var fechaInicio = $("#fechaInicio").val();
		var fechaTermino = $("#fechaTermino").val();
		var estado = $("#estado").val();
		
		if(sesion){
			fechaInicio = $(".resultfechaInicio").html();
			fechaTermino = $(".resultfechaTermino").html();
			estado = $(".resultEstado").html();
		}
		
		//var reprocesar = ""+seleccion+"]";
		$.get("dashboardTDISearchAction", "page="+page+"&fechaInicio="+fechaInicio+"&fechaTermino="+fechaTermino+"&estado="+estado+"&reprocesar="+seleccion, function(data){
				$("#contenidoTabla").html(data);
				botonesPaginacion();
				activarClickReprocesar();
				activarSeleccion();
				inciarJob();
				bloquearExportar();
		});
	}
	
	inciarJob();
	var timer;
	function inciarJob(){
		timer = setInterval(actualizacionReprocesado, 10000);
	}
	
	
	function actualizacionReprocesado(){
		var valor = $(".elementoEstadoPROCESANDO").length;
		if(valor > 0){
			peticionRecursiva(valor, 0);
		}else{
			clearInterval(timer);
		}
		
	}
	
	function peticionRecursiva(valor, eq){
		var valor = $(".elementoEstadoPROCESANDO").length;
		if(eq < valor){
			var id = $(".elementoEstadoPROCESANDO").eq(eq).attr('data-id');
			//Hacer peticion ajax buscando el valor actual y devolver los resultados para actualizarlos en la vista dinamicamente. 
			desactivarCargando();
			$.get("dashboardTDIUpdateEstadoAction", "estadoUpdate="+id, function(data){
				activarCargando();
				if(data !== "1"){
					var json = $.parseJSON(data);
					var elemento = $(".elementoEstadoPROCESANDO").eq(eq);
					elemento.addClass("elementoEstado"+json['estado']);
					elemento.removeClass("elementoEstadoPROCESANDO");
					
					var elementoTdEstado = $(".elementoTdEstado"+id);
					var elementoTdArchivo = $(".elementoTdArchivo"+id);
					elementoTdEstado.removeClass("extra");
					
					switch(json['estado']){
					   case 'ERROR': 
						   elementoTdEstado.addClass('error');
						   elementoTdEstado.html("<div class='tooltip2'> "+json['estado']+"<span class='tooltiptext'>"+json['error']+"</span></div>");
						   $('.elementoReprocesar'+id).css("visibility", "visible");
						   break;
					   case 'ENTREGADO': 
						   elementoTdEstado.addClass('info');
						   elementoTdEstado.html(json['estado']);
						   var cadena = "";
						   var archivos = json['error'].split(";");
						   for(var i=0; i<archivos.length; i++){
							   cadena+=archivos[i]+"<br>";
						   }
						   elementoTdArchivo.html(cadena);
						   break;
					}
					
					var elementoTdhorafin = $('.elementoTdhorafin'+id);
					elementoTdhorafin.html(json['hora_termino']);
					
					var elementoTdEjec = $('.elementoTdEjec'+id);
					elementoTdEjec.html(json['delta']);
					
				}
				eq++;
				peticionRecursiva(valor, eq);
			});
		}
	}
	
	function activarSeleccion(){
		if(seleccion.length > 0 ){
			$.each(seleccion, function (key,value) {
				$(".elementoReprocesar"+value).attr("checked", "checked");
			});
			$("#reprocesarLog").removeAttr("disabled");
		}else{
			$("#reprocesarLog").attr("disabled", "disabled");
		}
	}
	
	activarClickReprocesar();
	
	function activarClickReprocesar(){
		$('.elementoReprocesar').click(function(){
			if($(this).is(":checked")){
				seleccion.push($(this).attr('data-id'));
			}else{
				seleccion.splice($.inArray($(this).attr('data-id'), seleccion),1);
			}
			if(seleccion.length > 0 ){
				$("#reprocesarLog").removeAttr("disabled");
			}else{
				$("#reprocesarLog").attr("disabled", "disabled");
			}
			//alert(seleccion);
		});
	}

	function desactivarCargando(){
		$(document).unbind('ajaxStart');
		$(document).unbind('ajaxStop');
	}
	
	function activarCargando(){
		$(document).ajaxStart(function(){
			$("#loading").show();
		}).ajaxStop(function(){
			$("#loading").hide();
		});
	}
	
});