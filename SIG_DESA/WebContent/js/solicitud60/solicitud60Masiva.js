// CARGA INICIAL QUE LLAMA AL LOADINIT
$(loadInit);

/**
 * Metodo que realiza la carga de los campos al iniciar
 * 
 * Monto sugerido Monto Comision de pago
 */
function loadInit() {

	// CARGA FECHAS INICIO
	$("#fecha-desde").val(getYesterday());
	$("#fecha-hasta").val(getToday());
	
}


/**
 * Boton buscar trx
 */
$("#buscar").click(function(){
	var fechaDesde = $("#fecha-desde").val();
	var fechaHasta = $("#fecha-hasta").val();
	
	
	if(fechaDesde != '' && fechaHasta != ''){
		buscaTransaccion(fechaDesde, fechaHasta,$("#numPagina").val());
	}else{
		alert("Debe ingresar un rango de fecha v�lido para realizar al busqueda.");
	};
	
	
});



/**
 * 
 * Metodo que realiza la busqueda de transacciones Rechazo
 * 
 * @param fechaDesde
 * @param fechaHasta
 */
function buscaTransaccion(fechaDesde, fechaHasta,numPagina){
	var num_tc = $("#numeroTarjeta").val();

	var datos = {
			fechaDesde : fechaDesde,
			fechaHasta : fechaHasta,
			numeroTarjeta : num_tc,
			numPagina: numPagina
	};
	
	
	$.ajax({
		     url : 'buscarRegistrosSol60Masiva',
		    type : 'POST',
		    data : datos,
		 success : function(data, textStatus, xhr) {		
				
				$("#tablaRes").empty().append(data);
				$("#tablaResTitulo").css("display","");
				$("#tablaRes").css("display","");
			
		}
	});
}




/**
 * 
 */
function eliminarCargoAbono(sid){
	var msj ="\u00bfEst\u00E1 seguro de eliminar el cargo/abono seleccionado? \n\n";

	if(confirm(msj)){	
	
		var data = {
				sidCargoAbono : sid
		};
	
	$.ajax({
			url : "eliminarCargoAbonoPend",
			type : 'POST',
			data : data
			}).done(function(data) {
				
				var numeroPagina =$("#numPagina").val();
				var cantidadRegistros = $("#cant-registros").val();
				
				if(cantidadRegistros == 1 && numeroPagina > 1){
					numeroPagina--;
				}

				buscaCargosAbonos($("#fecha-desde").val(),$("#fecha-hasta").val(),numeroPagina);
				
			});
	
	}
}

/**
 * CARGAR ABONAR
 */
$("#cargoabono").click(
				function() {
					
					var array = $.map($('input[name="check[]"]:checked'), function(c){return c.value; });	
										
					if(array != null && array.length == 0){
						alert("No se ha seleccionado ninguna transaccion para cargar/abonar.");
						return;
					}
					
					$.ajax({
							url : "paginas/solicitud60/popUpCargoAbonoMasivo.jsp",
							type : 'POST',
							data : "array="+JSON.stringify(array),
							}).done(function(data) {
								
								
								$("#popup").css({
									display : 'block'
								});

								$("#popUpContenido").css({
									display : 'block'
								});

								$("#popUpContenido").empty().append(data);
								centradoDiv('popUpContenido', 'small');
					});
				});



/**
 * Boton buscar pagos
 */
$("#exportar").click(function(){

	var fechaDesde = $("#fecha-desde").val();
	var fechaHasta = $("#fecha-hasta").val();
	var num_tc = $("#numeroTarjeta").val();

	var parametros = "&fechaDesde=:ini:&fechaHasta=:fin:&numeroTarjeta=:tc:";
	parametros = parametros.replace(":ini:", fechaDesde);
	parametros = parametros.replace(":fin:", fechaHasta);
	parametros = parametros.replace(":tc:", num_tc);

	location.href = "./exportarSol60Masiva?"
			+ parametros;
	
	});


