// CARGA INICIAL QUE LLAMA AL LOADINIT
$(loadInit);

/**
 * Metodo que realiza la carga de los campos al iniciar
 * 
 * Monto sugerido Monto Comision de pago
 */
function loadInit() {

	// CARGA FECHAS INICIO
	$("#fecha-desde").val(getYesterday());
	$("#fecha-hasta").val(getToday());
	
}


/**
 * Boton buscar pagos
 */
$("#buscar-cargoabono").click(function(){
	var fechaDesde = $("#fecha-desde").val();
	var fechaHasta = $("#fecha-hasta").val();
	
	
	if(fechaDesde != '' && fechaHasta != ''){
		buscaCargosAbonos(fechaDesde, fechaHasta,$("#numPagina").val());
	}else{
		alert("Debe ingresar un rango de fecha v�lido para realizar al busqueda.");
	};
	
	
});



/**
 * 
 * Metodo que busca los pagos pendientes de autorizacion
 * 
 * @param fechaDesde
 * @param fechaHasta
 */
function buscaCargosAbonos(fechaDesde, fechaHasta,numPagina){
	var num_tc = $("#numerotarjeta").val();
	var tipo =$("#tipo").val();
	var estado = $("#estado").val();

	var datos = {
			fechaDesde : fechaDesde,
			fechaHasta : fechaHasta,
			numeroTarjeta : num_tc,
			tipoCargoAbono : tipo,
			estadoCargoAbono: estado,
			numPagina: numPagina
	};
	
	
	$.ajax({
		     url : 'buscarCargosAbonos',
		    type : 'POST',
		    data : datos,
		 success : function(data, textStatus, xhr) {		
				
				$("#tablaRes").empty().append(data);
				$("#tablaResTitulo").css("display", "");
				$("#tablaRes").css("display","");
			
		}
	});
}




/**
 * 
 */
function eliminarCargoAbono(sid){
	var msj ="\u00bfEst\u00E1 seguro de eliminar el cargo/abono seleccionado? \n\n";

	if(confirm(msj)){	
	
		var data = {
				sidCargoAbono : sid
		};
	
	$.ajax({
			url : "eliminarCargoAbonoPend",
			type : 'POST',
			data : data
			}).done(function(data) {
				
				var numeroPagina =$("#numPagina").val();
				var cantidadRegistros = $("#cant-registros").val();
				
				if(cantidadRegistros == 1 && numeroPagina > 1){
					numeroPagina--;
				}

				buscaCargosAbonos($("#fecha-desde").val(),$("#fecha-hasta").val(),numeroPagina);
				
			});
	
	}
}



/**
 * Boton buscar pagos
 */
$("#exportar").click(function(){

	var fechaDesde = $("#fecha-desde").val();
	var fechaHasta = $("#fecha-hasta").val();
	var num_tc = $("#numerotarjeta").val();
	var tipo =$("#tipo").val();
	var estado = $("#estado").val();

	var parametros = "&fechaDesde=:ini:&fechaHasta=:fin:&numeroTarjeta=:tc:&tipoCargoAbono=:ca:&estadoCargoAbono=:estado:";
	parametros = parametros.replace(":ini:", fechaDesde);
	parametros = parametros.replace(":fin:", fechaHasta);
	parametros = parametros.replace(":tc:", num_tc);
	parametros = parametros.replace(":ca:", tipo);
	parametros = parametros.replace(":estado:", estado);

	location.href = "./exportarCargoAbonoPend?"
			+ parametros;
	
	});


