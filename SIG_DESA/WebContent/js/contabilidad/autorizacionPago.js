// CARGA INICIAL QUE LLAMA AL LOADINIT
$(cargaDatosInicial);

/**
 * Metodo que realiza la carga de los campos al iniciar
 * 
 * Monto sugerido Monto Comision de pago
 */
function cargaDatosInicial() {

	// CARGA FECHAS INICIO
	//$("#fecha-pago-desde").val(getYesterday());
	//$("#fecha-pago-hasta").val(getToday());
	
	var operador = $("#idOperador").val();
	if(typeof operador == 'undefined' ||  operador == null){
		operador = 1;
	}
	
	var parametros = "operador=:operador:"
	parametros = parametros.replace(":operador:", operador);
	$.ajax({
		url:"obtenerTipoMovAsientosAction",
		type:'POST',
		data: parametros
	}).success(function(data){
		$("#tablaRes").empty().append(data);
	});
	
	
	
	
	buscaPagos("","", operador);

}


/**
 * Boton buscar pagos
 */
$("#buscar-pagos").click(function(){
	buscaPagos($("#fecha-pago-desde").val(), $("#fecha-pago-hasta").val());
});



/**
 * 
 * Metodo que busca los pagos pendientes de autorizacion
 * 
 * @param fechaDesde
 * @param fechaHasta
 */
function buscaPagos(fechaDesde, fechaHasta, operador){
	
	if(typeof operador == 'undefined' ||  operador == null){
		operador = 1;
	}
	
	
	var parametros = "&fechaDesde=:fechaDesde:&fechaHasta=:fechaHasta:&operador=:operador:";
	parametros = parametros.replace(":fechaDesde:", fechaDesde);
	parametros = parametros.replace(":fechaHasta:", fechaHasta);
	parametros = parametros.replace(":operador:", operador);

	$.ajax({
		url : 'buscarPagosNoAutorizados',
		type : 'POST',
		data : parametros,
		success : function(data, textStatus, xhr) {		
			$("#divlistaPagos").empty().append(data);
			$("#divlistaPagos").css("display","");
		}
	});
}

/**
 * 
 * Metodo que realiza la autorizacion de un pago
 * 
 * @param sidAutorizar
 * @param fechaPago
 * 
 */
function autorizarPago(sidAutorizar , fechaPago){
	var textoConfirmacion = "\u00BF Est\u00e1 seguro que desea autorizar el pago con fecha :fecha: ?";
	textoConfirmacion = textoConfirmacion.replace(":fecha:", fechaPago);

	var fechaDesde = $("#fecha-pago-desde").val();
	var fechaHasta = $("#fecha-pago-hasta").val();
	var operador = $("#idOperador").val();

	if(confirm(textoConfirmacion)){
		$.ajax({
			url : "autorizarPagoContable",
			type : 'POST',
			data : {
				sidAutorizar :sidAutorizar ,
				fechaPago : fechaPago,
				operador: operador
			},
			success : function(data, textStatus, xhr){
				if(data.codigoError == 0 ){
					alert("Se ha autorizado el pago corretamente.");
					buscaPagos("", "", operador);	
				}else{
					alert("Ha ocurrido un error al realizar la autorizacion.");
				}
			}
		});
	}else{
		return false;
	}	
}

/**
 * 
 * Metodo para realizar el rechazo de un pago
 * 
 * @param sidAutorizar
 * @param fechaPago
 * @param montoPago
 * 
 */
function rechazarPago(sidAutorizar , fechaPago, montoPago){
	var fechaDesde = $("#fecha-pago-desde").val();
	var fechaHasta = $("#fecha-pago-hasta").val();
	var operador = $("#idOperador").val();
	var parametros = "&fechaPago=:fechaPago:&montoPago=:montoPago:&fechaDesde=':fechaDesde:'&fechaHasta=':fechaHasta:'&operador=':operador:'";

	parametros = parametros.replace(":fechaPago:", fechaPago);
	parametros = parametros.replace(":montoPago:", formato_numero(montoPago));
	parametros = parametros.replace(":fechaDesde:", fechaDesde);
	parametros = parametros.replace(":fechaHasta:", fechaHasta);
	parametros = parametros.replace(":operador:", operador);

	$.ajax({
		url : "paginas/contabilidad/popup-rechazo-pago.jsp",
		type : 'POST',
		data : parametros
	}).done(function(data) {			
		$("#popup").css("height", $(document).height());
		$("#popup").css({ display : 'block' });
		$("#popUpContenido").css({ display : 'block' });
		$("#popUpContenido").empty().append(data);
		centradoDiv('popUpContenido', 'small');
	});
}



