// CARGA INICIAL QUE LLAMA AL LOADINIT
$(cargaOperador);
$(loadInit);
/**
 * 
 * 
 * Metodo que realiza la carga de los campos al iniciar
 * Monto sugerido Monto Comision de pago
 * 
 * 
 */
function loadInit() {
	var $body = $("#app-body");
	
	// SETEA LA FECHA DE HOY
	var $fechaPago = $body.find('#div-fecha-pago').find('input[type=text]');
	$fechaPago.val(getToday());
	var operador = $('#idOperador').val();
	
	if(operador == null){
		operador = 1;
	}
	
	// BUSCA POR LA FECHA DE HOY
	buscarPagoPorFecha($fechaPago.val(), operador);
}

function cargaOperador(){
	
	$.ajax({
   			url:'cargarComboOperadores',
   			type:'POST'
   		}).done(function(data){
   			data.listaOperadores;
   			var datos = (data.listaOperadores).split("~");
   			
   			var strCombo = "";
   			
   			for(var i=1;i<datos.length-1;i++)
				{			
   				var fila = datos[i].split("|");
   				
   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
				}
   			
   			$("#idOperador").append(strCombo);
   			
   		});
}
/**
 *
 * 
 * Busca si existe un pago para un dia x
 * 
 * 
 */
function buscarPagoPorFecha(fechaPago, operador) {
	
	
	var $body = $("#app-body");
	var parametros = "&fechaPago= " + fechaPago + "&operador= " + operador;
	var $infoContable = $body.find('#info-contable');
	
	$.ajax({
		url : 'cargarSumatoriasSugeridas',
		type : 'POST',
		data : parametros,
		success : function(datos, textStatus, xhr) {
			console.log(datos);
			if (datos.codigoError == 0) {
				var incFormat = datos.sumaIncoming;
				var descFormat = datos.sumaDescuento; 
				var cpagFormat = datos.sumaCpago;

				$infoContable.find('#monto-sugerido-ic').val(incFormat); 
				$infoContable.find('#dscto-pago').val(descFormat);
				$infoContable.find('#monto-cpago').val(cpagFormat);
				$( "#btn-registrar" ).prop( "disabled", false );
				$("#file-to-upload").prop("disabled", false);
			}else{
				alert("Ha ocurrido un problema y no se han podido cargar los montos sugeridos");
			};		
		}
	});
}

/**
 * 
 * 
 * Funcion que guarda un pago
 * 
 * 
 */
$("#btn-buscar-pago").click(function(e) {
	var $body = $("#app-body");
	var $fechaPago = $body.find('#div-fecha-pago').find('input[type=text]');
	var operador = $("#idOperador").val();
	
	if(operador == null){
		operador = 2;
	}
	
	if(validarFecha($fechaPago.val())){
		buscarPagoPorFecha($fechaPago.val(), operador);
	}else{
		alert("La fecha ingresada es incorrecta...");
		$fechaPago.focus();
	}
});

/**
 *
 * 
 * Evento para guardar un pago
 * 
 * 
 */
$("#btn-registrar").click(function(e) {
	
	var $body = $("#app-body");
	var montoPagoContable = parseInt($('#monto-ingresado').val());
	var montoDescContable = parseInt($('#monto-descuento').val());
	var montoFinalContable = parseInt($('#monto-final').val());
	var fechaPago = $('#fecha-pago').val();
	var archivo_imagen = $('#file-to-upload-imagen').val();
	var archivo_excel = $('#file-to-upload-excel').val();
	var operador = $("#idOperador").val();

	/*montoPagoContable = montoPagoContable.replace(",00", "");
	montoPagoContable = parseInt(numeroSinComaPunto(montoPagoContable));
	montoDescContable = montoDescContable.replace(",00", "");
	montoDescContable = parseInt(numeroSinComaPunto(montoDescContable));
	montoFinalContable = montoFinalContable.replace(",00", "");
	montoFinalContable = parseInt(numeroSinComaPunto(montoFinalContable));*/

	if (validarPagoIngresado(montoPagoContable, montoDescContable, fechaPago, archivo_imagen, archivo_excel)) {
		var r = confirm("\u00BF Est\u00e1 seguro que desea registrar el pago?");
		if(r == true){
			guardarPago(fechaPago,montoPagoContable,montoDescContable,montoFinalContable, operador);
		}
	}
});

/**
 * Metodo que quita formato del monto mostrado en pantalla
 * conservando los decimales
 * EJ:
 * 		100.000,00 => 10000000
 * 
 * @param strMonto
 * @returns
 */
function cleanMonto(strMonto){
	// QUITA COMAS Y PUNTOS 
	var monto = numeroSinComaPunto(strMonto);

	// QUITA CEROS DECIMALES AGREGADOS POR FORMATO
	// monto = monto.substring(0,monto.length-2);

	return monto;
}

/**
 * 
 * 
 * Validar si valor ingresado esta ok
 * 
 * 
 */
function validarPagoIngresado(monto, descuento, fechaPago, archivo_imagen, archivo_excel){
	// VALIDACION PAGO
	if (null == fechaPago || fechaPago == '' || !validarFecha(fechaPago)) {
		alert("Advertencia: Debe una fecha correcta.");
		return false;
	}

	// VALIDACION MONTO
	if (monto < 0 || monto == 0) {
		alert("Advertencia: Debe ingresar un monto correcto mayor a 0.");
		return false;
	}
	
	if (monto < descuento || monto == descuento) {
		alert("Advertencia: El Descuento debe ser menor al Monto informado.");
		return false;
	}

	if (monto < descuento) {
		alert("Advertencia: El monto a pagar debe ser mayor o igual al descuento.");
		return false;
	}

	// VALIDACION DE ADJUNTO
	if( archivo_imagen == '' ){
		alert("Advertencia: Debe adjuntar una Imagen Mail.");
		return false;
	}

	return true;
}

/**
 * 
 * 
 * SE ENVIA A GUARDAR PAGO
 * 
 * 
 */
function guardarPago(fecha, monto, descuento, total, operador){
	// ARCHIVOS
	var file_data_imagen =$("#file-to-upload-imagen").prop("files")[0];
	var file_data_excel =$("#file-to-upload-excel").prop("files")[0];

	// CREA EL ARCHIVO FORM DATA
    var form_data = new FormData();

	if(!validarExtensionArchivo(file_data_imagen.name)){
		alert("Tipo de archivo adjunto 'Imagen Mail' no es permitido");
		return false;
	}

	if(file_data_excel){
		if(!validarExtensionArchivoExcel(file_data_excel.name)){
			alert("Tipo de archivo adjunto 'Detalle Mail XLS' debe ser un archivo Excel");
			return false;
		}
	}else{
		file_data_excel = "";
	}

	form_data.append("fechaPago",fecha);
	form_data.append("montoPago",monto);
	form_data.append("montoDescuento",descuento);
	form_data.append("montoFinal",total);
	form_data.append("adjuntoImagen", file_data_imagen);
	form_data.append("adjuntoExcel", file_data_excel);
	form_data.append("filenameImagen",file_data_imagen.name);
	
	if(file_data_excel != ""){
		form_data.append("filenameExcel", file_data_excel.name);
	}else{
		form_data.append("filenameExcel", "");
	}
	form_data.append("operador",operador);

	$.ajax({
		url : 'guardarPagoContable',
		type : 'POST',
		data : form_data,
		processData: false,
		contentType : false,
		enctype : 'multipart/form-data',
		success : function(datos, textStatus, xhr) {
			if(datos.codigoError == 0 ){
				alert(" Se ha guardado el pago correctamente. ");
				$("#monto-ingresado").val(0);
				document.getElementById("file-to-upload-imagen").value = "";
				document.getElementById("file-to-upload-imagen-label").innerHTML = "Imagen Mail";
				document.getElementById("file-to-upload-excel").value = "";
				document.getElementById("file-to-upload-excel-label").innerHTML = "Detalle Mail XLS";
				$("#monto-descuento").val(0);
				$('#monto-final').val(0);
				$('#monto-sugerido-ic').val(0); 
				$('#dscto-pago').val(0);
				$('#monto-cpago').val(0);
				$("#btn-registrar").prop( "disabled", false );
			}else{
				alert(datos.mensajeError );
				//$("#monto-ingresado").val("");
				document.getElementById("file-to-upload-imagen").value = "";
				document.getElementById("file-to-upload-imagen-label").innerHTML = "Imagen Mail";
				document.getElementById("file-to-upload-excel").value = "";
				document.getElementById("file-to-upload-excel-label").innerHTML = "Detalle Mail XLS";
				$("#btn-registrar").prop( "disabled", false );
				$("#file-to-upload").prop("disabled", false);
			}
		},
	});
}


/**
 * 
 * 
 *  FORMATEA EL MONTO
 * 
 * 
 */
$("#monto-ingresado").blur(function(){
	if($('#monto-ingresado').val() == ""){
		$('#monto-ingresado').val(0);
		$('#monto-final').val(0);
	}
	$('#monto-ingresado').val($('#monto-ingresado').val());
});

$("#monto-descuento").blur(function(){
	if($('#monto-descuento').val() == ""){
		$('#monto-descuento').val(0);
		$('#monto-final').val(0);
	}
	$('#monto-descuento').val($('#monto-descuento').val());
});

$("#monto-ingresado").change(function(){
	checkMontos();
});

$("#monto-descuento").change(function(){
	checkMontos();
});

function checkMontos(){
	var monto = parseInt($("#monto-ingresado").val());
	var desc = parseInt($("#monto-descuento").val());

	/*monto = monto.replace(",00", "");
	monto = parseInt(numeroSinComaPunto(monto));
	desc = desc.replace(",00", "");
	desc = parseInt(numeroSinComaPunto(desc));*/

	if(monto < desc){
		alert("El monto total mail no puede ser menor al monto de descuento.");
		$("#monto-ingresado").val(0);
		$("#monto-descuento").val(0);
		$('#monto-final').val(0);
		return false;
	}

	var total = monto - desc;
	$('#monto-final').val(total);
}

/**
 * 
 * 
 *  SE LIMPIA EL CAMPO MONTO
 * 
 * 
 */
$("#monto-ingresado").focus(function(){
	$('#monto-ingresado').val("");
});

$("#monto-descuento").focus(function(){
	$('#monto-descuento').val("");
});

$("#monto-final").focus(function(){
	var monto = $("#monto-ingresado").val();
	var desc = $("#monto-descuento").val();

	monto = monto.replace(",00", "");
	monto = parseInt(numeroSinComaPunto(monto));
	desc = desc.replace(",00", "");
	desc = parseInt(numeroSinComaPunto(desc));
	var total = monto - desc;
	$('#monto-final').val(total);
});

/**
 * 
 * 
 * SI CAMBIA LA FECHA DEL PAGO SE BORRAN LOS CAMPOS
 * 
 * 
 */ 
$("#fecha-pago").change(function(){
	$('#monto-ingresado').val(0);
	$("#monto-sugerido-ic").val(0);
	$("#monto-cpago").val(0);
	$( "#btn-registrar" ).prop( "disabled", false );
	document.getElementById("file-to-upload-imagen").value = "";
	document.getElementById("file-to-upload-imagen-label").innerHTML= "Imagen Mail";
	document.getElementById("file-to-upload-excel").value = "";
	document.getElementById("file-to-upload-excel-label").innerHTML = "Detalle Mail XLS";
});

$("#fecha-pago").blur(function(){
	$("#monto-descuento").val(0);
	$('#monto-final').val(0);
	$('#monto-ingresado').val(0);
	$('#monto-sugerido-ic').val(0); 
	$('#dscto-pago').val(0);
	$('#monto-cpago').val(0);
	$( "#btn-registrar" ).prop( "disabled", false );
	document.getElementById("file-to-upload-imagen").value = "";
	document.getElementById("file-to-upload-imagen-label").innerHTML= "Imagen Mail";
	document.getElementById("file-to-upload-excel").value = "";
	document.getElementById("file-to-upload-excel-label").innerHTML = "Detalle Mail XLS";
});
	
	
	


