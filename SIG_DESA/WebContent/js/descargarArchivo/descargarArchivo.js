// CARGA INICIAL QUE LLAMA AL LOADINIT
$(loadInit);
var operadores;
operadores = cargaOperador();
console.log(operadores);

/**
 * Metodo que realiza la carga de los campos al iniciar
 * 
 */
function loadInit() {
	// CARGA FECHAS INICIO
	$("#fechaDesde").val(getToday());

}

/**
 * Clicks buscar
 */
$("#buscarArchivos").click(buscarArchivos);

function cargaOperador(){
	var operadores = new Array();
	$.ajax({
   			url:'cargarComboOperadores',
   			type:'POST'
   		}).done(function(data){
   			data.listaOperadores;
   			var datos = (data.listaOperadores).split("~");
   			
   			var strCombo = "";
   			
   			
   			for(var i=0;i<datos.length-1;i++)
				{			
   				var fila = datos[i].split("|");
   				
   				operadores[i] = fila[2];
				}
   			
   		});
	return operadores;
}


/**
 * Buscar transacciones dada la pagina y el rango de fechas.
 */
function buscarArchivos(){
	
	
	var tipoArchivo = $("#tipo-archivo").val();
	var numPagina = $("#numPagina").val();
	var fechaDesde = $("#fechaDesde").val();
	
	if (validarCamposObligatorios(fechaDesde,tipoArchivo)) {
		
	var datos = {
			fechaBusqueda : fechaDesde,
			tipoArchivo : tipoArchivo,
			numPagina: numPagina
		};
	

		$.ajax({
			url : 'buscarArchivosDescarga',
			type : 'POST',
			data : datos,
			success : function(data, textStatus, xhr) {
				
				if(data.codigoError ==-1 ){
					alert(data.mensajeError);
				}
				
				$("#tablaRes").empty().append(data);
				$("#tituloTablaResultado").css("display", "");
				$("#tablaRes").css("display","");
				
			}
		});	
	}
}


/**
 * 
 * @param nTarjeta
 */
function validarCamposObligatorios(fechaDesde,tipoArchivo) {

	if (fechaDesde == null  ||  fechaDesde == '' || validarFecha(fechaDesde) == false) {
		alert("Debe ingresar un rango de fechas v\u00e1lido.");
		return false;
	}
	
	
	if (tipoArchivo == null || tipoArchivo == '' ) {
		alert("Debe seleccionar tipo de  archivo.");
		return false;
	}

	return true;

}
