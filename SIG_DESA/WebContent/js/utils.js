$("input.campoSalida").attr('readonly','readonly');

// AlphaNumericos
var alphanumericSet = new RegExp("^[a-zA-Z0-9]+$");
//Solo n�meros
var onlyNumberSet = /^[0-9]*$/;

// Extensiones de archivo
var ext_permitidas = ["jpg","pdf","xls","xlsx"];
var ext_permitidas_excel = ["xls","xlsx"];


function cargaURL(url)
{
	$("#contenidoDin").load(url,function(responce,status,xhr)
		{
			if(status == "error")
			{
				alert(responce);	
			}
		}
	);
}


/**
 * SE AGREGA VALIDACION PARA INPUTS SOLO NUMERICOS
 */
$(function() {
	$(".numericOnly").bind('keypress', function(e) {
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

		if (e.keyCode != 8) {
			if (onlyNumberSet.test(str)) {
				return true;
			}
			e.preventDefault();
			return false;
		}
	});
	$(".numericOnly").bind("paste", function(e) {
		removeAlphaChars(this, e);

	});
	$(".numericOnly").bind('mouseenter', function(e) {

		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (onlyNumberSet.test(str)) {
			return true;
		}
		e.preventDefault();
		return false;
	});
}); 


/**
 * SE AGREGA VALIDACION PARA ALFANUMERICOS
 */
$(function() {
	$(".alphanumeric").bind('keypress', function(e) {
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

		if (e.keyCode != 8) {
			if (alphanumericSet.test(str)) {
				return true;
			}
			e.preventDefault();
			return false;
		}
	});
	$(".alphanumeric").bind("paste", function(e) {
		removeOthersChars(this, e);

	});
	$(".alphanumeric").bind('mouseenter', function(e) {

		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (alphanumericSet.test(str)) {
			return true;
		}
		e.preventDefault();
		return false;
	});
}); 


/**
 * Remueve caracteres no numericos
 * 
 * @param txt
 * @param e
 */
function removeAlphaChars(txt, e) {
	setTimeout(function() {
		var initVal = $(txt).val();

		outputVal = initVal.replace(/[^0-9]/g, "");

		if (initVal != outputVal)
			$(txt).val(outputVal);
	}, 1);
}

/**
 * Remueve caracteres no numericos
 * 
 * @param txt
 * @param e
 */
function removeOthersChars(txt, e) {
	setTimeout(function() {
		var initVal = $(txt).val();

		outputVal = initVal.replace(/[^a-zA-Z0-9]/g, "");

		if (initVal != outputVal)
			$(txt).val(outputVal);
	}, 1);
}




function envio(action,idmenu,idsubmenu)
{
	$.ajax({
		url:action,
		type:'POST',
		data : {
			idSubmenu:idsubmenu,
			idMenu:idmenu
			},
		
	}).done(function(resp){
		$("#contenidoDin").empty();
		$("#contenidoDin").append(resp);
	}).error(function(error, status, setting){
		alert("Error1: "+error);
	});
	
	//alert(action+" "+id);
}

function logout()
{
	$.ajax({
		url:"logout",
		type:'POST'
	}).done(function(resp){
		window.location = 'login.jsp';
	}).error(function(error, status, setting){
		alert("Error2: "+status);
	});
}

function centradoDiv(id,longitud)
{
	if(longitud == "small")
	{
		$("#"+id).width("500px");
	}else if(longitud == "big")
	{
		$("#"+id).width("900px");
	}
	
	var height = ($("#"+id).height()/2*-1) + 300;
	var width = $( "#"+id ).width()/2*-1;
	
	$("#"+id).css({
		'position':'absolute','left':"50%",'top':"20%",'margin-top': '1px','margin-left':width+'px'
	});
}

function substringTooltip(cadena, largoMax)
{
	if(cadena.length > largoMax)
	{
		return cadena.substring(0,largoMax)+'<img class="flecha" src="./img/flecha.gif"></img>';
	}else{
		return cadena;
	}
}

function colocaComaNumero(numero){
	var largonumero = numero.length;
	//alert("Numero = " + numero);
	//alert("Largo  = " + largonumero);
	
	var numeroFormateado = "0.00";
	if (largonumero > 2){
		var parteEntera  = numero.substring(0,largonumero -2);
		var parteDecimal = numero.substring(largonumero -2, largonumero);
		numeroFormateado = parteEntera + "." + parteDecimal;
	}else{
		var parteDecimal = numero;
		numeroFormateado = "." + parteDecimal;
	}
	
	return numeroFormateado;
}


function formato_numero(numero){
		 var decimales = 2;
		 var separador_decimal = ",";
		 var separador_miles   = "."; 
		 
	     numero=parseFloat(numero);
	     if(isNaN(numero)){
	         return "";
	     }
	 
	    if(decimales!==undefined){
	         // Redondeamos
	         numero=numero.toFixed(decimales);
	     }
	 
	    // Convertimos el punto en separador_decimal
	     numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");
	 
	    if(separador_miles){
	         // A�adimos los separadores de miles
	         var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
	         while(miles.test(numero)) {
	             numero=numero.replace(miles, "$1" + separador_miles + "$2");
	         }
	     }
	 
	    return numero;
}

function numeroSinComaPunto(numero){
    var valor=numero.replace(/\./g,"");
	var newValor = valor.replace(/\,/g,"");
	return newValor;
}

function sacaComaPunto(numero){
	var valor=numero.replace(/\./g,"");
	var newValor = valor.replace(/\,/g,".");
	return newValor;
}

function ingresoMonto(){	
	 var key=window.event.keyCode;//codigo de tecla.
	 if (key == 46 || key == 44 || key == 13){
		 return true;
	 }
	 if (key >= 48 && key <=57){//Validacion de numero 0-9
		 return true;
	 }else{
		 window.event.keyCode=0;
	 }
}

function sacaPuntos(numero){
	var newValor=numero.replace(/\./g,"");
	return newValor;
}

function reemplazarComaPunto(numero){
	var newValor = numero.replace(/\,/g,".");;
	return newValor;
}

function ingresoFecha(){					
	 var key=window.event.keyCode;//codigo de tecla.
	 if (key == 13 || key == 47){
		 return true;
	 }
	 if (key >= 48 && key <=57){//Validacion de numero 0-9
		 return true;
	 }else{
		 window.event.keyCode=0;
	 }
}

function convertirFecha(Cadena){
var Fecha = new String(Cadena);  
    
    /** Cadena Anio **/  
    var Ano= new String(Fecha.substring(Fecha.lastIndexOf("/")+1,Fecha.length));
    var Mes= new String(Fecha.substring(Fecha.indexOf("/")+1,Fecha.lastIndexOf("/"))); 
    var Dia= new String(Fecha.substring(0,Fecha.indexOf("/")));  
    
    return Ano+""+Mes+""+Dia;
 
}

function validarFecha(Cadena){  
    
	var Fecha = new String(Cadena);  
    
    /** Cadena Anio **/  
    var Ano= new String(Fecha.substring(Fecha.lastIndexOf("/")+1,Fecha.length));
    var Mes= new String(Fecha.substring(Fecha.indexOf("/")+1,Fecha.lastIndexOf("/"))); 
    var Dia= new String(Fecha.substring(0,Fecha.indexOf("/")));  
 
    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
        return false;
    }
    
    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
        return false;
    }  
      
    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
        return false;
    }  
    
    /** Valida si es bisiesto **/
    
    var bisiesto=false;
    
    if ((Ano % 4 == 0) && (Ano % 100 != 0) || (Ano % 400 == 0)) {
    	bisiesto=true;
    	if (Mes == 2 && (parseInt(Dia, 10) < 1 || parseInt(Dia, 10) >29) ){
    		return false;
    	}
    }
    
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {
    	
    	if (bisiesto == true && Mes == 2 && Dia > 29){
    		return false;
    	}
    	
        if (Mes==2 && Dia > 28 && bisiesto == false || Dia>30) {  
            return false;
        }  
    }  
      
  	return true;
}

function exportarExcelLogProcesos(idTablaExportar, Data){
	
	//alert("Valor Tabla  = " + idTablaExportar);
	
	var filas = $("#" + idTablaExportar + " > tbody > tr").length;
	var columnas = 0;
	
	columnas = $("#"+idTablaExportar+"> tbody > tr > th").length;
    if (columnas==0) columnas = $("#"+idTablaExportar+"> tbody > tr > td").length;
	//alert("Cantidad Filas = " + filas);
	//alert("Cantidad Columnas = " + columnas);
    //*************************************//
    //* Obtener los filas de datos        *//
    //*************************************//
    for(var i=0;i<filas;i++)
	{
    	//Recorro las columnas
    	Data += "DET|";
    	for(var j=0;j<columnas;j++)
		{
    		$('#'+idTablaExportar+' tr').eq(i).each(function(){
    			
    			if($(this).find("td").eq(j).find("input:radio").parent().index() != j){
    				
    				Data += $(this).find("td").eq(j).text();
				}
    			
    			if(j < columnas-1){
	    			Data += "|";
				}
    			
    		});
		}
    	Data += "~";
    	
    	
	}
    //alert(Data);
    return Data;
}

function exportarExcel(idTablaExportar, Data){
	
	//alert("Valor Tabla  = " + idTablaExportar);
	var filas = $("#" + idTablaExportar + " > tbody > tr").length;
	var columnas = 0;
	
	columnas = $("#"+idTablaExportar+"> tbody > tr > th").length;
    if (columnas==0) columnas = $("#"+idTablaExportar+"> tbody > tr > td").length;
	console.log("Cantidad Filas = " + filas);
	//alert("Cantidad Columnas = " + columnas);
    //*************************************//
    //* Obtener los filas de datos        *//
    //*************************************//
    for(var i=0;i<filas;i++)
	{
    	//Recorro las columnas
    	Data += "DET|";
    	for(var j=1;j<columnas;j++)
		{
    		$('#'+idTablaExportar+' tr').eq(i).each(function(){
    			
    			if($(this).find("td").eq(j).find("input:radio").parent().index() != j){
    				
    				Data += $(this).find("td").eq(j).text();
				}
    			
    			if(j < columnas-1){
	    			Data += "|";
				}
    			
    		});
		}
    	Data += "~";
    	
    	
	}
    //alert(Data);
    return Data;
}

function exportarExcelConComisiones(idTablaExportar, Data){
	
	//alert("Valor Tabla  = " + idTablaExportar);
	var filas = $("#" + idTablaExportar + " > tbody > tr").length;
	var columnas = 0;
	
	columnas = $("#"+idTablaExportar+"> tbody > tr > th").length;
    if (columnas==0) columnas = $("#"+idTablaExportar+"> tbody > tr > td").length;
    //*************************************//
    //* Obtener los filas de datos        *//
    //*************************************//
    for(var i=0;i<filas;i++)
	{
    	
    	if($('#'+idTablaExportar+' > tbody > tr').eq(i).attr("id") != "breakrow"){
	    	//Recorro las columnas
	    	Data += "DET|";
	    	for(var j=0;j<columnas;j++)
			{
	    		$('#'+idTablaExportar+' > tbody > tr').eq(i).each(function(){
	    			
	    			var tr = $(this).find("tr").eq(i);
	    			//console.log(tr);	
	    			/*if(tr.attr("id") === "breakrow"){
	    				console.log(tr);
	    			}*/
	    			
	    			if($(this).find("td").eq(j).find("input:radio").parent().index() != j){
	    				var elemento = $(this).find("td").eq(j);
	    				if(elemento.attr("id")==="titulo"){
	    					Data += "~TIT|"+elemento.text();
	    				}else{
	    					Data += elemento.text();
	    				} 
	    				
					}
	    			
	    			if(j < columnas-1){
		    			Data += "|";
					}
	    			
	    		});
			}
	    	Data += "~";
    	}
	}
    return Data;
}

function exportarExcelCobroCargo(idTablaExportar, Data){
	
	//alert("Valor Tabla  = " + idTablaExportar);
	var filas = $("#" + idTablaExportar + " > tbody > tr").length;
	var columnas = 0;
	
	columnas = $("#"+idTablaExportar+"> tbody > tr > th").length;
    if (columnas==0) columnas = $("#"+idTablaExportar+"> tbody > tr > td").length;
    //*************************************//
    //* Obtener los filas de datos        *//
    //*************************************//
    for(var i=0;i<filas;i++)
	{	    
	    for(var j=0;j<columnas;j++){
	    	$('#'+idTablaExportar+' > tbody > tr').eq(i).each(function(){
	    		
	    		var tr = $(this).find("tr").eq(i);
	    			
	    		if($(this).find("td").eq(j).find("input:radio").parent().index() != j){
	    			
	    			var elemento = $(this).find("td").eq(j);
	    			
	    			if(elemento.attr("id")!="hidden"){
	    				Data += elemento.text();
		    		}	
	    			
				}
	    		if(j < columnas-1){
		    		Data += "|";
				}	
	    	});
		}
	    Data += "~";
    	
	}
    return Data;
}

function exportarExcelTC46(idTablaExportar, Data){
	
	//alert("Valor Tabla  = " + idTablaExportar);
	var filas = $("#" + idTablaExportar + " > tbody > tr").length;
	var columnas = 0;
	
	columnas = $("#"+idTablaExportar+"> tbody > tr > th").length;
    if (columnas==0) columnas = $("#"+idTablaExportar+"> tbody > tr > td").length;
    //*************************************//
    //* Obtener los filas de datos        *//
    //*************************************//
    for(var i=0;i<filas;i++)
	{
    	
    	if($('#'+idTablaExportar+' > tbody > tr').eq(i).attr("id") == "totales"){
    		Data += "TIT|";
    	}
    	else{
    		Data += "DET|";	
    	}
	    	//Recorro las columnas
	    
	    for(var j=0;j<columnas;j++){
	    	$('#'+idTablaExportar+' > tbody > tr').eq(i).each(function(){
	    			
	    		var tr = $(this).find("tr").eq(i);
	    		//console.log(tr);	
	    		/*if(tr.attr("id") === "breakrow"){
	    			console.log(tr);
	    		}*/
	    			
	    		if($(this).find("td").eq(j).find("input:radio").parent().index() != j){
	    			var elemento = $(this).find("td").eq(j);
	    			if(elemento.attr("id")==="titulo"){
	    				Data += "~TIT|"+elemento.text();
	    			}else{
	    				Data += elemento.text();
	    			} 
	    			
				}
	    		
	    		if(j < columnas-1){
		    		Data += "|";
				}
	    			
	    	});
		}
	    Data += "~";
    	
	}
    return Data;
}


function exportarExcelCompensacion(idTablaExportar, Data){
	
	//alert("Valor Tabla  = " + idTablaExportar);
	var filas = $("#" + idTablaExportar + " > tbody > tr").length;
	var columnas = 0;
	
	columnas = $("#"+idTablaExportar+"> tbody > tr > th").length;
    if (columnas==0) columnas = $("#"+idTablaExportar+"> tbody > tr > td").length;
    //*************************************//
    //* Obtener los filas de datos        *//
    //*************************************//
    for(var i=0;i<filas;i++)
	{
    	//Recorro las columnas
    	Data += "DET|";
    	for(var j=0;j<columnas;j++)
		{
    		$('#'+idTablaExportar+' > tbody > tr').eq(i).each(function(){
    			
    			if($(this).find("td").eq(j).find("input:radio").parent().index() != j){
    				var elemento = $(this).find("td").eq(j);
    				if(elemento.attr("id")==="titulo"){
    					Data += "~TIT|"+elemento.text();
    				}else{
    					Data += elemento.text();
    				}
    				
				}
    			
    			if(j < columnas-1){
	    			Data += "|";
				}
    			
    		});
		}
    	Data += "~";
	}
    return Data;
}

function exportarExcelConciliacion(idTablaExportar, Data){

	//alert("Valor Tabla  = " + idTablaExportar);
	var filas = $("#" + idTablaExportar + " > tbody > tr").length;
	var columnas = 0;
	
	columnas = $("#"+idTablaExportar+"> tbody > tr > th").length;
    if (columnas==0) columnas = $("#"+idTablaExportar+"> tbody > tr > td").length;
	//alert("Cantidad Filas = " + filas);
	//alert("Cantidad Columnas = " + columnas);
    //*************************************//
    //* Obtener los filas de datos        *//
    //*************************************//
    for(var i=0;i<filas;i++)
	{
    	//Recorro las columnas
    	Data += "DET|";
    	for(var j=0;j<columnas;j++)
		{
    		$('#'+idTablaExportar+' tr').eq(i).each(function(){
    			
    			if($(this).find("td").eq(j).find("input:radio").parent().index() != j){
    				
    				Data += $(this).find("td").eq(j).text();
				}
    			
    			if(j < columnas-1){
	    			Data += "|";
				}

    		});
		}
    	Data += "~";    	    
	}
    //alert(Data);
    return Data;
}



$("#exportExcel").click(function(){
	
	var componente = $("#componente").val();
	var idTabla = $("#idTabla").val();
	
	var filas = $("#"+idTabla+" > tbody > tr").length;

	var columnas = 0;
    $('#'+idTabla+' tr:nth-child(1) td:visible').each(function () {
        columnas++;    
    });
    
    var serie = "";
    
    for(var j=0;j<columnas;j++)
	{
    	if($(".tablaContenido tr").find("th").eq(j).html()!="SEL")
		{
    		serie += $(".tablaContenido tr").find("th").eq(j).text();
    		if(j < columnas-1)
			{
    			serie += "|";
			}else{
				serie += "~";
			}
		}
	}
    
    for(var i=0;i<filas;i++)
	{
    	for(var j=0;j<columnas;j++)
		{
    		$('#'+idTabla+' tr').eq(i).each(function(){
    			
    			if($(this).find("td").eq(j).find("input:radio").parent().index() != j)
				{
    				serie += $(this).find("td").eq(j).text();
				}
    		});
    		
    		if(j > 0 && j < columnas-1)
			{
    			serie += "|";
			}

    		if(j == columnas-1)
			{
    			if(i != filas-1)
    				serie += "~";
			}
		}
	}
    
    location.href="./exportExcelAction?serie="+serie+"&componente="+componente;
    
	/*$.ajax({
		url:"exportExcelAction",
		type:"POST",
		data:"serie="+serie+"&componente="+componente
	}).done(function(data){
		
	}).error(function(error){
		alert("Error "+error);
	});*/
});

$(document).ajaxStart(function(){
	$("#loading").show();
}).ajaxStop(function(){
	$("#loading").hide();
});

$(document).ready(function () {
	$(".hora").click(function(){
		 $(this).val("");
		 $(this).addClass("campoObligatorio");
	});
	
    $(".hora").keypress(function (evt) {
        var keycod = evt.keyCode;
        var aux = $(this).val();
        if (aux.length == 2 && keycod != 8 && keycod != 46) {
            aux += ":";
            $(this).val(aux);
        }

        if (aux.length == 5 && keycod != 8 && keycod != 46) {
            aux += ":";
            $(this).val(aux);
        }
    });

    $(".hora").blur(function () {
        var obj = $(this);

        for (var i = 0; i < 10; i++) {
            if (isNaN(obj.val().substring(i, i + 1))) {
                if (i != 2 && i != 5) {
                	obj.removeClass("campoObligatorio");
                	obj.addClass("campoError");
                    break;
                }
            } else {
                if (i == 2 || i == 5) {
                	obj.removeClass("campoObligatorio");
                	obj.addClass("campoError");
                    break;
                } else {
                	obj.removeClass("campoObligatorio");
                	obj.removeClass("campoError");
                }
            }
        }
        
        if(validarFecha(obj.val()))
    	{
    		obj.removeClass("campoObligatorio");
        	obj.removeClass("campoError");
    	}else if(obj.val() == ""){
    		obj.addClass("campoObligatorio");
        	obj.removeClass("campoError");
    	}else{
        	obj.removeClass("campoObligatorio");
        	obj.addClass("campoError");
    	}
    });
});

$(document).ready(function () {
	$(".fecha").click(function(){
		 $(this).val("");
		 $(this).addClass("campoObligatorio");
	});
	
    $(".fecha").keypress(function (evt) {
        var keycod = evt.keyCode;
        var aux = $(this).val();
        if (aux.length == 2 && keycod != 8 && keycod != 46) {
            aux += "/";
            $(this).val(aux);
        }

        if (aux.length == 5 && keycod != 8 && keycod != 46) {
            aux += "/";
            $(this).val(aux);
        }
    });

    $(".fecha").blur(function () {
        var obj = $(this);

        for (var i = 0; i < 10; i++) {
            if (isNaN(obj.val().substring(i, i + 1))) {
                if (i != 2 && i != 5) {
                	obj.removeClass("campoObligatorio");
                	obj.addClass("campoError");
                    break;
                }
            } else {
                if (i == 2 || i == 5) {
                	obj.removeClass("campoObligatorio");
                	obj.addClass("campoError");
                    break;
                } else {
                	obj.removeClass("campoObligatorio");
                	obj.removeClass("campoError");
                }
            }
        }
        
        if(validarFecha(obj.val()))
    	{
    		obj.removeClass("campoObligatorio");
        	obj.removeClass("campoError");
    	}else if(obj.val() == ""){
    		obj.addClass("campoObligatorio");
        	obj.removeClass("campoError");
    	}else{
        	obj.removeClass("campoObligatorio");
        	obj.addClass("campoError");
    	}
    });
});

/**
 * 
 * @returns {Date}
 */
function getToday() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; // January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}

	today = dd + '/' + mm + '/' + yyyy;

	return today.toString();
};

/**
 * 
 * @returns {Date}
 */
function getYesterday() {
	var today = new Date();
	today.setDate(today.getDate() - 1);
	
	var dd = today.getDate();
	var mm = today.getMonth() + 1; // January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}

	today = dd + '/' + mm + '/' + yyyy;

	return today.toString();
};


/**
 * Valida la extension de un archivo 
 * 
 * @param extension
 */
function validarExtensionArchivo(filename){
	var ext = filename.match(/\.([^\.]+)$/)[1];
	ext = ext.toLowerCase();
	if(ext_permitidas.indexOf(ext) != -1){
		return true;
	}else{
		return false;
	}
}

/**
 * Valida la extension de un archivo excel
 * 
 * @param extension
 */
function validarExtensionArchivoExcel(filename){
	var ext = filename.match(/\.([^\.]+)$/)[1];
	ext = ext.toLowerCase();
	if(ext_permitidas_excel.indexOf(ext) != -1){
		return true;
	}else{
		return false;
	}
}


/**
 * Metodo para exportar las tablas 
 */
function exportarGenerico(dataExportar, nombreArchivo ){
	
	var datos = replaceCaracterEspecial(dataExportar);
	//alert(datos);
	var parametros = {
			dataExcel : datos,
 			nombreArchivo : nombreArchivo
 		}; 			

	$.ajax({
		url : "initExport",
		type : "POST",
		data : parametros,
		dataType : "json",
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('Error ' + textStatus);
			alert(errorThrown);
			alert(XMLHttpRequest.responseText);
		},
		success : function(data) {
			// si se puede guardar ok la data , exporta
			location.href = "./exportarExcelTabla";
			setTimeout(function(){ }, 5000);
		}
	});		
}
function exportarTc46(dataExportar, nombreArchivo ){
	
	var datos = replaceCaracterEspecial(dataExportar);
	//alert(datos);
	var parametros = {
			dataExcel : datos,
 			nombreArchivo : nombreArchivo
 		}; 			

	$.ajax({
		url : "initExport",
		type : "POST",
		data : parametros,
		dataType : "json",
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('Error ' + textStatus);
			alert(errorThrown);
			alert(XMLHttpRequest.responseText);
		},
		success : function(data) {
			// si se puede guardar ok la data , exporta
			location.href = "./exportarExcelTablaTc46";
			setTimeout(function(){ }, 5000);
		}
	});		
}


function replaceCaracterEspecial(cadena){
	var cadenaAux = cadena;
	cadenaAux = cadenaAux.replace(/�/g,'a');
	cadenaAux = cadenaAux.replace(/�/g,'e');
	cadenaAux = cadenaAux.replace(/�/g,'i');
	cadenaAux = cadenaAux.replace(/�/g,'o');
	cadenaAux = cadenaAux.replace(/�/g,'u');
	cadenaAux = cadenaAux.replace(/�/g,'A');
	cadenaAux = cadenaAux.replace(/�/g,'E');
	cadenaAux = cadenaAux.replace(/�/g,'I');
	cadenaAux = cadenaAux.replace(/�/g,'O');
	cadenaAux = cadenaAux.replace(/�/g,'U');
	return cadenaAux;
}

