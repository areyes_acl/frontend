// CARGA INICIAL QUE LLAMA AL LOADINIT
$(loadInit);

/**
 * Metodo que realiza la carga de los campos al iniciar
 * 
 */
function loadInit() {
	// CARGA FECHAS INICIO
	$("#fechaDesde").val(getYesterday());
	$("#fechaHasta").val(getToday());
	cargaOperador();

}

/**
 * Clicks buscar
 */
$("#buscarTrx").click(realizarBusquedaTransacciones);

/*$("#buscarTrx").click(function(){
	console.log($('#tipoTrx').val());
});*/
		

/**
 * Click Exportar
 */
$("#exportar").click(exportarTrx);

//funcion para cargar lista de operadores en combobox
function cargaOperador(){
	$.ajax({
   			url:'cargarComboOperadores',
   			type:'POST'
   		}).done(function(data){
   			data.listaOperadores;
   			var datos = (data.listaOperadores).split("~");
   			
   			var strCombo = "";
   			var html = "";
   			
   			for(var i=1;i<datos.length-1;i++)
				{			
   				var fila = datos[i].split("|");
   				
   				strCombo += "<option value='"+fila[0]+"'>"+fila[1]+"</option>";
   				html += "<input type='hidden' id='oper"+fila[0]+"' value='"+fila[2]+"'>";
				}
   			
   			$("#idOperador").append(strCombo);
   			$("#opr").append(html);
   		});
}



/**
 * funcion para buscar trx
 */
function realizarBusquedaTransacciones() {
	
	var fechaDesde = $("#fechaDesde").val();
	var fechaHasta = $("#fechaHasta").val();
	

	if (validarCamposObligatorios(fechaDesde, fechaHasta)) {

		buscarTransacciones(fechaDesde,fechaHasta,$("#numPagina").val());

	}

}

/**
 * Buscar transacciones dada la pagina y el rango de fechas.
 */
function buscarTransacciones(fechaDesde, fechaHasta, numPagina){
	var numeroTarjeta = $("#numerotarjeta").val();
	var estado = $("#estado").val();
	var tipoTrx = $("#tipoTrx").val();
	var operador = $("#idOperador").val();
	
	
	var datos = {
			numeroTarjeta : numeroTarjeta,
			fechaDesde : fechaDesde,
			fechaHasta : fechaHasta,
			estado : estado,
			tipoTrx : tipoTrx,
			numPagina: numPagina,
			operador: operador,
		};
	console.log(datos);
		$.ajax({
			url : 'buscarTransaccionesPend',
			type : 'POST',
			data : datos,
			success : function(data, textStatus, xhr) {

				$("#tablaRes").empty().append(data);
				$("#tablaResTitulo").css("display", "");
				$("#tablaRes").css("display","");
			}
		});	
}


/**
 * 
 * @param nTarjeta
 */
function validarCamposObligatorios(fechaDesde, fechaHasta) {

	if (fechaDesde == '' || fechaDesde == null || fechaHasta == ''
			|| fechaHasta == null) {
		alert("Debe ingresar un rango de fechas v\u00e1lido.");
		return false;
	}

	return true;

}



/**
 * 
 * @param sidTransaccion
 */
function eliminarTransaccion(sidTransaccion, xKey) {
	var msj ="\u00bfEst\u00E1 seguro de eliminar transacci\u00f3n seleccionada? \n\n";
	
	if(xKey !='15_00'){
	var advertencia ="ADVERTENCIA: Si es una peticion de vale y desea anular se debe validar si existe un contracargo asociado y si este existe se debe eliminar el contracargo y despues la peticion de vale para que este no se vaya en el incoming..";
	msj = msj.concat(advertencia);
	}
	if(confirm(msj)){
		
		datos = {
				sidTransaccion : sidTransaccion,
				tipoTrx: xKey
		};
		
		$.ajax({
			url : 'eliminarTransaccionPend',
			type : 'POST',
			data : datos,
			success : function(data, textStatus, xhr) {
				if(data.codigoError == 0){
					alert(data.mensajeError);
					
					var numeroPagina =$("#numPagina").val();
					
					var cantidadRegistros = $("#cant-registros").val();
					
					if(cantidadRegistros == 1 && numeroPagina > 1){
						numeroPagina--;
					}
					
					buscarTransacciones($("#fechaDesde").val(),$("#fechaHasta").val(),numeroPagina);
					
				}else{
					alert(data.mensajeError);
				}
			}
		});
	}
}


/**
 * 
 */
function exportarTrx(){
	
	var fechaDesde = $("#fechaDesde").val();
	var fechaHasta = $("#fechaHasta").val();
	var numeroTarjeta = $("#numerotarjeta").val();
	var estado = $("#estado").val();
	var tipoTrx = $("#tipoTrx").val();
	var operador = $("#idOperador").val();
	var opr = $("#oper"+operador).val();
	
	var parametros = "&fechaDesde=:ini:&fechaHasta=:fin:&numeroTarjeta=:tc:&tipoTrx=:tipo:&estado=:estado:&operador=:oper:";
	parametros = parametros.replace(":ini:", fechaDesde);
	parametros = parametros.replace(":fin:", fechaHasta) + "&codOperador=" + opr;
	parametros = parametros.replace(":tc:", numeroTarjeta);
	parametros = parametros.replace(":tipo:", tipoTrx);
	parametros = parametros.replace(":estado:", estado);
	parametros = parametros.replace(":oper:", operador);
	location.href = "./exportarTrxPend?"
			+ parametros;
	
	
}