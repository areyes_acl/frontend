// variable global
var meses = [];


// CARGA INICIAL QUE LLAMA AL LOADINIT
$(loadInit);


function cortarDecimales(numero){
	
	var num = parseInt(numero) /100;
	
	return num.toString();
	
}


/**
 * Metodo que realiza la carga de los campos al iniciar
 * 
 */
function loadInit() {

	// CARGA FECHAS INICIO
	var date = new Date();
	var year = date.getFullYear();
    $("#ano").attr("value", year);
}




/**
 * Busqueda de reporte trimestral
 */
$("#buscar").click(function() {

	$("#exportar").attr("disabled", true);
	var date = new Date();
	var year = date.getFullYear();

	if(year >= $('#ano').val() && $('#ano').val().trim() != ''){
		var parametros = "ano=" + $('#ano').val();
		parametros += "&trimestre=" + $('#trimestre').val();
		//alert('cuadraturaTransaccionesDiariaVisaAction');
		$.ajax({
			url : "reporteTrimestralAction",
			type : "POST",
			data : parametros,
			dataType : "json",
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('Error ' + textStatus);
				alert(errorThrown);
				alert(XMLHttpRequest.responseText);
			},
			success : function(data) {
				if(data.listaTrimestral != null && data.listaTrimestral.length > 0){
					mostrarResumenTrimestral(data.listaTrimestral);
					$("#exportar").attr("disabled", false);
				}else{
					
				}
			}
		});
	}else{
		document.getElementById("ano").value = year;
		alert('La fecha ingresada es invalida!');
	}

});


/**
 * 
 * @param listaPorTrimestre
 * @returns
 */
function mostrarResumenTrimestral(listaPorTrimestre){
	
	var appendToTable= '';
	var sumaCantidades=0;
	var sumaMontos=0;
	
	appendToTable = appendToTable +obtieneCabeceraReporte();
	
	// se rellena por trimestre (columna por columna)
	var i= 0 ;
		
		// Recorre Onus por concepto
			for(var j= 0 ; j < listaPorTrimestre[i].detalleOnus.length; j++){
				sumaCantidades =  listaPorTrimestre[i].detalleOnus[j].cantidad + listaPorTrimestre[i+1].detalleOnus[j].cantidad + listaPorTrimestre[i+2].detalleOnus[j].cantidad;
				sumaMontos = parseInt(listaPorTrimestre[i].detalleOnus[j].monto) + parseInt(listaPorTrimestre[i+1].detalleOnus[j].monto) + parseInt(listaPorTrimestre[i+2].detalleOnus[j].monto);
				
				appendToTable = appendToTable +'<tr style="height: 20px;"><td class="align-middle" style="padding-left: 10px;padding-top: 5px;">ONUS</td><td class="align-middle" style="padding-left: 10px;">' +listaPorTrimestre[i].detalleOnus[j].descripcionCant+ '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' + listaPorTrimestre[i].detalleOnus[j].cantidad + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' + listaPorTrimestre[i+1].detalleOnus[j].cantidad + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' + listaPorTrimestre[i+2].detalleOnus[j].cantidad + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">'+sumaCantidades+'</td></tr>';
				appendToTable = appendToTable +'<tr style="height: 20px;"><td class="align-middle" style="padding-left: 10px;padding-top: 5px;">ONUS</td><td class="align-middle" style="padding-left: 10px;">' +listaPorTrimestre[i].detalleOnus[j].descripcionMont+ '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' +formato_numero(listaPorTrimestre[i].detalleOnus[j].monto) + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' +formato_numero(listaPorTrimestre[i+1].detalleOnus[j].monto) + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' +formato_numero(listaPorTrimestre[i+2].detalleOnus[j].monto) + '<td class="align-middle" style="text-align: left;padding-right: 5px;">'+formato_numero(sumaMontos)+'</td></tr>';
				
			};
			
		// Recorre Nacional por concepto
		for(j= 0 ; j < listaPorTrimestre[i].detalleNacional.length; j++){
			sumaCantidades =  listaPorTrimestre[i].detalleNacional[j].cantidad + listaPorTrimestre[i+1].detalleNacional[j].cantidad + listaPorTrimestre[i+2].detalleNacional[j].cantidad;
			sumaMontos = parseInt(listaPorTrimestre[i].detalleNacional[j].monto) + parseInt(listaPorTrimestre[i+1].detalleNacional[j].monto) + parseInt(listaPorTrimestre[i+2].detalleNacional[j].monto);
			
			appendToTable = appendToTable +'<tr style="height: 20px;"><td class="align-middle" style="padding-left: 10px;padding-top: 5px;">NACIONAL</td><td class="align-middle" style="padding-left: 10px;">' +listaPorTrimestre[i].detalleNacional[j].descripcionCant+ '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' + listaPorTrimestre[i].detalleNacional[j].cantidad + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' + listaPorTrimestre[i+1].detalleNacional[j].cantidad + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' + listaPorTrimestre[i+2].detalleNacional[j].cantidad +  '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">'+sumaCantidades+'</td></tr>';
			appendToTable = appendToTable +'<tr style="height: 20px;"><td class="align-middle" style="padding-left: 10px;padding-top: 5px;">NACIONAL</td><td class="align-middle" style="padding-left: 10px;">' +listaPorTrimestre[i].detalleNacional[j].descripcionMont+ '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' +formato_numero(listaPorTrimestre[i].detalleNacional[j].monto) + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' +formato_numero(listaPorTrimestre[i+1].detalleNacional[j].monto) + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' +formato_numero(listaPorTrimestre[i+2].detalleNacional[j].monto) + '<td class="align-middle" style="text-align: left;padding-right: 5px;">'+formato_numero(sumaMontos)+'</td></tr>';
				
		};
		
		// Recorre Nacional por concepto
		for(j= 0 ; j < listaPorTrimestre[i].detalleInternacional.length; j++){
			sumaCantidades =  listaPorTrimestre[i].detalleInternacional[j].cantidad + listaPorTrimestre[i+1].detalleInternacional[j].cantidad + listaPorTrimestre[i+2].detalleInternacional[j].cantidad;
			sumaMontos = parseInt(listaPorTrimestre[i].detalleInternacional[j].monto) + parseInt(listaPorTrimestre[i+1].detalleInternacional[j].monto) + parseInt(listaPorTrimestre[i+2].detalleInternacional[j].monto);
			
			appendToTable = appendToTable +'<tr style="height: 20px;"><td class="align-middle" style="padding-left: 10px;padding-top: 5px;">INTERNACIONAL</td><td class="align-middle" style="padding-left: 10px;">' +listaPorTrimestre[i].detalleInternacional[j].descripcionCant+ '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' + listaPorTrimestre[i].detalleInternacional[j].cantidad + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' + listaPorTrimestre[i+1].detalleInternacional[j].cantidad + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' + listaPorTrimestre[i+2].detalleInternacional[j].cantidad +  '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">'+sumaCantidades+'</td></tr>';
			appendToTable = appendToTable +'<tr style="height: 20px;"><td class="align-middle" style="padding-left: 10px;padding-top: 5px;">INTERNACIONAL</td><td class="align-middle" style="padding-left: 10px;">' +listaPorTrimestre[i].detalleInternacional[j].descripcionMont+ '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' +formato_numero(listaPorTrimestre[i].detalleInternacional[j].monto) + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' +formato_numero(listaPorTrimestre[i+1].detalleInternacional[j].monto) + '</td><td class="align-middle" style="text-align: left;padding-right: 5px;">' +formato_numero(listaPorTrimestre[i+2].detalleInternacional[j].monto) +  '<td class="align-middle" style="text-align: left;padding-right: 5px;">'+formato_numero(sumaMontos)+'</td></tr>';
				
		};
		
		
	
	$("#tabla-trimestral").empty().append(appendToTable);
	
};




/**
 * General la cabecera de la tabla
 */
function obtieneCabeceraReporte(){
	meses=obtenerMesesPorTrimestre($('#trimestre').val());
	// style="background-color: darkgray;"
	var header =  '<thead><tr class="text-center" style="height: 20px;"><td class="align-middle" style="padding-top: 5px;padding-left: 5px;">TIPO</td><td class="align-middle" style="padding-left: 5px;">CONCEPTO</td>';
	header = header + '<td class="align-middle" style="padding-left: 5px;">'+meses[0]+'</td><td class="align-middle" style="padding-left: 5px;">'+meses[1]+'</td><td class="align-middle" style="padding-left: 5px;">'+meses[2]+'</td>';
	header = header + '<td class="align-middle" style="padding-left: 5px;">TOTAL TRIMESTRAL</tr></thead>';
	
	return header;
}


/**
 * Obtiene los meses que estan dentro de un trimestre dado
 * 
 * @param nroSemestre
 * @returns {Array}
 */
function obtenerMesesPorTrimestre(nroSemestre){
	var meses =[];
	
	switch(nroSemestre) {
    case "1":
    	meses = ["ENERO","FEBRERO","MARZO"]; 
        break;
    case "2":
    	meses = ["ABRIL","MAYO","JUNIO"]; 
        break;
    case "3":
    	meses = ["JULIO","AGOSTO","SEPTIEMBRE"]; 
        break;
    case "4":
		meses = ["OCTUBRE","NOVIEMBRE","DICIEMBRE"]; 
		break;
	}
	
	return meses;
}






/**
 * Exportar a Excel reporte trimestral
 */
$("#exportar").click(function() {
	$("#loading").show();
					DataExportar = "";
					DatasExcel = "";

					idTablaExportar = "tabla-trimestral";
					DataExportar += "TIT|TIPO|CONCEPTO|"+meses[0]+"|"+meses[1]+"|"+meses[2]+"|TOTAL TRIMESTRAL~";
		DataExportar += exportarExcel(idTablaExportar, DatasExcel);
	
		
	
		
		exportarGenerico(DataExportar,"export");
		
	
});





