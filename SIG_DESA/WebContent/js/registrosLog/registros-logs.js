// CARGA INICIAL QUE LLAMA AL LOADINIT
$(loadInit);

/**
 * Metodo que realiza la carga de fechas por defecto
 */
function loadInit() {

	// CARGA FECHAS INICIO
	$("#fecha-busqueda").val(getToday());

}

/**
 * Boton buscar trx
 */
$("#buscar").click(function() {
	var fechaBusqueda = $("#fecha-busqueda").val();

	var tabla = $("#select-tabla-log").val();
	
	var numPagina = $("#numPagina").val();
	
	

	if (fechaBusqueda == '') {
		alert("Debe ingresar una fecha.");
		return;
	}

	if (tabla == '') {
		alert("Debe seleccionar la tabla a consultar.");
		return;
	}

	buscarRegistrosLog(fechaBusqueda, tabla, numPagina);

});

/**
 * Fecha de busqeuda de registros de log
 * 
 * @param fechaDesde
 * @param numPagina
 */
function buscarRegistrosLog(fechaBusqueda, tabla, numPagina) {

	var datos = {
		fechaBusqueda : fechaBusqueda,
		tabla : tabla,
		numPagina : numPagina
	};

	$.ajax({
		url : 'buscarRegistrosLog',
		type : 'POST',
		data : datos,
		success : function(data, textStatus, xhr) {

			$("#tablaRes").empty().append(data);
			$("#tituloTablaResultado").css("display", "");
			$("#tablaRes").css("display", "");

		}
	});
}

/**
 * Exportars
 */
$("#exportar").click(function() {

	var fechaBusqueda = $("#fecha-busqueda").val();
	var tabla = $("#select-tabla-log").val();

	var parametros = "&fechaBusqueda=:ini:&tabla=:fin:";
	parametros = parametros.replace(":ini:", fechaBusqueda);
	parametros = parametros.replace(":fin:", tabla);

	location.href = "./exportarRegistrosLog?" + parametros;

});
