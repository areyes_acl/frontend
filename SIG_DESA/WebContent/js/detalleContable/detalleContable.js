// CARGA INICIAL QUE LLAMA AL LOADINIT
//$(loadInit);

/**
 * Metodo que realiza la carga de los campos al iniciar
 * 
 * Monto sugerido Monto Comision de pago
 */
function cargarTipoMovimiento(operador, producto) {
	$("#tipoMovBusqueda").empty();
	/*if(typeof operador == 'undefined'){
		operador = 1;
	}*/
	//alert(operador);
	
	// CARGA FECHAS INICIO
	$("#fechaInicio").val(getYesterday());
	$("#fechaTermino").val(getToday());
	//$("#exportarDC").prop("disabled", true);

	/*$("#tipoMovBusqueda").change(function() {
		$("#exportarDC").prop("disabled", true);
	});

	$("#fechaInicio").change(function() {
		$("#exportarDC").prop("disabled", true);
	});

	$("#fechaTermino").change(function() {
		$("#exportarDC").prop("disabled", true);
	})*/;
	
	var parametros = "operador="+operador+"&producto="+producto; 

	$.ajax({
		url : "obtenerTipoMovAsientosAction",
		type : 'POST',
		data: parametros
	}).success(function(data) {
		if (data.listaAsientoContable) {
			var lista = data.listaAsientoContable;
			/*$('#tipoMovBusqueda').append($('<option>', {
				value : 0,
				text : 'TODOS'
			}));*/
			var anterior = "";
			for ( var i = 0; i < lista.length; i++) {
				if (anterior != lista[i].tipoMov) {
					anterior = lista[i].tipoMov;
					$('#tipoMovBusqueda').append($('<option>', {
						value : lista[i].tipoMov,
						text : lista[i].tipoMov
					}));
				}
			}
		}
	});
}

/**
 * Registros Contables
 */
$("#solicitar-registros-contables")
		.click(
				function() {
					var tipoMov = $("#tipoMovBusqueda").val();
					var operador = $("#idOperador").val();
					var producto = $("#idProducto").val();
					var fichero = $('#myTable').find('td').eq('8').text();
					
					if ($('#fechaInicio').val().length == 0) {
						alert("Debe ingresar fecha de inicio para realizar la solicitud...");
						$("#fechaInicio").focus();
						return;
					}

					if ($('#fechaTermino').val().length == 0) {
						alert("Debe ingresar fecha de termino para realizar la solicitud...");
						$("#fechaTermino").focus();
						return;
					}

					if (validarFecha($('#fechaInicio').val()) == false) {
						alert("La fecha ingresada esta incorrecta, verifiquela...");
						$("#fechaInicio").val("");
						$("#fechaInicio").focus();
						return;
					}

					if (validarFecha($('#fechaTermino').val()) == false) {
						alert("La fecha ingresada esta incorrecta, verifiquela...");
						$("#fechaTermino").val("");
						$("#fechaTermino").focus();
						return;
					}

					var fechaTermino = convertirFecha($('#fechaTermino').val());
					var fechaInicio = convertirFecha($('#fechaInicio').val());

					if (fechaInicio > fechaTermino) {
						alert("La fecha de inicio es mayor a la fecha de termino, verifiquela... ");
						$("#fechaInicio").focus();
						return;
					}

					var parametros = "&fechaInicio=:ini:&fechaTermino=:fin:&tipoMov=:mov:&operador=:operador:&producto=:producto:&fichero=:fichero:";
					parametros = parametros.replace(":ini:", $("#fechaInicio")
							.val());
					parametros = parametros.replace(":fin:", $("#fechaTermino")
							.val());
					parametros = parametros.replace(":mov:", tipoMov);
					parametros = parametros.replace(":operador:", operador);
					parametros = parametros.replace(":producto:", producto);
					parametros = parametros.replace(":fichero:", fichero);
					
					$.ajax({
						url : "solicitarDetalleContable",
						type : 'POST',
						data : parametros
					}).success(
							function(data) {
								
								buscarSolicitudes();
								inciarJob();
								$("#cantidad-registros").empty();
							});

				});

/**
 * Exportar Detalles Contables
 */
$("#exportarDC")
		.click(
				function() {
					
					var fichero = $('#myTable').find('td').eq('8').text();
					
					var parametros = "&fichero=:fichero:";
					parametros = parametros.replace(":fichero:", fichero);

					location.href = "./obtenerDetalleContableAction?"
							+ parametros;

				});


setTimeout(function(){
	inciarJob()
}, 1000);

var timer;
function inciarJob(){
	timer = setInterval(actualizacionConsultaEstado, 10000);
}


function actualizacionConsultaEstado(){
	
	var sidSolicitud = $('#myTable').find('td').eq('7').text();
	var estadoActual = $('#myTable').find('td').eq('10').text();
	
	
	if(sidSolicitud != '' && estadoActual == 'PENDIENTE'){
		sid = parseInt(sidSolicitud);
		desactivarCargando();
		$.ajax({
			url: 'consultaEstado',
			type: 'POST',
			data: {'sidConsultaEstado' : sid}
		}).done(function(data){
			activarCargando();
			if(data.estadoConsulta != 'PENDIENTE'){
				
				$("#solicitar-registros-contables").prop("disabled", false);
				$("#exportarDC").prop("disabled", false);
				$('#myTable').find('td').eq('5').text("");
				$('#myTable').find('td').eq('5').html("<span title='"+data.estadoConsulta+"'><img src='img/checked.png'  style='width:18px;height:18px;'></span>");
				$('#myTable').find('td').eq('10').text("");
				$('#myTable').find('td').eq('10').text(data.estadoConsulta);
				document.getElementById("solicitar-registros-contables").setAttribute('title', '');
				$("#cantidad-registros").empty().append("Cantidad de Registros: " + data.totalRegistros);
				
			}
			else{
				$("#solicitar-registros-contables").prop("disabled", true);
				$("#exportarDC").prop("disabled", true);
				$('#myTable').find('td').eq('5').text("");
				$('#myTable').find('td').eq('5').html("<span title='"+data.estadoConsulta+"'><div class='loading' style='display: inline;'></div></span>");
				$('#myTable').find('td').eq('10').text("");
				$('#myTable').find('td').eq('10').text(data.estadoConsulta);
				document.getElementById("solicitar-registros-contables").setAttribute('title', 'No es posible generar una solicitud, ya que tiene una pendiente');
				$("#cantidad-registros").empty();
				
			}
			
		});
		
		
		
		
	}
	else{
		clearInterval(timer);
		$("#solicitar-registros-contables").prop("disabled", false);
	}	
	
	/*$.ajax({
	
		
	})*/
	
	
}


function desactivarCargando(){
	$(document).unbind('ajaxStart');
	$(document).unbind('ajaxStop');
}

function activarCargando(){
	$(document).ajaxStart(function(){
		$("#loading").show();
	}).ajaxStop(function(){
		$("#loading").hide();
	});
}


