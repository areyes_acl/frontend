/**
 * 
 */
package exception;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class UsuarioServiceException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 2440276286562255352L;
    private String descripcion;

    public UsuarioServiceException() {
    }

    public UsuarioServiceException(String message) {
	super(message);
    }

    public UsuarioServiceException(Throwable cause) {
	super(cause);
    }

    public UsuarioServiceException(String message, Throwable cause) {
	super(message, cause);
    }

    public UsuarioServiceException(String mensaje, String descripcion) {
	super(mensaje);
	this.descripcion = descripcion;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

}