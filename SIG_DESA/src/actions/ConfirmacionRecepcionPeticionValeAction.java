package actions;

import clases.ConfirmacionRecepcionPeticionValeDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase que define el action para la generacion de confirmacion de recepcion de peticion de vales. 
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class ConfirmacionRecepcionPeticionValeAction extends ActionSupport{
	
	private String      xkey;
	private int         sidtransaccion;
	private int         sidusuario;
	private String      glosadescriptiva;
	private String      mensajegrabacion;

	public String execute() throws Exception {
		return "SUCCESS";
	}

	/**
	 * Metodo que graba la transaccion de confirmacion de recepcion de vales.
	 * @return Mensaje de exito o error de grabacion.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String grabarConfirmacionRecepcionPeticionVale(){
		
		ConfirmacionRecepcionPeticionValeDAO confirmacionRecepcionPeticionVale = new ConfirmacionRecepcionPeticionValeDAO();
		mensajegrabacion = confirmacionRecepcionPeticionVale.grabarConfirmacionRecepcionPeticionVale(xkey, sidtransaccion, sidusuario, glosadescriptiva);
		return "SUCCESS";
		
	}

	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	public String getXkey() {
		return xkey;
	}

	public void setXkey(String xkey) {
		this.xkey = xkey;
	}

	public int getSidtransaccion() {
		return sidtransaccion;
	}

	public void setSidtransaccion(int sidtransaccion) {
		this.sidtransaccion = sidtransaccion;
	}

	public int getSidusuario() {
		return sidusuario;
	}

	public void setSidusuario(int sidusuario) {
		this.sidusuario = sidusuario;
	}

	public String getGlosadescriptiva() {
		return glosadescriptiva;
	}

	public void setGlosadescriptiva(String glosadescriptiva) {
		this.glosadescriptiva = glosadescriptiva;
	}

	public String getMensajegrabacion() {
		return mensajegrabacion;
	}

	public void setMensajegrabacion(String mensajegrabacion) {
		this.mensajegrabacion = mensajegrabacion;
	}

}
