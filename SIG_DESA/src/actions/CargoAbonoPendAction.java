package actions;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import types.CargoAbonoStatusType;
import types.CargoAbonoType;
import types.SearchType;
import Beans.CargoAbonoBean;
import Beans.Usuario;
import cl.filter.FiltroCargoAbono;
import cl.util.excel.impl.ExcelCargoAbonoPend;
import cl.util.excel.service.ExcelUtils;
import clases.CargoAbonoPendDAO;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class CargoAbonoPendAction extends ActionSupport implements
		ServletRequestAware, SessionAware {

	/**
     * 
     */
	private static final long serialVersionUID = 7821723563462515319L;
	private CargoAbonoPendDAO cargAboPendDAO;
	private static final Logger log = Logger
			.getLogger(CargoAbonoPendAction.class);

	// VARIABLES PANTALLA
	private Long sidCargoAbono;
	private int codigoError;
	private String mensaje;

	// FILTROS BUSQUEDA
	private String fechaDesde;
	private String fechaHasta;
	private String numeroTarjeta;
	private String tipoCargoAbono;
	private String estadoCargoAbono;

	// LISTA PERDIDAS
	List<CargoAbonoBean> listaCargoAbono = null;
	private Integer cantidadElementos = 0;

	// VARIABLES PAGINACION
	private String numPagina;
	private String cantReg;
	private String pagActual;
	
	private Long idSubmenu;
	private Long idMenu;

	// VARIABLE DE REQUEST
	private HttpServletRequest request;

	// VARIABLE PARA EXPORT DE CSV
	private FileInputStream cargoAbonoStream;

	// VARIABLE DE SESSION
	private Map<String, Object> sessionMap;

	@Override
	public void setSession(Map<String, Object> map) {
		sessionMap = map;

	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;

	}

	public FileInputStream getCargoAbonoStream() {
		return cargoAbonoStream;
	}

	public void setCargoAbonoStream(FileInputStream cargoAbonoStream) {
		this.cargoAbonoStream = cargoAbonoStream;
	}

	public String getMensaje() {
		return mensaje;
	}

	public Long getSidCargoAbono() {
		return sidCargoAbono;
	}

	public void setSidCargoAbono(Long sidCargoAbono) {
		this.sidCargoAbono = sidCargoAbono;
	}

	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	public List<CargoAbonoBean> getListaCargoAbono() {
		return listaCargoAbono;
	}

	public void setListaCargoAbono(List<CargoAbonoBean> listaCargoAbono) {
		this.listaCargoAbono = listaCargoAbono;
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getNumPagina() {
		return numPagina;
	}

	public void setNumPagina(String numPagina) {
		this.numPagina = numPagina;
	}

	public String getCantReg() {
		return cantReg;
	}

	public void setCantReg(String cantReg) {
		this.cantReg = cantReg;
	}

	public String getPagActual() {
		return pagActual;
	}

	public void setPagActual(String pagActual) {
		this.pagActual = pagActual;
	}

	public String getTipoCargoAbono() {
		return tipoCargoAbono;
	}

	public void setTipoCargoAbono(String tipoCargoAbono) {
		this.tipoCargoAbono = tipoCargoAbono;
	}

	public String getEstadoCargoAbono() {
		return estadoCargoAbono;
	}

	public void setEstadoCargoAbono(String estadoCargoAbono) {
		this.estadoCargoAbono = estadoCargoAbono;
	}

	public Integer getCantidadElementos() {
		return cantidadElementos;
	}

	public void setCantidadElementos(Integer cantidadElementos) {
		this.cantidadElementos = cantidadElementos;
	}

	
	public Long getIdSubmenu() {
	    return idSubmenu;
	}

	public void setIdSubmenu(Long idSubmenu) {
	    this.idSubmenu = idSubmenu;
	}

	public Long getIdMenu() {
	    return idMenu;
	}

	public void setIdMenu(Long idMenu) {
	    this.idMenu = idMenu;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @since 1.X
	 */
	private FiltroCargoAbono setearFiltroCargoAbono(SearchType tipoBusqueda) {
		FiltroCargoAbono filtro = new FiltroCargoAbono();
		filtro.setFechaDesde(fechaDesde);
		filtro.setFechaHasta(fechaHasta);
		filtro.setNumeroTarjeta((numeroTarjeta != null && !numeroTarjeta
				.equalsIgnoreCase("")) ? numeroTarjeta : "0");
		filtro.setTipoBusqueda(tipoBusqueda);
		filtro.setEstado(CargoAbonoStatusType.findByXkey(estadoCargoAbono));
		filtro.setTipoCargo(CargoAbonoType.findByXkey(tipoCargoAbono));
		Usuario user = (Usuario) sessionMap.get("usuarioLog");
		filtro.setSidUsuario(user.getSid());

		return filtro;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @throws Exception
	 * @since 1.X
	 */
	public String cargosAbonosPendientesIndex(){
	    Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
	    usuarioLog.setCreacion(0);
	    usuarioLog.setLectura(0);
	    usuarioLog.setActualizacion(0);
	    usuarioLog.setEliminacion(0);

	    Integer lectura = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 1);
	    usuarioLog.setLectura(lectura);
	    Integer eliminar = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 4);
	    usuarioLog.setEliminacion(eliminar);

	    return "SUCCESS";
	}

	public String eliminar() {

		log.info("==== METODO PARA ELIMINAR CARGO/ABONO PENDIENTE =====");
		log.info("Sid cargo/abono : " + sidCargoAbono);
		try {
			// INICIALIZA EL DAO
			if (sidCargoAbono == null || sidCargoAbono == 0) {
				codigoError = -1;
				mensaje = "ERROR: no se ha seleccionado un cargo/abono v�lido para eliminar.";
			} else {
				CargoAbonoPendDAO cargoAbonoPendDAO = new CargoAbonoPendDAO();
				cargoAbonoPendDAO.eliminarCargoAbono(sidCargoAbono);
				this.codigoError = 0;
				this.mensaje = "Se ha eliminado correctamente el cargo y abono.";
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			this.codigoError = -1;
			this.mensaje = e.getMessage();
		}

		return Action.SUCCESS;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @since 1.X
	 */
	public String buscarCargoAbonos() {

		codigoError = 0;

		try {
			// INICIALIZA EL DAO
			if (cargAboPendDAO == null) {
				cargAboPendDAO = new CargoAbonoPendDAO();
			}
			FiltroCargoAbono filtro = setearFiltroCargoAbono(SearchType.CON_PAGINACION);
			log.info(filtro);

			if (numPagina == null || numPagina.isEmpty()) {
				numPagina = null;
			}

			log.info("numPagina :" + numPagina);

			listaCargoAbono = cargAboPendDAO.buscaCargosAbonos(filtro,
					numPagina);
			cantidadElementos = listaCargoAbono.size();

			request.setAttribute("listaCargoAbono", listaCargoAbono);
			request.setAttribute("ultimaPagina",
					cargAboPendDAO.getUltimaPagina());
			request.setAttribute("numPagina", cargAboPendDAO.getPagActual());
			request.setAttribute("fechaInicioPag", fechaDesde);
			request.setAttribute("fechaTerminoPag", fechaHasta);

			log.info(listaCargoAbono);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			codigoError = -1;
		}

		return Action.SUCCESS;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @since 1.X
	 */
	public String exportarCargoAbonoPend() {

		codigoError = 0;

		try {
			// INICIALIZA EL DAO
			if (cargAboPendDAO == null) {
				cargAboPendDAO = new CargoAbonoPendDAO();
			}
			FiltroCargoAbono filtro = setearFiltroCargoAbono(SearchType.SIN_PAGINACION);
			log.info(filtro);

			listaCargoAbono = cargAboPendDAO.buscaCargosAbonos(filtro);

			ExcelUtils excelUtils = new ExcelCargoAbonoPend();
			File archivo =excelUtils.export(listaCargoAbono);
			
			 this.cargoAbonoStream = new FileInputStream(archivo);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			codigoError = -1;
		}

		return Action.SUCCESS;

	}

}