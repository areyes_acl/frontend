package actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import Beans.ContableAvancesBean;
import Beans.HistoriaContableAvancesBean;
import Beans.ParamExportCnblBean;
import cl.util.Constantes;
import clases.ConsultarContableAvancesDAO;
import clases.ConsultarTrxAvancesDAO;

import com.opensymphony.xwork2.ActionSupport;

public class RegenerarArchivoContableAVTAction extends ActionSupport implements
	SessionAware {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final Logger log = Logger
	    .getLogger(RegenerarArchivoContableAVTAction.class);

    private Map<String, Object> sessionMap;
    private String fechaInicio = null;
    private String fechaFinal = null;
    private Integer estado = null;
    private Integer sid = null;
    private Integer idUser = null;
    private String fileName = null;
   
    private List<ContableAvancesBean> listaFinal = new ArrayList<ContableAvancesBean>();
    private List<HistoriaContableAvancesBean> listaFinalHContable = new ArrayList<HistoriaContableAvancesBean>();
    
    private HttpServletRequest request;
    private String resEjecucion;
    private String motivo;
        
    
    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getFechaFinal() {
	return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
	this.fechaFinal = fechaFinal;
    }

    public Integer getEstado() {
	return estado;
    }

    public void setEstado(Integer estado) {
	this.estado = estado;
    }

    public List<ContableAvancesBean> getListaFinal() {
        return listaFinal;
    }

    public void setListaFinal(List<ContableAvancesBean> listaFinal) {
        this.listaFinal = listaFinal;
    }

    public String regenerarArchivoContableAVTIndex() {
	return "SUCCESS";
    }
    
    public String execute() throws Exception {
	fechaInicio = "";
	fechaFinal = "";

	return "SUCCESS";
    }
    

    public List<HistoriaContableAvancesBean> getListaFinalHContable() {
        return listaFinalHContable;
    }

    public void setListaFinalHContable(
    	List<HistoriaContableAvancesBean> listaFinalHContable) {
        this.listaFinalHContable = listaFinalHContable;
    }

    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2019, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * M�todo principal que realiza las busquedas de archivos contables de
     * avances
     * 
     * 
     * 
     * @return
     * @since 1.X
     */

    public String buscarLogContableAvances() {
	try {

	    listaFinal = new ArrayList<ContableAvancesBean>();

	    ConsultarContableAvancesDAO consultarContableAvanceDAO = new ConsultarContableAvancesDAO();
	    // SE ENVIA FECHA INICIO Y FIN, MAS ESTADO
	    HashMap<String, List<ContableAvancesBean>> hash = consultarContableAvanceDAO
		    .buscarRegistroLogContableAvances(fechaInicio, fechaFinal, estado);

	    // Parea listas
	    if (hash != null && hash.entrySet().size() > 0) {
		obtenerLista(hash);
	    }
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	}

	return "SUCCESS";
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2019, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que obtiene las listas del informe de trx
     * 
     * 
     * @param hash
     * @since 1.X
     */
    private void obtenerLista(HashMap<String, List<ContableAvancesBean>> hash) {
	// OBTENER LISTA DE HASH
	List<ContableAvancesBean> listalog = hash.get("listContableAvances");

	obtenerContableoAvances(listalog);

	log.info("Cantidad registros  : " + listaFinal.size());

	/**
	 * Se guardan las listas en sesi�n para no tener que ir nuevamente a BD
	 * y realizar todo el proceso
	 */

	sessionMap.remove(Constantes.RTRO_CTBLE_AVA);
	sessionMap.remove(Constantes.KEY_DATA_EXCEL_CTBLE_AVA);
	sessionMap.put(Constantes.RTRO_CTBLE_AVA, listaFinal);
	sessionMap.put(Constantes.KEY_FILENAME_CTBLE_AVA, "export");

    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2019, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que obtiene los registros contables
     * 
     * 
     * 
     * @param listalog
     * @since 1.X
     */

    private void obtenerContableoAvances(
	    List<ContableAvancesBean> listalog) {

	for (ContableAvancesBean registros : listalog) {
	    listaFinal.add(registros);
	}
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2019, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * M�todo principal que busca historia contable de tbl_historia_contable por sid de tbl_log_contable
     * 
     * 
     * 
     * @return
     * @since 1.X
     */

    public String buscarInformeHistoriaContable() {

	listaFinalHContable = new ArrayList<HistoriaContableAvancesBean>();

	ConsultarContableAvancesDAO consultarContableAvancesDAO = new ConsultarContableAvancesDAO();

	getListaFinalHContable().addAll(consultarContableAvancesDAO.buscarHistoriaContable(sid));

	try {
	    request.setAttribute("listaFinalHContable", getListaFinalHContable());
	} catch (Exception e) {
	    return "SUCCESS";
	}

	return "SUCCESS";
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2019, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * M�todo principal que realiza accion para registrar en bd estado regenerar y su historia
     * 
     * 
     * 
     * @return
     * @since 1.X
     */

    public String ejecutaRegistroRegenerar() {
	setEjecutaInsert();
	getEjecutaInsert();
	return "SUCCESS";
    }
    
    public void setEjecutaInsert() {
	ConsultarContableAvancesDAO consultarContableAvancesDAO = new ConsultarContableAvancesDAO();
	
	this.resEjecucion = consultarContableAvancesDAO.insertRegistroRegenerar(sid,
		idUser, motivo);

	System.out.print(resEjecucion);
    }
    
    public String getEjecutaInsert() {
 	return resEjecucion;
     }


}
