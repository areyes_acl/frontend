package actions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import types.AccionType;
import types.ParametrosType;
import Beans.Contrasena;
import Beans.ContrasenaLogic;
import Beans.IContrasena;
import Beans.Parametros;
import Beans.Usuario;
import cl.filter.FiltroParametro;
import clases.CambioContrasenaDAO;
import clases.ParametrosDAO;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import exception.UsuarioServiceException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class CambioContrasenaAction extends ActionSupport {

    /**
     * 
     */
    private static final long serialVersionUID = 223071668264860863L;

    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(CambioContrasenaAction.class);

    private String sidusuario;
    private String passActual;
    private String passNueva1;
    private String passNueva2;
    private String rutuser;
    private Boolean mostrarOpcionRecuperarContrasena;
    private String PARAMETRO_ACTIVO = "1";

    private String cambioContrasena;

    private String error;

    private CambioContrasenaDAO ccDao;

    public CambioContrasenaAction() throws FileNotFoundException, IOException {
	// logDao = new LoginDAO();
	ccDao = new CambioContrasenaDAO();
    }

    public String execute() {
	return "SUCCESS";
    }

    public String actualizacionContrasenaIndex() {
	return "SUCCESS";
    }

    public String restaurarContrasenaActionIndex() {
	return "SUCCESS";
    }

    public String reestablecerContrasenaIndex() {
	return "SUCCESS";
    }

    public String checkRut() {
	if (rutuser.isEmpty()) {
	    return "EMPTY";
	}
	CambioContrasenaDAO cc = new CambioContrasenaDAO();
	String exito = cc.checkUsarioRut(rutuser);
	return exito;
    }

    public String exeCambioContrasena() {
	actualizarContrasena();
	getCambioContrasena();
	return "SUCCESS";
    }

    public void actualizarContrasena() {
	IContrasena proxyContrasena = new ContrasenaLogic();

	try {
	    logger.info("Actualiza contrase�a");
	    Map session = ActionContext.getContext().getSession();
	    Usuario user = (Usuario) session.get("usuarioLog");
	    Contrasena contrasena = new Contrasena();
	    contrasena.setPassword(passNueva1);
	    user.setContrasena(contrasena);
	    proxyContrasena.validarContrasena(user, AccionType.ACTUALIZACION);

	    this.cambioContrasena = ccDao.aplicarCambioContrasena(sidusuario,
		    passActual, passNueva1);
	} catch (UsuarioServiceException e) {
	    logger.error(e.getMessage(), e);
	    error = "El error es : " + e.getMessage();
	}
    }

    public String getCambioContrasena() {
	return cambioContrasena;
    }

    public String exeCrearNuevaContrasena() {
	setCrearNuevaContrasena();
	getCrearNuevaContrasena();
	return "SUCCESS";
    }

    public void setCrearNuevaContrasena() {
	
	try {
	    // proxyContrasena.validarContrasena(passNueva1,
	    // Long.valueOf(sidusuario), 0);
	    Map session = ActionContext.getContext().getSession();
	    Usuario user = (Usuario) session.get("usuarioLog");
	    Contrasena contrasena = new Contrasena();
	    contrasena.setPassword(passNueva1);
	    user.setContrasena(contrasena);
	    IContrasena proxyContrasena = new ContrasenaLogic();
	    proxyContrasena.validarContrasena(user, AccionType.NUEVO);

	    this.cambioContrasena = ccDao.crearNuevaContrasena(sidusuario,
		    passNueva1);
	} catch (UsuarioServiceException e) {
	    error = "El error es : " + e.getMessage();

	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Funcion que valida si mostrar el link de recuperacion de contrase�a
     * 
     * @since 1.X
     */
    public String validarRecContrasena() {
	mostrarOpcionRecuperarContrasena = Boolean.FALSE;

	ParametrosDAO parametrosDAO = new ParametrosDAO();
	FiltroParametro filtro = new FiltroParametro(
		ParametrosType.PREGUNTAS_SEGURIDAD.getLabel());
	Parametros parametro = parametrosDAO.buscarPorFiltro(filtro).get(0);

	if (parametro != null && PARAMETRO_ACTIVO.equals(parametro.getValor())) {
	    mostrarOpcionRecuperarContrasena = Boolean.TRUE;
	}

	return "SUCCESS";
    }

    public String getCrearNuevaContrasena() {
	return cambioContrasena;
    }

    public String getSidusuario() {
	return sidusuario;
    }

    public void setSidusuario(String sidusuario) {
	this.sidusuario = sidusuario;
    }

    public String getPassActual() {
	return passActual;
    }

    public void setPassActual(String passActual) {
	this.passActual = passActual;
    }

    public String getPassNueva1() {
	return passNueva1;
    }

    public void setPassNueva1(String passNueva1) {
	this.passNueva1 = passNueva1;
    }

    public String getPassNueva2() {
	return passNueva2;
    }

    public void setPassNueva2(String passNueva2) {
	this.passNueva2 = passNueva2;
    }

    public String getError() {
	return error;
    }

    public void setError(String error) {
	this.error = error;
    }

    public String getRutuser() {
	return rutuser;
    }

    public void setRutuser(String rutuser) {
	this.rutuser = rutuser;
    }

    public Boolean getMostrarOpcionRecuperarContrasena() {
	return mostrarOpcionRecuperarContrasena;
    }

    public void setMostrarOpcionRecuperarContrasena(
	    Boolean mostrarOpcionRecuperarContrasena) {
	this.mostrarOpcionRecuperarContrasena = mostrarOpcionRecuperarContrasena;
    }

}
