package actions;

import com.opensymphony.xwork2.ActionSupport;

import clases.TipoObjecionesDAO;

public class TipoObjecionesAction extends ActionSupport{
	
	private String listaTipoObjecion;
	
	public TipoObjecionesAction(){
		
	}
	
	/**
	 * execute
	 */
	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	public String getListaTipoObjecion()
	{
		return listaTipoObjecion;
	}
	
	public void setListaTipoObjecion()
	{
		TipoObjecionesDAO paraDAO = new TipoObjecionesDAO();
		
		this.listaTipoObjecion = paraDAO.getListaTipoObjecion();
	}
	
	public String cargarTipoObjeciones()
	{
		setListaTipoObjecion();
		getListaTipoObjecion();
		return "SUCCESS";
	}
}
