package actions;

import java.util.List;
import java.util.Vector;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import types.TransactionRadioSearchType;
import Beans.Usuario;
import cl.util.Utils;
import clases.CargoAbonoDAO;
import clases.TiposTransaccionesDAO;
import clases.TransaccionesDAO;

import com.opensymphony.xwork2.ActionSupport;

public class CargoAbonoAction extends ActionSupport implements ServletRequestAware{
	
	/**
     * 
     */
    private static final long serialVersionUID = -7123469716521196713L;

	private String listaCargoAbono;
	static final Logger log =  Logger.getLogger(CargoAbonoAction.class);
	private String sid;
	private String estado;
	private String tipoCargoAbono;
	private String moneda;
	private String montoca;
	private String insertCargoAbono;
	private String codRazon;
	private String fechaTope;
	private String sidusuario;
	private String updateCargoAbono;
	private String sumatoriaCargoAbono;
	private Long idSubmenu;
	private Long idMenu;
	private HttpServletRequest request;
	
	private CargoAbonoDAO cargoAbonoDAO = new CargoAbonoDAO();
	
	public CargoAbonoAction(){
		
	}
	
	public void setUpdateCargoAbono(String updateCargoAbono) {
		this.updateCargoAbono = updateCargoAbono;
	}

	public String getSumatoriaCargoAbono() {
		return sumatoriaCargoAbono;
	}

	public void setSumatoriaCargoAbono(String sumatoriaCargoAbono) {
		this.sumatoriaCargoAbono = sumatoriaCargoAbono;
	}

	/**
	 * CARGA INICIAL DE PANTALLA DE 
	 */
	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	public String getListaCargoAbono()
	{
		return listaCargoAbono;
	}
	
	public void setListaCargoAbono()
	{
		//CargoAbonoDAO cargoAbonoDAO = new CargoAbonoDAO();
		
		this.listaCargoAbono = cargoAbonoDAO.getListaCargoAbono();
	}
	
	public void setListaCargoAbonoMnt()
	{
		//CargoAbonoDAO cargoAbonoDAO = new CargoAbonoDAO();
		
		this.listaCargoAbono = cargoAbonoDAO.getListaCargoAbonoMnt();
	}

	public Long getIdSubmenu() {
	    return idSubmenu;
	}

	public void setIdSubmenu(Long idSubmenu) {
	    this.idSubmenu = idSubmenu;
	}

	public Long getIdMenu() {
	    return idMenu;
	}

	public void setIdMenu(Long idMenu) {
	    this.idMenu = idMenu;
	}

	public String cargosAbonosIndex(){
	    Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
	    if(usuarioLog !=  null){
		usuarioLog.setCreacion(0);
		usuarioLog.setLectura(0);
		usuarioLog.setActualizacion(0);
		usuarioLog.setEliminacion(0);

		Integer actualizacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 3);
		usuarioLog.setActualizacion(actualizacion);

		return "SUCCESS";
	    }else{
		return "FAILED";
	    }
	}

	public String cargarListaCargoAbono()
	{
		setListaCargoAbono();
		getListaCargoAbono();
		return "SUCCESS";
	}
	
	public String cargarListaCargoAbonoMnt()
	{
		setListaCargoAbonoMnt();
		getListaCargoAbono();
		return "SUCCESS";
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getTipoCargoAbono() {
		return tipoCargoAbono;
	}

	public void setTipoCargoAbono(String tipoCargoAbono) {
		this.tipoCargoAbono = tipoCargoAbono;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String monedaca) {
		this.moneda = monedaca;
	}

	public String getMontoca() {
		return montoca;
	}

	public void setMontoca(String montoca) {
		this.montoca = montoca;
	}
	
	public void setInsertCargoAbono()
	{
		this.insertCargoAbono = cargoAbonoDAO.insertCargoAbono(sid, tipoCargoAbono, moneda, montoca, sidusuario);
	}
	
	public String getInsertCargoAbono()
	{
		return this.insertCargoAbono;
	}
	
	public String exeInsertCargoAbono()
	{
		getSid();
		setInsertCargoAbono();
		getInsertCargoAbono();
		return "SUCCESS";
	}

	public String getCodRazon() {
		return codRazon;
	}

	public void setCodRazon(String codRazon) {
		this.codRazon = codRazon;
	}

	public String getFechaTope() {
		return fechaTope;
	}

	public void setFechaTope() {
		
		this.fechaTope = cargoAbonoDAO.getFechaTope(sid, codRazon);
	}
	
	public String getSidusuario() {
		return sidusuario;
	}

	public void setSidusuario(String sidusuario) {
		this.sidusuario = sidusuario;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String calculoFechaTope()
	{
		setFechaTope();
		getFechaTope();
		return "SUCCESS";
	}
	
	public String exeCambioEstado()
	{
		setUpdateCargoAbono();
		getUpdateCargoAbono();
		return "SUCCESS";
	}

	public String getUpdateCargoAbono() {
		return updateCargoAbono;
	}

	public void setUpdateCargoAbono() {
		this.updateCargoAbono = cargoAbonoDAO.cambiarEstadoCargoAbono(sid, estado);
	}
	
	
	public String obtenerSumatoriaCargoAbono() {
		try{
	   
		log.info(sid +"-"+ tipoCargoAbono +"-"+ moneda +"-"+ montoca +"-"+ sidusuario);
		HashMap<String, String> sumatorias = this.cargoAbonoDAO.obtenerSumatoriaCargoAbonosHistoricos(sid);
		String sumatoriaCargo = sumatorias.get(Utils.SUMA_CARGO_MAP_NAME);
		String sumatoriaAbono = sumatorias.get(Utils.SUMA_ABONO_MAP_NAME);
		
		Double montoSumaCargo = Utils.limpiarMonto(sumatoriaCargo);
		Double montoSumaAbono = Utils.limpiarMonto(sumatoriaAbono);
		
		log.info("montoSumaCargo : "+ montoSumaCargo );
		log.info("montoSumaAbono : "+ montoSumaAbono ); 
		
		sumatoriaCargoAbono =String.valueOf(montoSumaCargo - montoSumaAbono);
		log.info("montoSumaAbono : "+ sumatoriaCargoAbono );
		
		}catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return "SUCCESS";
	}

	public void setServletRequest(HttpServletRequest httpServletRequest) {
	    this.request = httpServletRequest;
	}
}
