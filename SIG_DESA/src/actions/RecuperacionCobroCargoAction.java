package actions;

import java.math.BigDecimal;

import clases.RecuperacionCobroCargoDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase generadora de recuperacion de cobro de cargos.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class RecuperacionCobroCargoAction extends ActionSupport{
	
	private String      listaFunciones;
	private String      listaRazones;
	private String      codigoTransaccion;
	private int         sid;
	
	private String      vmit;
	private int         sidtransaccion;
	private String      codigofuncion;
	private String      codigorazon; 
    private int         sidacciontransac;
	private String      codigomoneda;
	private BigDecimal  montotransaccion;
	private BigDecimal  montooriginaltrx;
	private String      glosadescriptiva;
	private String      mensajegrabacion;
	private int         sidusuario;

	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	
	/**
	 * Metodo que consulta las funciones para recuperacion cobro de cargo.
	 * @return  Lista de funciones de recuperacion cobro cargo.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarFuncionesRecuperacionCobroCargo(){
		RecuperacionCobroCargoDAO recuperacionCobroCargo = new RecuperacionCobroCargoDAO();
		listaFunciones = recuperacionCobroCargo.buscarFuncionesRecuperacionCargo(codigoTransaccion);
		return "SUCCESS";
	}
	
	/**
	 * Metodo que consulta las razones para la recuperacion de cobro de cargos.
	 * @return  Lista de razones de recuperacion cobro cargo.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarRazonesFuncionRecuperacionCobro(){
		RecuperacionCobroCargoDAO recuperacionCobroCargo = new RecuperacionCobroCargoDAO();
		listaRazones = recuperacionCobroCargo.buscarRazonesFuncionRecuperacionCobro(sid);
		return "SUCCESS";
	}
	
	/**
	 * Metodo que graba la transaccion de recuperacion de cobro de cargos.
	 * @return Mensaje de grabacion.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String grabarRecuperacionCobroCargo(){
		
		RecuperacionCobroCargoDAO recuperacionCobroCargo = new RecuperacionCobroCargoDAO();
		mensajegrabacion = recuperacionCobroCargo.grabarRecuperacionCobroCargo(vmit, sidtransaccion, codigofuncion, codigorazon, sidacciontransac, codigomoneda, montotransaccion, montooriginaltrx, glosadescriptiva, sidusuario);
		return "SUCCESS";
	}
	
	
	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	public int getSidusuario() {
		return sidusuario;
	}

	public void setSidusuario(int sidusuario) {
		this.sidusuario = sidusuario;
	}
	
	public String getListaFunciones() {
		return listaFunciones;
	}

	public void setListaFunciones(String listaFunciones) {
		this.listaFunciones = listaFunciones;
	}

	public String getListaRazones() {
		return listaRazones;
	}

	public void setListaRazones(String listaRazones) {
		this.listaRazones = listaRazones;
	}

	public String getCodigoTransaccion() {
		return codigoTransaccion;
	}

	public void setCodigoTransaccion(String codigoTransaccion) {
		this.codigoTransaccion = codigoTransaccion;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getVmit() {
		return vmit;
	}

	public void setVmit(String vmit) {
		this.vmit = vmit;
	}

	public int getSidtransaccion() {
		return sidtransaccion;
	}

	public void setSidtransaccion(int sidtransaccion) {
		this.sidtransaccion = sidtransaccion;
	}

	public String getCodigofuncion() {
		return codigofuncion;
	}

	public void setCodigofuncion(String codigofuncion) {
		this.codigofuncion = codigofuncion;
	}

	public String getCodigorazon() {
		return codigorazon;
	}

	public void setCodigorazon(String codigorazon) {
		this.codigorazon = codigorazon;
	}

	public int getSidacciontransac() {
		return sidacciontransac;
	}

	public void setSidacciontransac(int sidacciontransac) {
		this.sidacciontransac = sidacciontransac;
	}

	public String getCodigomoneda() {
		return codigomoneda;
	}

	public void setCodigomoneda(String codigomoneda) {
		this.codigomoneda = codigomoneda;
	}

	public BigDecimal getMontotransaccion() {
		return montotransaccion;
	}

	public void setMontotransaccion(BigDecimal montotransaccion) {
		this.montotransaccion = montotransaccion;
	}

	public BigDecimal getMontooriginaltrx() {
		return montooriginaltrx;
	}

	public void setMontooriginaltrx(BigDecimal montooriginaltrx) {
		this.montooriginaltrx = montooriginaltrx;
	}

	public String getGlosadescriptiva() {
		return glosadescriptiva;
	}

	public void setGlosadescriptiva(String glosadescriptiva) {
		this.glosadescriptiva = glosadescriptiva;
	}

	public String getMensajegrabacion() {
		return mensajegrabacion;
	}

	public void setMensajegrabacion(String mensajegrabacion) {
		this.mensajegrabacion = mensajegrabacion;
	}

}