package actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import types.StatusPagoType;
import Beans.PagoContableBean;
import Beans.ParametroBean;
import Beans.Usuario;
import cl.util.Utils;
import clases.CommonsDAO;
import clases.PagoDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author afreire
 * 
 */
public class AutorizarPagoAction extends ActionSupport implements SessionAware, ServletRequestAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4740040611822706906L;
	static final Logger log = Logger.getLogger(AutorizarPagoAction.class);
	private Map<String, Object> sessionMap;
	private static final String PARAMETRO_RUTA = "PATH_FILENAME_PAGO";

	// VARIABLES PARA BUSQUEDA DE PAGOS
	private String fechaDesde;
	private String fechaHasta;
	private List<PagoContableBean> listaPagos;
	private int codigoError;

	// VARIABLES PARA DESCARGA DE ARCHIVO
	private InputStream inputStream;
	private String fileName;
	private long contentLength;
	private List<ParametroBean> listaParametros;

	// VARIABLE PARA LA AUTORIZACION/RECHAZO DE UN PAGO
	private Long sidAutorizar;
	private String fechaPago;
	private String motivoRechazo;

	// VARIABLES PARA RENDER TABLA
	private HttpServletRequest request;
	private Long idSubmenu;
	private Long idMenu;
	
	//OPERADOR
	private int operador;
	
	/**
	 * 
	 * GETTERS Y SETTERS
	 * 
	 * 
	 * @return
	 */

	public String getFileName() {
		return fileName;
	}

	public Long getIdSubmenu() {
	    return idSubmenu;
	}

	public void setIdSubmenu(Long idSubmenu) {
	    this.idSubmenu = idSubmenu;
	}

	public Long getIdMenu() {
	    return idMenu;
	}

	public void setIdMenu(Long idMenu) {
	    this.idMenu = idMenu;
	}

	public String getMotivoRechazo() {
		return motivoRechazo;
	}

	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public long getContentLength() {
		return contentLength;
	}

	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public List<PagoContableBean> getListaPagos() {
		return listaPagos;
	}

	public void setListaPagos(List<PagoContableBean> listaPagos) {
		this.listaPagos = listaPagos;
	}

	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	@Override
	public void setServletRequest(HttpServletRequest httpServlet) {
		this.request = httpServlet;

	}

	public Long getSidAutorizar() {
		return sidAutorizar;
	}

	public void setSidAutorizar(Long sidAutorizar) {
		this.sidAutorizar = sidAutorizar;
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String autorizarPagosIndex(){
	    Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
	    if(usuarioLog !=  null){
		usuarioLog.setCreacion(0);
		usuarioLog.setLectura(0);
		usuarioLog.setActualizacion(0);
		usuarioLog.setEliminacion(0);

		Integer actualizacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 3);
		usuarioLog.setActualizacion(actualizacion);

		return "SUCCESS";
	    }else{
		return "FAILED";
	    }
	}

	/**
	 * 
	 * 
	 * 
	 * Buscar pagos no autorizados en el sistema
	 * 
	 * 
	 * 
	 */
	public String buscarPagosNoAutorizados() {
	    log.info("======== BUSCA PAGOS NO AUTORIZADOS =======");
	    log.info("==  ESTADO      = " + StatusPagoType.INGRESADO);

	    try {
		PagoDAO pagoDAO = new PagoDAO();
		listaPagos = pagoDAO.buscarPago(fechaDesde, fechaHasta, StatusPagoType.INGRESADO, operador);
		
		log.info("lista Pagos : " + listaPagos);

		request.setAttribute("listaPagos", listaPagos);
		codigoError = 0;

		Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
		if(usuarioLog !=  null){
		    return "SUCCESS";
		}else{
		    return "FAILED";
		}
	    } catch (Exception e) {
		log.error(e.getMessage(), e);
		codigoError = -1;
	    }

	    return "SUCCESS";
	}

	/**
	 * 
	 * Metodo encargado de recibir un SID y realiza la autorizacion de un pago
	 * 
	 * 
	 * @return
	 */
	public String autorizarPago() {
	    log.info("======== AUTORIZA PAGO =======");
	    log.info("==  SID          = " + sidAutorizar);
	    log.info("==  FECHA PAGO   = " + fechaPago);

	    try {
		Long sidUsuario = (Long) sessionMap.get("sidusuario");

		// SETEA DATOS DEL USUARIO AUTORIZADOR
		PagoContableBean pago = new PagoContableBean();
		pago.setFechaPago(fechaPago);
		pago.setSidUsuario(sidUsuario);
		pago.setEstadoPago(StatusPagoType.AUTORIZADO);
		pago.setMontoBruto((long) 0);
		pago.setMontoDescuento((long) 0);
		pago.setMontoFinal((long) 0);
		pago.setOperador(operador);

		// DAO
		PagoDAO pagoDAO = new PagoDAO();
		pagoDAO.gestionarPago(pago);
	    } catch (Exception e) {
		log.error(e.getMessage(), e);
		codigoError = -1;
	    }

	    return "SUCCESS";
	}

	/**
	 * 
	 * Metodo que permite rechazar un pago
	 * 
	 * 
	 * @return
	 */
	public String rechazarPago() {
	    try {
		log.info("======== RECHAZAR PAGO =======");
		log.info("==  FECHA PAGO     = " + fechaPago);
		log.info("==  MOTIVO RECHAZO = " + motivoRechazo);

		codigoError = 0;
		Long sidUsuario = (Long) sessionMap.get("sidusuario");

		// SETEA DATOS DEL USUARIO AUTORIZADOR
		PagoContableBean pagoRechazado = new PagoContableBean();
		pagoRechazado.setFechaPago(fechaPago);
		pagoRechazado.setMotivoRechazo(motivoRechazo);
		pagoRechazado.setSidUsuario(sidUsuario);
		pagoRechazado.setEstadoPago(StatusPagoType.RECHAZADO);
		pagoRechazado.setMontoBruto((long) 0);
		pagoRechazado.setMontoDescuento((long) 0);
		pagoRechazado.setMontoFinal((long) 0);
		pagoRechazado.setOperador(operador);

		// GESTIONA EL PAGO DAO
		PagoDAO pagoDAO = new PagoDAO();
		pagoDAO.gestionarPago(pagoRechazado);
	    } catch (Exception e) {
		log.error(e.getMessage(), e);
		codigoError = -1;
	    }

	    return "SUCCESS";
	}

	/**
	 * 
	 * METODO PARA DESCARGAR EL ARCHIVO ADJUNTO
	 * 
	 * 
	 * @return
	 */
	public String download() {
		try {
			log.info("======== DESCARGAR PAGO =======");
	
			// CARGA LA LISTA DE PARAMETROS
			cargaListaParametros(Utils.GRUPO_PRTS_PAGO);
			
			// BUSCA RUTA EN LA PRTS DONDE SE DEJARA EL ARCHIVO TEMPORAL
			String ruta = buscarPorNombreParametro(PARAMETRO_RUTA);
			
			log.info("==  FILENAME     = " + fileName);
			log.info("==  RUTA         = " + ruta);
			
			File fileToDownload = new File(ruta.concat(fileName));

			inputStream = new FileInputStream(fileToDownload);
			fileName = fileToDownload.getName();
			contentLength = fileToDownload.length();

		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
			return "SUCCESS";
		
		}
			return "SUCCESS";
		
		
	}

	/**
	 * Metodo que busca dentro de la lista de parametros el nombre de el
	 * parametro y retorna su valor asociado
	 * 
	 * @return
	 */
	private String buscarPorNombreParametro(String nombreParametro) {

		for (ParametroBean parametro : listaParametros) {

			if (parametro.getCodDato().equalsIgnoreCase(nombreParametro)) {
				return parametro.getValor();
			}
		}

		return null;
	}

	/**
	 * 
	 * Metodo que carga la lista de parametros dado un grupo
	 */
	private void cargaListaParametros(String codGrupo) {
		CommonsDAO commonDao = new CommonsDAO();

		try {
			listaParametros = commonDao.obtenerParametrosPorGrupo(codGrupo);
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}
	}

	public int getOperador() {
	    return operador;
	}

	public void setOperador(int operador) {
	    this.operador = operador;
	}
}
