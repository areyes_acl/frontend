package actions;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import types.StatusPagoType;
import Beans.PagoContableBean;
import Beans.ParametroBean;
import Beans.Usuario;
import cl.util.Utils;
import clases.CommonsDAO;
import clases.GestionTrxDAO;
import clases.PagoDAO;
import clases.ParametrosDAO;
import clases.RptCmpnscPagoDAO;
import clases.TC46DAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author afreire
 * 
 */

public class ReporteTc46IndexAction extends ActionSupport{

    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    static final Logger log = Logger.getLogger(RegistrarPagoAction.class);
    private String fechaDesde;
    private String fechaHasta;
    private int operador;
    private String listaTC46;
    
    
    
    public String reporteTc46Index(){
	return "SUCCESS";
    }
    
    public String cargarTC46(){
	setListaTC46();
	getListaTC46();
	return "SUCCESS";
    }
    
    public String getListaTC46(){
	return listaTC46;
    }
	
    public void setListaTC46(){
	TC46DAO tc46DAO = new TC46DAO();		
	this.listaTC46 = tc46DAO.getListaTc46(fechaDesde, fechaHasta, operador);
	log.info(fechaDesde +' '+ fechaHasta + ' ' + operador);
    }

    public String getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(String fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public String getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(String fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public int getOperador() {
        return operador;
    }

    public void setOperador(int operador) {
        this.operador = operador;
    }
    
    
    
}
