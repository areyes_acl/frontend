package actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import types.CargoAbonoStatusType;
import types.MoneyType;
import Beans.CargaMasivaDTO;
import Beans.CargoAbonoDTO;
import cl.util.Constantes;
import cl.util.Utils;
import clases.ConsultarTrxAvancesDAO;
import clases.Solicitud60MasivaDAO;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

import exception.AppException;

public class ConciliacionAvancesAction extends ActionSupport implements
	SessionAware {
    /**
     * 
     */
    private static final Logger log = Logger
	    .getLogger(ConciliacionAvancesAction.class);

    private static final long serialVersionUID = -8991874868536430786L;
    private List<TransaccionesAvances> listaFinalAvances = new ArrayList<TransaccionesAvances>();
    private List<TransaccionesAvancesDetalleBice> listaFinalBice = new ArrayList<TransaccionesAvancesDetalleBice>();
    private List<TransaccionesAvancesDetalleIc> listaFinalIC = new ArrayList<TransaccionesAvancesDetalleIc>();
    private List<TransaccionesDetalleGestion> listaFinalGestion = new ArrayList<TransaccionesDetalleGestion>();
    private List<TransaccionesAvancesDetalleCanal> listaFinalAvance = null;
    private List<Long> listaSeleccionada;
    private String fechaInicio = null;
    private String fechaFinal = null;
    private Integer sidAvances = null;
    private Integer sidBice = null;
    private Integer sidOnus = null;
    private Integer sidConci = null;
    private String comentario = null;
    private Integer gestion = null;
    private Integer idUser = null;
    private Map<String, Object> sessionMap;
    private Long sumaMontoTrxPar = null;
    private Long sumaMontoTrxNoPar = null;
    private HttpServletRequest request;
    private String resEjecucion;
    private String listaTipoEstado;
    private String listaDetalleEstado;
    private Integer sidEstado = null;
    private Integer estado = null;
    private Integer estadoDetalle = null;
    private Integer estadoGestion = null;
    private int codigoError;
    private String mensajeError;

    private Long gestionMasivo;
    private Long idUserMasivo;

    public String getMensajeError() {
	return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
	this.mensajeError = mensajeError;
    }

    public Long getGestionMasivo() {
	return gestionMasivo;
    }

    public void setGestionMasivo(Long gestionMasivo) {
	this.gestionMasivo = gestionMasivo;
    }

    public Long getIdUserMasivo() {
	return idUserMasivo;
    }

    public void setIdUserMasivo(Long idUserMasivo) {
	this.idUserMasivo = idUserMasivo;
    }

    public Long getSumaMontoTrxPar() {
	return sumaMontoTrxPar;
    }

    public void setSumaMontoTrxPar(Long sumaMontoTrxPar) {
	this.sumaMontoTrxPar = sumaMontoTrxPar;
    }

    public Long getSumaMontoTrxNoPar() {
	return sumaMontoTrxNoPar;
    }

    public void setSumaMontoTrxNoPar(Long sumaMontoTrxNoPar) {
	this.sumaMontoTrxNoPar = sumaMontoTrxNoPar;
    }

    public Integer getEstado() {
	return estado;
    }

    public void setEstado(Integer estado) {
	this.estado = estado;
    }

    public String getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getFechaFinal() {
	return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
	this.fechaFinal = fechaFinal;
    }

    public List<TransaccionesAvances> getListaFinalAvances() {
	return listaFinalAvances;
    }

    public void setListaFinalAvances(
	    List<TransaccionesAvances> listaFinalAvances) {
	this.listaFinalAvances = listaFinalAvances;
    }

    public List<TransaccionesDetalleGestion> getListaFinalGestion() {
	return listaFinalGestion;
    }

    public void setListaFinalGestion(
	    List<TransaccionesDetalleGestion> listaFinalGestion) {
	this.listaFinalGestion = listaFinalGestion;
    }

    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;

    }

    public String ConciliacionAvancesIndex() {
	return "SUCCESS";
    }

    public List<TransaccionesAvancesDetalleBice> getListaFinalBice() {
	return listaFinalBice;
    }

    public void setListaFinalBice(
	    List<TransaccionesAvancesDetalleBice> listaFinalBice) {
	this.listaFinalBice = listaFinalBice;
    }

    public List<TransaccionesAvancesDetalleIc> getListaFinalIC() {
	return listaFinalIC;
    }

    public void setListaFinalIC(List<TransaccionesAvancesDetalleIc> listaFinalIC) {
	this.listaFinalIC = listaFinalIC;
    }

    public List<TransaccionesAvancesDetalleCanal> getListaFinalAvance() {
	return listaFinalAvance;
    }

    public void setListaFinalAvance(
	    List<TransaccionesAvancesDetalleCanal> listaFinalAvance) {
	this.listaFinalAvance = listaFinalAvance;
    }

    public List<Long> getListaSeleccionada() {
	return listaSeleccionada;
    }

    public void setListaSeleccionada(List<Long> listaSeleccionada) {
	this.listaSeleccionada = listaSeleccionada;
    }

    public Integer getSidAvances() {
	return sidAvances;
    }

    public void setSidAvances(Integer sidAvances) {
	this.sidAvances = sidAvances;
    }

    public Integer getSidBice() {
	return sidBice;
    }

    public void setSidBice(Integer sidBice) {
	this.sidBice = sidBice;
    }

    public Integer getSidOnus() {
	return sidOnus;
    }

    public void setSidOnus(Integer sidOnus) {
	this.sidOnus = sidOnus;
    }

    public Integer getSidConci() {
	return sidConci;
    }

    public void setSidConci(Integer sidConci) {
	this.sidConci = sidConci;
    }

    public String getComentario() {
	return comentario;
    }

    public void setComentario(String comentario) {
	this.comentario = comentario;
    }

    public Integer getGestion() {
	return gestion;
    }

    public void setGestion(Integer gestion) {
	this.gestion = gestion;
    }

    public Integer getIdUser() {
	return idUser;
    }

    public void setIdUser(Integer idUser) {
	this.idUser = idUser;
    }

    public Integer getSidEstado() {
	return sidEstado;
    }

    public void setSidEstado(Integer sidEstado) {
	this.sidEstado = sidEstado;
    }

    public Integer getEstadoDetalle() {
	return estadoDetalle;
    }

    public void setEstadoDetalle(Integer estadoDetalle) {
	this.estadoDetalle = estadoDetalle;
    }

    public Integer getEstadoGestion() {
	return estadoGestion;
    }

    public void setEstadoGestion(Integer estadoGestion) {
	this.estadoGestion = estadoGestion;
    }

    public String execute() throws Exception {
	fechaInicio = "";
	fechaFinal = "";

	return "SUCCESS";
    }

    public String informeCompensacionIndex() {
	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * M�todo principal que realiza las busquedas para el informe de LOG
     * 
     * 
     * 
     * @return
     * @since 1.X
     */

    public String buscarInformeLogTrxAvances() {
	try {

	    listaFinalAvances = new ArrayList<TransaccionesAvances>();
	    sumaMontoTrxNoPar = 0L;
	    sumaMontoTrxPar = 0L;

	    ConsultarTrxAvancesDAO consultarTrxAvanceDAO = new ConsultarTrxAvancesDAO();
	    // SE ENVIA FECHA INICIO Y FIN, MAS ESTADO
	    HashMap<String, List<TransaccionesAvances>> hash = consultarTrxAvanceDAO
		    .buscarInformeTrxAvances(fechaInicio, fechaFinal, estado,
			    estadoDetalle, estadoGestion);

	    // Parea listas
	    if (hash != null && hash.entrySet().size() > 0) {
		obtenerInforme(hash);
	    }
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	}

	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que obtiene las listas del informe de trx
     * 
     * 
     * @param hash
     * @since 1.X
     */
    private void obtenerInforme(HashMap<String, List<TransaccionesAvances>> hash) {
	// OBTENER LISTA DE HASH
	List<TransaccionesAvances> listalog = hash.get("listaTrxAvances");

	obtenerResumenInformadoAvances(listalog);

	log.info("Cantidad trx2  : " + listaFinalAvances.size());

	/**
	 * Se guardan las listas en sesi�n para no tener que ir nuevamente a BD
	 * y realizar todo el proceso
	 */

	sessionMap.remove(Constantes.TRX_AVA);
	sessionMap.remove(Constantes.KEY_FILENAME_AVA);
	sessionMap.put(Constantes.TRX_AVA, listaFinalAvances);
	sessionMap.put(Constantes.KEY_FILENAME_AVA, "export");

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que obtiene las trx informadas por avances
     * 
     * 
     * 
     * @param listalog
     * @since 1.X
     */

    private void obtenerResumenInformadoAvances(
	    List<TransaccionesAvances> listalog) {

	for (TransaccionesAvances trxAutor : listalog) {
	    listaFinalAvances.add(trxAutor);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * M�todo principal que realiza la busqueda de detalle de trx
     * 
     * 
     * 
     * @return
     * @since 1.X
     */

    public String buscarInformeLogTrxDetalle() {

	listaFinalBice = new ArrayList<TransaccionesAvancesDetalleBice>();
	listaFinalIC = new ArrayList<TransaccionesAvancesDetalleIc>();
	listaFinalAvance = new ArrayList<TransaccionesAvancesDetalleCanal>();

	ConsultarTrxAvancesDAO consultarTrxAvanceDAO = new ConsultarTrxAvancesDAO();

	// crear lista IC, BICE, CANAL(AVANCES)
	getListaFinalBice().addAll(
		consultarTrxAvanceDAO.buscarDetalleBice(sidBice));
	getListaFinalIC()
		.addAll(consultarTrxAvanceDAO.buscarDetalleIc(sidOnus));
	getListaFinalAvance().addAll(
		consultarTrxAvanceDAO.buscarDetalleCanal(sidAvances));

	// enviar a la vista
	try {
	    request.setAttribute("listaFinalIC", getListaFinalIC());
	    request.setAttribute("listaFinalAvance", getListaFinalAvance());
	    request.setAttribute("listaFinalBice", getListaFinalBice());
	} catch (Exception e) {
	    return "SUCCESS";
	}

	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * M�todo principal que realiza la busqueda dehistorico de gestion por trx
     * 
     * 
     * 
     * @return
     * @since 1.X
     */

    public String buscarInformeLogGestion() {

	listaFinalGestion = new ArrayList<TransaccionesDetalleGestion>();

	ConsultarTrxAvancesDAO consultarTrxAvanceDAO = new ConsultarTrxAvancesDAO();

	// crear lista detalle de gestion
	getListaFinalGestion().addAll(
		consultarTrxAvanceDAO.buscarDetalleGestion(sidConci));

	// enviar a la vista
	try {
	    request.setAttribute("listaFinalGestion", getListaFinalGestion());
	} catch (Exception e) {
	    return "SUCCESS";
	}

	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * M�todo principal que realiza el insert de gestion para la transaccion
     * 
     * 
     * 
     * @return
     * @since 1.X
     */

    public String ejecutaInsert() {
	setEjecutaInsert();
	getEjecutaInsert();
	return "SUCCESS";
    }

    public void setEjecutaInsert() {
	ConsultarTrxAvancesDAO consultarTrxAvanceDAO = new ConsultarTrxAvancesDAO();

	this.resEjecucion = consultarTrxAvanceDAO.insertGestion(sidConci,
		comentario, gestion, idUser);
	System.out.print(resEjecucion);
    }

    public String getEjecutaInsert() {
	return resEjecucion;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * obtener tipo de estado
     * 
     * 
     * 
     * @return
     * @since 1.X
     */

    public String cargarTipoEstado() {
	setListaTipoEstado();
	getListaTipoEstado();
	return "SUCCESS";
    }

    public String getListaTipoEstado() {
	return listaTipoEstado;
    }

    public void setListaTipoEstado() {
	ConsultarTrxAvancesDAO consultarTrxAvanceDAO = new ConsultarTrxAvancesDAO();
	this.listaTipoEstado = consultarTrxAvanceDAO.getListaTipoEstado();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * obtener detalle de estado
     * 
     * 
     * 
     * @return
     * @since 1.X
     */

    public String cargarDetalleEstado() {
	setListaDetalleEstado();
	getListaDetalleEstado();
	return "SUCCESS";
    }

    public String getListaDetalleEstado() {
	return listaDetalleEstado;
    }

    public void setListaDetalleEstado() {
	ConsultarTrxAvancesDAO consultarTrxAvanceDAO = new ConsultarTrxAvancesDAO();
	this.listaDetalleEstado = consultarTrxAvanceDAO
		.getListaDetalleEstado(sidEstado);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/11/2018, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @throws AppException
     * @since 1.X
     */

    public String ejecutaInsertMasivo() {
	ConsultarTrxAvancesDAO consultar = null;
	try {

	    List<CargaMasivaDTO> listaTodo = crearLista();
	    consultar = new ConsultarTrxAvancesDAO();
	    consultar.guardarCargoMasivo(listaTodo);

	    mensajeError = "Se ha guardado correctamente la gesti\u00F3n de los trx`s seleccionados.";

	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    if (e.getMessage().equals("EXISTE")) {
		mensajeError = "Para una de las transacciones seleccionadas ya se t\u00E9rmino la gesti\u00F3n, favor reintentar nuevamente.";
	    }
	    return "SUCCESS";
	}

	return "SUCCESS";

    }

    private List<CargaMasivaDTO> crearLista() {

	List<CargaMasivaDTO> listaTodo = new ArrayList<CargaMasivaDTO>();
	for (Long sid : listaSeleccionada) {
	    CargaMasivaDTO carga = new CargaMasivaDTO();
	    carga.setSid(sid);
	    carga.setComentario(comentario);
	    carga.setGestion(gestionMasivo);
	    carga.setIdUser(idUserMasivo);
	    listaTodo.add(carga);
	}

	return listaTodo;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/11/2018, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Exportar masivo
     * @throws AppException
     * @since 1.X
     */
    public String ejecutaExportaMasivo() {
	ConsultarTrxAvancesDAO consultar = null;
	try {

	    List<CargaMasivaDTO> listaTodo = crearLista();
	    consultar = new ConsultarTrxAvancesDAO();
	    consultar.guardarCargoMasivo(listaTodo);

	    mensajeError = "Se han guardado correctamente la gesti�n de los trx`s seleccionados.";

	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    if (e.getMessage().equals("EXISTE")) {
		mensajeError = "Para una de las transacciones seleccionadas ya se termino la gesti�n, favor reintentar nuevamente.";
	    }
	    return "SUCCESS";
	}

	return "SUCCESS";

    }

}
