package actions;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import Beans.Usuario;

import com.opensymphony.xwork2.ActionSupport;

import clases.TipoTransacDAO;

public class TipoTransacAction extends ActionSupport implements ServletRequestAware{

	/**
     * 
     */
    private static final long serialVersionUID = -2903582223835908476L;
	private TipoTransacDAO transacDAO;
	private String sid; 
	private String estado;
	private String listaTipoTransac;
	private String updateTransac;
	private Long idSubmenu;
	private Long idMenu;
	private HttpServletRequest request;
	
	public Long getIdSubmenu() {
	    return idSubmenu;
	}

	public void setIdSubmenu(Long idSubmenu) {
	    this.idSubmenu = idSubmenu;
	}

	public Long getIdMenu() {
	    return idMenu;
	}

	public void setIdMenu(Long idMenu) {
	    this.idMenu = idMenu;
	}

	public TipoTransacAction(){
		transacDAO = new TipoTransacDAO();
	}
	
	/**
	 * execute
	 */
	public String tiposTransaccionIndex(){
	    Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
	    if(usuarioLog !=  null){
		usuarioLog.setCreacion(0);
		usuarioLog.setLectura(0);
		usuarioLog.setActualizacion(0);
		usuarioLog.setEliminacion(0);

		Integer actualizacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 3);
		usuarioLog.setActualizacion(actualizacion);

		return "SUCCESS";
	    }else{
		return "FAILED";
	    }
	}

	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	public String getListaTipoTransac(){
		return listaTipoTransac;
	}
	
	public void setListaTipoTransac(){
		this.listaTipoTransac = transacDAO.getListaTipoTransac();
	}
	
	public String cargarTipoTransac(){
		setListaTipoTransac();
		getListaTipoTransac();
		return "SUCCESS";
	}
	
	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUpdateTransac() {
		return updateTransac;
	}

	public void setUpdateTransac() {
		this.updateTransac = transacDAO.cambiarEstadoTipo(sid, estado);
	}

	public String exeCambioEstado(){
		setUpdateTransac();
		getUpdateTransac();
		return "SUCCESS";
	}
	
	public void setServletRequest(HttpServletRequest httpServletRequest) {
	    this.request = httpServletRequest;
	}
}
