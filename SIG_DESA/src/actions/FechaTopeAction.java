package actions;

import clases.FechaTopeDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase que permite rescatar la fecha tope.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class FechaTopeAction extends ActionSupport{
	
	private int    sidtransaccion;
	private String vxkey;
	private String codigorazon;
	private String listafechatope;
	

	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	/**
	 * Metodo que obtiene la fecha tope.
	 * @return fecha tope.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String obtenerFechaTope(){
		   
		FechaTopeDAO fechaTope = new FechaTopeDAO();
		listafechatope = fechaTope.obtenerFechaTope(sidtransaccion, vxkey, codigorazon);
		return "SUCCESS";
	}
	
	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	public int getSidtransaccion() {
		return sidtransaccion;
	}

	public void setSidtransaccion(int sidtransaccion) {
		this.sidtransaccion = sidtransaccion;
	}

	public String getVxkey() {
		return vxkey;
	}

	public void setVxkey(String vxkey) {
		this.vxkey = vxkey;
	}

	public String getCodigorazon() {
		return codigorazon;
	}

	public void setCodigorazon(String codigorazon) {
		this.codigorazon = codigorazon;
	}

	public String getListafechatope() {
		return listafechatope;
	}

	public void setListafechatope(String listafechatope) {
		this.listafechatope = listafechatope;
	}

	
}



