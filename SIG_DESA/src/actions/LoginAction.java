package actions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import Beans.Usuario;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.srv.LdapSrv;
import cl.util.ValidationUtils;
import clases.LoginDAO;

public class LoginAction extends ActionSupport {
    /**
	 * 
	 */
    static final Logger log = Logger.getLogger(LoginAction.class);
    private static final long serialVersionUID = 1614470203092929560L;
    private String username;
    private String password;
    private LoginDAO logDao;
    private LdapSrv logLdapSrv;
    private String error = "";
    public Map session;

    public LoginAction() throws FileNotFoundException, IOException {
	logDao = new LoginDAO();
	logLdapSrv = LdapSrv.getInstance();
	this.session = ActionContext.getContext().getSession();
    }

    public String execute() {
	String res = this.loginValidation();
	return res;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    private String loginValidation() {
	if (logLdapSrv.valCredencialesLdap(username, password)) {
	    if (logDao.valCredencialesLdap(username)) {
		return SUCCESS;
	    } else {
		setError(logDao.getError());
		this.addActionError(error);
		return ERROR;
	    }
	}

	// verificar rut ingresado
	Boolean validacion = ValidationUtils.validarRut(username);

	if (validacion == false) {
	    error = "Rut no valido";
	    session.put("error", error);
	    return "ERROR";
	}

	Integer resol = logDao.valCredenciales(username, password);
	// 0: Usuario inexistente o inactivo o clave incorrecta
	// 1: Usuario existe

	if (resol == 1) {
	    Map session = ActionContext.getContext().getSession();
	    log.info("SESSION_COOKIE: " + session);
	    log.info("OBJETO_USUARIO_COOKIE: "
		    + (Usuario) session.get("usuarioLog"));
	    log.info("NOMBRE_USUARIO_COOKIE: " + session.get("nombre"));

	    return "SUCCESS";
	} else if (resol == 2) {
	    return "CHANGE_PASS";
	} else if (resol == 3) {
	    return "PREGUNTAS";
	} else if (resol == 4) {
	    session.clear();
	    session.put("error", "El Usuario no tiene permisos.");
	    return "ERROR";
	}

	setError(logDao.getError());
	this.addActionError(error);

	return "ERROR";
    }

    public String getError() {
	return error;
    }

    public void setError(String error) {
	this.error = error;
    }

}
