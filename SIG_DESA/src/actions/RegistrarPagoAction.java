package actions;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import types.StatusPagoType;
import Beans.PagoContableBean;
import Beans.ParametroBean;
import Beans.Usuario;
import cl.util.Utils;
import clases.CommonsDAO;
import clases.GestionTrxDAO;
import clases.PagoDAO;
import clases.RptCmpnscPagoDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase controlador para las peticiones del modulo de registro de pagos
 * 
 * 
 * @author dmedina
 * 
 */
public class RegistrarPagoAction extends ActionSupport implements SessionAware {

    /**
	 * 
	 */
    static final Logger log = Logger.getLogger(RegistrarPagoAction.class);
    private static final long serialVersionUID = 1246524587373029896L;
    private static final String PARAMETRO_STARWITH_IMAGE = "FORMAT_FILENAME_PAGO";
    private static final String PARAMETRO_RUTA_IMAGE = "PATH_FILENAME_PAGO";
    private static final String PARAMETRO_STARWITH_EXCEL = "FORMAT_FILENAME_PAGO_XLS";
    private static final String PARAMETRO_RUTA_EXCEL = "PATH_FILENAME_PAGO_XLS";

    private String listaOperadores;
    private String fechaPago;
    private String sumaIncoming;
    private String sumaDescuento;
    private String sumaCpago;
    private String mensajeError;
    private int codigoError;
    private String montoPago;
    private String montoDescuento;
    private String montoFinal;
    private String filenameImagen;
    private String filenameExcel;
    private List<ParametroBean> listaParametros;
    private Map<String, Object> sessionMap;

    private Long idSubmenu;
    private Long idMenu;
    // private HttpServletRequest request;
    
    private int operador;

    // VARIABLES PARA ARCHIVO ADJUNTO
    private File adjuntoImagen;
    private File adjuntoExcel;
    private String adjuntoContentType;
    private String adjuntoFileName;

    public Long getIdSubmenu() {
	return idSubmenu;
    }

    public void setIdSubmenu(Long idSubmenu) {
	this.idSubmenu = idSubmenu;
    }

    public Long getIdMenu() {
	return idMenu;
    }

    public void setIdMenu(Long idMenu) {
	this.idMenu = idMenu;
    }

    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;
    }

    public String getAdjuntoContentType() {
	return adjuntoContentType;
    }

    public void setAdjuntoContentType(String adjuntoContentType) {
	this.adjuntoContentType = adjuntoContentType;
    }

    public File getAdjuntoImagen() {
	return adjuntoImagen;
    }

    public void setAdjuntoImagen(File adjuntoImagen) {
	this.adjuntoImagen = adjuntoImagen;
    }

    public File getAdjuntoExcel() {
	return adjuntoExcel;
    }

    public void setAdjuntoExcel(File adjuntoExcel) {
	this.adjuntoExcel = adjuntoExcel;
    }

    public String getFechaPago() {
	return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
	this.fechaPago = fechaPago;
    }

    public String getSumaIncoming() {
	return sumaIncoming;
    }

    public void setSumaIncoming(String sumaIncoming) {
	this.sumaIncoming = sumaIncoming;
    }

    public String getSumaCpago() {
	return sumaCpago;
    }

    public void setSumaCpago(String sumaCpago) {
	this.sumaCpago = sumaCpago;
    }

    public String getMensajeError() {
	return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
	this.mensajeError = mensajeError;
    }

    public int getCodigoError() {
	return codigoError;
    }

    public void setCodigoError(int codigoError) {
	this.codigoError = codigoError;
    }

    public String getMontoPago() {
	return montoPago;
    }

    public void setMontoPago(String montoPago) {
	this.montoPago = montoPago;
    }

    public String getMontoDescuento() {
	return montoDescuento;
    }

    public void setMontoDescuento(String montoDescuento) {
	this.montoDescuento = montoDescuento;
    }

    public String getMontoFinal() {
	return montoFinal;
    }

    public void setMontoFinal(String montoFinal) {
	this.montoFinal = montoFinal;
    }

    public String getFilenameImagen() {
	return filenameImagen;
    }

    public void setFilenameImagen(String filenameImagen) {
	this.filenameImagen = filenameImagen;
    }

    public String getFilenameExcel() {
	return filenameExcel;
    }

    public void setFilenameExcel(String filenameExcel) {
	this.filenameExcel = filenameExcel;
    }

    public String getAdjuntoFileName() {
	return adjuntoFileName;
    }

    public void setAdjuntoFileName(String adjuntoFileName) {
	this.adjuntoFileName = adjuntoFileName;
    }

    public String getSumaDescuento() {
	return sumaDescuento;
    }

    public void setSumaDescuento(String sumaDescuento) {
	this.sumaDescuento = sumaDescuento;
    }

    public int getOperador() {
        return operador;
    }

    public void setOperador(int operador) {
        this.operador = operador;
    }
    
    public String cargarSinOperador() {
	setListaOperadores();
	getListaOperadores();
	return "SUCCESS";
    }
    
    public String getListaOperadores() {
   	return listaOperadores;
       }

       public void setListaOperadores() {
   	GestionTrxDAO dao = new GestionTrxDAO();
   	this.listaOperadores = dao.getListOperadorSinTbk();

       }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String registrarPagosIndex() {

	Usuario usuarioLog = (Usuario) sessionMap.get("usuarioLog");
	if (usuarioLog != null) {
	    usuarioLog.setCreacion(0);
	    usuarioLog.setLectura(0);
	    usuarioLog.setActualizacion(0);
	    usuarioLog.setEliminacion(0);

	    Integer creacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu,
		    2);
	    usuarioLog.setCreacion(creacion);

	    if (creacion == 1) {
		return "SUCCESS";
	    }
	}

	return "FAILED";
    }
 
    /**
     * 
     * Metodo para obtener las sumatorias
     * 
     * @return
     */
    public String cargarSumatoriasSugeridas() {

	log.info("=========== Metodo : Cargar Sumatorias Sugeridas ============== ");
	log.info("== Fecha a buscar pago : " + fechaPago);
	RptCmpnscPagoDAO rptCmpnscPagoDAO = new RptCmpnscPagoDAO();
	codigoError = 0;
	HashMap<String, String> sumatoria = null;
	
	try {
	    sumatoria = rptCmpnscPagoDAO.obtenerSumas(fechaPago.trim(), getOperador());
	    
	    if (sumatoria != null) {
		sumaIncoming = (sumatoria
			.get(Utils.SUMATORIA_INCOMING_MAP_NAME) == null) ? "0"
			: sumatoria.get(Utils.SUMATORIA_INCOMING_MAP_NAME);
		sumaDescuento = (sumatoria
			.get(Utils.SUMATORIA_DESCUENTO_MAP_NAME) == null) ? "0"
			: sumatoria.get(Utils.SUMATORIA_DESCUENTO_MAP_NAME);
		sumaCpago = (sumatoria.get(Utils.SUMATORIA_CPAGO_MAP_NAME) == null) ? "0"
			: sumatoria.get(Utils.SUMATORIA_CPAGO_MAP_NAME);
	    }

	    log.info("sumaIncoming: " + sumaIncoming);
	    log.info("sumaDescuento: " + sumaDescuento);
	    log.info("sumaCpago: " + sumaCpago);
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    mensajeError = "Ha ocurrido un error y no se ha podido obtener los montos sugeridos";
	    codigoError = -1;
	}

	return "SUCCESS";
    }
    
    
    /**
     * 
     * Metodo para obtener las sumatorias
     * 
     * @return
     */
    public String cargarSumatoriasSugeridasVisa() {

	log.info("=========== Metodo : Cargar Sumatorias Sugeridas ============== ");
	log.info("== Fecha a buscar pago : " + fechaPago);
	RptCmpnscPagoDAO rptCmpnscPagoDAO = new RptCmpnscPagoDAO();
	codigoError = 0;
	HashMap<String, String> sumatoria = null;
	
	try {
	    sumatoria = rptCmpnscPagoDAO.obtenerSumasVisa(fechaPago.trim(), getOperador());
	    
	    if (sumatoria != null) {
		sumaIncoming = (sumatoria
			.get(Utils.SUMATORIA_INCOMING_MAP_NAME) == null) ? "0"
			: sumatoria.get(Utils.SUMATORIA_INCOMING_MAP_NAME);
		sumaDescuento = (sumatoria
			.get(Utils.SUMATORIA_DESCUENTO_MAP_NAME) == null) ? "0"
			: sumatoria.get(Utils.SUMATORIA_DESCUENTO_MAP_NAME);
		sumaCpago = (sumatoria.get(Utils.SUMATORIA_CPAGO_MAP_NAME) == null) ? "0"
			: sumatoria.get(Utils.SUMATORIA_CPAGO_MAP_NAME);
	    }

	    log.info("sumaIncoming: " + sumaIncoming);
	    log.info("sumaDescuento: " + sumaDescuento);
	    log.info("sumaCpago: " + sumaCpago);
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    mensajeError = "Ha ocurrido un error y no se ha podido obtener los montos sugeridos";
	    codigoError = -1;
	}

	return "SUCCESS";
    }

    /**
     * Metodo principal que realiza el guardado de un pago
     * 
     * @return
     */
    public String guardarPago() {
	try {
	    imprimirDetalles();
	    cargaListaParametros(Utils.GRUPO_PRTS_PAGO);
	    PagoContableBean pago = obtenerPago();
	    // PagoContableBean pago = new PagoContableBean();
	    PagoDAO pagoDAO = new PagoDAO();

	    // GUARDA EL PAGO EN BASE DE DATOS
	    pagoDAO.gestionarPago(pago);

	    if (codigoError == 0) {
		// BUSCA RUTA EN LA PRTS DONDE SE DEJARA EL ARCHIVO TEMPORAL
		String ruta_imagen = buscarPorNombreParametro(PARAMETRO_RUTA_IMAGE);
		String ruta_excel = buscarPorNombreParametro(PARAMETRO_RUTA_EXCEL);

		// MUEVE ARCHIVO A RUTA DESTINO
		Utils.moveFileTo(pago.getFilenameAdjuntoImagen(), ruta_imagen,
			adjuntoImagen);
		if (!filenameExcel.contentEquals("")) {
		    log.info("pago.getFilenameAdjuntoExcel()>>>"
			    + pago.getFilenameAdjuntoExcel());
		    log.info("ruta_excel()>>>" + ruta_excel);
		    log.info("adjuntoExcel>>>> " + adjuntoExcel);
		    Utils.moveFileTo(pago.getFilenameAdjuntoExcel(),
			    ruta_excel, adjuntoExcel);
		}
	    }
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    mensajeError = "Ha ocurrido un error: ".concat(e.getMessage());
	    codigoError = -1;
	}

	return "SUCCESS";
    }
    
    public String guardarPagoVisa() {
	try {
	    imprimirDetalles();
	    cargaListaParametros(Utils.GRUPO_PRTS_PAGO);
	    PagoContableBean pago = obtenerPago();
	    // PagoContableBean pago = new PagoContableBean();
	    PagoDAO pagoDAO = new PagoDAO();

	    // GUARDA EL PAGO EN BASE DE DATOS
	    pagoDAO.gestionarPago(pago);

	    if (codigoError == 0) {
		// BUSCA RUTA EN LA PRTS DONDE SE DEJARA EL ARCHIVO TEMPORAL
		String ruta_imagen = buscarPorNombreParametro(PARAMETRO_RUTA_IMAGE);
		String ruta_excel = buscarPorNombreParametro(PARAMETRO_RUTA_EXCEL);

		// MUEVE ARCHIVO A RUTA DESTINO
		Utils.moveFileTo(pago.getFilenameAdjuntoImagen(), ruta_imagen,
			adjuntoImagen);
		if (!filenameExcel.contentEquals("")) {
		    log.info("pago.getFilenameAdjuntoExcel()>>>"
			    + pago.getFilenameAdjuntoExcel());
		    log.info("ruta_excel()>>>" + ruta_excel);
		    log.info("adjuntoExcel>>>> " + adjuntoExcel);
		    Utils.moveFileTo(pago.getFilenameAdjuntoExcel(),
			    ruta_excel, adjuntoExcel);
		}
	    }
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    mensajeError = "Ha ocurrido un error: ".concat(e.getMessage());
	    codigoError = -1;
	}

	return "SUCCESS";
    }

    /**
     * Metodo que imprime los datos de entrada
     */
    private void imprimirDetalles() {
	log.info("======== Metodo : Guardar Pago ==========");
	log.info("Fecha : " + fechaPago);
	log.info("Monto Pago : " + montoPago);
	log.info("Monto Descuento : " + montoDescuento);
	log.info("Monto Final : " + montoFinal);
	log.info("respaldofileImagen : " + adjuntoImagen);
	log.info("respaldofileExcel : " + adjuntoExcel);
	log.info("filenameImagen : " + filenameImagen);
	log.info("filenameExcel : " + filenameExcel);
	log.info("operador : " + operador);
    }

    /**
     * Metodo que genera el filenameFinal del archivo adjunto
     * 
     * @return
     */
    private String generateFilenameImagen() {
	// BUSCA NOMBRE EN LA PRTS DEL PARAMETRO STARWITH
	String starwith = buscarPorNombreParametro(PARAMETRO_STARWITH_IMAGE);
	String extension = Utils.obtenerExtensionArchivo(filenameImagen);
	String secuencial = Utils.getDateToday();

	// OBTIENE EL SECUENCIAL DE LA FECHA PARA EL NOMBRE DE ARCHIVO
	String filenameFinal = (starwith).concat(secuencial).concat(".")
		.concat(extension);

	return filenameFinal;
    }

    /**
     * Metodo que genera el filenameFinal del archivo adjunto
     * 
     * @return
     */
    private String generateFilenameExcel() {
	// BUSCA NOMBRE EN LA PRTS DEL PARAMETRO STARWITH
	String starwith = buscarPorNombreParametro(PARAMETRO_STARWITH_EXCEL);
	String extension = Utils.obtenerExtensionArchivo(filenameExcel);
	String secuencial = Utils.getDateToday();

	// OBTIENE EL SECUENCIAL DE LA FECHA PARA EL NOMBRE DE ARCHIVO
	String filenameFinal = (starwith).concat(secuencial).concat(".")
		.concat(extension);

	return filenameFinal;
    }

    /**
     * Metodo que busca dentro de la lista de parametros el nombre de el
     * parametro y retorna su valor asociado
     * 
     * @return
     */
    private String buscarPorNombreParametro(String nombreParametro) {
	for (ParametroBean parametro : listaParametros) {
	    if (parametro.getCodDato().equalsIgnoreCase(nombreParametro)) {
		return parametro.getValor();
	    }
	}

	return null;
    }

    /**
     * Metodo que seatea un pago
     * 
     * @return
     */
    private PagoContableBean obtenerPago() {
	PagoContableBean pago = new PagoContableBean();

	Long sidUsuario = (Long) sessionMap.get("sidusuario");
	pago.setEstadoPago(StatusPagoType.INGRESADO);
	pago.setFechaPago(fechaPago);
	pago.setFilenameAdjuntoImagen(generateFilenameImagen());
	
	if (!filenameExcel.contentEquals("")) {
	    pago.setFilenameAdjuntoExcel(generateFilenameExcel());
	}
	log.info(montoPago);
	pago.setMontoBruto(Long.valueOf(montoPago));
	pago.setMontoDescuento(Long.valueOf(montoDescuento));
	pago.setMontoFinal(Long.valueOf(montoFinal));
	pago.setSidUsuario(sidUsuario);
	pago.setOperador(operador);
	log.info(pago);
	return pago;
    }

    /**
     * 
     * Metodo que carga la lista de parametros dado un grupo
     */
    private void cargaListaParametros(String codGrupo) {
	CommonsDAO commonDao = new CommonsDAO();

	try {
	    listaParametros = commonDao.obtenerParametrosPorGrupo(codGrupo);
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}
    }

}
