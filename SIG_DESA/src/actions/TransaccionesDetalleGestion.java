package actions;

import java.io.Serializable;

public class TransaccionesDetalleGestion implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 3978071019742624448L;
    private String sid;
    private String sidConci;
    private String fecha;
    private String sidUser;
    private String comentario;
    private String estado;
    
    public String getSid() {
        return sid;
    }
    public void setSid(String sid) {
        this.sid = sid;
    }
    public String getSidConci() {
        return sidConci;
    }
    public void setSidConci(String sidConci) {
        this.sidConci = sidConci;
    }
    public String getFecha() {
        return fecha;
    }
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public String getSidUser() {
        return sidUser;
    }
    public void setSidUser(String sidUser) {
        this.sidUser = sidUser;
    }
    public String getComentario() {
        return comentario;
    }
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((sid == null) ? 0 : sid.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TransaccionesDetalleGestion other = (TransaccionesDetalleGestion) obj;
	if (sid == null) {
	    if (other.sid != null)
		return false;
	} else if (!sid.equals(other.sid))
	    return false;
	return true;
    }
    
    @Override
    public String toString() {
	return "TransaccionesDetalleGestion [sid=" + sid + ", sidConci="
		+ sidConci + ", fecha=" + fecha + ", sidUser=" + sidUser
		+ ", comentario=" + comentario + ", estado=" + estado + "]";
    }
    
    

}
