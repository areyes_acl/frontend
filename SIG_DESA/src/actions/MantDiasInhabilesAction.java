package actions;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import Beans.DiaInhabilBeans;
import Beans.Usuario;
import clases.DiasInhabilesDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author dmedina
 * 
 */
public class MantDiasInhabilesAction extends ActionSupport implements ServletRequestAware {

    static final Logger log = Logger.getLogger(MantDiasInhabilesAction.class);
    private static final long serialVersionUID = 1L;
    private DiasInhabilesDAO diasInhabilesDAO;

    private String sid;
    private String estado;
    private String glosa;
    private String fecha;
    private String ano;
    private Long idSubmenu;
    private Long idMenu;
    private HttpServletRequest request;

    private Integer codigoError = 0;
    private String RETURN_HEADER_ERROR = "esttrx:1";

    private String listaDiasInhabiles;

    public MantDiasInhabilesAction() {
	diasInhabilesDAO = new DiasInhabilesDAO();
    }

    
    public String execute() throws Exception {
	
	Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
	usuarioLog.setCreacion(0);
	usuarioLog.setLectura(0);
	usuarioLog.setActualizacion(0);
	usuarioLog.setEliminacion(0);
	
	Integer creacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 2);
	usuarioLog.setCreacion(creacion);
	Integer actualizacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 3);
	usuarioLog.setActualizacion(actualizacion);
	
	return "SUCCESS";
    }

    public String getListaDiasInhabiles() {
	return listaDiasInhabiles;
    }

    public void setListaDiasInhabiles(String listaDiasInhabilesStr) {
	this.listaDiasInhabiles = listaDiasInhabilesStr;
    }

    public String getSid() {
	return sid;
    }

    public void setSid(String sid) {
	this.sid = sid;
    }

    public String getEstado() {
	return estado;
    }

    public void setEstado(String estado) {
	this.estado = estado;
    }

    public Integer getCodigoError() {
	return codigoError;
    }

    public void setCodigoError(Integer codigoError) {
	this.codigoError = codigoError;
    }

    public String getGlosa() {
	return glosa;
    }

    public void setGlosa(String glosa) {
	this.glosa = glosa;
    }

    public String getFecha() {
	return fecha;
    }

    public void setFecha(String fecha) {
	this.fecha = fecha;
    }

    public String getAno() {
	return ano;
    }

    public void setAno(String ano) {
	this.ano = ano;
    }

    /**
     * 
     * @return
     */
    public String cargarDiasInhabiles() {
	log.info("D�as Inhabiles : " + ano);

	try {
	    this.listaDiasInhabiles = diasInhabilesDAO
		    .getListaDiasInhabiles(ano);
	} catch (Exception e) {
	    log.info(e.getMessage(), e);
	}

	log.info("listadiasInabiles : " + listaDiasInhabiles);

	// Si existe algun error o si no retorna la lista de dias
	String[] listaSeparada = listaDiasInhabiles.split("~");

	if (listaSeparada.length == 2
		&& RETURN_HEADER_ERROR.equalsIgnoreCase(listaSeparada[0])) {
	    codigoError = -1;
	}
	return "SUCCESS";
    }

    /**
     * 
     * @return
     */
    public String exeCambioEstado() {
	this.listaDiasInhabiles = diasInhabilesDAO.cambiarEstadoDiaInhabil(sid,
		estado);

	return "SUCCESS";
    }

    /**
     * 
     * @return
     */
    public String guardarDiaInhabil() {

	DiaInhabilBeans diaInhabil = new DiaInhabilBeans(null, glosa, fecha,
		Integer.valueOf(estado));
	codigoError = diasInhabilesDAO.guardarDiaInhabil(diaInhabil);
	return "SUCCESS";

    }


    @Override
    public void setServletRequest(HttpServletRequest httpServletRequest) {
	this.request = httpServletRequest;
	
    }


    public Long getIdSubmenu() {
        return idSubmenu;
    }


    public void setIdSubmenu(Long idSubmenu) {
        this.idSubmenu = idSubmenu;
    }


    public Long getIdMenu() {
        return idMenu;
    }


    public void setIdMenu(Long idMenu) {
        this.idMenu = idMenu;
    }
    
    
}
