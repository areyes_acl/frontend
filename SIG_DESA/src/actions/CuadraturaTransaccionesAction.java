package actions;

import clases.CuadraturaTransaccionesDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase que define el action para la cuadratura de transacciones diarias y mensuales.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class CuadraturaTransaccionesAction extends ActionSupport{

	private String      fechaProceso;
	private String      listaCuadraturaDiaria;
	private String      listaCuadraturaMensual;
	private String      fechaMensual;
	private int	    operador;
	private String      listaOperadores;
	private String	    headerOperador;

	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	/**
	 * Metodo que consulta las cuadraturas de transacciones diarias.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String indexCuadraturaMensualVisa(){
		return "SUCCESS";
	}

	public String buscarCuadraturaTransaccionDiaria(){
		CuadraturaTransaccionesDAO cuadraturaDiaria = new CuadraturaTransaccionesDAO();
		listaCuadraturaDiaria = cuadraturaDiaria.buscarCuadraturaTransaccionDiaria(fechaProceso, operador);
		return "SUCCESS";
	}
	
	public String buscarCuadraturaTransaccionDiariaVisa(){
		CuadraturaTransaccionesDAO cuadraturaDiaria = new CuadraturaTransaccionesDAO();
		listaCuadraturaDiaria = cuadraturaDiaria.buscarCuadraturaTransaccionDiariaVisa(fechaProceso, operador);
		return "SUCCESS";
	}
	
	/**
	 * Metodo que consulta las cuadraturas de transacciones mensual.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	
	public String indexDiariaVisa(){
		return "SUCCESS";
	}

	public String buscarCuadraturaTransaccionMensual(){
		CuadraturaTransaccionesDAO cuadraturaMensual = new CuadraturaTransaccionesDAO();
		listaCuadraturaMensual = cuadraturaMensual.buscarCuadraturaTransaccionMensual(fechaMensual);
		return "SUCCESS";
	}
	
	public String buscarCuadraturaTransaccionMensualVisa(){
		CuadraturaTransaccionesDAO cuadraturaMensual = new CuadraturaTransaccionesDAO();
		listaCuadraturaMensual = cuadraturaMensual.buscarCuadraturaTransaccionMensualVisa(fechaMensual, getOperador());
		return "SUCCESS";
	}
	
	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	
	public String getFechaMensual() {
		return fechaMensual;
	}

	public void setFechaMensual(String fechaMensual) {
		this.fechaMensual = fechaMensual;
	}
	
	public String getListaCuadraturaMensual() {
		return listaCuadraturaMensual;
	}

	public void setListaCuadraturaMensual(String listaCuadraturaMensual) {
		this.listaCuadraturaMensual = listaCuadraturaMensual;
	}
	
	public String getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(String fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public String getListaCuadraturaDiaria() {
		return listaCuadraturaDiaria;
	}

	public void setListaCuadraturaDiaria(String listaCuadraturaDiaria) {
		this.listaCuadraturaDiaria = listaCuadraturaDiaria;
	}

	public int getOperador() {
	    return operador;
	}

	public void setOperador(int operador) {
	    this.operador = operador;
	}


	public String getListaOperadores() {
	    return listaOperadores;
	}

	public void setListaOperadores(String listaOperadores) {
	    this.listaOperadores = listaOperadores;
	}

	public String getHeaderOperador() {
	    return headerOperador;
	}

	public void setHeaderOperador(String headerOperador) {
	    this.headerOperador = headerOperador;
	}

}
