package actions;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class TransaccionesAvances implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3978071019742624448L;
    private String sid;
    private String rutCliente;
    private String fechaFiltro;
    private Long montoAvance;
    private String numeroTarjeta;
    private String estadoConc;
    private String detalleEstado;
    private Integer cantConci;
    private Integer cantNoConci;
    private Long montoConci;
    private Long montoNoConci;
    private Integer sidAvances;
    private Integer sidBice;
    private Integer sidOnus;
    private String estadoGestion;
    private Long valorCuota;
    private String numCuota;
    
    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(String rutCliente) {
        this.rutCliente = rutCliente;
    }

    public String getFechaFiltro() {
        return fechaFiltro;
    }

    public void setFechaFiltro(String fechaFiltro) {
        this.fechaFiltro = fechaFiltro;
    }

    public Long getMontoAvance() {
        return montoAvance;
    }

    public void setMontoAvance(Long montoAvance) {
        this.montoAvance = montoAvance;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getEstadoConc() {
        return estadoConc;
    }

    public void setEstadoConc(String estadoConc) {
        this.estadoConc = estadoConc;
    }

    public String getDetalleEstado() {
        return detalleEstado;
    }

    public void setDetalleEstado(String detalleEstado) {
        this.detalleEstado = detalleEstado;
    }

    public Integer getCantConci() {
        return cantConci;
    }

    public void setCantConci(Integer cantConci) {
        this.cantConci = cantConci;
    }

    public Integer getCantNoConci() {
        return cantNoConci;
    }

    public void setCantNoConci(Integer cantNoConci) {
        this.cantNoConci = cantNoConci;
    }

    public Long getMontoConci() {
        return montoConci;
    }

    public void setMontoConci(Long montoConci) {
        this.montoConci = montoConci;
    }

    public Long getMontoNoConci() {
        return montoNoConci;
    }

    public void setMontoNoConci(Long montoNoConci) {
        this.montoNoConci = montoNoConci;
    }

    public Integer getSidAvances() {
        return sidAvances;
    }

    public void setSidAvances(Integer sidAvances) {
        this.sidAvances = sidAvances;
    }

    public Integer getSidBice() {
        return sidBice;
    }

    public void setSidBice(Integer sidBice) {
        this.sidBice = sidBice;
    }

    public Integer getSidOnus() {
        return sidOnus;
    }

    public void setSidOnus(Integer sidOnus) {
        this.sidOnus = sidOnus;
    }

    public String getEstadoGestion() {
        return estadoGestion;
    }

    public void setEstadoGestion(String estadoGestion) {
        this.estadoGestion = estadoGestion;
    }

    public Long getValorCuota() {
        return valorCuota;
    }

    public void setValorCuota(Long valorCuota) {
        this.valorCuota = valorCuota;
    }

    public String getNumCuota() {
        return numCuota;
    }

    public void setNumCuota(String numCuota) {
        this.numCuota = numCuota;
    }

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( sid == null ) ? 0 : sid.hashCode() );
        return result;
    }
    
    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        TransaccionesAvances other = ( TransaccionesAvances ) obj;
        if ( sid == null ) {
            if ( other.sid != null )
                return false;
        }
        else
            if ( !sid.equals( other.sid ) )
                return false;
        return true;
    }

    @Override
    public String toString() {
	return "TransaccionesAvances [sid=" + sid + ", rutCliente="
		+ rutCliente + ", fechaFiltro=" + fechaFiltro
		+ ", montoAvance=" + montoAvance + ", numeroTarjeta="
		+ numeroTarjeta + ", estadoConc=" + estadoConc
		+ ", detalleEstado=" + detalleEstado + ", cantConci="
		+ cantConci + ", cantNoConci=" + cantNoConci + ", montoConci="
		+ montoConci + ", montoNoConci=" + montoNoConci
		+ ", sidAvances=" + sidAvances + ", sidBice=" + sidBice
		+ ", sidOnus=" + sidOnus + ", estadoGestion=" + estadoGestion
		+ ", valorCuota=" + valorCuota + ", numCuota=" + numCuota + "]";
    }

}
