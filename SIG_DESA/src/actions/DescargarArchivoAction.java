package actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import types.FileType;
import Beans.Usuario;
import cl.filter.FiltroArchivo;
import cl.util.DateTools;
import clases.ArchivosDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class DescargarArchivoAction extends ActionSupport implements
	SessionAware, ServletRequestAware {

    private static final String LISTA_ARCHIVO = "ListaArchivo";

    /**
     * 
     */
    private static final long serialVersionUID = -4667382696523987177L;

    static final Logger log = Logger.getLogger(DescargarArchivoAction.class);

    // VARIABLE DE SESSION
    private Map<String, Object> sessionMap;

    // VARIABLES PARA RENDER TABLA
    private HttpServletRequest request;

    // VARIABLES DE FILTRO
    private String fechaBusqueda;
    private String tipoArchivo;
    private String tabla;
    private FiltroArchivo filtro = new FiltroArchivo();

    // VARIABLES DE RESULTADOS
    private List<File> listaArchivos;

    // VARIABLE DE RESPUESTA ACCIONES
    private int codigoError;
    private String mensajeError;

    // VARIABLE PARA EXPORT DE CSV
    private FileInputStream archivoStream;
    private File fileToDownload;
    private String fileName;

    // VARIABLES PARA MENUS
    private Long idSubmenu;
    private Long idMenu;


    /**
     * GETTERS Y SETTERS
     * 
     * @return
     */
    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;
    }

    @Override
    public void setServletRequest(HttpServletRequest httpServlet) {
	this.request = httpServlet;

    }

    public Map<String, Object> getSessionMap() {
	return sessionMap;
    }


    public int getCodigoError() {
	return codigoError;
    }

    public void setCodigoError(int codigoError) {
	this.codigoError = codigoError;
    }

    public String getMensajeError() {
	return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
	this.mensajeError = mensajeError;
    }

    public String getFechaBusqueda() {
	return fechaBusqueda;
    }

    public void setFechaBusqueda(String fechaBusqueda) {
	this.fechaBusqueda = fechaBusqueda;
    }

    public Long getIdSubmenu() {
	return idSubmenu;
    }

    public void setIdSubmenu(Long idSubmenu) {
	this.idSubmenu = idSubmenu;
    }

    public Long getIdMenu() {
	return idMenu;
    }

    public void setIdMenu(Long idMenu) {
	this.idMenu = idMenu;
    }

    public String getTabla() {
	return tabla;
    }

    public void setTabla(String tabla) {
	this.tabla = tabla;
    }

    public FiltroArchivo getFiltro() {
	return filtro;
    }

    public void setFiltro(FiltroArchivo filtro) {
	this.filtro = filtro;
    }

    public String getTipoArchivo() {
	return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
	this.tipoArchivo = tipoArchivo;
    }

    public List<File> getListaArchivos() {
	return listaArchivos;
    }

    public void setListaArchivos(List<File> listaArchivos) {
	this.listaArchivos = listaArchivos;
    }

    public File getFileToDownload() {
	return fileToDownload;
    }

    public void setFileToDownload(File fileToDownload) {
	this.fileToDownload = fileToDownload;
    }

    public FileInputStream getArchivoStream() {
	return archivoStream;
    }

    public void setArchivoStream(FileInputStream archivoStream) {
	this.archivoStream = archivoStream;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Se incorpora este metodo para poder ver si el usuario tiene permisos para
     * mostra el menu
     * 
     * @return
     * @since 1.X
     */
    public String descargaArchivosIndex() {
	Usuario usuarioLog = (Usuario) request.getSession().getAttribute(
		"usuarioLog");
	usuarioLog.setCreacion(0);
	usuarioLog.setLectura(0);
	usuarioLog.setActualizacion(0);
	usuarioLog.setEliminacion(0);

	Integer creacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 2);
	usuarioLog.setCreacion(creacion);

	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String buscarArchivos() {
	
	if(!fechaValida()){
	    codigoError = -1;
	    mensajeError = "La fecha ingresada no debe ser mayor a 364 dias, ni superior a la fecha de hoy";
		return ActionSupport.ERROR;
	}

	// Obtiene la lista de archivos en el directorios
	ArchivosDAO archivosService = new ArchivosDAO();
	listaArchivos = archivosService.obtieneArchivos(setearFiltroBusqueda());
	
	if(listaArchivos != null && !listaArchivos.isEmpty()){
	    sessionMap.remove(LISTA_ARCHIVO);
	    sessionMap.put(LISTA_ARCHIVO, listaArchivos);
	    
	}

	return ActionSupport.SUCCESS;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private boolean fechaValida() {
	
	Date today = DateTools.transformDateTimeNoHour(new Date());
	Date searchDate = DateTools.transformDateTime(fechaBusqueda, DateTools.DATE_FORMAT_LABEL);
	long diference = DateTools.getDateDiff(today, searchDate, TimeUnit.DAYS);
	
	// a�os anteriores - > a�os posterores +
	if(diference < -365 || diference > 0){
	    return Boolean.FALSE;
	}
	
	return Boolean.TRUE;
    }
    
    
  

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String download() {

	try {
	    listaArchivos = (List<File>) sessionMap.get(LISTA_ARCHIVO);
	    for (File file : listaArchivos) {
		if (file.getName().equals(fileName)){
		    fileToDownload = file;
		    break;
		}
	    }
	    
	    archivoStream = new FileInputStream(fileToDownload);

	} catch (FileNotFoundException e) {
	    log.error(e.getMessage(), e);
	    return ActionSupport.SUCCESS;

	}
	return ActionSupport.SUCCESS;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private FiltroArchivo setearFiltroBusqueda() {
	filtro.setFecha(fechaBusqueda);
	filtro.setTipoArchivo(FileType.getFileTypeBy(tipoArchivo));
	
	log.info(filtro);

	return filtro;
    }

    public String getFileName() {
	return fileName;
    }

    public void setFileName(String fileName) {
	this.fileName = fileName;
    }

}
