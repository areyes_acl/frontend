package actions;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import types.LogTableType;
import types.SearchType;
import Beans.RegistroLog;
import Beans.Usuario;
import cl.filter.FiltroRegistroLog;
import cl.util.excel.impl.ExcelRegistrosLog;
import cl.util.excel.service.ExcelUtils;
import clases.RegistrosLogDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RegistrosLogAction extends ActionSupport implements SessionAware,
	ServletRequestAware {

    /**
     * 
     */
    private static final long serialVersionUID = -4667382696523987177L;

    static final Logger log = Logger.getLogger(RegistrosLogAction.class);

    // VARIABLE DE SESSION
    private Map<String, Object> sessionMap;

    // VARIABLES PARA RENDER TABLA
    private HttpServletRequest request;

    // VARIABLES DE FILTRO
    private String fechaBusqueda;
    private String tabla;
    private FiltroRegistroLog filtro = new FiltroRegistroLog();

    // VARIABLES DE RESULTADOS
    private List<RegistroLog> listaRegistrosLog;

    // VARIABLES PAGINACION
    private String numPagina;
    private String cantReg;
    private String pagActual;

    // VARIABLE DE RESPUESTA ACCIONES
    private int codigoError;
    private String mensajeError;

    // VARIABLE PARA EXPORT DE CSV
    private FileInputStream registosLogStream;
    private int cantidadElementos;

    // VARIABLES PARA MENUS
    private Long idSubmenu;
    private Long idMenu;

    // DAO
    private RegistrosLogDAO registrosLogDAO;

    /**
     * GETTERS Y SETTERS
     * 
     * @return
     */
    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;
    }

    @Override
    public void setServletRequest(HttpServletRequest httpServlet) {
	this.request = httpServlet;

    }

    public Map<String, Object> getSessionMap() {
	return sessionMap;
    }

    public String getNumPagina() {
	return numPagina;
    }

    public void setNumPagina(String numPagina) {
	this.numPagina = numPagina;
    }

    public String getCantReg() {
	return cantReg;
    }

    public void setCantReg(String cantReg) {
	this.cantReg = cantReg;
    }

    public String getPagActual() {
	return pagActual;
    }

    public void setPagActual(String pagActual) {
	this.pagActual = pagActual;
    }

    public int getCodigoError() {
	return codigoError;
    }

    public void setCodigoError(int codigoError) {
	this.codigoError = codigoError;
    }

    public String getMensajeError() {
	return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
	this.mensajeError = mensajeError;
    }

    public FileInputStream getRegistosLogStream() {
	return registosLogStream;
    }

    public void setRegistosLogStream(FileInputStream registosLogStream) {
	this.registosLogStream = registosLogStream;
    }

    public int getCantidadElementos() {
	return cantidadElementos;
    }

    public void setCantidadElementos(int cantidadElementos) {
	this.cantidadElementos = cantidadElementos;
    }

    public String getFechaBusqueda() {
	return fechaBusqueda;
    }

    public void setFechaBusqueda(String fechaBusqueda) {
	this.fechaBusqueda = fechaBusqueda;
    }

    public Long getIdSubmenu() {
	return idSubmenu;
    }

    public void setIdSubmenu(Long idSubmenu) {
	this.idSubmenu = idSubmenu;
    }

    public Long getIdMenu() {
	return idMenu;
    }

    public void setIdMenu(Long idMenu) {
	this.idMenu = idMenu;
    }

    public List<RegistroLog> getListaRegistrosLog() {
	return listaRegistrosLog;
    }

    public void setListaRegistrosLog(List<RegistroLog> listaRegistrosLog) {
	this.listaRegistrosLog = listaRegistrosLog;
    }

    public String getTabla() {
	return tabla;
    }

    public void setTabla(String tabla) {
	this.tabla = tabla;
    }

    public FiltroRegistroLog getFiltro() {
	return filtro;
    }

    public void setFiltro(FiltroRegistroLog filtro) {
	this.filtro = filtro;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Se incorpora este metodo para poder ver si el usuario tiene permisos para
     * mostra el menu
     * 
     * @return
     * @since 1.X
     */
    public String registroLogActionIndex() {
	Usuario usuarioLog = (Usuario) request.getSession().getAttribute(
		"usuarioLog");
	usuarioLog.setCreacion(0);
	usuarioLog.setLectura(0);
	usuarioLog.setActualizacion(0);
	usuarioLog.setEliminacion(0);

	Integer creacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 2);
	usuarioLog.setCreacion(creacion);

	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String buscarRegistrosLog() {
	try {
	    filtro = setearFiltroBusqueda(SearchType.CON_PAGINACION);
	    registrosLogDAO = new RegistrosLogDAO();
	    listaRegistrosLog = registrosLogDAO.buscarRegistros(filtro,
		    numPagina);

	    log.info(listaRegistrosLog);

	    cantidadElementos = listaRegistrosLog.size();

	    request.setAttribute("listaRegistrosLog", listaRegistrosLog);
	    request.setAttribute("ultimaPagina",
		    registrosLogDAO.getUltimaPagina());
	    request.setAttribute("numPagina", registrosLogDAO.getPagActual());
	    request.setAttribute("fechaBusqueda", fechaBusqueda);

	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	}

	return ActionSupport.SUCCESS;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String exportar() {

	codigoError = 0;

	try {
	    // INICIALIZA EL DAO
	    if (registrosLogDAO == null) {
		registrosLogDAO = new RegistrosLogDAO();
	    }

	    filtro = setearFiltroBusqueda(SearchType.SIN_PAGINACION);

	    listaRegistrosLog = registrosLogDAO.buscarRegistros(filtro,
		    numPagina);
	    log.info("lista : " + listaRegistrosLog);

	    ExcelUtils<RegistroLog> excelUtils = new ExcelRegistrosLog(
		    filtro.getLogTableType());
	    File archivo = excelUtils.export(listaRegistrosLog);

	    this.registosLogStream = new FileInputStream(archivo);

	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    codigoError = -1;
	}

	return ActionSupport.SUCCESS;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que recibe desde la vista los datos para realizar la busqueda de
     * transacciones pendientes de incoming
     * 
     * @param username
     * @return
     * @throws Exception
     * @since 1.X
     */
    private FiltroRegistroLog setearFiltroBusqueda(SearchType tipoBusqueda) {
	filtro.setFecha(fechaBusqueda);
	filtro.setLogTableType(LogTableType.getFromLabelValue(tabla));
	filtro.setSearchType(tipoBusqueda);
	return filtro;
    }

}
