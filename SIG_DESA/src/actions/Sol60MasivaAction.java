package actions;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import types.CargoAbonoStatusType;
import types.MoneyType;
import types.SearchType;
import types.TransactionType;
import Beans.CargoAbonoDTO;
import Beans.TransaccionBean;
import Beans.Usuario;
import cl.filter.FiltroTransacciones;
import cl.util.excel.impl.ExcelSol60Masiva;
import cl.util.excel.service.ExcelUtils;
import clases.Solicitud60MasivaDAO;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Sol60MasivaAction extends ActionSupport implements SessionAware,
		ServletRequestAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8245088195354581505L;
	static final Logger log = Logger.getLogger(Sol60MasivaAction.class);

	// VARIABLE DE SESSION
	private Map<String, Object> sessionMap;

	// CARGAR ABONAR
	private List<Long> listaSeleccionada;
	private Long sidTipoCA;
	private String tipoCargoAbono;

	// VARIABLES PARA RENDER TABLA
	private HttpServletRequest request;

	// VARIABLES DE FILTRO
	private String fechaDesde;
	private String fechaHasta;
	private String numeroTarjeta;
	private List<TransaccionBean> listaTrx;

	// VARIABLES PAGINACION
	private String numPagina;
	private String cantReg;
	private String pagActual;

	// VARIABLE DE RESPUESTA ACCIONES
	private int codigoError;
	private String mensajeError;

	// VARIABLE PARA EXPORT DE CSV
	private FileInputStream sol60MasivaStream;
	private int cantidadElementos;
	
	private Long idSubmenu;
	private Long idMenu;

	/**
	 * GETTERS Y SETTERS
	 * 
	 * @return
	 */
	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	@Override
	public void setServletRequest(HttpServletRequest httpServlet) {
		this.request = httpServlet;

	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public List<TransaccionBean> getListaTrx() {
		return listaTrx;
	}

	public void setListaTrx(List<TransaccionBean> listaTrx) {
		this.listaTrx = listaTrx;
	}

	public String getNumPagina() {
		return numPagina;
	}

	public void setNumPagina(String numPagina) {
		this.numPagina = numPagina;
	}

	public String getCantReg() {
		return cantReg;
	}

	public void setCantReg(String cantReg) {
		this.cantReg = cantReg;
	}

	public String getPagActual() {
		return pagActual;
	}

	public void setPagActual(String pagActual) {
		this.pagActual = pagActual;
	}

	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public FileInputStream getSol60MasivaStream() {
		return sol60MasivaStream;
	}

	public void setSol60MasivaStream(FileInputStream sol60MasivaStream) {
		this.sol60MasivaStream = sol60MasivaStream;
	}

	public int getCantidadElementos() {
		return cantidadElementos;
	}

	public void setCantidadElementos(int cantidadElementos) {
		this.cantidadElementos = cantidadElementos;
	}

	public List<Long> getListaSeleccionada() {
		return listaSeleccionada;
	}

	public void setListaSeleccionada(List<Long> listaSeleccionada) {
		this.listaSeleccionada = listaSeleccionada;
	}

	public String getTipoCargoAbono() {
		return tipoCargoAbono;
	}

	public void setTipoCargoAbono(String tipoCargoAbono) {
		this.tipoCargoAbono = tipoCargoAbono;
	}

	public Long getSidTipoCA() {
		return sidTipoCA;
	}

	public void setSidTipoCA(Long sidTipoCA) {
		this.sidTipoCA = sidTipoCA;
	}

	public Long getIdSubmenu() {
	    return idSubmenu;
	}

	public void setIdSubmenu(Long idSubmenu) {
	    this.idSubmenu = idSubmenu;
	}

	public Long getIdMenu() {
	    return idMenu;
	}

	public void setIdMenu(Long idMenu) {
	    this.idMenu = idMenu;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Realiza la busqueda de transacciones que sean del tipo rechazos
	 * 
	 * 
	 * @return
	 * @since 1.X
	 */
	public String solicitud60MasivaIndex(){
	    Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
	    usuarioLog.setCreacion(0);
	    usuarioLog.setLectura(0);
	    usuarioLog.setActualizacion(0);
	    usuarioLog.setEliminacion(0);

	    Integer creacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 2);
	    usuarioLog.setCreacion(creacion);

	    return "SUCCESS";
	}

	public String buscarTransacciones() {
		Solicitud60MasivaDAO sol60DAO = null;
		try {
			FiltroTransacciones filtro = setearFiltroBusqueda(SearchType.CON_PAGINACION);
			sol60DAO = new Solicitud60MasivaDAO();
			listaTrx = sol60DAO.buscarTransacciones(filtro, numPagina);
			log.info(listaTrx);

			cantidadElementos = listaTrx.size();

			log.info(sol60DAO.getUltimaPagina());
			log.info(sol60DAO.getPagActual());
			log.info(fechaDesde);
			log.info(fechaHasta);

			request.setAttribute("listaTransacciones", listaTrx);
			request.setAttribute("ultimaPagina", sol60DAO.getUltimaPagina());
			request.setAttribute("numPagina", sol60DAO.getPagActual());
			request.setAttribute("fechaInicioPag", fechaDesde);
			request.setAttribute("fechaTerminoPag", fechaHasta);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return ActionSupport.SUCCESS;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Metodo que realiza el cargo o abono de acuerdo a lo que se dado una lista
	 * de id's de transacciones a las cuales cargar o abonar
	 * 
	 * @return
	 * @since 1.X
	 */
	public String cargarAbonarTransaccion() {
		Solicitud60MasivaDAO sol60DAO = null;
		try {

			List<CargoAbonoDTO> listaCargoAbono = crearListaACargarAbonar();
			log.info(listaCargoAbono);
			sol60DAO = new Solicitud60MasivaDAO();
			sol60DAO.guardarCargoAbonosMasivos(listaCargoAbono);
			mensajeError = "Se han guardado correctamente los cargos y abonos seleccionados.";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			mensajeError = "Ha ocurrido un error al guardar los cargos/abonos de forma masiva.";
			return Action.SUCCESS;
		}

		return Action.SUCCESS;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Metodo que forma un dto con los parametros de entrada que vienen desde la
	 * vista
	 * 
	 * sidTrx, sidUsuario, fecha_cargo_abono, fecha_actualizacion, moneda,
	 * monto_cargo_abono, tipo (tbl_cargo_abono), estado_proceso
	 * 
	 * @return
	 * @since 1.X
	 */
	private List<CargoAbonoDTO> crearListaACargarAbonar() {

		Long sidUsuario = (Long) sessionMap.get("sidusuario");
		List<CargoAbonoDTO> listaCargoAbono = new ArrayList<CargoAbonoDTO>();
		for (Long sid : listaSeleccionada) {
			CargoAbonoDTO cargoAbono = new CargoAbonoDTO();
			cargoAbono.setTransaccion(sid);
			cargoAbono.setSidUsuario(sidUsuario);
			cargoAbono.setTipoMoneda(MoneyType.PESO_CHILENO);
			cargoAbono.setIdTipoCargoAbono(sidTipoCA);
			cargoAbono.setEstadoProceso(CargoAbonoStatusType.PENDIENTE);
			listaCargoAbono.add(cargoAbono);
		}

		return listaCargoAbono;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * M�todo de acci�n que exporta transacciones
	 * 
	 * @return
	 * @since 1.X
	 */
	public String exportar() {
		Solicitud60MasivaDAO sol60DAO = null;
		codigoError = 0;

		try {
			// INICIALIZA EL DAO
			if (sol60DAO == null) {
				sol60DAO = new Solicitud60MasivaDAO();
			}

			FiltroTransacciones filtro = setearFiltroBusqueda(SearchType.SIN_PAGINACION);
			log.info(filtro);

			listaTrx = sol60DAO.buscarTransaccionesSinPaginacion(filtro);
			log.info("lista : " + listaTrx);

			ExcelUtils excelUtils = new ExcelSol60Masiva();
			File archivo = excelUtils.export(listaTrx);

			this.sol60MasivaStream = new FileInputStream(archivo);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			codigoError = -1;
		}

		return Action.SUCCESS;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Metodo que recibe desde la vista los datos para realizar la busqueda de
	 * transacciones pendientes de incoming
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 * @since 1.X
	 */
	private FiltroTransacciones setearFiltroBusqueda(SearchType tipoBusqueda)
			throws Exception {

		FiltroTransacciones filtro = new FiltroTransacciones();
		filtro.setFechaDesde(fechaDesde);
		filtro.setFechaHasta(fechaHasta);
		filtro.setNumeroTarjeta((numeroTarjeta == null)
				|| (numeroTarjeta != null && numeroTarjeta.isEmpty()) ? "0"
				: numeroTarjeta);
		filtro.setTipoTransaccion(TransactionType.RECHAZO);
		filtro.setTipoBusqueda(tipoBusqueda);
		return filtro;
	}

}
