package actions;

import java.rmi.RemoteException;

import com.opensymphony.xwork2.ActionSupport;

import ws.TarjetaClientWS;
	
public class LlamadaWSAction extends ActionSupport{
	
	private static final long serialVersionUID = 3091298000611225293L;
	private String numeroTarjeta;
	private TarjetaClientWS ws;
	private String cadena;
	
	public String execute() throws RemoteException
	{
		ws = new TarjetaClientWS();
		ws.setNumeroTarjeta(numeroTarjeta);
		cadena = ws.getData();
		return "SUCCESS";
	}
	
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getCadena() {
		return cadena;
	}

	public void setCadena(String cadena) {
		this.cadena = cadena;
	}
}
