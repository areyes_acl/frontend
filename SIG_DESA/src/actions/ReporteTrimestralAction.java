package actions;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import Beans.ReporteTrimestral;
import cl.util.ReporteTrimestralUtil;
import clases.ReporteTrimestralDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ReporteTrimestralAction extends ActionSupport {

    private static final long serialVersionUID = 6965463706410421034L;
    static final Logger log = Logger.getLogger(ReporteTrimestralAction.class);
    private String ano;
    private String trimestre;
    private String listaReporteVisa = "";
    private List<ReporteTrimestral> listaTrimestral;

    public ReporteTrimestralAction() {

    }

    public String getListaReporteVisa() {
	return listaReporteVisa;
    }

    public void setListaReporteVisa(String listaReporteVisa) {
	this.listaReporteVisa = listaReporteVisa;
    }

    public ReporteTrimestralAction(String ano, String trimestre) {
	super();
	this.ano = ano;
	this.trimestre = trimestre;
    }

    public String getAno() {
	return ano;
    }

    public void setAno(String ano) {
	this.ano = ano;
    }

    public String getTrimestre() {
	return trimestre;
    }

    public void setTrimestre(String trimestre) {
	this.trimestre = trimestre;
    }

    public List<ReporteTrimestral> getListaTrimestral() {
	return listaTrimestral;
    }

    public void setListaTrimestral(List<ReporteTrimestral> listaTrimestral) {
	this.listaTrimestral = listaTrimestral;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que busca el reporte trimestral
     * 
     * @return
     * @since 1.X
     */
    public String reporteTrimestralVisaIndex(){
	return "SUCCESS";
    }

    public String cargarInformeTrimestral() {
	Integer anoReporte = Integer.parseInt(ano);
	Integer trimestreReporte = Integer.parseInt(trimestre);
	ReporteTrimestralUtil repoUtils = new ReporteTrimestralUtil();
	// ReportesVISADAO reportesVISADAO = new ReportesVISADAO();
	// listaReporteVisa =
	// reportesVISADAO.buscarReporteVisa(trimestreReporte,
	// anoReporte);
	ReporteTrimestralDAO dao = new ReporteTrimestralDAO();

	listaTrimestral = dao.obtenerReporteTrimestral(anoReporte,
		trimestreReporte);

	List<ReporteTrimestral> reportesFaltantes = repoUtils
		.obtenerTrimestreFaltante(listaTrimestral, trimestreReporte,
			anoReporte);
	listaTrimestral.addAll(reportesFaltantes);
	Collections.sort(listaTrimestral);

	log.info(listaTrimestral);
	return "SUCCESS";
    }

}