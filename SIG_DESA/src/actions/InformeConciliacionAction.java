package actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import cl.util.Constantes;
import clases.ConsultarTrxAutorDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * Clase que define el action para el informe de conciliaci�n LORD COCHRANE 181
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class InformeConciliacionAction extends ActionSupport implements
	SessionAware {

    /**
     * 
     */
    private static final Logger log = Logger
	    .getLogger(InformeConciliacionAction.class);
    private static final long serialVersionUID = -8991874868536430786L;
    private List<TransaccionesAutor> listaFinalAutorizadas = null;
    private List<TransaccionesAutor> listaFinalNoAutorizadas = null;
    private List<TransaccionesAutor> listaFinalNoAutorizadasIC = null;
    private String fechaInicio = null;
    private String fechaFinal = null;
    private Long sumaMontoTrxPar = null;
    private Long sumaMontoTrxNoPar = null;
    private Long sumaMontoTrxNoParIC = null;
    private Map<String, Object> sessionMap;

    public Long getSumaMontoTrxNoPar() {
	return sumaMontoTrxNoPar;
    }

    public Long getSumaMontoTrxPar() {
	return sumaMontoTrxPar;
    }

    public void setSumaMontoTrxPar(Long sumaMontoTrxPar) {
	this.sumaMontoTrxPar = sumaMontoTrxPar;
    }

    public void setSumaMontoTrxNoPar(Long sumaMontoTrxNoPar) {
	this.sumaMontoTrxNoPar = sumaMontoTrxNoPar;
    }

    public Long getSumaMontoTrxNoParIC() {
	return sumaMontoTrxNoParIC;
    }

    public void setSumaMontoTrxNoParIC(Long sumaMontoTrxNoParIC) {
	this.sumaMontoTrxNoParIC = sumaMontoTrxNoParIC;
    }

    public List<TransaccionesAutor> getListaFinalAutorizadas() {
	return listaFinalAutorizadas;
    }

    public void setListaFinalAutorizadas(
	    List<TransaccionesAutor> listaFinalAutorizadas) {
	this.listaFinalAutorizadas = listaFinalAutorizadas;
    }

    public List<TransaccionesAutor> getListaFinalNoAutorizadas() {
	return listaFinalNoAutorizadas;
    }

    public void setListaFinalNoAutorizadas(
	    List<TransaccionesAutor> listaFinalNoAutorizadas) {
	this.listaFinalNoAutorizadas = listaFinalNoAutorizadas;
    }

    public List<TransaccionesAutor> getListaFinalNoAutorizadasIC() {
	return listaFinalNoAutorizadasIC;
    }

    public void setListaFinalNoAutorizadasIC(
	    List<TransaccionesAutor> listaFinalNoAutorizadasIC) {
	this.listaFinalNoAutorizadasIC = listaFinalNoAutorizadasIC;
    }

    public String getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getFechaFinal() {
	return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
	this.fechaFinal = fechaFinal;
    }

    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;

    }

    public String InformeConciliacionIndex() {
	return "SUCCESS";
    }

    public String execute() throws Exception {
	fechaInicio = "";
	fechaFinal = "";

	return "SUCCESS";
    }

    public String informeCompensacionIndex() {
	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * M�todo principal que realiza las busquedas para el informe de LOG
     * 
     * 
     * 
     * @return
     * @since 1.X
     */
    public String buscarInformeLogTrx() {
	try {
	    listaFinalNoAutorizadas = new ArrayList<TransaccionesAutor>();
	    listaFinalNoAutorizadasIC = new ArrayList<TransaccionesAutor>();
	    listaFinalAutorizadas = new ArrayList<TransaccionesAutor>();
	    sumaMontoTrxNoPar = 0L;
	    sumaMontoTrxPar = 0L;
	    sumaMontoTrxNoParIC = 0L;
	    ConsultarTrxAutorDAO consultarTrxAutorDAO = new ConsultarTrxAutorDAO();
	    // SE DEJA COMO FECHA DE INICIO FECHA DE INICIO POR SI
	    // DESPUES SE NECESITA PONER LOS RANGOS
	    HashMap<String, List<TransaccionesAutor>> hash = consultarTrxAutorDAO
		    .buscarInformeTrxAutor(fechaInicio, fechaInicio);

	    // Parea listas
	    if (hash != null && hash.entrySet().size() > 0) {
		obtenerInforme(hash);
	    }
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	}

	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que obtiene las listas del informe de trx
     * 
     * 
     * @param hash
     * @since 1.X
     */
    private void obtenerInforme(HashMap<String, List<TransaccionesAutor>> hash) {

	List<TransaccionesAutor> listalog = hash.get("listaTrxLogAutor");
	List<TransaccionesAutor> listaIncoming = hash.get("listaTrxIncoming");

	obtenerResumenInformadoPorIC(listalog, listaIncoming);

	obtenerResumenInformadoPorTransbank(listalog, listaIncoming);

	cuentaMontosTotales(listaFinalAutorizadas, listaFinalNoAutorizadas,
		listaFinalNoAutorizadasIC);
	
	log.info(listaFinalNoAutorizadas);
	log.info("Cantidad trx pareadas : " + listaFinalAutorizadas.size());
	log.info("Cantidad trx no autorizadas por TBK : "
		+ listaFinalNoAutorizadas.size());
	log.info("Cantidad trx no autorizada por IC : "
		+ listaFinalNoAutorizadasIC.size());
	
	/**
	 * Se guardan las listas en sesi�n para no tener que ir nuevamente  a BD y realizar todo el proceso
	 */

	sessionMap.remove(Constantes.TRX_NO_AUTORIZADAS_POR_IC);
	sessionMap.remove(Constantes.TRX_NO_PAREADAS_TRANSBANK);
	sessionMap.remove(Constantes.KEY_FILENAME);
	sessionMap.put(Constantes.TRX_NO_AUTORIZADAS_POR_IC, listaFinalNoAutorizadasIC);
	sessionMap.put(Constantes.TRX_NO_PAREADAS_TRANSBANK, listaFinalNoAutorizadas);
	sessionMap.put(Constantes.KEY_FILENAME, "export");
	

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que obtiene el informe de las transacciones informadas por
     * outgoing, que no han sido autorizadas por IC en el log de trx autorizadas
     * 
     * 
     * @param listalog
     * @param listaIncoming
     */
    private void obtenerResumenInformadoPorTransbank(
	    List<TransaccionesAutor> listaLog,
	    List<TransaccionesAutor> listaOutgoing) {

	// Se recorren las transacciones informadas por outgoing
	for (TransaccionesAutor trxOutgoing : listaOutgoing) {
	    Boolean estaAutorizada = Boolean.FALSE;

	    if (listaLog.size() > 0) {
		// Se recorren las trx del log de autorizacion IC,
		for (TransaccionesAutor trxLog : listaLog) {
		    estaAutorizada = isAutorizada(trxOutgoing, trxLog);
		    if (estaAutorizada) {
			break;
		    }
		}
		// Si no encontro pareo v�lido entre la trx de outgoing versus
		// la lista de trx del log de autorizadas se almacena en lista
	    }
	    if (!estaAutorizada) {
		listaFinalNoAutorizadas.add(trxOutgoing);
	    }
	}

	log.info("Resumen de Transbank :" + listaFinalNoAutorizadas);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que obtiene las trx informadas por IC en el log de trx, que han
     * sido pareadas correctamente con las trx informadas por transabank Adem�s
     * tambien obtiene el informa de las trx informadas en el log de trx, que no
     * han sido informadas por tbk en el Outgoing.
     * 
     * 
     * 
     * @param listalog
     * @param listaIncoming
     * @since 1.X
     */

    private void obtenerResumenInformadoPorIC(
	    List<TransaccionesAutor> listalog,
	    List<TransaccionesAutor> listaIncoming) {

	// CON LA LISTA DE TRX AUT Y DE OUTGOING SE VEN CUALES HACEN MATCH
	for (TransaccionesAutor trxAutor : listalog) {
	    Boolean estaAutorizada = Boolean.FALSE;

	    if (listaIncoming.size() > 0) {
		// RECORRE LA LISTA DE TRX DESDE INCOMING
		for (TransaccionesAutor trxInc : listaIncoming) {
		    estaAutorizada = isAutorizada(trxAutor, trxInc);
		    if (estaAutorizada) {
			listaFinalAutorizadas.add(trxAutor);
			break;
		    }
		}

		// SI NO SE ENCONTRO NINGUNA TRX AUTORIZADA EL OUTGOING DE TBK,
		// SE AGREGAN A LA LISTA DE NO AUTORIZADAS POR IC
		if (!estaAutorizada) {
		    listaFinalNoAutorizadasIC.add(trxAutor);
		}
	    } else {
		listaFinalNoAutorizadasIC.add(trxAutor);
	    }
	}

	// LISTA DE TRX NO AUTORIZADAS EN IC, PERO INFORMADAS EN EL OUTGOING
	listaFinalNoAutorizadasIC = cleanList(listaFinalAutorizadas,
		listaFinalNoAutorizadasIC);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * M�todo que realiza la sumatoria de los montos de las dos listas trx
     * autorizadas y trx no autorizadas.
     * 
     * @param listaAutor
     * @param listaNoAutor
     * @param listaFinalNoAutorizadasIC2
     * @since 1.X
     */
    private void cuentaMontosTotales(List<TransaccionesAutor> listaAutor,
	    List<TransaccionesAutor> listaNoAutor,
	    List<TransaccionesAutor> listaNoAutorIC) {
	
	//log.info(listaNoAutorIC);

	for (TransaccionesAutor trAutor : listaAutor) {
	    String amount = trAutor.getMontoTransac().replace(",", "")
		    .replace(".", "");
	    sumaMontoTrxPar = sumaMontoTrxPar
		    + Long.valueOf(amount.substring(0, amount.length() - 2));
	    trAutor.setMontoTransac(amount.substring(0, amount.length() - 2));
	}

	for (TransaccionesAutor tr : listaNoAutor) {
	    String amount = tr.getMontoTransac().replace(",", "")
		    .replace(".", "");
	    ;
	    sumaMontoTrxNoPar = sumaMontoTrxNoPar
		    + Long.valueOf(amount.substring(0, amount.length() - 2));
	    tr.setMontoTransac(amount.substring(0, amount.length() - 2));
	}

	for (TransaccionesAutor tr : listaNoAutorIC) {
	    String amount = tr.getMontoTransac().replace(",", "")
		    .replace(".", "");
	    //log.info(amount);
	    if(!amount.equals("0")){
		sumaMontoTrxNoParIC = sumaMontoTrxNoParIC + Long.valueOf(amount.substring(0, amount.length() - 2));
		tr.setMontoTransac(amount.substring(0, amount.length() - 2));
	    }
	    else{
		tr.setMontoTransac(amount);
	    }
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * M�todo encargado de extraer las trx que estan autorizadas desde la lista
     * de transacciones no autorizadas
     * 
     * @param listaFinalAutorizadas
     * @param listaFinalNoAutorizadas
     * @return
     * @since 1.X
     */
    private List<TransaccionesAutor> cleanList(
	    List<TransaccionesAutor> listaAutorizadas,
	    List<TransaccionesAutor> listaNoAutorizadas) {
	List<TransaccionesAutor> listaTemp = new ArrayList<TransaccionesAutor>();

	for (TransaccionesAutor trNoAut : listaNoAutorizadas) {
	    if (listaAutorizadas.size() > 0) {

		for (TransaccionesAutor trAut : listaAutorizadas) {
		    if (!isAutorizada(trNoAut, trAut)
			    && !listaTemp.contains(trNoAut)) {
			listaTemp.add(trNoAut);
			break;
		    }
		}
	    } else {
		listaTemp.add(trNoAut);
	    }
	}

	return listaTemp;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que chequea si una transaccion esta autorizada de acuerdo al
     * numero de tarjeta al monto de la transaccion y al codigo de autorizacion
     * 
     * 
     * 
     * @param trLogAutor
     * @param trIncoming
     * @return
     * @since 1.X
     */
    private boolean isAutorizada(TransaccionesAutor trLogAutor,
	    TransaccionesAutor trIncoming) {

	if (trLogAutor.getNumeroTarjeta().equalsIgnoreCase(
		trLogAutor.getNumeroTarjeta())
		&& trLogAutor.getMontoTransac().equalsIgnoreCase(
			trIncoming.getMontoTransac())
		&& trLogAutor.getCodAutorizacion().equalsIgnoreCase(
			trIncoming.getCodAutorizacion())) {
	    return Boolean.TRUE;
	}
	return Boolean.FALSE;
    }

}
