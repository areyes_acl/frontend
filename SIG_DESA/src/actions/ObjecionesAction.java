package actions;

import com.opensymphony.xwork2.ActionSupport;

import clases.ObjecionesDAO;

public class ObjecionesAction extends ActionSupport{
	
	ObjecionesDAO paraDAO;
	
	private String listaTipoObjecion;
	
	private String sid;
	private String numeroTarjeta;
	private String fechaTrx;
	private String codigoAutorizacion;
	
	private String glosa;
	private String objeciones;
	
	private String sidusuario;
	
	private String resEjecucion;
	private String numIncidente;
	
	public ObjecionesAction(){
		paraDAO = new ObjecionesDAO();
	}
	
	/**
	 * execute
	 */
	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	public String getListaTipoObjecion()
	{
		return listaTipoObjecion;
	}
	
	public void setListaTipoObjecion()
	{
		this.listaTipoObjecion = paraDAO.getListaTipoObjecion();
	}
	
	public String cargarTipoObjeciones()
	{
		setListaTipoObjecion();
		getListaTipoObjecion();
		return "SUCCESS";
	}
	
	public String ejecutaObjecion()
	{
		setEjecutaObjecion();
		getEjecutaObjecion();
		return "SUCCESS";
	}
	
	public void setEjecutaObjecion()
	{
		this.resEjecucion = paraDAO.ejecutaObjecion(sid, numeroTarjeta, fechaTrx, codigoAutorizacion, glosa, objeciones, sidusuario,numIncidente);
		System.out.print(resEjecucion);
	}
	
	public String getEjecutaObjecion()
	{
		return resEjecucion;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getFechaTrx() {
		return fechaTrx;
	}

	public void setFechaTrx(String fechaTrx) {
		this.fechaTrx = fechaTrx;
	}

	public String getCodigoAutorizacion() {
		return codigoAutorizacion;
	}

	public void setCodigoAutorizacion(String codigoAutorizacion) {
		this.codigoAutorizacion = codigoAutorizacion;
	}

	public String getGlosa() {
		return glosa;
	}

	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}

	public String getObjeciones() {
		return objeciones;
	}

	public void setObjeciones(String objeciones) {
		this.objeciones = objeciones;
	}

	public String getSidusuario() {
		return sidusuario;
	}

	public void setSidusuario(String sidusuario) {
		this.sidusuario = sidusuario;
	}

	public String getNumIncidente() {
		return numIncidente;
	}

	public void setNumIncidente(String numIncidente) {
		this.numIncidente = numIncidente;
	}
	
}
