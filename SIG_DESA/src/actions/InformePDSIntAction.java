package actions;

import clases.InformesPDSIntDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase que define el action para la cuadratura de transacciones diarias y mensuales.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class InformePDSIntAction extends ActionSupport{

	private String      fechaInicio;	
	private String 		fechaFin;
	private String      listaInforme;

	

	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	/**
	 * Metodo que consulta las cuadraturas de transacciones diarias.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarInformePDSIntRangoFechas(){
		InformesPDSIntDAO informePDS = new InformesPDSIntDAO();
		//listaInforme = informePDS.buscarInformePDSIntRangoFechas(fechaInicio,fechaFin);
		return "SUCCESS";
	}
	
	
	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getListaInforme() {
		return listaInforme;
	}

	public void setListaInforme(String listaInforme) {
		this.listaInforme = listaInforme;
	}
	
	

	
	
}
