/**
 * @see Action asociado a clase ExportExcelDAO
 */

package actions;

import clases.ExportExcel;
import clases.ExportarDataExcelDAO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ExportExcelAction {
	
	private ExportExcel excel;
	private String mensaje;
	
	private String serie;
	private String componente;
	
	private InputStream fileInputStream;
	private String fileName;
	private String nombreArchivo;
	private String      fechaInicio;	
	private String 		fechaFin;
	private String      listaInforme;
	
	public String getFechaInicio() {
		return fechaInicio;
	}



	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}



	public String getFechaFin() {
		return fechaFin;
	}



	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}



	public String getListaInforme() {
		return listaInforme;
	}



	public void setListaInforme(String listaInforme) {
		this.listaInforme = listaInforme;
	}

	private File file;
	
	public ExportExcelAction(){
		excel = new ExportExcel();
	}
	
	
	
	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	//generarExcelObjRech
	public String generarExcelObjRech() throws FileNotFoundException {
		ExportarDataExcelDAO exportar = new ExportarDataExcelDAO();
		file = exportar.generarPlanillaExcelObjRech(fechaInicio, fechaFin, componente, nombreArchivo);
		setFileInputStream(new FileInputStream(file));
		this.setFileName(file.getName());
		
		return "SUCCESS";
		
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getComponente() {
		return componente;
	}

	public void setComponente(String componente) {
		this.componente = componente;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	public String getNombreArchivo() {
		return nombreArchivo;
	}



	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	
	
}
