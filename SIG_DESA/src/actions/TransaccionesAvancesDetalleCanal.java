package actions;

import java.io.Serializable;

public class TransaccionesAvancesDetalleCanal implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 3978071019742624448L;
    private String sid;
    private String fechaTransafer;
    private String codAutor;
    private String numTrx;
    private String numTrajeta;
    private String monto;
    private String estado;
    private String montoSeguro;
    
    public String getSid() {
        return sid;
    }
    public void setSid(String sid) {
        this.sid = sid;
    }
    public String getFechaTransafer() {
        return fechaTransafer;
    }
    public void setFechaTransafer(String fechaTransafer) {
        this.fechaTransafer = fechaTransafer;
    }
    public String getCodAutor() {
        return codAutor;
    }
    public void setCodAutor(String codAutor) {
        this.codAutor = codAutor;
    }
    public String getNumTrx() {
        return numTrx;
    }
    public void setNumTrx(String numTrx) {
        this.numTrx = numTrx;
    }
    public String getNumTrajeta() {
        return numTrajeta;
    }
    public void setNumTrajeta(String numTrajeta) {
        this.numTrajeta = numTrajeta;
    }
    public String getMonto() {
        return monto;
    }
    public void setMonto(String monto) {
        this.monto = monto;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((sid == null) ? 0 : sid.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TransaccionesAvancesDetalleCanal other = (TransaccionesAvancesDetalleCanal) obj;
	if (sid == null) {
	    if (other.sid != null)
		return false;
	} else if (!sid.equals(other.sid))
	    return false;
	return true;
    }
    
    public String getMontoSeguro() {
	return montoSeguro;
    }
    public void setMontoSeguro(String montoSeguro) {
	this.montoSeguro = montoSeguro;
    }
    @Override
    public String toString() {
	return "TransaccionesAvancesDetalleCanal [sid=" + sid
		+ ", fechaTransafer=" + fechaTransafer + ", codAutor="
		+ codAutor + ", numTrx=" + numTrx + ", numTrajeta="
		+ numTrajeta + ", monto=" + monto + ", estado=" + estado
		+ ", montoSeguro=" + montoSeguro + "]";
    }
    
       

}
