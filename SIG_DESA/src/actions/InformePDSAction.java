package actions;

import clases.InformesPDSDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase que define el action para la cuadratura de transacciones diarias y mensuales.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class InformePDSAction extends ActionSupport{

	private String      fechaInicio;	
	private String 		fechaFin;
	private String      listaInforme;
	private String paginaActual;
	private String totalPaginas;
	private String paginaConsulta;
	

	

	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	/**
	 * Metodo que consulta las cuadraturas de transacciones diarias.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarInformePDSrangoFechas(){
		InformesPDSDAO informePDS = new InformesPDSDAO();
		//String[] res = informePDS.buscarInformePDSrangoFechas(fechaInicio,fechaFin,paginaConsulta);
		//listaInforme = res[0];
		//int cantidadResultados = Integer.parseInt(res[1]);
		
		
		//paginaActual = paginaConsulta;
		//totalPaginas= res[1];
		
		return "SUCCESS";
	}
	
	
	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getListaInforme() {
		return listaInforme;
	}

	public void setListaInforme(String listaInforme) {
		this.listaInforme = listaInforme;
	}

	

	public void setPaginaActual(String paginaActual) {
		this.paginaActual = paginaActual;
	}

	public String getPaginaActual() {
		return paginaActual;
	}

	public void setTotalPaginas(String totalPaginas) {
		this.totalPaginas = totalPaginas;
	}

	public String getTotalPaginas() {
		return totalPaginas;
	}

	public void setPaginaConsulta(String paginaConsulta) {
		this.paginaConsulta = paginaConsulta;
	}

	public String getPaginaConsulta() {
		return paginaConsulta;
	}
	
	

	
	
}
