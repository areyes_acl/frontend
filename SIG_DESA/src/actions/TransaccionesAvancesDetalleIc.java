package actions;

import java.io.Serializable;

public class TransaccionesAvancesDetalleIc implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 3978071019742624448L;
    private String sid;
    private String codTransac;
    private String numTrajeta;
    private String fechaCompra;
    private String fechaAutoriza;
    private String fechaPosteo;
    private String tipoVenta;
    private String numCuotas;
    private String numMicroFilm;
    private String numComercio;
    private String montoTransac;
    private String valorCuota;
    private String nombreComercio;
    private String ciudadComercio;
    private String rubroComercio;
    private String codAutor;
    
    public String getSid() {
        return sid;
    }
    public void setSid(String sid) {
        this.sid = sid;
    }
    public String getCodTransac() {
        return codTransac;
    }
    public void setCodTransac(String codTransac) {
        this.codTransac = codTransac;
    }
    public String getNumTrajeta() {
        return numTrajeta;
    }
    public void setNumTrajeta(String numTrajeta) {
        this.numTrajeta = numTrajeta;
    }
    public String getFechaCompra() {
        return fechaCompra;
    }
    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
    public String getFechaAutoriza() {
        return fechaAutoriza;
    }
    public void setFechaAutoriza(String fechaAutoriza) {
        this.fechaAutoriza = fechaAutoriza;
    }
    public String getFechaPosteo() {
        return fechaPosteo;
    }
    public void setFechaPosteo(String fechaPosteo) {
        this.fechaPosteo = fechaPosteo;
    }
    public String getTipoVenta() {
        return tipoVenta;
    }
    public void setTipoVenta(String tipoVenta) {
        this.tipoVenta = tipoVenta;
    }
    public String getNumCuotas() {
        return numCuotas;
    }
    public void setNumCuotas(String numCuotas) {
        this.numCuotas = numCuotas;
    }
    public String getNumMicroFilm() {
        return numMicroFilm;
    }
    public void setNumMicroFilm(String numMicroFilm) {
        this.numMicroFilm = numMicroFilm;
    }
    public String getNumComercio() {
        return numComercio;
    }
    public void setNumComercio(String numComercio) {
        this.numComercio = numComercio;
    }
    public String getMontoTransac() {
        return montoTransac;
    }
    public void setMontoTransac(String montoTransac) {
        this.montoTransac = montoTransac;
    }
    public String getValorCuota() {
        return valorCuota;
    }
    public void setValorCuota(String valorCuota) {
        this.valorCuota = valorCuota;
    }
    public String getNombreComercio() {
        return nombreComercio;
    }
    public void setNombreComercio(String nombreComercio) {
        this.nombreComercio = nombreComercio;
    }
    public String getCiudadComercio() {
        return ciudadComercio;
    }
    public void setCiudadComercio(String ciudadComercio) {
        this.ciudadComercio = ciudadComercio;
    }
    public String getRubroComercio() {
        return rubroComercio;
    }
    public void setRubroComercio(String rubroComercio) {
        this.rubroComercio = rubroComercio;
    }
    public String getCodAutor() {
        return codAutor;
    }
    public void setCodAutor(String codAutor) {
        this.codAutor = codAutor;
    }
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((sid == null) ? 0 : sid.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TransaccionesAvancesDetalleIc other = (TransaccionesAvancesDetalleIc) obj;
	if (sid == null) {
	    if (other.sid != null)
		return false;
	} else if (!sid.equals(other.sid))
	    return false;
	return true;
    }
    
    @Override
    public String toString() {
	return "TransaccionesAvancesDetalleIc [sid=" + sid + ", codTransac="
		+ codTransac + ", numTrajeta=" + numTrajeta + ", fechaCompra="
		+ fechaCompra + ", fechaAutoriza=" + fechaAutoriza
		+ ", fechaPosteo=" + fechaPosteo + ", tipoVenta=" + tipoVenta
		+ ", numCuotas=" + numCuotas + ", numMicroFilm=" + numMicroFilm
		+ ", numComercio=" + numComercio + ", montoTransac="
		+ montoTransac + ", valorCuota=" + valorCuota
		+ ", nombreComercio=" + nombreComercio + ", ciudadComercio="
		+ ciudadComercio + ", rubroComercio=" + rubroComercio
		+ ", codAutor=" + codAutor + "]";
    }
    
    
    
    
}
