package actions;

import org.apache.log4j.Logger;

import Beans.TDIJobBean;
import clases.TDIJobsDAO;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

public class TDIDeleteAction extends ActionSupport {
    private static final long serialVersionUID = -7123469716521196713L;
    
    static final Logger LOGGER = Logger.getLogger(TDIDeleteAction.class);
    
    private TDIJobsDAO tdiJobsDAO;
    private int idJobWork;
    
    private String jsonString;
    
    public TDIDeleteAction() {
	tdiJobsDAO = new TDIJobsDAO();
	idJobWork = 0;
    }
    
    public String eliminarTDI(){
	if(idJobWork > 0){
	    LOGGER.info("Tengo el id a eliminar "+idJobWork);  
	    TDIJobBean jobWork = tdiJobsDAO.getById(idJobWork);
	    int cantidad = jobWork.getCantidadDeLogs();
	    LOGGER.info("cantidad de logs que tiene el job :"+cantidad);
	    if(cantidad == 0){
		try{
			tdiJobsDAO.deleteJob(idJobWork);
			jsonString = "8";
		    }catch(Exception e){
			LOGGER.info(e);
			//Ocurrio algun error
			 jsonString = "9";
		    }
	    }else{
		jsonString = "9";
	    }
	    
	}
	return Action.SUCCESS;
    }

    public int getIdJobWork() {
        return idJobWork;
    }

    public void setIdJobWork(int idJobWork) {
        this.idJobWork = idJobWork;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

}