package actions;

/**
 * 
 * @author dmedina
 * 
 */
public class LogBean {

	private String fechaIngreso;
	private String nombreUsuarioIngreso;
	private String fechaAutorizacion;
	private String nombreUsuarioAutorizador;
	private String estadoContable;
	private String fechaContable;

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNombreUsuarioIngreso() {
		return nombreUsuarioIngreso;
	}

	public void setNombreUsuarioIngreso(String nombreUsuarioIngreso) {
		this.nombreUsuarioIngreso = nombreUsuarioIngreso;
	}

	public String getNombreUsuarioAutorizador() {
		return nombreUsuarioAutorizador;
	}

	public void setNombreUsuarioAutorizador(String nombreUsuarioAutorizador) {
		this.nombreUsuarioAutorizador = nombreUsuarioAutorizador;
	}

	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	public String getEstadoContable() {
		return estadoContable;
	}

	public void setEstadoContable(String estadoContable) {
		this.estadoContable = estadoContable;
	}

	public String getFechaContable() {
		return fechaContable;
	}

	public void setFechaContable(String fechaContable) {
		this.fechaContable = fechaContable;
	}

	@Override
	public String toString() {
		return "LogBean [fechaIngreso=" + fechaIngreso
				+ ", nombreUsuarioIngreso=" + nombreUsuarioIngreso
				+ ", fechaAutorizacion=" + fechaAutorizacion
				+ ", nombreUsuarioAutorizador=" + nombreUsuarioAutorizador
				+ ", estadoContable=" + estadoContable + ", fechaContable="
				+ fechaContable + "]";
	}

}
