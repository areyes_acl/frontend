package actions;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class TransaccionesAutor implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3978071019742624448L;
    private String sid;
    private String codigoTransaccion;
    private String numeroTarjeta;
    private String fecha_proceso;
    private String tipoVenta;
    private String numCuotas;
    private String numMicrofilm;
    private String numComercio;
    private String fechaCompra;
    private String montoTransac;
    private String valorCuota;
    private String nombreComercio;
    private String ciudadComercio;
    private String rubroComercio;
    private String codAutorizacion;
    private String codLocal;
    private String codPos;
    private String tipoAccion;
    private String montoFormateado;
    
    public String getSid() {
        return sid;
    }
    
    public void setSid( String sid ) {
        this.sid = sid;
    }
    
    public String getCodPos() {
        return codPos;
    }
    
    public void setCodPos( String codPos ) {
        this.codPos = codPos;
    }
    
    private Boolean isAutorizada;
    
    public String getCodigoTransaccion() {
        return codigoTransaccion;
    }
    
    public void setCodigoTransaccion( String codigoTransaccion ) {
        this.codigoTransaccion = codigoTransaccion;
    }
    
    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }
    
    public void setNumeroTarjeta( String numeroTarjeta ) {
        this.numeroTarjeta = numeroTarjeta;
    }
    
    public String getTipoVenta() {
        return tipoVenta;
    }
    
    public void setTipoVenta( String tipoVenta ) {
        this.tipoVenta = tipoVenta;
    }
    
    public String getNumCuotas() {
        return numCuotas;
    }
    
    public void setNumCuotas( String numCuotas ) {
        this.numCuotas = numCuotas;
    }
    
    public String getNumMicrofilm() {
        return numMicrofilm;
    }
    
    public void setNumMicrofilm( String numMicrofilm ) {
        this.numMicrofilm = numMicrofilm;
    }
    
    public String getNumComercio() {
        return numComercio;
    }
    
    public void setNumComercio( String numComercio ) {
        this.numComercio = numComercio;
    }
    
    public String getFechaCompra() {
        return fechaCompra;
    }
    
    public void setFechaCompra( String fechaCompra ) {
        this.fechaCompra = fechaCompra;
    }
    
    public String getMontoTransac() {
        return montoTransac;
    }
    
    public void setMontoTransac( String montoTransac ) {
        this.montoTransac = montoTransac;
    }
    
    public String getValorCuota() {
        return valorCuota;
    }
    
    public void setValorCuota( String valorCuota ) {
        this.valorCuota = valorCuota;
    }
    
    public String getNombreComercio() {
        return nombreComercio;
    }
    
    public void setNombreComercio( String nombreComercio ) {
        this.nombreComercio = nombreComercio;
    }
    
    public String getCiudadComercio() {
        return ciudadComercio;
    }
    
    public void setCiudadComercio( String ciudadComercio ) {
        this.ciudadComercio = ciudadComercio;
    }
    
    public String getRubroComercio() {
        return rubroComercio;
    }
    
    public void setRubroComercio( String rubroComercio ) {
        this.rubroComercio = rubroComercio;
    }
    
    public String getCodAutorizacion() {
        return codAutorizacion;
    }
    
    public void setCodAutorizacion( String codAutorizacion ) {
        this.codAutorizacion = codAutorizacion;
    }
    
    public Boolean getIsAutorizada() {
        return isAutorizada;
    }
    
    public void setIsAutorizada( Boolean isAutorizada ) {
        this.isAutorizada = isAutorizada;
    }
    
    public String getFecha_proceso() {
        return fecha_proceso;
    }
    
    public void setFecha_proceso( String fecha_proceso ) {
        this.fecha_proceso = fecha_proceso;
    }
    
    public String getCodLocal() {
        return codLocal;
    }
    
    public void setCodLocal( String codLocal ) {
        this.codLocal = codLocal;
    }
    
    public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( sid == null ) ? 0 : sid.hashCode() );
        return result;
    }
    
    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        TransaccionesAutor other = ( TransaccionesAutor ) obj;
        if ( sid == null ) {
            if ( other.sid != null )
                return false;
        }
        else
            if ( !sid.equals( other.sid ) )
                return false;
        return true;
    }

//	@Override
//	public String toString() {
//		return "TransaccionesAutor [sid=" + sid + ", codigoTransaccion="
//				+ codigoTransaccion + ", numeroTarjeta=" + numeroTarjeta
//				+ ", fecha_proceso=" + fecha_proceso + ", tipoVenta="
//				+ tipoVenta + ", numCuotas=" + numCuotas + ", numMicrofilm="
//				+ numMicrofilm + ", numComercio=" + numComercio
//				+ ", fechaCompra=" + fechaCompra + ", montoTransac="
//				+ montoTransac + ", valorCuota=" + valorCuota
//				+ ", nombreComercio=" + nombreComercio + ", ciudadComercio="
//				+ ciudadComercio + ", rubroComercio=" + rubroComercio
//				+ ", codAutorizacion=" + codAutorizacion + ", codLocal="
//				+ codLocal + ", codPos=" + codPos + ", tipoAccion="
//				+ tipoAccion + ", isAutorizada=" + isAutorizada + "]";
//	}
//    


	
	public String getMontoFormateado() {
	    return montoFormateado;
	}

	public void setMontoFormateado(String montoFormateado) {
	    this.montoFormateado = montoFormateado;
	}
	
	
	@Override
	public String toString() {
		return "TransaccionesAutor [numeroTarjeta=" + numeroTarjeta
				+", montoTransac="+ montoTransac + ",codAutorizacion=" + codAutorizacion+"]";
	}

    
}
