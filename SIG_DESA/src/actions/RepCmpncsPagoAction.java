package actions;

import clases.RptCmpnscPagoDAO;

import com.opensymphony.xwork2.ActionSupport;

public class RepCmpncsPagoAction extends ActionSupport {

	private static final long serialVersionUID = 5947795412573620490L;
	private String fecDesde;
	private String fecHasta;
	private String listaCPago;
	private int operador;

	public RepCmpncsPagoAction() {

	}

	public String getListaCPago() {
		return listaCPago;
	}

	public void setListaCPago(String listaCPago) {
		this.listaCPago = listaCPago;
	}

	public RepCmpncsPagoAction(String fecDesde, String fecHasta) {
		super();
		this.fecDesde = fecDesde;
		this.fecHasta = fecHasta;
	}

	public String getFecDesde() {
		return fecDesde;
	}

	public void setFecDesde(String fecDesde) {
		this.fecDesde = fecDesde;
	}

	public String getFecHasta() {
		return fecHasta;
	}

	public void setFecHasta(String fecHasta) {
		this.fecHasta = fecHasta;
	}
	
	public int getOperador() {
	    return operador;
	}

	public void setOperador(int operador) {
	    this.operador = operador;
	}

	public String compensacionesPagosIndex(){
	    return "SUCCESS";
	}

	public String cargarInformeCmpnscnPago() {
		RptCmpnscPagoDAO rptCmpnscPagoDAO = new RptCmpnscPagoDAO();
		listaCPago = rptCmpnscPagoDAO.listarCmpnsPago(fecDesde, fecHasta, operador);
		return "SUCCESS";
	}

}