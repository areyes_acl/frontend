package actions;

import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import types.CargoAbonoStatusType;
import types.CargoAbonoType;
import types.SearchType;
import Beans.CobroCargoVisaBean;
import Beans.Usuario;
import cl.filter.FiltroCargoAbono;
import cl.util.excel.impl.ExcelCargoAbonoPend;
import cl.util.excel.service.ExcelUtils;
import clases.CargoAbonoPendDAO;
import clases.CobroCargoVisaDAO;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class CobroDeCargoVisaAction extends ActionSupport implements ServletRequestAware, SessionAware {

	/**
     * 
     */
    	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(CobroDeCargoVisaAction.class);
	
	
	CobroCargoVisaDAO ccvDAO;
	private Vector<CobroCargoVisaBean> listaCobroCargoVisa;
	private String fechaDesde;
	private String fechaHasta;
	private String TipoTrx;
	private String estadoTrx;
	private int operador;
	private int codigoError;
	private int sidTrx;
	private String mitTrx;
	private String glosa;
	private String codRazon;
	
	private String numPagina;
	private String cantReg;
	private String pagActual;
	
	private Long idSubmenu;
	private Long idMenu;
	private String listaCodigosRazon;
	

	
	
	// VARIABLE DE REQUEST
	private HttpServletRequest request;

	// VARIABLE DE SESSION
	private Map<String, Object> sessionMap;
	
	
	public CobroDeCargoVisaAction(){
	    ccvDAO = new CobroCargoVisaDAO();
	}
	

	@Override
	public void setSession(Map<String, Object> map) {
		sessionMap = map;

	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;

	}

	public Vector<CobroCargoVisaBean> getListaCobroCargoVisa() {
	    return listaCobroCargoVisa;
	}

	public void setListaCobroCargoVisa() throws SQLException {
	    log.info(this.listaCobroCargoVisa);
	    //log.info(ccvDAO);
	    this.listaCobroCargoVisa = ccvDAO.listaCobroCargo(fechaDesde, fechaHasta, TipoTrx, numPagina, estadoTrx, operador);
	    
	    log.info(listaCobroCargoVisa);
	}
	
	public String listarCobroCargoVisa() throws SQLException{
	    setListaCobroCargoVisa();
	    request.setAttribute("listaCobroCargoVisa", getListaCobroCargoVisa());
	    request.setAttribute("ultimaPagina", ccvDAO.getUltimaPagina());
	    request.setAttribute("numPagina", ccvDAO.getPagActual());
	    request.setAttribute("fechaInicioPag", fechaDesde);
	    request.setAttribute("fechaTerminoPag", fechaHasta);
	    return "SUCCESS";
	}
	
	public String reversarTransaccion(){
	    Long sidUsuario = (Long) sessionMap.get("sidusuario");
	    log.info(sidUsuario);
	    log.info(mitTrx);
	    log.info(codRazon);
	    log.info(glosa);
	    try{
		
		ccvDAO.reversarTransaccion(sidTrx, mitTrx, codRazon, glosa, sidUsuario);
		
	    }
	    catch(Exception e){
		log.error(e.getMessage(), e);
	    }
	    
	    return "SUCCESS";
	}
	
	public String eliminarReversa(){
	    log.info(sidTrx);
	    try{
		if(ccvDAO.eliminarReversa(sidTrx)){
		    codigoError = 0;
		}
		else{
		    codigoError = 1;
		}
	    }
	    catch(Exception e){
		log.info(e.getMessage());
	    }
	    return "SUCCESS";
	}

	public String getFechaDesde() {
	    return fechaDesde;
	}


	public void setFechaDesde(String fechaDesde) {
	    this.fechaDesde = fechaDesde;
	}


	public String getFechaHasta() {
	    return fechaHasta;
	}


	public void setFechaHasta(String fechaHasta) {
	    this.fechaHasta = fechaHasta;
	}


	public String getTipoTrx() {
	    return TipoTrx;
	}

	public void setTipoTrx(String tipoTrx) {
	    TipoTrx = tipoTrx;
	}

	public String getEstadoTrx() {
	    return estadoTrx;
	}

	public void setEstadoTrx(String estadoTrx) {
	    this.estadoTrx = estadoTrx;
	}

	public int getOperador() {
	    return operador;
	}

	public void setOperador(int operador) {
	    this.operador = operador;
	}
	
	public String cargarCodigosRazonVisa(){
	    setListaCodigosRazon();
	    getListaCodigosRazon();
	    return "SUCCESS";
	}
	
	public String getListaCodigosRazon() {
	    return listaCodigosRazon;
	}


	public void setListaCodigosRazon() {
	    log.info(ccvDAO);
	    this.listaCodigosRazon = ccvDAO.getCodigosRazonVisa();
	}
	
	public int getSidTrx() {
	    return sidTrx;
	}


	public void setSidTrx(int sidTrx) {
	    this.sidTrx = sidTrx;
	}


	public String getMitTrx() {
	    return mitTrx;
	}


	public void setMitTrx(String mitTrx) {
	    this.mitTrx = mitTrx;
	}


	public String getGlosa() {
	    return glosa;
	}


	public void setGlosa(String glosa) {
	    this.glosa = glosa;
	}


	public String getCodRazon() {
	    return codRazon;
	}


	public void setCodRazon(String codRazon) {
	    this.codRazon = codRazon;
	}
	
	

	public String getNumPagina() {
	    return numPagina;
	}


	public void setNumPagina(String numPagina) {
	    this.numPagina = numPagina;
	}


	public String getCantReg() {
	    return cantReg;
	}


	public void setCantReg(String cantReg) {
	    this.cantReg = cantReg;
	}


	public String getPagActual() {
	    return pagActual;
	}


	public void setPagActual(String pagActual) {
	    this.pagActual = pagActual;
	}


	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @throws Exception
	 * @since 1.X
	 */
	public String cobroDeCargoIndex(){
	    Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
	    usuarioLog.setCreacion(0);
	    usuarioLog.setLectura(0);
	    usuarioLog.setActualizacion(0);
	    usuarioLog.setEliminacion(0);

	    Integer lectura = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 1);
	    usuarioLog.setLectura(lectura);
	    Integer eliminar = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 4);
	    usuarioLog.setEliminacion(eliminar);

	    return "SUCCESS";
	}


	public int getCodigoError() {
	    return codigoError;
	}


	public void setCodigoError(int codigoError) {
	    this.codigoError = codigoError;
	}


	


}