package actions;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.json.simple.JSONObject;

import Beans.Usuario;
import clases.PreguntasSecretasDAO;
import clases.UsuariosDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase que administra los usuarios del sistema.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */

public class UsuarioAction extends ActionSupport implements ServletRequestAware{
	
	private String idusuario;
	private String nombreusuario;
	private String apellidosusuario;
	private String claveusuario;
	private int estadousuario;
	private String rolusuario;
	private int sidusuariorol;
	public Long getId() {
	    return id;
	}


	public void setId(Long id) {
	    this.id = id;
	}
	private Long id;
	
	private String resultado;
	private Map<String, String>map = new HashMap<String, String>();
	private List<Map> listaClientes;
	private String listaRoles;
	private String listaUsuarios;
	private String listaPreguntas;
	private String listaPreguntasUsuario;
	private String guardarPreguntasRespuestas;
	private String checkPreguntasRespuestas;
	
	private Integer p1;
	private Integer p2;
	private Integer p3;
	private String r1;
	private String r2;
	private String r3;
	private Integer sidusuario;

	
	public Integer getP1() {
	    return p1;
	}
	public void setP1(Integer p1) {
	    this.p1 = p1;
	}
	public Integer getP2() {
	    return p2;
	}
	public void setP2(Integer p2) {
	    this.p2 = p2;
	}
	public Integer getP3() {
	    return p3;
	}
	public void setP3(Integer p3) {
	    this.p3 = p3;
	}
	public String getR1() {
	    return r1;
	}
	public void setR1(String r1) {
	    this.r1 = r1;
	}
	public String getR2() {
	    return r2;
	}
	public void setR2(String r2) {
	    this.r2 = r2;
	}
	public String getR3() {
	    return r3;
	}
	public void setR3(String r3) {
	    this.r3 = r3;
	}
	public Integer getSidusuario() {
	    return sidusuario;
	}
	public void setSidusuario(Integer sidusuario) {
	    this.sidusuario = sidusuario;
	}

	private HttpServletRequest request;

	public String execute() throws Exception {
	    return "SUCCESS";
	}

	public String buscarUsuario(){

		//***********************************//
		// Muestra los parametros de entrada //
		//***********************************//
		
		//***********************************//
		// Fin parametros de entrada         //
		//***********************************//
		
		nombreusuario = "PEPE";
		apellidosusuario = "APELLIDO";
		claveusuario="CLAVE";
		estadousuario=1;
		
		JSONObject obj = new JSONObject();
		obj.put("nombreusuario01", "JOHN");
		obj.put("apellidosusuario01", "DROGUETT");
		obj.put("claveusuario01", "RADSENER");
		obj.put("estadousuario01", "1");
		System.out.print(obj.toString());
		
		listaClientes = new LinkedList<Map>();
		//***********************
		//UTILIZACION DE MAP
		//***********************
		map = new LinkedHashMap();
		map.put("nombreusuarioMAP", "MAP1");
		map.put("apellidosusuarioMAP", "MAP2");
		map.put("claveusuarioMAP", "3");
		map.put("estadousuarioMAP", "1");
		listaClientes.add(map);
		
		this.resultado = obj.toString();
		
		//***************************
		//UTILIZACION DE ARRAY DE MAP
		//***************************
		map = new LinkedHashMap();
		map.put("nombreusuarioMAP", "USUARIO01");
		map.put("apellidosusuarioMAP", "APELLIDO01");
		map.put("claveusuarioMAP", "CLAVE01");
		map.put("estadousuarioMAP", "ESTADO01");
		listaClientes.add(map);
		
		map = new LinkedHashMap();
		map.put("nombreusuarioMAP", "USUARIO03");
		map.put("apellidosusuarioMAP", "APELLIDO03");
		map.put("claveusuarioMAP", "CLAVE03");
		map.put("estadousuarioMAP", "ESTADO03");
		listaClientes.add(map);
    	
    	
		//this.resultado = obj.toJSONString();
		//resultado = "{nombreusuario:'JOHN', apellidosusuario:'DROGUETT', claveusuario:'RADSENER', estadousuario:'1'}";
		return "SUCCESS";
	}

	/**
	 * Metodo que ingresa o actualiza los datos del usuario.
	 * @return  Mensaje de grabacion de usuario.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String grabarUsuario(){
		UsuariosDAO usuario = new UsuariosDAO();
		boolean esttrx = false;
		
		if (sidusuariorol==0){ /*Nuevo usuario*/
			
			      esttrx = usuario.buscarUsuario(idusuario);
			      if (esttrx==true){ /* El usuario existe no es nuevo */
			    	  resultado = "esttrx:1~MSG:El usuario ingresado existe, no se puede ingresar como nuevo usuario...~";
			    	  return "SUCCESS"; 
			      }
			      
			      /********************************************/
			      /* No existe por lo tanto lo puedo ingresar */
			      /********************************************/
				  esttrx = usuario.grabarUsuarios(idusuario, nombreusuario, apellidosusuario, claveusuario, estadousuario, rolusuario);
				  if (esttrx == true){
				       resultado = "esttrx:0~MSG:La grabación ha sido realizado con exito...~";
				  }else{
				       resultado = "esttrx:1~MSG:Problemas al insertar el registro en la base de datos...~";
				  }
				  
		}else{ /*Se actualizan los datos del usuario*/
			    esttrx = usuario.actualizarUsuarios(idusuario, nombreusuario, apellidosusuario, claveusuario, estadousuario, rolusuario, sidusuariorol);
			    if (esttrx == true){
				       resultado = "esttrx:0~MSG:La actualizacion ha sido realizado con exito...~";
				}else{
				       resultado = "esttrx:1~MSG:Problemas al actualizar usuario en la base de datos...~";
				}
		}
		return "SUCCESS";
		
	}

	/**
	 * Metodo que consulta los usauruos disponibles en el sistema.
	 * @return Lista de usuarios disponibles.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String cargarUsuarios(){
	    UsuariosDAO usuarioslista = new UsuariosDAO();
	    listaUsuarios = usuarioslista.cargarUsuarios();
	    return "SUCCESS";
	}

	/**
	 * Metodo que consulta las preguntas secretas.
	 * @return Success en caso de carga correcta
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String preguntaSecretaIndex(){
	    return "SUCCESS";
	}

	/**
	 * Metodo que consulta todas preguntas secretas activas mas las preguntas desactivas que tenga el usuario activas
	 * @return Lista de preguntas secretas.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String cargarListaPreguntasSecretas(){
	    setListaPreguntas(sidusuario);
	    //getListaPreguntas();
	    return "SUCCESS";
	}

	public void setListaPreguntas(Integer sidusuario){
	    PreguntasSecretasDAO preguntaSecreta = new PreguntasSecretasDAO();
	    this.listaPreguntas = preguntaSecreta.cargarPreguntasSecretas(sidusuario);
	}

	public String getListaPreguntas(){
	    return listaPreguntas;
	}

	/**
	 * Metodo que consulta todas preguntas secretas activas del usuario y sus respuestas
	 * @return Lista de preguntas y respuestas del usuario.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String cargarListaPreguntasRespuestasSecretasUsuario(){
	    setListaPreguntasRespuestasUsuario(" PREUSER.SID_REF_USUARIO = " + sidusuario + " AND PREUSER.ESTADO = 1");
	    //getListaPreguntas();
	    return "SUCCESS";
	}

	public void setListaPreguntasRespuestasUsuario(String where){
	    PreguntasSecretasDAO preguntaSecreta = new PreguntasSecretasDAO();
	    this.listaPreguntasUsuario = preguntaSecreta.cargarPreguntasRespuestasSecretasUsuario(where);
	}

	public String getListaPreguntasRespuestasUsuario(){
	    return listaPreguntasUsuario;
	}

	/**
	 * Metodo que consulta todas preguntas secretas activas del usuario
	 * @return Lista de preguntas del usuario.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String cargarListaPreguntasSecretasUsuario(){
	    setListaPreguntasUsuario(" PREUSER.SID_REF_USUARIO = " + sidusuario + " AND PREUSER.ESTADO = 1");
	    //getListaPreguntas();
	    return "SUCCESS";
	}

	public void setListaPreguntasUsuario(String where){
	    PreguntasSecretasDAO preguntaSecreta = new PreguntasSecretasDAO();
	    this.listaPreguntasUsuario = preguntaSecreta.cargarPreguntasSecretasUsuario(where);
	}

	public String getListaPreguntasUsuario(){
	    return listaPreguntasUsuario;
	}

	/**
	 * Metodo que guarda las preguntas secretas del usuario
	 * @return Lista de preguntas del usuario.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String guardarPreguntasRespuestas(){
	    setGuardarPreguntasRespuestas();
	    getGuardarPreguntasRespuestas();
	    return "SUCCESS";
	}

	public String getGuardarPreguntasRespuestas(){
	    return guardarPreguntasRespuestas;
	}

	public void setGuardarPreguntasRespuestas(){
	    PreguntasSecretasDAO preguntaSecreta = new PreguntasSecretasDAO();
	    this.guardarPreguntasRespuestas = preguntaSecreta.guardarPreguntasSecretasUsuario(p1, p2, p3, r1, r2, r3, sidusuario);
	    if(this.guardarPreguntasRespuestas.contentEquals("esttrx:0~")){
		Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
		usuarioLog.setPreguntas(0);
	    }
	}

	/**
	 * Metodo que guarda las preguntas secretas del usuario
	 * @return Lista de preguntas del usuario.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String checkPreguntasRespuestas(){
	    setCheckPreguntasRespuestas();
	    getCheckPreguntasRespuestas();
	    return "SUCCESS";
	}

	public String getCheckPreguntasRespuestas(){
	    return checkPreguntasRespuestas;
	}

	public void setCheckPreguntasRespuestas(){
	    PreguntasSecretasDAO preguntaSecreta = new PreguntasSecretasDAO();
	    this.checkPreguntasRespuestas = preguntaSecreta.checkPreguntasSecretasUsuario(p1, p2, p3, r1, r2, r3, sidusuario);
	    if(this.checkPreguntasRespuestas.contentEquals("esttrx:0~")){
		Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
		//usuarioLog.setPreguntas(0);
	    }
	}

	/**
	 * Metodo que carga los roles de usuario disponibles.
	 * @return Lista de roles de usuarios.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String cargarRoles(){
	    UsuariosDAO usuarioRoles = new UsuariosDAO();
	    listaRoles = usuarioRoles.cargarRoles();
	    return "SUCCESS";
	}

	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	public int getSidusuariorol() {
		return sidusuariorol;
	}

	public void setSidusuariorol(int sidusuariorol) {
		this.sidusuariorol = sidusuariorol;
	}
	
	public String getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(String listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
	
	public String getRolusuario() {
		return rolusuario;
	}

	public void setRolusuario(String rolusuario) {
		this.rolusuario = rolusuario;
	}
		
	public String getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(String listaRoles) {
		this.listaRoles = listaRoles;
	}
	
	public List<Map> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<Map> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public Map<String, String> getMap() {
		return map;
	}


	public void setMap(Map<String, String> map) {
		this.map = map;
	}
	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
	public String getIdusuario() {
		return idusuario;
	}
	public void setIdusuario(String idusuario) {
		this.idusuario = idusuario;
	}
	
	public String getNombreusuario() {
		return nombreusuario;
	}
	public void setNombreusuario(String nombreusuario) {
		this.nombreusuario = nombreusuario;
	}
	
	public String getApellidosusuario() {
		return apellidosusuario;
	}
	public void setApellidosusuario(String apellidosusuario) {
		this.apellidosusuario = apellidosusuario;
	}
	
	public String getClaveusuario() {
		return claveusuario;
	}
	public void setClaveusuario(String claveusuario) {
		this.claveusuario = claveusuario;
	}

	public int getEstadousuario() {
		return estadousuario;
	}
	public void setEstadousuario(int estadousuario) {
		this.estadousuario = estadousuario;
	}

	public void setGuardarPreguntasRespuestas(String guardarPreguntasRespuestas) {
	    this.guardarPreguntasRespuestas = guardarPreguntasRespuestas;
	}
	
	public void setServletRequest(HttpServletRequest httpServletRequest) {
	    this.request = httpServletRequest;
	}
}

