package actions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import clases.CuadraturaLogProcesoDAO;

import com.opensymphony.xwork2.ActionSupport;



/**
 * Clase que define el action para la generacion de los log de procesos.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class CuadraturaLogProcesoAction extends ActionSupport{
	
	private String      fechaProceso;
	private String      listaCuadraturaIncoming;
	private String      listaCuadraturaRechazos;
	private String      listaCuadraturaOutgoing;
	private String	    listaCuadraturaInconmingPatpass;
	private String      listaCuadraturaOutgoingPatpass;
	private int         operador;
	
	public String execute() throws Exception {
		return "SUCCESS";
	}

	/**
	 * Metodo que consulta las cuadraturas de carga de incoming.
	 * @return Lista de cuadraturas incoming.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String inicioAplicacion(){
	    return "SUCCESS";
	}
	public String logProcesosIndex(){
	    return "SUCCESS";
	}

	public String buscarCuadraturaLogProcesoIncoming(){
		genFechaProcess();
		CuadraturaLogProcesoDAO cuadraturaLogProceso = new CuadraturaLogProcesoDAO();
		listaCuadraturaIncoming = cuadraturaLogProceso.buscarCuadraturaLogProcesoIncoming(fechaProceso, operador);
		return "SUCCESS";
	}
	
	
	/**
	 * Metodo que consulta las cuadraturas de carga de rechazos.
	 * @return Lista de cuadraturas de rechazos.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarCuadraturaLogProcesoRechazos(){
		genFechaProcess();
		CuadraturaLogProcesoDAO cuadraturaLogProceso = new CuadraturaLogProcesoDAO();
		listaCuadraturaRechazos = cuadraturaLogProceso.buscarCuadraturaLogProcesoRechazos(fechaProceso, operador);
		return "SUCCESS";
	}
	
	/**
	 * Metodo que consulta las transacciones de outgoing pendientes, ok, los cargos y abonos pendientes y ok.
	 * @return Cantidad de registros de outgoing pendientes, ok, cargos y abonos pendienyes y ok.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarCuadraturaLogProcesoOutgoing(){
		genFechaProcess();
		CuadraturaLogProcesoDAO cuadraturaLogProceso = new CuadraturaLogProcesoDAO();
		listaCuadraturaOutgoing = cuadraturaLogProceso.buscarCuadraturaLogProcesoOutgoing(fechaProceso, operador);
		return "SUCCESS";
	}
	
	public String buscarCuadraturaLogPatPass(){
		genFechaProcess();
		CuadraturaLogProcesoDAO cuadraturaLogProceso = new CuadraturaLogProcesoDAO();
		listaCuadraturaInconmingPatpass = cuadraturaLogProceso.buscarCuadraturaLogPatPass(fechaProceso, operador);
		
		return "SUCCESS";
	}
	public String buscarCuadraturaLogOutgoingPatPass(){
		genFechaProcess();
		CuadraturaLogProcesoDAO cuadraturaLogProceso = new CuadraturaLogProcesoDAO();
		listaCuadraturaOutgoingPatpass = cuadraturaLogProceso.buscarCuadraturaLogOutgoingPatPass(fechaProceso, operador);
		
		return "SUCCESS";
	}
	
	
	public String genFechaProcess() {
		
		if (fechaProceso.equals("99999999")){
			
			Date fechaActual = new Date();
	        DateFormat formateador = new SimpleDateFormat("yyyyMMdd");
			String fechaArchivo = formateador.format(fechaActual);
			
			int ano = Integer.parseInt(fechaArchivo.substring(0, 4));
			int mes = Integer.parseInt(fechaArchivo.substring(4, 6));
			int dia = Integer.parseInt(fechaArchivo.substring(6));
			
			
			
			String diaPaso = "";
			String mesPaso = "";
			String anoPaso = "";
			
			if (dia < 10){
	        	diaPaso = "0" + dia;
	        }else{
	        	diaPaso = Integer.toString(dia);
	        }
			
	        if (mes < 10){
	        	mesPaso = "0" + mes;
	        }else{
	        	mesPaso = Integer.toString(mes);
	        }
	        	
	        anoPaso = Integer.toString(ano);
			fechaProceso  = anoPaso + mesPaso + diaPaso;
			
		}
		
		return fechaProceso;
	}
	

	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	public String getFechaProceso() {
		return fechaProceso;
	}

	public String getListaCuadraturaOutgoing() {
		return listaCuadraturaOutgoing;
	}

	public void setListaCuadraturaOutgoing(String listaCuadraturaOutgoing) {
		this.listaCuadraturaOutgoing = listaCuadraturaOutgoing;
	}

	public String getListaCuadraturaIncoming() {
		return listaCuadraturaIncoming;
	}

	public void setListaCuadraturaIncoming(String listaCuadraturaIncoming) {
		this.listaCuadraturaIncoming = listaCuadraturaIncoming;
	}

	public String getListaCuadraturaRechazos() {
		return listaCuadraturaRechazos;
	}

	public void setListaCuadraturaRechazos(String listaCuadraturaRechazos) {
		this.listaCuadraturaRechazos = listaCuadraturaRechazos;
	}

	public void setFechaProceso(String fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public String getListaCuadraturaInconmingPatpass() {
	    return listaCuadraturaInconmingPatpass;
	}

	public void setListaCuadraturaInconmingPatpass(String listaCuadraturaInconmingPatpass) {
	    this.listaCuadraturaInconmingPatpass = listaCuadraturaInconmingPatpass;
	}

	public String getListaCuadraturaOutgoingPatpass() {
	    return listaCuadraturaOutgoingPatpass;
	}

	public void setListaCuadraturaOutgoingPatpass(
		String listaCuadraturaOutgoingPatpass) {
	    this.listaCuadraturaOutgoingPatpass = listaCuadraturaOutgoingPatpass;
	}

	public int getOperador() {
	    return operador;
	}

	public void setOperador(int operador) {
	    this.operador = operador;
	}
	
	
	
}
