package actions;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LogoutAction extends ActionSupport{
	
	public LogoutAction()
	{
		
	}

	public String execute() throws Exception { 
		Map session = ActionContext.getContext().getSession();
		session.remove("logged-in"); 
		session.remove("nombre");
		session.remove("usuarioLog");
		return SUCCESS;
	}
	
}
