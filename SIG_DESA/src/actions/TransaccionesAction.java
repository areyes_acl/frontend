package actions;

import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import types.TransactionRadioSearchType;
import Beans.AccionTransaccion;
import Beans.TransaccionBean;
import Beans.TransaccionOnusBean;
import Beans.Usuario;
import clases.TiposTransaccionesDAO;
import clases.TransaccionesDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase que consulta los diversos tipos de transacciones existentes.
 * 
 * @version 1.0, 20/08/2013
 * @author RADS
 */
public class TransaccionesAction extends ActionSupport implements
	ServletRequestAware {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    static final Logger log = Logger.getLogger(TransaccionesDAO.class);
    private String listaTransacciones;
    private String numeroTarjeta;
    private String tipoBusqueda;
    private String fechaInicio;
    private String fechaTermino;
    private String tipoObjecionRechazo;
    private String codigoTransaccion;
    private String codigoFuncion;
    private String op;
    private List<AccionTransaccion> listaTipoTransacciones;

    private String numPagina;
    private String cantReg;
    private String pagActual;
    private Long idSubmenu;
    private Long idMenu;

    // Mod Pag
    private HttpServletRequest request;
    private TransaccionesDAO transaccionPresentacion;
    private Vector<TransaccionBean> listaTransaccionPresentacion;
    private Vector<TransaccionOnusBean> listaTransaccionOnUs;
    private TiposTransaccionesDAO tipoTransaccionesDAO;

    // Mod Pag

    public TransaccionesAction() {
	transaccionPresentacion = new TransaccionesDAO();
	tipoTransaccionesDAO = new TiposTransaccionesDAO();
    }

    public String execute() throws Exception {

	listaTipoTransacciones = tipoTransaccionesDAO.cargarTiposDeBusquedaDeTransacciones(TransactionRadioSearchType.GESTION);

	Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
	usuarioLog.setCreacion(0);
	usuarioLog.setLectura(0);
	usuarioLog.setActualizacion(0);
	usuarioLog.setEliminacion(0);

	Integer creacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 2);
	usuarioLog.setCreacion(creacion);
	Integer actualizacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 3);
	usuarioLog.setActualizacion(actualizacion);

	return "SUCCESS";
    }
    
    public String gestTrxPendientes() throws Exception {
	Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
	usuarioLog.setCreacion(0);
	usuarioLog.setLectura(0);
	usuarioLog.setActualizacion(0);
	usuarioLog.setEliminacion(0);

	Integer lectura = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 1);
	usuarioLog.setLectura(lectura);
	Integer eliminar = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 4);
	usuarioLog.setEliminacion(eliminar);

	return "SUCCESS";
    }

    /**
     * Metodo que consulta las transacciones de presentacion.
     * 
     * @return Lista de transacciones de presentacion.
     * @throws Mensaje
     *             de excepcion controlado.
     * @param.
     */
    public String buscarTransaccionPresentacion() {
	// TransaccionesDAO transaccionPresentacion = new TransaccionesDAO();

	listaTransaccionPresentacion = transaccionPresentacion
		.buscarTransaccionPresentacion(numeroTarjeta, fechaInicio,
			fechaTermino, op, numPagina);
	
	
	request.setAttribute("listaTransacciones", listaTransaccionPresentacion);
	request.setAttribute("ultimaPagina",
		transaccionPresentacion.getUltimaPagina());
	request.setAttribute("numPagina",
		transaccionPresentacion.getPagActual());
	request.setAttribute("fechaInicioPag", fechaInicio);
	request.setAttribute("fechaTerminoPag", fechaTermino);

	return "SUCCESS";
    }

    /**
     * Metodo que consulta las transacciones de presentacion de visa.
     * 
     * @return Lista de transacciones de presentacion de visa.
     * @throws Mensaje
     *             de excepcion controlado.
     * @param.
     */
    public String buscarTransaccionPresentacionVisa() {
	// TransaccionesDAO transaccionPresentacion = new TransaccionesDAO();
	listaTransaccionPresentacion = transaccionPresentacion
		.buscarTransaccionPresentacionVisa(numeroTarjeta, fechaInicio,
			fechaTermino, op, numPagina, codigoFuncion);

	request.setAttribute("listaTransacciones", listaTransaccionPresentacion);
	request.setAttribute("ultimaPagina",
		transaccionPresentacion.getUltimaPagina());
	request.setAttribute("numPagina",
		transaccionPresentacion.getPagActual());
	request.setAttribute("fechaInicioPag", fechaInicio);
	request.setAttribute("fechaTerminoPag", fechaTermino);

	return "SUCCESS";
    }

    /**
     * Metodo que consulta las transacciones de objecion o rechazos.
     * 
     * @return Lista de transacciones de objecion o rechazos.
     * @throws Mensaje
     *             de excepcion controlado.
     * @param.
     */
    public String buscarTransaccionObjecionRechazo() {
	log.info("==== Buscar Transaccion Objecion Rechazo ====");
	TransaccionesDAO transaccionPresentacion = new TransaccionesDAO();
	listaTransaccionPresentacion = transaccionPresentacion
		.buscarTransaccionObjecionRechazo(numeroTarjeta, fechaInicio,
			fechaTermino, tipoObjecionRechazo, op, numPagina,
			tipoBusqueda);

	request.setAttribute("listaTransacciones", listaTransaccionPresentacion);
	request.setAttribute("ultimaPagina",
		transaccionPresentacion.getUltimaPagina());
	request.setAttribute("numPagina",
		transaccionPresentacion.getPagActual());
	request.setAttribute("fechaInicioPag", fechaInicio);
	request.setAttribute("fechaTerminoPag", fechaTermino);

	return "SUCCESS";
    }

    /**
     * Metodo que consulta las transacciones asociadas a la accion seleccionada.
     * 
     * @return Lista de transacciones segun accion.
     * @throws Mensaje
     *             de excepcion controlado.
     * @param.
     */
    public String buscarTransaccionAcciones() {

	TransaccionesDAO transaccionPresentacion = new TransaccionesDAO();
	listaTransaccionPresentacion = transaccionPresentacion
		.buscarTransaccionAcciones(numeroTarjeta, fechaInicio,
			fechaTermino, codigoTransaccion, codigoFuncion, op,
			numPagina, tipoBusqueda);

	request.setAttribute("listaTransacciones", listaTransaccionPresentacion);
	request.setAttribute("ultimaPagina",
		transaccionPresentacion.getUltimaPagina());
	request.setAttribute("numPagina",
		transaccionPresentacion.getPagActual());
	request.setAttribute("fechaInicioPag", fechaInicio);
	request.setAttribute("fechaTerminoPag", fechaTermino);

	return "SUCCESS";
    }

    /**
     * Metodo que consulta las transacciones de recuperacion de cobro de cargos
     * incoming.
     * 
     * @return Lista de transacciones de recuperacion de cobro de cargos
     *         incoming.
     * @throws Mensaje
     *             de excepcion controlado.
     * @param.
     */
    public String buscarTransaccionRecuperacionCargosInc() {

	TransaccionesDAO transaccionPresentacion = new TransaccionesDAO();
	listaTransaccionPresentacion = transaccionPresentacion
		.buscarTransaccionRecuperacionCargosIncoming(numeroTarjeta,
			fechaInicio, fechaTermino, codigoTransaccion, op,
			numPagina);

	request.setAttribute("listaTransacciones", listaTransaccionPresentacion);
	request.setAttribute("ultimaPagina",
		transaccionPresentacion.getUltimaPagina());
	request.setAttribute("numPagina",
		transaccionPresentacion.getPagActual());
	request.setAttribute("fechaInicioPag", fechaInicio);
	request.setAttribute("fechaTerminoPag", fechaTermino);

	return "SUCCESS";
    }

    /**
     * Metodo que consulta las transacciones de recuperacion de cobro de cargos
     * de outgoing.
     * 
     * @return Lista de transacciones de recuperacion de cobro de cargos
     *         outgoing.
     * @throws Mensaje
     *             de excepcion controlado.
     * @param.
     */
    public String buscarTransaccionRecuperacionCargosOut() {

	TransaccionesDAO transaccionPresentacion = new TransaccionesDAO();
	listaTransaccionPresentacion = transaccionPresentacion
		.buscarTransaccionRecuperacionCargosOutgoing(numeroTarjeta,
			fechaInicio, fechaTermino, codigoTransaccion, op,
			numPagina);

	request.setAttribute("listaTransacciones", listaTransaccionPresentacion);
	request.setAttribute("ultimaPagina",
		transaccionPresentacion.getUltimaPagina());
	request.setAttribute("numPagina",
		transaccionPresentacion.getPagActual());
	request.setAttribute("fechaInicioPag", fechaInicio);
	request.setAttribute("fechaTerminoPag", fechaTermino);

	return "SUCCESS";
    }

    /**
     * Metodo que consulta las transacciones de confirmacion de recepcion de
     * peticion de vales.
     * 
     * @return Lista de transacciones de confirmacion de recepcion de peticion
     *         de vales.
     * @throws Mensaje
     *             de excepcion controlado.
     * @param.
     */
    public String buscarTransaccionConfirmacionRecepcionPeticionVales() {
	String[] codigo;
	String vKey = "";

	// SE UNIFICA CODIGO
	if (codigoTransaccion != null) {
	    codigo = codigoTransaccion.split("_");
	    if (codigo.length < 2) {
		vKey = codigo[0] + "_" + codigoFuncion;
	    } else {
		vKey = codigoTransaccion;
	    }
	}

	listaTransaccionPresentacion = transaccionPresentacion
		.buscarTransaccionConfirmacionRecepcionPeticionVales(
			numeroTarjeta, fechaInicio, fechaTermino, vKey,
			numPagina, tipoBusqueda);

	request.setAttribute("listaTransacciones", listaTransaccionPresentacion);
	request.setAttribute("ultimaPagina",
		transaccionPresentacion.getUltimaPagina());
	request.setAttribute("numPagina",
		transaccionPresentacion.getPagActual());
	request.setAttribute("fechaInicioPag", fechaInicio);
	request.setAttribute("fechaTerminoPag", fechaTermino);

	return "SUCCESS";
    }

    /**
     * Metodo que consulta las transacciones de On Us de visa.
     * 
     * @return Lista de transacciones de On Us de visa.
     * @throws Mensaje
     *             de excepcion controlado.
     * @param.
     */
    public String buscarTransaccionOnUsVisa() {
	// TransaccionesDAO transaccionPresentacion = new TransaccionesDAO();

	listaTransaccionOnUs = transaccionPresentacion
		.buscarTransaccionOnUsVisa(numeroTarjeta, fechaInicio,
			fechaTermino, op, numPagina, codigoFuncion);

	request.setAttribute("listaTransacciones", listaTransaccionOnUs);
	request.setAttribute("ultimaPagina",
		transaccionPresentacion.getUltimaPagina());
	request.setAttribute("numPagina",
		transaccionPresentacion.getPagActual());
	request.setAttribute("fechaInicioPag", fechaInicio);
	request.setAttribute("fechaTerminoPag", fechaTermino);

	return "SUCCESS";
    }

    /**
     * Definicion de gettres y setters para las variables parametricas.
     * 
     */
    public String getCodigoTransaccion() {
	return codigoTransaccion;
    }

    public void setCodigoTransaccion(String codigoTransaccion) {
	this.codigoTransaccion = codigoTransaccion;
    }

    public String getCodigoFuncion() {
	return codigoFuncion;
    }

    public void setCodigoFuncion(String codigoFuncion) {
	this.codigoFuncion = codigoFuncion;
    }

    public String getTipoObjecionRechazo() {
	return tipoObjecionRechazo;
    }

    public void setTipoObjecionRechazo(String tipoObjecionRechazo) {
	this.tipoObjecionRechazo = tipoObjecionRechazo;
    }

    public String getListaTransacciones() {
	return listaTransacciones;
    }

    public void setListaTransacciones(String listaTransacciones) {
	this.listaTransacciones = listaTransacciones;
    }

    public String getNumeroTarjeta() {
	return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
	this.numeroTarjeta = numeroTarjeta;
    }

    public String getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
	return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
	this.fechaTermino = fechaTermino;
    }

    public String getOp() {
	return op;
    }

    public void setOp(String op) {
	this.op = op;
    }

    public void setServletRequest(HttpServletRequest httpServletRequest) {
	this.request = httpServletRequest;
    }

    public String getNumPagina() {
	return numPagina;
    }

    public void setNumPagina(String numPagina) {
	this.numPagina = numPagina;
    }

    public String getCantReg() {
	return cantReg;
    }

    public String getTipoBusqueda() {
	return tipoBusqueda;
    }

    public void setTipoBusqueda(String tipoBusqueda) {
	this.tipoBusqueda = tipoBusqueda;
    }

    public void setCantReg(String cantReg) {
	this.cantReg = cantReg;
    }

    public String getPagActual() {
	return pagActual;
    }

    public void setPagActual(String pagActual) {
	this.pagActual = pagActual;
    }

    public List<AccionTransaccion> getListaTipoTransacciones() {
	return listaTipoTransacciones;
    }

    public void setListaTipoTransacciones(
	    List<AccionTransaccion> listaTipoTransacciones) {
	this.listaTipoTransacciones = listaTipoTransacciones;
    }

    public Long getIdSubmenu() {
        return idSubmenu;
    }

    public void setIdSubmenu(Long idSubmenu) {
        this.idSubmenu = idSubmenu;
    }

    public Long getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(Long idMenu) {
        this.idMenu = idMenu;
    }
}
