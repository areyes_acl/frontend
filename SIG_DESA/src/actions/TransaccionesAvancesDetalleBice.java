package actions;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */

public class TransaccionesAvancesDetalleBice implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3978071019742624448L;
    private String sid;
    private String tipoRegistro;
    private String numOperPro;
    private String codServicio;
    private String rutOrdenante;
    private String numCtaOrdenante;
    private String idOperCliente;
    private String codEstado;
    private String glosaEstado;
    private String fechaInstruccion;
    private String fechaContable;
    private String codBancoBenefi;
    private String tipoCtaBenefi;
    private String ctaBenefi;
    private String rutBenefi;
    private String nomBenefi;
    private String monto;

    public String getSid() {
	return sid;
    }

    public void setSid(String sid) {
	this.sid = sid;
    }

    public String getTipoRegistro() {
	return tipoRegistro;
    }

    public void setTipoRegistro(String tipoRegistro) {
	this.tipoRegistro = tipoRegistro;
    }

    public String getNumOperPro() {
	return numOperPro;
    }

    public void setNumOperPro(String numOperPro) {
	this.numOperPro = numOperPro;
    }

    public String getCodServicio() {
	return codServicio;
    }

    public void setCodServicio(String codServicio) {
	this.codServicio = codServicio;
    }

    public String getRutOrdenante() {
	return rutOrdenante;
    }

    public void setRutOrdenante(String rutOrdenante) {
	this.rutOrdenante = rutOrdenante;
    }

    public String getNumCtaOrdenante() {
	return numCtaOrdenante;
    }

    public void setNumCtaOrdenante(String numCtaOrdenante) {
	this.numCtaOrdenante = numCtaOrdenante;
    }

    public String getIdOperCliente() {
	return idOperCliente;
    }

    public void setIdOperCliente(String idOperCliente) {
	this.idOperCliente = idOperCliente;
    }

    public String getCodEstado() {
	return codEstado;
    }

    public void setCodEstado(String codEstado) {
	this.codEstado = codEstado;
    }

    public String getGlosaEstado() {
	return glosaEstado;
    }

    public void setGlosaEstado(String glosaEstado) {
	this.glosaEstado = glosaEstado;
    }

    public String getFechaInstruccion() {
	return fechaInstruccion;
    }

    public void setFechaInstruccion(String fechaInstruccion) {
	this.fechaInstruccion = fechaInstruccion;
    }

    public String getFechaContable() {
	return fechaContable;
    }

    public void setFechaContable(String fechaContable) {
	this.fechaContable = fechaContable;
    }

    public String getCodBancoBenefi() {
	return codBancoBenefi;
    }

    public void setCodBancoBenefi(String codBancoBenefi) {
	this.codBancoBenefi = codBancoBenefi;
    }

    public String getTipoCtaBenefi() {
	return tipoCtaBenefi;
    }

    public void setTipoCtaBenefi(String tipoCtaBenefi) {
	this.tipoCtaBenefi = tipoCtaBenefi;
    }

    public String getCtaBenefi() {
	return ctaBenefi;
    }

    public void setCtaBenefi(String ctaBenefi) {
	this.ctaBenefi = ctaBenefi;
    }

    public String getRutBenefi() {
	return rutBenefi;
    }

    public void setRutBenefi(String rutBenefi) {
	this.rutBenefi = rutBenefi;
    }

    public String getNomBenefi() {
	return nomBenefi;
    }

    public void setNomBenefi(String nomBenefi) {
	this.nomBenefi = nomBenefi;
    }

    public String getMonto() {
	return monto;
    }

    public void setMonto(String monto) {
	this.monto = monto;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((sid == null) ? 0 : sid.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TransaccionesAvancesDetalleBice other = (TransaccionesAvancesDetalleBice) obj;
	if (sid == null) {
	    if (other.sid != null)
		return false;
	} else if (!sid.equals(other.sid))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "TransaccionesDetalleAvances [sid=" + sid + ", tipoRegistro="
		+ tipoRegistro + ", numOperPro=" + numOperPro
		+ ", codServicio=" + codServicio + ", rutOrdenante="
		+ rutOrdenante + ", numCtaOrdenante=" + numCtaOrdenante
		+ ", idOperCliente=" + idOperCliente + ", codEstado="
		+ codEstado + ", glosaEstado=" + glosaEstado
		+ ", fechaInstruccion=" + fechaInstruccion + ", fechaContable="
		+ fechaContable + ", codBancoBenefi=" + codBancoBenefi
		+ ", tipoCtaBenefi=" + tipoCtaBenefi + ", ctaBenefi="
		+ ctaBenefi + ", rutBenefi=" + rutBenefi + ", nomBenefi="
		+ nomBenefi + ", monto=" + monto + "]";
    }
    
    

}
