package actions;

import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import types.StatusPagoType;
import Beans.AsientoContable;
import Beans.PagoContableBean;
import Beans.ParametroBean;
import Beans.Usuario;
import cl.util.Constantes;
import cl.util.Utils;
import clases.CommonsDAO;
import clases.PagoDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase controlador para la consulta de pagos pendientes o procesados.
 * 
 * 
 * @author dmedina
 * 
 */
public class ConsultarPagoAction extends ActionSupport implements SessionAware,
	ServletRequestAware {

    private static final long serialVersionUID = 1L;
    static final Logger log = Logger.getLogger(ConsultarPagoAction.class);
    private HttpServletRequest request;
    private PagoDAO pagoDAO;
    private CommonsDAO commonsDao;
    private String fechaInicio;
    private String fechaTermino;
    private String estadoPago;
    private String numPagina;
    private List<AsientoContable> listaAsientoContable;
    private List<PagoContableBean> listaPagos;
    private int cantReg;
    private String filenameExcel;
    private Map<String, Object> sessionMap;
    private static final String PARAMETRO_STARWITH_EXCEL = "FORMAT_FILENAME_PAGO_XLS";
    private static final String PARAMETRO_RUTA_EXCEL = "PATH_FILENAME_PAGO_XLS";
    private List<ParametroBean> listaParametros;
    private File adjuntoExcel;

    private int codigoError;
    private String mensajeError;
    
    //OPERADOR
    private int operador;
    
    // MANEJO DE MENUS
    private Long idSubmenu;
    private Long idMenu;
    private Long sidPago;
    
    //variable de prueba para renderizado en struts
    private String mensage;
    

    public File getAdjuntoExcel() {
	return adjuntoExcel;
    }

    public void setAdjuntoExcel(File adjuntoExcel) {
	this.adjuntoExcel = adjuntoExcel;
    }

    public Long getSidPago() {
	return sidPago;
    }

    public void setSidPago(Long sidPago) {
	this.sidPago = sidPago;
    }

    public Map<String, Object> getSessionMap() {
	return sessionMap;
    }

    public void setSessionMap(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;
    }

    public ConsultarPagoAction() {
    }

    public int getCantReg() {
	return cantReg;
    }

    public void setCantReg(int cantReg) {
	this.cantReg = cantReg;
    }

    public String getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
	return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
	this.fechaTermino = fechaTermino;
    }

    public String getEstadoPago() {
	return estadoPago;
    }

    public void setEstadoPago(String estadoPago) {
	this.estadoPago = estadoPago;
    }

    public String getNumPagina() {
	return numPagina;
    }

    public void setNumPagina(String numPagina) {
	this.numPagina = numPagina;
    }

    public List<AsientoContable> getListaAsientoContable() {
	return listaAsientoContable;
    }

    public void setListaAsientoContable(
	    List<AsientoContable> listaAsientoContable) {
	this.listaAsientoContable = listaAsientoContable;
    }

    public List<PagoContableBean> getListaPagos() {
	return listaPagos;
    }

    public void setListaPagos(List<PagoContableBean> listaPagos) {
	this.listaPagos = listaPagos;
    }

    public void setServletRequest(HttpServletRequest httpServletRequest) {
	this.request = httpServletRequest;
    }

    public String getFilenameExcel() {
	return filenameExcel;
    }

    public void setFilenameExcel(String filenameExcel) {
	this.filenameExcel = filenameExcel;
    }

    public List<ParametroBean> getListaParametros() {
	return listaParametros;
    }

    public void setListaParametros(List<ParametroBean> listaParametros) {
	this.listaParametros = listaParametros;
    }

    public String detalleContableDiarioIndex() {
	return "SUCCESS";
    }

    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;

    }

    public Long getIdSubmenu() {
	return idSubmenu;
    }

    public void setIdSubmenu(Long idSubmenu) {
	this.idSubmenu = idSubmenu;
    }

    public Long getIdMenu() {
	return idMenu;
    }

    public void setIdMenu(Long idMenu) {
	this.idMenu = idMenu;
    }

    public int getCodigoError() {
	return codigoError;
    }

    public String getMensajeError() {
	return mensajeError;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String consultarPagosIndex() {

	Usuario usuarioLog = (Usuario) sessionMap.get("usuarioLog");
	if (usuarioLog != null) {
	    usuarioLog.setCreacion(0);
	    usuarioLog.setLectura(0);
	    usuarioLog.setActualizacion(0);
	    usuarioLog.setEliminacion(0);

	    Integer actualizacion = usuarioLog.getPermisoSubMenu(idMenu,
		    idSubmenu, 3);
	    usuarioLog.setActualizacion(actualizacion);

	    if (actualizacion == 1) {
		return "SUCCESS";
	    }
	}

	return "FAILED";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String buscarPagoContable() {
	log.info("==== Buscar Pago Contable ====");
	try {
	    commonsDao = new CommonsDAO();
	    //Integer sidOperador = commonsDao.getOperador(operador);
	    pagoDAO = new PagoDAO();
	    listaPagos = pagoDAO.buscarPago(fechaInicio, fechaTermino,
		    StatusPagoType.findByXkey(estadoPago), operador);
	    request.setAttribute("listaPagos", listaPagos);
	    log.info(listaPagos);
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	}

	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo encargado de agregar el xls en pagos autorizados que no fueron
     * adjuntados sus excel
     * 
     * @return
     * @since 1.X
     */
    public String actualizarPagoConAdjuntoXLS() {
	try {
	    PagoContableBean pago = obtenerPago();
	    PagoDAO pagoDAO = new PagoDAO();

	    // GUARDA EL PAGO EN BASE DE DATOS
	    codigoError = pagoDAO.actualizarArchivoXlsPagoPorId(pago.getSid(),
		    pago.getFilenameAdjuntoExcel());

	    log.info("Se actualiza pago con excel ===>");
	    log.info("filenameExcel : " + filenameExcel);
	    log.info("adjuntoExcel : " + adjuntoExcel);

	    if (codigoError == 0) {
		// BUSCA RUTA EN LA PRTS DONDE SE DEJARA EL ARCHIVO TEMPORAL
		String ruta_excel = buscarPorNombreParametro(PARAMETRO_RUTA_EXCEL);

		if (!filenameExcel.contentEquals("")) {

		    Utils.moveFileTo(pago.getFilenameAdjuntoExcel(),
			    ruta_excel, adjuntoExcel);
		}
	    }
	    codigoError = 0;
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    mensajeError = "Ha ocurrido un error: ".concat(e.getMessage());
	    codigoError = -1;
	}

	return "SUCCESS";
    }

    /**
     * Metodo que seatea un pago
     * 
     * @return
     */
    private PagoContableBean obtenerPago() {

	cargaListaParametros(Utils.GRUPO_PRTS_PAGO);
	PagoContableBean pago = new PagoContableBean();

	Long sidUsuario = (Long) sessionMap.get("sidusuario");
	pago.setSid(sidPago);
	pago.setFilenameAdjuntoExcel(generateFilenameExcel());
	pago.setSidUsuario(sidUsuario);

	return pago;
    }

    /**
     * Metodo que genera el filenameFinal del archivo adjunto
     * 
     * @return
     */
    private String generateFilenameExcel() {
	// BUSCA NOMBRE EN LA PRTS DEL PARAMETRO STARWITH
	String starwith = buscarPorNombreParametro(PARAMETRO_STARWITH_EXCEL);
	String extension = Utils.obtenerExtensionArchivo(filenameExcel);
	String secuencial = Utils.getDateToday();

	// OBTIENE EL SECUENCIAL DE LA FECHA PARA EL NOMBRE DE ARCHIVO
	String filenameFinal = (starwith).concat(secuencial).concat(".")
		.concat(extension);

	return filenameFinal;
    }

    /**
     * Metodo que busca dentro de la lista de parametros el nombre de el
     * parametro y retorna su valor asociado
     * 
     * @return
     */
    private String buscarPorNombreParametro(String nombreParametro) {
	for (ParametroBean parametro : listaParametros) {
	    if (parametro.getCodDato().equalsIgnoreCase(nombreParametro)) {
		return parametro.getValor();
	    }
	}

	return null;
    }

    /**
     * 
     * Metodo que carga la lista de parametros dado un grupo
     */
    private void cargaListaParametros(String codGrupo) {
	CommonsDAO commonDao = new CommonsDAO();

	try {
	    listaParametros = commonDao.obtenerParametrosPorGrupo(codGrupo);
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}
    }

    public int getOperador() {
	return operador;
    }

    public void setOperador(int operador) {
	this.operador = operador;
    }

    public String getMensage() {
	return mensage;
    }

    public void setMensage(String mensage) {
	this.mensage = mensage;
    }

}
