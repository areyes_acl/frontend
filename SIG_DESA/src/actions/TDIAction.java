package actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import properties.UtilProperties;
import Beans.TDIConexionTipoBean;
import Beans.TDIJobBean;
import Beans.TDIJobValidacionBean;
import Beans.TDITipoValidacionBean;
import clases.TDIConexionTipoDAO;
import clases.TDIJobsDAO;
import clases.TDITipoValidacionDAO;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class TDIAction extends ActionSupport {
    private static final long serialVersionUID = -7123469716521196713L;
    
    static final Logger LOGGER = Logger.getLogger(TDIAction.class);
    
    private TDIJobsDAO tdiJobsDAO;
    private TDIConexionTipoDAO tdiconexionDAO;
    private TDITipoValidacionDAO tdivalidacionDAO;

    private int page;
    private int ultimaPagina;
    private int valorInicial = 0;
    private int cantidadResultados;
    private String searchArchivo;
    
    private int resultadoAction = 0;
    
    private int showId;
    
    private String horaInicio = "";
    private String horaTermino = "";
    
    private String proceso;
    private String origenPath;
    private String destinoPath;
    private List<TDIConexionTipoBean> tiposConexion;
    private List<TDITipoValidacionBean> tiposValidacion;
    private List<TDIJobBean> jobs;
    
    private TDIJobBean jobWork;
    private int idJobWork;
    
    private int origenTipo;
    private int destinoTipo;
    private String origenIP;
    private String destinoIP;
    
    private int creado;
    
    private String origenPuerto;
    private String destinoPuerto;
    
    private String origenUsuario;
    private String origenPass;
    private String destinoUsuario;
    private String destinoPass;

    private String archivo;
    private String archivoExt;
    private String control;
    private String controlExt;
    
    private int alerta;
    private String cadena;
    
    private String reintentos;
    private String reintentosMin;
    
    private int ejecucionHora;
    private int ejecucionMinuto;
    
    
    private String activo;
    
    private String jsonString;
    
    private String validaciones;
    
    private String variabilidad;
    private String variabilidadPorciento;
    private String variabilidadLineas;
    
    public TDIAction() {
	tdiJobsDAO = new TDIJobsDAO();
	tdiconexionDAO = new TDIConexionTipoDAO();
	tdivalidacionDAO = new TDITipoValidacionDAO();
	jobs = new ArrayList<TDIJobBean>(); 
	page = 1;
	cantidadResultados = Integer.parseInt(UtilProperties.getProperty("TDICantidadRegistros"));
	searchArchivo = "";
	idJobWork = 0;
	jobWork = new TDIJobBean();
	showId = 0;
    }

    public String gestionTDI() { // Renderiza plantilla 1
	filter();
	return Action.SUCCESS;
    }

    public String gestionTDISearchResult() { // Renderiza plantilla 2
	LOGGER.info("Valor con que llego la variable creado" + creado);
	filter();
	return Action.SUCCESS;
    }

    private void filter() {
	resultadoAction = 0;
	
	LOGGER.info("La hora inicio a filtrar es: " + horaInicio);
	LOGGER.info("La hora fin a filtrar es: " + horaTermino);
	
	/*if (horaInicio.length() > 0 || horaTermino.length() > 0) {
	    SimpleDateFormat formatDate = new SimpleDateFormat("HH:mm");
	    try {
		Date inicio = formatDate.parse(horaInicio);
		Date fin = formatDate.parse(horaTermino);
		if (inicio.after(fin)) {
		    resultadoAction = 3;
		}
	    } catch (Exception e) {
		LOGGER.info( "Error al parsear el parametro de filtrado horaInicio o  horaTermino",e);
		resultadoAction = 1;
	    }
	}*/
	
	LOGGER.info("Validador de hora : " + resultadoAction);
	//if (resultadoAction == 0) {
	ultimaPagina = tdiJobsDAO.getUltimaPagina(searchArchivo);
	LOGGER.info("valor ultima pagina "+ ultimaPagina);
	jobs = tdiJobsDAO.findAll(page, searchArchivo);
	 if (jobs.isEmpty()) {
		resultadoAction = 2;
	    }
	valorInicial = (page - 1) * cantidadResultados;
	//}
    }
    

    public String detalleTDI() {
	if(showId > 0){
	    jobWork = tdiJobsDAO.getById(showId);
	}
	return Action.SUCCESS;
    }

    public String crearTDI() {
	tiposConexion = tdiconexionDAO.findAll();
	tiposValidacion = tdivalidacionDAO.findAll();
	if(idJobWork > 0){
	    LOGGER.info("LLego el id a editar es: "+idJobWork);
	    jobWork = tdiJobsDAO.getById(idJobWork);
	}
	LOGGER.info("Cantidad de registros encontrados: "+tiposConexion.size());
	return Action.SUCCESS;
    }

    public String guardarTDI() {
	int idproceso = tdiJobsDAO.getJobByProceso(proceso);
	boolean bandera = true;
	LOGGER.info("VALOR QUE LLEGA COMO NOMBRE ARCHIVO: "+archivo);
	if(idJobWork > 0){
	    LOGGER.info("LLego el id a editar es: "+idJobWork);
	    if(idproceso != 0 && idproceso != idJobWork){
		LOGGER.info("Existe un proceso con el mismo nombre");
		bandera = false;
		jsonString = "2";
	    }
	}else{
	    if(idproceso == 0){
		    bandera = true;
		}else{
		    jsonString = "2";
		    bandera = false;
		}
	}
	
	if(bandera){
	//Validaciones 
	JSONParser parser = new JSONParser();
	List<TDIJobValidacionBean> valid = new ArrayList<TDIJobValidacionBean>();

            Object obj;
	    try {
		obj = parser.parse(validaciones);
		    JSONArray valida = (JSONArray) obj;
	            LOGGER.info("validaciones que llegaron: "+valida.toString());
	            Iterator<JSONObject> iterator = valida.iterator();
	            while (iterator.hasNext()) {
	        	JSONObject jsonObject = iterator.next();
	        	TDIJobValidacionBean val = new TDIJobValidacionBean(null, new TDITipoValidacionBean(Long.parseLong(String.valueOf(jsonObject.get("id")))), (String)jsonObject.get("origen"), (String)jsonObject.get("destino"));
	        	valid.add(val);
	        	LOGGER.info("EL ID DE LA VAL ES " +jsonObject.get("id"));
	            }
	    } catch (ParseException e1) {
		LOGGER.info("Error al parsear la expresino "+e1);
	    }
	    
	SimpleDateFormat ejecucion = new SimpleDateFormat("HH:mm");
	Date runner = null;
	try {
	    runner = ejecucion.parse(ejecucionHora+":"+ejecucionMinuto);
	} catch (Exception e) {
	    LOGGER.info("hora de ejecucion con problemas para parsear "+ e);
	}
	try{
	    if(idJobWork > 0){
		// Si estoy editando ---
		TDIJobBean job = new TDIJobBean(Long.parseLong(String.valueOf(idJobWork)), proceso, origenPath, destinoPath, new TDIConexionTipoBean(Long.parseLong(String.valueOf(origenTipo))), new TDIConexionTipoBean(Long.parseLong(String.valueOf(destinoTipo))), origenIP, destinoIP, this.convertirValorInt(origenPuerto), this.convertirValorInt(destinoPuerto), origenUsuario, origenPass, destinoUsuario, destinoPass,  archivo,archivoExt,  this.convertirValorInt(control),  controlExt, alerta, cadena, this.convertirValorInt(reintentos), this.convertirValorInt(reintentosMin), runner, this.convertirValorInt(activo), valid, this.convertirValorInt(variabilidad), this.convertirValorInt(variabilidadPorciento), this.convertirValorInt(variabilidadLineas));
		tdiJobsDAO.updateJob(job);
		jsonString = "3";
	    }else{
		// Si estoy creando ---
		TDIJobBean job = new TDIJobBean(null, proceso, origenPath, destinoPath, new TDIConexionTipoBean(Long.parseLong(String.valueOf(origenTipo))), new TDIConexionTipoBean(Long.parseLong(String.valueOf(destinoTipo))), origenIP, destinoIP, this.convertirValorInt(origenPuerto), this.convertirValorInt(destinoPuerto), origenUsuario, origenPass, destinoUsuario, destinoPass,  archivo,archivoExt,  this.convertirValorInt(control),  controlExt, alerta, cadena, this.convertirValorInt(reintentos), this.convertirValorInt(reintentosMin), runner, this.convertirValorInt(activo), valid, this.convertirValorInt(variabilidad), this.convertirValorInt(variabilidadPorciento), this.convertirValorInt(variabilidadLineas));
		tdiJobsDAO.createJob(job);
		jsonString = "1";
	    }
		
	}catch(Exception e){
	    LOGGER.info(e);
	    jsonString = "0";
	}
	}
	return Action.SUCCESS;
    }
    
    private int convertirValorInt(String propiedad){
	int valor = 0;
	try{
	    valor = Integer.parseInt(propiedad);
	}catch(Exception e){
	    
	}
	return valor;
    }

    public String editarTDI() {
	tiposConexion = tdiconexionDAO.findAll();
	tiposValidacion = tdivalidacionDAO.findAll();
	LOGGER.info("Cantidad de registros encontrados: "+tiposConexion.size());
	return Action.SUCCESS;
    }
    
    public int getCantidadResultados() {
        return cantidadResultados;
    }

    public void setCantidadResultados(int cantidadResultados) {
        this.cantidadResultados = cantidadResultados;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    public int getResultadoAction() {
        return resultadoAction;
    }

    public void setResultadoAction(int resultadoAction) {
        this.resultadoAction = resultadoAction;
    }

    public int getUltimaPagina() {
        return ultimaPagina;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraTermino() {
        return horaTermino;
    }

    public void setHoraTermino(String horaTermino) {
        this.horaTermino = horaTermino;
    }

    public void setUltimaPagina(int ultimaPagina) {
        this.ultimaPagina = ultimaPagina;
    }

    public List<TDITipoValidacionBean> getTiposValidacion() {
        return tiposValidacion;
    }

    public void setTiposValidacion(List<TDITipoValidacionBean> tiposValidacion) {
        this.tiposValidacion = tiposValidacion;
    }

    public int getEjecucionHora() {
        return ejecucionHora;
    }

    public void setEjecucionHora(int ejecucionHora) {
        this.ejecucionHora = ejecucionHora;
    }

    public int getEjecucionMinuto() {
        return ejecucionMinuto;
    }

    public void setEjecucionMinuto(int ejecucionMinuto) {
        this.ejecucionMinuto = ejecucionMinuto;
    }

    public String getReintentos() {
        return reintentos;
    }

    public void setReintentos(String reintentos) {
        this.reintentos = reintentos;
    }

    public String getReintentosMin() {
        return reintentosMin;
    }

    public void setReintentosMin(String reintentosMin) {
        this.reintentosMin = reintentosMin;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getArchivoExt() {
        return archivoExt;
    }

    public void setArchivoExt(String archivoExt) {
        this.archivoExt = archivoExt;
    }

    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    public String getControlExt() {
        return controlExt;
    }

    public void setControlExt(String controlExt) {
        this.controlExt = controlExt;
    }

    public String getOrigenUsuario() {
        return origenUsuario;
    }

    public void setOrigenUsuario(String origenUsuario) {
        this.origenUsuario = origenUsuario;
    }

    public String getOrigenPass() {
        return origenPass;
    }

    public void setOrigenPass(String origenPass) {
        this.origenPass = origenPass;
    }

    public String getDestinoUsuario() {
        return destinoUsuario;
    }

    public void setDestinoUsuario(String destinoUsuario) {
        this.destinoUsuario = destinoUsuario;
    }

    public String getDestinoPass() {
        return destinoPass;
    }

    public void setDestinoPass(String destinoPass) {
        this.destinoPass = destinoPass;
    }

    public String getOrigenPuerto() {
        return origenPuerto;
    }

    public void setOrigenPuerto(String origenPuerto) {
        this.origenPuerto = origenPuerto;
    }

    public String getDestinoPuerto() {
        return destinoPuerto;
    }

    public void setDestinoPuerto(String destinoPuerto) {
        this.destinoPuerto = destinoPuerto;
    }

    public String getOrigenIP() {
        return origenIP;
    }

    public void setOrigenIP(String origenIP) {
        this.origenIP = origenIP;
    }

    public String getDestinoIP() {
        return destinoIP;
    }

    public void setDestinoIP(String destinoIP) {
        this.destinoIP = destinoIP;
    }

    public int getOrigenTipo() {
        return origenTipo;
    }

    public void setOrigenTipo(int origenTipo) {
        this.origenTipo = origenTipo;
    }

    public int getDestinoTipo() {
        return destinoTipo;
    }

    public void setDestinoTipo(int destinoTipo) {
        this.destinoTipo = destinoTipo;
    }

    public String getOrigenPath() {
	return origenPath;
    }

    public void setOrigenPath(String origenPath) {
	this.origenPath = origenPath;
    }

    public String getDestinoPath() {
	return destinoPath;
    }

    public void setDestinoPath(String destinoPath) {
	this.destinoPath = destinoPath;
    }

    public String getProceso() {
	return proceso;
    }

    public void setProceso(String proceso) {
	this.proceso = proceso;
    }



   

    public List<TDIConexionTipoBean> getTiposConexion() {
        return tiposConexion;
    }

    public void setTiposConexion(List<TDIConexionTipoBean> tiposConexion) {
        this.tiposConexion = tiposConexion;
    }

    public List<TDIJobBean> getJobs() {
        return jobs;
    }

    public void setJobs(List<TDIJobBean> jobs) {
        this.jobs = jobs;
    }

    public int getValorInicial() {
        return valorInicial;
    }

    public void setValorInicial(int valorInicial) {
        this.valorInicial = valorInicial;
    }

    public String getSearchArchivo() {
        return searchArchivo;
    }

    public void setSearchArchivo(String searchArchivo) {
        this.searchArchivo = searchArchivo;
    }

    public int getCreado() {
        return creado;
    }

    public void setCreado(int creado) {
        this.creado = creado;
    }

    public int getShowId() {
        return showId;
    }

    public void setShowId(int showId) {
        this.showId = showId;
    }

    public int getAlerta() {
        return alerta;
    }

    public void setAlerta(int alerta) {
        this.alerta = alerta;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getValidaciones() {
        return validaciones;
    }

    public void setValidaciones(String validaciones) {
        this.validaciones = validaciones;
    }

    public int getIdJobWork() {
        return idJobWork;
    }

    public void setIdJobWork(int idJobWork) {
        this.idJobWork = idJobWork;
    }

    public TDIJobBean getJobWork() {
        return jobWork;
    }

    public void setJobWork(TDIJobBean jobWork) {
        this.jobWork = jobWork;
    }

    public String getVariabilidad() {
        return variabilidad;
    }

    public void setVariabilidad(String variabilidad) {
        this.variabilidad = variabilidad;
    }

    public String getVariabilidadPorciento() {
        return variabilidadPorciento;
    }

    public void setVariabilidadPorciento(String variabilidadPorciento) {
        this.variabilidadPorciento = variabilidadPorciento;
    }

    public String getVariabilidadLineas() {
        return variabilidadLineas;
    }

    public void setVariabilidadLineas(String variabilidadLineas) {
        this.variabilidadLineas = variabilidadLineas;
    }
}
