package actions;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import Beans.GestionPresentacionBean;
import Beans.Usuario;
import clases.GestionPorCuentaDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase que permite consultar los movimientos de las transacciones.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class GestionPorCuentaAction extends ActionSupport implements ServletRequestAware{
	
	private String numeroTarjeta;
	private String numerotarjeta;
	private String fechaInicio;
	private String fechaTermino;
	private int    sidIncomming;
	//sidIncomming
	private String listaGestionPorCuentas;
	private String listaPresentaciones;
	private String listaGestionCargoAbono;
	private String cerrarTransacResp;
	private Long idMenu;
	private Long idSubmenu;
	
	
	
	public Long getIdMenu() {
	    return idMenu;
	}

	public void setIdMenu(Long idMenu) {
	    this.idMenu = idMenu;
	}

	public Long getIdSubmenu() {
	    return idSubmenu;
	}

	public void setIdSubmenu(Long idSubmenu) {
	    this.idSubmenu = idSubmenu;
	}


	private String numPagina;
	public String getNumPagina() {
		return numPagina;
	}

	
	private HttpServletRequest request;
	
	public void setNumPagina(String numPagina) {
		this.numPagina = numPagina;
	}

	private String cantReg;
	private String pagActual;
	
	private Vector<GestionPresentacionBean> listaGestionPresentacion;
	
	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @since 1.X
	 */
	public String gestionesDiariasIndex(){
	    
	    Usuario usuarioLog = (Usuario) request.getSession().getAttribute("usuarioLog");
		Integer actualizacion = usuarioLog.getPermisoSubMenu(idMenu, idSubmenu, 3);
		usuarioLog.setActualizacion(actualizacion);
	    
	    return ActionSupport.SUCCESS;
	}
	
	
	
	/**
	 * Metodo que obtiene las transacciones de presentacion.
	 * @return lista de transacciones de presentacion.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarGestionPorCuentaPresentaciones(){
		//modificar ATG
		GestionPorCuentaDAO gestionPorCuenta = new GestionPorCuentaDAO();
		
		
		String[] temp = fechaInicio.split("/");
		String fi= temp[2]+temp[1]+temp[0];
		temp = fechaTermino.split("/");
		String ff = temp[2]+temp[1]+temp[0];
		
		numeroTarjeta = numerotarjeta;
		
		
		listaGestionPresentacion = gestionPorCuenta.buscarGestionPorCuentaPresentacionesPag(numeroTarjeta, fi, ff, numPagina);
		
		
		request.setAttribute("listaTransacciones", listaGestionPresentacion);
		request.setAttribute("ultimaPagina", gestionPorCuenta.getUltimaPagina());
		request.setAttribute("numPagina", gestionPorCuenta.getPagActual());
		request.setAttribute("fechaInicioPag", fechaInicio);
		request.setAttribute("fechaTerminoPag", fechaTermino);
		
		
		
		
		return "SUCCESS";
	}
	
	/**
	 * Metodo que obtiene el historial de la transaccion.
	 * @return lista de transacciones.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarGestionPorCuenta(){
		
		GestionPorCuentaDAO gestionPorCuenta = new GestionPorCuentaDAO();
		listaGestionPorCuentas = gestionPorCuenta.buscarGestionPorCuenta(numeroTarjeta, fechaInicio, fechaTermino, sidIncomming);
		return "SUCCESS";
	}
	
	/**
	 * Metodo que obtiene los cargos y abonos realizados a la transaccion.
	 * @return lista de cargos y abonos.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarGestionCuentaCargosAbonos(){
		GestionPorCuentaDAO gestionPorCuenta = new GestionPorCuentaDAO();
		listaGestionCargoAbono = gestionPorCuenta.buscarGestionCuentaCargosAbonos(fechaInicio, fechaTermino, sidIncomming);
		return "SUCCESS";
	}
	
	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	public String getListaGestionCargoAbono() {
		return listaGestionCargoAbono;
	}
	
	public String exeCerrarTransaccion()
	{
		GestionPorCuentaDAO gestionPorCuenta = new GestionPorCuentaDAO();
		try {
			cerrarTransacResp = gestionPorCuenta.cerrarTransaccion(sidIncomming);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "SUCCESS";
	}

	public void setListaGestionCargoAbono(String listaGestionCargoAbono) {
		this.listaGestionCargoAbono = listaGestionCargoAbono;
	}

	public int getSidIncomming() {
		return sidIncomming;
	}

	public void setSidIncomming(String sidIncomming) {
		if(sidIncomming != null)
			this.sidIncomming = Integer.parseInt(sidIncomming);
		/*if(sidIncomming == "")
			this.sidIncomming = sidIncomming;*/
	}
	
	public String getListaPresentaciones() {
		return listaPresentaciones;
	}

	public void setListaPresentaciones(String listaPresentaciones) {
		this.listaPresentaciones = listaPresentaciones;
	}
	
	public String getListaGestionPorCuentas() {
		return listaGestionPorCuentas;
	}

	public void setListaGestionPorCuentas(String listaGestionPorCuentas) {
		this.listaGestionPorCuentas = listaGestionPorCuentas;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(String fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public String getCerrarTransacResp() {
		return cerrarTransacResp;
	}

	public void setCerrarTransacResp(String cerrarTransacResp) {
		this.cerrarTransacResp = cerrarTransacResp;
	}

	public void setCantReg(String cantReg) {
		this.cantReg = cantReg;
	}

	public String getCantReg() {
		return cantReg;
	}

	public void setPagActual(String pagActual) {
		this.pagActual = pagActual;
	}

	public String getPagActual() {
		return pagActual;
	}

	public void setServletRequest(HttpServletRequest httpServletRequest)
	{
		this.request = httpServletRequest;
	}

	public void setNumerotarjeta(String numerotarjeta) {
		this.numerotarjeta = numerotarjeta;
	}

	public String getNumerotarjeta() {
		return numerotarjeta;
	}

}
