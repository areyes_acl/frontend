package actions;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import clases.PeticionContraCargosDAO;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase que permite generar contra cargos.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class PeticionContraCargosAction extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	static final Logger log =  Logger.getLogger(PeticionContraCargosAction.class);

	
	private String      xkey;
	private String      listaRazones;
	private String      mensajegrabacion;
	private String      vmit;
	private int         sidtransaccion;
	private String      codigorazon;
	private BigDecimal  vmontoconciliacion;
	private int         sidusuario;


	public String execute() throws Exception {
		return "SUCCESS";
	}

	/**
	 * Metodo que consulta las razones para peticion de vales y contracargos.
	 * @return  Lista de razones para peticiones de vales o contracargos.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String buscarRazonesPeticionContraCargos(){
		PeticionContraCargosDAO peticionValeContraCargo = new PeticionContraCargosDAO();
		listaRazones = peticionValeContraCargo.buscarRazonesPeticionContraCargos(xkey);
		return "SUCCESS";
	}
	
	/**
	 * Metodo que graba la peticion de vale contracargo.
	 * @return  Mensaje de grabacion.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String grabarPeticionValeContraCargos(){
		PeticionContraCargosDAO peticionValeContraCargos = new PeticionContraCargosDAO();
		mensajegrabacion = peticionValeContraCargos.grabarPeticionValeContraCargos(xkey, vmit, sidtransaccion, codigorazon, vmontoconciliacion, sidusuario);
		return "SUCCESS";
	}
	
	
	/**
	 * Definicion de gettres y setters para las variables parametricas.
	 * 
	 */
	public int getSidusuario() {
		return sidusuario;
	}

	public void setSidusuario(int sidusuario) {
		this.sidusuario = sidusuario;
	}
	
	public String getVmit() {
		return vmit;
	}

	public void setVmit(String vmit) {
		this.vmit = vmit;
	}

	public int getSidtransaccion() {
		return sidtransaccion;
	}

	public void setSidtransaccion(int sidtransaccion) {
		this.sidtransaccion = sidtransaccion;
	}

	public String getCodigorazon() {
		return codigorazon;
	}

	public void setCodigorazon(String codigorazon) {
		this.codigorazon = codigorazon;
	}

	public BigDecimal getVmontoconciliacion() {
		return vmontoconciliacion;
	}

	public void setVmontoconciliacion(BigDecimal vmontoconciliacion) {
		this.vmontoconciliacion = vmontoconciliacion;
	}

	public String getXkey() {
		return xkey;
	}

	public void setXkey(String xkey) {
		this.xkey = xkey;
	}
	
	public String getListaRazones() {
		return listaRazones;
	}

	public void setListaRazones(String listaRazones) {
		this.listaRazones = listaRazones;
	}

	public String getMensajegrabacion() {
		return mensajegrabacion;
	}

	public void setMensajegrabacion(String mensajegrabacion) {
		this.mensajegrabacion = mensajegrabacion;
	}
	
}