package actions;

import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import types.TransactionRadioSearchType;

import Beans.AccionTransaccion;
import Beans.ObjecionRechazoDiaBean;

import com.opensymphony.xwork2.ActionSupport;

import clases.ObjecionRechazoDiaDAO;
import clases.TiposTransaccionesDAO;

public class ObjecionRechazoDiaAction extends ActionSupport implements ServletRequestAware{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	ObjecionRechazoDiaDAO orDAO;
	
	private Vector<ObjecionRechazoDiaBean> listaTransaccion;
	private List<AccionTransaccion> listaTipoTransacciones;
	static final Logger log =  Logger.getLogger(ObjecionRechazoDiaAction.class);
	
	private String tipoObjecionRechazo;
	private String fechaInicio;
	private String fechaTermino;
	
	private String numPagina;
	private String cantReg;
	private String pagActual;
	//private String tipotransaccion;
	private TiposTransaccionesDAO tipoTransaccionesDAO;
	
	// Testing
	

	private HttpServletRequest request;

	public ObjecionRechazoDiaAction(){
		orDAO = new ObjecionRechazoDiaDAO();
		tipoTransaccionesDAO= new TiposTransaccionesDAO();
	}
	
	/**
	 * execute
	 */
	public String execute() throws Exception {

		return "SUCCESS";
	}

	public String getTipoObjecionRechazo() {
		return tipoObjecionRechazo;
	}

	public void setTipoObjecionRechazo(String tipoObjecionRechazo) {
		this.tipoObjecionRechazo = tipoObjecionRechazo;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(String fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public String getNumPagina() {
		return numPagina;
	}

	public void setNumPagina(String numPagina) {
		this.numPagina = numPagina;
	}

	public String getCantReg() {
		return cantReg;
	}

	public void setCantReg(String cantReg) {
		this.cantReg = cantReg;
	}

	public Vector<ObjecionRechazoDiaBean> getListaTransaccion() {
		return listaTransaccion;
	}

	public void setListaTransaccion() {
	    log.info("LOG " + tipoObjecionRechazo + " num pa " + numPagina  +" fechaInicio "+fechaInicio);
	    log.info(this.listaTransaccion);
	    log.info(orDAO);
	    this.listaTransaccion = orDAO.listaRechazoObjecion(fechaInicio, fechaTermino, tipoObjecionRechazo, numPagina);
	
		
	}
	
	public String listarObjecionRechazo()
	{
		setListaTransaccion();
		//getListaTransaccion();
		log.info("LOG " + tipoObjecionRechazo + " num pa " + numPagina  +" fechaInicio "+fechaInicio);
		request.setAttribute("listaTransaccion", getListaTransaccion());
		request.setAttribute("ultimaPagina", orDAO.getUltimaPagina());
		request.setAttribute("numPagina",orDAO.getPagActual());
		request.setAttribute("fechaInicioPag", fechaInicio);
		request.setAttribute("fechaTerminoPag", fechaTermino);
		
		return "SUCCESS";
	}
	
	public String index(){
	     return "SUCCESS";
	}
	
	
	public String cargarListaTipoBusqueda(){
	    // CARGA LA LISTA DE LOS CHECKBOX A LISTAR
        listaTipoTransacciones = tipoTransaccionesDAO.cargarTiposDeBusquedaDeTransacciones( TransactionRadioSearchType.CONSULTA );
        
        return "SUCCESS";
	}
	

	public void setServletRequest(HttpServletRequest httpServletRequest)
	{
		this.request = httpServletRequest;
	}

	public String getPagActual() {
		return pagActual;
	}

	public void setPagActual(String pagActual) {
		this.pagActual = pagActual;
	}

    public List<AccionTransaccion> getListaTipoTransacciones() {
        return listaTipoTransacciones;
    }

    public void setListaTipoTransacciones(
            List<AccionTransaccion> listaTipoTransacciones ) {
        this.listaTipoTransacciones = listaTipoTransacciones;
    }
	
	
}
