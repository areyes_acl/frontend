package actions;

import Beans.OtraGestionBean;
import clases.OtrasGestionesDAO;

public class OtrasGestionesAction {

	private String sid;
	private String sidusuario;
	private String tpGestiones;
	private String glosa;
	private String numIncidente;
	private String ejecutaObjecion;

	OtrasGestionesDAO otrasGestionesDAO;
	private String listaTpOtrasGestiones;

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getSidusuario() {
		return sidusuario;
	}

	public void setSidusuario(String sidusuario) {
		this.sidusuario = sidusuario;
	}

	public String getTpGestiones() {
		return tpGestiones;
	}

	public void setTpGestiones(String tpGestiones) {
		this.tpGestiones = tpGestiones;
	}

	public String getGlosa() {
		return glosa;
	}

	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}

	public String getNumIncidente() {
		return numIncidente;
	}

	public void setNumIncidente(String numIncidente) {
		this.numIncidente = numIncidente;
	}

	public String getEjecutaObjecion() {
		return ejecutaObjecion;
	}

	public void setEjecutaObjecion(String ejecutaObjecion) {
		this.ejecutaObjecion = ejecutaObjecion;
	}

	public OtrasGestionesAction() {
		if (otrasGestionesDAO == null)
			otrasGestionesDAO = new OtrasGestionesDAO();
	}

	public String getListaTpOtrasGestiones() {
		return listaTpOtrasGestiones;
	}

	public void setListaTpOtrasGestiones() {
		this.listaTpOtrasGestiones = otrasGestionesDAO.obtenerTipoGestion();
	} 

	public String obtenerTipos() {
		setListaTpOtrasGestiones();
		return "SUCCESS";
	}

	public String crearNuevaGestion() {
		OtraGestionBean otraGestionBean = crearInstancia();
		ejecutaObjecion = otrasGestionesDAO.crearOtraGestion(otraGestionBean);
		return "SUCCESS";
	}

	public OtraGestionBean crearInstancia() {
		OtraGestionBean otraGestionBean = new OtraGestionBean();
		otraGestionBean.setRefTransaccion(Integer.parseInt(sid));
		otraGestionBean.setRefTpGestion(Integer.parseInt(tpGestiones));
		otraGestionBean.setRefUsuario(Integer.parseInt(sidusuario));
		otraGestionBean.setGlosa(glosa);
		otraGestionBean.setNumeroIncidente(numIncidente);
		return otraGestionBean;
	}

}