package actions;

import clases.RptCmpnscPagoDAO;

import com.opensymphony.xwork2.ActionSupport;

public class RepCmpncsPagoVisaAction extends ActionSupport{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String fecDesde;
    private String fecHasta;
    private String listaCPago;
    private int operador;
    public String getFecDesde() {
        return fecDesde;
    }
    public void setFecDesde(String fecDesde) {
        this.fecDesde = fecDesde;
    }
    public String getFecHasta() {
        return fecHasta;
    }
    public void setFecHasta(String fecHasta) {
        this.fecHasta = fecHasta;
    }
    public String getListaCPago() {
        return listaCPago;
    }
    public void setListaCPago(String listaCPago) {
        this.listaCPago = listaCPago;
    }
    public int getOperador() {
        return operador;
    }
    public void setOperador(int operador) {
        this.operador = operador;
    }
    
    
    public String cargarInformeCmpnscnVisaPago() {
	RptCmpnscPagoDAO rptCmpnscPagoDAO = new RptCmpnscPagoDAO();
	listaCPago = rptCmpnscPagoDAO.listarCmpnsPagoVisa(fecDesde, fecHasta, operador);
	return "SUCCESS";
}
    

}
