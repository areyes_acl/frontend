package actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import types.SearchType;
import types.StatusTransactionType;
import types.TransactionType;
import Beans.TransaccionBean;
import Beans.Usuario;
import cl.filter.FiltroTransacciones;
import cl.util.excel.impl.ExcelTrxPendientes;
import cl.util.excel.service.ExcelUtils;
import clases.GestionTrxDAO;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

import exception.AppException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * Clase de acci�n para el m�dulo de gesti�n de transacciones pendientes de
 * incoming
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class GestionTrxPendientesAction extends ActionSupport implements
	SessionAware, ServletRequestAware {

    /**
	 * 
	 */
    private static final long serialVersionUID = 8245088195354581505L;
    static final Logger log = Logger
	    .getLogger(GestionTrxPendientesAction.class);

    // VARIABLE DE SESSION
    private Map<String, Object> sessionMap;

    // VARIABLES PARA RENDER TABLA
    private HttpServletRequest request;

    // VARIABLES DE FILTRO
    private String fechaDesde;
    private String fechaHasta;
    private String tipoTrx;
    private String estado;
    private String numeroTarjeta;
    private List<TransaccionBean> listaTrx;
    private String operador;
    private String codOperador;

    private String nombreArchivo;

    // VARIABLES PAGINACION
    private String numPagina;
    private String cantReg;
    private String pagActual;

    // VARIABLES PARA ELIMINAR
    private String sidTransaccion;

    // VARIABLE DE RESPUESTA ACCIONES
    private int codigoError;
    private String mensajeError;

    // VARIABLE PARA EXPORT DE CSV
    private FileInputStream transactionStream;
    private int cantidadElementos;

    private String listaOperadores;
    
    private String listaProductos;

    /**
     * GETTERS Y SETTERS
     * 
     * @return
     */

    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;
    }

    @Override
    public void setServletRequest(HttpServletRequest httpServlet) {
	this.request = httpServlet;

    }

    public Map<String, Object> getSessionMap() {
	return sessionMap;
    }

    public String getFechaDesde() {
	return fechaDesde;
    }

    public void setFechaDesde(String fechaDesde) {
	this.fechaDesde = fechaDesde;
    }

    public String getFechaHasta() {
	return fechaHasta;
    }

    public void setFechaHasta(String fechaHasta) {
	this.fechaHasta = fechaHasta;
    }

    public String getTipoTrx() {
	return tipoTrx;
    }

    public void setTipoTrx(String tipoTrx) {
	this.tipoTrx = tipoTrx;
    }

    public String getEstado() {
	return estado;
    }

    public void setEstado(String estado) {
	this.estado = estado;
    }

    public String getNumeroTarjeta() {
	return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
	this.numeroTarjeta = numeroTarjeta;
    }

    public List<TransaccionBean> getListaTrx() {
	return listaTrx;
    }

    public void setListaTrx(List<TransaccionBean> listaTrx) {
	this.listaTrx = listaTrx;
    }

    public String getNumPagina() {
	return numPagina;
    }

    public void setNumPagina(String numPagina) {
	this.numPagina = numPagina;
    }

    public String getCantReg() {
	return cantReg;
    }

    public void setCantReg(String cantReg) {
	this.cantReg = cantReg;
    }

    public String getPagActual() {
	return pagActual;
    }

    public void setPagActual(String pagActual) {
	this.pagActual = pagActual;
    }

    public String getSidTransaccion() {
	return sidTransaccion;
    }

    public void setSidTransaccion(String sidTransaccion) {
	this.sidTransaccion = sidTransaccion;
    }

    public int getCodigoError() {
	return codigoError;
    }

    public void setCodigoError(int codigoError) {
	this.codigoError = codigoError;
    }

    public String getMensajeError() {
	return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
	this.mensajeError = mensajeError;
    }

    public FileInputStream getTransactionStream() {
	return transactionStream;
    }

    public void setTransactionStream(FileInputStream transactionStream) {
	this.transactionStream = transactionStream;
    }

    public int getCantidadElementos() {
	return cantidadElementos;
    }

    public void setCantidadElementos(int cantidadElementos) {
	this.cantidadElementos = cantidadElementos;
    }

    public String getOperador() {
	return operador;
    }

    public void setOperador(String operador) {
	this.operador = operador;
    }

    public String cargarOperadores() {
	setListaOperadores();
	getListaOperadores();
	return "SUCCESS";
    }
    
    public String cargarProductos() {
	setListaProductos();
	getListaProductos();
	return "SUCCESS";
    }

    public String getListaOperadores() {
	return listaOperadores;
    }

    public void setListaOperadores() {
	GestionTrxDAO dao = new GestionTrxDAO();
	this.listaOperadores = dao.getListaOperadores();

    }
    
    public String getListaProductos() {
	return listaProductos;
    } 

    public void setListaProductos() {
	GestionTrxDAO dao = new GestionTrxDAO();
	this.listaProductos = dao.getListaProductos(Integer.parseInt(operador));

    }

    public String getNombreArchivo() {
	return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
	this.nombreArchivo = nombreArchivo;
    }
    

    public String getCodOperador() {
        return codOperador;
    }

    public void setCodOperador(String codOperador) {
        this.codOperador = codOperador;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * M�todo de accion que se encarga de recibir la petici�n de b�squeda de
     * transacciones desde la p�gina JSP, llama a setear el filtro de busqueda y
     * realiza la consulta a BD para obtener las transacciones que cumplan con
     * los criterios de b�squedas seleecionados
     * 
     * @return
     * @since 1.X
     */
    public String buscarTransacciones() {
	GestionTrxDAO gestionTrxDAO = null;
	try {
	    FiltroTransacciones filtro = setearFiltroBusqueda(SearchType.CON_PAGINACION);
	    gestionTrxDAO = new GestionTrxDAO();
	    listaTrx = gestionTrxDAO.buscarTrx(filtro, numPagina);

	    cantidadElementos = (listaTrx != null) ? listaTrx.size() : 0;

	    log.info(gestionTrxDAO.getUltimaPagina());
	    log.info(gestionTrxDAO.getPagActual());
	    log.info(fechaDesde);
	    log.info(fechaHasta);
	    log.info("TIPO TRX : " + tipoTrx);
	    log.info(operador);

	    request.setAttribute("listaTransacciones", listaTrx);
	    request.setAttribute("ultimaPagina",gestionTrxDAO.getUltimaPagina());
	    request.setAttribute("numPagina", gestionTrxDAO.getPagActual());
	    request.setAttribute("fechaInicioPag", fechaDesde);
	    request.setAttribute("fechaTerminoPag", fechaHasta);
	    request.setAttribute("FILTRO_OPERADOR", operador);
	    request.setAttribute("TIPO TRX", tipoTrx);

	    log.info(listaTrx);
	    
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return ActionSupport.SUCCESS;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo de acci�n que recibe la petici�n para deshacer una transaccion
     * (por sid) que se encuentre en estado pendiente de envio por incoming, a
     * su estado anterior. Si es contracargo => valida si tiene pet_vale =>
     * puede quedar como pet. vale o objeci�n. Si es pet vale => queda como
     * objeci�n. Si es segundo contracargo queda como => representaci�n
     * 
     * 
     * @return
     * @since 1.X
     */
    public String eliminarTransaccion() {
	GestionTrxDAO gestionTrxDAO = null;

	TransactionType tipo = TransactionType.findByXkey(tipoTrx);
	log.info("SID TRANSACCION A ELIMINAR : " + sidTransaccion);
	log.info("TIPO TRANSACCION A ELIMINAR : " + tipo);

	try {
	    gestionTrxDAO = new GestionTrxDAO();
	    gestionTrxDAO.eliminarTransaccion(sidTransaccion, tipo);
	    codigoError = 0;
	    mensajeError = "Se ha realizado la eliminaci�n de la transaccion correctamente";
	} catch (AppException e) {
	    log.error(e.getMessage());
	    mensajeError = e.getMessage();
	    codigoError = -1;
	}

	return Action.SUCCESS;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * M�todo de acci�n que exporta transacciones
     * 
     * @return
     * @since 1.X
     */
    public String exportarTrx() {
	GestionTrxDAO gestionTrxDAO = null;
	codigoError = 0;

	try {
	    // INICIALIZA EL DAO
	    
	    if (gestionTrxDAO == null) {
		gestionTrxDAO = new GestionTrxDAO();
	    }

	    FiltroTransacciones filtro = setearFiltroBusqueda(SearchType.SIN_PAGINACION);
	    listaTrx = gestionTrxDAO.buscarTrx(filtro, null);
	    
	    setNombreArchivo(getCodOperador());

	    log.info("recibe: " + operador);

	    ExcelUtils excelUtils = new ExcelTrxPendientes();
	    File archivo = excelUtils.export(listaTrx);

	    this.transactionStream = new FileInputStream(archivo);

	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    codigoError = -1;
	}

	return Action.SUCCESS;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que recibe desde la vista los datos para realizar la busqueda de
     * transacciones pendientes de incoming
     * 
     * @param username
     * @return
     * @throws Exception
     * @since 1.X
     */
    private FiltroTransacciones setearFiltroBusqueda(SearchType tipoBusqueda)
	    throws Exception {

	Usuario user = (Usuario) sessionMap.get("usuarioLog");
	FiltroTransacciones filtro = new FiltroTransacciones();
	filtro.setFechaDesde(fechaDesde);
	filtro.setFechaHasta(fechaHasta);
	filtro.setNumeroTarjeta(numeroTarjeta);
	filtro.setSidUsuario(user.getSid());
	filtro.setEstado(StatusTransactionType.findByXkey(estado));
	filtro.setTipoTransaccion(TransactionType.findByXkey(tipoTrx));
	filtro.setTipoBusqueda(tipoBusqueda);
	filtro.setOperador(operador);

	log.info(filtro);
	return filtro;
    }

}
