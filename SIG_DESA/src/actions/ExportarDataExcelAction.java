package actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import Beans.CargaMasivaDTO;
import cl.util.Constantes;
import clases.ExportarDataExcelDAO;
import clases.InformesPDSDAO;

import com.opensymphony.xwork2.ActionSupport;

import db.OracleBD;

/**
 * Clase que permite que invoca el metodo de genetacion de archivo de
 * exportacion de datos a excel.
 * 
 * @version 1.0, 20/08/2013
 * @author RADS
 */
public class ExportarDataExcelAction extends ActionSupport implements
	SessionAware {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    static final Logger log = Logger.getLogger(ExportarDataExcelAction.class);
    private String dataExcel;
    private String nombreArchivo;
    private String respuestaExcel;
    private String fechaInicio;
    private String componente;
    private String numeroTarjeta;
    private String tipoTransaccion;
    private String tipoBusqueda;
    private Integer ano;
    private Integer trimestre;
    private Map<String, Object> sessionMap;
    
    private String operador;
    private String codOperador;
    
    private String listaOperadores;

    private String fechaFin;
    private InputStream fileInputStream;
    private String fileName;
    private File file;
    
    private Integer estado;
    private Integer estadoGestion;
    private Integer estadoDetalle;
    
    private List<Long> listaSeleccionada;

    
    private String[] listaTodos;
    
    public String getOperador() {
		return operador;
	    }
	
	    public void setOperador(String operador) {
		this.operador = operador;
	    }
	
	    public String cargarOperadores() {
		setListaOperadores();
		getListaOperadores();
		return "SUCCESS";
	    }

	    public String getListaOperadores() {
		return listaOperadores;
	    }
	
	    public void setListaOperadores() {
		InformesPDSDAO dao = new InformesPDSDAO();
		this.listaOperadores = dao.getOperadores();
	
	    }

    public String getCodOperador() {
	        return codOperador;
	    }

	    public void setCodOperador(String codOperador) {
	        this.codOperador = codOperador;
	    }

    public String[] getListaTodos() {
        return listaTodos;
    }

    public void setListaTodos(String[] listaTodos) {
        this.listaTodos = listaTodos;
    }

    public List<Long> getListaSeleccionada() {
        return listaSeleccionada;
    }

    public void setListaSeleccionada(List<Long> listaSeleccionada) {
        this.listaSeleccionada = listaSeleccionada;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getEstadoGestion() {
        return estadoGestion;
    }

    public void setEstadoGestion(Integer estadoGestion) {
        this.estadoGestion = estadoGestion;
    }

    public Integer getEstadoDetalle() {
        return estadoDetalle;
    }

    public void setEstadoDetalle(Integer estadoDetalle) {
        this.estadoDetalle = estadoDetalle;
    }

    public String getTipoTransaccion() {
	return tipoTransaccion;
    }

    public Integer getAno() {
	return ano;
    }

    public void setAno(Integer ano) {
	this.ano = ano;
    }

    public Integer getTrimestre() {
	return trimestre;
    }

    public void setTrimestre(Integer trimestre) {
	this.trimestre = trimestre;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
	this.tipoTransaccion = tipoTransaccion;
    }

    public String getNumeroTarjeta() {
	return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
	this.numeroTarjeta = numeroTarjeta;
    }

    public String getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
	return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
	this.fechaFin = fechaFin;
    }

    public String getComponente() {
	return componente;
    }

    public void setComponente(String componente) {
	this.componente = componente;
    }

    /**
     * Definicion de gettres y setters para las variables parametricas.
     * 
     */
    public String getDataExcel() {
	return dataExcel;
    }

    public InputStream getFileInputStream() {
	return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
	this.fileInputStream = fileInputStream;
    }

    public String getFileName() {
	return fileName;
    }

    public void setFileName(String fileName) {
	this.fileName = fileName;
    }

    public File getFile() {
	return file;
    }

    public void setFile(File file) {
	this.file = file;
    }

    public void setDataExcel(String dataExcel) {
	this.dataExcel = dataExcel;
    }

    public String getNombreArchivo() {
	return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
	this.nombreArchivo = nombreArchivo;
    }

    public String getRespuestaExcel() {
	return respuestaExcel;
    }

    public void setRespuestaExcel(String respuestaExcel) {
	this.respuestaExcel = respuestaExcel;
    }

    public String getTipoBusqueda() {
	return tipoBusqueda;
    }

    public void setTipoBusqueda(String tipoBusqueda) {
	this.tipoBusqueda = tipoBusqueda;
    }

    @Override
    public void setSession(Map<String, Object> map) {
	this.sessionMap = map;

    }

    public String execute() throws Exception {

	return "SUCCESS";
    }

    /**
     * Metodo que genera la planilla excel.
     * 
     * @return Archivo de exportacion .xls
     * @throws Mensaje
     *             de excepcion controlado.
     * @param.
     */
    public String generarPlanillaExcel() throws FileNotFoundException {
	ExportarDataExcelDAO exportar = new ExportarDataExcelDAO();
	file = exportar.generarPlanillaExcel(dataExcel, nombreArchivo);
	setFileInputStream(new FileInputStream(file));
	this.setFileName(file.getName());

	return "SUCCESS";

    }

    public String generarPlanillaExcelPDS() throws FileNotFoundException {

	ExportarDataExcelDAO exportar = new ExportarDataExcelDAO();
	file = exportar.generarPlanillaExcelPDS(fechaInicio, fechaFin,
		nombreArchivo, getOperador());
	
	setNombreArchivo(getCodOperador());
	
	setFileInputStream(new FileInputStream(file));
	this.setFileName(file.getName());

	return "SUCCESS";

    }

    public String generarPlanillaExcelTransacciones()
	    throws FileNotFoundException {

	ExportarDataExcelDAO exportar = new ExportarDataExcelDAO();
	file = exportar.generarPlanillaExcelTxs(fechaInicio, fechaFin,
		numeroTarjeta, tipoTransaccion, nombreArchivo, tipoBusqueda);
	setFileInputStream(new FileInputStream(file));
	this.setFileName(file.getName());

	return "SUCCESS";

    }

    public String generarPlanillaExcelPDSInt() throws FileNotFoundException {

	ExportarDataExcelDAO exportar = new ExportarDataExcelDAO();
	file = exportar.generarPlanillaExcelPDSInt(fechaInicio, fechaFin,
		nombreArchivo);
	setFileInputStream(new FileInputStream(file));
	this.setFileName(file.getName());

	return "SUCCESS";

    }

    public String generarPlanillaExcelObjRech() throws FileNotFoundException {

	ExportarDataExcelDAO exportar = new ExportarDataExcelDAO();
	file = exportar.generarPlanillaExcelObjRech(fechaInicio, fechaFin,
		componente, nombreArchivo);
	setFileInputStream(new FileInputStream(file));
	this.setFileName(file.getName());

	return "SUCCESS";

    }

    /**
     * Metodo que recibe la tabla a exportar y guarda la informacion en la
     * session para no pasar el parametro por la url
     * 
     * @return
     */
    public String prepararExport() {
	log.info("==== PREPARA LA DATA QUE SE EXPORTAR� A EXCEL ====");
	sessionMap.put(Constantes.KEY_DATA_EXCEL, dataExcel);
	sessionMap.put(Constantes.KEY_FILENAME, nombreArchivo);

	log.info(sessionMap);

	return "SUCCESS";
    }

    /**
     * 
     * ACTION DEL METODO QUE PERMITE REALIZAR EL EXPORT GENERICO DE UN EXCEL CON
     * LOS DATOS PASADOS POR SESSION
     * 
     * 
     * @return
     * @throws FileNotFoundException
     */
    public String generarExcel() throws FileNotFoundException {
	log.info("==== EXPORTA REPORTE A EXCEL ====");

	dataExcel = (String) sessionMap.get(Constantes.KEY_DATA_EXCEL);
	nombreArchivo = (String) sessionMap.get(Constantes.KEY_FILENAME);

	log.info("dataExcel : " + dataExcel);
	log.info("nombreArchivo : " + nombreArchivo);

	ExportarDataExcelDAO exportDAO = new ExportarDataExcelDAO();
	file = exportDAO.generarPlanillaExcel(dataExcel, nombreArchivo);
	setFileInputStream(new FileInputStream(file));

	sessionMap.remove(Constantes.KEY_DATA_EXCEL);
	sessionMap.remove(Constantes.KEY_FILENAME);

	log.info(sessionMap);

	return "SUCCESS";
    }
    
    /**
     * 
     * ACTION DEL METODO QUE PERMITE REALIZAR EL EXPORT GENERICO DE UN EXCEL CON
     * LOS DATOS PASADOS POR SESSION
     * 
     * 
     * @return
     * @throws FileNotFoundException
     */
    public String generarExcelTc46() throws FileNotFoundException {
	log.info("==== EXPORTA REPORTE A EXCEL ====");

	dataExcel = (String) sessionMap.get(Constantes.KEY_DATA_EXCEL);
	nombreArchivo = (String) sessionMap.get(Constantes.KEY_FILENAME);

	log.info("dataExcel : " + dataExcel);
	log.info("nombreArchivo : " + nombreArchivo);

	ExportarDataExcelDAO exportDAO = new ExportarDataExcelDAO();
	file = exportDAO.generarPlanillaExcelTc46(dataExcel, nombreArchivo);
	setFileInputStream(new FileInputStream(file));

	sessionMap.remove(Constantes.KEY_DATA_EXCEL);
	sessionMap.remove(Constantes.KEY_FILENAME);

	log.info(sessionMap);

	return "SUCCESS";
    }
    
    

    /**
     * 
     * ACTION DEL METODO QUE PERMITE REALIZAR EL EXPORT GENERICO DE UN EXCEL CON
     * LOS DATOS PASADOS POR SESSION
     * 
     * 
     * @return
     * @throws FileNotFoundException
     */
    public String exportarExcelConciliacionTrxNoAutorizadasIC() throws FileNotFoundException {
	log.info("==== EXPORTA REPORTE CONCILIACIO A EXCEL ====");

	List<TransaccionesAutor> trxNoAutorizadasPorIc = (List<TransaccionesAutor>) sessionMap
		.get(Constantes.TRX_NO_AUTORIZADAS_POR_IC);
	
	log.info(sessionMap.get(Constantes.TRX_NO_AUTORIZADAS_POR_IC));

	nombreArchivo = (String) sessionMap.get(Constantes.KEY_FILENAME);

	log.debug("trxNoAutorizadasPorIc : " + trxNoAutorizadasPorIc);
	log.debug("nombreArchivo : " + nombreArchivo);

	ExportarDataExcelDAO exportDAO = new ExportarDataExcelDAO();
	file = exportDAO.generarExcelConciliacion(trxNoAutorizadasPorIc, nombreArchivo);
	setFileInputStream(new FileInputStream(file));

	return "SUCCESS";
    }
    
    
    /**
     * 
     * ACTION DEL METODO QUE PERMITE REALIZAR EL EXPORT GENERICO DE UN EXCEL CON
     * LOS DATOS PASADOS POR SESSION
     * 
     * 
     * @return
     * @throws FileNotFoundException
     */		  
    public String exportarExcelConciliacionTrxTransbank() throws FileNotFoundException {
	log.info("==== EXPORTA REPORTE CONCILIACIO A EXCEL ====");

	List<TransaccionesAutor> trxNoAutorizadasPorTbk = (List<TransaccionesAutor>) sessionMap
		.get(Constantes.TRX_NO_PAREADAS_TRANSBANK);
	//log.info(sessionMap.get(Constantes.TRX_NO_PAREADAS_TRANSBANK));
	nombreArchivo = (String) sessionMap.get(Constantes.KEY_FILENAME);

	log.info("trxNoAutorizadasPorTbk : " + trxNoAutorizadasPorTbk);
	log.info("nombreArchivo : " + nombreArchivo);

	ExportarDataExcelDAO exportDAO = new ExportarDataExcelDAO();
	file = exportDAO.generarExcelConciliacion(trxNoAutorizadasPorTbk, nombreArchivo);
	setFileInputStream(new FileInputStream(file));

	return "SUCCESS";
    }
    
    /**
     * 
     * ACTION DEL METODO QUE PERMITE REALIZAR EL EXPORT DETALLE DE INTERFACES AVANCES
     * 
     * 
     * @return
     * @throws FileNotFoundException
     */	
    public String generarPlanillaExcelDeatlleInterfaces() throws FileNotFoundException{
	
	ExportarDataExcelDAO exportar = new ExportarDataExcelDAO();
	/*
	 * fechaInicio
	 * fechaFin
	 * estado
	 * estadoGestion
	 * estadoDetalle
	 * */
	
	file = exportar.generarPlanillaExcelInterfaces(fechaInicio, fechaFin, estado, estadoGestion, estadoDetalle);
	setFileInputStream(new FileInputStream(file));
	this.setFileName(file.getName());
	
	return "SUCCESS";
    }
    
    /**
     * 
     * ACTION DEL METODO QUE PERMITE REALIZAR EL EXPORT DETALLE DE INTERFACES AVANCES SELECCIONADAS
     * 
     * 
     */	
    public String generarPlanillaExcelDetalleInterfacesSeleccionadas() throws FileNotFoundException{
	
	ExportarDataExcelDAO exportar = new ExportarDataExcelDAO();
	/*
	 * listaSeleccionada
	 * */
	
	
	String listaIds = "";
	
	for (int i = 0; i < listaTodos.length; i++){
	    listaIds = listaTodos[i];
	}
		
	file = exportar.generarPlanillaExcelInterfacesSeleccionadas(fechaInicio, fechaFin, estado, estadoGestion, estadoDetalle, listaIds);
	setFileInputStream(new FileInputStream(file));
	this.setFileName(file.getName());
	
	return "SUCCESS";
    }
    

}
