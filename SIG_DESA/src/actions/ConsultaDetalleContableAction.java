package actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import sun.tools.tree.ThisExpression;
import weblogic.utils.classloaders.FileSource;
import Beans.AsientoContable;
import Beans.DetalleContableBean;
import Beans.ParametroBean;
import Beans.solicitudBean;
import cl.util.DateTools;
import cl.util.excel.impl.ExcelDetalleContable;
import cl.util.excel.service.ExcelUtils;
import clases.AsientosDAO;
import clases.DetalleContableDAO;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Clase controlador para la consulta de pagos pendientes o procesados.
 * 
 * 
 * @author dmedina
 * 
 */
public class ConsultaDetalleContableAction extends ActionSupport implements
	ServletRequestAware, SessionAware {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private final String COMIENZACON = "DC_";
    static final Logger log = Logger
	    .getLogger(ConsultaDetalleContableAction.class);
    private HttpServletRequest request;
    private AsientosDAO asientosDAO;
    private DetalleContableDAO detalleContable = new DetalleContableDAO(); 
    private String fechaInicio;
    private String fechaTermino;
    private String tipoMov;
    private String estadoPago;
    private String numPagina;
    private List<AsientoContable> listaAsientoContable;
    private List<solicitudBean> listaSolicitudes;
    private List<DetalleContableBean> listaDetalle;
    private Integer idMenu;
    private Integer idSubmenu;
    // VARIABLE PARA EXPORT DE CSV
    private FileInputStream archivoDetalleStream;

    private String fileName;
    private String mensaje;
    
    private int sidConsultaEstado;
    private String estadoConsulta;
    private String fichero;
    private String ruta;
    private int totalRegistros;
    //OPERADOR
    private int operador;
    
    //PRODUCTO
    private int producto;
    
    private Map<String, Object> sessionMap;
    
    public void setSession(Map<String, Object> map) {
	sessionMap = map;
    }

    public String getFileName() {
	return fileName;
    }

    public void setFileName(String fileName) {
	this.fileName = fileName;
    }
    
    public int getSidConsultaEstado() {
        return sidConsultaEstado;
    }

    public void setSidConsultaEstado(int sidConsultaEstado) {
        this.sidConsultaEstado = sidConsultaEstado;
    }

    public String getEstadoConsulta() {
        return estadoConsulta;
    }

    public void setEstadoConsulta(String estadoConsulta) {
        this.estadoConsulta = estadoConsulta;
    }

    public Integer getIdMenu() {
	return idMenu;
    }

    public void setIdMenu(Integer idMenu) {
	this.idMenu = idMenu;
    }

    public Integer getIdSubmenu() {
	return idSubmenu;
    }

    public void setIdSubmenu(Integer idSubmenu) {
	this.idSubmenu = idSubmenu;
    }

    public String getFichero() {
        return fichero;
    }

    public void setFichero(String fichero) {
        this.fichero = fichero;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public int getTotalRegistros() {
        return totalRegistros;
    }

    public void setTotalRegistros(int totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    public FileInputStream getArchivoDetalleStream() {
	return archivoDetalleStream;
    }

    public void setArchivoDetalleStream(FileInputStream archivoDetalleStream) {
	this.archivoDetalleStream = archivoDetalleStream;
    }

    public ConsultaDetalleContableAction() {
	asientosDAO = new AsientosDAO();
    }

    public String getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
	return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
	this.fechaTermino = fechaTermino;
    }

    public String getTipoMov() {
	return tipoMov;
    }

    public void setTipoMov(String tipoMov) {
	this.tipoMov = tipoMov;
    }

    public String getEstadoPago() {
	return estadoPago;
    }

    public void setEstadoPago(String estadoPago) {
	this.estadoPago = estadoPago;
    }

    public String getNumPagina() {
	return numPagina;
    }

    public void setNumPagina(String numPagina) {
	this.numPagina = numPagina;
    }

    public List<AsientoContable> getListaAsientoContable() {
	return listaAsientoContable;
    }

    public void setListaAsientoContable(
	    List<AsientoContable> listaAsientoContable) {
	this.listaAsientoContable = listaAsientoContable;
    }
    
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


    public List<solicitudBean> getListaSolicitudes() {
        return listaSolicitudes;
    }

    public void setListaSolicitudes(List<solicitudBean> listaSolicitudes) {
        this.listaSolicitudes = listaSolicitudes;
    }

    public DetalleContableDAO getDetalleContable() {
	return detalleContable;
    }

    public List<DetalleContableBean> getListaDetalle() {
	return listaDetalle;
    }

    public void setListaDetalle(List<DetalleContableBean> listaDetalle) {
	this.listaDetalle = listaDetalle;
    }

    public void setDetalleContable(DetalleContableDAO detalleContable) {
	this.detalleContable = detalleContable;
    }

    public void setServletRequest(HttpServletRequest httpServletRequest) {
	this.request = httpServletRequest;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String detalleContableDiarioIndex() {
	return "SUCCESS";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * YYYYMMDD
     * 
     * @return
     * @since 1.X
     */
    public String exportarDetalleContable() {
	log.info("======== EXPORTA DETALLE CONTABLE =======");
	
	
	
	Long sidUsuario = (Long) sessionMap.get("sidusuario");

	fileName = fichero;
	try {
	    ParametroBean prts = asientosDAO.getRuta();
	    this.ruta = prts.getValor() + fileName;
	    log.info(this.ruta);
	    
	    // EXPORTA A EXCEL
	    File archivo = new File(this.ruta);
	    this.archivoDetalleStream = new FileInputStream(archivo);
	} catch (IOException  e) {
	    LOG.error(e.getMessage(), e);
	    log.info(e.getMessage());
	}
	return Action.SUCCESS;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String buscarRegistrosContables() {
	log.info("======== BUSCA REGISTROS CONTABLES =======");
	log.info("==  FECHA DESDE = " + fechaInicio);
	log.info("==  FECHA HASTA = " + fechaTermino);
	log.info("==  TIPO MOV    = " + tipoMov);
	log.info("==  OPERADOR    = " + operador);
	log.info("==  PRODUCTO    = " + producto);

	try {
	    listaDetalle = detalleContable.obtenerDetalleContable(fechaInicio, fechaTermino, tipoMov, operador, producto);
	    log.info(listaDetalle);
	} catch (Exception e) {
	   log.error(e.getMessage(), e);
	}
	

	return Action.SUCCESS;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String solicitarRegistrosContables() {
	log.info("======== SOLICITAR REGISTROS CONTABLES =======");
	
	Long sidUsuario = (Long) sessionMap.get("sidusuario");
	fileName = COMIENZACON
		.concat(DateTools.formatStringDate(fechaInicio,
			DateTools.DATE_FORMAT_LABEL,
			DateTools.DATE_FORMAT_CONTABLE_EXCEL))
		.concat("-")
		.concat(DateTools.formatStringDate(fechaTermino,
			DateTools.DATE_FORMAT_LABEL,
			DateTools.DATE_FORMAT_CONTABLE_EXCEL))
		.concat("_"+sidUsuario).concat(".TXT");
	
	log.info("==  FECHA DESDE = " + fechaInicio);
	log.info("==  FECHA HASTA = " + fechaTermino);
	log.info("==  TIPO MOV    = " + tipoMov);
	log.info("==  OPERADOR    = " + operador);
	log.info("==  PRODUCTO    = " + producto);
	log.info("==  USUARIO     = " + sidUsuario);
	log.info("==  FILENAME    = " + fileName );
	log.info("==  FICHERO     = " + fichero);

	try {
	    if(!fichero.equals("")){
		ParametroBean prts = asientosDAO.getRuta();
		this.ruta = prts.getValor() + fichero;
		
		File file = new File(this.ruta);
		if(file.delete()){
		    log.info("Archivo: "+fichero+ " eliminado correctamente");
		}
	    }
	     String msg = asientosDAO.solicitarRegistrosContables(fechaInicio, fechaTermino, tipoMov, operador, producto, sidUsuario, fileName);
	     setMensaje(msg); 
	    log.info(this.mensaje);
	} catch (Exception e) {
	   log.error(e.getMessage(), e);
	}
	

	return "SUCCESS";
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String consultaEstado() {
	
	log.info(sidConsultaEstado);
	try {
	    solicitudBean solicitud = asientosDAO.getById(sidConsultaEstado);
	    this.estadoConsulta = solicitud.getEstado();
	    this.totalRegistros = solicitud.getTotal();
	    log.info(solicitud.getEstado());
	} catch (Exception e) {
	   log.error(e.getMessage(), e);
	}
	

	return "SUCCESS";
    }
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String obtenerTipoMovAsientos() {
	
	
	
	log.info(" Valor que llega de operador = "+operador);
	log.info(" Valor que llega de producto = "+producto);
	try {
	    listaAsientoContable = asientosDAO.getListaTipoMovimientos(operador, producto);
	} catch (SQLException e) {
	    LOG.error(e.getMessage(), e);
	}
	return "SUCCESS";
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String buscarSolicitudes() {
	
	Long sidUsuario = (Long) sessionMap.get("sidusuario");
	
	log.info("Usuario: " + sidUsuario);
	
	try {
	    this.listaSolicitudes = asientosDAO.getSolicitudes(sidUsuario);
	    log.info(this.listaSolicitudes);
	} catch (SQLException e) {
	    LOG.error(e.getMessage(), e);
	}
	return "SUCCESS";
    }
    
    
    public int getOperador() {
	return operador;
    }

    public void setOperador(int operador) {
	this.operador = operador;
    }

    public int getProducto() {
        return producto;
    }

    public void setProducto(int producto) {
        this.producto = producto;
    }
    
    
}
