package actions;

import clases.ConciliacionCDAO;

import com.opensymphony.xwork2.ActionSupport;

public class ConciliacionCAction extends ActionSupport {

	private static final long serialVersionUID = 5947795412573620490L;
	private String fecDesde;
	private String fecHasta;
	private String listaConciliacionC;

	public ConciliacionCAction() {

	}

	public ConciliacionCAction(String fecDesde, String fecHasta) {
		super();
		this.fecDesde = fecDesde;
		this.fecHasta = fecHasta;
	}

	public String getListaConciliacionC() {
		return listaConciliacionC;
	}

	public void setListaConciliacionC(String listaConciliacionC) {
		this.listaConciliacionC = listaConciliacionC;
	}

	public String getFecDesde() {
		return fecDesde;
	}

	public void setFecDesde(String fecDesde) {
		this.fecDesde = fecDesde;
	}

	public String getFecHasta() {
		return fecHasta;
	}

	public void setFecHasta(String fecHasta) {
		this.fecHasta = fecHasta;
	}

	public String conciliacionComisionesIndex(){
	    return "SUCCESS";
	}

	public String cargarConciliacionCmn() {
		ConciliacionCDAO conciliacionCDAO = new ConciliacionCDAO();
		listaConciliacionC = conciliacionCDAO.listarConciliacionCPago(fecDesde, fecHasta);
		return "SUCCESS";
	}

}