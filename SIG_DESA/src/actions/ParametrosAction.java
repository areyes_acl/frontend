package actions;

import com.opensymphony.xwork2.ActionSupport;

import clases.ParametrosDAO;

public class ParametrosAction extends ActionSupport{
	
	private String listaParametros;
	
	private String sid;
	private String plazo;
	
	ParametrosDAO paraDAO;
	
	public ParametrosAction(){
		paraDAO = new ParametrosDAO();
	}
	
	/**
	 * execute
	 */
	public String execute() throws Exception {
		return "SUCCESS";
	}
	
	public String codigosRazonIndex(){
	    return "SUCCESS";
	}
	
	public String getListaParametros()
	{
		return listaParametros;
	}
	
	public void setListaParametros()
	{
		ParametrosDAO paraDAO = new ParametrosDAO();
		
		this.listaParametros = paraDAO.getListaParametros();
	}
	
	public String cargarParametros()
	{
		setListaParametros();
		getListaParametros();
		return "SUCCESS";
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getPlazo() {
		return plazo;
	}

	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}
	
	public void doUpdatePlazo()
	{
		paraDAO.updatePlazo(sid, plazo);
	}
	
	public String updatePlazo(){
		doUpdatePlazo();
		return "SUCCESS";
	}
}
