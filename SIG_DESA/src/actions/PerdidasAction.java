package actions;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import types.SearchType;
import Beans.PerdidaBean;
import Beans.PerdidaDTO;
import cl.filter.FiltroPerdida;
import cl.util.excel.impl.ExcelPerdidas;
import cl.util.excel.service.ExcelUtils;
import clases.PerdidasDAO;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PerdidasAction extends ActionSupport implements
	ServletRequestAware {

    /**
     * 
     */
    private static final long serialVersionUID = 7821723563462515319L;
    private PerdidasDAO perdidaDAO;
    private static final Logger log = Logger.getLogger(PerdidasAction.class);

    // VARIABLES PANTALLA
    private String sidTransaccion;
    private String sidUsuario;
    private String sidTipoGestion;
    private String glosa;
    private String numIncidente;
    private int codigoError;
    private String mensaje;

    // FILTROS BUSQUEDA
    private String fechaInicio;
    private String fechaTermino;
    private String numeroTarjeta;

    // LISTA PERDIDAS
    List<PerdidaBean> listaPerdidas = null;

    // VARIABLES PAGINACION
    private String numPagina;
    private String cantReg;
    private String pagActual;

    // VARIABLE DE REQUEST
    private HttpServletRequest request;

    // VARIABLE PARA EXPORT DE CSV
    private FileInputStream archivoPerdidasStream;

    @Override
    public void setServletRequest(HttpServletRequest request) {
	this.request = request;

    }

    public FileInputStream getArchivoPerdidasStream() {
	return archivoPerdidasStream;
    }

    public void setArchivoPerdidasStream(FileInputStream archivoPerdidasStream) {
	this.archivoPerdidasStream = archivoPerdidasStream;
    }

    public String getMensaje() {
	return mensaje;
    }

    private String listaComboPerdidas;

    public String getSidTransaccion() {
	return sidTransaccion;
    }

    public void setSidTransaccion(String sidTransaccion) {
	this.sidTransaccion = sidTransaccion;
    }

    public String getSidUsuario() {
	return sidUsuario;
    }

    public void setSidUsuario(String sidUsuario) {
	this.sidUsuario = sidUsuario;
    }

    public String getSidTipoGestion() {
	return sidTipoGestion;
    }

    public void setSidTipoGestion(String sidTipoGestion) {
	this.sidTipoGestion = sidTipoGestion;
    }

    public String getGlosa() {
	return glosa;
    }

    public void setGlosa(String glosa) {
	this.glosa = glosa;
    }

    public String getNumIncidente() {
	return numIncidente;
    }

    public void setNumIncidente(String numIncidente) {
	this.numIncidente = numIncidente;
    }

    public String getListaComboPerdidas() {
	return listaComboPerdidas;
    }

    public int getCodigoError() {
	return codigoError;
    }

    public void setCodigoError(int codigoError) {
	this.codigoError = codigoError;
    }

    public List<PerdidaBean> getListaPerdidas() {
	return listaPerdidas;
    }

    public void setListaPerdidas(List<PerdidaBean> listaPerdidas) {
	this.listaPerdidas = listaPerdidas;
    }

    public String getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
	return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
	this.fechaTermino = fechaTermino;
    }

    public String getNumeroTarjeta() {
	return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
	this.numeroTarjeta = numeroTarjeta;
    }

    public String getNumPagina() {
	return numPagina;
    }

    public void setNumPagina(String numPagina) {
	this.numPagina = numPagina;
    }

    public String getCantReg() {
	return cantReg;
    }

    public void setCantReg(String cantReg) {
	this.cantReg = cantReg;
    }

    public String getPagActual() {
	return pagActual;
    }

    public void setPagActual(String pagActual) {
	this.pagActual = pagActual;
    }

    public String consultaPerdidasIndex(){
	return "SUCCESS";
    }
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private FiltroPerdida seteaFiltroPerdida() {
	FiltroPerdida filtro = new FiltroPerdida();
	filtro.setFechaInicio(fechaInicio);
	filtro.setFechaTermino(fechaTermino);
	filtro.setNumeroTarjeta((numeroTarjeta != null && !numeroTarjeta
		.equalsIgnoreCase("")) ? numeroTarjeta : "0");

	return filtro;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private PerdidaDTO crearInstanciaPerdida() {
	PerdidaDTO perdida = new PerdidaDTO();
	perdida.setSidIncoming(Long.parseLong(sidTransaccion));
	perdida.setSidTipoGestion(Long.parseLong(sidTipoGestion));
	perdida.setSidUsuario(Long.parseLong(sidUsuario));
	perdida.setGlosa(glosa);
	perdida.setNumeroIncidente(numIncidente);

	log.info(perdida);
	return perdida;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String obtenerTipoPerdida() {

	// INICIALIZA EL DAO
	if (perdidaDAO == null) {
	    perdidaDAO = new PerdidasDAO();
	}

	// CARGA TIPO PERDIDA
	this.listaComboPerdidas = perdidaDAO.obtenerTipoPerdida();

	return Action.SUCCESS;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
    public String guardarPerdida() {

	log.info("==== METODO GUARDAR PERDIDA =====");
	try {
	    // INICIALIZA EL DAO
	    if (perdidaDAO == null) {
		perdidaDAO = new PerdidasDAO();
	    }

	    PerdidaDTO perdida = crearInstanciaPerdida();
	    perdidaDAO.guardarPerdida(perdida);
	    this.codigoError = 0;
	    this.mensaje = "Se ha guardado correctamente el  ";
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    this.codigoError = -1;
	    this.mensaje = e.getMessage();
	}

	return Action.SUCCESS;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String obtenerListaPerdidas() {

	codigoError = 0;

	try {
	    // INICIALIZA EL DAO
	    if (perdidaDAO == null) {
		perdidaDAO = new PerdidasDAO();
	    }
	    FiltroPerdida filtro = seteaFiltroPerdida();
	    filtro.setTipoBusqueda(SearchType.CON_PAGINACION);
	    log.info(filtro);

	    listaPerdidas = perdidaDAO.buscarPerdidasPorPagina(filtro,
		    numPagina);
	    log.info("num :" + numPagina);

	    request.setAttribute("listaTransacciones", listaPerdidas);
	    request.setAttribute("ultimaPagina", perdidaDAO.getUltimaPagina());
	    request.setAttribute("numPagina", perdidaDAO.getPagActual());
	    request.setAttribute("fechaInicioPag", fechaInicio);
	    request.setAttribute("fechaTerminoPag", fechaTermino);

	    log.info(listaPerdidas);
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    codigoError = -1;
	}

	return Action.SUCCESS;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String exportarPerdidas() {

	codigoError = 0;

	try {
	    // INICIALIZA EL DAO
	    if (perdidaDAO == null) {
		perdidaDAO = new PerdidasDAO();
	    }
	    log.info("EXPORTAR CVS PERDIDAS ");

	    // INICIALIZA EL FILTRO DE PERDIDAS
	    FiltroPerdida filtro = seteaFiltroPerdida();
	    filtro.setTipoBusqueda(SearchType.SIN_PAGINACION);
	    log.info(filtro);

	    // BUSCA PERDIDAS
	    listaPerdidas = perdidaDAO.buscarPerdidasPorPagina(filtro,
		    numPagina);

	    // EXPORTA A EXCEL
	    ExcelUtils excelUtil = new ExcelPerdidas();
	    File archivo = excelUtil.export(listaPerdidas);
	    this.archivoPerdidasStream = new FileInputStream(archivo);

	    log.info("num :" + numPagina);

	    log.info(listaPerdidas);
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    codigoError = -1;
	}

	return Action.SUCCESS;

    }

}