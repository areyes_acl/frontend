package actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import properties.UtilProperties;
import Beans.TDIExecutionEstBean;
import Beans.TDIExecutionLogBean;
import cl.util.Constantes;
import cl.util.Utils;
import clases.TDIExecutionEstDao;
import clases.TDIExecutionLogDao;
import clases.TDIExportarDAO;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Action;

public class TDIDashboardAction extends ActionSupport implements SessionAware,  ServletRequestAware{

    static final Logger LOGGER = Logger.getLogger(TDIDashboardAction.class);
    private static final long serialVersionUID = -7123469716521196713L;

    private List<TDIExecutionEstBean> logEstados;
    private TDIExecutionEstDao tDIExecutionEstDao;

    private List<TDIExecutionLogBean> executionLogs;
    private TDIExecutionLogDao tDIExecutionLogDao;

    private int page;
    private int ultimaPagina;
    private int valorInicial = 0;
    private int cantidadResultados;
    private String reprocesar;

    private String fechaInicio = "";
    private String fechaTermino = "";
    private int estado = -1;

    private int resultadoAction = 0;
    private String jsonString;
    
    private int estadoUpdate;

    private InputStream fileInputStream;
    private String fileName;
    private File file;
    
    private HttpServletRequest request;
    private Map<String, Object> sessionMap;
    
    public TDIDashboardAction() {
	tDIExecutionEstDao = new TDIExecutionEstDao();
	tDIExecutionLogDao = new TDIExecutionLogDao();
	page = 1;
	cantidadResultados = Integer.parseInt(UtilProperties.getProperty("TDICantidadRegistros"));
	logEstados = new ArrayList<TDIExecutionEstBean>();
	executionLogs = new ArrayList<TDIExecutionLogBean>();
	reprocesar = "";
    }

    public String dashboardTDI() { // Renderiza plantilla 1
	filterDashboard();
	return Action.SUCCESS;
    }

    public String dashboardTDISearchResult() { // Renderiza plantilla 2
	filterDashboard();
	return Action.SUCCESS;
    }

    private void filterDashboard() {
	resultadoAction = 0;
	LOGGER.info("La fecha inicio a filtrar es: " + fechaInicio);
	LOGGER.info("La fecha fin a filtrar es: " + fechaTermino);
	LOGGER.info("El estado a filtrar es: " + estado);

	if (fechaInicio.length() > 0 || fechaTermino.length() > 0) {
	    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
	    try {
		Date inicio = formatDate.parse(fechaInicio);
		Date fin = formatDate.parse(fechaTermino);
		if (inicio.after(fin)) {
		    resultadoAction = 3;
		}
	    } catch (Exception e) {
		LOGGER.info(
			"Error al parsear el parametro de filtrado fechaInicio o  fechaTermino",
			e);
		resultadoAction = 1;
	    }
	}

	LOGGER.info("Validador de fecha : " + resultadoAction);
	if (resultadoAction == 0) {
	    ultimaPagina = tDIExecutionLogDao.getUltimaPagina(fechaInicio,
		    fechaTermino, estado);
	    logEstados = tDIExecutionEstDao.findAll();
	    executionLogs = tDIExecutionLogDao.findAll(page, fechaInicio,
		    fechaTermino, estado);
	    if (executionLogs.isEmpty()) {
		resultadoAction = 2;
	    }

	    valorInicial = (page - 1) * cantidadResultados;
	}

	LOGGER.info("Cantidad de estados: " + logEstados.size());
	LOGGER.info("Cantidad de logs: " + logEstados.size());
	sessionMap.put("logs", executionLogs);
    }

    public String reprocesado() {
	String[] ids = reprocesar.split(",");
	// Por cada uno de los Ids correctos cambiar el estado a REPROCESAR y
	// crear un nuevo log de ejecucion al minuto siguiente del actual
	LOGGER.info("IDs a reprocesar " + Arrays.toString(ids));
	for (String s : ids) {
	    try {
		int id = Integer.parseInt(s);
		TDIExecutionEstBean estadoProcesar = tDIExecutionEstDao
			.getEstado("REPROCESAR");
		LOGGER.info("Se busco el estado reprocesar el id es " + estadoProcesar.getSid());
		tDIExecutionLogDao.setEstado(id, estadoProcesar); // Cambio de
								  // estado del
								  // log
		LOGGER.info("Se cambio el estado de id " + id);
		// Crear el nuevo log para ejecucion en el proximo minuto
		// Sumarle la cantidad de minutos determinados en el job //
		// ahora
		crearLog(id);
		
		jsonString = "1";
	    } catch (Exception e) {
		LOGGER.info("Convirtiendo el valor a reprocesar a Integer" + e);
		jsonString = "0";
	    }
	}

	LOGGER.info("la respuesta en json es :" + jsonString);

	return Action.SUCCESS;
    }
    
    public String updateEstado(){
	TDIExecutionLogBean logEx = tDIExecutionLogDao.getById(estadoUpdate); 
	if(!"PROCESANDO".equals(logEx.getEstado())){ // Ya se proceso
	    jsonString = logEx.getJsonData();
	}else{
	    jsonString = "1"; // Aun se esta procesando
	}
	return Action.SUCCESS;
    }
    
    private void crearLog(int id){
	Date ahora = new Date();
	SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
	LOGGER.info("hora sin minutos: " + format.format(ahora));
	Date finalx = Utils.addMinutes(ahora, 1);
	LOGGER.info("hora + minutos: " + format.format(finalx));
	TDIExecutionEstBean estadoprocesando = tDIExecutionEstDao.getEstado("PROCESANDO");
	TDIExecutionLogBean logEx = tDIExecutionLogDao.getById(id); 
	logEx.setEstadoBean(estadoprocesando);
	logEx.setHoraInicio(finalx);
	tDIExecutionLogDao.createLog(logEx);
    }

    public String getJsonString() {
	return jsonString;
    }

    public void setJsonString(String jsonString) {
	this.jsonString = jsonString;
    }

    public int getResultadoAction() {
	return resultadoAction;
    }

    public void setResultadoAction(int resultadoAction) {
	this.resultadoAction = resultadoAction;
    }

    
    public int getEstadoUpdate() {
        return estadoUpdate;
    }

    public void setEstadoUpdate(int estadoUpdate) {
        this.estadoUpdate = estadoUpdate;
    }

    public int getPage() {
	return page;
    }

    public void setPage(int page) {
	this.page = page;
    }

    public List<TDIExecutionEstBean> getLogEstados() {
	return logEstados;
    }

    public void setLogEstados(List<TDIExecutionEstBean> logEstados) {
	this.logEstados = logEstados;
    }

    public TDIExecutionEstDao gettDIExecutionEstDao() {
	return tDIExecutionEstDao;
    }

    public void settDIExecutionEstDao(TDIExecutionEstDao tDIExecutionEstDao) {
	this.tDIExecutionEstDao = tDIExecutionEstDao;
    }

    public List<TDIExecutionLogBean> getExecutionLogs() {
	return executionLogs;
    }

    public void setExecutionLogs(List<TDIExecutionLogBean> executionLogs) {
	this.executionLogs = executionLogs;
    }

    public TDIExecutionLogDao gettDIExecutionLogDao() {
	return tDIExecutionLogDao;
    }

    public void settDIExecutionLogDao(TDIExecutionLogDao tDIExecutionLogDao) {
	this.tDIExecutionLogDao = tDIExecutionLogDao;
    }

    public int getUltimaPagina() {
	return ultimaPagina;
    }

    public void setUltimaPagina(int ultimaPagina) {
	this.ultimaPagina = ultimaPagina;
    }

    public int getCantidadResultados() {
	return cantidadResultados;
    }

    public void setCantidadResultados(int cantidadResultados) {
	this.cantidadResultados = cantidadResultados;
    }

    public int getValorInicial() {
	return valorInicial;
    }

    public void setValorInicial(int valorInicial) {
	this.valorInicial = valorInicial;
    }

    public String getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
	return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
	this.fechaTermino = fechaTermino;
    }

    public int getEstado() {
	return estado;
    }

    public void setEstado(int estado) {
	this.estado = estado;
    }

    public String getReprocesar() {
	return reprocesar.trim();
    }

    public void setReprocesar(String reprocesar) {
	this.reprocesar = reprocesar;
    }
    
    
    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    /**
     * 
     * ACTION DEL METODO QUE PERMITE REALIZAR EL EXPORT GENERICO DE UN EXCEL CON
     * LOS DATOS PASADOS POR SESSION
     * 
     * 
     * @return
     * @throws FileNotFoundException
     */
    public String generarExcel() throws FileNotFoundException {
	LOGGER.info("==== EXPORTA REPORTE A EXCEL ====");
	List<TDIExecutionLogBean> execution = tDIExecutionLogDao.findAll(null, fechaInicio, fechaTermino, estado);
	LOGGER.info("Cantidad a exportar: "+execution.size());

	TDIExportarDAO exportDAO = new TDIExportarDAO();
	file = exportDAO.generarPlanillaExcel(Constantes.KEY_DATA_EXCEL, Constantes.KEY_FILENAME, execution);
	setFileInputStream(new FileInputStream(file));

	return "SUCCESS";
    }

    @Override
    public void setServletRequest(HttpServletRequest arg0) {
	this.request = arg0;
    }

    @Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
