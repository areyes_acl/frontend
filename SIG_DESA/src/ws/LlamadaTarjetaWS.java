package ws;

import java.rmi.RemoteException;
import java.util.Properties;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;

public class LlamadaTarjetaWS {
	
	//private ConsultaDetallesTarjetaRequest req = new ConsultaDetallesTarjetaRequest();
	//private ConsultaDetallesTarjetaResponse resp = new ConsultaDetallesTarjetaResponse();
	//private ConsultaDetallesTarjetaPortTypeProxy proxy = new ConsultaDetallesTarjetaPortTypeProxy();
	//private cl.walmart.presto.ti.servicios.tarjetas.sei.Rut seiRut = new cl.walmart.presto.ti.servicios.tarjetas.sei.Rut();
	//private TarjetaDTOV2[] dto;
	//private cl.walmart.presto.ti.servicios.tarjetas.sei.UserRequesterDTO reqDto = new cl.walmart.presto.ti.servicios.tarjetas.sei.UserRequesterDTO();
	//private cl.walmart.presto.ti.servicios.tarjetas.sei.TarjetasSEIProxy seiProxy = new cl.walmart.presto.ti.servicios.tarjetas.sei.TarjetasSEIProxy();
	
	private String rut;
	private String numeroTajeta;
	private String cadena;
	private String rutaWS;
	static final Category log = Logger.getLogger(LlamadaTarjetaWS.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	public LlamadaTarjetaWS(String numTarjeta) throws RemoteException
	{
		/*.setId(0);
		reqDto.setDvRequester("");
		reqDto.setCodigoSucursal((long) 0);
        reqDto.setUsername("");
        reqDto.setCodigoCanal((long) 0);
        reqDto.setIpRequester("");
        reqDto.setRutRequester((long) 0);
        reqDto.setIdSession("");*/
        
        this.setNumeroTajeta(numTarjeta);
	}
	
	public String getData()
	{
		String numTaj = this.getNumeroTajeta();

		Properties propiedades = new Properties();
		
		//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
        propiedades = PropertiesConfiguration.getProperty();
		rutaWS = propiedades.getProperty("rutaWsConsultaClientePorTarjeta");
		if (!rutaWS.equalsIgnoreCase("")) {

			/*seiProxy.setEndpoint(rutaWS);
			try {
				dto = seiProxy.obtieneTarjetasPorNumero(numTaj, reqDto);
			} catch (DependencyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				cadena = "trx|1~" + e.getMessage();
			} catch (ParameterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				cadena = "trx|1~" + e.getMessage();
			} catch (InternalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				cadena = "trx|1~" + e.getMessage();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				cadena = "trx|1~" + e.getMessage();
			}
		}
		if(dto != null)
		{
			java.util.List lista= java.util.Arrays.asList(dto);
					
			for(int i=0;i<lista.size();i++)
			{
				System.out.println("Nombre: " + dto[i].getNombre()+ " ");
				System.out.println("Apellido Paterno: " + dto[i].getApellidoPaterno()+" | ");
				System.out.println("Apellido Materno: " + dto[i].getApellidoMaterno()+" | "); 
				System.out.println("Nombre en tajeta: " + dto[i].getNombreEnTarjeta()+" | "); 
				System.out.println("Rut: " + dto[i].getRut().getRut()+" | "); 
				System.out.println("dv: " + dto[i].getRut().getDv()+" | "); 
				System.out.println("Cupo: " + dto[i].getCupo()+" | ");	
				//Estado tarjeta
				System.out.println("Cod Estado: " + dto[i].getEstadoTarjeta().getCodEstado()+" | ");
				System.out.println("Descripcion corta: " + dto[i].getEstadoTarjeta().getDescripcionCorta()+" | ");
				//Estado tarjeta
				System.out.println("Fecha Vencimiento: " + dto[i].getFechaVencimiento()+" | ");
				System.out.println("Numero cuenta: " + dto[i].getNumeroCuenta()+" | ");
				System.out.println("Tipo Membresia: " + dto[i].getTipoMembresia()+" | ");
				System.out.println("Tipo Tarjeta: " + dto[i].getTipoTarjeta()+" | ");
				System.out.println("Nombre Producto: " + dto[i].getNombreProducto());
				System.out.println("Numero de tarjeta: " + dto[i].getNumeroTarjeta());
				
				String tarjetaAux = dto[i].getNumeroTarjeta().toString();
				
				if(tarjetaAux.equals(numTaj))
				{
					System.out.println("Corresponde a --> "+dto[i].getNombre() +" "+dto[i].getApellidoPaterno());
					cadena = "trx|0~";
					cadena += dto[i].getNombre() + " " + dto[i].getApellidoPaterno() + " " +dto[i].getApellidoMaterno() + "|" +
							dto[i].getRut().getRut()+"|"+
							dto[i].getNumeroCuenta()+"|"+
							dto[i].getCupo()+"|"+
							dto[i].getFechaVencimiento()+"|"+
							dto[i].getEstadoTarjeta().getDescripcionCorta()+
							"~";
				}else{
					cadena = "trx|1~";
				}
			}*/
		}else{
			cadena = "trx|1~Numero de tarjeta erroneo";
		}
		return cadena;
	}

	public String getNumeroTajeta() {
		return numeroTajeta;
	}

	public void setNumeroTajeta(String numeroTajeta) {
		this.numeroTajeta = numeroTajeta;
	}

	public String getCadena() {
		return cadena;
	}

	public void setCadena(String cadena) {
		this.cadena = cadena;
	}
}
