package ws;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Category;

import properties.PropertiesConfiguration;

import cl.service.SOAPWSListener;
import cl.service.SOAPWSListenerImplServiceLocator;
import cl.util.Utils;

public class TarjetaClientWS {
	

	@SuppressWarnings("deprecation")
	static final Category log = Category.getInstance(TarjetaClientWS.class);
	
	private String numeroTarjeta = "4946111100323009";
	private String cadena;
	
	private String TAGNUMTRJ = "<DATOS_CLIENTEREQ_COMP_IN><num_tarjeta>%s</num_tarjeta></DATOS_CLIENTEREQ_COMP_IN>";
	
	private Matcher matcher				  = null;
	final Pattern patronNombre 			  = Pattern.compile("<nombre_titular>(.+?)</nombre_titular>");
	final Pattern patronCuenta 			  = Pattern.compile("<num_cuenta>(.+?)</num_cuenta>");
	final Pattern patronRut 			  = Pattern.compile("<rut_titular>(.+?)</rut_titular>");
	final Pattern patronDgitoVerificador  = Pattern.compile("<dv_titular>(.+?)</dv_titular>");
	final Pattern patronCupo			  = Pattern.compile("<cupo>(.+?)</cupo>");
	final Pattern patronEstado			  = Pattern.compile("<flag_estado_cuenta>(.+?)</flag_estado_cuenta>");
	final Pattern patronFecVec			  = Pattern.compile("<fecha_expiracion>(.+?)</fecha_expiracion>");
	
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	public String getData()
	{
		Properties propiedades = PropertiesConfiguration.getProperty();
		
		try {
			String rutaWS = propiedades.getProperty("rutaWsConsultaClientePorTarjeta");
			if (!rutaWS.equalsIgnoreCase("") && rutaWS != null) {
				SOAPWSListenerImplServiceLocator es = new SOAPWSListenerImplServiceLocator();
				es.setSOAPWSListenerImplPortEndpointAddress(rutaWS);
				SOAPWSListener s = es.getSOAPWSListenerImplPort();
				TAGNUMTRJ = String.format(TAGNUMTRJ, getNumeroTarjeta());
				String response = s.processMessage(TAGNUMTRJ);
				cadena = obtenerInfoCuenta(response);
				log.info("Response WS Tarjeta : " +response);
			}else{
				cadena = "trx|1~No se encuentra la URL del servicio";
			}
		} catch (IOException ioEx) {
			log.error(ioEx.getMessage());
			cadena = "trx|1~Ocurrio un error al obtener la informacion";
		} catch (ServiceException srvEx) {
			log.error(srvEx.getMessage());
			cadena = "trx|1~Ocurrio un error al obtener la informacion";
		}
		return cadena;
	}
	
	private String obtenerInfoCuenta(String respuesta){
		String informacion = "trx|0~";
		matcher = patronNombre.matcher(respuesta);
		if(matcher.find())
			informacion += matcher.group(1);
		informacion += "|";
		matcher = patronRut.matcher(respuesta);
		if(matcher.find())
			informacion += obtieneRut(matcher.group(1));
		informacion += "|";
		matcher = patronCuenta.matcher(respuesta);
		if(matcher.find())
			informacion += matcher.group(1);
		informacion += "|";
		matcher = patronCupo.matcher(respuesta);
		if(matcher.find())
			informacion += matcher.group(1);
		informacion += "|";
		matcher = patronFecVec.matcher(respuesta);
		if(matcher.find())
			informacion += matcher.group(1);
		informacion += "|";
		matcher = patronEstado.matcher(respuesta);
		if(matcher.find())
			informacion += matcher.group(1);
		informacion += "|";
				
		return informacion;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL) -  versi�n inicial 
	 * </ul>
	 * <p>
	 * 
	 * 
	 * @param rut
	 * @return rut formateado o vacio si no lo retorna
	 * @since 1.X
	 */
	private String obtieneRut( String rut ) {
	    
	    if(rut == null ){
	        return "";
	    }
	    if( "".equalsIgnoreCase( rut )){
	        return "";
	    }
	    
        return Utils.formatearRut( rut );
    }

    public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getCadena() {
		return cadena;
	}

	public void setCadena(String cadena) {
		this.cadena = cadena;
	}
}
