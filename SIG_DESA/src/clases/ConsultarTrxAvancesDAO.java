package clases;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import cl.util.Utils;
import Beans.CargaMasivaDTO;
import Beans.CargoAbonoDTO;
import actions.TransaccionesAvances;
import actions.TransaccionesAvancesDetalleBice;
import actions.TransaccionesAvancesDetalleCanal;
import actions.TransaccionesAvancesDetalleIc;
import actions.TransaccionesDetalleGestion;
import au.com.bytecode.opencsv.CSVWriter;
import db.OracleBD;
import exception.AppException;
import properties.PropertiesConfiguration;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class ConsultarTrxAvancesDAO {
    static final Logger log = Logger.getLogger(ConsultarTrxAvancesDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    private String esquema;
    private String listaTipoEstado;
    private String listaDetalleEstado;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL ) - versi�n inicial
     * </ul>
     * <p>
     * Constructor de clase
     * 
     * 
     * @since 1.X
     */
    public ConsultarTrxAvancesDAO() {
	try {
	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    esquema = propiedades.getProperty("esquema");
	} catch (Exception e) {
	    log.error("Error al leer el archivo de propiedades.");
	    log.error(e.getMessage(), e);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param fechaProceso
     * @return
     * @throws SQLException
     * @since 1.X
     */
    public HashMap<String, List<TransaccionesAvances>> buscarInformeTrxAvances(
	    String fechaDesde, String fechaHasta, Integer estado,
	    Integer estadoDetalle, Integer estadoGestion) throws SQLException {
	HashMap<String, List<TransaccionesAvances>> hash = new HashMap<String, List<TransaccionesAvances>>();
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs = null;
	List<TransaccionesAvances> listaTrxAvances = null;
	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT CA.SID AS id_sid,  "
		+ " NVL(TO_CHAR(CA.FECHA, 'dd/mm/yyyy'),' ') AS fecha_filtro, "
		+ " NVL(TB.RUT_BENEFICIARIO,' ') AS rut_cliente, "
		+ " NVL(TA.NUM_TARJETA,' ') AS num_tarjeta, "
		+ " CA.SID_ESTADO AS estado_detalle, "
		+ " TE.DETALLE AS detalle_estado, "
		+ " TPE.NOMBRE AS estado_estado_conci, "
		+ " NVL(SUBSTR(CA.MONTO, 0, LENGTH(CA.MONTO) - 2 ),0)  as monto_avances,  "
		+ " (select count(*) "
		+ " FROM TBL_CONC_AVANCE CA "
		+ " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO "
		+ " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		+ " WHERE CA.SID IS NOT NULL "
		+ " AND TO_CHAR(CA.FECHA, 'YYYYMMDD') >= '"
		+ fechaDesde
		+ "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '"
		+ fechaHasta
		+ "'  AND TE.SID_TIPO = 1) as cant_conci, "
		+ " (select count(*) "
		+ " FROM TBL_CONC_AVANCE CA "
		+ " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO "
		+ " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		+ " WHERE CA.SID IS NOT NULL "
		+ " AND TO_CHAR(CA.FECHA, 'YYYYMMDD') >= '"
		+ fechaDesde
		+ "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '"
		+ fechaHasta
		+ "' AND TE.SID_TIPO = 2 ) AS cant_no_conci, "
		+ " (select sum(NVL(SUBSTR(CA.MONTO, 0, LENGTH(CA.MONTO) - 2 ),0))  "
		+ " FROM TBL_CONC_AVANCE CA "
		+ " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO "
		+ " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		+ " WHERE CA.SID IS NOT NULL "
		+ " AND TO_CHAR(CA.FECHA, 'YYYYMMDD') >= '"
		+ fechaDesde
		+ "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '"
		+ fechaHasta
		+ "'  AND TE.SID_TIPO = 1 ) AS monto_conci, "
		+ " (select sum(NVL(SUBSTR(CA.MONTO, 0, LENGTH(CA.MONTO) - 2 ),0))  "
		+ " FROM TBL_CONC_AVANCE CA "
		+ " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO "
		+ " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		+ " WHERE CA.SID IS NOT NULL "
		+ " AND TO_CHAR(CA.FECHA, 'YYYYMMDD') >= '"
		+ fechaDesde
		+ "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '"
		+ fechaHasta
		+ "'  AND TE.SID_TIPO = 2) AS monto_no_conci, "
		+ " CA.SID_AVANCE AS sid_avances,  "
		+ " CA.SID_BICE AS sid_bice, "
		+ " CA.SID_ONUS AS sid_onus,  "
		+ " FUNC_SIG_OBTENER_EST_GEST(CA.SID) as estado_gestion,  "
		+ " NVL(SUBSTR(TON.VALOR_CUOTA, 0, LENGTH(TON.VALOR_CUOTA) - 2 ),0) AS valor_cuota,  "
		+ " NVL(TON.NUM_CUOTAS,0) AS num_cuotas  "
		+ " FROM TBL_CONC_AVANCE CA  "
		+ " LEFT JOIN TBL_TRANSAC_AVANCES TA ON TA.SID = CA.SID_AVANCE  "
		+ " LEFT JOIN TBL_TRANSAC_AVA_BICE TB ON TB.SID = CA.SID_BICE  "
		+ " LEFT JOIN TBL_TRANSAC_ONUS TON ON TON.SID = CA.SID_ONUS "
		+ " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO  "
		+ " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO  "
		+ " WHERE CA.SID IS NOT NULL  "
		+ "AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  >= '"
		+ fechaDesde
		+ "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '"
		+ fechaHasta
		+ "' ");

	if (estado != 0) {
	    sql.append(" AND TE.SID_TIPO = " + estado);
	}
	if (estadoDetalle != 0) {
	    sql.append(" AND CA.SID_ESTADO = " + estadoDetalle);
	}

	if (estadoGestion == 88) {
	    sql.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) IS NULL ");
	} else if (estadoGestion != 99 && estadoGestion != 88) {
	    sql.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) = "
		    + estadoGestion);
	}
	sql.append(" ORDER BY fecha_filtro DESC ");

	try {
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: buscarInformeTrxAvances - Obteniendo parametros: fechaDesde-> "
		    + fechaDesde);
	    log.info("Method: buscarInformeTrxAvances - Obteniendo parametros: fechaHasta-> "
		    + fechaHasta);
	    log.info("Method: buscarInformeTrxAvances - Obteniendo parametros: estado-> "
		    + estado);
	    log.info("Method: buscarInformeTrxAvances - Obteniendo parametros: estadoDetalle-> "
		    + estadoDetalle);
	    log.info("Method: buscarInformeTrxAvances - Obteniendo parametros: estadoGestion-> "
		    + estadoGestion);

	    rs = bdOracle.consultar(sql.toString());
	    bdOracle.setResultado(rs);

	    log.info("Method: buscarInformeTrxAvances - Obteniendo resulstados");
	    listaTrxAvances = parserTrxAvancesList(rs);

	    hash.put("listaTrxAvances", listaTrxAvances);
	    rs.close();

	} finally {

	    bdOracle.setResultado(rs);
	    bdOracle.cerrarResultSet();
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return hash;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private List<TransaccionesAvances> parserTrxAvancesList(ResultSet rs1) {
	List<TransaccionesAvances> list = new ArrayList<TransaccionesAvances>();
	try {

	    while (rs1.next()) {

		TransaccionesAvances tr = new TransaccionesAvances();
		tr.setSid(rs1.getString("id_sid"));
		tr.setRutCliente(rs1.getString("rut_cliente"));
		tr.setFechaFiltro(rs1.getString("fecha_filtro"));
		tr.setMontoAvance(rs1.getLong("monto_avances"));
		tr.setNumeroTarjeta(rs1.getString("num_tarjeta"));
		tr.setEstadoConc(rs1.getString("estado_detalle"));
		tr.setDetalleEstado(rs1.getString("detalle_estado"));
		tr.setCantConci(rs1.getInt("cant_conci"));
		tr.setCantNoConci(rs1.getInt("cant_no_conci"));
		tr.setMontoConci(rs1.getLong("monto_conci"));
		tr.setMontoNoConci(rs1.getLong("monto_no_conci"));
		tr.setSidAvances(rs1.getInt("sid_avances"));
		tr.setSidBice(rs1.getInt("sid_bice"));
		tr.setSidOnus(rs1.getInt("sid_onus"));
		tr.setEstadoGestion(rs1.getString("estado_gestion"));
		tr.setNumCuota(rs1.getString("num_cuotas"));
		tr.setValorCuota(rs1.getLong("valor_cuota"));
		list.add(tr);
	    }
	} catch (SQLException e) {
	    log.error(
		    "ConsultarTrxAvancesDAO-parserTrxAvancesList : Ha ocurrido un error al parsear la trx de Avances.",
		    e);
	}

	return list;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Detalle de BICE
     * 
     * @param id
     * @return
     * @since 1.X
     */
    public List<TransaccionesAvancesDetalleBice> buscarDetalleBice(
	    Integer sidBice) {
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet rs1 = null;
	List<TransaccionesAvancesDetalleBice> listaTrxAvancesBice = null;

	try {
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: buscarDetalleBice - Obteniendo parametros: sidAvances-> "
		    + sidBice);
	    log.info("Method: buscarDetalleBice - Ejecutando SP_SIG_OBTENER_INFO_TRX_BICE");

	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_OBTENER_INFO_TRX_BICE(?, ?, ?, ?); END;");

	    stmtProcedure.setInt("id_sid", sidBice);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    int codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");

	    log.info("==== EJECUCION DE SP_SIG_OBTENER_INFO_TRX_AVA_BICE ===");
	    log.info("Codigo error: " + codigoError);
	    log.info("Mensaje: " + warning);
	    log.info("=========================================");

	    if (codigoError == 0) {

		rs1 = (ResultSet) ((stmtProcedure).getObject("prfcursor"));

		log.info("Method: buscarDetalleBice - Recorriendo resultados");

		listaTrxAvancesBice = parserTrxAvancesBiceList(rs1);

		log.info("listaTrxAvancesBice :" + listaTrxAvancesBice);

	    } else {
		log.error("Method: buscarDetalleBice"
			+ " - Problemas al leer las trx.: No se ha leido ningun registro");
	    }

	} catch (SQLException e) {

	    log.error(e.getMessage(), e);
	} finally {

	    bdOracle.setResultado(rs1);
	    bdOracle.cerrarResultSet();
	    bdOracle.cerrarStatement(stmtProcedure);

	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaTrxAvancesBice;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * lista de bice detalle
     * 
     * @return
     * @since 1.X
     */
    private List<TransaccionesAvancesDetalleBice> parserTrxAvancesBiceList(
	    ResultSet rs1) {
	List<TransaccionesAvancesDetalleBice> list = new ArrayList<TransaccionesAvancesDetalleBice>();
	try {
	    while (rs1.next()) {

		TransaccionesAvancesDetalleBice tr = new TransaccionesAvancesDetalleBice();
		tr.setSid(rs1.getString("sid"));
		tr.setTipoRegistro(rs1.getString("tipoRegistro"));
		tr.setNumOperPro(rs1.getString("numOperPro"));
		tr.setCodServicio(rs1.getString("cod_servicio"));
		tr.setRutOrdenante(rs1.getString("rutOrdenante"));
		tr.setNumCtaOrdenante(rs1.getString("numCtaOrdenante"));
		tr.setIdOperCliente(rs1.getString("idOperCliente"));
		tr.setCodEstado(rs1.getString("codEstado"));
		tr.setGlosaEstado(rs1.getString("glosaEstado"));
		tr.setFechaInstruccion(rs1.getString("fechaInstruccion"));
		tr.setFechaContable(rs1.getString("fechaContable"));
		tr.setCodBancoBenefi(rs1.getString("codBancoBenefi"));
		tr.setTipoCtaBenefi(rs1.getString("tipoCtaBenefi"));
		tr.setCtaBenefi(rs1.getString("ctaBenefi"));
		tr.setRutBenefi(rs1.getString("rutBenefi"));
		tr.setNomBenefi(rs1.getString("nomBenefi"));
		tr.setMonto(Utils.formateaMonto(rs1.getString("monto")));
		list.add(tr);
	    }
	} catch (SQLException e) {
	    log.error(
		    "ConsultarTrxAvancesDAO-parserTrxAvancesBiceList : Ha ocurrido un error al parsear la trx de Avances bice.",
		    e);
	}

	return list;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Detalle de IC (ONUS)
     * 
     * @param id
     * @return
     * @since 1.X
     */
    public List<TransaccionesAvancesDetalleIc> buscarDetalleIc(Integer sidOnus) {
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet rs1 = null;
	List<TransaccionesAvancesDetalleIc> listaTrxAvancesIc = null;

	try {
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: buscarDetalleIc - Obteniendo parametros: sidOnus-> "
		    + sidOnus);
	    log.info("Method: buscarDetalleIc - Ejecutando SP_SIG_OBTENER_INFO_TRX_ONUS");

	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_OBTENER_INFO_TRX_ONUS(?, ?, ?, ?); END;");

	    stmtProcedure.setInt("id_sid", sidOnus);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    int codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");

	    log.info("==== EJECUCION DE SP_SIG_OBTENER_INFO_TRX_ONUS ===");
	    log.info("Codigo error: " + codigoError);
	    log.info("Mensaje: " + warning);
	    log.info("=========================================");

	    if (codigoError == 0) {

		rs1 = (ResultSet) ((stmtProcedure).getObject("prfcursor"));

		log.info("Method: buscarDetalleIc - Recorriendo resultados");

		listaTrxAvancesIc = parserTrxAvancesIcList(rs1);

		log.info("listaTrxAvancesIc :" + listaTrxAvancesIc);

	    } else {
		log.error("Method: buscarDetalleIc"
			+ " - Problemas al leer las trx.: No se ha leido ningun registro");
	    }

	} catch (SQLException e) {

	    log.error(e.getMessage(), e);
	} finally {

	    bdOracle.setResultado(rs1);
	    bdOracle.cerrarResultSet();
	    bdOracle.cerrarStatement(stmtProcedure);

	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaTrxAvancesIc;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * lista de ic (onus) detalle
     * 
     * @return
     * @since 1.X
     */
    private List<TransaccionesAvancesDetalleIc> parserTrxAvancesIcList(
	    ResultSet rs1) {
	List<TransaccionesAvancesDetalleIc> list = new ArrayList<TransaccionesAvancesDetalleIc>();
	try {
	    while (rs1.next()) {

		TransaccionesAvancesDetalleIc tr = new TransaccionesAvancesDetalleIc();
		tr.setSid(rs1.getString("sid"));
		tr.setCodTransac(rs1.getString("codTransac"));
		tr.setNumTrajeta(rs1.getString("numTrajeta"));
		tr.setFechaCompra(rs1.getString("fechaCompra"));
		tr.setFechaAutoriza(rs1.getString("fechaAutoriza"));
		tr.setFechaPosteo(rs1.getString("fechaPosteo"));
		tr.setTipoVenta(rs1.getString("tipoVenta"));
		tr.setNumCuotas(rs1.getString("numCuotas"));
		tr.setNumMicroFilm(rs1.getString("numMicroFilm"));
		tr.setNumComercio(rs1.getString("numComercio"));
		tr.setMontoTransac(Utils.formateaMonto(rs1.getString("montoTransac")));
		tr.setValorCuota(Utils.formateaMonto(rs1.getString("valorCuota")));
		tr.setNombreComercio(rs1.getString("nombreComercio"));
		tr.setCiudadComercio(rs1.getString("ciudadComercio"));
		tr.setRubroComercio(rs1.getString("rubroComercio"));
		tr.setCodAutor(rs1.getString("codAutor"));
		list.add(tr);
	    }
	} catch (SQLException e) {
	    log.error(
		    "ConsultarTrxAvancesDAO-parserTrxAvancesIcList : Ha ocurrido un error al parsear la trx de Avances Ic.",
		    e);
	}

	return list;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Detalle de avances (canal)
     * 
     * @param id
     * @return
     * @since 1.X
     */
    public List<TransaccionesAvancesDetalleCanal> buscarDetalleCanal(
	    Integer sidAvances) {
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet rs1 = null;
	List<TransaccionesAvancesDetalleCanal> listaTrxAvancesCanal = null;

	try {
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: buscarDetalleCanal - Obteniendo parametros: sidAvances-> "
		    + sidAvances);
	    log.info("Method: buscarDetalleCanal - Ejecutando SP_SIG_OBTENER_INFO_TRX_AVANC");

	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_OBTENER_INFO_TRX_AVANC(?, ?, ?, ?); END;");

	    stmtProcedure.setInt("id_sid", sidAvances);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    int codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");

	    log.info("==== EJECUCION DE SP_SIG_OBTENER_INFO_TRX_AVANC ===");
	    log.info("Codigo error: " + codigoError);
	    log.info("Mensaje: " + warning);
	    log.info("=========================================");

	    if (codigoError == 0) {

		rs1 = (ResultSet) ((stmtProcedure).getObject("prfcursor"));

		log.info("Method: buscarDetalleCanal - Recorriendo resultados");

		listaTrxAvancesCanal = parserTrxAvancesCanalList(rs1);

		log.info("listaTrxAvancesCanal :" + listaTrxAvancesCanal);

	    } else {
		log.error("Method: buscarDetalleCanal"
			+ " - Problemas al leer las trx.: No se ha leido ningun registro");
	    }

	} catch (SQLException e) {

	    log.error(e.getMessage(), e);
	} finally {

	    bdOracle.setResultado(rs1);
	    bdOracle.cerrarResultSet();
	    bdOracle.cerrarStatement(stmtProcedure);

	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaTrxAvancesCanal;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * lista de avances (canal) detalle
     * 
     * @return
     * @since 1.X
     */
    private List<TransaccionesAvancesDetalleCanal> parserTrxAvancesCanalList(
	    ResultSet rs1) {
	List<TransaccionesAvancesDetalleCanal> list = new ArrayList<TransaccionesAvancesDetalleCanal>();
	try {
	    while (rs1.next()) {

		TransaccionesAvancesDetalleCanal tr = new TransaccionesAvancesDetalleCanal();
		tr.setSid(rs1.getString("sid"));
		tr.setFechaTransafer(rs1.getString("fechaTransafer"));
		tr.setCodAutor(rs1.getString("codAutor"));
		tr.setNumTrx(rs1.getString("numTrx"));
		tr.setNumTrajeta(rs1.getString("numTrajeta"));
		tr.setMonto(Utils.formateaMonto(rs1.getString("monto")));
		tr.setEstado(rs1.getString("estado"));
		tr.setMontoSeguro(Utils.formateaMonto(rs1.getString("montoSeguro")));
		list.add(tr);
	    }
	} catch (SQLException e) {
	    log.error(
		    "ConsultarTrxAvancesDAO-parserTrxAvancesCanalList : Ha ocurrido un error al parsear la trx de Avances Canal.",
		    e);
	}

	return list;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Detalle de BICE
     * 
     * @param id
     * @return
     * @since 1.X
     */
    public List<TransaccionesDetalleGestion> buscarDetalleGestion(
	    Integer sidConci) {
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet rs1 = null;
	List<TransaccionesDetalleGestion> listaDetalleGestion = null;

	try {
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: buscarDetalleGestion - Obteniendo parametros: sidConci-> "
		    + sidConci);
	    log.info("Method: buscarDetalleGestion - Ejecutando SP_SIG_OBTENER_INFO_GESTION");

	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_OBTENER_INFO_GESTION(?, ?, ?, ?); END;");

	    stmtProcedure.setInt("id_sid", sidConci);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    int codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");

	    log.info("==== EJECUCION DE SP_SIG_OBTENER_INFO_GESTION ===");
	    log.info("Codigo error: " + codigoError);
	    log.info("Mensaje: " + warning);
	    log.info("=========================================");

	    if (codigoError == 0) {

		rs1 = (ResultSet) ((stmtProcedure).getObject("prfcursor"));

		log.info("Method: buscarDetalleGestion - Recorriendo resultados");

		listaDetalleGestion = parserTrxDetalleGestionList(rs1);

		log.info("listaDetalleGestion :" + listaDetalleGestion);

	    } else {
		log.error("Method: buscarDetalleBice"
			+ " - Problemas al leer las trx.: No se ha leido ningun registro");
	    }

	} catch (SQLException e) {

	    log.error(e.getMessage(), e);
	} finally {

	    bdOracle.setResultado(rs1);
	    bdOracle.cerrarResultSet();
	    bdOracle.cerrarStatement(stmtProcedure);

	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaDetalleGestion;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * lista gestion desde TBL_CONC_AVA_GEST
     * 
     * @return
     * @since 1.X
     */
    private List<TransaccionesDetalleGestion> parserTrxDetalleGestionList(
	    ResultSet rs1) {
	List<TransaccionesDetalleGestion> list = new ArrayList<TransaccionesDetalleGestion>();
	try {
	    while (rs1.next()) {

		TransaccionesDetalleGestion tr = new TransaccionesDetalleGestion();
		tr.setSid(rs1.getString("sid"));
		tr.setSidConci(rs1.getString("sidConci"));
		tr.setFecha(rs1.getString("fecha"));
		tr.setSidUser(rs1.getString("sidUser"));
		tr.setComentario(rs1.getString("comentario"));
		tr.setEstado(rs1.getString("estado"));
		list.add(tr);
	    }
	} catch (SQLException e) {
	    log.error(
		    "ConsultarTrxAvancesDAO-parserTrxAvancesBiceList : Ha ocurrido un error al parsear detalles de gestion.",
		    e);
	}

	return list;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/11/2018, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @throws AppException
     * @since 1.X
     */

    public String insertGestion(Integer sidConci, String comentario,
	    Integer gestion, Integer idUser) {
	log.info("Method: insertGestion - Obteniendo parametros: sidConci-> "
		+ sidConci + " | comentario-> " + comentario + " | gestion-> "
		+ gestion + " | idUser-> " + idUser);

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet rs1 = null;

	String dev = "";

	try {

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: insertGestion - Ejecutando SP_SIG_CARGA_AVA_GESTION");
	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_CARGA_AVA_GESTION(?, ?, ?, ?, ?, ?); END;");

	    stmtProcedure.setInt("sidConci", sidConci);
	    stmtProcedure.setString("comentario", comentario);
	    stmtProcedure.setInt("gestion", gestion);
	    stmtProcedure.setInt("idUser", idUser);

	    stmtProcedure.registerOutParameter("FLAG", OracleTypes.VARCHAR);
	    stmtProcedure
		    .registerOutParameter("ERROR_OUT", OracleTypes.VARCHAR);

	    stmtProcedure.execute();

	    String error = stmtProcedure.getString("ERROR_OUT");

	    if (error.equals("0")) {
		log.info("Method: insertGestion - Salida Flag: "
			+ stmtProcedure.getString("FLAG"));
		dev = stmtProcedure.getString("FLAG");
	    } else if (error.equals("1")) {
		log.info("Method: Repetida insertGestion - Salida Flag: "
			+ stmtProcedure.getString("FLAG"));
		dev = stmtProcedure.getString("FLAG");

	    } else {
		log.error("Method: insertGestion - "
			+ stmtProcedure.getString("ERROR_OUT"));

		String msgError = stmtProcedure.getString("ERROR_OUT");

		dev = "esttrx:1|" + msgError;

	    }
	    stmtProcedure.close();
	} catch (SQLException e) {
	    bdOracle.cerrarCallableStatement(stmtProcedure);
	    dev = "esttrx:1" + e.getMessage();
	    log.error(e.getMessage(), e);
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}
	return dev;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/11/2018, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @throws AppException
     * @since 1.X
     */

    public String getListaTipoEstado() {
	log.info("Method: getListaTipoEstado - Obtencion de parametros: ninguno");

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs = null;

	bdOracle = new OracleBD();
	conexion = bdOracle.conectar();
	bdOracle.setConexion(conexion);

	log.info("Method: getListaTipoEstado - Obteniendo tipo estado");
	listaTipoEstado = "esttrx:0~";
	rs = bdOracle.consultar("SELECT SID AS id, NOMBRE AS nombre FROM "
		+ esquema + ".TBL_CONC_TIPO_ESTADO ORDER BY NOMBRE ");
	bdOracle.setResultado(rs);

	try {

	    if (rs.next()) {
		log.info("Method: getListaTipoEstado - Obteniendo resulstados");
		rs.previous();
		while (rs.next()) {
		    listaTipoEstado += rs.getInt("id") + "|";
		    listaTipoEstado += rs.getString("nombre") + ("~");
		}
		rs.close();
		log.info("Method: getListaTipoEstado - " + listaTipoEstado);
	    } else {
		listaTipoEstado = "esttrx:1~No hay datos";
	    }
	    log.info("Method: getListaTipoEstado - Serializando lista");
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    bdOracle.cerrarConexionBD();
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaTipoEstado;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/11/2018, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @throws AppException
     * @since 1.X
     */

    public String getListaDetalleEstado(Integer sidEstado) {
	log.info("Method: getListaDetalleEstado - Obtencion de parametros: sidEstado: "
		+ sidEstado);

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs2 = null;

	bdOracle = new OracleBD();
	conexion = bdOracle.conectar();
	bdOracle.setConexion(conexion);

	log.info("Method: getListaDetalleEstado - Obteniendo detalle estado");
	listaDetalleEstado = "esttrx:0~";
	rs2 = bdOracle.consultar("select SID, DETALLE  from " + esquema
		+ ".TBL_CONC_ESTADO WHERE SID_TIPO = " + sidEstado
		+ " AND ESTADO = 1 ORDER BY DETALLE ");
	bdOracle.setResultado(rs2);

	try {

	    if (rs2.next()) {
		log.info("Method: getListaDetalleEstado - Obteniendo resulstados");
		rs2.previous();
		while (rs2.next()) {
		    listaDetalleEstado += rs2.getInt("SID") + "|";
		    listaDetalleEstado += rs2.getString("DETALLE") + ("~");
		}
		rs2.close();
		log.info("Method: getListaDetalleEstado - "
			+ listaDetalleEstado);
	    } else {
		listaDetalleEstado = "esttrx:1~No hay datos";
	    }
	    log.info("Method: getListaDetalleEstado - Serializando lista");
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    bdOracle.cerrarConexionBD();
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaDetalleEstado;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * header y contenido bice, ic
     * 
     * @since 1.X
     */

    public File buscarInterfacesExport(String fechaInicio, String fechaFin,
	    Integer estado, Integer estadoGestion, Integer estadoDetalle,
	    String ruta) {

	Connection conexion = null;
	db.OracleBD bdOracle = null;

	ResultSet resultados = null;
	ResultSet resultadosIc = null;
	ResultSet resultadosCanal = null;

	String listaTransaccion = "";
	StringBuilder sql = new StringBuilder();
	StringBuilder sqlIc = new StringBuilder();
	StringBuilder sqlCanal = new StringBuilder();

	File archivoSalida = new File(ruta);

	// LISTA PARA CREAR
	List<String[]> dataListBice = new ArrayList<String[]>();
	List<String[]> dataListIc = new ArrayList<String[]>();
	List<String[]> dataListCanal = new ArrayList<String[]>();

	List<String[]> dataListTodo = new ArrayList<String[]>();

	try {

	    log.info("Method: buscarInterfacesExport - Parametros Entrada : "
		    + " fechaInicio = " + fechaInicio + " fechaFin = "
		    + fechaFin + " estado = " + estado + " estadoGestion = "
		    + estadoGestion + " " + " estadoDetalle = " + estadoDetalle
		    + " ruta = " + ruta);
	    log.info("Ejecutando Consulta de interfaces BICE");

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    // CONSULTA BICE
	    sql.append(" SELECT "
		    + " NVL(TO_CHAR(CA.FECHA,'DD-MON-RR'),' ') AS fecha_filtro, "
		    + " NVL(TO_CHAR(TB.SID),' ') AS sid, "
		    + " NVL(TB.TIPO_REGISTRO,' ') AS tipoRegistro, "
		    + " NVL(TB.NUM_OPER_PROG,' ') AS numOperPro, "
		    + " NVL(TB.COD_SERVICIO,' ') AS cod_servicio, "
		    + " NVL(TB.RUT_ORDENANTE,' ') AS rutOrdenante, "
		    + " NVL(TB.NUM_CTA_ORDENANTE,' ') AS numCtaOrdenante, "
		    + " NVL(TB.ID_OPER_CLIENTE,' ') AS idOperCliente, "
		    + " NVL((CASE WHEN TB.COD_ESTADO = '070' THEN 'Realizada' WHEN TB.COD_ESTADO = '080' THEN 'Rechazada' END ),' ') AS codEstado, "
		    + " NVL(TB.GLS_ESTADO,' ') AS glosaEstado, "
		    + " NVL(TO_CHAR(TB.FECHA_INSTRUCCION,'DD-MON-RR'),' ') AS fechaInstruccion, "
		    + " NVL(TO_CHAR(TB.FECHA_CONTABLE,'DD-MON-RR'),' ')AS fechaContable, "
		    + " NVL(TB.COD_BANCO_BENEFICIARIO,' ') AS codBancoBenefi, "
		    + " NVL((CASE WHEN TB.TIPO_CTA_BENEFICIARIO = '100' THEN 'Cta.Corriente' WHEN TB.TIPO_CTA_BENEFICIARIO = '120' THEN 'Cta.Vista'  WHEN TB.TIPO_CTA_BENEFICIARIO = '300' THEN 'Cta.Ahorro'  END ),' ') AS tipoCtaBenefi, "
		    + " NVL(TB.CTA_BENEFICIARIO,' ') AS ctaBenefi, "
		    + " NVL(TB.RUT_BENEFICIARIO,' ') AS rutBenefi, "
		    + " NVL(TB.NOM_BENEFICIARIO,' ') AS nomBenefi, "
		    + " NVL(TO_CHAR(TB.MONTO),' ') AS monto "
		    + " FROM TBL_CONC_AVANCE CA "
		    + " INNER JOIN TBL_TRANSAC_AVA_BICE TB ON TB.SID = CA.SID_BICE "
		    + " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO "
		    + " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		    + " WHERE CA.SID IS NOT NULL "
		    + " AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  >= '" + fechaInicio
		    + "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '" + fechaFin
		    + "' ");

	    // CONSULTA IC/ONUS
	    sqlIc.append(" SELECT "
		    + " TON.SID AS sid,  "
		    + " NVL(TON.CODIGO_TRANSACCION,' ') AS codTransac,   "
		    + " NVL(TON.NUMERO_TARJETA,' ') AS numTrajeta,  "
		    + " NVL(TO_CHAR(TON.FECHA_COMPRA ,'DD-MON-RR'),' ') AS fechaCompra,    "
		    + " NVL(TO_CHAR(TON.FECHA_AUTORIZACION ,'DD-MON-RR'),' ') AS fechaAutoriza, "
		    + " NVL(TO_CHAR(TON.FECHA_POSTEO ,'DD-MON-RR'),' ') AS fechaPosteo,  "
		    + " NVL(TON.TIPO_VENTA,' ') AS tipoVenta, "
		    + " NVL(TON.NUM_CUOTAS,' ') AS numCuotas,   "
		    + " NVL(TON.NUM_MICROFILM,' ') AS numMicroFilm,  "
		    + " NVL(TON.NUM_COMERCIO,' ') AS numComercio, "
		    + " NVL(TO_CHAR(TON.MONTO_TRANSACCION),' ') AS montoTransac, "
		    + " NVL(TO_CHAR(TON.VALOR_CUOTA),' ') AS valorCuota,  "
		    + " NVL(TON.NOMBRE_COMERCIO,' ') AS nombreComercio, "
		    + " NVL(TON.CIUDAD_COMERCIO,' ') AS ciudadComercio, "
		    + " NVL(TON.RUBRO_COMERCIO,' ') AS rubroComercio,  "
		    + " NVL(TON.COD_AUTOR,' ') AS codAutor  "
		    + " FROM TBL_CONC_AVANCE CA  "
		    + " INNER JOIN TBL_TRANSAC_ONUS TON ON TON.SID = CA.SID_ONUS     "
		    + " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO      "
		    + " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		    + " WHERE CA.SID IS NOT NULL "
		    + " AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  >= '" + fechaInicio
		    + "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '" + fechaFin
		    + "' ");

	    // CONSULTA CANAL
	    sqlCanal.append(" SELECT "
		    + " NVL(TO_CHAR(TA.SID), ' ') AS sid,  "
		    + " NVL(TO_CHAR(TA.FECHA_TRANSFERENCIA ,'DD-MON-RR'),' ') AS fechaTransaferencia, "
		    + " NVL(TA.COD_AUTORIZACION,' ')AS codAutorizacion, "
		    + " NVL(TA.NUM_TRX,' ') AS numTrx, "
		    + " NVL(TA.NUM_TARJETA, ' ') AS numeroTarjeta, "
		    + " NVL(TO_CHAR(TA.MONTO), ' ') AS monto, "
		    + " NVL((CASE WHEN TA.ESTADO = '70 ' THEN 'Realizada' WHEN  TA.ESTADO = '80 ' THEN 'Rechazada'  WHEN (TA.ESTADO != '70 ' AND TA.ESTADO != '80 ') THEN 'Pendiente' END ),' ') AS codEstado, "
		    + " NVL(TO_CHAR(TA.SEGURO_MONTO), ' ') AS montoSeguro "
		    + " FROM TBL_CONC_AVANCE CA  "
		    + " INNER JOIN TBL_TRANSAC_AVANCES TA ON TA.SID = CA.SID_AVANCE  "
		    + " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO     "
		    + " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		    + " WHERE CA.SID IS NOT NULL "
		    + " AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  >= '" + fechaInicio
		    + "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '" + fechaFin
		    + "' ");

	    if (estado != 0) {
		sql.append(" AND TE.SID_TIPO = " + estado);
		sqlIc.append(" AND TE.SID_TIPO = " + estado);
		sqlCanal.append(" AND TE.SID_TIPO = " + estado);
	    }
	    if (estadoDetalle != 0) {
		sql.append(" AND CA.SID_ESTADO = " + estadoDetalle);
		sqlIc.append(" AND CA.SID_ESTADO = " + estadoDetalle);
		sqlCanal.append(" AND CA.SID_ESTADO = " + estadoDetalle);
	    }

	    if (estadoGestion == 88) {
		sql.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) IS NULL ");
		sqlIc.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) IS NULL ");
		sqlCanal.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) IS NULL ");
	    } else if (estadoGestion != 99 && estadoGestion != 88) {
		sql.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) = "
			+ estadoGestion);
		sqlIc.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) = "
			+ estadoGestion);
		sqlCanal.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) = "
			+ estadoGestion);
	    }

	    sql.append(" ORDER BY fecha_filtro DESC ");
	    sqlIc.append(" ORDER BY fechaCompra DESC ");
	    sqlCanal.append(" ORDER BY fechaTransaferencia DESC ");

	    // RESULTADO BICE
	    resultados = bdOracle.consultar(sql.toString());
	    bdOracle.setResultado(resultados);
	    // RESULTADO IC
	    resultadosIc = bdOracle.consultar(sqlIc.toString());
	    bdOracle.setResultado(resultadosIc);
	    // RESULTADO CANAL
	    resultadosCanal = bdOracle.consultar(sqlCanal.toString());
	    bdOracle.setResultado(resultadosCanal);

	    // GENERA CABECERA
	    String[] header = generateHeaderToExcelTodo();

	    // LLENAR CONTENIDO BICE
	    int countBice = 0;
	    while (resultados.next()) {
		countBice = countBice + 1;
		dataListBice.add(getRegistrosToExcelInterfacesBice(resultados,
			countBice));
	    }
	    dataListTodo.addAll(dataListBice);

	    // LLENAR CONTENIDO IC
	    int countIc = 0;
	    while (resultadosIc.next()) {
		countIc = countIc + 1;
		dataListIc.add(getRegistrosToExcelInterfacesIc(resultadosIc,
			countIc));
	    }

	    // LLENAR CONTENIDO IC
	    int countCanal = 0;
	    while (resultadosCanal.next()) {
		countCanal = countCanal + 1;
		dataListCanal.add(getRegistrosToExcelInterfacesCanal(
			resultadosCanal, countCanal));
	    }

	    int flag = 0;
	    if ((countBice >= countIc) && (countBice >= countCanal)) {
		flag = 1;
	    }
	    if ((countIc >= countBice) && (countIc >= countCanal)) {
		flag = 2;
	    }
	    if ((countCanal >= countIc) && (countCanal >= countBice)) {
		flag = 3;
	    }

	    if (estado == 2 || estado == 0) {
		if (flag == 1) {
		    int i = 0;
		    while (i < dataListBice.size()) {
			String[] prueba = new String[41];

			if (i < dataListIc.size()) {
			    prueba = dataListBice.get(i);
			    prueba[16] = dataListIc.get(i)[16];
			    prueba[17] = dataListIc.get(i)[17];
			    prueba[18] = dataListIc.get(i)[18];
			    prueba[19] = dataListIc.get(i)[19];
			    prueba[20] = dataListIc.get(i)[20];
			    prueba[21] = dataListIc.get(i)[21];
			    prueba[22] = dataListIc.get(i)[22];
			    prueba[23] = dataListIc.get(i)[23];
			    prueba[24] = dataListIc.get(i)[24];
			    prueba[25] = dataListIc.get(i)[25];
			    prueba[26] = dataListIc.get(i)[26];
			    prueba[27] = dataListIc.get(i)[27];
			    prueba[28] = dataListIc.get(i)[28];
			    prueba[29] = dataListIc.get(i)[29];
			    prueba[30] = dataListIc.get(i)[30];
			    prueba[31] = dataListIc.get(i)[31];
			    dataListTodo.set(i, prueba);
			}
			i++;
		    }

		    int i2 = 0;
		    while (i2 < dataListTodo.size()) {
			String[] prueba = new String[41];

			if (i2 < dataListCanal.size()) {
			    prueba = dataListTodo.get(i2);

			    prueba[32] = dataListCanal.get(i2)[32];
			    prueba[33] = dataListCanal.get(i2)[33];
			    prueba[34] = dataListCanal.get(i2)[34];
			    prueba[35] = dataListCanal.get(i2)[35];
			    prueba[36] = dataListCanal.get(i2)[36];
			    prueba[37] = dataListCanal.get(i2)[37];
			    prueba[38] = dataListCanal.get(i2)[38];
			    prueba[39] = dataListCanal.get(i2)[39];
			    dataListTodo.set(i2, prueba);
			}
			i2++;
		    }

		}

		if (flag == 2) {
		    int i = 0;
		    while (i < dataListIc.size()) {
			String[] prueba = new String[41];
			if (i < dataListIc.size()) {
			    prueba[16] = dataListIc.get(i)[16];
			    prueba[17] = dataListIc.get(i)[17];
			    prueba[18] = dataListIc.get(i)[18];
			    prueba[19] = dataListIc.get(i)[19];
			    prueba[20] = dataListIc.get(i)[20];
			    prueba[21] = dataListIc.get(i)[21];
			    prueba[22] = dataListIc.get(i)[22];
			    prueba[23] = dataListIc.get(i)[23];
			    prueba[24] = dataListIc.get(i)[24];
			    prueba[25] = dataListIc.get(i)[25];
			    prueba[26] = dataListIc.get(i)[26];
			    prueba[27] = dataListIc.get(i)[27];
			    prueba[28] = dataListIc.get(i)[28];
			    prueba[29] = dataListIc.get(i)[29];
			    prueba[30] = dataListIc.get(i)[30];
			    prueba[31] = dataListIc.get(i)[31];
			    dataListTodo.add(prueba);
			}
			i++;
		    }

		    int i2 = 0;
		    while (i2 < dataListIc.size()) {
			String[] prueba = new String[41];

			if (i2 < dataListCanal.size()) {
			    prueba = dataListTodo.get(i2);
			    prueba[32] = dataListCanal.get(i2)[32];
			    prueba[33] = dataListCanal.get(i2)[33];
			    prueba[34] = dataListCanal.get(i2)[34];
			    prueba[35] = dataListCanal.get(i2)[35];
			    prueba[36] = dataListCanal.get(i2)[36];
			    prueba[37] = dataListCanal.get(i2)[37];
			    prueba[38] = dataListCanal.get(i2)[38];
			    prueba[39] = dataListCanal.get(i2)[39];
			    dataListTodo.set(i2, prueba);
			}
			i2++;
		    }

		}

		if (flag == 3) {
		    int i = 0;
		    if (dataListBice.size() == 0) {
			while (i < dataListIc.size()) {
			    String[] prueba = new String[41];
			    if (i < dataListIc.size()) {
				prueba[16] = dataListIc.get(i)[16];
				prueba[17] = dataListIc.get(i)[17];
				prueba[18] = dataListIc.get(i)[18];
				prueba[19] = dataListIc.get(i)[19];
				prueba[20] = dataListIc.get(i)[20];
				prueba[21] = dataListIc.get(i)[21];
				prueba[22] = dataListIc.get(i)[22];
				prueba[23] = dataListIc.get(i)[23];
				prueba[24] = dataListIc.get(i)[24];
				prueba[25] = dataListIc.get(i)[25];
				prueba[26] = dataListIc.get(i)[26];
				prueba[27] = dataListIc.get(i)[27];
				prueba[28] = dataListIc.get(i)[28];
				prueba[29] = dataListIc.get(i)[29];
				prueba[30] = dataListIc.get(i)[30];
				prueba[31] = dataListIc.get(i)[31];
				dataListTodo.add(prueba);
			    }
			    i++;
			}

			int i2 = 0;
			while (i2 < dataListIc.size()) {
			    String[] prueba = new String[41];

			    if (i2 < dataListCanal.size()) {
				prueba = dataListTodo.get(i2);
				prueba[32] = dataListCanal.get(i2)[32];
				prueba[33] = dataListCanal.get(i2)[33];
				prueba[34] = dataListCanal.get(i2)[34];
				prueba[35] = dataListCanal.get(i2)[35];
				prueba[36] = dataListCanal.get(i2)[36];
				prueba[37] = dataListCanal.get(i2)[37];
				prueba[38] = dataListCanal.get(i2)[38];
				prueba[39] = dataListCanal.get(i2)[39];
				dataListTodo.set(i2, prueba);
			    }
			    i2++;
			}

		    }
		}
	    }
	    if (estado == 1) {
		int i = 0;
		while (i < dataListBice.size()) {
		    String[] prueba = new String[41];

		    if (i < dataListIc.size()) {
			prueba = dataListBice.get(i);
			prueba[16] = dataListIc.get(i)[16];
			prueba[17] = dataListIc.get(i)[17];
			prueba[18] = dataListIc.get(i)[18];
			prueba[19] = dataListIc.get(i)[19];
			prueba[20] = dataListIc.get(i)[20];
			prueba[21] = dataListIc.get(i)[21];
			prueba[22] = dataListIc.get(i)[22];
			prueba[23] = dataListIc.get(i)[23];
			prueba[24] = dataListIc.get(i)[24];
			prueba[25] = dataListIc.get(i)[25];
			prueba[26] = dataListIc.get(i)[26];
			prueba[27] = dataListIc.get(i)[27];
			prueba[28] = dataListIc.get(i)[28];
			prueba[29] = dataListIc.get(i)[29];
			prueba[30] = dataListIc.get(i)[30];
			prueba[31] = dataListIc.get(i)[31];
			dataListTodo.set(i, prueba);
		    }
		    i++;
		}

		int i2 = 0;
		while (i2 < dataListTodo.size()) {
		    String[] prueba = new String[41];

		    if (i2 < dataListCanal.size()) {
			prueba = dataListTodo.get(i2);

			prueba[32] = dataListCanal.get(i2)[32];
			prueba[33] = dataListCanal.get(i2)[33];
			prueba[34] = dataListCanal.get(i2)[34];
			prueba[35] = dataListCanal.get(i2)[35];
			prueba[36] = dataListCanal.get(i2)[36];
			prueba[37] = dataListCanal.get(i2)[37];
			prueba[38] = dataListCanal.get(i2)[38];
			prueba[39] = dataListCanal.get(i2)[39];
			dataListTodo.set(i2, prueba);
		    }
		    i2++;
		}
	    }

	    CSVWriter writer = new CSVWriter(new FileWriter(ruta), ';');
	    writer.writeNext(header);
	    writer.writeAll(dataListTodo);
	    writer.close();

	    resultados.close();
	    resultadosIc.close();

	} catch (SQLException e) {
	    bdOracle.setResultado(resultados);
	    bdOracle.setResultado(resultadosIc);
	    bdOracle.cerrarResultSet();
	    log.error(e.getMessage(), e);
	    listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	} finally {

	    if (!bdOracle.cerrarConexionBD()) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		return archivoSalida;
	    }
	}

	return archivoSalida;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * header y contenido bice, ic seleccionadas
     * 
     * @since 1.X
     */

    public File buscarInterfacesExportSeleccionadas(String fechaInicio,
	    String fechaFin, Integer estado, Integer estadoGestion,
	    Integer estadoDetalle, String ruta, String listaIds) {

	Connection conexion = null;
	db.OracleBD bdOracle = null;

	ResultSet resultados = null;
	ResultSet resultadosIc = null;
	ResultSet resultadosCanal = null;

	String listaTransaccion = "";
	StringBuilder sql = new StringBuilder();
	StringBuilder sqlIc = new StringBuilder();
	StringBuilder sqlCanal = new StringBuilder();

	File archivoSalida = new File(ruta);

	// LISTA PARA CREAR
	List<String[]> dataListBice = new ArrayList<String[]>();
	List<String[]> dataListIc = new ArrayList<String[]>();
	List<String[]> dataListCanal = new ArrayList<String[]>();

	List<String[]> dataListTodo = new ArrayList<String[]>();

	try {

	    log.info("Method: buscarInterfacesExportSeleccionadas - Parametros Entrada : "
		    + " fechaInicio = "
		    + fechaInicio
		    + " fechaFin = "
		    + fechaFin
		    + " estado = "
		    + estado
		    + " estadoGestion = "
		    + estadoGestion
		    + " "
		    + " estadoDetalle = "
		    + estadoDetalle
		    + " listaIds = " + listaIds + " ruta = " + ruta);

	    log.info("Ejecutando Consulta de interfaces seleccionadas");

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    // CONSULTA BICE
	    sql.append(" SELECT "
		    + " NVL(TO_CHAR(CA.FECHA,'DD-MON-RR'),' ') AS fecha_filtro, "
		    + " NVL(TO_CHAR(TB.SID),' ') AS sid, "
		    + " NVL(TB.TIPO_REGISTRO,' ') AS tipoRegistro, "
		    + " NVL(TB.NUM_OPER_PROG,' ') AS numOperPro, "
		    + " NVL(TB.COD_SERVICIO,' ') AS cod_servicio, "
		    + " NVL(TB.RUT_ORDENANTE,' ') AS rutOrdenante, "
		    + " NVL(TB.NUM_CTA_ORDENANTE,' ') AS numCtaOrdenante, "
		    + " NVL(TB.ID_OPER_CLIENTE,' ') AS idOperCliente, "
		    + " NVL((CASE WHEN TB.COD_ESTADO = '070' THEN 'Realizada' WHEN TB.COD_ESTADO = '080' THEN 'Rechazada' END ),' ') AS codEstado, "
		    + " NVL(TB.GLS_ESTADO,' ') AS glosaEstado, "
		    + " NVL(TO_CHAR(TB.FECHA_INSTRUCCION,'DD-MON-RR'),' ') AS fechaInstruccion, "
		    + " NVL(TO_CHAR(TB.FECHA_CONTABLE,'DD-MON-RR'),' ')AS fechaContable, "
		    + " NVL(TB.COD_BANCO_BENEFICIARIO,' ') AS codBancoBenefi, "
		    + " NVL((CASE WHEN TB.TIPO_CTA_BENEFICIARIO = '100' THEN 'Cta.Corriente' WHEN TB.TIPO_CTA_BENEFICIARIO = '120' THEN 'Cta.Vista'  WHEN TB.TIPO_CTA_BENEFICIARIO = '300' THEN 'Cta.Ahorro'  END ),' ') AS tipoCtaBenefi, "
		    + " NVL(TB.CTA_BENEFICIARIO,' ') AS ctaBenefi, "
		    + " NVL(TB.RUT_BENEFICIARIO,' ') AS rutBenefi, "
		    + " NVL(TB.NOM_BENEFICIARIO,' ') AS nomBenefi, "
		    + " NVL(TO_CHAR(TB.MONTO),' ') AS monto "
		    + " FROM TBL_CONC_AVANCE CA "
		    + " INNER JOIN TBL_TRANSAC_AVA_BICE TB ON TB.SID = CA.SID_BICE "
		    + " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO "
		    + " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		    + " WHERE CA.SID IN (" + listaIds + ")"
		    + " AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  >= '" + fechaInicio
		    + "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '" + fechaFin
		    + "' ");

	    // CONSULTA IC/ONUS
	    sqlIc.append(" SELECT "
		    + " TON.SID AS sid,  "
		    + " NVL(TON.CODIGO_TRANSACCION,' ') AS codTransac,   "
		    + " NVL(TON.NUMERO_TARJETA,' ') AS numTrajeta,  "
		    + " NVL(TO_CHAR(TON.FECHA_COMPRA ,'DD-MON-RR'),' ') AS fechaCompra,    "
		    + " NVL(TO_CHAR(TON.FECHA_AUTORIZACION ,'DD-MON-RR'),' ') AS fechaAutoriza, "
		    + " NVL(TO_CHAR(TON.FECHA_POSTEO ,'DD-MON-RR'),' ') AS fechaPosteo,  "
		    + " NVL(TON.TIPO_VENTA,' ') AS tipoVenta, "
		    + " NVL(TON.NUM_CUOTAS,' ') AS numCuotas,   "
		    + " NVL(TON.NUM_MICROFILM,' ') AS numMicroFilm,  "
		    + " NVL(TON.NUM_COMERCIO,' ') AS numComercio, "
		    + " NVL(TO_CHAR(TON.MONTO_TRANSACCION),' ') AS montoTransac, "
		    + " NVL(TO_CHAR(TON.VALOR_CUOTA),' ') AS valorCuota,  "
		    + " NVL(TON.NOMBRE_COMERCIO,' ') AS nombreComercio, "
		    + " NVL(TON.CIUDAD_COMERCIO,' ') AS ciudadComercio, "
		    + " NVL(TON.RUBRO_COMERCIO,' ') AS rubroComercio,  "
		    + " NVL(TON.COD_AUTOR,' ') AS codAutor  "
		    + " FROM TBL_CONC_AVANCE CA  "
		    + " INNER JOIN TBL_TRANSAC_ONUS TON ON TON.SID = CA.SID_ONUS     "
		    + " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO      "
		    + " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		    + " WHERE CA.SID IN (" + listaIds + ")"
		    + " AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  >= '" + fechaInicio
		    + "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '" + fechaFin
		    + "' ");

	    // CONSULTA CANAL
	    sqlCanal.append(" SELECT "
		    + " NVL(TO_CHAR(TA.SID), ' ') AS sid,  "
		    + " NVL(TO_CHAR(TA.FECHA_TRANSFERENCIA ,'DD-MON-RR'),' ') AS fechaTransaferencia, "
		    + " NVL(TA.COD_AUTORIZACION,' ')AS codAutorizacion, "
		    + " NVL(TA.NUM_TRX,' ') AS numTrx, "
		    + " NVL(TA.NUM_TARJETA, ' ') AS numeroTarjeta, "
		    + " NVL(TO_CHAR(TA.MONTO), ' ') AS monto, "
		    + " NVL((CASE WHEN TA.ESTADO = '70 ' THEN 'Realizada' WHEN  TA.ESTADO = '80 ' THEN 'Rechazada'  WHEN (TA.ESTADO != '70 ' AND TA.ESTADO != '80 ') THEN 'Pendiente' END ),' ') AS codEstado, "
		    + " NVL(TO_CHAR(TA.SEGURO_MONTO), ' ') AS montoSeguro "
		    + " FROM TBL_CONC_AVANCE CA  "
		    + " INNER JOIN TBL_TRANSAC_AVANCES TA ON TA.SID = CA.SID_AVANCE  "
		    + " LEFT JOIN TBL_CONC_ESTADO TE ON TE.SID = CA.SID_ESTADO     "
		    + " LEFT JOIN TBL_CONC_TIPO_ESTADO TPE ON TPE.SID = TE.SID_TIPO "
		    + " WHERE CA.SID IN (" + listaIds + ")"
		    + " AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  >= '" + fechaInicio
		    + "' AND TO_CHAR(CA.FECHA, 'YYYYMMDD')  <= '" + fechaFin
		    + "' ");

	    if (estado != 0) {
		sql.append(" AND TE.SID_TIPO = " + estado);
		sqlIc.append(" AND TE.SID_TIPO = " + estado);
		sqlCanal.append(" AND TE.SID_TIPO = " + estado);
	    }
	    if (estadoDetalle != 0) {
		sql.append(" AND CA.SID_ESTADO = " + estadoDetalle);
		sqlIc.append(" AND CA.SID_ESTADO = " + estadoDetalle);
		sqlCanal.append(" AND CA.SID_ESTADO = " + estadoDetalle);
	    }

	    if (estadoGestion == 88) {
		sql.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) IS NULL ");
		sqlIc.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) IS NULL ");
		sqlCanal.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) IS NULL ");
	    } else if (estadoGestion != 99 && estadoGestion != 88) {
		sql.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) = "
			+ estadoGestion);
		sqlIc.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) = "
			+ estadoGestion);
		sqlCanal.append(" AND FUNC_SIG_OBTENER_EST_GEST(CA.SID) = "
			+ estadoGestion);
	    }

	    sql.append(" ORDER BY fecha_filtro DESC ");
	    sqlIc.append(" ORDER BY fechaCompra DESC ");
	    sqlCanal.append(" ORDER BY fechaTransaferencia DESC ");

	    // RESULTADO BICE
	    resultados = bdOracle.consultar(sql.toString());
	    bdOracle.setResultado(resultados);
	    // RESULTADO IC
	    resultadosIc = bdOracle.consultar(sqlIc.toString());
	    bdOracle.setResultado(resultadosIc);
	    // RESULTADO CANAL
	    resultadosCanal = bdOracle.consultar(sqlCanal.toString());
	    bdOracle.setResultado(resultadosCanal);

	    // GENERA CABECERA
	    String[] header = generateHeaderToExcelTodo();

	    // LLENAR CONTENIDO BICE
	    int countBice = 0;
	    while (resultados.next()) {
		countBice = countBice + 1;
		dataListBice.add(getRegistrosToExcelInterfacesBice(resultados,
			countBice));
	    }
	    dataListTodo.addAll(dataListBice);

	    // LLENAR CONTENIDO IC
	    int countIc = 0;
	    while (resultadosIc.next()) {
		countIc = countIc + 1;
		dataListIc.add(getRegistrosToExcelInterfacesIc(resultadosIc,
			countIc));
	    }

	    // LLENAR CONTENIDO IC
	    int countCanal = 0;
	    while (resultadosCanal.next()) {
		countCanal = countCanal + 1;
		dataListCanal.add(getRegistrosToExcelInterfacesCanal(
			resultadosCanal, countCanal));
	    }

	    int flag = 0;
	    if ((countBice >= countIc) && (countBice >= countCanal)) {
		flag = 1;
	    }
	    if ((countIc >= countBice) && (countIc >= countCanal)) {
		flag = 2;
	    }
	    if ((countCanal >= countIc) && (countCanal >= countBice)) {
		flag = 3;
	    }

	    
	    //log.info("flag: "+flag);
	    
	    if (estado == 2 || estado == 0) {
		if (flag == 1) {
		    int i = 0;
		    while (i < dataListBice.size()) {
			String[] prueba = new String[41];

			if (i < dataListIc.size()) {
			    prueba = dataListBice.get(i);
			    prueba[16] = dataListIc.get(i)[16];
			    prueba[17] = dataListIc.get(i)[17];
			    prueba[18] = dataListIc.get(i)[18];
			    prueba[19] = dataListIc.get(i)[19];
			    prueba[20] = dataListIc.get(i)[20];
			    prueba[21] = dataListIc.get(i)[21];
			    prueba[22] = dataListIc.get(i)[22];
			    prueba[23] = dataListIc.get(i)[23];
			    prueba[24] = dataListIc.get(i)[24];
			    prueba[25] = dataListIc.get(i)[25];
			    prueba[26] = dataListIc.get(i)[26];
			    prueba[27] = dataListIc.get(i)[27];
			    prueba[28] = dataListIc.get(i)[28];
			    prueba[29] = dataListIc.get(i)[29];
			    prueba[30] = dataListIc.get(i)[30];
			    prueba[31] = dataListIc.get(i)[31];
			    dataListTodo.set(i, prueba);
			}
			i++;
		    }

		    int i2 = 0;
		    while (i2 < dataListTodo.size()) {
			String[] prueba = new String[41];

			if (i2 < dataListCanal.size()) {
			    prueba = dataListTodo.get(i2);

			    prueba[32] = dataListCanal.get(i2)[32];
			    prueba[33] = dataListCanal.get(i2)[33];
			    prueba[34] = dataListCanal.get(i2)[34];
			    prueba[35] = dataListCanal.get(i2)[35];
			    prueba[36] = dataListCanal.get(i2)[36];
			    prueba[37] = dataListCanal.get(i2)[37];
			    prueba[38] = dataListCanal.get(i2)[38];
			    prueba[39] = dataListCanal.get(i2)[39];
			    dataListTodo.set(i2, prueba);
			}
			i2++;
		    }

		}

		if (flag == 2) {
		    int i = 0;
		    while (i < dataListIc.size()) {
			String[] prueba = new String[41];
			if (i < dataListIc.size()) {
			    prueba[16] = dataListIc.get(i)[16];
			    prueba[17] = dataListIc.get(i)[17];
			    prueba[18] = dataListIc.get(i)[18];
			    prueba[19] = dataListIc.get(i)[19];
			    prueba[20] = dataListIc.get(i)[20];
			    prueba[21] = dataListIc.get(i)[21];
			    prueba[22] = dataListIc.get(i)[22];
			    prueba[23] = dataListIc.get(i)[23];
			    prueba[24] = dataListIc.get(i)[24];
			    prueba[25] = dataListIc.get(i)[25];
			    prueba[26] = dataListIc.get(i)[26];
			    prueba[27] = dataListIc.get(i)[27];
			    prueba[28] = dataListIc.get(i)[28];
			    prueba[29] = dataListIc.get(i)[29];
			    prueba[30] = dataListIc.get(i)[30];
			    prueba[31] = dataListIc.get(i)[31];
			    dataListTodo.add(prueba);
			}
			i++;
		    }

		    int i2 = 0;
		    while (i2 < dataListIc.size()) {
			String[] prueba = new String[41];

			if (i2 < dataListCanal.size()) {
			    prueba = dataListTodo.get(i2);
			    prueba[32] = dataListCanal.get(i2)[32];
			    prueba[33] = dataListCanal.get(i2)[33];
			    prueba[34] = dataListCanal.get(i2)[34];
			    prueba[35] = dataListCanal.get(i2)[35];
			    prueba[36] = dataListCanal.get(i2)[36];
			    prueba[37] = dataListCanal.get(i2)[37];
			    prueba[38] = dataListCanal.get(i2)[38];
			    prueba[39] = dataListCanal.get(i2)[39];
			    dataListTodo.set(i2, prueba);
			}
			i2++;
		    }

		}

		if (flag == 3) {
		    int i = 0;
		    if (dataListBice.size() == 0) {
			while (i < dataListIc.size()) {
			    String[] prueba = new String[41];
			    if (i < dataListIc.size()) {
				prueba[16] = dataListIc.get(i)[16];
				prueba[17] = dataListIc.get(i)[17];
				prueba[18] = dataListIc.get(i)[18];
				prueba[19] = dataListIc.get(i)[19];
				prueba[20] = dataListIc.get(i)[20];
				prueba[21] = dataListIc.get(i)[21];
				prueba[22] = dataListIc.get(i)[22];
				prueba[23] = dataListIc.get(i)[23];
				prueba[24] = dataListIc.get(i)[24];
				prueba[25] = dataListIc.get(i)[25];
				prueba[26] = dataListIc.get(i)[26];
				prueba[27] = dataListIc.get(i)[27];
				prueba[28] = dataListIc.get(i)[28];
				prueba[29] = dataListIc.get(i)[29];
				prueba[30] = dataListIc.get(i)[30];
				prueba[31] = dataListIc.get(i)[31];
				dataListTodo.add(prueba);
			    }
			    i++;
			}

			int i2 = 0;
			while (i2 < dataListIc.size()) {
			    String[] prueba = new String[41];

			    if (i2 < dataListCanal.size()) {
				prueba = dataListTodo.get(i2);
				prueba[32] = dataListCanal.get(i2)[32];
				prueba[33] = dataListCanal.get(i2)[33];
				prueba[34] = dataListCanal.get(i2)[34];
				prueba[35] = dataListCanal.get(i2)[35];
				prueba[36] = dataListCanal.get(i2)[36];
				prueba[37] = dataListCanal.get(i2)[37];
				prueba[38] = dataListCanal.get(i2)[38];
				prueba[39] = dataListCanal.get(i2)[39];
				dataListTodo.set(i2, prueba);
			    }
			    i2++;
			}

		    }
		}
	    }
	    if (estado == 1) {
		int i = 0;
		while (i < dataListBice.size()) {
		    String[] prueba = new String[41];

		    if (i < dataListIc.size()) {
			prueba = dataListBice.get(i);
			prueba[16] = dataListIc.get(i)[16];
			prueba[17] = dataListIc.get(i)[17];
			prueba[18] = dataListIc.get(i)[18];
			prueba[19] = dataListIc.get(i)[19];
			prueba[20] = dataListIc.get(i)[20];
			prueba[21] = dataListIc.get(i)[21];
			prueba[22] = dataListIc.get(i)[22];
			prueba[23] = dataListIc.get(i)[23];
			prueba[24] = dataListIc.get(i)[24];
			prueba[25] = dataListIc.get(i)[25];
			prueba[26] = dataListIc.get(i)[26];
			prueba[27] = dataListIc.get(i)[27];
			prueba[28] = dataListIc.get(i)[28];
			prueba[29] = dataListIc.get(i)[29];
			prueba[30] = dataListIc.get(i)[30];
			prueba[31] = dataListIc.get(i)[31];
			dataListTodo.set(i, prueba);
		    }
		    i++;
		}

		int i2 = 0;
		while (i2 < dataListTodo.size()) {
		    String[] prueba = new String[41];

		    if (i2 < dataListCanal.size()) {
			prueba = dataListTodo.get(i2);

			prueba[32] = dataListCanal.get(i2)[32];
			prueba[33] = dataListCanal.get(i2)[33];
			prueba[34] = dataListCanal.get(i2)[34];
			prueba[35] = dataListCanal.get(i2)[35];
			prueba[36] = dataListCanal.get(i2)[36];
			prueba[37] = dataListCanal.get(i2)[37];
			prueba[38] = dataListCanal.get(i2)[38];
			prueba[39] = dataListCanal.get(i2)[39];
			dataListTodo.set(i2, prueba);
		    }
		    i2++;
		}
	    }

	    CSVWriter writer = new CSVWriter(new FileWriter(ruta), ';');
	    writer.writeNext(header);
	    writer.writeAll(dataListTodo);
	    writer.close();

	    resultados.close();
	    resultadosIc.close();

	} catch (SQLException e) {
	    bdOracle.setResultado(resultados);
	    bdOracle.setResultado(resultadosIc);
	    bdOracle.cerrarResultSet();
	    log.error(e.getMessage(), e);
	    listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	} finally {

	    if (!bdOracle.cerrarConexionBD()) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		return archivoSalida;
	    }
	}

	return archivoSalida;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * headerBICE, IC, CANAL
     * 
     * @since 1.X
     */
    private String[] generateHeaderToExcelTodo() {

	return new String[] { "TIPO REGISTRO", "NUM OPER PROG", "COD SERVICIO",
		"RUT ORDENANTE", "NUM CTA ORDENANTE", "ID OPER CLIENTE",
		"COD ESTADO", "GLOSA ESTADO", "FECHA INSTRUCCION",
		"FECHA CONTABLE", "BANCO BENEFICIARIO",
		"TIPO CTA BENEFICIARIO", "CTA BENEFICIARIO",
		"RUT BENEFICIARIO", "NOMBRE BENEFICIARIO", "MONTO", " ",
		"COD TRANSACCION", "NUM TARJETA", "FECHA COMPRA",
		"FECHA AUTORIZACION", "FECHA POSTEO", "TIPO VENTA",
		"NUM CUOTAS", "NUM MICRO FILM", "NUM COMERCIO",
		"MONTO TRANSACCION", "VALOR CUOTA", "NOMBRE COMERCIO",
		"CIUDAD COMERCIO", "RUBRO COMERCIO", "COD AUTOR", " ",
		"FECHA TRANSFERENCIA", "COD AUTORIZACION", "NUM TRX",
		"NUM TARJETA", "MONTO", "ESTADO", "SEGURO_MONTO" };

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * recibe result BICE, IC, CANAL
     * 
     * @since 1.X
     */
    private String[] getRegistrosToExcelInterfacesBice(ResultSet resultados,
	    int countBice) {

	String[] datos = new String[41];
	try {
	    // BICE
	    datos[0] = resultados.getString("tipoRegistro");
	    datos[1] = resultados.getString("numOperPro");
	    datos[2] = resultados.getString("cod_servicio");
	    datos[3] = resultados.getString("rutOrdenante");
	    datos[4] = resultados.getString("numCtaOrdenante");
	    datos[5] = resultados.getString("idOperCliente");
	    datos[6] = resultados.getString("codEstado");
	    datos[7] = resultados.getString("glosaEstado");
	    datos[8] = resultados.getString("fechaInstruccion");
	    datos[9] = resultados.getString("fechaContable");
	    datos[10] = resultados.getString("codBancoBenefi");
	    datos[11] = resultados.getString("tipoCtaBenefi");
	    datos[12] = resultados.getString("ctaBenefi");
	    datos[13] = resultados.getString("rutBenefi");
	    datos[14] = resultados.getString("nomBenefi");
	    datos[15] = Utils.formateaMonto(resultados.getString("monto"));
	    return datos;

	} catch (SQLException e) {
	    log.error(
		    "ConsultarTrxAvancesDAO-getRegistrosToExcelInterfaces : Ha ocurrido un error al parsear la trx de Avances bice.",
		    e);
	}

	return null;

    }

    private String[] getRegistrosToExcelInterfacesIc(ResultSet resultadosIc,
	    int countIc) {

	String[] datos = new String[41];
	try {
	    // IC
	    datos[16] = " ";
	    datos[17] = resultadosIc.getString("codTransac");
	    datos[18] = resultadosIc.getString("numTrajeta");
	    datos[19] = resultadosIc.getString("fechaCompra");
	    datos[20] = resultadosIc.getString("fechaAutoriza");
	    datos[21] = resultadosIc.getString("fechaPosteo");
	    datos[22] = resultadosIc.getString("tipoVenta");
	    datos[23] = resultadosIc.getString("numCuotas");
	    datos[24] = resultadosIc.getString("numMicroFilm");
	    datos[25] = resultadosIc.getString("numComercio");
	    datos[26] = Utils.formateaMonto(resultadosIc
		    .getString("montoTransac"));
	    datos[27] = Utils.formateaMonto(resultadosIc
		    .getString("valorCuota"));
	    datos[28] = resultadosIc.getString("nombreComercio");
	    datos[29] = resultadosIc.getString("ciudadComercio");
	    datos[30] = resultadosIc.getString("rubroComercio");
	    datos[31] = resultadosIc.getString("codAutor");

	    return datos;
	} catch (SQLException e) {
	    log.error(
		    "ConsultarTrxAvancesDAO-getRegistrosToExcelInterfacesIc : Ha ocurrido un error al parsear la trx de Avances Ic.",
		    e);
	}

	return null;

    }

    private String[] getRegistrosToExcelInterfacesCanal(
	    ResultSet resultadosCanal, int countCanal) {
	String[] datos = new String[41];
	try {
	    datos[32] = " ";
	    datos[33] = resultadosCanal.getString("fechaTransaferencia");
	    datos[34] = resultadosCanal.getString("codAutorizacion");
	    datos[35] = resultadosCanal.getString("numTrx");
	    datos[36] = resultadosCanal.getString("numeroTarjeta");
	    datos[37] = Utils.formateaMonto(resultadosCanal.getString("monto"));
	    datos[38] = resultadosCanal.getString("codEstado");
	    datos[39] =  Utils.formateaMonto(resultadosCanal.getString("montoSeguro"));

	    return datos;
	} catch (SQLException e) {
	    log.error(
		    "ConsultarTrxAvancesDAO-getRegistrosToExcelInterfacesCanal : Ha ocurrido un error al parsear la trx de Avances Canal.",
		    e);
	}

	return null;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/11/2018, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @throws AppException
     * @since 1.X
     */

    public void guardarCargoMasivo(List<CargaMasivaDTO> listaTodo)
	    throws AppException {

	if (listaTodo == null) {
	    log.warn("No se ha ingresado ninguna transaccion y lista viene nula");
	    throw new AppException("No se ha guardado ning�n cargo/abono ");

	} else if (listaTodo != null && listaTodo.isEmpty()) {
	    log.warn("No se ha ingresado ninguna transaccion la lista viene vac�a");
	    throw new AppException("No se ha guardado ning�n cTransaccion.");
	}

	for (CargaMasivaDTO cargaMasivaDTO : listaTodo) {

	    insertGestionMasiva(cargaMasivaDTO);

	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/11/2018, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Inserta registro masivo de gestion
     * 
     * @throws AppException
     * @since 1.X
     */
    public void insertGestionMasiva(CargaMasivaDTO cargaMasivaDTO)
	    throws AppException {

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedureMasivo = null;

	try {

	    log.info("Ejecutando SP - SP_SIG_CARGA_AVA_GEST_MASIVO");

	    // INICIA CONEXIONES
	    bdOracle = new OracleBD();

	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    stmtProcedureMasivo = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_CARGA_AVA_GEST_MASIVO(?, ?, ?, ?, ?, ?); END;");

	    // ENTRADA
	    stmtProcedureMasivo.setLong("sidConci", cargaMasivaDTO.getSid());
	    stmtProcedureMasivo.setString("comentario",
		    cargaMasivaDTO.getComentario());
	    stmtProcedureMasivo.setLong("gestion", cargaMasivaDTO.getGestion());
	    stmtProcedureMasivo.setLong("idUser", cargaMasivaDTO.getIdUser());

	    // SALIDA
	    stmtProcedureMasivo.registerOutParameter("warning",
		    OracleTypes.VARCHAR);
	    stmtProcedureMasivo.registerOutParameter("cod_error",
		    OracleTypes.NUMBER);

	    stmtProcedureMasivo.execute();

	    String warning = stmtProcedureMasivo.getString("warning");
	    int codError = stmtProcedureMasivo.getInt("cod_error");

	    log.info("Warning : " + warning);
	    log.info("CodError : " + codError);

	    if (codError == 1) {
		throw new AppException("EXISTE");
	    }
	    // fin

	    // fin
	    log.info("Se ha almacenado la gestion masiva");
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	} finally {

	    try {
		if (stmtProcedureMasivo != null
			&& !stmtProcedureMasivo.isClosed()) {
		    stmtProcedureMasivo.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }

	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }

	}

    }

}
