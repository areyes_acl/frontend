package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.CargoAbonoStatusType;
import types.CargoAbonoType;
import Beans.CargoAbonoBean;
import cl.filter.FiltroCargoAbono;
import cl.util.Utils;
import db.OracleBD;
import exception.AppException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * @author afreire
 * 
 *         Clase que gestiona la interacci�n con la base de datos para el modulo
 *         de p�rdida
 * 
 *         <p>
 *         <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class CargoAbonoPendDAO {

	private static final Logger log = Logger.getLogger(CargoAbonoPendDAO.class);
	private String esquema;
	private final String ORACLE_EXCEPTION = "ORA-00001";

	// PAGINACION
	private int ultimaPagina = 0;
	private int pagActual;
	private String cantidadRegistros = "";

	// ATRIBUTOS PARA PAGINACION
	public int getUltimaPagina() {
		return ultimaPagina;
	}

	public int getPagActual() {
		return pagActual;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * Constructor de la clase
	 * 
	 * @since 1.X
	 */
	public CargoAbonoPendDAO() {
		try {
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();
			esquema = propiedades.getProperty("esquema");
			cantidadRegistros = propiedades.getProperty("cantidadRegistros");
		} catch (Exception e) {
			log.error("Error al leer el archivo de propiedades.");
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * Obtiene la lista de cargos y abonos sin paginacion
	 * 
	 * 
	 * @param Perdida
	 * @return
	 * @throws Exception
	 * @throws
	 * @since 1.X
	 */
	public List<CargoAbonoBean> buscaCargosAbonos(FiltroCargoAbono filtro)
			throws Exception {

		CallableStatement stmtProcedure = null;
		ResultSet rs = null;
		OracleBD bdOracle = new OracleBD();
		Connection conexion = bdOracle.conectar();
		List<CargoAbonoBean> listaCargAbo = null;

		try {

			bdOracle.setConexion(conexion);
			log.info("*** SE EJECUTA SP_SIG_BUSCAR_CARGOABO_PEND( ?, ?, ?, ?, ?, ?, ?, ?, ? ) ***");

			stmtProcedure = conexion
					.prepareCall("BEGIN "
							+ esquema
							+ ".SP_SIG_BUSCAR_CARGOABO_PEND( ?, ?, ?, ?, ?, ?, ?, ?, ?); END;");

			stmtProcedure.setString("p_fechaDesde", filtro.getFechaDesde());
			stmtProcedure.setString("p_fechaHasta", filtro.getFechaHasta());
			stmtProcedure.setInt("p_sid_estado", filtro.getEstado().getSid());
			stmtProcedure.setInt("p_sid_tipo_gestion", filtro.getTipoCargo()
					.getSid());
			stmtProcedure.setString("p_nro_tarjeta", filtro.getNumeroTarjeta());
			stmtProcedure.setLong("p_sid_user", filtro.getSidUsuario());
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError = stmtProcedure.getInt("cod_error");
			String warning = stmtProcedure.getString("warning");

			if (codigoError == 0) {
				listaCargAbo = parsearListaCargoAbono(rs);
			} else {
				throw new Exception(warning);
			}

		} finally {
			cerrarConexiones(conexion, bdOracle, stmtProcedure, rs);
		}
		return listaCargAbo;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo que realiza la busqueda de registros de cargos y abonos sin
	 * paginaci�n (se utiliza para exportar a excel todos los registros)
	 * 
	 * @param filtro
	 * @param numPagina
	 * @return
	 * @throws Exception
	 * @since 1.X
	 */
	public List<CargoAbonoBean> buscaCargosAbonos(FiltroCargoAbono filtro,
			String numPagina) throws Exception {

		CallableStatement stmtProcedure = null;
		ResultSet rs = null;
		OracleBD bdOracle = new OracleBD();
		Connection conexion = bdOracle.conectar();
		List<CargoAbonoBean> listaCargAbo = null;

		try {

			// LOGICA PAGINACION
			if (numPagina == null) {
				pagActual = 1;
			} else {
				pagActual = Integer.parseInt(numPagina);
			}

			bdOracle.setConexion(conexion);
			log.info("*** SE EJECUTA SP_SIG_BUSCAR_CARGOABO_PEND_PG( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ***");

			stmtProcedure = conexion
					.prepareCall("BEGIN "
							+ esquema
							+ ".SP_SIG_BUSCAR_CARGOABO_PEND_PG( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); END;");

			stmtProcedure.setString("p_fechaDesde", filtro.getFechaDesde());
			stmtProcedure.setString("p_fechaHasta", filtro.getFechaHasta());
			stmtProcedure.setInt("p_sid_estado", filtro.getEstado().getSid());
			stmtProcedure.setInt("p_sid_tipo_gestion", filtro.getTipoCargo()
					.getSid());
			stmtProcedure.setString("p_nro_tarjeta", filtro.getNumeroTarjeta());
			stmtProcedure.setLong("p_sid_user", filtro.getSidUsuario());
			stmtProcedure.setInt("numPagina", pagActual);
			stmtProcedure
					.setInt("cantReg", Integer.parseInt(cantidadRegistros));
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError = stmtProcedure.getInt("cod_error");
			String warning = stmtProcedure.getString("warning");

			if (codigoError == 0) {
				float totReg = stmtProcedure.getInt("totReg");
				ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));

				listaCargAbo = parsearListaCargoAbono(rs);
			} else {
				throw new Exception(warning);
			}

		} finally {
			cerrarConexiones(conexion, bdOracle, stmtProcedure,rs);
		}
		return listaCargAbo;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de realizar la llamada a BD para eliminar un cargo y
	 * abono ingresado
	 * 
	 * @param sid
	 * @throws AppException
	 * @since 1.X
	 */
	public void eliminarCargoAbono(Long sid) throws AppException {

		CallableStatement stmtProcedure = null;
		OracleBD bdOracle = new OracleBD();
		Connection conexion = bdOracle.conectar();

		try {

			bdOracle.setConexion(conexion);
			log.info("*** SE EJECUTA SP_SIG_DELETE_CARG_ABO( ?, ?, ?  ) ***");

			stmtProcedure = conexion.prepareCall("BEGIN " + esquema
					+ ".SP_SIG_DELETE_CARG_ABO( ?, ?, ?); END;");

			stmtProcedure.setLong("p_sid_carg_abo", sid);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);

			stmtProcedure.execute();

			int codigoError = stmtProcedure.getInt("cod_error");
			String warning = stmtProcedure.getString("warning");

			if (codigoError != 0) {
				throw new AppException(warning);
			}

		} catch (SQLException e) {
			throw new AppException(e);
		} finally {
			cerrarConexiones(conexion, bdOracle,stmtProcedure,null);
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Cerrar conexiones
	 * 
	 * @param conexion
	 * @param bdOracle
	 * @since 1.X
	 */
	private void cerrarConexiones(Connection conexion, OracleBD bdOracle,
			CallableStatement callstatement, ResultSet rs) {
		if (bdOracle.cerrarResultSet() == false) {
			log.error("Error: No se pudo cerrar resulset.");
			if (bdOracle.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}
		} else {
			if (bdOracle.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}
		}
		try {
			if (conexion != null && !conexion.isClosed()) {
				conexion.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}

		try {
			if (callstatement != null && !callstatement.isClosed()) {
				callstatement.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}

		try {
			if (rs != null && !rs.isClosed()) {
				rs.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 * @since 1.X
	 */
	private List<CargoAbonoBean> parsearListaCargoAbono(ResultSet rs)
			throws SQLException {
		List<CargoAbonoBean> listaCargAbo = new ArrayList<CargoAbonoBean>();

		while (rs.next()) {
			CargoAbonoBean cargAbo = new CargoAbonoBean();
			cargAbo.setSid(rs.getLong("SID_CARGO_ABONO"));
			cargAbo.setNumeroTarjeta(rs.getString("NRO_TARJETA"));
			cargAbo.setFechaTransaccion(rs.getString("FECHATRANSAC"));
			cargAbo.setComercio(rs.getString("COMERCIO"));
			cargAbo.setMontoOriginal(Utils.formateaMonto(rs.getString("MONTO_ORIG")));
			cargAbo.setMontoCargoAbono(Utils.formateaMonto(rs.getString("MONTO_CARGO_ABONO")));
			cargAbo.setEstado(CargoAbonoStatusType.findById(rs
					.getInt("ESTADO_CARG_ABO")));
			cargAbo.setCodRazon(rs.getString("CODRAZON"));
			cargAbo.setNumIncidente(rs.getString("NUM_INCIDENTE"));
			cargAbo.setUsername(rs.getString("USUARIO"));
			cargAbo.setTipo(CargoAbonoType.findById(rs.getInt("TIPO")));
			cargAbo.setFechaCargoAbono(rs.getString("FECHA_CARGO_ABONO"));
			listaCargAbo.add(cargAbo);
		}
		return listaCargAbo;
	}

}
