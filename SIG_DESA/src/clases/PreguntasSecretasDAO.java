package clases;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import cl.util.CryptUtils;

import properties.PropertiesConfiguration;

import db.OracleBD;

public class PreguntasSecretasDAO {
    static final Logger log =  Logger.getLogger(UsuariosDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    private String esquema;

    private OracleBD obd = new OracleBD();
    private Properties prop = new Properties();
    private Connection conexion;
    
    public void setConexion() {
	this.conexion = obd.conectar();
    }
    
    private ResultSet rs;

    public PreguntasSecretasDAO(){
	try {
	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    esquema = propiedades.getProperty("esquema");
	}catch (Exception e){          
	    log.error("Error al leer el archivo de propiedades.");
	    log.error(e.getMessage(), e);
	}
    }

    public String cargarPreguntasSecretas(Integer sidusuario){
	Connection conexion=null;
	db.OracleBD bdOracle=null;
	ResultSet resultados = null;

	try {
	    log.info("Method: cargarPreguntasSecretas");

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);
	    
	    String query = "SELECT DISTINCT PRE.SID, PRE.PREGUNTA, PRE.ESTADO FROM TBL_PREGUNTAS PRE " +
	    		"LEFT JOIN TBL_PREGUNTAS_USUARIO PREUS ON PREUS.SID_REF_PREG = PRE.SID AND PREUS.ESTADO = 1 " +
	    		"WHERE PRE.ESTADO = 1 OR PREUS.SID_REF_USUARIO = " + sidusuario;

	    //log.info("Ejecutando SQL : " + query);

	    resultados = bdOracle.consultar(query);

	    bdOracle.setResultado(resultados);

	    String listaPreguntas="esttrx:0~";
	    while (resultados.next()){
		listaPreguntas = listaPreguntas + resultados.getString("SID") + "|" + resultados.getString("PREGUNTA") + "|" + resultados.getString("ESTADO") + "~";
		log.info("Rescatando registro : " + resultados.getString("SID") + "|" + resultados.getString("PREGUNTA") + "|" + resultados.getString("ESTADO"));
	    }

	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error No se pudo cerrar resulset.");
		return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
	    }

	    if (bdOracle.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }

	    return listaPreguntas;
	}catch (SQLException e){
	    bdOracle.setResultado(resultados);
	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error No se pudo cerrar resulset.");
		return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
	    }

	    if (bdOracle.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }

	    log.error(e.getMessage(), e);
	    return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	}finally{
	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}			
	    }else{
		if (bdOracle.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}	
	    }
	}
    }

    public String cargarPreguntasRespuestasSecretasUsuario(String where){
	Connection conexion=null;
	db.OracleBD bdOracle=null;
	ResultSet resultados = null;

	try {
	    log.info("Method: cargarPreguntasSecretasUsuario");

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    String query = "SELECT " +
	    		"PREUSER.SID IDPREUSER, " +
	    		"PREUSER.SID_REF_PREG AS IDPREGUNTA, " +
	    		"PREGUNTA.PREGUNTA, " +
	    		"PREUSER.ESTADO, " +
	    		"PREUSER.RESPUESTA_PREGUNTA " +
	    		"FROM " + esquema +".TBL_PREGUNTAS_USUARIO PREUSER " +
	    		"INNER JOIN " + esquema +".TBL_PREGUNTAS PREGUNTA ON PREGUNTA.SID = PREUSER.SID_REF_PREG ";

	    if(!where.isEmpty()){
		query = query + " where " + where;
	    }

	    //log.info("Ejecutando SQL : " + query);

	    resultados = bdOracle.consultar(query);

	    bdOracle.setResultado(resultados);

	    String listaPreguntas="esttrx:0~";
	    while (resultados.next()){
		listaPreguntas = listaPreguntas + resultados.getString("IDPREUSER") + "|" + resultados.getString("IDPREGUNTA") + "|" + resultados.getString("PREGUNTA")  + "|" + CryptUtils.decodeBase64(resultados.getString("RESPUESTA_PREGUNTA")) + "~";
		log.info("Rescatando registro : " + resultados.getString("IDPREUSER") + "|" + resultados.getString("IDPREGUNTA") + "|" + resultados.getString("PREGUNTA") + "|" + CryptUtils.decodeBase64(resultados.getString("RESPUESTA_PREGUNTA")));
	    }

	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error No se pudo cerrar resulset.");
		return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
	    }

	    if (bdOracle.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }

	    return listaPreguntas;
	}catch (SQLException e){
	    bdOracle.setResultado(resultados);
	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error No se pudo cerrar resulset.");
		return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
	    }

	    if (bdOracle.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }

	    log.error(e.getMessage(), e);
	    return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	}finally{
	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}			
	    }else{
		if (bdOracle.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}	
	    }
	}
    }

    public String cargarPreguntasSecretasUsuario(String where){
	Connection conexion=null;
	db.OracleBD bdOracle=null;
	ResultSet resultados = null;

	try {
	    log.info("Method: cargarPreguntasSecretasUsuario");

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    String query = "SELECT " +
	    		"PREUSER.SID IDPREUSER, " +
	    		"PREUSER.SID_REF_PREG AS IDPREGUNTA, " +
	    		"PREGUNTA.PREGUNTA, " +
	    		"PREUSER.ESTADO " +
	    		"FROM " + esquema +".TBL_PREGUNTAS_USUARIO PREUSER " +
	    		"INNER JOIN " + esquema +".TBL_PREGUNTAS PREGUNTA ON PREGUNTA.SID = PREUSER.SID_REF_PREG ";

	    if(!where.isEmpty()){
		query = query + " where " + where;
	    }

	    //log.info("Ejecutando SQL : " + query);

	    resultados = bdOracle.consultar(query);

	    bdOracle.setResultado(resultados);

	    Integer contador = 0;
	    String listaPreguntas="esttrx:0~";
	    while (resultados.next()){
		listaPreguntas = listaPreguntas + resultados.getString("IDPREUSER") + "|" + resultados.getString("IDPREGUNTA") + "|" + resultados.getString("PREGUNTA") + "~";
		log.info("Rescatando registro : " + resultados.getString("IDPREUSER") + "|" + resultados.getString("IDPREGUNTA") + "|" + resultados.getString("PREGUNTA"));
		contador++;
	    }

	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error No se pudo cerrar resulset.");
		return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
	    }

	    if (bdOracle.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }

	    if(contador < 3){
		return "esttrx:2~";
	    }else{
		return listaPreguntas;
	    }
	}catch (SQLException e){
	    bdOracle.setResultado(resultados);
	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error No se pudo cerrar resulset.");
		return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
	    }

	    if (bdOracle.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }

	    log.error(e.getMessage(), e);
	    return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	}finally{
	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}			
	    }else{
		if (bdOracle.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}	
	    }
	}
    }

    public String guardarPreguntasSecretasUsuario(Integer p1, Integer p2, Integer p3, String r1, String r2, String r3, Integer sidusuario){
	Connection conexion=null;
	db.OracleBD bdOracle=null;
	ResultSet resultados = null;

	setConexion();

        log.info( "Method: guardarPreguntasSecretasUsuario - insertando parametros: " 
        	+ "sidusuario-> " + sidusuario 
        	+ " | p1-> " + p1 + " | p2-> " + p2 + " | p3-> " + p3
        	+ " | r1-> " + r1 + " | r2-> " + r2 + " | r3-> " + r3
                + " | sidusuario-> " + sidusuario );

	String respuesta ="esttrx:0~";
	Integer idRespuesta = 0;
	
	//desactivar todas las respuestas de ese usuario
	String strsql = "UPDATE TBL_PREGUNTAS_USUARIO SET ESTADO = 0 WHERE SID_REF_USUARIO = " + sidusuario;
	try {
	    Boolean res = obd.ejecutar(strsql);

	    if (res != true){
		log.error("Error al actualizar las respuestas del usuario : " + sidusuario);
	    }
	} catch (Exception e){
	    log.error( e.getMessage(), e );
	}

	//consulta si la pregunta 1 esta activa para el usuario
	strsql = "SELECT SID FROM TBL_PREGUNTAS_USUARIO WHERE SID_REF_USUARIO = " + sidusuario + " AND SID_REF_PREG = " + p1;

	try {
	    rs = obd.consultar(strsql);
	    obd.setResultado(rs);
	    if (rs.next()){
		idRespuesta = rs.getInt("SID");
	    }
	    obd.cerrarResultSet();
	} catch (SQLException e){
	    log.error( e.getMessage(), e );
	}

	if(idRespuesta > 0){
	    //actualizar respuesta
	    strsql = "UPDATE TBL_PREGUNTAS_USUARIO SET RESPUESTA_PREGUNTA = '" + CryptUtils.encodeBase64(r1) + "', ESTADO = 1 WHERE SID = " + idRespuesta;
	    try {
		Boolean res = obd.ejecutar(strsql);

		if (res != true){
		    log.error("Error al actualizar la pregunta : " + idRespuesta);
		}
	    } catch (Exception e){
		log.error( e.getMessage(), e );
	    }
	}else{
	    //insertar respuesta
	    strsql = "INSERT INTO TBL_PREGUNTAS_USUARIO (SID,SID_REF_USUARIO,ESTADO,RESPUESTA_PREGUNTA,SID_REF_PREG) " +
	    		"VALUES(SEQ_TBL_PREGUNTAS_USUARIO.NEXTVAL," + sidusuario + ",1,'" + CryptUtils.encodeBase64(r1) + "'," + p1 + ")";
	    try {
		Boolean res = obd.ejecutar(strsql);

		if (res != true){
		    log.error("Error al insertar la pregunta 1");
		}
	    } catch (Exception e){
		log.error( e.getMessage(), e );
	    }
	}

	//consulta si la pregunta 2 esta activa para el usuario
	idRespuesta = 0;
	strsql = "SELECT SID FROM TBL_PREGUNTAS_USUARIO WHERE SID_REF_USUARIO = " + sidusuario + " AND SID_REF_PREG = " + p2;

	try {
	    rs = obd.consultar(strsql);
	    obd.setResultado(rs);
	    if (rs.next()){
		idRespuesta = rs.getInt("SID");
	    }
	    obd.cerrarResultSet();
	} catch (SQLException e){
	    log.error( e.getMessage(), e );
	}

	if(idRespuesta > 0){
	    //actualizar respuesta
	    strsql = "UPDATE TBL_PREGUNTAS_USUARIO SET RESPUESTA_PREGUNTA = '" + CryptUtils.encodeBase64(r2) + "', ESTADO = 1 WHERE SID = " + idRespuesta;
	    try {
		Boolean res = obd.ejecutar(strsql);

		if (res != true){
		    log.error("Error al actualizar la pregunta : " + idRespuesta);
		}
	    } catch (Exception e){
		log.error( e.getMessage(), e );
	    }
	}else{
	    //insertar respuesta
	    strsql = "INSERT INTO TBL_PREGUNTAS_USUARIO (SID,SID_REF_USUARIO,ESTADO,RESPUESTA_PREGUNTA,SID_REF_PREG) " +
	    		"VALUES(SEQ_TBL_PREGUNTAS_USUARIO.NEXTVAL," + sidusuario + ",1,'" + CryptUtils.encodeBase64(r2) + "'," + p2 + ")";
	    try {
		Boolean res = obd.ejecutar(strsql);

		if (res != true){
		    log.error("Error al insertar la pregunta 2");
		}
	    } catch (Exception e){
		log.error( e.getMessage(), e );
	    }
	}

	//consulta si la pregunta 3 esta activa para el usuario
	idRespuesta = 0;
	strsql = "SELECT SID FROM TBL_PREGUNTAS_USUARIO WHERE SID_REF_USUARIO = " + sidusuario + " AND SID_REF_PREG = " + p3;

	try {
	    rs = obd.consultar(strsql);
	    obd.setResultado(rs);
	    if (rs.next()){
		idRespuesta = rs.getInt("SID");
	    }
	    obd.cerrarResultSet();
	} catch (SQLException e){
	    log.error( e.getMessage(), e );
	}

	if(idRespuesta > 0){
	    //actualizar respuesta
	    strsql = "UPDATE TBL_PREGUNTAS_USUARIO SET RESPUESTA_PREGUNTA = '" + CryptUtils.encodeBase64(r3) + "', ESTADO = 1 WHERE SID = " + idRespuesta;
	    try {
		Boolean res = obd.ejecutar(strsql);

		if (res != true){
		    log.error("Error al actualizar la pregunta : " + idRespuesta);
		}
	    } catch (Exception e){
		log.error( e.getMessage(), e );
	    }
	}else{
	    //insertar respuesta
	    strsql = "INSERT INTO TBL_PREGUNTAS_USUARIO (SID,SID_REF_USUARIO,ESTADO,RESPUESTA_PREGUNTA,SID_REF_PREG) " +
	    		"VALUES(SEQ_TBL_PREGUNTAS_USUARIO.NEXTVAL," + sidusuario + ",1,'" + CryptUtils.encodeBase64(r3) + "'," + p3 + ")";

	    try {
		Boolean res = obd.ejecutar(strsql);

		if (res != true){
		    log.error("Error al insertar la pregunta 3");
		}
	    } catch (Exception e){
		log.error( e.getMessage(), e );
	    }
	}

	if (obd.cerrarResultSet()==false){
	    log.error("Error: No se pudo cerrar resulset.");
	    if (obd.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		//return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }
	    //return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
	}else{
	    if (obd.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		//return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }
	}

	try {
	    if(conexion != null && !conexion.isClosed()){
		conexion.close();
	    }
	}catch ( SQLException e ) {
	    log.error( e.getMessage(), e );
	}

	return respuesta;
    }

    public String checkPreguntasSecretasUsuario(Integer p1, Integer p2, Integer p3, String r1, String r2, String r3, Integer sidusuario){
	Connection conexion=null;
	db.OracleBD bdOracle=null;
	ResultSet resultados = null;

	r1 = CryptUtils.encodeBase64(r1);
	r2 = CryptUtils.encodeBase64(r2);
	r3 = CryptUtils.encodeBase64(r3);

	try {
	    log.info("Method: checkPreguntasSecretasUsuario");

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    String query = "SELECT PREUSER.SID IDPREUSER, PREUSER.SID_REF_PREG AS IDPREGUNTA, PREGUNTA.PREGUNTA, PREUSER.ESTADO " +
	    		"FROM SIG.TBL_PREGUNTAS_USUARIO PREUSER " +
	    		"INNER JOIN SIG.TBL_PREGUNTAS PREGUNTA ON PREGUNTA.SID = PREUSER.SID_REF_PREG " +
	    		"WHERE PREUSER.SID_REF_USUARIO = " + sidusuario + " AND PREUSER.ESTADO = 1 " +
	    		"AND (PREUSER.RESPUESTA_PREGUNTA = '" + r1 + "' OR PREUSER.RESPUESTA_PREGUNTA = '" + r2 + "' OR PREUSER.RESPUESTA_PREGUNTA = '" + r3 + "')";

	    //log.info("Ejecutando SQL : " + query);

	    resultados = bdOracle.consultar(query);

	    bdOracle.setResultado(resultados);

	    String listaPreguntas="esttrx:0~";
	    while (resultados.next()){
		listaPreguntas = listaPreguntas + resultados.getString("IDPREUSER") + "~";
		log.info("Rescatando registro : " + resultados.getString("IDPREUSER"));
	    }

	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error No se pudo cerrar resulset.");
		return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
	    }

	    if (bdOracle.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }

	    return listaPreguntas;
	}catch (SQLException e){
	    bdOracle.setResultado(resultados);
	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error No se pudo cerrar resulset.");
		return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
	    }

	    if (bdOracle.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
		return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	    }

	    log.error(e.getMessage(), e);
	    return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
	}finally{
	    if (bdOracle.cerrarResultSet()==false){
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}			
	    }else{
		if (bdOracle.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}	
	    }
	}
    }
}
