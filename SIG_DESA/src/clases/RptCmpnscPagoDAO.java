package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import Beans.PagoContableBean;
import properties.PropertiesConfiguration;
import cl.util.Utils;
import db.OracleBD;

public class RptCmpnscPagoDAO {
	 

	static final Category log =  Logger.getLogger(RptCmpnscPagoDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private String esquema;
	
	public RptCmpnscPagoDAO(){
		try {
			Properties propiedades = new Properties();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
		    log.error("Error al leer el archivo de propiedades.");
		    log.error( e.getMessage(),e );
		}	
    }
	
	public String listarCmpnsPago(String fecDesde, String fecHasta, int operador){
		int posicionCursor = 0;
		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		ResultSet resultados = null;

		String listaCPago = "";
		String fecPDSDesde = fecDesde.split("/")[2].substring( 2,4 ) + "/" + fecDesde.split("/")[1] + "/" + fecDesde.split("/")[0];
		String fecPDSHasta = fecHasta.split("/")[2].substring( 2,4 ) + "/" + fecHasta.split("/")[1] + "/" + fecHasta.split("/")[0];
		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);

		try {
		    log.info( " Se ejecuta el procedimiento : SP_SIG_OBTENER_REPORTE_CP(?, ?, ?, ?, ?, ?, ?, ?)" );
		    log.info("Parametros : desde = "+fecDesde + " -  hasta =" +fecHasta + " - fecPDSDesde = " + fecPDSDesde + " -  fecPDSHasta = "+ fecPDSHasta   );
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_OBTENER_REPORTE_CP(?, ?, ?, ?, ?, ?, ?, ?); END;");

			stmtProcedure.setString("desde", fecDesde);
			stmtProcedure.setString("hasta", fecHasta);
			stmtProcedure.setString("desdePDS", fecPDSDesde);
			stmtProcedure.setString("hastaPDS", fecPDSHasta);
			stmtProcedure.setInt("filtro_operador", operador);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError = stmtProcedure.getInt("cod_error");
			String warning  = stmtProcedure.getString("warning");
			
			log.info( "codigoError : " + codigoError );
			log.info( "warning : " + warning);
			
			if (codigoError==0){
				
				log.info("Method: listarCmpnsPago - Recorriendo resultados");
				long cantTrx = 0;
				long montoTrx = 0;
				
				while (resultados.next()) {
					// Si es el primer valor que retorna el cursor
					if (posicionCursor == 0) {
						listaCPago += resultados.getString("CANT") + "|";
						listaCPago += resultados.getString("MONT") + ";";

					}else if(posicionCursor == 1){
						listaCPago += resultados.getString("CANT") + "|";
						listaCPago += resultados.getString("MONT") + ";";
					
					} else if(posicionCursor > 1) {
						cantTrx = cantTrx + resultados.getLong("CANT");
						montoTrx = montoTrx + resultados.getLong("MONT");
					}

					log.info("posicion cursor : " + posicionCursor + "- valor  = " + listaCPago);
					posicionCursor++;
				}
				listaCPago += cantTrx + "|";
				listaCPago += montoTrx + ";";
					
			}else{
				log.error("Method: buscarReporteVisa - Problemas al leer reporte visa desde la base de datos.");
				listaCPago="esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
			}
			listaCPago += "-" + fecDesde + "-" + fecHasta ;
			
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCPago = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
				 return listaCPago;
			}
		} catch (SQLException e) {
			log.error( e.getMessage(),e );
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			if (bdOracle.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaCPago = "esttrx|1~MSG|Problemas al leer reporte vissa desde la base de datos...~";
				return listaCPago;
			}
			
			listaCPago = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
		} finally {
		    
		    if( resultados != null){
		        try {
                    resultados.close();
                }
                catch ( SQLException e ) {
                   log.error( e.getMessage(), e );
                }
		    }
		    
			if (bdOracle.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			} else {
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			}
			try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
		}

		return listaCPago;
	}
	
	public String listarCmpnsPagoVisa(String fecDesde, String fecHasta, int operador){
		int posicionCursor = 0;
		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		ResultSet resultados = null;

		String listaCPago = "";
		String fecPDSDesde = fecDesde.split("/")[2].substring( 2,4 ) + "/" + fecDesde.split("/")[1] + "/" + fecDesde.split("/")[0];
		String fecPDSHasta = fecHasta.split("/")[2].substring( 2,4 ) + "/" + fecHasta.split("/")[1] + "/" + fecHasta.split("/")[0];
		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);

		try {
		    log.info( " Se ejecuta el procedimiento : SP_SIG_OBTENER_REPORTE_CP(?, ?, ?, ?, ?, ?, ?, ?)" );
		    log.info("Parametros : desde = "+fecDesde + " -  hasta =" +fecHasta + " - fecPDSDesde = " + fecPDSDesde + " -  fecPDSHasta = "+ fecPDSHasta   );
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_OBTENER_REPORTE_CP_VISA(?, ?, ?, ?, ?, ?, ?, ?); END;");

			stmtProcedure.setString("desde", fecDesde);
			stmtProcedure.setString("hasta", fecHasta);
			stmtProcedure.setString("desdePDS", fecPDSDesde);
			stmtProcedure.setString("hastaPDS", fecPDSHasta);
			stmtProcedure.setInt("filtro_operador", operador);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError = stmtProcedure.getInt("cod_error");
			String warning  = stmtProcedure.getString("warning");
			
			log.info( "codigoError : " + codigoError );
			log.info( "warning : " + warning);
			
			if (codigoError==0){
				
				log.info("Method: listarCmpnsPago - Recorriendo resultados");
				long cantTrx = 0;
				long montoTrx = 0;
				
				while (resultados.next()) {
					// Si es el primer valor que retorna el cursor
					if (posicionCursor == 0) {
						listaCPago += resultados.getString("CANT") + "|";
						listaCPago += resultados.getString("MONT") + ";";
						log.info(resultados.getLong("MONT"));

					}else if(posicionCursor == 1){
						listaCPago += resultados.getString("CANT") + "|";
						listaCPago += resultados.getString("MONT") + ";";
						log.info(resultados.getLong("MONT"));
					
					} else if(posicionCursor > 1) {
						cantTrx = cantTrx + resultados.getLong("CANT");
						montoTrx = montoTrx + resultados.getLong("MONT");
					}

					log.info("posicion cursor : " + posicionCursor + "- valor  = " + listaCPago);
					posicionCursor++;
				}
				listaCPago += cantTrx + "|";
				listaCPago += montoTrx + ";";
					
			}else{
				log.error("Method: buscarReporteVisa - Problemas al leer reporte visa desde la base de datos.");
				listaCPago="esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
			}
			listaCPago += "-" + fecDesde + "-" + fecHasta ;
			
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCPago = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
				 return listaCPago;
			}
		} catch (SQLException e) {
			log.error( e.getMessage(),e );
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			if (bdOracle.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaCPago = "esttrx|1~MSG|Problemas al leer reporte vissa desde la base de datos...~";
				return listaCPago;
			}
			
			listaCPago = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
		} finally {
		    
		    if( resultados != null){
		        try {
                resultados.close();
            }
            catch ( SQLException e ) {
               log.error( e.getMessage(), e );
            }
		    }
		    
			if (bdOracle.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			} else {
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			}
			try {
            if(conexion != null && !conexion.isClosed()){
                conexion.close();
            }
        }
        catch ( SQLException e ) {
            log.error( e.getMessage(),e );
        }
		}

		return listaCPago;
	}
	
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public  HashMap<String,String>  obtenerSumas(String fecha, int operador) throws SQLException{
	    
		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		ResultSet resultados = null;
		HashMap<String,String> sumatorias =  null;

		String fecPDSDesde = fecha.split("/")[2].substring( 2,4 ) + "/" + fecha.split("/")[1] + "/" + fecha.split("/")[0];
		String fecPDSHasta = fecha.split("/")[2].substring( 2,4 ) + "/" + fecha.split("/")[1] + "/" + fecha.split("/")[0];
		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);

		try {
		    log.info( " Se ejecuta el procedimiento : SP_SIG_OBTENER_REPORTE_CP(?, ?, ?, ?, ?, ?, ?, ?)" );
		    log.info("fechaPDSDesde :  "+ fecPDSDesde);
		    log.info("fecPDSHasta :  "+ fecPDSHasta);
		    log.info("fecha :  "+ fecha);
		    log.info("operador :  "+ operador);
		    
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_OBTENER_REPORTE_CP(?, ?, ?, ?, ?, ?, ?, ?); END;");
		    
			stmtProcedure.setString("desde", fecha);
			stmtProcedure.setString("hasta", fecha);
			stmtProcedure.setString("desdePDS", fecPDSDesde);
			stmtProcedure.setString("hastaPDS", fecPDSHasta);
			stmtProcedure.setInt("filtro_operador", operador);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError = stmtProcedure.getInt("cod_error");
			String warning  = stmtProcedure.getString("warning");
			
			
			log.info( "codigoError : " + codigoError );
			log.info( "warning : " + warning);
			
			if (codigoError==0){
				
				log.info("Method: listarCmpnsPago - Recorriendo resultados");
				sumatorias = new HashMap<String, String>();
				int posicionCursor = 0;
				int suma = 0 ;
				while (resultados.next()) {
					if (posicionCursor == 0){
						sumatorias.put(Utils.SUMATORIA_CPAGO_MAP_NAME, resultados.getString("MONT"));
					}else if( posicionCursor == 1){
						sumatorias.put(Utils.SUMATORIA_DESCUENTO_MAP_NAME, resultados.getString("MONT"));
					
					}else if(posicionCursor > 1){
						suma += resultados.getInt("MONT");
					}
					posicionCursor++;
				}
				sumatorias.put(Utils.SUMATORIA_INCOMING_MAP_NAME,String.valueOf(suma));
			}else{
				log.error("No se ha podido consultar las sumatorias del incoming o de cpago sugeridas");
				sumatorias = null;
			}
			
		} catch (SQLException e) {
			log.error(e.getMessage(), e );
			throw e;
		} finally {
		    
		    if( resultados != null){
		        try {
                    resultados.close();
                }
                catch ( SQLException e ) {
                   log.error( e.getMessage(), e );
                }
		    }
		    
			if (bdOracle.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			} else {
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			}
			try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
		}

		return sumatorias;
		
	}
	
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public  HashMap<String,String>  obtenerSumasVisa(String fecha, int operador) throws SQLException{
	    
		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		ResultSet resultados = null;
		HashMap<String,String> sumatorias =  null;

		String fecPDSDesde = fecha.split("/")[2].substring( 2,4 ) + "/" + fecha.split("/")[1] + "/" + fecha.split("/")[0];
		String fecPDSHasta = fecha.split("/")[2].substring( 2,4 ) + "/" + fecha.split("/")[1] + "/" + fecha.split("/")[0];
		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);

		try {
		    log.info( " Se ejecuta el procedimiento : SP_SIG_OBTENER_REPORTE_CP(?, ?, ?, ?, ?, ?, ?, ?)" );
		    log.info("fechaPDSDesde :  "+ fecPDSDesde);
		    log.info("fecPDSHasta :  "+ fecPDSHasta);
		    log.info("fecha :  "+ fecha);
		    log.info("operador :  "+ operador);
		    
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_OBTENER_REPORTE_CP_VISA(?, ?, ?, ?, ?, ?, ?, ?); END;");
		    
			stmtProcedure.setString("desde", fecha);
			stmtProcedure.setString("hasta", fecha);
			stmtProcedure.setString("desdePDS", fecPDSDesde);
			stmtProcedure.setString("hastaPDS", fecPDSHasta);
			stmtProcedure.setInt("filtro_operador", operador);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError = stmtProcedure.getInt("cod_error");
			String warning  = stmtProcedure.getString("warning");
			
			
			log.info( "codigoError : " + codigoError );
			log.info( "warning : " + warning);
			
			if (codigoError==0){
				
				log.info("Method: listarCmpnsPago - Recorriendo resultados");
				sumatorias = new HashMap<String, String>();
				int posicionCursor = 0;
				int suma = 0 ;
				
				while (resultados.next()) {
					if (posicionCursor == 0){
						sumatorias.put(Utils.SUMATORIA_CPAGO_MAP_NAME, resultados.getString("MONT"));
						log.info(resultados.getInt("MONT"));
					}else if( posicionCursor == 1){
						sumatorias.put(Utils.SUMATORIA_DESCUENTO_MAP_NAME, resultados.getString("MONT"));
						log.info(resultados.getInt("MONT"));
					
					}else if(posicionCursor > 1){
						suma += resultados.getInt("MONT");
						log.info(resultados.getInt("MONT"));
					}
					posicionCursor++;
				}
				sumatorias.put(Utils.SUMATORIA_INCOMING_MAP_NAME,String.valueOf(suma));
			}else{
				log.error("No se ha podido consultar las sumatorias del incoming o de cpago sugeridas");
				sumatorias = null;
			}
			
		} catch (SQLException e) {
			log.error(e.getMessage(), e );
			throw e;
		} finally {
		    
		    if( resultados != null){
		        try {
                    resultados.close();
                }
                catch ( SQLException e ) {
                   log.error( e.getMessage(), e );
                }
		    }
		    
			if (bdOracle.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			} else {
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			}
			try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
		}

		return sumatorias;
		
	}
	
}
