package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import db.OracleBD;
import exception.AppException;
import Beans.ContableAvancesBean;
import Beans.HistoriaContableAvancesBean;
import properties.PropertiesConfiguration;

public class ConsultarContableAvancesDAO {

    static final Logger log = Logger
	    .getLogger(ConsultarContableAvancesDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    private String esquema;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2019, (ACL ) - versi�n inicial
     * </ul>
     * <p>
     * Constructor de clase
     * 
     * 
     * @since 1.X
     */
    public ConsultarContableAvancesDAO() {
	try {
	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    esquema = propiedades.getProperty("esquema");
	} catch (Exception e) {
	    log.error("Error al leer el archivo de propiedades.");
	    log.error(e.getMessage(), e);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param fechaaInicio
     *            fechaFin estado
     * @return
     * @throws SQLException
     * @since 1.X
     */
    public HashMap<String, List<ContableAvancesBean>> buscarRegistroLogContableAvances(
	    String fechaDesde, String fechaHasta, Integer estado)
	    throws SQLException {

	HashMap<String, List<ContableAvancesBean>> hash = new HashMap<String, List<ContableAvancesBean>>();
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs = null;
	List<ContableAvancesBean> listaContableAvances = null;
	StringBuilder sql = new StringBuilder();

	sql.append("SELECT TC.SID AS ID, "
		+ " NVL(TO_CHAR(TC.FILE_TS, 'dd/mm/yyyy hh24:mi:ss'),' ') AS FILE_TS, "
		+ " TC.FILE_NAME AS FILE_NAME, "
		+ " NVL(TC.FILE_FLAG,'') AS ESTADO, "
		+ " NVL(TO_CHAR(TC.FECHA, 'dd/mm/yyyy'),' ') AS FECHA "
		+ " FROM TBL_LOG_CONTABLE TC "
		+ " WHERE TC.FILE_NAME LIKE '%AD_%' "
		+ " AND TO_CHAR(TC.FECHA, 'YYYYMMDD')  >= '" + fechaDesde
		+ "' AND TO_CHAR(TC.FECHA, 'YYYYMMDD')  <= '" + fechaHasta
		+ "' ");

	if (estado != 99) {
	    sql.append(" AND TC.FILE_FLAG = '" + estado + "' ");
	}

	sql.append("ORDER BY TC.SID DESC ");

	try {
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: buscarRegistroLogContableAvances - Obteniendo parametros: fechaDesde-> "
		    + fechaDesde);
	    log.info("Method: buscarRegistroLogContableAvances - Obteniendo parametros: fechaHasta-> "
		    + fechaHasta);
	    log.info("Method: buscarRegistroLogContableAvances - Obteniendo parametros: estado-> "
		    + estado);

	    rs = bdOracle.consultar(sql.toString());
	    bdOracle.setResultado(rs);

	    log.info("Method: buscarRegistroLogContableAvances - Obteniendo resulstados");
	    listaContableAvances = parserRegistrosContablesList(rs);

	    hash.put("listContableAvances", listaContableAvances);
	    rs.close();

	} finally {

	    bdOracle.setResultado(rs);
	    bdOracle.cerrarResultSet();
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return hash;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2019, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private List<ContableAvancesBean> parserRegistrosContablesList(ResultSet rs) {
	List<ContableAvancesBean> list = new ArrayList<ContableAvancesBean>();
	try {

	    while (rs.next()) {
		ContableAvancesBean tr = new ContableAvancesBean();
		tr.setId(rs.getString("ID"));
		tr.setFechaTs(rs.getString("FILE_TS"));
		tr.setFileName(rs.getString("FILE_NAME"));
		tr.setEstado(rs.getString("ESTADO"));
		tr.setFecha(rs.getString("FECHA"));
		list.add(tr);
	    }
	} catch (SQLException e) {
	    log.error(
		    "ConsultarContableAvancesDAO-parserRegistrosContablesList : Ha ocurrido un error al parsear los registros contabless.",
		    e);
	}

	return list;

    }

    /*
     * 
     * <p> Registro de versiones: <ul> <li>1.0 XX/YY/2019, (ACL Chile) - versi�n
     * inicial </ul> <p>
     * 
     * historia de archivo contable de avances
     * 
     * @param id
     * 
     * @return
     * 
     * @since 1.X
     */
    public List<HistoriaContableAvancesBean> buscarHistoriaContable(
	    Integer sid) {
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs = null;
	List<HistoriaContableAvancesBean> listaFinalHContable = null;
	StringBuilder sql = new StringBuilder();
	

	sql.append(" SELECT HCON.SID AS ID, "
		+ " NVL(TO_CHAR(HCON.FECHA_REGISTRO, 'dd/mm/yyyy hh24:mi:ss'),' ') AS FECHA, "
		+ " HCON.SID_CONTABLE AS SID_CNTBL, "
		+ " (UIG.NOMBRE||' '||UIG.APEPAT) AS USERS, "
		+ " NVL(HCON.MOTIVO,' ') AS MOTIVO, "
		+ " (CASE WHEN HCON.ESTADO = 1 THEN 'Nuevo' WHEN HCON.ESTADO = 0 THEN 'Antiguo' END) AS ESTADO "
		+ " FROM TBL_HISTORIA_CONTABLE HCON "
		+ " LEFT JOIN TBL_USUARIO_IG UIG ON UIG.SID = HCON.SID_USER "
		+ " WHERE HCON.SID_CONTABLE = "+sid
		+ " ORDER BY HCON.SID  DESC ");
	
	try {
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: buscarHistoriaContable - Obteniendo parametros: sid-> "
		    + sid);
	    
	    rs = bdOracle.consultar(sql.toString());
	    bdOracle.setResultado(rs);

	    log.info("Method: buscarHistoriaContable - Obteniendo resulstados");
	    listaFinalHContable = parserRegistrosHContablesList(rs);

	    rs.close();

	} catch (SQLException e) {

	    log.error(e.getMessage(), e);
	} finally {

	    bdOracle.setResultado(rs);
	    bdOracle.cerrarResultSet();
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}


	return listaFinalHContable;

    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2019, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private List<HistoriaContableAvancesBean> parserRegistrosHContablesList(ResultSet rs) {
	List<HistoriaContableAvancesBean> list = new ArrayList<HistoriaContableAvancesBean>();
	try {

	    while (rs.next()) {
		HistoriaContableAvancesBean tr = new HistoriaContableAvancesBean();
		tr.setId(rs.getString("ID"));
		tr.setFechaRegistro(rs.getString("FECHA"));
		tr.setSidContable(rs.getString("SID_CNTBL"));
		tr.setUsuario(rs.getString("USERS"));
		tr.setMotivo(rs.getString("MOTIVO"));
		tr.setEstado(rs.getString("ESTADO"));
		list.add(tr);
	    }
	} catch (SQLException e) {
	    log.error(
		    "ConsultarContableAvancesDAO-parserRegistrosHContablesList : Ha ocurrido un error al parsear los registros contabless.",
		    e);
	}

	return list;

    }
    
    

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/11/2018, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @throws AppException
     * @since 1.X
     */

    public String insertRegistroRegenerar(Integer sid,
	    Integer idUser, String motivo) {
	log.info("Method: insertRegistroRegenerar - Obteniendo parametros: sidContable-> "
		+ sid + " | idUser-> " + idUser + " | motivo-> " + motivo);

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet rs1 = null;

	String dev = "";

	try {

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: insertRegistroRegenerar - Ejecutando SP_SIG_INSERT_HIS_CONTABLE");
	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_INSERT_HIS_CONTABLE(?, ?, ?, ?, ?, ?); END;");

	    stmtProcedure.registerOutParameter("FLAG", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("ERROR_OUT", OracleTypes.VARCHAR);
	    
	    stmtProcedure.setInt("VSID_CONTABLE", sid);
	    stmtProcedure.setInt("VSID_USER", idUser);
	    stmtProcedure.setString("VMOTIVO", motivo);
	    stmtProcedure.setString("VTIPO", "WEB");

	    stmtProcedure.execute();

	    String error = stmtProcedure.getString("ERROR_OUT");

	    if (error.equals("0")) {
		log.info("Method: insertRegistroRegenerar - Salida Flag: "
			+ stmtProcedure.getString("FLAG"));
		dev = stmtProcedure.getString("FLAG");
	    } else if(error.equals("1")){
		log.info("Method: insertRegistroRegenerar - Salida Flag: "
			+ stmtProcedure.getString("FLAG"));
		dev = stmtProcedure.getString("FLAG");
	    
	    }else{
		log.error("Method: insertRegistroRegenerar - "
			+ stmtProcedure.getString("ERROR_OUT"));

		String msgError = stmtProcedure.getString("ERROR_OUT");

		dev = "esttrx:1|" + msgError;

	    }
	    stmtProcedure.close();
	} catch (SQLException e) {
	    bdOracle.cerrarCallableStatement(stmtProcedure);
	    dev = "esttrx:1" + e.getMessage();
	    log.error(e.getMessage(), e);
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}
	return dev;
    }

}
