package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

public class ReportesVISADAO {

	static final Logger log =  Logger.getLogger(ReportesVISADAO.class);
	
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

	private String esquema;

	public ReportesVISADAO() {
		try {
			Properties propiedades = new Properties();
//			propiedades.load(getClass().getResourceAsStream(
//					"/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
			esquema = propiedades.getProperty("esquema");
		} catch (Exception e) {
			log.error("Error al leer el archivo de propiedades.");
			log.error( e.getMessage(),e );
		}
	}

	public String buscarReporteVisa(Integer trimestre, Integer ano) {

		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		ResultSet resultados = null;

		String listaReporteVisa = "";
		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);

		try {
		    log.info( "SP_SIG_OBTENER_REPORTES_VISA(trimestre,a�o, ?, ?, ?)" );
		    log.info( "Trimestre : "+trimestre+" | A�o" + ano );
		    
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_OBTENER_REPORTES_VISA(?, ?, ?, ?, ?); END;");

			stmtProcedure.setInt("trimestre", trimestre);
			stmtProcedure.setInt("ano", ano);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError = stmtProcedure.getInt("cod_error");
			String warning =  stmtProcedure.getString("warning");
			
			if (codigoError==0){
				
				log.info("Method: buscarReporteVisa - Recorriendo resultados");
				while (resultados.next()) {
					listaReporteVisa += resultados.getString("FEC") + "|";

					listaReporteVisa += resultados.getString("CPRMNTONUS") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDONUS") + "|";
					listaReporteVisa += resultados.getString("CPRMNTONUS_CHIP") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDONUS_CHIP") + "|";
					listaReporteVisa += resultados.getString("CPRMNTONUS_ELEC") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDONUS_ELEC") + "|";
					listaReporteVisa += resultados.getString("CPRMNTONUS_AVNC") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDONUS_AVNC") + "|";
					listaReporteVisa += resultados.getString("CPRMNTONUS_AVNC_CJR") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDONUS_AVNC_CJR") + "|";

					listaReporteVisa += resultados.getString("CPRMNTONAC") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDNAC") + "|";
					listaReporteVisa += resultados.getString("CPRMNTONAC_CHIP") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDNAC_CHIP") + "|";
					listaReporteVisa += resultados.getString("CPRMNTONAC_ELEC") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDNAC_ELEC") + "|";
					listaReporteVisa += resultados.getString("CPRMNTONAC_AVNC") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDNAC_AVNC") + "|";
					listaReporteVisa += resultados.getString("CPRMNTONAC_AVNC_CJR") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDNAC_AVNC_CJR") + "|";

					listaReporteVisa += resultados.getString("CPRMNTINTER") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDINTER") + "|";
					listaReporteVisa += resultados.getString("CPRMNTINTER_CHIP") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDINTER_CHIP") + "|";
					listaReporteVisa += resultados.getString("CPRMNTINTER_ELEC") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDINTER_ELEC") + "|";
					listaReporteVisa += resultados.getString("CPRMNTINTER_AVNC") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDINTER_AVNC") + "|";
					listaReporteVisa += resultados.getString("CPRMNTINTER_AVNC_CJR") + "|";
					listaReporteVisa += resultados.getInt("CPRCNTDINTER_AVNC_CJR") + "|";

					listaReporteVisa += resultados.getString("DBTTBKMNT_NA") + "|";
					listaReporteVisa += resultados.getInt("DBTTBKCNTD_NA") + "|";
					listaReporteVisa += resultados.getString("DBTTBKMNT_IN") + "|";
					listaReporteVisa += resultados.getInt("DBTTBKCNTD_IN") + "|";
					
					listaReporteVisa += resultados.getString("DBTMNT_NA") + "|";
					listaReporteVisa += resultados.getInt("DBTCNTD_NA") + "|";
					listaReporteVisa += resultados.getString("DBTMNT_IN") + "|";
					listaReporteVisa += resultados.getInt("DBTCNTD_IN") + "|";

					listaReporteVisa += resultados.getString("CRDTTBKMNT_NA") + "|";
					listaReporteVisa += resultados.getInt("CRDTTBKCNTD_NA") + "|";
					listaReporteVisa += resultados.getString("CRDTTBKMNT_IN") + "|";
					listaReporteVisa += resultados.getInt("CRDTTBKCNTD_IN") + "|";
					
					listaReporteVisa += resultados.getString("CRDTMNT_NA") + "|";
					listaReporteVisa += resultados.getInt("CRDTCNTD_NA") + "|";
					listaReporteVisa += resultados.getString("CRDTMNT_IN") + "|";
					listaReporteVisa += resultados.getInt("CRDTCNTD_IN") + "|";
					
					listaReporteVisa += resultados.getString("PAGOMNT_NA") + "|";
					listaReporteVisa += resultados.getInt("PAGOCNTD_NA") + "|";
					listaReporteVisa += resultados.getString("PAGOMNT_IN") + "|";
					listaReporteVisa += resultados.getInt("PAGOCNTD_IN") + "|" + ";";

				}
					
			}else{
				log.error("Method: buscarReporteVisa - Problemas al leer reporte visa desde la base de datos.warning: "+ warning  +"- codigo error : " +codigoError  );
				listaReporteVisa="esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
			}
			
			
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaReporteVisa = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
				 return listaReporteVisa;
			}
			
			log.info("Method: buscarReporteVisa - " + listaReporteVisa);
		} catch (SQLException e) {

			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			if (bdOracle.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaReporteVisa = "esttrx|1~MSG|Problemas al leer reporte vissa desde la base de datos...~";
				return listaReporteVisa;
			}
			log.error( e.getMessage(),e );
			listaReporteVisa = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
		} finally {
		    
		    if( resultados != null){
		        try {
                    resultados.close();
                }
                catch ( SQLException e ) {
                    log.error(e.getMessage(), e);
                }
		    }
		    
		    
			if (bdOracle.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			} else {
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			}
			try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
		}

		return listaReporteVisa;
	}
}
