package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import actions.TransaccionesAutor;
import db.OracleBD;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class ConsultarTrxAutorDAO {
    
    static final Logger log = Logger.getLogger( ConsultarTrxAutorDAO.class );
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    
    private String esquema;
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL ) - versi�n inicial
     * </ul>
     * <p>
     * Constructor de clase
     * 
     * 
     * @since 1.X
     */
    public ConsultarTrxAutorDAO() {
        try {
            Properties propiedades = new Properties();
            propiedades = PropertiesConfiguration.getProperty();
            esquema = propiedades.getProperty( "esquema" );
        }
        catch ( Exception e ) {
            log.error( "Error al leer el archivo de propiedades." );
            log.error( e.getMessage(), e );
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param fechaProceso
     * @return
     * @since 1.X
     */
    public HashMap<String, List<TransaccionesAutor>> buscarInformeTrxAutor(
            String fechaDesde, String fechaHasta ) {
        HashMap<String, List<TransaccionesAutor>> hash = new HashMap<String, List<TransaccionesAutor>>();
        Connection conexion = null;
        db.OracleBD bdOracle = null;
        CallableStatement stmtProcedure = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        List<TransaccionesAutor> listaTrxLogAutor = null;
        List<TransaccionesAutor> listaTrxIncoming = null;
        try {
            bdOracle = new OracleBD();
            conexion = bdOracle.conectar();
            bdOracle.setConexion( conexion );
            
            log.info( "Method: buscarInformeTrxAutor - Obteniendo parametros: fechaDesde-> "
                    + fechaDesde );
            log.info( "Method: buscarInformeTrxAutor - Obteniendo parametros: fechaHasta-> "
                    + fechaHasta );
            log.info( "Method: buscarInformeTrxAutor - Ejecutando Sp_sig_obtener_info_trx" );
            
            stmtProcedure = conexion.prepareCall( "BEGIN " + esquema
                    + ".Sp_sig_obtener_info_trx(?, ?, ?, ?, ?, ?); END;" );
            
            stmtProcedure.setString( "fecha_inicial", fechaDesde );
            stmtProcedure.setString( "fecha_final", fechaHasta );
            stmtProcedure.registerOutParameter( "warning", OracleTypes.VARCHAR );
            stmtProcedure
                    .registerOutParameter( "cod_error", OracleTypes.NUMBER );
            stmtProcedure
                    .registerOutParameter( "prfCursor", OracleTypes.CURSOR );
            stmtProcedure.registerOutParameter( "prfCursor2",
                    OracleTypes.CURSOR );
            
            stmtProcedure.execute();
            
            int codigoError = stmtProcedure.getInt( "cod_error" );
            String warning = stmtProcedure.getString( "warning" );
            
            log.info( "==== EJECUCION DE Sp_sig_obtener_info_trx ===" );
            log.info( "Codigo error: " + codigoError );
            log.info( "Mensaje: " + warning );
            log.info( "=========================================" );
            
            if ( codigoError == 0 ) {
                
                rs1 = ( ResultSet ) ( ( stmtProcedure ).getObject( "prfCursor" ) );
                rs2 = ( ResultSet ) ( ( stmtProcedure )
                        .getObject( "prfCursor2" ) );
                
                log.info( "Method: buscarInformeTrxAutor - Recorriendo resultados" );
                
                listaTrxLogAutor = parserTrxLogAutorList( rs1 );
                listaTrxIncoming = parserTrxIncomingList( rs2 );
                
                //log.info("listaTrxLogAutor :" +listaTrxLogAutor);
                //log.info("listaTrxIncoming :" +listaTrxIncoming);
                // se agregan al hash
                hash.put( "listaTrxLogAutor", listaTrxLogAutor );
                hash.put( "listaTrxIncoming", listaTrxIncoming );
                
            }
            else {
                log.error( "Method: buscarInformeTrxAutor - Problemas al leer las trx.: No se ha leido ningun registro" );
            }
            
        }
        catch ( SQLException e ) {
            
            log.error( e.getMessage(), e );
        }
        finally {
            
            bdOracle.setResultado( rs1 );
            bdOracle.cerrarResultSet();
            bdOracle.setResultado( rs2 );
            bdOracle.cerrarResultSet();
            bdOracle.cerrarStatement( stmtProcedure );
            
            if ( bdOracle.cerrarResultSet() == false ) {
                log.error( "Error: No se pudo cerrar resulset." );
                if ( bdOracle.cerrarConexionBD() == false ) {
                    log.error( "Error: No se pudo cerrar conexion a la base de datos." );
                }
            }
            else {
                if ( bdOracle.cerrarConexionBD() == false ) {
                    log.error( "Error: No se pudo cerrar conexion a la base de datos." );
                }
            }
            try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
        }
        //log.info(hash);
        return hash;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @throws SQLException
     * 
     * @since 1.X
     */
    private List<TransaccionesAutor> parserTrxLogAutorList( ResultSet rs1 )
            throws SQLException {
        List<TransaccionesAutor> list = new ArrayList<TransaccionesAutor>();
        
        while ( rs1.next() ) {
            TransaccionesAutor tr = new TransaccionesAutor();
            tr.setSid(rs1.getString("sid"));
            tr.setCodigoTransaccion( rs1.getString( "codigo_transaccion" ) );
            tr.setNumeroTarjeta( rs1.getString( "numero_tarjeta" ) );
            tr.setFecha_proceso( rs1.getString( "fecha_proceso" ) );
            tr.setTipoVenta( rs1.getString( "tipo_venta" ) );
            tr.setNumCuotas( rs1.getString( "num_cuotas" ) );
            tr.setNumMicrofilm( rs1.getString( "num_microfilm" ) );
            tr.setNumComercio( rs1.getString( "num_comercio" ) );
            tr.setFechaCompra( rs1.getString( "fecha_compra" ) );
            tr.setMontoTransac( rs1.getString( "monto_transac" ) );
            tr.setValorCuota( rs1.getString( "valor_cuota" ) );
            tr.setNombreComercio( rs1.getString( "nombre_comercio" ) );
            tr.setCiudadComercio( rs1.getString( "ciudad_comercio" ) );
            tr.setRubroComercio( rs1.getString( "rubro_comercio" ) );
            tr.setCodAutorizacion( rs1.getString( "cod_autorizacion" ) );
            tr.setCodLocal( rs1.getString( "cod_local" ) );
            tr.setCodPos( rs1.getString( "cod_pos" ) );
            tr.setFecha_proceso( rs1.getString( "fecha_hora_trx" ) );
            tr.setMontoFormateado(rs1.getString("monto_transac"));
            list.add( tr );
        }
        return list;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private List<TransaccionesAutor> parserTrxIncomingList( ResultSet rs2 ) {
        List<TransaccionesAutor> list = new ArrayList<TransaccionesAutor>();
        try {
            while ( rs2.next() ) {
                
                TransaccionesAutor tr = new TransaccionesAutor();
                tr.setSid( rs2.getString( "sid" ) );
                tr.setCodigoTransaccion( rs2.getString( "codigo_transaccion" ) );
                tr.setNumeroTarjeta( rs2.getString( "numero_tarjeta" ) );
                tr.setTipoVenta( rs2.getString( "tipo_venta" ) );
                tr.setNumCuotas( rs2.getString( "num_cuotas" ) );
                tr.setNumMicrofilm( rs2.getString( "num_microfilm" ) );
                tr.setNombreComercio( rs2.getString( "nombre_comercio" ) );
                tr.setNumComercio( rs2.getString( "num_comercio" ) );
                tr.setFechaCompra( rs2.getString( "fecha_compra" ) );
                log.info(rs2.getString( "monto_transac" ));
                tr.setMontoTransac( rs2.getString( "monto_transac" ) );
                tr.setValorCuota( rs2.getString( "valor_cuota" ) );
                tr.setCiudadComercio( rs2.getString( "ciudad_comercio" ) );
                tr.setRubroComercio( rs2.getString( "rubro_comercio" ) );
                tr.setCodAutorizacion( rs2.getString( "cod_autorizacion" ) );
                tr.setTipoAccion( rs2.getString("tipo_accion"));
                tr.setMontoFormateado(rs2.getString("monto_transac"));
                list.add( tr );
            }
        }
        catch ( SQLException e ) {
            log.error(
                    "ConsultarTrxAutorDAO-parserTrxIncomingList : Ha ocurrido un error al parsear la trx de incoming.",
                    e );
        }
        
        return list;
        
    }
    
}
