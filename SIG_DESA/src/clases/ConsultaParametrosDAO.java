package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.PagoContableBean;
import db.OracleBD;

/**
 * 
 * @author afreire
 * 
 */
public class ConsultaParametrosDAO {
    // CONSTANTES DEL SISTEMA
    static final Category log = Logger.getLogger(ConsultaParametrosDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    private String esquema;

    /**
     * Constructor
     */
    public ConsultaParametrosDAO() {
	try {
	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    esquema = propiedades.getProperty("esquema");

	} catch (Exception e) {
	    log.error("Error al leer el archivo de propiedades.");
	    log.error(e.getMessage(), e);
	}
    }

    /**
     * 
     * Metodo de gestion sobre los pagos, se recibe un pago y se llama al
     * SP_SIG_GESTIONAR con un estado del pago, de acuerdo a este parametro el
     * SP sabe si se trata de un ingreso, un rechazo o una autorizacion del pago
     * y se realiza la logica correspondiente
     * 
     * @param pago
     * @return
     * @throws Exception
     */
    public void consultaParametro() throws Exception {
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet resultados = null;
	String sql = "SELECT * FROM SIG.TBL_ASIENTOS";

	try {
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    resultados = bdOracle.consultar(sql);
	    bdOracle.setResultado(resultados);

	    bdOracle.cerrarResultSet();
	}catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    cerrarConexiones(bdOracle, resultados, conexion, null);
	}
    }

    /**
     * Metodo encargado de cerrar las conexioness
     * 
     * @param oracleBD
     * @param rs
     * @param conexion
     */
    private void cerrarConexiones(OracleBD oracleBD, ResultSet rs,
	    Connection conexion, CallableStatement stmtProcedure) {
	if (rs != null) {
	    try {
		rs.close();
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	if (oracleBD.cerrarResultSet() == false) {
	    log.error("Error: No se pudo cerrar resulset.");
	    if (oracleBD.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	} else {
	    if (oracleBD.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	}

	try {
	    if (conexion != null && !conexion.isClosed()) {
		conexion.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

	try {
	    if (stmtProcedure != null && !stmtProcedure.isClosed()) {
		stmtProcedure.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}
    }
}