package clases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Category;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import properties.PropertiesConfiguration;
import Beans.CargaMasivaDTO;
import actions.TransaccionesAutor;
import au.com.bytecode.opencsv.CSVWriter;
import cl.util.Utils;
import exception.AppException;

public class ExportarDataExcelDAO {

    static final Category log = Logger.getLogger(ExportarDataExcelDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    private String directorioExportacion;

    private File archivoSalida;
    public String fileName;

    /**
     * Metodo que genera la planilla excel.
     * 
     * @return Archivo de exportacion .xls
     * @throws Mensaje
     *             de excepcion controlado.
     * @param dataExcel
     *            Datos a ser exprotacdos en planilla.
     * @param nombreArchivo
     *            Nombre del archivo generado.
     */
    
    public File generarPlanillaExcel(String dataExcel, String nombreArchivo) {

	try {

	    log.info("Method: generarPlanillaExcel - Obteniendo parametros: nombreArchivo-> "
		    + nombreArchivo);

	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    directorioExportacion = propiedades.getProperty("exportarexcel");

	} catch (Exception e) {

	    log.error(e.getMessage(), e);
	    return null;
	}

	try {

	    Date fechaActual = new Date();
	    DateFormat formateador = new SimpleDateFormat("yyyyMMddHHmmss");

	    String fechaArchivo = formateador.format(fechaActual);
	    File dir = new File(nombreArchivo);
	    //

	    // if (!dir.exists())
	    // if (!dir.mkdir())
	    // return null;

	    // *************************************************************************//
	    // Creacion de planilla excel en direcotrio definido en archivo
	    // parametros //
	    // *************************************************************************//

	    String generaArchivoExcel = directorioExportacion + nombreArchivo
		    + ".xls";
	    archivoSalida = new File(generaArchivoExcel);

	    File file = new File(generaArchivoExcel);
	    FileOutputStream fileOut = new FileOutputStream(file);
	    HSSFWorkbook workbook = new HSSFWorkbook();
	    HSSFSheet worksheet = workbook.createSheet("Reporte");

	    // *************************************************************************//
	    // Lectura de filas y columnas de la data recepcionada para
	    // exportacion //
	    // *************************************************************************//
	    String[] filas = dataExcel.split("~");
	    log.info(dataExcel);
	    log.info("Cantidad de filas a imprimir: " + filas.length);
	    
	    for (int i = 0; i < filas.length; i++) {
		
		HSSFRow row = worksheet.createRow((short) i);
		String[] columnas = filas[i].split("\\|");
		log.info(columnas);

		int valorCelda = columnas[0].compareToIgnoreCase("TIT");
		for (int x = 1; x < columnas.length; x++) {

		    worksheet.autoSizeColumn(x, true);
		    HSSFCell cell = row.createCell(x);
		    HSSFCellStyle cellStyle = workbook.createCellStyle();
		    HSSFFont font = workbook.createFont();
		    String celda = evaluarCampoTarjeta(columnas[x]);
		    cell.setCellValue(celda);

		    if (valorCelda == 0) {
			cellStyle
				.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
			cellStyle
				.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

			cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			cellStyle.setBorderBottom((short) 1);
			cellStyle.setBorderTop((short) 1);
			cellStyle.setBorderLeft((short) 1);
			cellStyle.setBorderRight((short) 1);

			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			cellStyle.setFont(font);
		    }

		    if (NumberUtils.isNumber(columnas[x])) {
			cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		    }
		    cell.setCellStyle(cellStyle);
		    worksheet.autoSizeColumn(x, true);
		}

	    }

	    workbook.write(fileOut);
	    fileOut.flush();
	    fileOut.close();

	    log.info("Fin de impresión");

	} catch (FileNotFoundException e) {
	    log.error(e.getMessage(), e);
	    return null;

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	    return null;
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    return null;
	}

	return archivoSalida;

    }

    /**
     * Metodo que genera la planilla excel de TC46.
     * 
     * @return Archivo de exportacion .xls
     * @throws Mensaje
     *             de excepcion controlado.
     * @param dataExcel
     *            Datos a ser exprotacdos en planilla.
     * @param nombreArchivo
     *            Nombre del archivo generado.
     */
    public File generarPlanillaExcelTc46(String dataExcel, String nombreArchivo) {

	try {

	    log.info("Method: generarPlanillaExcel - Obteniendo parametros: nombreArchivo-> "
		    + nombreArchivo);

	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    directorioExportacion = propiedades.getProperty("exportarexcel");

	} catch (Exception e) {

	    log.error(e.getMessage(), e);
	    return null;
	}

	try {

	    Date fechaActual = new Date();
	    DateFormat formateador = new SimpleDateFormat("yyyyMMddHHmmss");

	    String fechaArchivo = formateador.format(fechaActual);
	    File dir = new File(nombreArchivo);
	    //

	    // if (!dir.exists())
	    // if (!dir.mkdir())
	    // return null;

	    // *************************************************************************//
	    // Creacion de planilla excel en direcotrio definido en archivo
	    // parametros //
	    // *************************************************************************//

	    String generaArchivoExcel = directorioExportacion + nombreArchivo
		    + ".xls";
	    archivoSalida = new File(generaArchivoExcel);

	    File file = new File(generaArchivoExcel);
	    FileOutputStream fileOut = new FileOutputStream(file);
	    HSSFWorkbook workbook = new HSSFWorkbook();
	    HSSFSheet worksheet = workbook.createSheet("Reporte");

	    // *************************************************************************//
	    // Lectura de filas y columnas de la data recepcionada para
	    // exportacion //
	    // *************************************************************************//
	    String[] filas = dataExcel.split("~");
	    log.info(dataExcel);
	    log.info("Cantidad de filas a imprimir: " + filas.length);
	    log.info("align left");
	    for (int i = 0; i < filas.length; i++) {
		
		HSSFRow row = worksheet.createRow((short) i);
		String[] columnas = filas[i].split("\\|");
		log.info(columnas);

		int valorCelda = columnas[0].compareToIgnoreCase("TIT");
		for (int x = 1; x < columnas.length; x++) {

		    worksheet.autoSizeColumn(x, true);
		    HSSFCell cell = row.createCell(x);
		    HSSFCellStyle cellStyle = workbook.createCellStyle();
		    cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		    HSSFFont font = workbook.createFont();
		    String celda = evaluarCampoTarjeta(columnas[x]);
		    cell.setCellValue(celda);

		    if (valorCelda == 0) {
			cellStyle
				.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
			cellStyle
				.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

			cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			cellStyle.setBorderBottom((short) 1);
			cellStyle.setBorderTop((short) 1);
			cellStyle.setBorderLeft((short) 1);
			cellStyle.setBorderRight((short) 1);

			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			cellStyle.setFont(font);
		    }

		    if (NumberUtils.isNumber(columnas[x])) {
			cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		    }
		    cell.setCellStyle(cellStyle);
		    worksheet.autoSizeColumn(x, true);
		}

	    }

	    workbook.write(fileOut);
	    fileOut.flush();
	    fileOut.close();

	    log.info("Fin de impresión");

	} catch (FileNotFoundException e) {
	    log.error(e.getMessage(), e);
	    return null;

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	    return null;
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    return null;
	}

	return archivoSalida;

    }
    
    private String evaluarCampoTarjeta(final String celda) {
	String celdaAux = null;

	celdaAux = celda;
	if (celdaAux.length() == 16) {
	    if (Utils.isNumber(celdaAux)) {
		celdaAux = celdaAux.substring(0, 4).concat("XXXXXXXX")
			+ celdaAux.substring(12, 16);
	    }
	}
	return celdaAux;
    }

    public File generarPlanillaExcelObjRech(String fechaInicio,
	    String fechaFin, String componente, String nombreArchivo) {

	try {

	    log.info("Method: generarPlanillaExcelObjRech - Obteniendo parametros: nombreArchivo-> "
		    + nombreArchivo);
	    log.info("Method: generarPlanillaExcelObjRech - Obteniendo parametros: fechaInicio-> "
		    + fechaInicio);
	    log.info("Method: generarPlanillaExcelObjRech - Obteniendo parametros: fechaFin-> "
		    + fechaFin);
	    log.info("Method: generarPlanillaExcelObjRech - Obteniendo parametros: componente-> "
		    + componente);
	    Properties propiedades = PropertiesConfiguration.getProperty();

	    directorioExportacion = propiedades.getProperty("exportarexcel");

	} catch (Exception e) {

	    log.error(e.getMessage());
	    e.printStackTrace();
	    return null;
	}

	try {

	    Date fechaActual = new Date();
	    DateFormat formateador = new SimpleDateFormat("yyyyMMddHHmmss");

	    String fechaArchivo = formateador.format(fechaActual);
	    File dir = new File(nombreArchivo);

	    if (!dir.exists())
		if (!dir.mkdir())
		    return null;

	    // *************************************************************************//
	    // Creacion de planilla excel en direcotrio definido en archivo
	    // parametros //
	    // *************************************************************************//

	    // String generaArchivoExcel = directorioExportacion +
	    // nombreArchivo.toUpperCase() + "-" + fechaArchivo + ".xls";
	    String generaArchivoExcel = directorioExportacion + nombreArchivo
		    + ".csv";
	    archivoSalida = new File(generaArchivoExcel);

	    File file = new File(generaArchivoExcel);
	    FileOutputStream fileOut = new FileOutputStream(file);
	    HSSFWorkbook workbook = new HSSFWorkbook();
	    HSSFSheet worksheet = workbook.createSheet("POI Worksheet");

	    // *************************************************************************//
	    // Lectura de filas y columnas de la data recepcionada para
	    // exportacion //
	    // *************************************************************************//

	    ObjecionRechazoDiaDAO objRech = new ObjecionRechazoDiaDAO();
	    Object[] res = objRech.generarPlanillaExcelObjRech(fechaInicio,
		    fechaFin, componente, generaArchivoExcel);
	    // Object[] res =
	    // objRech.listaRechazoObjecion(fechaInicio,fechaFin,componente);
	    // int codigoError = (Integer) res[1];
	    // ResultSet rs = (ResultSet) res[0];

	    // CSVWriter writer = new CSVWriter(new
	    // FileWriter(generaArchivoExcel), ';');
	    // writer.writeAll(rs,true);
	    // writer.close();
	    log.info("Fin de impresión");

	} catch (FileNotFoundException e) {
	    // TODO Auto-generated catch block
	    log.error(e.getMessage());
	    e.printStackTrace();
	    return null;

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	    return null;
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    return null;
	}

	return archivoSalida;

    }

    private String ponerComaNumero(String numero) {

	int largonumero = numero.length();
	// alert("Numero = " + numero);
	// alert("Largo  = " + largonumero);

	String numeroFormateado = "0.00";
	if (largonumero > 2) {
	    String parteEntera = numero.substring(0, largonumero - 2);

	    String parteDecimal = numero
		    .substring(largonumero - 2, largonumero);
	    String parteEntFinal = "";
	    if (parteEntera.length() / 3 > 0) {
		StringBuilder builder = new StringBuilder(parteEntera);
		String s = builder.reverse().toString();

		String r = "";
		for (int i = 0; i < (parteEntera.length() / 3); i++) {

		    int j = 0;
		    for (j = 0; j < 3; j++) {
			r = r + s.charAt(j + (3 * i));

		    }
		    if (j == 3) {
			if (parteEntera.length() % 3 != 0) {
			    r += ".";
			} else {
			    if (parteEntera.length() / 3 != (i + 1))
				r += ".";
			}
		    }
		    // /&& (parteEntera.length()/3 != (i+1)) && () )

		}
		if (parteEntera.length() % 3 > 0) {

		    int valor = parteEntera.length() % 3;
		    String aux = parteEntera.substring(0, valor);
		    StringBuilder builderA = new StringBuilder(aux);
		    String xua = builderA.reverse().toString();
		    r += xua;

		}
		// dar vuelta
		StringBuilder builderF = new StringBuilder(r);
		parteEntFinal = builderF.reverse().toString();

	    } else {
		// no tiene mas de tres digitos
		parteEntFinal = parteEntera;
	    }

	    numeroFormateado = parteEntFinal + "," + parteDecimal;
	} else {
	    String parteDecimal = numero;
	    if (numero.length() == 2)
		numeroFormateado = "0," + parteDecimal;
	    else
		numeroFormateado = "0,0" + parteDecimal;
	}

	return numeroFormateado;

    }

    public File generarPlanillaExcelPDS(String fechaInicio, String fechaFin,
	    String nombreArchivo, String operador) {
	try {

	    log.info("Method: generarPlanillaExcelPDS - Obteniendo parametros: nombreArchivo-> "
		    + nombreArchivo);
	    log.info("Method: generarPlanillaExcelPDS - Obteniendo parametros: fechaInicio-> "
		    + fechaInicio);
	    log.info("Method: generarPlanillaExcelPDS - Obteniendo parametros: fechaFin-> "
		    + fechaFin);
	    Properties propiedades = PropertiesConfiguration.getProperty();
	    // propiedades.load(
	    // getClass().getResourceAsStream("/properties/conexion.properties"));
	    
	    

	    directorioExportacion = propiedades.getProperty("exportarexcel");

	} catch (Exception e) {

	    log.error(e.getMessage(), e);
	    return null;
	}

	try {
	    DecimalFormat formateadorMiles = new DecimalFormat("###,###.##");
	    Date fechaActual = new Date();
	    DateFormat formateador = new SimpleDateFormat("yyyyMMddHHmmss");

	    String fechaArchivo = formateador.format(fechaActual);
	    File dir = new File(nombreArchivo);

	    if (!dir.exists())
		if (!dir.mkdir())
		    return null;

	    // *************************************************************************//
	    // Creacion de planilla excel en direcotrio definido en archivo
	    // parametros //
	    // *************************************************************************//

	    // String generaArchivoExcel = directorioExportacion +
	    // nombreArchivo.toUpperCase() + "-" + fechaArchivo + ".xls";
	    String generaArchivoExcel = directorioExportacion + nombreArchivo
		    + ".xls";
	    String archivoPrueba = directorioExportacion + nombreArchivo
		    + ".csv";
	    archivoSalida = new File(archivoPrueba);

	    File file = new File(generaArchivoExcel);
	    FileOutputStream fileOut = new FileOutputStream(file);
	    HSSFWorkbook workbook = new HSSFWorkbook();
	    HSSFSheet worksheet = workbook.createSheet("POI Worksheet");

	    // *************************************************************************//
	    // Lectura de filas y columnas de la data recepcionada para
	    // exportacion //
	    // *************************************************************************//

	    InformesPDSDAO informePDS = new InformesPDSDAO();
	    Object[] res = informePDS.buscarInformePDSrangoFechas(fechaInicio,
		    fechaFin, archivoPrueba, operador);

	    int codigoError = (Integer) res[1];
	    log.info("Resultado codigo error " + codigoError);
	    ResultSet rs = (ResultSet) res[0];
	    // Fecha_Trx

	    // StringWriter writer = new StringWriter();
	    // CSVWriter writer = new CSVWriter(new FileWriter(archivoPrueba),
	    // ';');
	    // writer.writeAll(rs,true);
	    // writer.close();
	    log.info("archivo generado");
	    log.info("Fin de impresión");

	} catch (FileNotFoundException e) {
	    log.error(e.getMessage(), e);
	    return null;

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	    return null;
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    return null;
	}

	return archivoSalida;

    }

    private String validaDatoPDS(String s) {
	String valor = " ";

	if (s == null)
	    return valor;
	else
	    return s;
    }

    public File generarPlanillaExcelPDSInt(String fechaInicio, String fechaFin,
	    String nombreArchivo) {
	try {

	    log.info("Method: generarPlanillaExcelPDSInt - Obteniendo parametros: nombreArchivo-> "
		    + nombreArchivo);
	    log.info("Method: generarPlanillaExcelPDSInt - Obteniendo parametros: fechaInicio-> "
		    + fechaInicio);
	    log.info("Method: generarPlanillaExcelPDSInt - Obteniendo parametros: fechaFin-> "
		    + fechaFin);
	    Properties propiedades = PropertiesConfiguration.getProperty();
	    // propiedades.load(
	    // getClass().getResourceAsStream("/properties/conexion.properties"));

	    directorioExportacion = propiedades.getProperty("exportarexcel");

	} catch (Exception e) {

	    log.error(e.getMessage(), e);
	    return null;
	}

	try {
	    DecimalFormat formateadorMiles = new DecimalFormat("###,###.##");
	    Date fechaActual = new Date();
	    DateFormat formateador = new SimpleDateFormat("yyyyMMddHHmmss");

	    String fechaArchivo = formateador.format(fechaActual);
	    File dir = new File(nombreArchivo);

	    if (!dir.exists())
		if (!dir.mkdir())
		    return null;

	    // *************************************************************************//
	    // Creacion de planilla excel en direcotrio definido en archivo
	    // parametros //
	    // *************************************************************************//

	    // String generaArchivoExcel = directorioExportacion +
	    // nombreArchivo.toUpperCase() + "-" + fechaArchivo + ".xls";
	    String generaArchivoExcel = directorioExportacion + nombreArchivo
		    + ".csv";
	    archivoSalida = new File(generaArchivoExcel);

	    File file = new File(generaArchivoExcel);
	    FileOutputStream fileOut = new FileOutputStream(file);
	    HSSFWorkbook workbook = new HSSFWorkbook();
	    HSSFSheet worksheet = workbook.createSheet("POI Worksheet");

	    // *************************************************************************//
	    // Lectura de filas y columnas de la data recepcionada para
	    // exportacion //
	    // *************************************************************************//

	    InformesPDSIntDAO informePDS = new InformesPDSIntDAO();
	    // String dataExcel =
	    // informePDS.buscarInformePDSIntRangoFechas(fechaInicio,fechaFin);
	    Object[] res = informePDS.buscarInformePDSIntRangoFechas(
		    fechaInicio, fechaFin);
	    int codigoError = (Integer) res[1];
	    ResultSet rs = (ResultSet) res[0];

	    // StringWriter writer = new StringWriter();
	    CSVWriter writer = new CSVWriter(
		    new FileWriter(generaArchivoExcel), ';');
	    writer.writeAll(rs, true);
	    writer.close();
	    log.info("Fin de impresión");

	} catch (FileNotFoundException e) {
	    // TODO Auto-generated catch block
	    log.error(e.getMessage(), e);
	    return null;

	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    log.error(e.getMessage(), e);
	    return null;
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    log.error(e.getMessage(), e);
	    return null;
	}

	return archivoSalida;

    }

    public File generarPlanillaExcelTxs(String fechaInicio, String fechaFin,
	    String numeroTarjeta, String tipoTransaccion, String nombreArchivo,
	    String tipoBusqueda) {
	int tipoTransaccion1 = 0;
	String[] xKeyArray = null;
	try {

	    log.info("Method: generarPlanillaExcelTxs - Obteniendo parametros: nombreArchivo-> "
		    + nombreArchivo);
	    log.info("Method: generarPlanillaExcelTxs - Obteniendo parametros: fechaInicio-> "
		    + fechaInicio);
	    log.info("Method: generarPlanillaExcelTxs - Obteniendo parametros: fechaFin-> "
		    + fechaFin);
	    log.info("Method: generarPlanillaExcelTxs - Obteniendo parametros: numeroTarjeta-> "
		    + numeroTarjeta);
	    log.info("Method: generarPlanillaExcelTxs - Obteniendo parametros: tipoTransaccion-> "
		    + tipoTransaccion);
	    Properties propiedades = PropertiesConfiguration.getProperty();
	    // propiedades.load(
	    // getClass().getResourceAsStream("/properties/conexion.properties"));

	    directorioExportacion = propiedades.getProperty("exportarexcel");

	} catch (Exception e) {

	    log.error(e.getMessage(), e);
	    return null;
	}
	/***
	 * cambiar formato fecha a aaammdd llega dd/mm/aaaa
	 */

	fechaInicio = fechaInicio.substring(6, 10)
		+ fechaInicio.substring(3, 5) + fechaInicio.substring(0, 2);

	fechaFin = fechaFin.substring(6, 10) + fechaFin.substring(3, 5)
		+ fechaFin.substring(0, 2);

	try {
	    DecimalFormat formateadorMiles = new DecimalFormat("###,###.##");
	    Date fechaActual = new Date();
	    DateFormat formateador = new SimpleDateFormat("yyyyMMddHHmmss");

	    String fechaArchivo = formateador.format(fechaActual);
	    File dir = new File(nombreArchivo);

	    if (!dir.exists())
		if (!dir.mkdir())
		    return null;

	    // *************************************************************************//
	    // Creacion de planilla excel en direcotrio definido en archivo
	    // parametros //
	    // *************************************************************************//

	    // String generaArchivoExcel = directorioExportacion +
	    // nombreArchivo.toUpperCase() + "-" + fechaArchivo + ".xls";
	    String generaArchivoExcel = directorioExportacion + nombreArchivo
		    + ".xls";
	    String archivoPrueba = directorioExportacion + nombreArchivo
		    + ".csv";

	    archivoSalida = new File(archivoPrueba);
	    

	    File file = new File(generaArchivoExcel);
	    FileOutputStream fileOut = new FileOutputStream(file);
	    HSSFWorkbook workbook = new HSSFWorkbook();
	    HSSFSheet worksheet = workbook.createSheet("POI Worksheet");

	    // *************************************************************************//
	    // Lectura de filas y columnas de la data recepcionada para
	    // exportacion //
	    // *************************************************************************//
	    // cambiar por el metodo de tx
	    TransaccionesDAO transaccion = new TransaccionesDAO();

	    File res = null;
	    String codigoTransaccion = "";
	    String codigoFuncion = "";
	    if (tipoTransaccion.equalsIgnoreCase("1240_200")
		    || tipoTransaccion.equalsIgnoreCase("05_00")
		    || tipoTransaccion.equalsIgnoreCase("06_00")
		    || tipoTransaccion.equalsIgnoreCase("07_00")
		    || tipoTransaccion.equalsIgnoreCase("25_00")
		    || tipoTransaccion.equalsIgnoreCase("26_00")
		    || tipoTransaccion.equalsIgnoreCase("27_00")
		    || tipoTransaccion.equalsIgnoreCase("00_00")
		    || tipoTransaccion.equalsIgnoreCase("05_205")
		    || tipoTransaccion.equalsIgnoreCase("06_205")
		    || tipoTransaccion.equalsIgnoreCase("07_205")) { // Transaccion
								     // De
								     // Presentacion.//
		// buscarTransaccionPresentacion("");
		// transaccionPresentacionAction
		xKeyArray = tipoTransaccion.split("_");
		codigoTransaccion = xKeyArray[0];
		codigoFuncion = xKeyArray[1];
		res = transaccion.buscarTransaccionPresentacionExport(
			numeroTarjeta, fechaInicio, fechaFin, "",
			archivoPrueba, codigoTransaccion, codigoFuncion);
	    }

	    if (tipoTransaccion.equalsIgnoreCase("1442_450")
		    || tipoTransaccion.equalsIgnoreCase("1442_453")
		    || tipoTransaccion.equalsIgnoreCase("1442_451")
		    || tipoTransaccion.equalsIgnoreCase("1442_453")
		    || tipoTransaccion.equalsIgnoreCase("1442_454")
		    || tipoTransaccion.equalsIgnoreCase("1644_603")
		    || tipoTransaccion.equalsIgnoreCase("1644_605")
		    || tipoTransaccion.equalsIgnoreCase("1240_205")
		    || tipoTransaccion.equalsIgnoreCase("1240_282")
		    // VISA
		    || tipoTransaccion.equalsIgnoreCase("15_00")
		    || tipoTransaccion.equalsIgnoreCase("15_205")
		    || tipoTransaccion.equalsIgnoreCase("52_00")) {
		xKeyArray = tipoTransaccion.split("_");
		codigoTransaccion = xKeyArray[0];
		codigoFuncion = xKeyArray[1];
		// Primer Contracargo Total 1442_450
		// Primer Contracargo Parcial 1442_453
		// Segundo Contracargo Total 1442_451
		// Segundo Contracargo Parcial 1442_454
		// Peticion De Vale 1644_603
		// Confirmacion Peticion Vale 1644_605
		// Representacion Total 1240_205
		// Representacion Parcial 1240_282
		res = transaccion.buscarTransaccionAccionesExport(
			numeroTarjeta, fechaInicio, fechaFin,
			codigoTransaccion, codigoFuncion, "", archivoPrueba,
			tipoBusqueda);

	    }

	    String tipoObjecionRechazo = "";
	    if (tipoTransaccion.equalsIgnoreCase("OBJ")
		    || tipoTransaccion.equalsIgnoreCase("REC")) { // Transaccion
								  // De
								  // Objeciones
								  // Rechazos.
								  // //
		tipoObjecionRechazo = tipoTransaccion;
		res = transaccion.buscarTransaccionObjecionRechazoExport(
			numeroTarjeta, fechaInicio, fechaFin,
			tipoObjecionRechazo, "", archivoPrueba, tipoBusqueda);
	    }

	    if (tipoTransaccion.equalsIgnoreCase("1740_700")) { // Transaccion
								// Recuperacion
								// De Cargos
								// Incoming. //
		codigoTransaccion = "1740";
		res = transaccion
			.buscarTransaccionRecuperacionCargosIncomingExport(
				numeroTarjeta, fechaInicio, fechaFin,
				codigoTransaccion, "", archivoPrueba);
		// buscarTransaccionRecuperacionCargosIncoming(codigoTransaccion,"");
	    }

	    if (tipoTransaccion.equalsIgnoreCase("1740_780")) { // Transaccion
								// Recuperacion
								// De Cargos
								// Outgoing. //
		codigoTransaccion = "1740";
		res = transaccion
			.buscarTransaccionRecuperacionCargosOutgoingExport(
				numeroTarjeta, fechaInicio, fechaFin,
				codigoTransaccion, "", archivoPrueba);
		// buscarTransaccionRecuperacionCargosOutgoing(codigoTransaccion,"");
	    }

	    if (tipoTransaccion.equalsIgnoreCase("CONF_RVL")) { // Transaccion
								// Confirmacion
								// de recepcion
								// de vales. //
		codigoTransaccion = "CONF_RVL";
		res = transaccion
			.buscarTransaccionConfirmacionRecepcionPeticionValesExport(
				numeroTarjeta, fechaInicio, fechaFin,
				codigoTransaccion, "", archivoPrueba);
		// buscarTransaccionConfirmacionRecepcionPeticionVales(codigoTransaccion,"");
	    }

	    if (tipoTransaccion.equalsIgnoreCase("01_00")
		    || tipoTransaccion.equalsIgnoreCase("02_00")
		    || tipoTransaccion.equalsIgnoreCase("03_00")
		    || tipoTransaccion.equalsIgnoreCase("04_00")) {
		xKeyArray = tipoTransaccion.split("_");
		codigoTransaccion = xKeyArray[0];
		codigoFuncion = xKeyArray[1];
		res = transaccion.buscarTransaccionOnUsExport(numeroTarjeta,
			fechaInicio, fechaFin, tipoTransaccion, codigoFuncion,
			codigoTransaccion, archivoPrueba);
	    }

	    // StringWriter writer = new StringWriter();

	    log.info("Fin de impresión");

	} catch (FileNotFoundException e) {
	    log.error(e.getMessage(), e);
	    return null;

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	    return null;
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    return null;
	}

	return archivoSalida;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param nombreArchivo
     * @param trxNoAutorizadasPorIc
     * 
     * @param dataExcel
     * @param nombreArchivo
     * @return
     * @since 1.X
     */
    public File generarExcelConciliacion(
	    List<TransaccionesAutor> trxNoAutorizadasPorIc, String nombreArchivo) {

	try {

	    log.info("Method: generarExcelConciliacion - Obteniendo parametros: nombreArchivo-> "
		    + nombreArchivo);

	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    directorioExportacion = propiedades.getProperty("exportarexcel");
	    Date fechaActual = new Date();
	    DateFormat formateador = new SimpleDateFormat("yyyyMMddHHmmss");

	    String fechaArchivo = formateador.format(fechaActual);
	    File dir = new File(nombreArchivo);

	    // *************************************************************************//
	    // Creacion de planilla excel en direcotorio definido en archivo
	    // parametros
	    //
	    // *************************************************************************//
	    //log.info(trxNoAutorizadasPorIc);
	    String generaArchivoExcel = directorioExportacion + nombreArchivo
		    + ".csv";
	    archivoSalida = new File(generaArchivoExcel);
	    FileOutputStream fileOut = new FileOutputStream(archivoSalida);

	    exportarExcel(trxNoAutorizadasPorIc, generaArchivoExcel);

	    log.info("Fin de impresión");

	} catch (FileNotFoundException e) {
	    log.error(e.getMessage(), e);
	    return null;

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	    return null;
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    return null;
	}

	return archivoSalida;

    }

    private String[] generarCabeceraExcelConciliacion() {

	return new String[] { "SID", "NUMERO TARJETA", "CODIGO AUTORIZACION",
		"NOMBRE COMERCIO", "MONTO TRANSACCION", "FECHA COMPRA" };

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param trxNoAutorizadasPorIc
     * @param ruta
     * @since 1.X
     */
    private void exportarExcel(List<TransaccionesAutor> trxNoAutorizadasPorIc,
	    String ruta) {
	List<String[]> dataList = new ArrayList<String[]>();
	CSVWriter writer = null;

	try {
	    // GENERA CABECERA
	    String[] header = generarCabeceraExcelConciliacion();

	    // PARSEO DE DATOS DEL EXCEL
	    for (TransaccionesAutor trxInstance : trxNoAutorizadasPorIc) {
		dataList.add(getRegistrosToString(trxInstance));
	    }

	    writer = new CSVWriter(new FileWriter(ruta), ';');
	    writer.writeNext(header);
	    writer.writeAll(dataList);

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	} finally {
	    if (writer != null) {
		try {
		    writer.close();
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}
	    }
	}
    }

    /**
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param trx
     * @return
     * @since 1.X
     */
    private String[] getRegistrosToString(TransaccionesAutor trx) {
	String registro = "";
	registro += trx.getSid() + ";";
	registro += trx.getNumeroTarjeta() + ";";
	registro += trx.getCodAutorizacion() + ";";
	registro += trx.getNombreComercio() + ";";
	//log.info(trx.getMontoFormateado());
	registro += Utils.formateaMonto(trx.getMontoFormateado()) + ";";
	registro += trx.getFechaCompra() + ";";

	return registro.split(";");
    }

    /**
     * Metodo que genera la planilla excel.
     * 
     * @return Archivo de exportacion .csv
     * @throws Mensaje
     *             de excepcion controlado.
     * @param dataExcel
     *            Datos a ser exprotacdos en planilla.
     * @param nombreArchivo
     *            Nombre del archivo generado.
     */
    public File generarPlanillaExcelInterfaces(String fechaInicio,
	    String fechaFin, Integer estado, Integer estadoGestion,
	    Integer estadoDetalle) {

	String nombreArchivo = "DetalleInterfacesAvances";

	try {

	    log.info("Method: generarPlanillaExcelInterfaces - Obteniendo parametros: fechaInicio-> "
		    + fechaInicio);
	    log.info("Method: generarPlanillaExcelInterfaces - Obteniendo parametros: fechaFin-> "
		    + fechaFin);
	    log.info("Method: generarPlanillaExcelInterfaces - Obteniendo parametros: estado-> "
		    + estado);
	    log.info("Method: generarPlanillaExcelInterfaces - Obteniendo parametros: estadoGestion-> "
		    + estadoGestion);
	    log.info("Method: generarPlanillaExcelInterfaces - Obteniendo parametros: estadoDetalle-> "
		    + estadoDetalle);
	    Properties propiedades = PropertiesConfiguration.getProperty();

	    directorioExportacion = propiedades.getProperty("exportarexcel");

	    log.info("directorioExportacion: " + directorioExportacion);

	} catch (Exception e) {

	    log.error(e.getMessage(), e);
	    return null;
	}

	try {
	    File dir = new File(nombreArchivo);

	    if (!dir.exists())
		if (!dir.mkdir())
		    return null;

	    // *************************************************************************//
	    // Creacion de planilla excel en direcotrio definido en archivo
	    // parametros //
	    // *************************************************************************/
	    
	    String archivoPrueba = directorioExportacion + nombreArchivo
		    + ".csv";
	    archivoSalida = new File(archivoPrueba);

	    File file = new File(archivoPrueba);
	    FileOutputStream fileOut = new FileOutputStream(file);
	    FileWriter outputfile = new FileWriter(file); 
	    HSSFWorkbook workbook = new HSSFWorkbook();
	    HSSFSheet worksheet = workbook.createSheet("POI Worksheet");
		        
	    // *************************************************************************//
	    // Lectura de filas y columnas de la data recepcionada para
	    // exportacion //
	    // *************************************************************************//
	    ConsultarTrxAvancesDAO consultarTrxAvanceDAO = new ConsultarTrxAvancesDAO();
	    File res = null;

	    res = consultarTrxAvanceDAO.buscarInterfacesExport(fechaInicio,
		    fechaFin, estado, estadoGestion, estadoDetalle,
		    archivoPrueba);
	    
	    log.info("Fin de impresión");

	} catch (FileNotFoundException e) {
	    log.error(e.getMessage(), e);
	    return null;

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	    return null;
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    return null;
	}

	return archivoSalida;
    }
    
    /**
     * Metodo que genera la planilla excel seleccionadas
     * 
     * @return Archivo de exportacion .csv
     * @throws Mensaje
     *             de excepcion controlado.
     * @param dataExcel
     *            Datos a ser exprotacdos en planilla.
     * @param nombreArchivo
     *            Nombre del archivo generado.
     */
    
    
    public File generarPlanillaExcelInterfacesSeleccionadas(String fechaInicio,
	    String fechaFin, Integer estado, Integer estadoGestion,
	    Integer estadoDetalle, String listaIds) {

	String nombreArchivo = "DetalleInterfacesAvancesSeleccionadas";
	

	try {
	    log.info("Method: generarPlanillaExcelInterfacesSeleccionadas - Obteniendo parametros: fechaInicio-> "
		    + fechaInicio);
	    log.info("Method: generarPlanillaExcelInterfacesSeleccionadas - Obteniendo parametros: fechaFin-> "
		    + fechaFin);
	    log.info("Method: generarPlanillaExcelInterfacesSeleccionadas - Obteniendo parametros: estado-> "
		    + estado);
	    log.info("Method: generarPlanillaExcelInterfacesSeleccionadas - Obteniendo parametros: estadoGestion-> "
		    + estadoGestion);
	    log.info("Method: generarPlanillaExcelInterfacesSeleccionadas - Obteniendo parametros: estadoDetalle-> "
		    + estadoDetalle);
	    log.info("Method: generarPlanillaExcelInterfacesSeleccionadas - Obteniendo parametros: listaTodo-> "
		    + listaIds);

	    Properties propiedades = PropertiesConfiguration.getProperty();

	    directorioExportacion = propiedades.getProperty("exportarexcel");

	    log.info("directorioExportacion: " + directorioExportacion);

	} catch (Exception e) {

	    log.error(e.getMessage(), e);
	    return null;
	}

	try {
	    File dir = new File(nombreArchivo);

	    if (!dir.exists())
		if (!dir.mkdir())
		    return null;

	    // *************************************************************************//
	    // Creacion de planilla excel en direcotrio definido en archivo
	    // parametros //
	    // *************************************************************************/
	    
	    String archivoPrueba = directorioExportacion + nombreArchivo
		    + ".csv";
	    archivoSalida = new File(archivoPrueba);

	    File file = new File(archivoPrueba);
	    FileOutputStream fileOut = new FileOutputStream(file);
	    FileWriter outputfile = new FileWriter(file); 
	    HSSFWorkbook workbook = new HSSFWorkbook();
	    HSSFSheet worksheet = workbook.createSheet("POI Worksheet");
		        
	    // *************************************************************************//
	    // Lectura de filas y columnas de la data recepcionada para
	    // exportacion //
	    // *************************************************************************//
	    ConsultarTrxAvancesDAO consultarTrxAvanceDAO = new ConsultarTrxAvancesDAO();
	    File res = null;
	    
	   
	    res = consultarTrxAvanceDAO.buscarInterfacesExportSeleccionadas(fechaInicio,
		    fechaFin, estado, estadoGestion, estadoDetalle,
		    archivoPrueba,listaIds);

	    
	    log.info("Fin de impresión");

	} catch (FileNotFoundException e) {
	    log.error(e.getMessage(), e);
	    return null;

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	    return null;
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    return null;
	}

	return archivoSalida;
    }

}
