package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import Beans.TC46;
import cl.filter.FiltroParametro;
import properties.PropertiesConfiguration;
import types.EstadoType;
import db.OracleBD;

public class TC46DAO {
    
    	static final Logger log =  Logger.getLogger(ParametrosDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

	private OracleBD obd = new OracleBD();
	private Properties prop = new Properties();
	private String esquema;
	private ResultSet rs;
	private Connection conexion;
	private String listaTC46;
	
	
	public TC46DAO(){
	    setProp();
	    this.esquema = prop.getProperty("esquema");
	}
	
	private void setProp(){
	    prop= PropertiesConfiguration.getProperty();
	}
	
	public void setConexion(){
	    this.conexion = obd.conectar();
	    obd.setResultado(rs);
	}
	
	public Connection getConexion(){
	    return conexion;
	}
	public void setConexion(Connection conexion) {
	    this.conexion = conexion;
	}
	
	public String getListaTc46(String fechaInicio, String fechaFin, int operador){
	    setConexion();
	    listaTC46 = "esttrx:0~";
	    
	    //String sql = "SELECT SETTLEMENT_DATE AS SETTLEMENT_DATE, TOTAL_INTERCHANGE_COUNT AS TOTAL_INTERCHANGE_COUNT, TOTAL_INTERCHANGE_VALUE AS TOTAL_INTERCHANGE_VALUE, INTERCHANGE_VALUE_SIGN AS INTERCHANGE_VALUE_SIGN, TOTAL_REIMBURSEMENT_FEES AS TOTAL_REIMBURSEMENT_FEES, REIMBURSEMENT_FEES_SIGN AS REIMBURSEMENT_FEES_SIGN  FROM TBL_CPAGO_VISA";
	    String sql = "select TO_CHAR(SETTLEMENT_DATE, 'dd/mm/yyyy') AS FECHAPAGO, TOTAL_INTERCHANGE_COUNT AS CANT_TOTAL_INTERCAMBIO, TOTAL_INTERCHANGE_VALUE AS MONTO_TOTAL_INTERCAMBIO, (SELECT SIGN FROM TBL_CPAGO_VISA_SIGN WHERE SID = INTERCHANGE_VALUE_SIGN) AS SIGN_INTERCAMBIO, TOTAL_REIMBURSEMENT_FEES AS MONTO_TOTAL_REEMBOLZO, (SELECT SIGN FROM TBL_CPAGO_VISA_SIGN WHERE SID = REIMBURSEMENT_FEES_SIGN) AS SIGN_REEMBOLSO, TOTAL_VISA_CHARGES AS CARGOS_TOTALES_VISA, (SELECT SIGN FROM TBL_CPAGO_VISA_SIGN WHERE SID = VISA_CHARGES_SIGN) AS SIGN_CARGO_VISA, NET_SETTLEMENT_AMOUNT AS MONTO_A_PAGAR, (SELECT SIGN FROM TBL_CPAGO_VISA_SIGN WHERE SID = NET_SETTLEMENT_AMOUNT_SIGN) AS SIGN_MONTO_A_PAGAR FROM TBL_CPAGO_VISA WHERE SETTLEMENT_DATE BETWEEN TO_DATE ('"+fechaInicio+"', 'DD-MM-YY') AND TO_DATE('"+fechaFin+"', 'DD-MM-YY') AND BUSINESS_MODE = '9' AND REC_SRE_IDENTIFIER IS NULL AND OPERADOR = "+operador+"";
            log.info(sql);
            rs = obd.consultar(sql);
            log.info(rs);
            obd.setResultado(rs);   
                
                
            try {
		if(rs.next()){
		    log.info("Method: getListaTc46 - Obteniendo resultados");
		    rs.previous();
		    while(rs.next()){
			//listaTC46 += rs.getInt("IDS")+"|";
			listaTC46 += rs.getString("FECHAPAGO")+"|";
			listaTC46 += rs.getInt("CANT_TOTAL_INTERCAMBIO")+"|";
			listaTC46 += rs.getInt("MONTO_TOTAL_INTERCAMBIO")+"|";
			listaTC46 += rs.getString("SIGN_INTERCAMBIO")+"|";
			listaTC46 += rs.getInt("MONTO_TOTAL_REEMBOLZO")+"|";
			listaTC46 += rs.getString("SIGN_REEMBOLSO")+"|";
			listaTC46 += rs.getInt("CARGOS_TOTALES_VISA")+"|";
			listaTC46 += rs.getString("SIGN_CARGO_VISA")+"|";
			listaTC46 += rs.getInt("MONTO_A_PAGAR")+"|";
			listaTC46 += rs.getString("SIGN_MONTO_A_PAGAR")+("~");
		    }
		    rs.close();
			
		}else{
		    log.info("Method: getListaParametros - No hay datos");
		    listaTC46 = "esttrx:1~No hay datos";
		}
		log.info("Method: getListaParametros - Serializando lista");
            } catch (SQLException e) {
		
		log.error(e.getMessage(),e);
		
            }finally{
		if (obd.cerrarResultSet()==false){
		    log.error("Error: No se pudo cerrar resulset.");
		    if (obd.cerrarConexionBD()==false){
			 log.error("Error: No se pudo cerrar conexion a la base de datos.");
		    }			
		}else{
		    if (obd.cerrarConexionBD()==false){
			 log.error("Error: No se pudo cerrar conexion a la base de datos.");
		    }	
		}
		try {
		    if(conexion != null && !conexion.isClosed()){
			conexion.close();
		    }
		}
		catch ( SQLException e ) {
		    log.error(e.getMessage(),e);
		}
            }    
            log.info(listaTC46);
            //where SETTLEMENT_DATE BETWEEN '01-JAN-00' AND '26-JUL-19'
	    return listaTC46;
	}

}
