package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.OtraGestionBean;
import db.OracleBD;

public class OtrasGestionesDAO {

	static final Logger log =  Logger.getLogger(OtrasGestionesDAO.class);
	
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

	private String esquema;

	public OtrasGestionesDAO() {
		try {
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();
			esquema = propiedades.getProperty("esquema");
		} catch (Exception e) {
			log.error("Error al leer el archivo de propiedades.");
			log.error( e.getMessage(),e );
		}
	}

	public String obtenerTipoGestion() {

		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		ResultSet resultados = null;

		String listaTipoGestion = "";
		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);

		try {
		    log.info( "***** SE EJECUTA SP_SIG_OBTTIPO_OTRGEST(?, ?, ?)" );
		    
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_OBTTIPO_OTRGEST(?, ?, ?, ?); END;");
			
			stmtProcedure.setString("tipoGestion", "G");
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
				log.info("Method: buscarReporteVisa - Recorriendo resultados");
				while (resultados.next()) {
					listaTipoGestion += resultados.getString("SID") + "|";
					listaTipoGestion += resultados.getString("DESCRIPCION") + "~";
				}
					
			}else{
				log.error("Method: obtenerTipoGestion - Problemas al leer reporte visa desde la base de datos.");
				listaTipoGestion="esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
			}
			
			resultados.close();
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTipoGestion = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
				 return listaTipoGestion;
			}
			
			log.info("Method: buscarReporteVisa - " + listaTipoGestion);
		} catch (SQLException e) {

			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			if (bdOracle.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaTipoGestion = "esttrx|1~MSG|Problemas al leer reporte vissa desde la base de datos...~";
				return listaTipoGestion;
			}
			log.error( e.getMessage(),e );
			listaTipoGestion = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
		} finally {
			if (bdOracle.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			} else {
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			}
			try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
		}

		return listaTipoGestion;
	}

	public String crearOtraGestion(OtraGestionBean otraGestionBean) {

		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;

		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);
		String response = "";

		try {
		    log.info( "***** SE EJECUTA SP_SIG_INGRESAR_OTRA_GESTION(refTransaccion, refTPGestion, refUsuario, glosa, numeroIncidente)" );
		    log.info( otraGestionBean );
		    
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_INGRESAR_OTRA_GESTION(?, ?, ?, ?, ?); END;");

			stmtProcedure.setInt("refTransaccion", otraGestionBean.getRefTransaccion());
			stmtProcedure.setInt("refTPGestion", otraGestionBean.getRefTpGestion());
			stmtProcedure.setInt("refUsuario", otraGestionBean.getRefUsuario());
			stmtProcedure.setString("glosa", otraGestionBean.getGlosa());
			stmtProcedure.setString("numeroIncidente", otraGestionBean.getNumeroIncidente());

			stmtProcedure.execute();

			stmtProcedure.close();

			response = "1";
		} catch (SQLException e) {

			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error( e.getMessage(),e );
			response = "0";
		} finally {
			if (bdOracle.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			} else {
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			}
			try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
		}
		return response;
	}
}
