package clases;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;
/**
 * Clase DAO que administra la recuperacion de cobro de cargos.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class RecuperacionCobroCargoDAO {

	static final Logger log =  Logger.getLogger(RecuperacionCobroCargoDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	private String esquema;
	
	public RecuperacionCobroCargoDAO(){
		try {
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));

		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
			    log.error("Error al leer el archivo de propiedades.");
        		log.error( e.getMessage(), e );
    	}
	}
	
	/**
	 * Metodo que consulta las funciones para recuperacion cobro de cargo.
	 * @return  Lista de funciones de recuperacion cobro cargo.
	 * @throws Mensaje de excepcion controlado.
	 * @param codigoTransaccion codigo de la transaccion.
	 */
	public String buscarFuncionesRecuperacionCargo(String codigoTransaccion){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;

		String listaFunciones="";
		
		try {
			
			log.info("Method: buscarFuncionesRecuperacionCargo - Parametros Entrada : " + " codigoTransaccion = " + codigoTransaccion);
			log.info("Ejecutando SP - SP_SIG_FUNCIONESCOBROCARGO");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_FUNCIONESCOBROCARGO(?, ?, ?, ?); END;");
			
			stmtProcedure.setString("codigoTransaccion", codigoTransaccion); 
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			if (resultados.next()){
				
						log.info("Lectura de datos desde el cursor : ");
				
						listaFunciones="esttrx|0~";
						   
						listaFunciones += resultados.getInt("SID") + "|";
						listaFunciones += resultados.getString("XKEY") + "|";
						listaFunciones += resultados.getString("COD_TRANSAC") + "|";
						listaFunciones += resultados.getString("COD_FUNCION") + "|";
						listaFunciones += resultados.getString("DESCRIPCION") + "|";
						listaFunciones += resultados.getString("TIPO_ACCION") + "~";         				
						
						String strprint = "";
						strprint = resultados.getInt("SID") + " " + resultados.getString("XKEY") + " ";
						strprint+= resultados.getString("COD_TRANSAC") + " " + resultados.getString("COD_FUNCION") + " ";
						strprint+= resultados.getString("DESCRIPCION") + " " + resultados.getString("TIPO_ACCION") + " ";
						
						log.info("Rescatando registro : " + strprint);
						
						while (resultados.next()) {
						
								listaFunciones += resultados.getInt("SID") + "|";
								listaFunciones += resultados.getString("XKEY") + "|";
								listaFunciones += resultados.getString("COD_TRANSAC") + "|";
								listaFunciones += resultados.getString("COD_FUNCION") + "|";
								listaFunciones += resultados.getString("DESCRIPCION") + "|";
								listaFunciones += resultados.getString("TIPO_ACCION") + "~";
								
								strprint = resultados.getInt("SID") + " " + resultados.getString("XKEY") + " ";
								strprint+= resultados.getString("COD_TRANSAC") + " " + resultados.getString("COD_FUNCION") + " ";
								strprint+= resultados.getString("DESCRIPCION") + " " + resultados.getString("TIPO_ACCION") + " ";
								
								log.info("Rescatando registro : " + strprint);
								
						}
			}else{
				listaFunciones="esttrx|1~MSG|No existe informacion con los parametros seleccionados.~";
				log.info("No existe informacion con los parametros seleccionados.");
			}
			
			resultados.close();
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaFunciones = "esttrx|1~MSG|Problemas al leer las funciones para recuperacion cobro cargo...~";
				 return listaFunciones;
			}
			
		}catch (SQLException e){
			
				bdOracle.setResultado(resultados);
				bdOracle.cerrarResultSet();
				bdOracle.cerrarStatement(stmtProcedure);
			
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaFunciones = "esttrx|1~MSG|Problemas al leer las funciones para recuperacion cobro cargo...~";
					return listaFunciones;
				}
			
			    log.error( e.getMessage(),e );
        		listaFunciones = "esttrx|1~MSG|Problemas al leer las funciones para recuperacion cobro cargo...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
    	}
		
		return listaFunciones;
	}/* Fin buscarFuncionesRecuperacionCargo*/
	

	/**
	 * Metodo que consulta las razones para la recuperacion de cobro de cargos.
	 * @return Lista de razones de recuperacion cobro cargo.
	 * @throws Mensaje de excepcion controlado.
	 * @param sid codigo de sid.
	 */
	public String buscarRazonesFuncionRecuperacionCobro(Integer sid){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		
		String listaRazones="";
		
		try {
			
			log.info("Method: buscarRazonesFuncionRecuperacionCobro - Parametros Entrada : " + " sid = " + sid);
			log.info("Ejecutando SP - SP_SIG_RAZONESCOBROCARGO");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_RAZONESCOBROCARGO(?, ?, ?, ?); END;");
			
			stmtProcedure.setInt("vsid", sid); 
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			if (resultados.next()){
				
						log.info("Lectura de datos desde el cursor : ");
						listaRazones="esttrx|0~";
						
						listaRazones += resultados.getInt("SID") + "|";
						listaRazones += resultados.getString("COD_MOTIVO_INI") + "|";
						listaRazones += resultados.getString("COD_MOTIVO_FIN") + "|";
						listaRazones += resultados.getString("DESCRIPCION") + "|";
						listaRazones += resultados.getInt("PLAZO") + "~";         				
						
						String strprint = "";
						strprint = resultados.getInt("SID") + " " + resultados.getString("COD_MOTIVO_INI") + " ";
						strprint+= resultados.getString("COD_MOTIVO_FIN") + " " + resultados.getString("DESCRIPCION") + " ";
						strprint+= resultados.getInt("PLAZO") + " ";
						
						log.info("Rescatando registro : " + strprint);
						
						while (resultados.next()) {
						
								listaRazones += resultados.getInt("SID") + "|";
								listaRazones += resultados.getString("COD_MOTIVO_INI") + "|";
								listaRazones += resultados.getString("COD_MOTIVO_FIN") + "|";
								listaRazones += resultados.getString("DESCRIPCION") + "|";
								listaRazones += resultados.getInt("PLAZO") + "~";         				
							
								strprint = resultados.getInt("SID") + " " + resultados.getString("COD_MOTIVO_INI") + " ";
								strprint+= resultados.getString("COD_MOTIVO_FIN") + " " + resultados.getString("DESCRIPCION") + " ";
								strprint+= resultados.getInt("PLAZO") + " ";
								
								log.info("Rescatando registro : " + strprint);
						
						}
			}else{
				listaRazones="esttrx|1~MSG|No existe informacion con los parametros seleccionados.~";
				log.info("No existe informacion con los parametros seleccionados.");
			}
			
			resultados.close();
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaRazones = "esttrx|1~MSG|Problemas al leer las razones para funcion de recuperacion cobro cargo...~";
				 return listaRazones;
			}
			
		}catch (SQLException e){   
			
				bdOracle.setResultado(resultados);
				bdOracle.cerrarResultSet();
				bdOracle.cerrarStatement(stmtProcedure);
		
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaRazones = "esttrx|1~MSG|Problemas al leer las razones para funcion de recuperacion cobro cargo...~";
					return listaRazones;
				}
			
				log.error( e.getMessage(),e );
        		listaRazones = "esttrx|1~MSG|Problemas al leer las razones para funcion de recuperacion cobro cargo...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
    	}
		
		return listaRazones;
	}/* Fin buscarRazonesFuncionRecuperacionCobro*/

	
	
	/**
	 * Metodo que graba la transaccion de recuperacion de cobro de cargos.
	 * @return Mensaje de grabacion.
	 * @throws Mensaje de excepcion controlado.
	 * @param vmit mit.
	 * @param sidtransaccion sid de la transaccion.
	 * @param codigofuncion  codigo de funcion.
	 * @param codigorazon codigo de razon.
	 * @param sidacciontransac sid de la accion.
	 * @param codigomoneda codigo de moneda.
	 * @param montotransaccion monto nuevo de la transaccion.
	 * @param montooriginaltrx monto anterior de la transaccion.
	 * @param glosadescriptiva glosa de la transaccion.
	 * @param sidusuario sid del usuario.
	 */
	public String grabarRecuperacionCobroCargo(String vmit, Integer sidtransaccion, String codigofuncion, String codigorazon, Integer sidacciontransac, String codigomoneda, BigDecimal montotransaccion, BigDecimal montooriginaltrx, String glosadescriptiva, int sidusuario){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		
		try {
			
			log.info("Method: grabarRecuperacionCobroCargo - Parametros Entrada : " + " vmit=" + vmit + " sidtransaccion=" + sidtransaccion + " codigofuncion=" + codigofuncion + " codigorazon=" + codigorazon + " sidacciontransac=" + sidacciontransac + " codigomoneda=" + codigomoneda +  " montotransaccion=" + montotransaccion + "  montooriginaltrx=" + montooriginaltrx + " glosadescriptiva=" + glosadescriptiva + " sidusuario=" + sidusuario);
			log.info("Ejecutando SP - SP_SIG_GRABARRECCOBROCARGO");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_GRABARRECCOBROCARGO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("vmit", vmit);
			stmtProcedure.setInt("sidtransaccion", sidtransaccion);
			stmtProcedure.setString("codigofuncion", codigofuncion);
			stmtProcedure.setString("codigorazon", codigorazon);
			stmtProcedure.setInt("sidacciontransac", sidacciontransac);
			stmtProcedure.setString("codigomoneda", codigomoneda);
			stmtProcedure.setBigDecimal("montotransaccion", montotransaccion); // Se tiene que ir con punto //
			stmtProcedure.setBigDecimal("montooriginaltrx", montooriginaltrx); // Se tiene que ir con punto //
			stmtProcedure.setString("glosadescriptiva", glosadescriptiva);
			stmtProcedure.setInt("sidusuario", sidusuario);
			
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("flag", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int error = stmtProcedure.getInt("cod_error");
			int flag = stmtProcedure.getInt("flag");
			
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				 log.info("Error: No se pudo cerrar conexion a la base de datos.");
				 return "esttrx|1~MSG|Problemas al grabar recuperacion de cobro cargo...~";
			}

			if (flag==0){
				log.info("La operacion ha sido realizada con exito...");
				return "esttrx|0~MSG|La operacion ha sido realizada con exito...~";
			}else{
				log.info("Problemas al grabar recuperacion cobro cargo...");
				return "esttrx|1~MSG|Problemas al grabar recuperacion cobro cargo...~";
			}

		}catch (SQLException e){    
			
				bdOracle.cerrarStatement(stmtProcedure);
			
				if (bdOracle.cerrarConexionBD()==false){
					log.info("Error: No se pudo cerrar conexion a la base de datos.");
					return "esttrx|1~MSG|Problemas al grabar recuperacion de cobro cargo...~";
				}
			
				log.error( e.getMessage(),e );
        		return "esttrx|1~MSG|Problemas al grabar recuperacion de cobro cargo...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
    	}
		
	}/* Fin grabarRecuperacionCobroCargo*/
	
}
