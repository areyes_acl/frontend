package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import Beans.TDITipoValidacionBean;
import properties.UtilProperties;
import db.OracleBD;

public class TDITipoValidacionDAO {
    static final Logger log = Logger.getLogger(TDITipoValidacionDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    private OracleBD obd = new OracleBD();

    private String esquema;

    private ResultSet rs;

    private Connection conexion;

    /*
     * Constructor
     */
    public TDITipoValidacionDAO() {
	this.esquema = UtilProperties.getProperty("esquema");
    }

    public void setConexion() {
	this.conexion = obd.conectar();
	obd.setResultado(rs);
    }

    public Connection getConexion() {
	return conexion;
    }

    public void setConexion(Connection conexion) {
	this.conexion = conexion;
    }

    public List<TDITipoValidacionBean> findAll() {
	setConexion();
	String valor = "SELECT * FROM " + esquema + ".TBL_TDI_VALIDACION";
	rs = obd.consultar(valor);
	obd.setResultado(rs);
	
	List<TDITipoValidacionBean> lista = new ArrayList<TDITipoValidacionBean>();
	
	try {
	    log.info("Method: getListaParametros - Obteniendo resultados");

	    while (rs.next()) {
		TDITipoValidacionBean tipo = new TDITipoValidacionBean();
		tipo.setSid(Long.valueOf(rs.getInt("ID")));
		tipo.setDetalle(rs.getString("DETALLE"));
		tipo.setClasif(rs.getInt("CLASIF"));
		tipo.setValidaOD(rs.getString("VALIDA_O_D"));
		lista.add(tipo);
	    }

	    obd.cerrarResultSet();
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	}

	cerrarConexion();

	return lista;
    }
    
    private void cerrarConexion(){
	if (!obd.cerrarResultSet()) {
	    log.error("Error: No se pudo cerrar resulset.");
	    if (!obd.cerrarConexionBD()) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	} else {
	    if (!obd.cerrarConexionBD()) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	}
	try {
	    if (conexion != null && !conexion.isClosed()) {
		conexion.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}
    }

}
