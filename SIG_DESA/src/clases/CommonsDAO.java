package clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.ParametroBean;
import db.OracleBD;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class CommonsDAO {
	/** VARIABLE PARA EL LOGER */
	private static final Logger log = Logger.getLogger(CommonsDAO.class);
	private String esquema;

	public CommonsDAO() {
		try {
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();
			esquema = propiedades.getProperty("esquema");
		} catch (Exception e) {
			log.error("Error al leer el archivo de propiedades.");
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * Metodo que dado un nombre de un grupo busca en la tabla prts todos los
	 * datos que tengan el mismo grupo
	 * 
	 * @param codGrupo
	 * @return
	 * @throws SQLException
	 */
	public List<ParametroBean> obtenerParametrosPorGrupo(String codGrupo)
			throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<ParametroBean> listaPrts = null;
		Connection conexion = null;
		db.OracleBD bdOracle = null;

		try {
			StringBuilder str = new StringBuilder("SELECT ")
					.append("COD_GRUPO_DATO,  COD_DATO, VALOR, DESCRIPCION, ID_USER, FECHA_CREACION, FECHA_MODIFICACION")
					.append(" FROM ").append(esquema).append(".TBL_PRTS ")
					.append(" where COD_GRUPO_DATO = ? ");

			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);

			stmt = conexion.prepareStatement(str.toString());
			stmt.setString(1, codGrupo);
			rs = stmt.executeQuery();

			listaPrts = parseParametros(rs);
		} finally {
			cerrarConexion(conexion, stmt, rs);
		}

		return listaPrts;
	}
	
	public Integer getOperador(final String operatorName) throws SQLException {
	    PreparedStatement stmt = null; 
	    ResultSet rs = null;
	    Integer sidOperador = 0;
	    Connection conexion = null;

	    try {
	    StringBuilder str = new StringBuilder().append("select SID from ")
	    .append(esquema)
	    .append(".TBL_OPERADOR where OPERADOR = '"+operatorName+"'");

	    stmt = conexion.prepareStatement(str.toString());
	    rs = stmt.executeQuery();

	    if (rs.next()) {
		sidOperador = rs.getInt("SID");
	    }
	    return sidOperador;
	    } finally {
		if (stmt != null) {
		    stmt.close();
		}
    
		if (rs != null) {
		    rs.close();
		}

	    }
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @param code
	 * @return
	 * @since 1.X
	 */
	public List<ParametroBean> getParameterByCode(String code) {
		Connection conexion = null;
		db.OracleBD bdOracle = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<ParametroBean> listaPrts = null;

		try {
		    	log.info(code);
		    	// prepara la query
			StringBuilder str = new StringBuilder("SELECT ")
					.append("COD_GRUPO_DATO,  COD_DATO, VALOR, DESCRIPCION, ID_USER, FECHA_CREACION, FECHA_MODIFICACION")
					.append(" FROM ").append(esquema).append(".TBL_PRTS ");
			
			if(code != null && !code.isEmpty()){
			    str.append(" WHERE COD_DATO = ? ");
			}
					
			log.info(str.toString());
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);

			stmt = conexion.prepareStatement(str.toString());
			
			
			// setea parametros
			if(code != null && !code.isEmpty()){
			    stmt.setString(1, code);
			}
			
			rs = stmt.executeQuery();
			listaPrts = parseParametros(rs);
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} finally {
			cerrarConexion(conexion, stmt, rs);

		}

		return listaPrts;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versi�n inicial 
	 * </ul>
	 * <p>
	 * 
	 * @param conexion
	 * @param stmt
	 * @param rs
	 * @since 1.X
	 */
	private void cerrarConexion(Connection conexion, PreparedStatement stmt,
			ResultSet rs) {
		try {
			if (stmt != null && !stmt.isClosed()) {
				stmt.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}

		try {
			if (rs != null && !rs.isClosed()) {
				rs.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}

		try {
			if (conexion != null && !conexion.isClosed()) {
				conexion.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 * @since 1.X
	 */
	private List<ParametroBean> parseParametros(ResultSet rs)
			throws SQLException {

		List<ParametroBean> listaPrts = null;

		while (rs.next()) {
			ParametroBean parametro = new ParametroBean();
			parametro.setCodGrupo(rs.getString("COD_GRUPO_DATO"));
			parametro.setCodDato(rs.getString("COD_DATO"));
			parametro.setValor(rs.getString("VALOR"));
			parametro.setDescripcion(rs.getString("DESCRIPCION"));
			parametro.setSidUsuario(rs.getString("ID_USER"));
			parametro.setFechaCreacion(rs.getString("FECHA_CREACION"));
			parametro.setFechaModificacion(rs.getString("FECHA_MODIFICACION"));

			if (listaPrts == null) {
				listaPrts = new ArrayList<ParametroBean>();
			}
			listaPrts.add(parametro);

		}

		return listaPrts;

	}

}
