package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import cl.util.Utils;
import Beans.TDIConexionTipoBean;
import Beans.TDIJobBean;
import Beans.TDIJobValidacionBean;
import Beans.TDITipoValidacionBean;
import properties.UtilProperties;
import db.OracleBD;

public class TDIJobsDAO {
	static final Logger LOGGER =  Logger.getLogger(TDIJobsDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

	private OracleBD obd = new OracleBD();

	private int cResultados;
	
	private String esquema;

	private ResultSet rs;

	private Connection conexion;

	/*
	 * Constructor 
	 */
	public TDIJobsDAO(){
		this.esquema = UtilProperties.getProperty("esquema");
		this.cResultados = Integer.parseInt(UtilProperties.getProperty("TDICantidadRegistros"));
	}
	
	public void setConexion(){
		this.conexion = obd.conectar();
		obd.setResultado(rs);
	}
	
	public Connection getConexion(){
		return conexion;
	}
	
	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}
	
	public int getJobByProceso(String job){
	    setConexion();
	    String query = "SELECT JOB.ID FROM " +esquema+".TBL_TDI_JOB JOB WHERE JOB.PROCESO = '"+job+"' ";
		LOGGER.info("CONSULTA realizada "+ query);
		try{
		    rs = obd.consultar(query);
		}catch(Exception e){
		    LOGGER.error("Error al intentar hacer la consulta "+e);
		}
		obd.setResultado(rs);
		
		int id = 0;
		try {
		    while (rs.next()) {
			id = rs.getInt("ID");
		    }
		    obd.cerrarResultSet();
		} catch (Exception e) {
		    LOGGER.error(e);
		}
		LOGGER.info("Obtuve el id del job creado es "+id);
	    cerrarConexion();
	    return id;
	}
	
	public void createJob(TDIJobBean job) throws Exception{
	    setConexion();
	    String guardarE = "";
	    try{
		SimpleDateFormat ejecucion = new SimpleDateFormat("HH:mm");
		guardarE = ejecucion.format(job.getEjecucion());
	    }catch(Exception e){
		LOGGER.info("Problema al formatear la ejecucion para insertar "+e);
	    }
	    
	    String valor = "INSERT INTO " +esquema+".TBL_TDI_JOB (PROCESO, ORIGEN_ID, DESTINO_ID, ORIGEN_PATH, DESTINO_PATH, ORIGE_IP, DESTINO_IP, ORIGEN_PUERTO, DESTINO_PUERTO, ORIGEN_USER, ORIGEN_PASS, DESTINO_USER, DESTINO_PASS, ARCHIVO, ARCHIVO_EXT, CTL_ACTIVO, CTL_EXT, ALERTA, CADENA, REINTENTO, ESPERA_MIN, ACTIVO, EJECUCION, VARIABILIDAD_ACTIVA, VARIABILIDAD_PORCIENTO, VARIABILIDAD_LINEAS) VALUES ('"+job.getProceso()+"', '"+job.getOrigenTipo().getSid()+"', '"+job.getDestinoTipo().getSid()+"', '"+job.getOrigenPath()+"', '"+job.getDestinoPath()+"', '"+job.getOrigenIP()+"', '"+job.getDestinoIP()+"', '"+job.getOrigenPuerto()+"', '"+job.getDestinoPuerto()+"', '"+job.getOrigenUsuario()+"', '"+job.getOrigenPass()+"', '"+job.getDestinoUsuario()+"', '"+job.getDestinoPass()+"', '"+job.getArchivo()+"', '"+job.getArchivoExt()+"', '"+job.getControl()+"', '"+job.getControlExt()+"', '"+job.getNotificacion1()+"', '"+job.getNotificacion1Cadena()+"', '"+job.getReintentos()+"', '"+job.getReintentosMin()+"', '"+job.getActivo()+"', to_date('"+guardarE+"', 'hh24:mi'), '"+job.getVariabilidad()+"', '"+job.getVariabilidadPorciento()+"', '"+job.getVariabilidadLineas()+"')"; 
	    LOGGER.info("Creacion de nuevo job: "+valor);
	    obd.ejecutar(valor);
	    LOGGER.info("Se ha creado correctamente el job");
	    cerrarConexion();
	    crearValidaciones(job);
	   
	}
	
	private void cerrarConexion(){
	    if (!obd.cerrarResultSet()) {
		LOGGER.error("Error: No se pudo cerrar resulset.");
		    if (!obd.cerrarConexionBD()) {
			LOGGER.error("Error: No se pudo cerrar conexion a la base de datos.");
		    }
		} else {
		    if (!obd.cerrarConexionBD()) {
			LOGGER.error("Error: No se pudo cerrar conexion a la base de datos.");
		    }
		}
		try {
		    if (conexion != null && !conexion.isClosed()) {
			conexion.close();
		    }
		} catch (SQLException e) {
		    LOGGER.error(e.getMessage(), e);
		}
	}

	public List<TDIJobBean> findAll(Integer pageNumber, String searchArchivo) {
	    setConexion();
	    String paginadorQuery = "";
		if(pageNumber != null && pageNumber > 0){
		 // Calculo de paginado
			int inicio =  (pageNumber-1) * cResultados;
			int actualReal = inicio;
			inicio++;
			int termino = actualReal + cResultados;
		        paginadorQuery = "WHERE NUMBERR BETWEEN "+inicio+" AND "+termino;
		}
		
		String criterioQuery = "";
		if(searchArchivo.length() > 0){
		    criterioQuery = "WHERE LOWER(JOB.PROCESO) LIKE LOWER('%"+searchArchivo.trim()+"%') OR LOWER(JOB.ARCHIVO||'.'||JOB.ARCHIVO_EXT) LIKE LOWER('%"+searchArchivo.trim()+"%')";
		}
		
		/*String fechasQuery = "";
		if(horaInicio.length() > 0 && horaFin.length() > 0){
		    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
		    try{
			formatDate.parse(horaInicio);
			Date ftermino = Utils.addDays(formatDate.parse(horaFin), 1);
			fechasQuery = " AND HORA_INICIO BETWEEN to_date('"+horaInicio+"', 'dd/mm/yyyy') and to_date('"+formatDate.format(ftermino)+"', 'dd/mm/yyyy') ";
		    }catch (Exception e) {
			    LOGGER.info("Error al parsear el parametro de filtrado fechaInicio o  fechaTermino", e);
		    }
		}*/
		
	    ResultSet rs2 = null;
		String valor = "SELECT * FROM (SELECT Row_Number() OVER (ORDER BY JOB.ID) NUMBERR, JOB.ID, JOB.ARCHIVO, JOB.ARCHIVO_EXT, JOB.PROCESO, JOB.ACTIVO, (to_char(JOB.EJECUCION, 'hh24:mi')) AS EJECUCION FROM " +esquema+".TBL_TDI_JOB JOB "+criterioQuery+")" +paginadorQuery;
		LOGGER.info("CONSULTA realizada "+ valor);
		try{
		    rs = obd.consultar(valor);
		}catch(Exception e){
		    LOGGER.error("Error al intentar hacer la consulta "+e);
		}
		
		obd.setResultado(rs);

		List<TDIJobBean> lista = new ArrayList<TDIJobBean>();
		try {
		    LOGGER.info("Listado de jobs - Obteniendo resultados");

		    while (rs.next()) {
			TDIJobBean tipo = new TDIJobBean();
			tipo.setSid(rs.getLong("ID"));
			tipo.setArchivo(rs.getString("ARCHIVO"));
			tipo.setArchivoExt(rs.getString("ARCHIVO_EXT"));
			tipo.setProceso(rs.getString("PROCESO"));
			tipo.setActivo(rs.getInt("ACTIVO"));
			
			try{
			    SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
			    LOGGER.info("formato de la hora "+ rs.getString("EJECUCION"));
			    tipo.setEjecucion(formatoHora.parse(rs.getString("EJECUCION")));
			}catch(Exception e){
			    LOGGER.error("Error al intentar formatear la ejecucion "+e);
			}
			
			
			// Buscar la cantidad de logs del job
			String sql = "SELECT COUNT(LOG.ID) AS CANT_LOG FROM " +esquema+".TBL_TDI_JOB JOB INNER JOIN " +esquema+".TBL_TDI_EXECUTION_LOG LOG ON LOG.JOB_ID = JOB.ID WHERE JOB.ID = '"+tipo.getSid()+"'";
			LOGGER.info("CONSULTA para buscar la cantidad "+ sql);
			rs2 = obd.consultar(sql);
			while (rs2.next()) {
			    tipo.setCantidadDeLogs(rs2.getInt("CANT_LOG")); 
			}
			
			lista.add(tipo);
		    }

		    obd.cerrarResultSet();
		    obd.setResultado(rs2);
		    obd.cerrarResultSet();
		} catch (Exception e) {
		    LOGGER.error("Error al obtener el listado de los jobs");
		    LOGGER.error(e);
		}
		
		LOGGER.info("Cantidad de jobs "+lista.size());

		cerrarConexion();

		return lista;
	}
	
	public int getUltimaPagina(String criterio) {
		int totReg = this.findAll(null, criterio).size();
		return Utils.calcularUltimaPagina(totReg, cResultados);
	    }
	
	public TDIJobBean getById(int id){
	    setConexion();
	    ResultSet rs2 = null;
		String valor = "SELECT JOB.ID, JOB.PROCESO, JOB.ORIGE_IP, JOB.ORIGEN_PUERTO, JOB.ORIGEN_USER, JOB.ORIGEN_PASS, JOB.ORIGEN_PATH, JOB.DESTINO_IP, JOB.DESTINO_PUERTO, JOB.DESTINO_USER, JOB.DESTINO_PASS, JOB.DESTINO_PATH, JOB.ARCHIVO, JOB.ARCHIVO_EXT, JOB.CTL_EXT, JOB.REINTENTO, JOB.ESPERA_MIN, JOB.VARIABILIDAD_ACTIVA, JOB.VARIABILIDAD_PORCIENTO, JOB.VARIABILIDAD_LINEAS, JOB.ACTIVO, (to_char(JOB.EJECUCION, 'hh24:mi')) AS EJECUCION, JOB.ALERTA, JOB.CADENA, JOB.CTL_ACTIVO, CON.ID AS CONEXION_ORIGEN_ID, CON.TIPO AS CONEXION_ORIGEN_TIPO, DES.ID AS CONEXION_DESTINO_ID, DES.TIPO AS CONEXION_DESTINO_TIPO FROM " +esquema+".TBL_TDI_JOB JOB "
			+ " INNER JOIN " +esquema+".TBL_TDI_CONEXION_TIPO CON ON JOB.ORIGEN_ID = CON.ID "
			+ " INNER JOIN " +esquema+".TBL_TDI_CONEXION_TIPO DES ON JOB.DESTINO_ID = DES.ID "
			+ " WHERE JOB.ID = '"+id+"' ";
		LOGGER.info("CONSULTA realizada "+ valor);
		try{
		    rs = obd.consultar(valor);
		}catch(Exception e){
		    LOGGER.error("Error al intentar hacer la consulta "+e);
		}
		
		obd.setResultado(rs);

		TDIJobBean tipo = new TDIJobBean();
		try {
		    LOGGER.info("Listado de jobs - Obteniendo resultados");

		    while (rs.next()) {
			
			tipo.setSid(rs.getLong("ID"));
			tipo.setProceso(rs.getString("PROCESO"));
			tipo.setOrigenIP(rs.getString("ORIGE_IP"));
			tipo.setOrigenPuerto(rs.getInt("ORIGEN_PUERTO"));
			tipo.setOrigenUsuario(rs.getString("ORIGEN_USER"));
			tipo.setOrigenPass(rs.getString("ORIGEN_PASS"));
			tipo.setOrigenPath(rs.getString("ORIGEN_PATH"));
			tipo.setDestinoIP(rs.getString("DESTINO_IP"));
			tipo.setDestinoPuerto(rs.getInt("DESTINO_PUERTO"));
			tipo.setDestinoUsuario(rs.getString("DESTINO_USER"));
			tipo.setDestinoPass(rs.getString("DESTINO_PASS"));
			tipo.setDestinoPath(rs.getString("DESTINO_PATH"));
			tipo.setArchivo(rs.getString("ARCHIVO"));
			tipo.setArchivoExt(rs.getString("ARCHIVO_EXT"));
			tipo.setControlExt(rs.getString("CTL_EXT"));
			tipo.setReintentos(rs.getInt("REINTENTO"));
			tipo.setReintentosMin(rs.getInt("ESPERA_MIN"));
			tipo.setVariabilidad(rs.getInt("VARIABILIDAD_ACTIVA"));
			tipo.setVariabilidadPorciento(rs.getInt("VARIABILIDAD_PORCIENTO"));
			tipo.setVariabilidadLineas(rs.getInt("VARIABILIDAD_LINEAS"));
			tipo.setActivo(rs.getInt("ACTIVO"));
			tipo.setNotificacion1(rs.getInt("ALERTA"));
			tipo.setNotificacion1Cadena(rs.getString("CADENA"));
			tipo.setControl(rs.getInt("CTL_ACTIVO"));
			tipo.setOrigenTipo(new TDIConexionTipoBean(rs.getLong("CONEXION_ORIGEN_ID"), rs.getString("CONEXION_ORIGEN_TIPO")));
			tipo.setDestinoTipo(new TDIConexionTipoBean(rs.getLong("CONEXION_DESTINO_ID"), rs.getString("CONEXION_DESTINO_TIPO")));
			
			try{
			    SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
			    LOGGER.info("formato de la hora "+ rs.getString("EJECUCION"));
			    tipo.setEjecucion(formatoHora.parse(rs.getString("EJECUCION")));
			}catch(Exception e){
			    LOGGER.error("Error al intentar formatear la ejecucion "+e);
			}
			// Buscar las validaciones del job
			String sql = "SELECT VALJ.ID AS VALJ_ID, VAL.ID AS VAL_ID, VAL.DETALLE, VAL.CLASIF, VAL.VALIDA_O_D, VALJ.ORIGEN, VALJ.DESTINO FROM " +esquema+".TBL_TDI_JOB_VAL VALJ INNER JOIN " +esquema+".TBL_TDI_VALIDACION VAL ON VAL.ID = VALJ.VALICACION_ID WHERE VALJ.JOB_ID = '"+tipo.getSid()+"' ORDER BY VALJ.ID";
			LOGGER.info("CONSULTA para buscar la cantidad "+ sql);
			rs2 = obd.consultar(sql);
			List<TDIJobValidacionBean> validaciones = new ArrayList<TDIJobValidacionBean>();
			while (rs2.next()) {
			    TDIJobValidacionBean val = new TDIJobValidacionBean(rs2.getLong("VALJ_ID"), new TDITipoValidacionBean(rs2.getLong("VAL_ID"), rs2.getString("DETALLE"), rs2.getInt("CLASIF"), rs2.getString("VALIDA_O_D")), rs2.getString("ORIGEN"), rs2.getString("DESTINO"));
			    validaciones.add(val);
			}
			tipo.setValidaciones(validaciones);
			
			// Buscar la cantidad de logs del job
			String sqlC = "SELECT COUNT(LOG.ID) AS CANT_LOG FROM " +esquema+".TBL_TDI_JOB JOB INNER JOIN " +esquema+".TBL_TDI_EXECUTION_LOG LOG ON LOG.JOB_ID = JOB.ID WHERE JOB.ID = '"+tipo.getSid()+"'";
			LOGGER.info("CONSULTA para buscar la cantidad "+ sqlC);
			rs2 = obd.consultar(sqlC);
			while (rs2.next()) {
			tipo.setCantidadDeLogs(rs2.getInt("CANT_LOG")); 
			}
			
		    }

		    obd.cerrarResultSet();
		    obd.setResultado(rs2);
		    obd.cerrarResultSet();
		} catch (Exception e) {
		    LOGGER.error("Error al obtener el listado de los jobs");
		    LOGGER.error(e);
		}
		cerrarConexion();

		return tipo;
	}

	public void updateJob(TDIJobBean job) {
	    setConexion();
	    String guardarE = "";
	    try{
		SimpleDateFormat ejecucion = new SimpleDateFormat("HH:mm");
		guardarE = ejecucion.format(job.getEjecucion());
	    }catch(Exception e){
		LOGGER.info("Problema al formatear la ejecucion para insertar "+e);
	    }
	    
	    String valor = "UPDATE " +esquema+".TBL_TDI_JOB SET PROCESO = '"+job.getProceso()+"',  ORIGEN_ID = '"+job.getOrigenTipo().getSid()+"', DESTINO_ID = '"+job.getDestinoTipo().getSid()+"', ORIGEN_PATH = '"+job.getOrigenPath()+"', DESTINO_PATH = '"+job.getDestinoPath()+"', ORIGE_IP = '"+job.getOrigenIP()+"', DESTINO_IP = '"+job.getDestinoIP()+"', ORIGEN_PUERTO = '"+job.getOrigenPuerto()+"', DESTINO_PUERTO = '"+job.getDestinoPuerto()+"', ORIGEN_USER = '"+job.getOrigenUsuario()+"', ORIGEN_PASS = '"+job.getOrigenPass()+"', DESTINO_USER = '"+job.getDestinoUsuario()+"', DESTINO_PASS = '"+job.getDestinoPass()+"', ARCHIVO = '"+job.getArchivo()+"', ARCHIVO_EXT = '"+job.getArchivoExt()+"', CTL_ACTIVO = '"+job.getControl()+"', CTL_EXT = '"+job.getControlExt()+"', ALERTA = '"+job.getNotificacion1()+"', CADENA = '"+job.getNotificacion1Cadena()+"', REINTENTO = '"+job.getReintentos()+"', ESPERA_MIN = '"+job.getReintentosMin()+"', ACTIVO = '"+job.getActivo()+"', EJECUCION = to_date('"+guardarE+"', 'hh24:mi'), VARIABILIDAD_ACTIVA = '"+job.getVariabilidad()+"', VARIABILIDAD_PORCIENTO = '"+job.getVariabilidadPorciento()+"', VARIABILIDAD_LINEAS = '"+job.getVariabilidadLineas()+"' WHERE ID = '"+job.getSid()+"' "; 
	    LOGGER.info("Actualizacion de nuevo job: "+valor);
	    obd.ejecutar(valor);
	    LOGGER.info("Se ha actualizado correctamente el job");
	    
	    //Purgar posibles validaciones anteriores
	    String sql = "DELETE FROM  " +esquema+".TBL_TDI_JOB_VAL WHERE JOB_ID = '"+job.getSid()+"' ";
	    LOGGER.info("Purgando validaciones anteriores: "+sql);
	    obd.ejecutar(sql);
	    LOGGER.info("Se borraron las posibles validaciones anteriores");
	    cerrarConexion();
	    crearValidaciones(job);
	}
	
	public void crearValidaciones(TDIJobBean job){
	    if(!job.getValidaciones().isEmpty()){
		int id = this.getJobByProceso(job.getProceso());
			if(id != 0){
			    setConexion();
			for(int i = 0; i<job.getValidaciones().size(); i++){
			    TDIJobValidacionBean valbean = job.getValidaciones().get(i);
			    String queryVal = "INSERT INTO " +esquema+".TBL_TDI_JOB_VAL (JOB_ID, VALICACION_ID, ORIGEN, DESTINO) VALUES ('"+id+"', '"+valbean.getValidacionId().getSid()+"', '"+valbean.getOrigen()+"', '"+valbean.getDestino()+"')"; 
			    LOGGER.info("Creando las validaciones del nuevo job: "+queryVal);
			    obd.ejecutar(queryVal);
			    LOGGER.info("Se ha creado la validacion OK");
			}
			cerrarConexion();
		}
	    }
	}

	public void deleteJob(int idJobWork) {
	    setConexion();
	  //Purgar posibles validaciones anteriores
	    String sql = "DELETE FROM  " +esquema+".TBL_TDI_JOB_VAL WHERE JOB_ID = '"+idJobWork+"' ";
	    LOGGER.info("Purgando validaciones anteriores: "+sql);
	    obd.ejecutar(sql);
	    LOGGER.info("Se borraron las posibles validaciones anteriores");
	    String sql2 = "DELETE FROM  " +esquema+".TBL_TDI_JOB WHERE ID = '"+idJobWork+"' ";
	    LOGGER.info("Eliminando job: "+sql2);
	    obd.ejecutar(sql2);
	    LOGGER.info("Se elimino ok");
	    cerrarConexion();
	}
	
}
