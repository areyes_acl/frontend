/**
 * Descripcion: Clase que administra los cargos y abonos
 * @param SID de transaccion
 * @param listaCargoAbono
 * @return devuevle listaCargoAbono y updateCargoAbono
 */

package clases;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import cl.util.Utils;
import db.OracleBD;

public class CargoAbonoDAO {
	
    static final Logger log = Logger.getLogger( CargoAbonoDAO.class );
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private OracleBD obd = new OracleBD();
	private Properties prop = new Properties();
	
	private String esquema;
	
	private String listaCargoAbono;
	private String fechaTope;
	private String updateCargoAbono;
	
	private ResultSet rs;
	
	private Connection conexion;
	
	/*
	 * Constructor 
	 */
    public CargoAbonoDAO() {
		setProp();
			
		this.esquema = prop.getProperty("esquema");
	}
	
    private void setProp() {
        this.prop = PropertiesConfiguration.getProperty();
	}
	
    public void setConexion() {
		this.conexion = obd.conectar();
	}
	
    public Connection getConexion() {
		return conexion;
	}
	
    public String getListaCargoAbonoMnt() {
		setConexion();
		log.info("Method: getListaCargoAbonoMnt - Obteniendo Lista Cargo Abono");
		listaCargoAbono = "esttrx:0~";
		rs = obd.consultar("select* from "+esquema+".TBL_CARGOS_ABONOS");
		log.info("Method: getListaCargoAbonoMnt - RS CargoAbono");
		obd.setResultado(rs);
		try {
            while ( rs.next() ) {
				listaCargoAbono += rs.getInt("SID")+"|";
				listaCargoAbono += rs.getString("XKEY")+"|";
				listaCargoAbono += rs.getString("DESCRIPCION")+"|";
				listaCargoAbono += rs.getString("TIPO")+"|";
				listaCargoAbono += rs.getString("COMERCIO")+"|";
				listaCargoAbono += rs.getString("ACTIVO")+("~");
			}
			log.info("Method: getListaCargoAbonoMnt - Lista Cargos y abonos serializada");
        }
        catch ( SQLException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			listaCargoAbono = "esttrx:1~"+e.getMessage();
            log.error( e.getMessage(), e );
        }
        finally {
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}
		
			try {
				if (conexion != null && !conexion.isClosed()) {
					conexion.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
	
		}

       
		log.info("Method: getListaCargoAbonoMnt - Cerrando conexion y salir");
		return listaCargoAbono;
	}
	
    public String insertCargoAbono( String sid, String tipoCargoAbono,
            String moneda, String montoca, String sidusuario ) {
    	CallableStatement stmtProcedure = null;
        log.info( "Method: insertarCargoAbono - insertando parametros: Sid-> "
                + sid + " | tipoCargoAbono-> " + tipoCargoAbono
                + " | moneda-> " + moneda + " | monto-> " + montoca
                + " | sidusuario-> " + sidusuario );
		setConexion();
		obd.setConexion(conexion);
		
		String dev = "";
		
		BigDecimal monto = new BigDecimal(montoca);
		
		try {
			
			log.info("Method: insertarCargoAbono - Llamada SP_SIG_CARGARABONARTRANSAC");
			stmtProcedure = conexion.prepareCall( "BEGIN "
                    + esquema
                    + ".SP_SIG_CARGARABONARTRANSAC(?, ?, ?, ?, ?, ?, ?); END;" );
			
			stmtProcedure.setString("SIDin", sid);
			stmtProcedure.setString("MONEDA", moneda);
			stmtProcedure.setBigDecimal("MONTO", monto);
			stmtProcedure.setInt("sidUsuario", Integer.parseInt(sidusuario));
			
			stmtProcedure.setString("TIPO_CARGO_ABONO", tipoCargoAbono);
			
			stmtProcedure.registerOutParameter("FLAG", OracleTypes.VARCHAR);
            stmtProcedure.registerOutParameter( "ERROR_OUT",
                    OracleTypes.VARCHAR );
			
			log.info("Method: insertarCargoAbono - Ejcutando SP_SIG_CARGARABONARTRANSAC...");
			stmtProcedure.execute();
			
			String error = stmtProcedure.getString("ERROR_OUT");
			
            if ( error.equals( "0" ) ) {
				log.info("Method: insertarCargoAbono - Ejecucion SP_SIG_CARGARABONARTRANSAC: La operacion ha sido realizada con exito!");
                dev = "esttrx:0~"
                        + "La operacion ha sido realizada con exito...~";
            }
            else {
				log.error("Method: insertarCargoAbono - Ejecucion SP_SIG_CARGARABONARTRANSAC: Problemas al realizar el ingreso del cargo o abono");
                dev = "esttrx:1~"
                        + "Problemas al realizar el ingreso del cargo o abono...~";
			}
			
        }
        catch ( SQLException e ) {
			// TODO Auto-generated catch block
			dev="esttrx:1~"+e.getMessage()+"~";
			e.printStackTrace();
            log.error( e.getMessage() , e );
        }
        finally {
			if (obd.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (obd.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			} else {
				if (obd.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			}

			try {
				if (conexion != null && !conexion.isClosed()) {
					conexion.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				log.error(e.getMessage(), e);
			}

			try {
				if (stmtProcedure != null && !stmtProcedure.isClosed()) {
					stmtProcedure.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        
		
		return dev;
	}
	
    public String cambiarEstadoCargoAbono( String sid, String estado ) {
		setConexion();
        log.info( "Method: cambiarEstadoCargoAbono - Cambiando estado: sid-> "
                + sid + " | estado-> " + estado );
		updateCargoAbono = "esttrx:0~";
		
		try{
			log.info("Method: cambiarEstadoCargoAbono - Actualizando...");
            String sql = "update " + esquema
                    + ".TBL_CARGOS_ABONOS set ACTIVO = " + estado
                    + " where SID = " + sid;

			obd.ejecutar(sql);
        }
        catch ( Exception e ) {
			updateCargoAbono = "esttrx:1~"+e.getMessage();
            log.error( e.getMessage() , e);
        }
        finally {
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
            }
            else {
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
		
            try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
		}
            catch ( SQLException e ) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                log.error( e.getMessage() , e );
            }
        }
       
		return updateCargoAbono;
	}
	
	public String getUpdateCargoAbono() {
		return updateCargoAbono;
	}

	public void setUpdateCargoAbono(String updateCargoAbono) {
		this.updateCargoAbono = updateCargoAbono;
	}

    public String getFechaTope( String sid, String codRazon ) {
		setConexion();
		obd.setConexion(conexion);
		
        log.debug( " ==== Se Calcula la Fecha tope ==== " );
		
		String consulta="";
		
        consulta += "select to_char(sysdate+plazo,'DD/MM/YYYY') as fecha_tope from "
                + esquema
                + ".TBL_TRANSAC_POR_NRO_RAZON where ACCION_TRANSAC = (";
		consulta += "select SID from TBL_ACCION_TRANSAC where XKEY = (";
        consulta += "select MIT||'_'||CODIGO_FUNCION from " + esquema
                + ".TBL_TRANSACCIONES where SID = " + sid + ")";
		consulta += ") and COD_MOTIVO_INI = "+codRazon;
		
        log.debug( "Consulta: " + consulta );
		
        log.debug( "Calculo fecha tope --> RS" );
		rs = obd.consultar(consulta);
		
		try {
            if ( rs.next() ) {
				fechaTope = "esttrx:0~";
				fechaTope += rs.getString("FECHA_TOPE");
                log.info( "Existe fecha Tope : " +  fechaTope  );
            }
            else {
                log.info( "No existe fecha tope" );
				fechaTope = "esttrx:1~Fecha tope no ha podido ser calculada.";
			}
			
            
        }
        catch ( SQLException e ) {
			fechaTope = "esttrx:1~"+e.getMessage();
            log.error( e.getMessage(), e );
        }
        finally {
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
            }
            else {
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
		
            try {
                if( conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                // TODO Auto-generated catch block
                log.error( e.getMessage(), e );
		}
		}
        
       
		return fechaTope;
	}

    public String getListaCargoAbono() {
		setConexion();
		log.info("Method: getListaCargoAbono - Obteniendo lista cargo/abono");
		listaCargoAbono = "esttrx:0~";
        rs = obd.consultar( "select* from " + esquema
                + ".TBL_CARGOS_ABONOS where activo = 1" );
		obd.setResultado(rs);
		try {
			log.info("Method: getListaCargoAbono - Recorriendo resultSet");
            while ( rs.next() ) {
				listaCargoAbono += rs.getInt("SID")+"|";
				listaCargoAbono += rs.getString("XKEY")+"|";
				listaCargoAbono += rs.getString("DESCRIPCION")+"|";
				listaCargoAbono += rs.getString("TIPO")+"|";
				listaCargoAbono += rs.getString("COMERCIO")+"|";
				listaCargoAbono += rs.getString("ACTIVO")+("~");
			}
			log.info("Method: getListaCargoAbono - Lista Cargos y abonos serializada");
			log.info("Method: getListaCargoAbono - "+listaCargoAbono);
        }
        catch ( SQLException e ) {
			e.printStackTrace();
            log.error( e.getMessage(), e);
        }
        finally {
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
            }
            else {
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
		
            try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
		}
		}
            catch ( SQLException e ) {
                log.error( e.getMessage(), e);
            }
        }
        
		return listaCargoAbono;
	}
    
    
    /**
     * Metodo que retorna la sumatoria de los cargos de los abonos 
     * de acuerdo a un sid ingresado
     * @return
     */
    public HashMap<String, String> obtenerSumatoriaCargoAbonosHistoricos(String sid){
    	
    	HashMap<String,String> sumatorias = new HashMap<String,String>();
    	try {
    		log.info("Method: obtenerSumatoriaCargoAbonosHistoricos - Ejecutando SP_SIG_GET_SUM_CARG_ABO...");
    		log.info("Parametros:  - Sid <=> sidTrx =  "+sid );
    		setConexion();
    		obd.setConexion(conexion);    		
    		CallableStatement stmtProcedure = conexion.prepareCall( "BEGIN "+ esquema + ".SP_SIG_GET_SUM_CARG_ABO(?, ?, ?, ?, ?); END;" );
			stmtProcedure.setString("sidTrx", sid);
			stmtProcedure.registerOutParameter("SUMA_CARGOS", OracleTypes.VARCHAR);
            stmtProcedure.registerOutParameter( "SUMA_ABONOS", OracleTypes.VARCHAR );
			stmtProcedure.registerOutParameter("CODIGO_ERROR", OracleTypes.VARCHAR);
            stmtProcedure.registerOutParameter( "WARNING", OracleTypes.VARCHAR );
			
			stmtProcedure.execute();
			
			
			Integer codError = stmtProcedure.getInt("CODIGO_ERROR");
			String mensaje = stmtProcedure.getString("WARNING");
			String sumatoria_cargos = stmtProcedure.getString(Utils.SUMA_CARGO_MAP_NAME);
			String sumatoria_abonos = stmtProcedure.getString(Utils.SUMA_ABONO_MAP_NAME);
			
			if(codError == 0){
				sumatorias.put("SUMA_CARGOS", sumatoria_cargos);
				sumatorias.put("SUMA_ABONOS", sumatoria_abonos);
			}else{
				sumatorias.put("SUMA_CARGOS", null);
				sumatorias.put("SUMA_ABONOS", null);
			}
			
			
			log.info("==== RESULTADO EJECUCION SP_SIG_CARGARABONARTRANSAC ==== ");
			log.info("Codigo error: " + codError);
			log.info("Mensaje error: " + mensaje);
			log.info("sumatoria_cargos: " + sumatoria_cargos);
			log.info("sumatoria_abonos: " + sumatoria_abonos);
			
			
        }catch (SQLException e) {
        	log.error(e.getMessage(), e);
		}finally{
			
			if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
           }
           else {
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
			
		}
    	    	
    	return sumatorias;
    	
    	
    }
    
}
