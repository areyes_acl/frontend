package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.AsientoContable;
import Beans.ParametroBean;
import Beans.TDIExecutionLogBean;
import Beans.TDIJobBean;
import Beans.solicitudBean;
import db.OracleBD;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class AsientosDAO {
    static final Logger log = Logger.getLogger(AsientosDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    private String esquema;

    private OracleBD obd = new OracleBD();
    private Connection conexion;

    private ResultSet rs;
    private ResultSet rs2;
    private ResultSet rs3;
    private ResultSet rs4;
    private ResultSet rs5;
    
    
    public void setConexion() {
	this.conexion = obd.conectar();
	obd.setResultado(rs);
    }
    
    public Connection getConexion(){
	return conexion;
    }
    public void setConexion(Connection conexion) {
	this.conexion = conexion;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public AsientosDAO() {
	try {
	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    esquema = propiedades.getProperty("esquema");
	} catch (Exception e) {
	    log.error("Error al leer el archivo de propiedades.");
	    log.error(e.getMessage(), e);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws SQLException
     * @since 1.X
     */
    public List<AsientoContable> getListaTipoMovimientos(int operador, int producto) throws SQLException {
	log.info(" Valor que llega de operador = "+operador);
	log.info(" Valor que llega de producto = "+producto);
	String strsql = "SELECT DISTINCT ASI.TIPO_MOV, ASI.SID, ASI.AGRUPA, ASI.CODIGO_FUNCION, ASI.CONCEPTO, ASI.DEBE, ASI.HABER, ASI.ESTADO, ASI.MIT, ASI.OPE_PROD FROM "
		+ esquema
		+ ".TBL_ASIENTOS ASI INNER JOIN "+esquema+".TBL_OPERADOR_PRODUCTO OP ON ASI.OPE_PROD = OP.SID WHERE ASI.ESTADO = 1 AND OP.ID_OPERADOR = "+operador+" AND OP.ID_PRODUCTO = "+producto+" ORDER BY ASI.TIPO_MOV ASC";
	log.info(" Consulta a realizar = "+strsql);
	List<AsientoContable> listaAsientos = null;

	try {
	    setConexion();
	    log.info(" ===== Se ejecuta el procedimiento : detalleContableDiarioIndex =====");

	    rs = obd.consultar(strsql);
	    obd.setResultado(rs);

	    listaAsientos = mapearResulset();
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw e;
	} finally {
	    if (obd.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}
	return listaAsientos;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws SQLException
     * @since 1.X
     */
    public String solicitarRegistrosContables(String fechaInicio, String fechaFin, String tipoMov, int operador, int producto, Long sidUsuario, String fileName) throws SQLException {
	setConexion();
	
	String sql3 = "";
	String sql4 = "";
	String mensaje = "";
	
	try {
	    sql3 += "DELETE FROM SIG.TBL_EXPDC_SOL WHERE ID_USUARIO = " + sidUsuario;
	    log.info("=============se borran las solicitudes solicitudes del usuario");
	    log.info(" Consulta a realizar = "+sql3);
	    obd.ejecutar(sql3);
	} finally {
	    if (obd.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}
	
	setConexion();
	
		    try {
			 sql4 += "INSERT INTO SIG.TBL_EXPDC_SOL VALUES " + 
				    "(SIG.SEQ_TBL_EXPDC_SOL.NEXTVAL, TO_DATE('"+fechaInicio+"', 'DD-MM-YY'), TO_DATE('"+fechaFin+"', 'DD-MM-YY'), "+sidUsuario+",  '"+tipoMov+"', " + 
				    "1, SYSTIMESTAMP, '"+fileName+"', (SELECT SID FROM SIG.TBL_OPERADOR_PRODUCTO WHERE ID_PRODUCTO = "+producto+" AND ID_OPERADOR = "+operador+"), NULL, NULL)";
				    log.info("=============Se crea la nueva solicitud");
				    log.info(" Consulta a realizar = "+sql4);
				    obd.ejecutar(sql4);
			} finally {
			    if (obd.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (obd.cerrarConexionBD() == false) {
				    log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			    } else {
				if (obd.cerrarConexionBD() == false) {
				    log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			    }
			    try {
				if (conexion != null && !conexion.isClosed()) {
				    conexion.close();
				}
			    } catch (SQLException e) {
				log.error(e.getMessage(), e);
			    }
			}
	return mensaje;
    }
    
    
    
    public ParametroBean getRuta(){
	setConexion();
	String sql =  "SELECT VALOR AS RUTA FROM TBL_PRTS where COD_DATO = 'PATH_TRX_GEN_REPORTE_CONTABLE'";
	log.info("CONSULTA realizada "+ sql);
	rs = obd.consultar(sql);
	obd.setResultado(rs);
	
	ParametroBean prts = new ParametroBean();
	
	try {
	    log.info("Listado de logs - Obteniendo resultados");

	    while (rs.next()) {
		prts.setValor(rs.getString("RUTA"));
		log.info(rs.getString("RUTA"));
	    }

	    obd.cerrarResultSet();
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    if (obd.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}
	
	
	return prts;
    }
    
    public solicitudBean getById(int id) {
        setConexion();
	String sql =  "SELECT TEE.DESCRIPCION AS ESTADO, NVL(TES.TOTAL_REGISTROS, 0) AS TOTAL FROM TBL_EXPDC_SOL TES LEFT JOIN TBL_EXPDC_EST TEE ON(TES.ID_ESTADO = TEE.SID) WHERE TES.SID = " + id;
	log.info("CONSULTA realizada "+ sql);
	rs = obd.consultar(sql);
	obd.setResultado(rs);

	solicitudBean solicitud = new solicitudBean();

	try {
	    log.info("Obteniendo resultados de consulta de estado de solicitud ");

	    while (rs.next()) {
		solicitud.setEstado(rs.getString("ESTADO"));
		solicitud.setTotal(rs.getInt("TOTAL"));
	    }

	    obd.cerrarResultSet();
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    if (obd.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return solicitud;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws SQLException
     * @since 1.X
     */
    public List<solicitudBean> getSolicitudes(Long sidUsuario) throws SQLException {
	
	List<solicitudBean> listaSolicitudes = new ArrayList<solicitudBean>();
	String sql = "";
	//Consulta para traer las solicitudes del usuario logueado
	
	 sql += "SELECT TES.SID AS SID, TO_CHAR(TES.FECHA_INI, 'DD/MM/YYYY') AS INICIO, TO_CHAR(TES.FECHA_FIN, 'DD/MM/YYYY') AS FIN, USR.NOMBRE AS USUARIO, TES.TIPO_MOV AS TIPO_MOV, " + 
		"TEE.DESCRIPCION AS ESTADO, TO_CHAR(TES.MOMENTO, 'DD-MM-YY HH:MI:SS AM') AS MOMENTO, TES.FICHERO AS FICHERO, OPR.NOMBRE AS OPERADOR, TPR.BIN AS BIN, NVL(TES.TOTAL_REGISTROS, 0) AS TOTAL  " +
		"FROM TBL_EXPDC_SOL TES  " +
		"LEFT JOIN TBL_EXPDC_EST TEE ON (TES.ID_ESTADO = TEE.SID) " + 
		"LEFT JOIN TBL_USUARIO_IG USR ON (TES.ID_USUARIO = USR.SID) " +
		"LEFT JOIN TBL_OPERADOR_PRODUCTO TOP ON (TES.ID_OPE_PROD = TOP.SID) " +
		"LEFT JOIN TBL_OPERADOR OPR ON (TOP.ID_OPERADOR = OPR.SID) " +
		"LEFT JOIN TBL_PRODUCTO TPR ON (TOP.ID_PRODUCTO = TPR.SID) " +
		"WHERE TES.ID_USUARIO =" + sidUsuario;
	log.info(" Consulta a realizar = "+sql);
	
	setConexion();
	  

	rs = obd.consultar(sql);
	obd.setResultado(rs);

	try {
	    log.info(" ===== Se buscan las solicitudes del usuario logueado =====");
	    
	    if(rs.next()){
		rs.previous();
		while(rs.next()){
		    
		  solicitudBean solicitud = new solicitudBean();  
		  solicitud.setSid(rs.getInt("SID"));  
		  solicitud.setFechaInicio(rs.getString("INICIO"));
		  solicitud.setFechaFin(rs.getString("FIN"));
		  solicitud.setUsuario(rs.getString("USUARIO"));
		  solicitud.setTipoMov(rs.getString("TIPO_MOV"));
		  solicitud.setEstado(rs.getString("ESTADO"));
		  solicitud.setMomento(rs.getString("MOMENTO"));
		  solicitud.setFichero(rs.getString("FICHERO"));
		  solicitud.setOperador(rs.getString("OPERADOR"));
		  solicitud.setBin(rs.getString("BIN"));
		  solicitud.setTotal(rs.getInt("TOTAL"));
		  
		  listaSolicitudes.add(solicitud);
		  
		}
		
	    }
	    
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw e;
	} finally {
	    if (obd.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (obd.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}
	log.info(listaSolicitudes);
	return listaSolicitudes;
    }
    
    

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private List<AsientoContable> mapearResulset() throws SQLException {
	List<AsientoContable> listaAsientos = new ArrayList<AsientoContable>();

	while (rs.next()) {
	    AsientoContable asientoTemp = new AsientoContable();
	    asientoTemp.setSid(rs.getLong("SID"));
	    asientoTemp.setAgrupa(rs.getString("AGRUPA"));
	    asientoTemp.setCodigoFunction(rs.getString("CODIGO_FUNCION"));
	    asientoTemp.setConcepto(rs.getString("CONCEPTO"));
	    asientoTemp.setDebe(rs.getString("DEBE"));
	    asientoTemp.setHaber(rs.getString("HABER"));
	    asientoTemp.setEstado(rs.getString("ESTADO"));
	    asientoTemp.setMit(rs.getString("MIT"));
	    asientoTemp.setTipoMov(rs.getString("TIPO_MOV"));
	    listaAsientos.add(asientoTemp);
	}

	return listaAsientos;
    }

}
