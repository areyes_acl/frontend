package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import constant.DatabaseConstant;

import properties.PropertiesConfiguration;
import db.OracleBD;

/**
 * Clase DAO que administra los usuarios del sistema.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */

public class UsuariosDAO {

	 static final Logger log =  Logger.getLogger(UsuariosDAO.class);
	 static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	 private String esquema;
	 
	 public UsuariosDAO(){
			try {
				Properties propiedades = new Properties();
//				propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
				propiedades = PropertiesConfiguration.getProperty();
			    esquema = propiedades.getProperty("esquema");
			    
			}catch (Exception e){          
				    log.error("Error al leer el archivo de propiedades.");
				    log.error(e.getMessage(), e);
	    	}
	}
	 
	/**
	 * Metodo que ingresa un nuevo usuario.
	 * @return  true - false.
	 * @throws Mensaje de excepcion controlado.
	 * @param idusuario id del usuario.
	 * @param nombreusuario nombres del usuario.
	 * @param apellidosusuario apellidos del usuario.
	 * @param claveusuario clave del usuario.
	 * @param estadousuario estado del usuario.
	 * @param rolusuario rol del usuario.
	 */
	public boolean grabarUsuarios(String idusuario, String nombreusuario, String apellidosusuario, String claveusuario, int estadousuario, String rolusuario){
		
		EncriptacionClave encriptacionClave = new EncriptacionClave();
		String claveUsuarioEncriptada = encriptacionClave.getStringMessageDigest(claveusuario, encriptacionClave.SHA256);
		
		log.info("Method: grabarUsuarios - Parametros Entrada : " + " idusuario=" + idusuario + " nombreusuario=" + nombreusuario + " apellidosusuario=" + apellidosusuario + " claveusuario=" + claveusuario + " estadousuario=" + estadousuario + " rolusuario=" + rolusuario);
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		ResultSet resultados = null;

		/******************************************/
		/* Proceso de ingreso de usuario a la BD  */
		/******************************************/
		try {
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			String strSql = "INSERT INTO " + esquema +"."+DatabaseConstant.TABLA_USUARIO+" (SID, ID_USUARIO, NOMBRE, APELLIDO, CLAVE, ACTIVO)";
			strSql = strSql + " VALUES (" + esquema + ".SEQ_USUARIO_IG.nextval" + ",";
			strSql = strSql + "'" + idusuario + "','" +  nombreusuario + "','" + apellidosusuario + "','" + claveUsuarioEncriptada + "'," + estadousuario;
			strSql = strSql + ")";
			
			//log.info("Ejecutando SQL = " + strSql);
			
			boolean esttrx = bdOracle.ejecutar(strSql);
			
			if (esttrx == false){
				log.info("Error: No se pudo ejecutar insercion de registro USUARIO_IG.");
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
				return false;
			}
			
			/*******************************************************************/
			/* Busco valor de los SID del usuario insertado y del rol asignado */
			/*******************************************************************/
			int sidUsuarioIg = 0;
			int sidRol = 0;
			strSql = "SELECT SID FROM " + esquema +"."+DatabaseConstant.TABLA_USUARIO+" WHERE ID_USUARIO = '" + idusuario + "'";
			//log.info("Ejecutando SQL : " + strSql);
			resultados = bdOracle.consultar(strSql);
			
			bdOracle.setResultado(resultados);
			
			if (resultados.next()){
				sidUsuarioIg = resultados.getInt("SID");
			}

			resultados = null;
			strSql = "SELECT SID FROM " + esquema +"."+DatabaseConstant.TABLA_ROL+" WHERE XKEY = '" + rolusuario + "'";
			//log.info("Ejecutando SQL : " + strSql);
			resultados = bdOracle.consultar(strSql);
			
			bdOracle.setResultado(resultados);
			
			if (resultados.next()){
				sidRol = resultados.getInt("SID");
			}
			
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 bdOracle.rollback();
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
				return false;
			}
			/*******************************************/
			/* Fin                                     */
			/*******************************************/
			esttrx = false;
			strSql = "INSERT INTO " + esquema +".TBL_USUARIO_ROL VALUES (" + esquema + ".SEQ_USUARIO_ROL.nextval, " + sidUsuarioIg + "," + sidRol + ")";
			//log.info("Ejecutando SQL : " + strSql);
			esttrx = bdOracle.ejecutar(strSql);
			
			if (esttrx == false){
				log.info("Error: No se pudo ejecutar insercion de registro USUARIO_ROL.");
				/********************************************/
				/* ROLBACK USUARIO_IG                      */
				/********************************************/
				bdOracle.rollback();
				/********************************************/
				/* Fin ROLBACK                              */
				/********************************************/
				
				return false;
			}
			
			return true;
		}
		catch (Exception e){ 
			
			bdOracle.setResultado(resultados);			
			log.error(e.getMessage(), e);
     		return false;
    	
		}finally{
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
		}
	}
	/***************************************/
	/* Fin grabarUsuarios                  */     
	/***************************************/
	
	/**
	 * Metodo que obtiene los usuarios disponibles.
	 * @return Lista de usuarios.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String cargarUsuarios(){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		ResultSet resultados = null;
		
		try {
			
			log.info("Method: cargarUsuarios ");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			String strSql = "SELECT U.SID AS SIDUSUARIO, U.ID_USUARIO, U.NOMBRE, U.APELLIDO, U.CLAVE, U.ACTIVO, R.SID AS SIDUSUARIOROL, R.ROL AS SIDROLUSUARIOROL, RO.XKEY, RO.DESCRIPCION AS DESCRIPCION";
			strSql = strSql + " FROM " + esquema +"."+DatabaseConstant.TABLA_USUARIO+" U, " + esquema +".TBL_USUARIO_ROL R, " + esquema +"."+DatabaseConstant.TABLA_ROL+" RO";
			strSql = strSql + " WHERE U.SID = R.USUARIO_IG AND";
			strSql = strSql + " R.ROL = RO.SID";
			
			//log.info("Ejecutando SQL : " + strSql);
			resultados = bdOracle.consultar(strSql);
			
			bdOracle.setResultado(resultados);
			
			String listaUsuarios="esttrx:0~";
			while (resultados.next()){
				
				listaUsuarios = listaUsuarios + resultados.getInt("SIDUSUARIO") + "|";
				listaUsuarios = listaUsuarios + resultados.getString("ID_USUARIO") + "|";
				listaUsuarios = listaUsuarios + resultados.getString("NOMBRE") + "|";
				listaUsuarios = listaUsuarios + resultados.getString("APELLIDO") + "|";
				listaUsuarios = listaUsuarios + resultados.getString("CLAVE") + "|";
				listaUsuarios = listaUsuarios + resultados.getInt("ACTIVO") + "|";
				listaUsuarios = listaUsuarios + resultados.getInt("SIDUSUARIOROL") + "|";
				listaUsuarios = listaUsuarios + resultados.getInt("SIDROLUSUARIOROL") + "|";
				listaUsuarios = listaUsuarios + resultados.getString("XKEY") + "|";
				listaUsuarios = listaUsuarios + resultados.getString("DESCRIPCION") + "~";
				
				String strprint = "";
				strprint = resultados.getInt("SIDUSUARIO") + " " + resultados.getString("ID_USUARIO") + " ";
				strprint = strprint + resultados.getString("NOMBRE") + " " + resultados.getString("APELLIDO") + " ";
				strprint = strprint + resultados.getString("CLAVE") + " ";
				strprint = strprint + resultados.getInt("ACTIVO") + " " +  resultados.getInt("SIDUSUARIOROL") + " ";
				strprint = strprint + resultados.getInt("SIDROLUSUARIOROL")  + " " + resultados.getString("XKEY") + " ";
				strprint = strprint + resultados.getString("DESCRIPCION");
				
				log.info("Rescatando registro : " + strprint);
	        }
			
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error No se pudo cerrar resulset.");
				 return "esttrx:1~MSG:Problemas al leer los usuarios desde la base de datos...~";
			}
		
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 return "esttrx:1~MSG:Problemas al leer los usuarios desde la base de datos...~";
			}
			return listaUsuarios;
		}
		catch (SQLException e){  
			
				bdOracle.setResultado(resultados);
				if (bdOracle.cerrarResultSet()==false){
				 log.error("Error No se pudo cerrar resulset.");
				 return "esttrx:1~MSG:Problemas al leer los usuarios desde la base de datos...~";
				}
		
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					return "esttrx:1~MSG:Problemas al leer los usuarios desde la base de datos...~";
				}
			
				log.error(e.getMessage(), e);
        		return "esttrx:1~MSG:Problemas al leer los usuarios desde la base de datos...~";
		}finally{
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
		}
	}/* FIN */
	
	
	/**
	 * Metodo que actualiza un nuevo usuario.
	 * @return  true - false.
	 * @throws Mensaje de excepcion controlado.
	 * @param idusuario id del usuario.
	 * @param nombreusuario nombres del usuario.
	 * @param apellidosusuario apellidos del usuario.
	 * @param claveusuario clave del usuario.
	 * @param estadousuario estado del usuario.
	 * @param rolusuario rol del usuario.
	 */
	public boolean actualizarUsuarios(String idusuario, String nombreusuario, String apellidosusuario, String claveusuario, int estadousuario, String rolusuario, int sidusuariorol){
		
		log.info("Method: actualizarUsuarios - Parametros Entrada : " + " idusuario=" + idusuario + " nombreusuario=" + nombreusuario + " apellidosusuario=" + apellidosusuario + " claveusuario=" + claveusuario + " estadousuario=" + estadousuario + " rolusuario=" + rolusuario + " sidusuariorol=" + sidusuariorol);
		
		EncriptacionClave encriptacionClave = new EncriptacionClave();
		String claveUsuarioEncriptada = "";
		if (claveusuario.length() > 10){ //Biene la clave encriptada no cambia
				claveUsuarioEncriptada = claveusuario;
		}else{
				claveUsuarioEncriptada = encriptacionClave.getStringMessageDigest(claveusuario, encriptacionClave.SHA256);
		}
		
		/***********************************************/
		/* Proceso de actualizar datos                 */
		/***********************************************/
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		ResultSet resultados = null;
		
		try {
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			/**********************************************/
			/* Actualizacion datos tabla usuario_ig       */
			/**********************************************/
			String strsql = "UPDATE " + esquema +"."+DatabaseConstant.TABLA_USUARIO+" SET NOMBRE = '" + nombreusuario + "',";
            strsql = strsql + " APELLIDO = '" + apellidosusuario + "',";
            strsql = strsql + " CLAVE    = '" + claveUsuarioEncriptada + "',";
            strsql = strsql + " ACTIVO   = " + estadousuario;
            strsql = strsql + " WHERE ID_USUARIO = '" + idusuario + "'";

			//log.info("Ejecutando SQL : " + strsql);
			boolean esttrx = bdOracle.ejecutar(strsql);
			
			if (esttrx == false){
				log.info("Error: No se pudo ejecutar la actualizacion de registro USUARIO_IG.");
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
				return false;
			}
			
			
			/******************************************/
			/* Busco el sid asociado al rol ingresado */
			/******************************************/
			int sidRol = 0;
			strsql = "SELECT SID FROM " + esquema +"."+DatabaseConstant.TABLA_ROL+" WHERE XKEY = '" + rolusuario + "'";
			//log.info("Ejecutando SQL : " + strsql);
			resultados = bdOracle.consultar(strsql);
			
			bdOracle.setResultado(resultados);
			
			if (resultados.next()){
				sidRol = resultados.getInt("SID");
			}
			
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 bdOracle.rollback();
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
				return false;
			}
			
			/**********************************************/
			/* Actualizacion datos tabla usaurio_rol      */
			/**********************************************/
            strsql = "UPDATE " + esquema +".TBL_USUARIO_ROL SET ROL = " + sidRol;
            strsql = strsql + " WHERE SID = " + sidusuariorol;
			//log.info("Ejecutando SQL : " + strsql);
			esttrx = bdOracle.ejecutar(strsql);
			
			if (esttrx == false){
				bdOracle.rollback();
				log.info("Error: No se pudo ejecutar la actualizacion del registro USUARIO_ROL.");
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
				return false;
			}
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 return false;
			}
			
			return true;
		}
		catch (Exception e){  
			
			bdOracle.setResultado(resultados);
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 bdOracle.rollback();
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
				return false;
			}
			
			log.error(e.getMessage(), e);
     		return false;
		}finally{
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
		}
		
	}//Fin actualizacion
	
	
	/**
	 * Metodo que consulta la existencia de un usuario.
	 * @return  true - false.
	 * @throws Mensaje de excepcion controlado.
	 * @param idusuario id del usuario.
	 */
	public boolean buscarUsuario(String idusuario){
		/***********************************************/
		/* Proceso de actualizar datos                 */
		/***********************************************/
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		ResultSet resultados = null;
		
		try {
			
			log.info("Method: buscarUsuario - Parametros Entrada : " + " idusuario=" + idusuario);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			/******************************************/
			/* Busco el sid asociado al rol ingresado */
			/******************************************/
			int sidusuario = 0;
			String strsql = "SELECT SID FROM " + esquema +"."+DatabaseConstant.TABLA_USUARIO+" WHERE ID_USUARIO = '" + idusuario + "'";
			//log.info("Ejecutando SQL : " + strsql);
			resultados = bdOracle.consultar(strsql);
			
			bdOracle.setResultado(resultados);
			
			if (resultados.next()){
				sidusuario = resultados.getInt("SID");
				log.info("Obteniedo datos = " + resultados.getInt("SID"));
			}
			
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
				return false;
			}
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 return false;
			}
			if (sidusuario ==0){
				return false; /* No existe el usuario */
			}
			return true;
		}
		catch (Exception e){   

			bdOracle.setResultado(resultados);
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
				return false;
			}
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 return false;
			}
			
			log.error(e.getMessage(), e);
     		return false;
    	}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    	}
	}/* FIN */
	
	
	/**
	 * Metodo que obtiene los roles.
	 * @return  Lista de roles.
	 * @throws Mensaje de excepcion controlado.
	 * @param.
	 */
	public String cargarRoles (){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		ResultSet resultados = null;
		
		
		try {
			
			log.info("Method: cargarRoles");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			//log.info("Ejecutando SQL : " + "SELECT XKEY, DESCRIPCION FROM " + esquema +"."+DatabaseConstant.TABLA_ROL);
			
			resultados = bdOracle.consultar("SELECT XKEY, DESCRIPCION FROM " + esquema +"."+DatabaseConstant.TABLA_ROL);
			
			bdOracle.setResultado(resultados);
			
			String listaRoles="esttrx:0~";
			while (resultados.next()){
				listaRoles = listaRoles + resultados.getString("XKEY") + ":" + resultados.getString("DESCRIPCION") + "~";
				log.info("Rescatando registro : " + resultados.getString("XKEY") + ":" + resultados.getString("DESCRIPCION"));
	        }
			
			if (bdOracle.cerrarResultSet()==false){
				 log.error("Error No se pudo cerrar resulset.");
				 return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
			}
		
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
			}
			return listaRoles;
		}
		catch (SQLException e){     
			
				bdOracle.setResultado(resultados);
				if (bdOracle.cerrarResultSet()==false){
					log.error("Error No se pudo cerrar resulset.");
					return "esttrx:1~MSG:Error No se pudo cerrar resulset.~";
				}
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
				}
			
				log.error(e.getMessage(), e);
        		return "esttrx:1~MSG:Error No se pudo cerrar conexion a la base de datos.~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    	}
	
	}/* FIN */
	
}
