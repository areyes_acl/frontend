package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

/**
 * Clase DAO que consulta cuadratura para generacion de los log de procesos.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class CuadraturaLogProcesoDAO {
	
	static final Category log = Logger.getLogger(CuadraturaLogProcesoDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private String esquema;
	
	public CuadraturaLogProcesoDAO(){
		try {
			Properties propiedades = new Properties();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
			    log.error("Error al leer el archivo de propiedades.");
			    log.error( e.getMessage(), e );
    	}
	}
	
	/**
	 * Metodo que consulta las cuadraturas de carga de incoming.
	 * @return Lista de cuadraturas incoming.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso fecha de consulta de informacion.
	 */
	public String buscarCuadraturaLogProcesoIncoming(String fechaProceso, int operador){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados=null;
		
		String listaCuadraturaIncoming="";
		
		try {
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarCuadraturaLogProcesoIncoming - Obteniendo parametros: fechaProceso-> "+fechaProceso);
			log.info("Method: buscarCuadraturaLogProcesoIncoming - Ejecutando SP_SIG_CUADRATURALOGPROCESOINC");
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_CUADRATURALOGPROCESOINC(?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaProceso", fechaProceso);
			stmtProcedure.setInt("filtro_operador", operador);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("cantidadIncoming", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError      = stmtProcedure.getInt("cod_error");
			int cantidadIncoming = stmtProcedure.getInt("cantidadIncoming");
			
			if (codigoError==0){
				
				log.info("Method: buscarCuadraturaLogProcesoIncoming - Recorriendo resultados");
				if (resultados.next()){
					listaCuadraturaIncoming="esttrx|0~";
					listaCuadraturaIncoming += cantidadIncoming + "|";
					listaCuadraturaIncoming += resultados.getInt("SID") + "|";
					listaCuadraturaIncoming += resultados.getString("DESCRIPCION") + "|";
					listaCuadraturaIncoming += resultados.getString("NUMEROTARJETA") + "|";
					listaCuadraturaIncoming += resultados.getString("MIT") + "|";
					listaCuadraturaIncoming += resultados.getString("CODIGOFUNCION") + "|";
					listaCuadraturaIncoming += resultados.getString("CODIGORAZON") + "|";
					listaCuadraturaIncoming += resultados.getBigDecimal("MONTOTRANSACCION") + "|"; 
					listaCuadraturaIncoming += resultados.getBigDecimal("MONTOCONCILIACION") + "|"; 
					listaCuadraturaIncoming += resultados.getBigDecimal("MONTOFACTURACION") + "~";
					
					while (resultados.next()) {
						
						listaCuadraturaIncoming += cantidadIncoming + "|";
						listaCuadraturaIncoming += resultados.getInt("SID") + "|";
						listaCuadraturaIncoming += resultados.getString("DESCRIPCION") + "|";
						listaCuadraturaIncoming += resultados.getString("NUMEROTARJETA") + "|";
						listaCuadraturaIncoming += resultados.getString("MIT") + "|";
						listaCuadraturaIncoming += resultados.getString("CODIGOFUNCION") + "|";
						listaCuadraturaIncoming += resultados.getString("CODIGORAZON") + "|";
						listaCuadraturaIncoming += resultados.getBigDecimal("MONTOTRANSACCION") + "|"; 
						listaCuadraturaIncoming += resultados.getBigDecimal("MONTOCONCILIACION") + "|"; 
						listaCuadraturaIncoming += resultados.getBigDecimal("MONTOFACTURACION") + "~";
						
					}
					
				}else{
					if (cantidadIncoming!=0){
						listaCuadraturaIncoming  = "esttrx|2~MSG|" + cantidadIncoming + "~";
					}
					else{
						//listaCuadraturaIncoming="esttrx|1~MSG|No existe informacion con los parametros seleccionados para cuadratura de incoming.~";
						listaCuadraturaIncoming  = "esttrx|2~MSG|0" + "~";
						
					}
				}
				
			}else{
				log.error("Method: buscarCuadraturaLogProcesoIncoming - Problemas al leer cuadratura carga incoming desde la base de datos.");
				listaCuadraturaIncoming="esttrx|1~MSG|Problemas al leer cuadratura carga incoming desde la base de datos...~";
			}
			
			resultados.close();
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCuadraturaIncoming = "esttrx|1~MSG|Problemas al leer cuadratura carga incoming desde la base de datos...~";
				 return listaCuadraturaIncoming;
			}
			
			log.info("Method: buscarCuadraturaLogProcesoIncoming - "+listaCuadraturaIncoming);
			
		}
		catch (SQLException e){
			
				bdOracle.setResultado(resultados);
			    bdOracle.cerrarResultSet();
				bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaCuadraturaIncoming = "esttrx|1~MSG|Problemas al leer cuadratura carga incoming desde la base de datos...~";
					return listaCuadraturaIncoming;
				}
				log.error( e.getMessage(), e );
        		listaCuadraturaIncoming = "esttrx|1~MSG|Problemas al leer cuadratura carga incoming desde la base de datos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaCuadraturaIncoming;
	}/* Fin buscarCuadraturaLogProcesoIncoming*/
	
	
	/**
	 * Metodo que consulta las cuadraturas de carga de rechazos.
	 * @return Lista de cuadraturas de rechazos.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso fecha de consulta de informacion.
	 */
	public String buscarCuadraturaLogProcesoRechazos(String fechaProceso, int operador){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados=null;
		String listaCuadraturaRechazos="";
		
		try {
			log.info("Method: buscarCuadraturaLogProcesoRechazos - Obteniendo parametros: fechaProceso-> "+fechaProceso);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarCuadraturaLogProcesoRechazos - Ejecutando SP_SIG_CUADRATURALOGPROCESOREC");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_CUADRATURALOGPROCESOREC(?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaProceso", fechaProceso);
			stmtProcedure.setInt("filtro_operador", operador);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("cantidadRechazos", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

			int codigoError      = stmtProcedure.getInt("cod_error");
			int cantidadRechazos = stmtProcedure.getInt("cantidadRechazos");
			
			if (codigoError==0){
				
				if (resultados.next()){
					log.info("Method: buscarCuadraturaLogProcesoIncoming - Recorriendo resultados");
					listaCuadraturaRechazos="esttrx|0~";
					listaCuadraturaRechazos += cantidadRechazos + "|";
					listaCuadraturaRechazos += resultados.getInt("SID") + "|";
					listaCuadraturaRechazos += resultados.getString("DESCRIPCION") + "|";
					listaCuadraturaRechazos += resultados.getString("NUMEROTARJETA") + "|";
					listaCuadraturaRechazos += resultados.getString("MIT") + "|";
					listaCuadraturaRechazos += resultados.getString("CODIGOFUNCION") + "|";
					listaCuadraturaRechazos += resultados.getString("CODIGORAZON") + "|";
					listaCuadraturaRechazos += resultados.getBigDecimal("MONTOTRANSACCION") + "|"; 
					listaCuadraturaRechazos += resultados.getBigDecimal("MONTOCONCILIACION") + "|"; 
					listaCuadraturaRechazos += resultados.getBigDecimal("MONTOFACTURACION") + "~";
					
					while (resultados.next()) {
						
						listaCuadraturaRechazos += cantidadRechazos + "|";
						listaCuadraturaRechazos += resultados.getInt("SID") + "|";
						listaCuadraturaRechazos += resultados.getString("DESCRIPCION") + "|";
						listaCuadraturaRechazos += resultados.getString("NUMEROTARJETA") + "|";
						listaCuadraturaRechazos += resultados.getString("MIT") + "|";
						listaCuadraturaRechazos += resultados.getString("CODIGOFUNCION") + "|";
						listaCuadraturaRechazos += resultados.getString("CODIGORAZON") + "|";
						listaCuadraturaRechazos += resultados.getBigDecimal("MONTOTRANSACCION") + "|"; 
						listaCuadraturaRechazos += resultados.getBigDecimal("MONTOCONCILIACION") + "|"; 
						listaCuadraturaRechazos += resultados.getBigDecimal("MONTOFACTURACION") + "~";
						
					}
					
				}else{
					if (cantidadRechazos!=0){
						listaCuadraturaRechazos  = "esttrx|2~MSG|" + cantidadRechazos + "~";
					}
					else{
						//listaCuadraturaRechazos="esttrx|1~MSG|No existe informacion con los parametros seleccionados para cuadratura de rechazos.~";
						listaCuadraturaRechazos  = "esttrx|2~MSG|0" + "~";
					}
				}
				
			}else{
				listaCuadraturaRechazos="esttrx|1~MSG|Problemas al leer las cuadratura de carga de rechazos desde la base de datos...~";
				log.error("Problemas al leer las cuadratura de carga de rechazos desde la base de datos.");
			}
			
			resultados.close();
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCuadraturaRechazos = "esttrx|1~MSG|Problemas al leer las cuadratura de carga de rechazos desde la base de datos...~";
				 return listaCuadraturaRechazos;
			}
			
		}catch (SQLException e){
			
				bdOracle.setResultado(resultados);
				bdOracle.cerrarResultSet();
				bdOracle.cerrarStatement(stmtProcedure);

				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaCuadraturaRechazos = "esttrx|1~MSG|Problemas al leer las cuadratura de carga de rechazos desde la base de datos...~";
					return listaCuadraturaRechazos;
				}
		   
				log.error( e.getMessage(), e );
        		listaCuadraturaRechazos = "esttrx|1~MSG|Problemas al leer las cuadratura de carga de rechazos desde la base de datos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaCuadraturaRechazos;
	}/* Fin buscarCuadraturaLogProcesoRechazos*/
	
	
	
	/**
	 * Metodo que consulta las transacciones de outgoing pendientes, ok, los cargos y abonos pendientes y ok.
	 * @return Cantidad de registros de outgoing pendientes, ok, cargos y abonos pendientes y ok.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso Fecha de consulta de informacion.
	 */
	public String buscarCuadraturaLogProcesoOutgoing(String fechaProceso, int operador){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		
		String listaCuadraturaOutgoing="";
		
		try {
			
			log.info("Method: buscarCuadraturaLogProcesoOutgoing - Obteniendo parametros: fechaProceso-> "+fechaProceso);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarCuadraturaLogProcesoRechazos - Ejecutando SP_SIG_CUADRATURALOGPROCESOSAL");
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_CUADRATURALOGPROCESOSAL(?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaProceso", fechaProceso);
			stmtProcedure.setInt("filtro_operador", operador);
			stmtProcedure.registerOutParameter("cantidadoutpen", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("cantidadoutok", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("cantidadcarpen", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("cantidadcarok", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int codigoError          = stmtProcedure.getInt("cod_error");
			int cantidadoutpen       = stmtProcedure.getInt("cantidadoutpen");
			int cantidadoutok        = stmtProcedure.getInt("cantidadoutok");
			int cantidadcarpen       = stmtProcedure.getInt("cantidadcarpen");
			int cantidadcarok        = stmtProcedure.getInt("cantidadcarok");
			
			if (codigoError==0){
				
				log.info("Method: buscarCuadraturaLogProcesoRechazos - Obteniendo respuesta...");
				listaCuadraturaOutgoing ="esttrx|0~";
				listaCuadraturaOutgoing += cantidadoutpen + "|";
				listaCuadraturaOutgoing += cantidadoutok + "|";
				listaCuadraturaOutgoing += cantidadcarpen + "|";
				listaCuadraturaOutgoing += cantidadcarok + "~";
				
			}else{
				log.error("Problemas al leer la cuadratura de outgoing desde la base de datos.");
				listaCuadraturaOutgoing ="esttrx|1~MSG|Problemas al leer la cuadratura de outgoing desde la base de datos...~";
			}
			
			stmtProcedure.close();
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCuadraturaOutgoing = "esttrx|1~MSG|Problemas al leer la cuadratura de outgoing desde la base de datos...~";
				 return listaCuadraturaOutgoing;
			}
			
		}
		catch (SQLException e){     
			
			    bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaCuadraturaOutgoing =  "esttrx|1~MSG|Problemas al leer la cuadratura de outgoing desde la base de datos...~";
					return listaCuadraturaOutgoing;
				}
				
				log.error( e.getMessage(), e );
        		listaCuadraturaOutgoing = "esttrx|1~MSG|Problemas al leer la cuadratura de outgoing desde la base de datos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaCuadraturaOutgoing;
	}/* Fin buscarCuadraturaLogProcesoOutgoing*/
		

	/**
	 * Metodo que consulta las transacciones con Patpass.
	 * @return Cantidad de registros con patapass,sin patpass
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso Fecha de consulta de informacion.
	 */
	public String buscarCuadraturaLogPatPass(String fechaProceso, int operador){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		
		String listaCuadraturaPastpass="";
		
		try {
			
			log.info("Method: buscarCuadraturaLogProcesoOutgoing - Obteniendo parametros: fechaProceso-> "+fechaProceso);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarCuadraturaLogProcesoRechazos - Ejecutando SP_SIG_CUADRATURALOGPATPASS");
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_CUADRATURALOGPATPASS(?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaProceso", fechaProceso);
			stmtProcedure.setInt("filtro_operador", operador);
			stmtProcedure.registerOutParameter("cantidadinconormales", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("cantidadincopatpass", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int codigoError          = stmtProcedure.getInt("cod_error");
			int cantidadinconormales       = stmtProcedure.getInt("cantidadinconormales");
			int cantidadincopatpass        = stmtProcedure.getInt("cantidadincopatpass");
		
			
			if (codigoError==0){
				
				log.info("Method: buscarCuadraturaLogPatPass - Obteniendo respuesta...");
				listaCuadraturaPastpass ="esttrx|0~";
				listaCuadraturaPastpass += cantidadinconormales + "|";
				
				listaCuadraturaPastpass += cantidadincopatpass + "~";
				
			}else{
				log.error("Problemas al leer la cuadratura Patpass desde la base de datos.");
				listaCuadraturaPastpass ="esttrx|1~MSG|Problemas al leer la cuadratura Patpass desde la base de datos...~";
			}
			
			stmtProcedure.close();
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCuadraturaPastpass = "esttrx|1~MSG|Problemas al leer la cuadratura Patpass desde la base de datos...~";
				 return listaCuadraturaPastpass;
			}
			
		}
		catch (SQLException e){     
			
			    bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaCuadraturaPastpass =  "esttrx|1~MSG|Problemas al leer la cuadratura Patpass desde la base de datos...~";
					return listaCuadraturaPastpass;
				}
				
				log.error( e.getMessage(), e );
				listaCuadraturaPastpass = "esttrx|1~MSG|Problemas al leer la cuadratura Patpass desde la base de datos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaCuadraturaPastpass;
	}/* Fin buscarCuadraturaLogPatPass*/
	
	/**
	 * Metodo que consulta las transacciones con Patpass.
	 * @return Cantidad de registros con patapass,sin patpass
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso Fecha de consulta de informacion.
	 */
	public String buscarCuadraturaLogOutgoingPatPass(String fechaProceso, int operador){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		
		String listaCuadraturaOutgoingPastpass="";
		
		try {
			
			log.info("Method: buscarCuadraturaLogProcesoOutgoing - Obteniendo parametros: fechaProceso-> "+fechaProceso);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarCuadraturaLogProcesoRechazos - Ejecutando SP_SIG_CUADRATURALOGOUTPATPASS");
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_CUADRATURALOGOUTPATPASS(?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaProceso", fechaProceso);
			stmtProcedure.setInt("filtro_operador", operador);
			stmtProcedure.registerOutParameter("cantidadoutnormales", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("cantidadoutpatpass", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int codigoError          = stmtProcedure.getInt("cod_error");
			int cantidadoutnormales       = stmtProcedure.getInt("cantidadoutnormales");
			int cantidadoutpatpass        = stmtProcedure.getInt("cantidadoutpatpass");
		
			
			if (codigoError==0){
				
				log.info("Method: buscarCuadraturaLogOutgoingPatPass - Obteniendo respuesta...");
				listaCuadraturaOutgoingPastpass ="esttrx|0~";
				listaCuadraturaOutgoingPastpass += cantidadoutnormales + "|";
				
				listaCuadraturaOutgoingPastpass += cantidadoutpatpass + "~";
				
			}else{
				log.error("Problemas al leer la cuadratura Patpass Outgoing desde la base de datos.");
				listaCuadraturaOutgoingPastpass ="esttrx|1~MSG|Problemas al leer la cuadratura Patpass desde la base de datos...~";
			}
			
			stmtProcedure.close();
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCuadraturaOutgoingPastpass = "esttrx|1~MSG|Problemas al leer la cuadratura Patpass desde la base de datos...~";
				 return listaCuadraturaOutgoingPastpass;
			}
			
		}
		catch (SQLException e){     
			
			    bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaCuadraturaOutgoingPastpass =  "esttrx|1~MSG|Problemas al leer la cuadratura Patpass desde la base de datos...~";
					return listaCuadraturaOutgoingPastpass;
				}
				
				log.error( e.getMessage(), e );
				listaCuadraturaOutgoingPastpass = "esttrx|1~MSG|Problemas al leer la cuadratura Patpass desde la base de datos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaCuadraturaOutgoingPastpass;
	}/* Fin buscarCuadraturaLogOutgoingPatPass*/
	
	
	
}
