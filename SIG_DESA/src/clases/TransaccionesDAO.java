package clases;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import cl.util.Utils;
import properties.PropertiesConfiguration;
import Beans.TransaccionBean;
import Beans.TransaccionOnusBean;
import au.com.bytecode.opencsv.CSVWriter;
import db.OracleBD;



/**
 * Clase DAO que administra la consulta de transacciones.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class TransaccionesDAO {
	
	static final Logger log =  Logger.getLogger(TransaccionesDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	private static final String TIPO_EXPORT_PRESENTACION = "P";
	private static final String TIPO_EXPORT_OBJ_RCH = "OR";
	private String esquema;
	private String fechaProceso = "";
	private String cantidadRegistros="";
	private String[] fechaGregoriana = null;
	
	private String fechaProcesoFinal="";
	
	
	private int ultimaPagina = 0;
	private int pagActual;
	
	private Vector<TransaccionBean> transaccionBeanVector = new Vector<TransaccionBean>();
	private Vector<TransaccionOnusBean> transaccionOnUsBeanVector = new Vector<TransaccionOnusBean>();	
	
	 public TransaccionesDAO(){
		
		try {
			Properties propiedades = new Properties();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
			cantidadRegistros = propiedades.getProperty("cantidadRegistros");
		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
			   log.error("Error al leer el archivo de propiedades.");
			   log.error(e.getMessage(), e);
    	}
		
	}
	
	/**
	 * Metodo que consulta las transacciones de presentacion.
	 * @return Lista de transacciones de presentacion.
	 * @throws Mensaje de excepcion controlado.
	 * @param numeroTarjeta numero de tarjeta.
	 * @param fechaInicio fecha de inicio.
	 * @param fechaTermino fecha de termino.
	 */
	 public Vector<TransaccionBean> buscarTransaccionPresentacion(String numeroTarjeta, String fechaInicio, String fechaTermino, String op, String numPagina){
			Connection conexion=null;
			db.OracleBD bdOracle=null;
			CallableStatement stmtProcedure=null;
			ResultSet rs = null;

			String listaTransaccion="";
			TransaccionBean transaccionBean = new TransaccionBean(); 
			
			try {
				
				int pagActual;
				
				if(numPagina == null)
				{
					pagActual = 1;
				}else{
					pagActual = Integer.parseInt(numPagina);
				}
				this.setPagActual(pagActual);
				
				bdOracle = new OracleBD();
				conexion = bdOracle.conectar();
				bdOracle.setConexion(conexion);
				
				
				log.info("Method: buscarTransaccionPresentacion - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + " fechaInicio = " + fechaInicio + " fechaTermino = " + fechaTermino + "pagActual = "+ pagActual + "cantidadRegistros = "+ cantidadRegistros);
				log.info("Ejecutando SP - SP_SIG_TRANSACPRESENTACIONPG");
				
				stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACPRESENTACIONPG(?, ?, ?, ?, ?, ?, ?, ?, ?); END;");
				
				if (numeroTarjeta.length()==0){
					numeroTarjeta="0";
				}
				stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
				stmtProcedure.setString("fechaInicio", fechaInicio);
				stmtProcedure.setString("fechaTermino", fechaTermino);
				stmtProcedure.setInt("numPagina", pagActual);
				stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
				
				stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
				stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
				stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
				stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
				
				stmtProcedure.execute();
				
				//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
				rs= (ResultSet) ((stmtProcedure).getObject("prfCursor"));
				
				int error = stmtProcedure.getInt("cod_error");
				
				if(error != 100)
				{
					log.info("Method: listaRechazoObjecion - Obteniendo resultados");
					
					float totReg = stmtProcedure.getInt("totReg");
					float ultimaPagina;

					ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
					this.setUltimaPagina((int) ultimaPagina);
					
					//rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
					
					listaTransaccion="esttrx:0~";
					
					
					// Alberto Contreras
					String auxMonto = null;
					while(rs.next())
					{
					    auxMonto = "";
						transaccionBean = new TransaccionBean(); 
						String valor = rs.getString("COMERCIOPAIS");
						
						
						String comercio = "";
						String regionpais     = "";
						String region = "";
						String provincia   = "";
						String pais        = "";
						String[] comerciopais;
						
						if(valor != null){
                            if (valor.length() == 36 || valor.length() == 43) {
                                comerciopais = valor.split("\\|");
                                comercio = comerciopais[0];
                                regionpais = comerciopais[1];
                                region = comerciopais[1];
                                provincia = comerciopais[1];
                                pais = comerciopais[2];
                            } else {
                                comerciopais = valor.split("\\|");
                                comercio = comerciopais[0];
                                regionpais = comerciopais[3];
                                region = regionpais.substring(0, 9);
                                provincia = regionpais.substring(10, 13);
                                pais = regionpais.substring(13);
                            }
                            
                        }
						
						
						transaccionBean.setMit(rs.getString("MIT"));
						transaccionBean.setCodigoFuncion(rs.getString("CODIGO_FUNCION"));
						transaccionBean.setNumeroTarjeta(rs.getString("NRO_TARJETA"));
						transaccionBean.setFechaTransac(rs.getString("FECHATRANSAC"));
						transaccionBean.setComercio(comercio);
						transaccionBean.setPais(pais);
						transaccionBean.setMontoTransac(rs.getString("MONTO_TRANSAC"));
						transaccionBean.setMontoFacturacion(rs.getString("MONTO_FACTURACION"));
						transaccionBean.setCodRazon(rs.getString("CODRAZON"));
						transaccionBean.setMicroFilm(rs.getString("MICROFILM"));
						transaccionBean.setCodigoAutorizacion(rs.getString("CODIGO_AUTORIZACION"));
						transaccionBean.setFechaEfectiva(rs.getString("FECHAEFECTIVA"));
						
						fechaProceso = rs.getString("FECHAPROCESO");
						fechaGregoriana = this.fromJulian(fechaProceso);
						fechaProcesoFinal =  fechaGregoriana[2]+"/" + fechaGregoriana[1] + "/" +fechaGregoriana[0];

						transaccionBean.setFechaProceso(fechaProcesoFinal);
						transaccionBean.setBinAdquiriente(rs.getString("BINADQUIRENTE"));
						transaccionBean.setLeeBanda(rs.getString("LEEBANDA"));
						transaccionBean.setEstadoTrx(rs.getString("ESTADOTRX"));
						transaccionBean.setOtrosDatos1(rs.getString("OTRODATO1"));
						transaccionBean.setOtrosDatos2(rs.getString("OTRODATO2"));
						transaccionBean.setOtrosDatos3(rs.getString("OTRODATO3"));
						transaccionBean.setOtrosDatos4(rs.getString("OTRODATO4"));
						transaccionBean.setCodMonedaTrx(rs.getString("CODMONEDATRX"));
						transaccionBean.setRubroComercio(rs.getString("RUBROCOMERCIO"));
						transaccionBean.setSid(rs.getString("SID"));
						transaccionBean.setGlosaGeneral(rs.getString("GLOSAGENERAL"));
						transaccionBean.setReferencia(rs.getString("REFERENCIA"));
						transaccionBean.setMontoConciliacion(rs.getString("MONTO_CONCILIACION"));
						transaccionBean.setOperador(rs.getString("OPERADOR"));
						
						  // Formateo de Monto Conciliacion
                        auxMonto = Utils.formateaMonto( transaccionBean.getMontoConciliacion());
                        transaccionBean.setMontoConciliacion( auxMonto );
                        
                        // formateo de monto Monto Facturacion
                        auxMonto = Utils.formateaMonto( transaccionBean.getMontoFacturacion());
                        transaccionBean.setMontoFacturacion(auxMonto);
                        
                        // formateo de Monto de Transaccion
                        auxMonto = Utils.formateaMonto( transaccionBean.getMontoTransac());
                        transaccionBean.setMontoTransac(auxMonto);
						
						transaccionBeanVector.add(transaccionBean);
						
						
						log.info("Method: listaRechazoObjecion - "+listaTransaccion);
					}
					//rs.close();
					stmtProcedure.close();
				}else{
					log.info("Method: listaRechazoObjecion - Lista vac�a");
					//listaTransaccion="esttrx:1~Lista vac�a";
				}
				
				rs.close();
				stmtProcedure.close();
				
				
			}
			catch (SQLException e){ 					
				//bdOracle.setResultado(rs);
				//bdOracle.cerrarResultSet();
				bdOracle.cerrarStatement(stmtProcedure);
				log.error(e.getMessage(), e);
	        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
			}finally{
	    		
	    		if (!bdOracle.cerrarConexionBD()){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
					 transaccionBean.setError("esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~");
					 transaccionBeanVector.add(transaccionBean);
					 return transaccionBeanVector;
				}
	    		try {
	                if(conexion != null && !conexion.isClosed()){
	                    conexion.close();
	                }
	            }
	            catch ( SQLException e ) {
	                log.error(e.getMessage(), e);
	            }
	    	}
			
			return transaccionBeanVector;
		}/* Fin buscarTransaccionPresentacion*/
	 
	 /**
		 * Metodo que consulta las transacciones de presentacion.
		 * @return Lista de transacciones de presentacion.
		 * @throws Mensaje de excepcion controlado.
		 * @param numeroTarjeta numero de tarjeta.
		 * @param fechaInicio fecha de inicio.
		 * @param fechaTermino fecha de termino.
		 */
		 public Vector<TransaccionBean> buscarTransaccionPresentacionVisa(String numeroTarjeta, String fechaInicio, String fechaTermino, String op, String numPagina, String codFunc){
				Connection conexion=null;
				db.OracleBD bdOracle=null;
				CallableStatement stmtProcedure=null;
				ResultSet rs = null;

				String listaTransaccion="";
				TransaccionBean transaccionBean = new TransaccionBean(); 
				
				try {
					
					int pagActual;
					
					if(numPagina == null)
					{
						pagActual = 1;
					}else{
						pagActual = Integer.parseInt(numPagina);
					}
					this.setPagActual(pagActual);
					
					bdOracle = new OracleBD();
					conexion = bdOracle.conectar();
					bdOracle.setConexion(conexion);
					
					
					log.info("Method: buscarTransaccionPresentacion - Parametros Entrada : "
							+ " numeroTarjeta = "
							+ numeroTarjeta
							+ " fechaInicio = "
							+ fechaInicio + " fechaTermino = " + fechaTermino
							+ " codTransac = " + op
							+ " codFunc = " + codFunc
							+ " numPagina = " + numPagina);
					
					log.info("Ejecutando SP - SP_SIG_TRANSACPRESENT_PG_VISA");
					
					stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACPRESENT_PG_VISA(?,?,?,?,?,?,?,?,?,?,?); END;");
					
					if (numeroTarjeta != null && numeroTarjeta.length()==0){
						numeroTarjeta="0";
					}
					stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
					stmtProcedure.setString("fechaInicio", fechaInicio);
					stmtProcedure.setString("fechaTermino", fechaTermino);
					stmtProcedure.setInt("numPagina", pagActual);
					stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
					stmtProcedure.setString("codTransac", op);
					stmtProcedure.setString("codFuncion", codFunc);
					
					stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
					stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
					stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
					stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
					
					stmtProcedure.execute();
					
					//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
					rs= (ResultSet) ((stmtProcedure).getObject("prfCursor"));
					
					int error = stmtProcedure.getInt("cod_error");
					
					if(error != 100)
					{
						log.info("Method: listaRechazoObjecion - Obteniendo resultados");
						
						float totReg = stmtProcedure.getInt("totReg");
						float ultimaPagina;

						ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
						this.setUltimaPagina((int) ultimaPagina);
						
						//rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
						
						listaTransaccion="esttrx:0~";
						
						
						// Alberto Contreras
						String auxMonto;
						while(rs.next())
						{
							transaccionBean = new TransaccionBean(); 
							auxMonto = "";
							String valor = rs.getString("COMERCIOPAIS");
							
							
							String comercio = "";
							String regionpais     = "";
							String region = "";
							String provincia   = "";
							String pais        = "";
							String[] comerciopais;
							
							if(valor != null){
								if (valor.length() == 43 || valor.length() == 36) {
									comerciopais = valor.split("\\|");
									comercio = comerciopais[0];
									regionpais = comerciopais[1];
									region = comerciopais[1];
									provincia = comerciopais[1];
									pais = comerciopais[2];
								} else {
									comerciopais = valor.split("\\|");
									
									
									comercio = comerciopais[0];
									
									regionpais = comerciopais[3];
									region = regionpais.substring(0, 9);
									provincia = regionpais.substring(10, 13);
									pais = regionpais.substring(13);
									
									
									
									
								}
								
							}
							
							transaccionBean.setMit(rs.getString("MIT"));
							transaccionBean.setCodigoFuncion(rs.getString("CODIGO_FUNCION"));
							transaccionBean.setNumeroTarjeta(rs.getString("NRO_TARJETA"));
							transaccionBean.setFechaTransac(rs.getString("FECHATRANSAC"));
							transaccionBean.setComercio(comercio);
							transaccionBean.setPais(pais);
							transaccionBean.setMontoTransac(rs.getString("MONTO_TRANSAC"));
							transaccionBean.setMontoFacturacion(rs.getString("MONTO_FACTURACION"));
							transaccionBean.setCodRazon(rs.getString("CODRAZON"));
							transaccionBean.setMicroFilm(rs.getString("MICROFILM"));
							transaccionBean.setCodigoAutorizacion(rs.getString("CODIGO_AUTORIZACION"));
							transaccionBean.setFechaEfectiva(rs.getString("FECHAEFECTIVA"));
							
							fechaProceso = rs.getString("FECHAPROCESO");
							fechaGregoriana = this.fromJulian(fechaProceso);
							fechaProcesoFinal =  fechaGregoriana[2]+"/" + fechaGregoriana[1] + "/" +fechaGregoriana[0];

							transaccionBean.setFechaProceso(fechaProcesoFinal);
							transaccionBean.setBinAdquiriente(rs.getString("BINADQUIRENTE"));
							transaccionBean.setLeeBanda(rs.getString("LEEBANDA"));
							transaccionBean.setEstadoTrx(rs.getString("ESTADOTRX"));
							transaccionBean.setOtrosDatos1(rs.getString("OTRODATO1"));
							transaccionBean.setOtrosDatos2(rs.getString("OTRODATO2"));
							transaccionBean.setOtrosDatos3(rs.getString("OTRODATO3"));
							transaccionBean.setOtrosDatos4(rs.getString("OTRODATO4"));
							transaccionBean.setCodMonedaTrx(rs.getString("CODMONEDATRX"));
							transaccionBean.setRubroComercio(rs.getString("RUBROCOMERCIO"));
							transaccionBean.setSid(rs.getString("SID"));
							transaccionBean.setGlosaGeneral(rs.getString("GLOSAGENERAL"));
							transaccionBean.setReferencia(rs.getString("REFERENCIA"));

							transaccionBean.setMontoConciliacion(rs.getString("MONTO_CONCILIACION"));
														
							// Formateo de Monto Conciliacion
							auxMonto = Utils.formateaMonto( transaccionBean.getMontoConciliacion());
							transaccionBean.setMontoConciliacion( auxMonto );
							
							// formateo de monto Monto Facturacion
							auxMonto = Utils.formateaMonto( transaccionBean.getMontoFacturacion());
							transaccionBean.setMontoFacturacion(auxMonto);
							
							// formateo de Monto de Transaccion
							auxMonto = Utils.formateaMonto( transaccionBean.getMontoTransac());
							transaccionBean.setMontoTransac(auxMonto);
									
							transaccionBean.setDatosAdicionales5(rs.getString("DATOSADICIONALES5"));
							transaccionBean.setPatpass(rs.getString("PATPASS"));
							
							transaccionBean.setOperador(rs.getString("OPERADOR"));
							transaccionBeanVector.add(transaccionBean);
							
						}
						
						log.info( "Valores de las trx" );
						log.info( transaccionBeanVector );
						//rs.close();
						stmtProcedure.close();
					}else{
						log.info("Method: listaRechazoObjecion - Lista vac�a");
						//listaTransaccion="esttrx:1~Lista vac�a";
					}
					
					rs.close();
					stmtProcedure.close();
					
					
				}
				catch (SQLException e){ 					
					//bdOracle.setResultado(rs);
					//bdOracle.cerrarResultSet();
					bdOracle.cerrarStatement(stmtProcedure);
					log.error(e.getMessage(), e);
		        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				}finally{
		    		
		    		if (!bdOracle.cerrarConexionBD()){
						 log.error("Error: No se pudo cerrar conexion a la base de datos.");
						 transaccionBean.setError("esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~");
						 transaccionBeanVector.add(transaccionBean);
						 return transaccionBeanVector;
					}
		    		try {
		                if(conexion != null && !conexion.isClosed()){
		                    conexion.close();
		                }
		            }
		            catch ( SQLException e ) {
		                log.error(e.getMessage(), e);
		            }
		    	}
				
				return transaccionBeanVector;
			}/* Fin buscarTransaccionPresentacionVisa*/
	
public File buscarTransaccionPresentacionExport(String numeroTarjeta, String fechaInicio, String fechaTermino, String op, String ruta, String codigoTransaccion, String codigoFuncion){
		
		
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		Object[] o = new Object[2];
		List<String[]> dataList = new ArrayList<String[]>();
		String listaTransaccion="";
		
		File archivoSalida = new File(ruta);
		
		try {
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			
			log.info("Method: buscarTransaccionPresentacion - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + " fechaInicio = " + fechaInicio + " fechaTermino = " + fechaTermino);
			log.info("Ejecutando SP - SP_SIG_TRANSACPRESENTACION");
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACPRESENTACION(?, ?, ?,?,?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("codTransac", codigoTransaccion);
			stmtProcedure.setString("codigoFuncion", codigoFuncion);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			int codigoError = stmtProcedure.getInt("cod_error");
			
			
			//GENERA CABECERA
			String[] header = generateHeaderToExcel(TIPO_EXPORT_PRESENTACION);
			

			// PARSEO DE DATOS DEL EXCEL
			while (resultados.next()) {
			    dataList.add(getRegistrosToExcel(resultados));
            }
					
			CSVWriter writer = new CSVWriter(new FileWriter(ruta), ';');
			writer.writeNext( header );
			writer.writeAll( dataList );
			writer.close();
			
			resultados.close();
			stmtProcedure.close();
			
			
		}
		catch (SQLException e){ 					
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}finally{
    		
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return archivoSalida;
			}
    	}
		
		return archivoSalida;
	}/* Fin buscarTransaccionPresentacion*/
	

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private String[] generateHeaderToExcel(String tipo) {
    	
    	if (TIPO_EXPORT_OBJ_RCH.equalsIgnoreCase(tipo)){

       return new String[]{
              "NRO_TARJETA",
              "FECHATRANSAC",
              "COMERCIO",
              "PAIS",
              "MONTO_TRANSAC",
              "MONTO_FACTURACION",
              "CODRAZON",
              "ESTADOTRX",
              "GLOSAGENERAL",
    	              "NUM_INCIDENTE",
    	              "FECHA_GESTION",
    	              "USUARIO","PATPASS","OPERADOR"
       };
 
    	}else{
    		
    		return new String[]{
    	              "NRO_TARJETA",
    	              "FECHATRANSAC",
    	              "COMERCIO",
    	              "PAIS",
    	              "MONTO_TRANSAC",
    	              "MONTO_FACTURACION",
    	              "CODRAZON",
    	              "ESTADOTRX",
    	              "GLOSAGENERAL",
    	              "NUM_INCIDENTE",
					  "FECHA_GESTION",
    	              "USUARIO","PATPASS","OPERADOR"
    	       };
    		
    	}
       
 
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) -  versi�n inicial 
     * </ul>
     * <p>
     * Metodo que crea una lista de String
     * 
     * @param resultados
     * @return
     * @since 1.X
     */
    private String[] getRegistrosToExcel(ResultSet rs) {
	    
	            
        try {
            String registro = "";
            registro += rs.getString( "NRO_TARJETA") + ";";
            registro += rs.getString( "FECHATRANSAC" )+ ";";
            registro += rs.getString( "COMERCIO" )+";";
            registro += rs.getString( "PAIS" )+ ";";
            registro += Utils.formateaMonto( rs.getString( "MONTO_TRANSAC" ))+ ";";
            registro += Utils.formateaMonto( rs.getString( "MONTO_FACTURACION" ))+ ";";
            registro += rs.getString( "CODRAZON" )+ ";";
            registro += rs.getString( "ESTADOTRX" )+ ";";
            registro += rs.getString( "GLOSAGENERAL" )+";";
            registro += rs.getString( "NUM_INCIDENTE" )== null?";": rs.getString( "NUM_INCIDENTE" )+";" ;
            /*if(rs.getString( "USUARIO" ) == null || rs.getString( "USUARIO" ).equals("") ){
        	registro += ";";
            }
            else{
        	registro += rs.getString( "USUARIO" )+ ";";
            }*/
            
            registro += ";";
            registro += ";";
            
            //log.info(rs.getString( "FECHA_GESTION" ));
            
            //registro += rs.getString( "FECHA_GESTION" )== null?";": rs.getString( "FECHA_GESTION" )+";" ;
            
            
            //registro += rs.getString( "USUARIO" )== null?";": rs.getString( "USUARIO" )+";" ;
            
            
            if ( rs.getString( "PATPASS" ).equals(" ") || rs.getString( "PATPASS" ).equals("") ||  rs.getString( "PATPASS" ).equals("-")  || rs.getString( "PATPASS" ) == null || rs.getString("OPERADOR").equals("VN") || rs.getString("OPERADOR").equals("VI") ) {
       	 	registro += "No"+";";
	    }else{
		registro += "Si"+";";
	    }
            registro += rs.getString("OPERADOR");
            
            return registro.split( ";" );
        }
        catch ( SQLException e ) {
          log.error( e.getMessage(), e );
        }
        return null;

	}

	/**
	 * Metodo que consulta las transacciones de objecion o rechazos.
	 * @return Lista de transacciones de objecion o rechazos.
	 * @throws Mensaje de excepcion controlado.
	 * @param numeroTarjeta numero de tarjeta.
	 * @param fechaInicio fecha de inicio.
	 * @param fechaTermino fecha de termino.
	 * @param tipoObjecionRechazo tipo de consulta.
	 */
	public Vector<TransaccionBean> buscarTransaccionObjecionRechazo(String numeroTarjeta, String fechaInicio, String fechaTermino, String tipoObjecionRechazo, String op, String numPagina, String tipoBusqueda){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		TransaccionBean transaccionBean = new TransaccionBean(); 
		String listaTransaccion="";
		
		try {
			
			log.info("Method: buscarTransaccionObjecionRechazo - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta 
																						+ " fechaInicio = " + fechaInicio 
																						+ " fechaTermino = " + fechaTermino 
																						+ " tipoObjecionRechazo = " + tipoObjecionRechazo
																						+ " tipoBusqueda = " + tipoBusqueda);
			log.info("Ejecutando SP - SP_SIG_TRANSACOBJRECHPG");
			int pagActual;
			
			if(numPagina == null)
			{
				pagActual = 1;
			}else{
				pagActual = Integer.parseInt(numPagina);
			}
			
			if(tipoObjecionRechazo == null){
			    tipoObjecionRechazo = op;
			}
			
			this.setPagActual(pagActual);
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACOBJRECHPG(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			
			// ENTRADA
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setInt("numPagina", pagActual);
			stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
			stmtProcedure.setString("tipoObjecionRechazo", tipoObjecionRechazo);
			stmtProcedure.setString("tipoBusqueda", tipoBusqueda);
			
		    // SALIDA	
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			float totReg = stmtProcedure.getInt("totReg");
			float ultimaPagina;

			ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
			this.setUltimaPagina((int) ultimaPagina);
			String montoAux = null;
			while(resultados.next())
			{
			    montoAux = "";
				transaccionBean = new TransaccionBean(); 
				String valor = resultados.getString("COMERCIOPAIS");
				
				
				String comercio = "";
				String regionpais     = "";
				String region = "";
				String provincia   = "";
				String pais        = "";
				String[] comerciopais;
				
				if(valor != null){
				    log.info(valor);
					if (valor.length() == 36 || valor.length() == 43) {
					    	
						comerciopais = valor.split("\\|");
						log.info(comerciopais);
						comercio = comerciopais[0];
						regionpais = comerciopais[1];
						region = comerciopais[1];
						provincia = comerciopais[1];
						pais = comerciopais[2];
					} else {
						comerciopais = valor.split("\\|");
						log.info(comerciopais);
						comercio = comerciopais[0];
						regionpais = comerciopais[3];
						region = regionpais.substring(0, 9);
						provincia = regionpais.substring(10, 13);
						pais = regionpais.substring(13);
					}
				}
				
				transaccionBean.setMit(resultados.getString("MIT"));
				transaccionBean.setCodigoFuncion(resultados.getString("CODIGO_FUNCION"));
				log.info(resultados.getString("NRO_TARJETA"));
				transaccionBean.setNumeroTarjeta(resultados.getString("NRO_TARJETA"));
				transaccionBean.setFechaTransac(resultados.getString("FECHATRANSAC"));
				transaccionBean.setComercio(comercio);
				transaccionBean.setPais(pais);
				transaccionBean.setMontoTransac(resultados.getString("MONTO_TRANSAC"));
				transaccionBean.setMontoFacturacion(resultados.getString("MONTO_FACTURACION"));
				transaccionBean.setCodRazon(resultados.getString("CODRAZON"));
				transaccionBean.setMicroFilm(resultados.getString("MICROFILM"));
				transaccionBean.setCodigoAutorizacion(resultados.getString("CODIGO_AUTORIZACION"));
				transaccionBean.setFechaEfectiva(resultados.getString("FECHAEFECTIVA"));
				
				fechaProceso = resultados.getString("FECHAPROCESO");
				fechaGregoriana = this.fromJulian(fechaProceso);
				fechaProcesoFinal =  fechaGregoriana[2]+"/" + fechaGregoriana[1] + "/" +fechaGregoriana[0];

				transaccionBean.setFechaProceso(fechaProcesoFinal);
				transaccionBean.setBinAdquiriente(resultados.getString("BINADQUIRENTE"));
				transaccionBean.setLeeBanda(resultados.getString("LEEBANDA"));
				transaccionBean.setEstadoTrx(resultados.getString("ESTADOTRX"));
				transaccionBean.setOtrosDatos1(resultados.getString("OTRODATO1"));
				transaccionBean.setOtrosDatos2(resultados.getString("OTRODATO2"));
				transaccionBean.setOtrosDatos3(resultados.getString("OTRODATO3"));
				transaccionBean.setOtrosDatos4(resultados.getString("OTRODATO4"));
				transaccionBean.setCodMonedaTrx(resultados.getString("CODMONEDATRX"));
				transaccionBean.setRubroComercio(resultados.getString("RUBROCOMERCIO"));
				transaccionBean.setSid(resultados.getString("SID"));
				transaccionBean.setGlosaGeneral(resultados.getString("GLOSAGENERAL"));
				transaccionBean.setReferencia(resultados.getString("REFERENCIA"));
				transaccionBean.setMontoConciliacion(resultados.getString("MONTO_CONCILIACION"));
				transaccionBean.setNumIncidente(resultados.getString("NUM_INCIDENTE"));
				transaccionBean.setFechaGestion(resultados.getString("FECHA_GESTION"));
				transaccionBean.setIdUsuario(resultados.getString("USUARIO"));
				transaccionBean.setPatpass(resultados.getString("PATPASS"));
				transaccionBean.setOperador(resultados.getString("OPERADOR"));
				
				
				// Formateo de montos
				montoAux = Utils.formateaMonto( transaccionBean.getMontoTransac() );
				transaccionBean.setMontoTransac( montoAux );
				
				// Formateo de montos
				montoAux =Utils.formateaMonto( transaccionBean.getMontoConciliacion());
				transaccionBean.setMontoConciliacion( montoAux );
				
				// Formateo de montos
				montoAux = Utils.formateaMonto( transaccionBean.getMontoFacturacion() );
				transaccionBean.setMontoFacturacion( montoAux );
				
				transaccionBeanVector.add(transaccionBean);
				
				log.info("Method: listaRechazoObjecion - "+transaccionBean);
				
			}
//			log.info("Method: listaRechazoObjecion - "+transaccionBeanVector);
			resultados.close();
			stmtProcedure.close();
			
			
		}
		catch (SQLException e){ 					
			//bdOracle.setResultado(resultados);
			//bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		}finally{
    		
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return transaccionBeanVector;
			}
    	}
		
		return transaccionBeanVector;
	}/* Fin buscarTransaccionObjecionRechazo */
	
	
public File buscarTransaccionObjecionRechazoExport(String numeroTarjeta, String fechaInicio, String fechaTermino, String tipoObjecionRechazo, String op, String ruta, String tipoBusqueda){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		CSVWriter writer = null;
		List<String[]> dataList = new ArrayList<String[]>();

		String listaTransaccion="";
		File archivoSalida = new File(ruta);
		try {
			
			log.info("Method: buscarTransaccionObjecionRechazo - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + " fechaInicio = " + fechaInicio + " fechaTermino = " + fechaTermino + " tipoObjecionRechazo = " + tipoObjecionRechazo);
			log.info("Ejecutando SP - SP_SIG_TRANSACOBJECIONRECHAZO");

			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACOBJECIONRECHAZO(?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("tipoObjecionRechazo", tipoObjecionRechazo);
			stmtProcedure.setString("tipoBusqueda", tipoBusqueda);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

			int codigoError = stmtProcedure.getInt("cod_error");
			
			
			 //GENERA CABECERA
            String[] header = generateHeaderToExcel(TIPO_EXPORT_OBJ_RCH);
            

            // PARSEO DE DATOS DEL EXCEL
            while (resultados.next()) {
                dataList.add(getRegistrosToExcelTrxGenerales(resultados));
            }
			
			
			writer = new CSVWriter(new FileWriter(ruta), ';');
			writer.writeNext( header );
			writer.writeAll( dataList );
			
			
			stmtProcedure.close();
			
			
		}
		catch (SQLException e){ 					
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}finally{
    		
            try {
                if ( writer != null ) {
                    writer.close();
                }
            }
            catch ( IOException e ) {
                log.error(
                        "Error al intentar cerrar el writer  : "
                                + e.getMessage(), e );
            }
            
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return archivoSalida;
			}
    	}
		
		return archivoSalida;
	}/* Fin buscarTransaccionObjecionRechazo */
	/**
	 * Metodo que consulta las transacciones asociadas a la accion seleccionada.
	 * @return Lista de transacciones segun accion.
	 * @throws Mensaje de excepcion controlado.
	 * @param numeroTarjeta numero de tarjeta.
	 * @param fechaInicio fecha de inicio.
	 * @param fechaTermino fecha de termino.
	 * @param codigoTransaccion codigo de transaccion.
	 * @param codigoFuncion codigo de funcion.
	 */
	public Vector<TransaccionBean> buscarTransaccionAcciones(String numeroTarjeta, String fechaInicio, String fechaTermino, String codigoTransaccion, String codigoFuncion, String op, String numPagina, String tipoBusqueda ){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		TransaccionBean transaccionBean = new TransaccionBean(); 
		String listaTransaccion="";
		
		try {
			
			log.info("Method: buscarTransaccionAcciones - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + "" +
																				   " fechaInicio = " + fechaInicio + 
																				   " fechaTermino = " + fechaTermino +
																				   " codigoTransaccion = " + codigoTransaccion +
																				   " codigoFuncion = " + codigoFuncion +
																				   " tipoBusqueda ="+ tipoBusqueda);
			log.info("Ejecutando SP - SP_SIG_TRANSACACCIONESGRALPG");
			int pagActual;
			
			if(numPagina == null)
			{
				pagActual = 1;
			}else{
				pagActual = Integer.parseInt(numPagina);
			}
			
			// PARCHE POR QUE VIENE NULO AL HACER LA PAGINACION
			if(codigoTransaccion == null ){
			    codigoTransaccion = op;
			}
			
			this.setPagActual(pagActual);
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACACCIONESGRALPG(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("codigoTransaccion", codigoTransaccion);
			stmtProcedure.setString("codigoFuncion", codigoFuncion);
			stmtProcedure.setString("tipoBusqueda", tipoBusqueda);
			stmtProcedure.setInt("numPagina", pagActual);
			stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
			
			
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			log.info( " numPagina : " + numPagina  +" - cantReg: " + cantidadRegistros );
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			float totReg = stmtProcedure.getInt("totReg");
			float ultimaPagina;

			ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
			this.setUltimaPagina((int) ultimaPagina);
			
			String montoAux = new String();
			while(resultados.next())
			{
			    montoAux = "";
				transaccionBean = new TransaccionBean(); 
				String valor = resultados.getString("COMERCIOPAIS");
				
				
				String comercio = "";
				String regionpais     = "";
				String region = "";
				String provincia   = "";
				String pais        = "";
				String[] comerciopais;
				
				if(valor != null){
					if (valor.length() == 36 || valor.length() == 43) {
						comerciopais = valor.split("\\|");
						comercio = comerciopais[0];
						regionpais = comerciopais[1];
						region = comerciopais[1];
						provincia = comerciopais[1];
						pais = comerciopais[2];
					} else {
						comerciopais = valor.split("\\|");
						comercio = comerciopais[0];
						regionpais = comerciopais[3];
						region = regionpais.substring(0, 9);
						provincia = regionpais.substring(10, 13);
						pais = regionpais.substring(13);
					}
				}
				
				transaccionBean.setMit(resultados.getString("MIT"));
				transaccionBean.setCodigoFuncion(resultados.getString("CODIGO_FUNCION"));
				transaccionBean.setNumeroTarjeta(resultados.getString("NRO_TARJETA"));
				transaccionBean.setFechaTransac(resultados.getString("FECHATRANSAC"));
				transaccionBean.setComercio(comercio);
				transaccionBean.setPais(pais);
				transaccionBean.setMontoTransac(resultados.getString("MONTO_TRANSAC"));
				transaccionBean.setMontoFacturacion(resultados.getString("MONTO_FACTURACION"));
				transaccionBean.setCodRazon(resultados.getString("CODRAZON"));
				transaccionBean.setMicroFilm(resultados.getString("MICROFILM"));
				transaccionBean.setCodigoAutorizacion(resultados.getString("CODIGO_AUTORIZACION"));
				transaccionBean.setFechaEfectiva(resultados.getString("FECHAEFECTIVA"));
				
				fechaProceso = resultados.getString("FECHAPROCESO");
				fechaGregoriana = this.fromJulian(fechaProceso);
				fechaProcesoFinal =  fechaGregoriana[2]+"/" + fechaGregoriana[1] + "/" +fechaGregoriana[0];

				transaccionBean.setFechaProceso(fechaProcesoFinal);
				transaccionBean.setBinAdquiriente(resultados.getString("BINADQUIRENTE"));
				transaccionBean.setLeeBanda(resultados.getString("LEEBANDA"));
				transaccionBean.setEstadoTrx(resultados.getString("ESTADOTRX"));
				transaccionBean.setOtrosDatos1(resultados.getString("OTRODATO1"));
				transaccionBean.setOtrosDatos2(resultados.getString("OTRODATO2"));
				transaccionBean.setOtrosDatos3(resultados.getString("OTRODATO3"));
				transaccionBean.setOtrosDatos4(resultados.getString("OTRODATO4"));
				transaccionBean.setCodMonedaTrx(resultados.getString("CODMONEDATRX"));
				transaccionBean.setRubroComercio(resultados.getString("RUBROCOMERCIO"));
				transaccionBean.setSid(resultados.getString("SID"));
				transaccionBean.setGlosaGeneral(resultados.getString("GLOSAGENERAL"));
				transaccionBean.setReferencia(resultados.getString("REFERENCIA"));
				transaccionBean.setMontoConciliacion(resultados.getString("MONTO_CONCILIACION"));
				transaccionBean.setNumIncidente(resultados.getString("NUM_INCIDENTE"));
				transaccionBean.setIdUsuario(resultados.getString("USUARIO"));
				transaccionBean.setFechaGestion(resultados.getString("FECHA_GESTION"));
				transaccionBean.setPatpass(resultados.getString("PATPASS"));
				transaccionBean.setOperador(resultados.getString("OPERADOR"));
				// Formateo de monto Conciliacion
				montoAux = Utils.formateaMonto( transaccionBean.getMontoConciliacion());
				transaccionBean.setMontoConciliacion(montoAux);
				
				// Formateo de monto Facturacion
                montoAux =  Utils.formateaMonto(transaccionBean.getMontoFacturacion());
                transaccionBean.setMontoFacturacion(montoAux);
                
                // Formateo de monto MontoTransac
                montoAux =  Utils.formateaMonto(transaccionBean.getMontoTransac());
                transaccionBean.setMontoTransac(montoAux);
                
				
				transaccionBeanVector.add(transaccionBean);
				
				
				log.info("Method: listaRechazoObjecion - "+listaTransaccion);
			}
			
			resultados.close();
			stmtProcedure.close();
			
			
		}
		catch (SQLException e){ 					
			//bdOracle.setResultado(resultados);
			//bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		}finally{
    		
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return transaccionBeanVector;
			}
    	}
		
		return transaccionBeanVector;
	}/* Fin buscarTransaccionAcciones */
	

	public File buscarTransaccionAccionesExport(String numeroTarjeta, String fechaInicio, String fechaTermino, String codigoTransaccion, String codigoFuncion, String op, String ruta, String tipoBusqueda){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
	    List<String[]> dataList = new ArrayList<String[]>();
		
		String listaTransaccion="";
				
		File archivoSalida = new File(ruta);
		
		try {
			
			log.info("Method: buscarTransaccionAcciones - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + " fechaInicio = " + fechaInicio + " fechaTermino = " + fechaTermino + " codigoTransaccion = " + codigoTransaccion + " " + " codigoFuncion = " + codigoFuncion);
			log.info("Ejecutando SP - SP_SIG_TRANSACACCIONESGENERAL");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACACCIONESGENERAL(?, ?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("codigoTransaccion", codigoTransaccion);
			stmtProcedure.setString("codigoFuncion", codigoFuncion);
			stmtProcedure.setString("tipoBusqueda", tipoBusqueda);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			 //GENERA CABECERA
            String[] header = generateHeaderToExcelTrxGenerales();

            // PARSEO DE DATOS DEL EXCEL
            while (resultados.next()) {
                dataList.add(getRegistrosToExcelTrxGenerales(resultados));
            }
			
           
			CSVWriter writer = new CSVWriter(new FileWriter(ruta), ';');
			writer.writeNext( header );
			writer.writeAll( dataList );
			writer.close();
			
			//resultados.close();
			stmtProcedure.close();
			
			
		}
		catch (SQLException e){ 					
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}finally{
    		
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return archivoSalida;
			}
    	}
		
		return archivoSalida;
	}/* Fin buscarTransaccionAcciones */
		
	
	/**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private String[] generateHeaderToExcelTrxGenerales() {
       return new String[]{
              "NRO_TARJETA",
              "FECHATRANSAC",
              "COMERCIO",
              "PAIS",
              "MONTO_TRANSAC",
              "MONTO_FACTURACION",
              "CODRAZON",
              "ESTADOTRX",
              "GLOSAGENERAL",
              "NUM_INCIDENTE",
              "FECHA_GESTION",
              "USUARIO",
              "PATPASS",
              "OPERADOR"
       };
 
    }
	
		
	private String[] getRegistrosToExcelTrxGenerales( ResultSet rs ) {
	    try {
	            String registro = "";
	            registro += rs.getString( "NRO_TARJETA") + ";";
	            registro += rs.getString( "FECHATRANSAC" )+ ";";
	            registro += rs.getString( "COMERCIO" )+";";
	            registro += rs.getString( "PAIS" )+ ";";
	            registro += Utils.formateaMonto( rs.getString( "MONTO_TRANSAC" ))+ ";";
	            registro += Utils.formateaMonto( rs.getString( "MONTO_FACTURACION" ))+ ";";
	            registro += rs.getString( "CODRAZON" )+ ";";
	            registro += rs.getString( "ESTADOTRX" )+ ";";
	            registro += rs.getString( "DESCRIPCION" )+ ";";
	            registro += rs.getString( "NUM_INCIDENTE" )== null?"": rs.getString( "NUM_INCIDENTE" )+";" ;
	            registro += rs.getString( "FECHA_GESTION" )+";";
	            registro += rs.getString( "USUARIO" )+";";
	            if ( rs.getString( "PATPASS" ).equals(" ") || rs.getString( "PATPASS" ).equals("") ||  rs.getString( "PATPASS" ).equals("-")  || rs.getString( "PATPASS" ) == null || rs.getString("OPERADOR").equals("VI") || rs.getString("OPERADOR").equals("VN") ) {
	       	 	registro += "No"+";";
		    }else{
			registro += "Si"+";";
		    }
	            registro += rs.getString("OPERADOR");
	            
	            return registro.split( ";" );
	        }
	        catch ( SQLException e ) {
	          log.error( e.getMessage(), e );
	        }
	        return null;

		
    }

	/**
	 * Metodo que consulta las transacciones de recuperacion de cobro de cargos incoming.
	 * @return Lista de transacciones de recuperacion de cobro de cargos incoming.
	 * @throws Mensaje de excepcion controlado.
	 * @param numeroTarjeta numero de tarjeta.
	 * @param fechaInicio fecha de inicio.
	 * @param fechaTermino fecha de termino.
	 * @param codigoTransaccion codigo de transaccion.
	 */
	public Vector<TransaccionBean> buscarTransaccionRecuperacionCargosIncoming(String numeroTarjeta, String fechaInicio, String fechaTermino, String codigoTransaccion, String op, String numPagina){
		
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		TransaccionBean transaccionBean = new TransaccionBean(); 
		String listaTransaccion="";
		
		try {
			int pagActual;
			
			if(numPagina == null)
			{
				pagActual = 1;
			}else{
				pagActual = Integer.parseInt(numPagina);
			}
			this.setPagActual(pagActual);
			log.info("Method: buscarTransaccionRecuperacionCargosIncoming - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + " fechaInicio = " + fechaInicio + " fechaTermino = " + fechaTermino + " codigoTransaccion = " + codigoTransaccion);
			log.info("Ejecutando SP - SP_SIG_TRANSACRECUPINCPG");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACRECUPINCPG(?, ?, ?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("codigoTransaccion", codigoTransaccion);
			
			stmtProcedure.setInt("numPagina", pagActual);
			stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			float totReg = stmtProcedure.getInt("totReg");
			float ultimaPagina;

			ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
			this.setUltimaPagina((int) ultimaPagina);
			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
			    String auxMonto = null;
				while(resultados.next())
				{
				    auxMonto = "";
					transaccionBean = new TransaccionBean(); 
					String valor = resultados.getString("COMERCIOPAIS");
					
					
					String comercio = "";
					String regionpais     = "";
					String region = "";
					String provincia   = "";
					String pais        = "";
					String[] comerciopais;
					
					if(valor != null){
						if (valor.length() == 36 || valor.length() == 43) {
                            comerciopais = valor.split("\\|");
                            comercio = comerciopais[0];
                            regionpais = comerciopais[1];
                            region = comerciopais[1];
                            provincia = comerciopais[1];
                            pais = comerciopais[2];
                        } else {
                            comerciopais = valor.split("\\|");
                            comercio = comerciopais[0];
                            regionpais = comerciopais[3];
                            region = regionpais.substring(0, 9);
                            provincia = regionpais.substring(10, 13);
                            pais = regionpais.substring(13);
                        }
					}
					
					transaccionBean.setMit(resultados.getString("MIT"));
					transaccionBean.setCodigoFuncion(resultados.getString("CODIGO_FUNCION"));
					transaccionBean.setNumeroTarjeta(resultados.getString("NRO_TARJETA"));
					transaccionBean.setFechaTransac(resultados.getString("FECHATRANSAC"));
					transaccionBean.setComercio(comercio);
					transaccionBean.setPais(pais);
					transaccionBean.setMontoTransac(resultados.getString("MONTO_TRANSAC"));
					transaccionBean.setMontoFacturacion(resultados.getString("MONTO_FACTURACION"));
					transaccionBean.setCodRazon(resultados.getString("CODRAZON"));
					transaccionBean.setMicroFilm(resultados.getString("MICROFILM"));
					transaccionBean.setCodigoAutorizacion(resultados.getString("CODIGO_AUTORIZACION"));
					transaccionBean.setFechaEfectiva(resultados.getString("FECHAEFECTIVA"));
					
					fechaProceso = resultados.getString("FECHAPROCESO");
					fechaGregoriana = this.fromJulian(fechaProceso);
					fechaProcesoFinal =  fechaGregoriana[2]+"/" + fechaGregoriana[1] + "/" +fechaGregoriana[0];

					transaccionBean.setFechaProceso(fechaProcesoFinal);
					transaccionBean.setBinAdquiriente(resultados.getString("BINADQUIRENTE"));
					transaccionBean.setLeeBanda(resultados.getString("LEEBANDA"));
					transaccionBean.setEstadoTrx(resultados.getString("ESTADOTRX"));
					transaccionBean.setOtrosDatos1(resultados.getString("OTRODATO1"));
					transaccionBean.setOtrosDatos2(resultados.getString("OTRODATO2"));
					transaccionBean.setOtrosDatos3(resultados.getString("OTRODATO3"));
					transaccionBean.setOtrosDatos4(resultados.getString("OTRODATO4"));
					transaccionBean.setCodMonedaTrx(resultados.getString("CODMONEDATRX"));
					transaccionBean.setRubroComercio(resultados.getString("RUBROCOMERCIO"));
					transaccionBean.setSid(resultados.getString("SID"));
					transaccionBean.setGlosaGeneral(resultados.getString("GLOSAGENERAL"));
					transaccionBean.setReferencia(resultados.getString("REFERENCIA"));
					transaccionBean.setMontoConciliacion(resultados.getString("MONTO_CONCILIACION"));
					
					 // Formateo de Monto Conciliacion
	                auxMonto = Utils.formateaMonto( transaccionBean.getMontoConciliacion());
	                transaccionBean.setMontoConciliacion( auxMonto );
	                
	                // formateo de monto Monto Facturacion
	                auxMonto = Utils.formateaMonto( transaccionBean.getMontoFacturacion());
	                transaccionBean.setMontoFacturacion(auxMonto);
	                
	                // formateo de Monto de Transaccion
	                auxMonto = Utils.formateaMonto( transaccionBean.getMontoTransac());
	                transaccionBean.setMontoTransac(auxMonto);
					
					
					transaccionBeanVector.add(transaccionBean);
					
					
					log.info("Method: listaRechazoObjecion - "+listaTransaccion);
				}
			}else{
				if(!op.equalsIgnoreCase("")){
					listaTransaccion="esttrx|1~MSG|op~";
					log.info("No existe informacion con los parametros seleccionados. Problema con op");
				}else{
					listaTransaccion="esttrx|1~MSG|No existe informacion con los parametros seleccionados.~";
					log.info("No existe informacion con los parametros seleccionados.");
				}
			}
			resultados.close();
			stmtProcedure.close();
			
			
			
		}
		catch (SQLException e){ 					
			//bdOracle.setResultado(resultados);
			//bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		}finally{
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return transaccionBeanVector;
			}
    	}
		
		return transaccionBeanVector;
	}/* Fin buscarTransaccionRecuperacionCargosIncoming */
	
	

	public File buscarTransaccionRecuperacionCargosIncomingExport(String numeroTarjeta, String fechaInicio, String fechaTermino, String codigoTransaccion, String op, String ruta){
		
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		Object[] o = new Object[2];
		String listaTransaccion="";
		File archivoSalida = new File(ruta);
		try {
			
			log.info("Method: buscarTransaccionRecuperacionCargosIncoming - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + " fechaInicio = " + fechaInicio + " fechaTermino = " + fechaTermino + " codigoTransaccion = " + codigoTransaccion);
			log.info("Ejecutando SP - SP_SIG_TRANSACACCIONRECUPINC");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACACCIONRECUPINC(?, ?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("codigoTransaccion", codigoTransaccion);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			CSVWriter writer = new CSVWriter(new FileWriter(ruta), ';');
			writer.writeAll(resultados,true);
			writer.close();
			
			
		}
		catch (SQLException e){ 					
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}finally{
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return archivoSalida;
			}
    	}
		
		return archivoSalida;
	}/* Fin buscarTransaccionRecuperacionCargosIncoming */
	
	/**
	 * Metodo que consulta las transacciones de recuperacion de cobro de cargos de outgoing.
	 * @return Lista de transacciones de recuperacion de cobro de cargos outgoing.
	 * @throws Mensaje de excepcion controlado.
	 * @param numeroTarjeta numero de tarjeta.
	 * @param fechaInicio fecha de inicio.
	 * @param fechaTermino fecha de termino.
	 * @param codigoTransaccion codigo de transaccion.
	 */
	public Vector<TransaccionBean> buscarTransaccionRecuperacionCargosOutgoing(String numeroTarjeta, String fechaInicio, String fechaTermino, String codigoTransaccion, String op, String numPagina){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		TransaccionBean transaccionBean = new TransaccionBean(); 
		String listaTransaccion="";
		
		try {
			
			log.info("Method: buscarTransaccionRecuperacionCargosOutgoing - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + " fechaInicio = " + fechaInicio + " fechaTermino = " + fechaTermino + " codigoTransaccion = " + codigoTransaccion);
			log.info("Ejecutando SP - SP_SIG_TRANSACRECUPOUTPG");
			int pagActual;
			
			if(numPagina == null)
			{
				pagActual = 1;
			}else{
				pagActual = Integer.parseInt(numPagina);
			}
			this.setPagActual(pagActual);
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACRECUPOUTPG(?, ?, ?, ?, ?, ?, ?,?,?,?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("codigoTransaccion", codigoTransaccion);
			
			stmtProcedure.setInt("numPagina", pagActual);
			stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

			float totReg = stmtProcedure.getInt("totReg");
			float ultimaPagina;

			ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
			this.setUltimaPagina((int) ultimaPagina);
			
			String auxMonto = null;
			while(resultados.next())
			{
			    auxMonto = "";
				transaccionBean = new TransaccionBean(); 
				String valor = resultados.getString("COMERCIOPAIS");
				
				
				String comercio = "";
				String regionpais     = "";
				String region = "";
				String provincia   = "";
				String pais        = "";
				String[] comerciopais;
				
				if(valor != null){
					if (valor.length() == 36 || valor.length() == 43) {
                        comerciopais = valor.split("\\|");
                        comercio = comerciopais[0];
                        regionpais = comerciopais[1];
                        region = comerciopais[1];
                        provincia = comerciopais[1];
                        pais = comerciopais[2];
                    } else {
                        comerciopais = valor.split("\\|");
                        comercio = comerciopais[0];
                        regionpais = comerciopais[3];
                        region = regionpais.substring(0, 9);
                        provincia = regionpais.substring(10, 13);
                        pais = regionpais.substring(13);
                    }
				}
				
				transaccionBean.setMit(resultados.getString("MIT"));
				transaccionBean.setCodigoFuncion(resultados.getString("CODIGO_FUNCION"));
				transaccionBean.setNumeroTarjeta(resultados.getString("NRO_TARJETA"));
				transaccionBean.setFechaTransac(resultados.getString("FECHATRANSAC"));
				transaccionBean.setComercio(comercio);
				transaccionBean.setPais(pais);
				transaccionBean.setMontoTransac(resultados.getString("MONTO_TRANSAC"));
				transaccionBean.setMontoFacturacion(resultados.getString("MONTO_FACTURACION"));
				transaccionBean.setCodRazon(resultados.getString("CODRAZON"));
				transaccionBean.setMicroFilm(resultados.getString("MICROFILM"));
				transaccionBean.setCodigoAutorizacion(resultados.getString("CODIGO_AUTORIZACION"));
				transaccionBean.setFechaEfectiva(resultados.getString("FECHAEFECTIVA"));
				
				fechaProceso = resultados.getString("FECHAPROCESO");
				fechaGregoriana = this.fromJulian(fechaProceso);
				fechaProcesoFinal =  fechaGregoriana[2]+"/" + fechaGregoriana[1] + "/" +fechaGregoriana[0];

				transaccionBean.setFechaProceso(fechaProcesoFinal);
				transaccionBean.setBinAdquiriente(resultados.getString("BINADQUIRENTE"));
				transaccionBean.setLeeBanda(resultados.getString("LEEBANDA"));
				transaccionBean.setEstadoTrx(resultados.getString("ESTADOTRX"));
				transaccionBean.setOtrosDatos1(resultados.getString("OTRODATO1"));
				transaccionBean.setOtrosDatos2(resultados.getString("OTRODATO2"));
				transaccionBean.setOtrosDatos3(resultados.getString("OTRODATO3"));
				transaccionBean.setOtrosDatos4(resultados.getString("OTRODATO4"));
				transaccionBean.setCodMonedaTrx(resultados.getString("CODMONEDATRX"));
				transaccionBean.setRubroComercio(resultados.getString("RUBROCOMERCIO"));
				transaccionBean.setSid(resultados.getString("SID"));
				transaccionBean.setGlosaGeneral(resultados.getString("GLOSAGENERAL"));
				transaccionBean.setReferencia(resultados.getString("REFERENCIA"));
				transaccionBean.setMontoConciliacion(resultados.getString("MONTO_CONCILIACION"));
				
				 // Formateo de Monto Conciliacion
                auxMonto = Utils.formateaMonto( transaccionBean.getMontoConciliacion());
                transaccionBean.setMontoConciliacion( auxMonto );
                
                // formateo de monto Monto Facturacion
                auxMonto = Utils.formateaMonto( transaccionBean.getMontoFacturacion());
                transaccionBean.setMontoFacturacion(auxMonto);
                
                // formateo de Monto de Transaccion
                auxMonto = Utils.formateaMonto( transaccionBean.getMontoTransac());
                transaccionBean.setMontoTransac(auxMonto);
				
				
				transaccionBeanVector.add(transaccionBean);
				
				
				log.info("Method: listaRechazoObjecion - "+listaTransaccion);
			}
			
			resultados.close();
			stmtProcedure.close();
			
			
		}
		catch (SQLException e){ 					
			//bdOracle.setResultado(resultados);
			//bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		}finally{
    		
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return transaccionBeanVector;
			}
    	}
		
		return transaccionBeanVector;
	}/* Fin buscarTransaccionRecuperacionCargosOutgoing */
	
	

	public File buscarTransaccionRecuperacionCargosOutgoingExport(String numeroTarjeta, String fechaInicio, String fechaTermino, String codigoTransaccion, String op, String ruta){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		Object[] o = new Object[2];
		String listaTransaccion="";
		File archivoSalida = new File(ruta);
		try {
			
			log.info("Method: buscarTransaccionRecuperacionCargosOutgoing - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + " fechaInicio = " + fechaInicio + " fechaTermino = " + fechaTermino + " codigoTransaccion = " + codigoTransaccion);
			log.info("Ejecutando SP - SP_SIG_TRANSACACCIONRECUPOUT");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACACCIONRECUPOUT(?, ?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("codigoTransaccion", codigoTransaccion);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

			int codigoError = stmtProcedure.getInt("cod_error");
			
			CSVWriter writer = new CSVWriter(new FileWriter(ruta), ';');
			writer.writeAll(resultados,true);
			writer.close();
			
			//resultados.close();
			stmtProcedure.close();
			
			
		}
		catch (SQLException e){ 					
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}finally{
    		
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return archivoSalida;
			}
    	}
		
		return archivoSalida;
	}/* Fin buscarTransaccionRecuperacionCargosOutgoing */
	
	/**
	 * Metodo que consulta las transacciones de confirmacion de recepcion de peticion de vales.
	 * @return Lista de transacciones de confirmacion de recepcion de peticion de vales.
	 * @throws Mensaje de excepcion controlado.
	 * @param numeroTarjeta numero de tarjeta.
	 * @param fechaInicio fecha de inicio.
	 * @param fechaTermino fecha de termino.
	 * @param codigoTransaccion codigo de transaccion.
	 */
	public Vector<TransaccionBean> buscarTransaccionConfirmacionRecepcionPeticionVales(String numeroTarjeta, String fechaInicio, String fechaTermino, String vkey, String numPagina, String tipoBusqueda){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		TransaccionBean transaccionBean = new TransaccionBean(); 
		String listaTransaccion="";
		
		try {
			
			log.info("Method: buscarTransaccionConfirmacionRecepcionPeticionVales - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta +
																											" fechaInicio = " + fechaInicio + 
																											" fechaTermino = " + fechaTermino + 
																											" codigoTransaccion = " + vkey +
																											" tipoBusqueda = " +  tipoBusqueda);
			log.info("Ejecutando SP - SP_SIG_TRANSACRECPETVALPG");
			int pagActual;
			
			if(numPagina == null)
			{
				pagActual = 1;
			}else{
				pagActual = Integer.parseInt(numPagina);
			}
			this.setPagActual(pagActual);
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACRECPETVALPG(?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("vxkey", vkey);
			stmtProcedure.setString("tipoBusqueda", tipoBusqueda);
			stmtProcedure.setInt("numPagina", pagActual);
			stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			float totReg = stmtProcedure.getInt("totReg");
			float ultimaPagina;

			ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
			this.setUltimaPagina((int) ultimaPagina);
			
            int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){

			    String montoAux;
				while(resultados.next())
				{
					transaccionBean = new TransaccionBean(); 
					String valor = resultados.getString("COMERCIOPAIS");
					montoAux = "";
					
					String comercio = "";
					String regionpais     = "";
					String region = "";
					String provincia   = "";
					String pais        = "";
					String[] comerciopais;
					
					if(valor != null){
						if (valor.length() == 36 || valor.length() == 43) {
                            comerciopais = valor.split("\\|");
                            comercio = comerciopais[0];
                            regionpais = comerciopais[1];
                            region = comerciopais[1];
                            provincia = comerciopais[1];
                            pais = comerciopais[2];
                        } else {
                            comerciopais = valor.split("\\|");
                            comercio = comerciopais[0];
                            regionpais = comerciopais[3];
                            region = regionpais.substring(0, 9);
                            provincia = regionpais.substring(10, 13);
                            pais = regionpais.substring(13);
                        }
					}
					
					transaccionBean.setMit(resultados.getString("MIT"));
					transaccionBean.setCodigoFuncion(resultados.getString("CODIGO_FUNCION"));
					transaccionBean.setNumeroTarjeta(resultados.getString("NRO_TARJETA"));
					transaccionBean.setFechaTransac(resultados.getString("FECHATRANSAC"));
					transaccionBean.setComercio(comercio);
					transaccionBean.setPais(pais);
					transaccionBean.setMontoTransac(resultados.getString("MONTO_TRANSAC"));
					transaccionBean.setMontoFacturacion(resultados.getString("MONTO_FACTURACION"));
					transaccionBean.setCodRazon(resultados.getString("CODRAZON"));
					transaccionBean.setMicroFilm(resultados.getString("MICROFILM"));
					transaccionBean.setCodigoAutorizacion(resultados.getString("CODIGO_AUTORIZACION"));
					transaccionBean.setFechaEfectiva(resultados.getString("FECHAEFECTIVA"));
					
					fechaProceso = resultados.getString("FECHAPROCESO");
					fechaGregoriana = this.fromJulian(fechaProceso);
					fechaProcesoFinal =  fechaGregoriana[2]+"/" + fechaGregoriana[1] + "/" +fechaGregoriana[0];

					transaccionBean.setFechaProceso(fechaProcesoFinal);
					transaccionBean.setBinAdquiriente(resultados.getString("BINADQUIRENTE"));
					transaccionBean.setLeeBanda(resultados.getString("LEEBANDA"));
					transaccionBean.setEstadoTrx(resultados.getString("ESTADOTRX"));
					transaccionBean.setOtrosDatos1(resultados.getString("OTRODATO1"));
					transaccionBean.setOtrosDatos2(resultados.getString("OTRODATO2"));
					transaccionBean.setOtrosDatos3(resultados.getString("OTRODATO3"));
					transaccionBean.setOtrosDatos4(resultados.getString("OTRODATO4"));
					transaccionBean.setCodMonedaTrx(resultados.getString("CODMONEDATRX"));
					transaccionBean.setRubroComercio(resultados.getString("RUBROCOMERCIO"));
					transaccionBean.setSid(resultados.getString("SID"));
					transaccionBean.setGlosaGeneral(resultados.getString("GLOSAGENERAL"));
					transaccionBean.setReferencia(resultados.getString("REFERENCIA"));
					transaccionBean.setMontoConciliacion(resultados.getString("MONTO_CONCILIACION"));
					transaccionBean.setNumIncidente(resultados.getString("NUM_INCIDENTE"));
					transaccionBean.setIdUsuario(resultados.getString("USUARIO"));
					transaccionBean.setFechaGestion(resultados.getString("FECHA_GESTION"));
					transaccionBean.setOperador(resultados.getString("OPERADOR"));
					
					// Formateo de monto Conciliacion
	                montoAux = Utils.formateaMonto( transaccionBean.getMontoConciliacion());
	                transaccionBean.setMontoConciliacion(montoAux);
	                
	                // Formateo de monto Facturacion
	                montoAux =  Utils.formateaMonto(transaccionBean.getMontoFacturacion());
	                transaccionBean.setMontoFacturacion(montoAux);
	                
	                // Formateo de monto MontoTransac
	                montoAux =  Utils.formateaMonto(transaccionBean.getMontoTransac());
	                transaccionBean.setMontoTransac(montoAux);
					
					transaccionBeanVector.add(transaccionBean);
					
					
					log.info("Method: listaRechazoObjecion - "+listaTransaccion);
				}
				
			}else{
				listaTransaccion="esttrx|1~MSG|Problemas al leer las confirmaciones de recepcion de peticion de vales...~";
				log.error("Problemas al leer las confirmaciones de recepcion de peticion de vales...");
			}
			
			resultados.close();
			stmtProcedure.close();
			
			
		}
		catch (SQLException e){ 					
			//bdOracle.setResultado(resultados);
			//bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		}finally{
    		
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return transaccionBeanVector;
			}
    	}
		
		return transaccionBeanVector;
	}/* Fin buscarTransaccionConfirmacionRecepcionPeticionVales */
	

	public File buscarTransaccionConfirmacionRecepcionPeticionValesExport(String numeroTarjeta, String fechaInicio, String fechaTermino, String codigoTransaccion, String op, String ruta){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		
	    
	    File archivoSalida = new File(ruta);
		String listaTransaccion="";
		
		try {
			
			log.info("Method: buscarTransaccionConfirmacionRecepcionPeticionVales - Parametros Entrada : " + " numeroTarjeta = " + numeroTarjeta + " fechaInicio = " + fechaInicio + " fechaTermino = " + fechaTermino + " codigoTransaccion = " + codigoTransaccion);
			log.info("Ejecutando SP - SP_SIG_TRANSACCIONCONRECPETVAL");
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACCIONCONRECPETVAL(?, ?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setString("vxkey", codigoTransaccion);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
            int codigoError = stmtProcedure.getInt("cod_error");	
	
			
			
			
			CSVWriter writer = new CSVWriter(new FileWriter(ruta), ';');
			writer.writeAll(resultados,true);
			writer.close();
			
			//resultados.close();
			stmtProcedure.close();
			
			
		}
		catch (SQLException e){ 					
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			log.error(e.getMessage(), e);
        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}finally{
    		
    		if (!bdOracle.cerrarConexionBD()){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				 return archivoSalida;
			}
    	}
		
		return archivoSalida;
	}/* Fin buscarTransaccionConfirmacionRecepcionPeticionVales */
	
		
	 public static String[] fromJulian(String injulian) {
		 
		 String miF = "";
		 miF = "" + injulian;
		 
		 String ano = miF.substring(0, 1);
		 int anoTx = Integer.parseInt(ano);
		 
		 
		 
		 Calendar c1 = Calendar.getInstance();
		 Calendar c2 = new GregorianCalendar();
		 
		 int anoActual = c2.get(Calendar.YEAR);
		 
		 
		 String anoActualS = anoActual + "";
		 
		 String anoActualF = anoActualS.substring(3, 4);
		 
		 int anoActualInt = Integer.parseInt(anoActualF);
		 
		 
		 String anoActualFinalS = "";
		 if(anoActualInt<anoTx){
			 anoActual = anoActual - 10;
			 anoActualFinalS = (anoActual + "").substring(0, 3) + anoTx;
		 }else{
			 anoActualFinalS = (anoActual + "").substring(0, 3) + anoTx;
		 }
		 
		 int diasJulianos = Integer.parseInt(miF.substring(1, 4));
		 
		 Calendar fechaFinal = new GregorianCalendar(Integer.parseInt(anoActualFinalS),0,0);
		 
		 fechaFinal.add(Calendar.DATE, diasJulianos);
		 
		 String diaFinal = "";
		 if(fechaFinal.get(Calendar.DAY_OF_MONTH)<10)
			 diaFinal = "0"+fechaFinal.get(Calendar.DAY_OF_MONTH);
		 else{
			 diaFinal = ""+fechaFinal.get(Calendar.DAY_OF_MONTH);
		 }
		 String mesFinal = "";
		 int mesTX = fechaFinal.get(Calendar.MONTH) + 1;
		if(mesTX<10)
			mesFinal = "0"+mesTX;
		 else
			 mesFinal= ""+mesTX;
			 
		 
		   return new String[] {fechaFinal.get(Calendar.YEAR)+"",mesFinal , diaFinal};
		}
	 
		public int getUltimaPagina() {
			return ultimaPagina;
		}

		public void setUltimaPagina(int ultimaPagina) {
			this.ultimaPagina = ultimaPagina;
		}

		public int getPagActual() {
			return pagActual;
		}

		public void setPagActual(int pagActual) {
			this.pagActual = pagActual;
		}
	
		public Vector<TransaccionOnusBean> buscarTransaccionOnUsVisa(
				String numeroTarjeta, String fechaInicio, String fechaTermino,
				String op, String numPagina, String codFunc) {
			Connection conexion=null;
			db.OracleBD bdOracle=null;
			CallableStatement stmtProcedure=null;
			ResultSet resultados = null;

			String listaTransaccion="";
			TransaccionOnusBean transaccionBean = null; 
			
			try {
				
				int pagActual;
				
				if(numPagina == null)
				{
					pagActual = 1;
				}else{
					pagActual = Integer.parseInt(numPagina);
				}
				this.setPagActual(pagActual);
				
				bdOracle = new OracleBD();
				conexion = bdOracle.conectar();
				bdOracle.setConexion(conexion);
				
				
				log.info("Method: buscarTransaccionPresentacion - Parametros Entrada : "
						+ " numeroTarjeta = "
						+ numeroTarjeta
						+ " fechaInicio = "
						+ fechaInicio + " fechaTermino = " + fechaTermino
						+ " codTransac = " + op
						+ " codFunc = " + codFunc);
				log.info("Ejecutando SP - SP_SIG_TRANSAC_ONUS_VISA");
				
				stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSAC_ONUS_VISA(?,?,?,?,?,?,?,?,?,?,?); END;");
				
				if (numeroTarjeta.length()==0){
					numeroTarjeta="0";
				}
				stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
				stmtProcedure.setString("fechaInicio", fechaInicio);
				stmtProcedure.setString("fechaTermino", fechaTermino);
				stmtProcedure.setInt("numPagina", pagActual);
				stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
				stmtProcedure.setString("codTransac", op);
				stmtProcedure.setString("codFuncion", codFunc);
				
				stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
				stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
				stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
				stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
				
				stmtProcedure.execute();
				
				//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
				resultados= (ResultSet) ((stmtProcedure).getObject("prfCursor"));
				
				int error = stmtProcedure.getInt("cod_error");
				
				if(error != 100)
				{
					log.info("Method: listaRechazoObjecion - Obteniendo resultados");
					
					float totReg = stmtProcedure.getInt("totReg");
					float ultimaPagina;

					ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
					this.setUltimaPagina((int) ultimaPagina);
					
					listaTransaccion="esttrx:0~";
					
					String montoAux = "";
					String cuotaAux = "";
					while(resultados.next())
					{
						transaccionBean = new TransaccionOnusBean();
						transaccionBean.setRegistro(resultados.getLong("registro"));
						transaccionBean.setCodigoTransaccion(resultados.getString("CODIGO_TRANSACCION"));
						transaccionBean.setNumCuotas(resultados.getString("NUM_CUOTAS"));
						transaccionBean.setNumeroTarjeta(resultados.getString("NRO_TARJETA"));
						transaccionBean.setFechaCompra(resultados.getString("FECHA_COMPRA"));
						transaccionBean.setTipoVenta(resultados.getString("TIPO_VENTA"));
						transaccionBean.setMontoTransaccion(resultados.getString("MONTO_TRANSAC"));
						transaccionBean.setNumMicroFilm(resultados.getString("MICROFILM"));
						transaccionBean.setCodAutor(resultados.getString("CODIGO_AUTORIZACION"));
						transaccionBean.setFechaPosteo(resultados.getString("FECHA_POSTEO"));
						transaccionBean.setFechaAutorizacion(resultados.getString("FECHA_AUTORIZACION"));
						transaccionBean.setRubroComercio(resultados.getString("RUBROCOMERCIO"));
						transaccionBean.setSid(resultados.getLong("SID"));
						transaccionBean.setNumComercio(resultados.getString("NUM_COMERCIO"));
						transaccionBean.setValorCuota(resultados.getString("VALOR_CUOTA"));
						transaccionBean.setNombreComercio(resultados.getString("NOMBRE_COMERCIO"));
						transaccionBean.setCiudadComercio(resultados.getString("CIUDAD_COMERCIO"));
						
						// Formateo de monto TRX
		                montoAux =  Utils.formateaMonto(transaccionBean.getMontoTransaccion());
		                transaccionBean.setMontoTransaccion(montoAux);
		                						
		                // Formateo del valor de la cuota
		                cuotaAux = Utils.formateaMonto(transaccionBean.getValorCuota());
		                transaccionBean.setValorCuota(cuotaAux);
		                						
						
						transaccionOnUsBeanVector.add(transaccionBean);
					
					}
					stmtProcedure.close();
				}else{
					log.info("Method: listaRechazoObjecion - Lista vac�a");
				}
				
				resultados.close();
				stmtProcedure.close();
				
				
			}
			catch (SQLException e){ 					

				bdOracle.cerrarStatement(stmtProcedure);
				log.error(e.getMessage(), e);
	        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
			}finally{
	    		
	    		if (!bdOracle.cerrarConexionBD()){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
					 transaccionBean.setError("esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~");
					 transaccionOnUsBeanVector.add(transaccionBean);
					 return transaccionOnUsBeanVector;
				}
	    	}
			
			return transaccionOnUsBeanVector;
		}
	
		/**
		 * 
		 * @param numeroTarjeta
		 * @param fechaInicio
		 * @param fechaTermino
		 * @param codigoTransaccion
		 * @param codigoFuncion
		 * @param op
		 * @param ruta
		 * @return
		 */
	public File buscarTransaccionOnUsExport(String numeroTarjeta,
			String fechaInicio, String fechaTermino, String codigoTransaccion,
			String codFunc, String op, String ruta) {

			
			Connection conexion=null;
			db.OracleBD bdOracle=null;
			CallableStatement stmtProcedure=null;
			ResultSet resultados = null;
			
			String listaTransaccion="";
			
			File archivoSalida = new File(ruta);
			
			try {
				
				log.info("Method: buscarTransaccionPresentacion - Parametros Entrada : "
						+ " numeroTarjeta = "
						+ numeroTarjeta
						+ " fechaInicio = "
						+ fechaInicio + " fechaTermino = " + fechaTermino
						+ " codTransac = " + op
						+ " codFunc = " + codFunc);
				log.info("Ejecutando SP - SP_SIG_TRANSAC_ON_US_EXPORT");
				
				
				
				bdOracle = new OracleBD();
				conexion = bdOracle.conectar();
				bdOracle.setConexion(conexion);
				
				stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSAC_ON_US_EXPORT(?,?,?,?,?,?,?,?); END;");
				
				if (numeroTarjeta.length()==0){
					numeroTarjeta="0";
				}
				stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
				stmtProcedure.setString("fechaInicio", fechaInicio);
				stmtProcedure.setString("fechaTermino", fechaTermino);
				stmtProcedure.setString("codTransac", op);
				stmtProcedure.setString("codigoFuncion", codFunc);
				stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
				stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
				stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
				
				stmtProcedure.execute();
				
				//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
				resultados= (ResultSet) ((stmtProcedure).getObject("prfCursor"));
				
				int codigoError = stmtProcedure.getInt("cod_error");
				
				// ANTIGUA FORMA DE EXPORTAR
//				CSVWriter writer = new CSVWriter(new FileWriter(ruta), ';');
//				writer.writeAll(resultados,true);
//				writer.close();
				
				// NUEVA FORMA DE EXPORTAR POR PROBLEMA DE FORMATEO DE PUNTOS POR COMA
				exportarExcelOnus( resultados, ruta );
				
				//resultados.close();
				stmtProcedure.close();
				
				
			}
			catch (SQLException e){ 					
				bdOracle.setResultado(resultados);
				bdOracle.cerrarResultSet();
				bdOracle.cerrarStatement(stmtProcedure);
				log.error(e.getMessage(), e);
	        		listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
			}finally{
	    		
	    		if (!bdOracle.cerrarConexionBD()){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
					 listaTransaccion = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
					 return archivoSalida;
				}
	    	}
			
			return archivoSalida;
		}/* Fin buscarTransaccionOnUsExport */
	
	
	 /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param rs2
     * @since 1.X
     */
    private void exportarExcelOnus( ResultSet rs, String ruta ) {
        List<String[]> dataList = new ArrayList<String[]>();
        CSVWriter writer = null;
        
        try {
            // GENERA CABECERA
            String[] header = generateHeaderToExcelTrxOnus();
            
            // PARSEO DE DATOS DEL EXCEL
            while ( rs.next() ) {
                dataList.add( getRegistrosToExcelTrxOnus( rs ) );
            }
            
            writer = new CSVWriter( new FileWriter( ruta ), ';' );
            writer.writeNext( header );
            writer.writeAll( dataList );
            
        }
        catch ( SQLException e ) {
            log.error( e.getMessage(), e );
        }
        catch ( IOException e ) {
            log.error( e.getMessage(), e );
        }
        finally {
            if ( writer != null ) {
                try {
                    writer.close();
                }
                catch ( IOException e ) {
                    log.error( e.getMessage(), e );
                }
            }
        }
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private String[] generateHeaderToExcelTrxOnus() {
       return new String[]{
              "NRO. TARJETA",
              "FECHA COMPRA",
              "FECHA AUTORIZACION",
              "TIPO VENTA",
              "NUM-CUOTAS",
              "MONTO TRANSACCION",
              "VALOR CUOTA",
              "COD AUTOR",
       };
 
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) -  versi�n inicial 
     * </ul>
     * <p>
     * 
     * @param rs
     * @return
     * @since 1.X
     */
    private String[] getRegistrosToExcelTrxOnus( ResultSet rs ) {
        try {
            String registro = "";
            registro += rs.getString( "NUMERO_TARJETA") + ";";
            registro += rs.getString( "FECHATRANSAC" )+ ";";
            registro += rs.getString( "FECHA_AUTORIZACION" )+";";
            registro += rs.getString( "TIPO_VENTA" )+ ";";
            registro += rs.getString( "NUM_CUOTAS" )+ ";";
            registro += Utils.formateaMonto( rs.getString( "MONTO_TRANSAC" ))+ ";";
            registro += Utils.formateaMonto(rs.getString( "VALOR_CUOTA" ))+ ";";
            registro += rs.getString( "COD_AUTOR" )+ ";";
           
            
            return registro.split( ";" );
        }
        catch ( SQLException e ) {
          log.error( e.getMessage(), e );
        }
        return null;
    }
			
			
}