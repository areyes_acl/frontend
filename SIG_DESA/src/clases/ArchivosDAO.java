package clases;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import Beans.ParametroBean;
import cl.filter.FiltroArchivo;
import cl.util.file.FileUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 1- IR A BUSCAR LAS RUTAS DE LOS PARAMETROS DEL PROCESO (OK) 1.1- OBTIEN RUTA
 * DE ARCHIVOS DE BKP (OK)
 * 
 * FILTRAR POR FECHAS
 * 
 * 2- OBTENER LOS ARCHIVOS DENTRO DEL DIRECTORIO (OK)
 * 
 * 
 * 3- EXPONER LOS RESULTADOS
 * 
 * 4- PERMITIR DESCARGA DE ARCHIVOS
 * 
 * 5- DESCARGAR ARCHIVOS
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ArchivosDAO {
    static final Category log = Logger.getLogger(ArchivosDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Se realiza la simulacion de los parametros obtenidos desde BD
     * 
     * @since 1.X
     */
    public List<File> obtieneArchivos(FiltroArchivo filtro) {
	//log.info(filtro);
	CommonsDAO commonDAO = new CommonsDAO();
	List<File> listaArchivos = new ArrayList<File>();
	
	// Obtiene la lista de parametros
	List<ParametroBean> rutasArchivos = commonDAO.getParameterByCode(filtro
		.getTipoArchivo().getCodigoRutaArchivo());
	
	// Obtiene la lista de parametro
	List<ParametroBean> listStarWidth = commonDAO.getParameterByCode(filtro
			.getTipoArchivo().getCodigoStarWith());
	log.info(filtro
		.getTipoArchivo().getCodigoStarWith());
	log.info(listStarWidth.get(0).getValor());
	
	
	
	filtro.setStarwith(listStarWidth.get(0).getValor());
	
	// Obtiene lista de archivos
	for (ParametroBean ruta : rutasArchivos) {
	    log.info(ruta.getValor()+"-"+filtro);
	    listaArchivos = FileUtils.getFilesByFilter(ruta.getValor(),filtro);
	}
	

	return listaArchivos;

    }

}
