/**
 * Descripci�n: Clase para cambios de contrase�a
 * @param sidusuario SID usuario
 * @param passActual Contrase�a a cambiar
 * @param passNueva1 Nueva contrase�a
 * 
 * @return Devuelve mensaje por error u ok
 */

package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionContext;

import constant.DatabaseConstant;

import Beans.ContrasenaLogic;
import Beans.IContrasena;
import Beans.PasswordUtils;
import Beans.Usuario;

import properties.PropertiesConfiguration;
import db.OracleBD;

public class CambioContrasenaDAO {

    static final Logger log = Logger.getLogger(CambioContrasenaDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    private String esquema;

    private static final String SP_SIG_GUARDAR_CONTRASENA = "SP_SIG_GUARDAR_CONTRASENA(?,?,?)";

    private db.OracleBD bdOracle;
    private Connection conexion;

    private Statement st;
    private ResultSet rs;
    private String respCambioContrasena;
    private OracleBD obd = new OracleBD();

    private EncriptacionClave enc;
    public Map session;

    public void setConexion() {
	this.conexion = obd.conectar();
	obd.setResultado(rs);
    }

    public Connection getConexion() {
	return conexion;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     * Constructor de clase
     * 
     * @since 1.X
     */
    public CambioContrasenaDAO() {
	Properties propiedades = null;

	this.session = ActionContext.getContext().getSession();

	propiedades = PropertiesConfiguration.getProperty();

	esquema = propiedades.getProperty("esquema");

	enc = new EncriptacionClave();

	bdOracle = new OracleBD();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     * 
     * @param rut
     * @return
     * @since 1.X
     */
    public String checkUsarioRut(String rut) {
	setConexion();
	// verificar si usuario existe y tiene permisos
	Integer idUsuario = 0;
	Integer rutUsuario = 0;
	String digitoVerificador = "";
	String strsql = "SELECT U.SID AS SIDUSUARIO, U.RUT, U.DV, U.NOMBRE, U.APEPAT, U.APEMAT, U.ESTADO, U.INTENTOS, PRTS.VALOR "
		+ "FROM "+DatabaseConstant.ESQUEMA+"."+DatabaseConstant.TABLA_USUARIO+" U "
		+ "INNER JOIN TBL_POLITICAS_USUARIO_PRTS PRTS ON PRTS.NOMBRE_PARAMETRO = 'NRO_REINTENTOS_BLOQUEAR_CUENTA' "
		+ "WHERE CONCAT(U.RUT, U.DV) = '" + rut + "' AND (U.ESTADO = 1 OR (U.ESTADO = 2 AND U.INTENTOS > 0))";

	try {
	    rs = obd.consultar(strsql);
	    obd.setResultado(rs);
	    if (rs.next()) {
		idUsuario = rs.getInt("SIDUSUARIO");
		rutUsuario = rs.getInt("U.RUT");
		digitoVerificador = rs.getString("U.DV");
	    } else {
		log.warn("Usuario inexistente o inactivo.");
	    }
	    obd.cerrarResultSet();
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

	if (obd.cerrarResultSet() == false) {
	    log.error("Error: No se pudo cerrar resulset.");
	    if (obd.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	} else {
	    if (obd.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	}

	try {
	    if (conexion != null && !conexion.isClosed()) {
		conexion.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

	if (idUsuario > 0) {
	    Usuario usuarioLog = new Usuario();
	    usuarioLog.setSid(Long.valueOf(idUsuario));
	    usuarioLog.setPreguntas(0);
	    usuarioLog.setActualizar(0);
	    usuarioLog.setRut(rutUsuario);
	    usuarioLog.setDv(digitoVerificador);
	    session.put("usuarioLog", usuarioLog);
	    return "SUCCESS";
	}else{
	    return "EMPTY";
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo encargado de realizar el cambio de una contrase�a
     * 
     * @param sidusuario
     * @param passActual
     * @param passNueva1
     * @return
     * @since 1.X
     */
    public String aplicarCambioContrasena(String sidusuario, String passActual, String passNueva1) {
	log.info("Method: aplicarCambioContrasena - Aplicando cambio contrase�a: sidusuario-> " + sidusuario);

	try {
	    String aux = null;
	    String actual = enc.getStringMessageDigest(passActual, enc.SHA256);
	    String aux2 = enc.getStringMessageDigest(passNueva1, enc.SHA256);
	    Integer idContrasena = 0;
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);
	    bdOracle.setResultado(rs);

	    st = conexion.createStatement();
	    rs = st.executeQuery("SELECT CONTRA.CONTRASENA AS CLAVE, CONTRA.SID FROM SIG.TBL_CONTRASENA CONTRA "
		    + " WHERE CONTRA.SID_USR = " + sidusuario
		    + " AND CONTRA.SID_CONTRASENA_ESTADO > 1");

	    log.info("Method: aplicarCambioContrasena - Validando credenciales...");

	    this.respCambioContrasena = "0;Cambio realizado correctamente.";

	    while (rs.next()) {
		aux = rs.getString(1);
		idContrasena = rs.getInt(2);
	    }

	    if (actual.equals(aux)) {
		// la clave anterior es correcta, proceso cambiar clave
		// Actualizar contrase�a anterior a estado = 1
		// st.execute("update "+ esquema
		// +".TBL_CONTRASENA set SID_CONTRASENA_ESTADO = 1 where SID = "+
		// idContrasena);
		// hacer insert nueva contrase�a

		// Se llama a SP que realiza la actualizacion de la clave
		callSPModificarPassword(sidusuario, aux2);

	    } else {
		this.respCambioContrasena = "1;Password inicial no coincide. Ingrese nuevamente.";
		log.warn("Method: aplicarCambioContrasena - " + respCambioContrasena);
	    }
	} catch (SQLException e) {
	    this.respCambioContrasena = "2;Ha ocurrido un problema en el cambio de contrase�a";
	    log.error(respCambioContrasena);
	    log.error(e.getMessage(), e);
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }

	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return this.respCambioContrasena;
    }

    public String crearNuevaContrasena(String sidusuario, String passActual) {
	log.info("Method: crearNuevaContrasena - Aplicando cambio contrase�a: sidusuario-> " + sidusuario);

	this.respCambioContrasena = "0;Cambio realizado correctamente.";

	try {
	    String aux = null;
	    String actual = enc.getStringMessageDigest(passActual, enc.SHA256);

	    Integer idContrasena = 0;
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);
	    bdOracle.setResultado(rs);

	    st = conexion.createStatement();

	    // Se llama a SP que realiza la actualizacion de la clave
	    callSPModificarPassword(sidusuario, actual);
	    
	    respCambioContrasena = "0;Cambio realizado correctamente.";

	} catch (SQLException e) {
	    this.respCambioContrasena = "2;Ha ocurrido un problema en el cambio de contrase�a";
	    log.error(respCambioContrasena);
	    log.error(e.getMessage(), e);
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }

	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return this.respCambioContrasena;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Sp que dado el sid del usuario y la password actual realiza el updateo de la password en Base de datos
     * @param sidusuario
     * @param actual
     * @throws SQLException 
     * @since 1.X
     */
    private void callSPModificarPassword(String sidusuario, String actual) throws SQLException{

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet resultados = null;
	int codigoError = 0;

	    log.info(" ===== Se ejecuta el procedimiento : SP_SIG_GUARDAR_CONTRASENA(?,?,?) =====");
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);
	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema + "."
		    + SP_SIG_GUARDAR_CONTRASENA + "; END;");
	    stmtProcedure.setLong("p_sid_usuario", Long.parseLong(sidusuario));
	    stmtProcedure.setString("p_contrasena", actual);
	    stmtProcedure.setString("aplicativo", "usuario");

	    stmtProcedure.execute();

	    // codigoError = stmtProcedure.getInt("cod_error");
	    // String warning = stmtProcedure.getString("warning");

	    log.info("======= RESULTADOS SP_SIG_GUARDAR_CONSTRASENA ========");
	    log.info("codigoError : " + codigoError);

	    log.info("Method: aplicarCambioContrasena - " + respCambioContrasena);
	    
	    cerrarConexiones(bdOracle, resultados, conexion, stmtProcedure);
    }

  
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     * Metodo encargado de cerrar las conexioness
     * 
     * @param oracleBD
     * @param rs
     * @param conexion
     * @param stmtProcedure
     * @since 1.X
     */
    private void cerrarConexiones(OracleBD oracleBD, ResultSet rs,
	    Connection conexion, CallableStatement stmtProcedure) {
	if (rs != null) {
	    try {
		rs.close();
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	if (oracleBD.cerrarResultSet() == false) {
	    log.error("Error: No se pudo cerrar resulset.");
	    if (oracleBD.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	} else {
	    if (oracleBD.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	}
	try {
	    if (conexion != null && !conexion.isClosed()) {
		conexion.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

	try {
	    if (stmtProcedure != null && !stmtProcedure.isClosed()) {
		stmtProcedure.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

    }
}
