package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import Beans.TDIExecutionEstBean;
import properties.UtilProperties;
import db.OracleBD;

public class TDIExecutionEstDao {
    static final Logger log = Logger.getLogger(TDIExecutionEstDao.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    private OracleBD obd = new OracleBD();

    private String esquema;

    private ResultSet rs;

    private Connection conexion;

    /*
     * Constructor
     */
    public TDIExecutionEstDao() {
	this.esquema = UtilProperties.getProperty("esquema");
    }

    public void setConexion() {
	this.conexion = obd.conectar();
	obd.setResultado(rs);
    }

    public Connection getConexion() {
	return conexion;
    }

    public void setConexion(Connection conexion) {
	this.conexion = conexion;
    }

    public List<TDIExecutionEstBean> findAll() {
	setConexion();
	String valor = "SELECT * FROM " + esquema + ".TBL_TDI_EXECUTION_EST";
	rs = obd.consultar(valor);
	obd.setResultado(rs);
	
	List<TDIExecutionEstBean> lista = new ArrayList<TDIExecutionEstBean>();
	
	try {
	    log.info("Buscando listado de estados de los log - Obteniendo resultados");

	    while (rs.next()) {
		TDIExecutionEstBean tipo = new TDIExecutionEstBean();
		tipo.setSid(Long.valueOf(rs.getInt("ID")));
		tipo.setEstado(rs.getString("ESTADO"));
		lista.add(tipo);
	    }

	    obd.cerrarResultSet();
	} catch (Exception e) {
	    log.error("Error al obtener resultados de los estados de los log");
	    log.error(e);
	}

	cerrarConexion();

	return lista;
    }

    private void cerrarConexion(){
   	if (!obd.cerrarResultSet()) {
   	    log.error("Error: No se pudo cerrar resulset.");
   	    if (!obd.cerrarConexionBD()) {
   		log.error("Error: No se pudo cerrar conexion a la base de datos.");
   	    }
   	} else {
   	    if (!obd.cerrarConexionBD()) {
   		log.error("Error: No se pudo cerrar conexion a la base de datos.");
   	    }
   	}
   	try {
   	    if (conexion != null && !conexion.isClosed()) {
   		conexion.close();
   	    }
   	} catch (SQLException e) {
   	    log.error(e.getMessage(), e);
   	}
       }

    public TDIExecutionEstBean getEstado(String string) {
	setConexion();
	String valor = "SELECT * FROM " + esquema + ".TBL_TDI_EXECUTION_EST WHERE ESTADO = '"+string+"'";
	rs = obd.consultar(valor);
	obd.setResultado(rs);
	
	TDIExecutionEstBean lista = new TDIExecutionEstBean();
	
	try {
	    log.info("Buscando estado - Obteniendo resultados");

	    while (rs.next()) {
		lista.setSid(Long.valueOf(rs.getInt("ID")));
		lista.setEstado(rs.getString("ESTADO"));
	    }

	    obd.cerrarResultSet();
	} catch (Exception e) {
	    log.error("Error al obtener resultados de los estados de los log");
	    log.error(e);
	}

	cerrarConexion();

	return lista;
    }
}
