package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;

import db.OracleBD;

import Beans.Contrasena;
import Beans.Parametros;
import types.EstadoContrasenaType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ContrasenaByUsuarioDAO {
    static final Logger log =  Logger.getLogger(ParametrosDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    private OracleBD obd = new OracleBD();
    private Properties prop = new Properties();
    private ResultSet rs;
    private String esquema;
    private Connection conexion;

    public ContrasenaByUsuarioDAO(){
	setProp();
	this.esquema = prop.getProperty("esquema");
	//setConexion();
    }
    
    public void setConexion(){
	this.conexion = obd.conectar();
	obd.setResultado(rs);
    }
    
    public void setConexion(Connection conexion) {
	this.conexion = conexion;
    }
    
    private void setProp(){
	prop= PropertiesConfiguration.getProperty();
    }

    public List<Contrasena> obtenerContrasenasAnterioresPorUsuario(EstadoContrasenaType estado, Long sidUsuario, Integer cantidadAnteriores){
	setConexion();
	List<Contrasena> listaContrasena = new ArrayList<Contrasena>();
	
	String strsql = "SELECT SID, CONTRASENA FROM (SELECT SID, CON.CONTRASENA AS CONTRASENA FROM TBL_CONTRASENA CON"
		+ " WHERE CON.SID_USR ="
		+ sidUsuario
		+ "  ORDER BY CON.SID DESC)"
		+ " WHERE ROWNUM <= " + cantidadAnteriores;

	rs = obd.consultar(strsql);
	obd.setResultado(rs);

	try {
	    while (rs.next()) {
		Contrasena con = new Contrasena();
//		String contra = rs.getString("CONTRASENA");
//		EncriptacionClave encriptacionClave = new EncriptacionClave();
//		String claveUsuarioEncriptada = encriptacionClave.getStringMessageDigest(contra, encriptacionClave.SHA256);
		con.setPassword( rs.getString("CONTRASENA"));

		listaContrasena.add(con);
	    }
	    
	    obd.cerrarResultSet();
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	}

	if (obd.cerrarResultSet()==false){
	    log.error("Error: No se pudo cerrar resulset.");
	    if (obd.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }	
	}else{
	    if (obd.cerrarConexionBD()==false){
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	}
	try {
	    if(conexion != null && !conexion.isClosed()){
		conexion.close();
	    }
	}catch ( SQLException e ) {
	    log.error( e.getMessage(), e );
	}

	return listaContrasena;
    }

}