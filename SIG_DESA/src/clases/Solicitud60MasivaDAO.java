package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.CargoAbonoDTO;
import Beans.TransaccionBean;
import cl.filter.FiltroTransacciones;
import cl.util.Utils;
import db.OracleBD;
import exception.AppException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Solicitud60MasivaDAO {

	static final Logger log = Logger.getLogger(Solicitud60MasivaDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	private final String PARAM_LIST_CHECKED = "CHECK_SOL60MASIVA";
	private final String SPLIT_BY_COMMA = ",";
	private Properties propiedades;
	private String esquema;

	// paginacion
	private int ultimaPagina = 0;
	private int pagActual;
	private String cantidadRegistros = "";

	/**
	 * Constructor
	 */
	public Solicitud60MasivaDAO() {
		this.propiedades = PropertiesConfiguration.getProperty();
		this.esquema = propiedades.getProperty("esquema");
		cantidadRegistros = propiedades.getProperty("cantidadRegistros");

	}

	public int getPagActual() {
		return pagActual;
	}

	public void setPagActual(int pagActual) {
		this.pagActual = pagActual;
	}

	public int getUltimaPagina() {
		return ultimaPagina;
	}

	public void setUltimaPagina(int ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @param numPagina
	 * @return
	 * @since 1.X
	 */
	public List<TransaccionBean> buscarTransacciones(
			FiltroTransacciones filtro, String numPagina) {

		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		ResultSet resultados = null;
		List<TransaccionBean> listaTransacciones = new ArrayList<TransaccionBean>();
		String storeProcedure = "BEGIN "
				+ esquema
				+ ".SP_SIG_BUSCAR_TRX_SOL60PG(?, ?, ?, ?, ?, ?, ?, ?, ?, ?); END;";

		try {

			log.info("Ejecutando SP - SP_SIG_BUSCAR_TRX_SOL60PG");

			int pagActual;
			if (numPagina == null) {
				pagActual = 1;
			} else {
				pagActual = Integer.parseInt(numPagina);
			}

			this.setPagActual(pagActual);
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);

			stmtProcedure = conexion.prepareCall(storeProcedure);

			// ENTRADA
			stmtProcedure.setString("numeroTarjeta", filtro.getNumeroTarjeta());
			stmtProcedure.setString("fechaInicio", filtro.getFechaDesde());
			stmtProcedure.setString("fechaTermino", filtro.getFechaHasta());
			stmtProcedure.setString("tipoTrx", filtro.getTipoTransaccion()
					.getXkey());
			stmtProcedure.setInt("numPagina", pagActual);
			stmtProcedure
					.setInt("cantReg", Integer.parseInt(cantidadRegistros));

			log.info(filtro);
			log.info("numero de tarjeta : " + filtro.getNumeroTarjeta());
			log.info("tipo : " + filtro.getTipoTransaccion().getXkey());
			log.info("nPAGINA : " + pagActual);
			log.info("cantidadRegistros:" + cantidadRegistros);

			// SALIDA
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);

			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			float totReg = stmtProcedure.getInt("totReg");
			float ultimaPagina;

			ultimaPagina = Utils.calcularUltimaPagina(totReg,
					Integer.parseInt(cantidadRegistros));
			this.setUltimaPagina((int) ultimaPagina);

			// PARSEA LA LISTA DE RESULTADOS
			while (resultados.next()) {
				TransaccionBean tb = parsearResultado(resultados);
				listaTransacciones.add(tb);
			}

			if (listaTransacciones != null && !listaTransacciones.isEmpty()) {
				List<String> listaCodigos = getListToCheck();
				log.info("Codigos de rechazos a chequear : " + listaCodigos);
				validarSiSeDebeCheckearPorDefault(listaCodigos,
						listaTransacciones);
			}

			resultados.close();

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} finally {

			if (stmtProcedure != null) {
				bdOracle.cerrarStatement(stmtProcedure);
			}

			if (!bdOracle.cerrarConexionBD()) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}

			try {
				if (conexion != null && !conexion.isClosed()) {
					conexion.close();
				}
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
			}
		}

		return listaTransacciones;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Metodo que realiza el parse de los datos obtenidos de la consulta al sp
	 * hace un objeto
	 * 
	 * @param resultados
	 * @return
	 * @throws SQLException
	 * @since 1.X
	 */
	private TransaccionBean parsearResultado(ResultSet resultados)
			throws SQLException {

		TransaccionBean transaccionBean = new TransaccionBean();

		String montoAux = "";
		String comercio = "";
		String regionpais = "";
		String region = "";
		String provincia = "";
		String pais = "";
		String[] comerciopais;

		String valor = resultados.getString("COMERCIOPAIS");

		if (valor != null) {
			if (valor.length() == 36) {
				comerciopais = valor.split("\\|");
				comercio = comerciopais[0];
				regionpais = comerciopais[1];
				region = comerciopais[1];
				provincia = comerciopais[1];
				pais = comerciopais[2];
			} else {
				comerciopais = valor.split("\\|");
				comercio = comerciopais[0];
				regionpais = comerciopais[3];
				region = regionpais.substring(0, 9);
				provincia = regionpais.substring(10, 13);
				pais = regionpais.substring(13);
			}
		}

		transaccionBean.setMit(resultados.getString("MIT"));
		transaccionBean
				.setCodigoFuncion(resultados.getString("CODIGO_FUNCION"));
		transaccionBean.setNumeroTarjeta(resultados.getString("NRO_TARJETA"));
		transaccionBean.setFechaTransac(resultados.getString("FECHATRANSAC"));
		transaccionBean.setComercio(comercio);
		transaccionBean.setPais(pais);
		transaccionBean.setMontoTransac(resultados.getString("MONTO_TRANSAC"));
		transaccionBean.setMontoFacturacion(resultados
				.getString("MONTO_FACTURACION"));
		transaccionBean.setCodRazon(resultados.getString("CODRAZON"));
		transaccionBean.setMicroFilm(resultados.getString("MICROFILM"));
		transaccionBean.setCodigoAutorizacion(resultados
				.getString("CODIGO_AUTORIZACION"));
		transaccionBean.setFechaEfectiva(resultados.getString("FECHAEFECTIVA"));
		transaccionBean.setFechaProceso(Utils
				.parseJulianDateToGregorianDate(resultados
						.getString("FECHAPROCESO")));
		transaccionBean
				.setBinAdquiriente(resultados.getString("BINADQUIRENTE"));
		transaccionBean.setLeeBanda(resultados.getString("LEEBANDA"));
		transaccionBean.setEstadoTrx(resultados.getString("ESTADOTRX"));
		transaccionBean.setOtrosDatos1(resultados.getString("OTRODATO1"));
		transaccionBean.setOtrosDatos2(resultados.getString("OTRODATO2"));
		transaccionBean.setOtrosDatos3(resultados.getString("OTRODATO3"));
		transaccionBean.setOtrosDatos4(resultados.getString("OTRODATO4"));
		transaccionBean.setCodMonedaTrx(resultados.getString("CODMONEDATRX"));
		transaccionBean.setRubroComercio(resultados.getString("RUBROCOMERCIO"));
		transaccionBean.setSid(resultados.getString("SID"));
		transaccionBean.setGlosaGeneral(resultados.getString("GLOSAGENERAL"));
		transaccionBean.setReferencia(resultados.getString("REFERENCIA"));
		transaccionBean.setMontoConciliacion(resultados
				.getString("MONTO_CONCILIACION"));
		transaccionBean.setNumIncidente(resultados.getString("NUM_INCIDENTE"));
		transaccionBean.setFechaGestion(resultados.getString("FECHA_GESTION"));
		transaccionBean.setIdUsuario(resultados.getString("USUARIO"));
		transaccionBean.setTipoTransc(resultados.getString("TIPO_TRANSAC"));
		transaccionBean.setPatpass(resultados.getString("PATPASS"));

		// Formateo de montos
		montoAux = Utils.formateaMonto(transaccionBean.getMontoTransac());
		transaccionBean.setMontoTransac(montoAux);

		// Formateo de montos
		montoAux = Utils.formateaMonto(transaccionBean.getMontoConciliacion());
		transaccionBean.setMontoConciliacion(montoAux);

		// Formateo de montos
		montoAux = Utils.formateaMonto(transaccionBean.getMontoFacturacion());
		transaccionBean.setMontoFacturacion(montoAux);

		return transaccionBean;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @since 1.X
	 */
	private List<String> getListToCheck() {
		CommonsDAO common = new CommonsDAO();
		String listCodigosCheck = common.getParameterByCode(PARAM_LIST_CHECKED).get(0).getValor();
		
		// CREA UNA LISTA DE CADA SID DE TIPO DE TRANSAC QUE DEBE CHEQUEARSE AL
		// MOSTAR EN PANTALLA
		List<String> listaCheck = listCodigosCheck != null
				&& !listCodigosCheck.isEmpty() ? Arrays.asList(listCodigosCheck
				.split(SPLIT_BY_COMMA)) : new ArrayList<String>();

		return listaCheck;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	private void validarSiSeDebeCheckearPorDefault(List<String> listaCheck,
			List<TransaccionBean> listaTransacciones) {

		for (TransaccionBean transaccionBean : listaTransacciones) {

			if (listaCheck != null && !listaCheck.isEmpty()
					&& listaCheck.contains(transaccionBean.getTipoTransc())) {
				transaccionBean.setIsCheck(Boolean.TRUE);

			} else {
				transaccionBean.setIsCheck(Boolean.FALSE);
			}

		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo encargado de realizar la llamada al SP de cargo masivo, para poder
	 * cargar o abonar masivamente las transacciones en BD
	 * 
	 * @param listaCargoAbono
	 * @throws AppException
	 * @since 1.X
	 */
	public void guardarCargoAbonosMasivos(List<CargoAbonoDTO> listaCargoAbono)
			throws AppException {

		if (listaCargoAbono == null) {
			log.warn("No se ha ingresado ningun cargo/abono la lista viene nula");
			throw new AppException("No se ha guardado ning�n cargo/abono ");

		} else if (listaCargoAbono != null && listaCargoAbono.isEmpty()) {
			log.warn("No se ha ingresado ningun cargo/abono la lista viene vac�a");
			throw new AppException("No se ha guardado ning�n cargo/abono.");
		}

		for (CargoAbonoDTO cargoAbonoDTO : listaCargoAbono) {

			callSpCargoAbonoMasivo(cargoAbonoDTO);

		}

	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versi�n inicial 
	 * </ul>
	 * <p>
	 * 
	 * @param filtro
	 * @return
	 * @since 1.X
	 */
	public List<TransaccionBean> buscarTransaccionesSinPaginacion(
			FiltroTransacciones filtro) {

		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		ResultSet resultados = null;
		List<TransaccionBean> listaTransacciones = new ArrayList<TransaccionBean>();
		String storeProcedure = "BEGIN "
				+ esquema
				+ ".SP_SIG_BUSCAR_TRX_SOL60(?, ?, ?, ?, ?, ?, ?); END;";

		try {

			log.info("Ejecutando SP - SP_SIG_BUSCAR_TRX_SOL60");
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);

			stmtProcedure = conexion.prepareCall(storeProcedure);

			// ENTRADA
			stmtProcedure.setString("numeroTarjeta", filtro.getNumeroTarjeta());
			stmtProcedure.setString("fechaInicio", filtro.getFechaDesde());
			stmtProcedure.setString("fechaTermino", filtro.getFechaHasta());
			stmtProcedure.setString("tipoObjecionRechazo", filtro.getTipoTransaccion().getXkey());
			
			// SALIDA
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			// PARSEA LA LISTA DE RESULTADOS
			while (resultados.next()) {
				TransaccionBean tb = parsearResultado(resultados);
				listaTransacciones.add(tb);
			}

			resultados.close();

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} finally {

			if (stmtProcedure != null) {
				bdOracle.cerrarStatement(stmtProcedure);
			}

			if (!bdOracle.cerrarConexionBD()) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}

			try {
				if (conexion != null && !conexion.isClosed()) {
					conexion.close();
				}
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
			}
		}

		return listaTransacciones;
	}


	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @param listaCargoAbono
	 * @throws AppException
	 * @since 1.X
	 */
	private void callSpCargoAbonoMasivo(CargoAbonoDTO cargoAbonoDTO)
			throws AppException {
		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		String STORE_PROCEDURE = "BEGIN " + esquema
				+ ".SP_SIG_CARGO_ABONO_MASIVO(?, ?, ?, ?, ?, ?); END;";

		try {

			log.info("Ejecutando SP - SP_SIG_CARGO_ABONO_MASIVO");
			log.info(cargoAbonoDTO);

			// INICIA CONEXIONES
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);

			stmtProcedure = conexion.prepareCall(STORE_PROCEDURE);

			// ENTRADA
			stmtProcedure.setLong("sidTransaccion",
					cargoAbonoDTO.getTransaccion());
			stmtProcedure.setString("codMoneda", cargoAbonoDTO.getTipoMoneda()
					.getNumero());
			stmtProcedure.setLong("sidTipoCargoAbono",
					cargoAbonoDTO.getIdTipoCargoAbono());
			stmtProcedure.setLong("sidUsuario", cargoAbonoDTO.getSidUsuario());

			// SALIDA
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);

			stmtProcedure.execute();

			String warning = stmtProcedure.getString("warning");
			int codError = stmtProcedure.getInt("cod_error");

			log.info("Warning : " + warning);
			log.info("CodError : " + codError);

			if (codError != 0) {
				throw new AppException(codError + "-" + warning);
			}

			log.info("Se ha almacenado el cargo y abono para la trx sid = "
					+ cargoAbonoDTO.getTransaccion() + ", correctamtente");

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} finally {
			try {
				if( conexion != null && !conexion.isClosed()){
					conexion.close();
				}
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
			}
			
			try {
				if(stmtProcedure != null && !stmtProcedure.isClosed()){
					stmtProcedure.close();
				}
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
			}
		}

	}
}
