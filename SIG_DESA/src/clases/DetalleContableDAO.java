package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.DetalleContableBean;
import db.OracleBD;

/**
 * 
 * @author afreire
 * 
 */
public class DetalleContableDAO {
    // CONSTANTES DEL SISTEMA
    static final Category log = Logger.getLogger(DetalleContableDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    private String esquema;

    /**
     * Constructor
     */
    public DetalleContableDAO() {
	try {
	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    esquema = propiedades.getProperty("esquema");
	} catch (Exception e) {
	    log.error("Error al leer el archivo de propiedades.");
	    log.error(e.getMessage(), e);
	}
    }

    /**
     * Metodo que busca pagos de acuerdo a una fecha y a un estado
     * 
     * @param fechaDesde
     * @param fechaHasta
     * @param estado
     * @return
     * @throws Exception
     */
    public List<DetalleContableBean> obtenerDetalleContable(String fechaInicio, String fechaTermino, String tipoMov, int operador, int producto) throws Exception {
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet rs = null;
	int codigoError = 0;
	List<DetalleContableBean> listaDetalle = null;

	try {
	    log.info(" ===== Se ejecuta el procedimiento : SP_SIG_DETALLE_CONTABLE =====");
	    log.info(" fecha_inicio : " + fechaInicio);
	    log.info(" fecha_termino : " + fechaTermino);
	    log.info(" tipo_movimiento : " + tipoMov);
	    log.info(" operador : " + operador);
	    log.info(" producto : " + producto);

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);
	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_DETALLE_CONTABLE(?, ?, ?, ?, ?, ?,?, ?); END;");
	    stmtProcedure.setString("fecha_inicio", fechaInicio);
	    stmtProcedure.setString("fecha_termino", fechaTermino);
	    stmtProcedure.setString("tipo_movimiento", tipoMov);
	    stmtProcedure.setInt("filtro_operador", operador);
	    stmtProcedure.setInt("producto_id", producto);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");
	    rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

	    log.info("======= RESULTADOS :  BuscarPago ========");
	    log.info("codigoError : " + codigoError);
	    log.info("warning : " + warning);
	    
	    if (codigoError == 0) {
		listaDetalle = new ArrayList<DetalleContableBean>();

		// BUSCA DATOS DEL CURSOR
		while (rs.next()) {
		    DetalleContableBean detalle = new DetalleContableBean();
		    detalle.setFechaContable(rs.getString("FECHA_CONTABLE"));
		    detalle.setTipoMovimiento(rs.getString("TIPO_MOV"));
		    detalle.setCuentaDebito(rs.getString("DEBE"));
		    detalle.setCuentaCredito(rs.getString("HABER"));
		    detalle.setMonto(rs.getString("MONTO_TRANSAC"));
		    detalle.setSubledger(rs.getString("SUBLEDGER"));
		    detalle.setNumeroTarjeta(rs.getString("NRO_TARJETA"));
		    detalle.setMicrofilm(rs.getString("MICROFILM"));
		    detalle.setTipoVenta(rs.getString("TIPO_VENTA"));
		    detalle.setCantidadCuotas(rs.getString("CUOTAS"));
		    detalle.setNombreArchivo(rs
			    .getString("NOMBRE_ARCHIVO_CONTABLE"));

		    listaDetalle.add(detalle);
		}

	    } else {
		throw new Exception(warning);
	    }
	} catch (Exception e) {
	    throw e;
	} finally {
	    cerrarConexiones(bdOracle, rs, conexion, stmtProcedure);
	}
	return listaDetalle;
    }
    
    /**
     * Metodo que busca pagos de acuerdo a una fecha y a un estado
     * 
     * @param fechaDesde
     * @param fechaHasta
     * @param estado
     * @return
     * @throws Exception
     */
    public int obtenerCountDetalleContable(String fechaInicio, String fechaTermino, String tipoMov, int operador, int producto) throws Exception {
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet rs = null;
	int codigoError = 0;
	int total_count = 0;

	try {
	    log.info(" ===== Se ejecuta el procedimiento : SP_SIG_DETALLE_CONTABLE =====");
	    log.info(" fecha_inicio : " + fechaInicio);
	    log.info(" fecha_termino : " + fechaTermino);
	    log.info(" tipo_movimiento : " + tipoMov);
	    log.info(" operador : " + operador);
	    log.info(" producto : " + producto);

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);
	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_DETALLE_CONTABLE(?, ?, ?, ?, ?, ?,?, ?); END;");
	    stmtProcedure.setString("fecha_inicio", fechaInicio);
	    stmtProcedure.setString("fecha_termino", fechaTermino);
	    stmtProcedure.setString("tipo_movimiento", tipoMov);
	    stmtProcedure.setInt("filtro_operador", operador);
	    stmtProcedure.setInt("producto_id", producto);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");
	    rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

	    log.info("======= RESULTADOS :  BuscarPago ========");
	    log.info("codigoError : " + codigoError);
	    log.info("warning : " + warning);
	    
	    if (codigoError == 0) {
		

		// BUSCA DATOS DEL CURSOR
		while (rs.next()) {
		    total_count ++;
		}

	    } else {
		throw new Exception(warning);
	    }
	} catch (Exception e) {
	    throw e;
	} finally {
	    cerrarConexiones(bdOracle, rs, conexion, stmtProcedure);
	}
	log.info("Cantidad de registros: " + total_count);
	return total_count;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo encargado de cerrar las conexioness
     * 
     * @param oracleBD
     * @param rs
     * @param conexion
     * @param stmtProcedure
     * @since 1.X
     */
    private void cerrarConexiones(OracleBD oracleBD, ResultSet rs,
	    Connection conexion, CallableStatement stmtProcedure) {
	if (rs != null) {
	    try {
		rs.close();
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	try {
	    if (conexion != null) {
		conexion.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

	try {
	    if (stmtProcedure != null) {
		stmtProcedure.close();
	    }
	    else{
		log.info("Error 20134");
	    }

	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

	if (oracleBD.cerrarResultSet() == false) {
	    if (oracleBD.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	} else {
	    if (oracleBD.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	}

    }

}
