package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import cl.util.Utils;
import Beans.TDIExecutionEstBean;
import Beans.TDIExecutionLogBean;
import Beans.TDIJobBean;
import properties.UtilProperties;
import db.OracleBD;

public class TDIExecutionLogDao {
    static final Logger LOGGER = Logger.getLogger(TDIExecutionLogDao.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    private int cResultados;

    private OracleBD obd = new OracleBD();

    private String esquema;

    private ResultSet rs;

    private Connection conexion;

    /*
     * Constructor
     */
    public TDIExecutionLogDao() {
	this.esquema = UtilProperties.getProperty("esquema");
	this.cResultados = Integer.parseInt(UtilProperties.getProperty("TDICantidadRegistros")); 
    }

    public void setConexion() {
	this.conexion = obd.conectar();
	obd.setResultado(rs);
    }

    public Connection getConexion() {
	return conexion;
    }

    public void setConexion(Connection conexion) {
	this.conexion = conexion;
    }

    public List<TDIExecutionLogBean> findAll(Integer pageNumber, String fechaInicio, String fechaTermino, int estado) {
	setConexion();
	
	String paginadorQuery = "";
	if(pageNumber != null && pageNumber > 0){
	 // Calculo de paginado
		int inicio =  (pageNumber-1) * cResultados;
		int actualReal = inicio;
		inicio++;
		int termino = actualReal + cResultados;
	        paginadorQuery = "WHERE NUMBERR BETWEEN "+inicio+" AND "+termino;
	}
	
	String estadoQuery = "";
	if(estado != -1){
	    estadoQuery = " WHERE EST.ID = "+estado;
	}
	
	String fechasQuery = "";
	if(fechaInicio.length() > 0 && fechaTermino.length() > 0){
	    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
	    try{
		formatDate.parse(fechaInicio);
		Date ftermino = Utils.addDays(formatDate.parse(fechaTermino), 1);
		fechasQuery = " AND HORA_INICIO BETWEEN to_date('"+fechaInicio+"', 'dd/mm/yyyy') and to_date('"+formatDate.format(ftermino)+"', 'dd/mm/yyyy') ";
	    }catch (Exception e) {
		    LOGGER.info("Error al parsear el parametro de filtrado fechaInicio o  fechaTermino", e);
	    }
	}
	
	SimpleDateFormat hourFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	String valor =  "SELECT * FROM (SELECT Row_Number() OVER (ORDER BY LOG.HORA_INICIO DESC) NUMBERR, LOG.ID AS LOG_ID, ARCHIVO, ARCHIVO_EXT, PROCESO, (to_char(HORA_INICIO, 'dd-mm-yyyy hh24:mi:ss')) AS HORA_INICIO , (to_char(HORA_TERMINO, 'dd-mm-yyyy hh24:mi:ss')) AS HORA_TERMINO, DELTA_EJECUCION, ESTADO, DESCRIPCION, ORIGEN, DESTINO "
	        + "FROM " + esquema + ".TBL_TDI_EXECUTION_LOG LOG INNER JOIN " + esquema + ".TBL_TDI_EXECUTION_EST EST ON LOG.EST_ID = EST.ID "+estadoQuery+" "+fechasQuery+") "+paginadorQuery;
	LOGGER.info("CONSULTA realizada "+ valor);
	rs = obd.consultar(valor);
	obd.setResultado(rs);

	List<TDIExecutionLogBean> lista = new ArrayList<TDIExecutionLogBean>();
	try {
	    LOGGER.info("Listado de logs - Obteniendo resultados");

	    while (rs.next()) {
		TDIExecutionLogBean tipo = new TDIExecutionLogBean();
		tipo.setSid(rs.getLong("LOG_ID"));
		tipo.setArchivo(rs.getString("ARCHIVO"));
		tipo.setArchivoExt(rs.getString("ARCHIVO_EXT"));
		tipo.setProceso(rs.getString("PROCESO"));
		try {
		    tipo.setHoraInicio(hourFormat.parse(rs.getString("HORA_INICIO")));
		} catch (Exception e) {
		    LOGGER.info("Error al parsear el parametro HORA_INICIO", e);
		}
		try {
		    tipo.setHoraTermino(hourFormat.parse(rs.getString("HORA_TERMINO")));
		} catch (Exception e) {
		    LOGGER.info("Error al parsear el parametro HORA_TERMINO", e);
		}
		
		tipo.setDeltaEjecucion(rs.getInt("DELTA_EJECUCION"));
		tipo.setEstado(rs.getString("ESTADO"));
		tipo.setDescripcionError(rs.getString("DESCRIPCION"));
		tipo.setOrigen(rs.getString("ORIGEN"));
		tipo.setDestino(rs.getString("DESTINO"));
		lista.add(tipo);
	    }

	    obd.cerrarResultSet();
	} catch (Exception e) {
	    LOGGER.error("Error al obtener el listado de los logs");
	    LOGGER.error(e);
	}
	
	LOGGER.info("Cantidad de valores "+lista.size());

	cerrarConexion();

	return lista;
    }

    private void cerrarConexion() {
	if (!obd.cerrarResultSet()) {
	    LOGGER.error("Error: No se pudo cerrar resulset.");
	    if (!obd.cerrarConexionBD()) {
		LOGGER.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	} else {
	    if (!obd.cerrarConexionBD()) {
		LOGGER.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	}
	try {
	    if (conexion != null && !conexion.isClosed()) {
		conexion.close();
	    }
	} catch (SQLException e) {
	    LOGGER.error(e.getMessage(), e);
	}
    }

    public int getUltimaPagina(String fechaInicio, String fechaTermino,  int estado) {
	int totReg = this.findAll(null, fechaInicio, fechaTermino, estado).size();
	return Utils.calcularUltimaPagina(totReg, cResultados);
    }

    public void setEstado(int id, TDIExecutionEstBean estadoProcesar) {
	    setConexion();
	    String sql = "UPDATE " +esquema+".TBL_TDI_EXECUTION_LOG SET EST_ID = '"+estadoProcesar.getSid()+"' WHERE ID = '"+id+"'";
	    obd.ejecutar(sql);
	    cerrarConexion();
    }

    public TDIExecutionLogBean getById(int id) {
        setConexion();
        SimpleDateFormat hourFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        
	String valor =  "SELECT LOG.ID AS LOG_ID, PROCESO, ARCHIVO, ARCHIVO_EXT, CTL_EXT, JOB_ID, (to_char(HORA_INICIO, 'dd-mm-yyyy hh24:mi:ss')) AS HORA_INICIO , (to_char(HORA_TERMINO, 'dd-mm-yyyy hh24:mi:ss')) AS HORA_TERMINO, DELTA_EJECUCION, ESTADO, DESCRIPCION "
                      + "FROM " + esquema + ".TBL_TDI_EXECUTION_LOG LOG INNER JOIN " + esquema + ".TBL_TDI_EXECUTION_EST EST ON LOG.EST_ID = EST.ID WHERE LOG.ID = '"+id+"'";
	LOGGER.info("CONSULTA realizada "+ valor);
	rs = obd.consultar(valor);
	obd.setResultado(rs);

	TDIExecutionLogBean tipo = new TDIExecutionLogBean();

	try {
	    LOGGER.info("Listado de logs - Obteniendo resultados");

	    while (rs.next()) {
		tipo.setSid(rs.getLong("LOG_ID"));
		tipo.setProceso(rs.getString("PROCESO"));
		tipo.setArchivo(rs.getString("ARCHIVO"));
		tipo.setArchivoExt(rs.getString("ARCHIVO_EXT"));
		tipo.setControlExt(rs.getString("CTL_EXT"));
		tipo.setDeltaEjecucion(rs.getInt("DELTA_EJECUCION"));
		tipo.setEstado(rs.getString("ESTADO"));
		tipo.setDescripcionError(rs.getString("DESCRIPCION"));
		TDIJobBean job = new TDIJobBean();
		job.setSid(rs.getLong("JOB_ID"));
		tipo.setTdiJob(job);
		try {
		    tipo.setHoraInicio(hourFormat.parse(rs.getString("HORA_INICIO")));
		} catch (Exception e) {
		    LOGGER.info("Error al parsear el parametro HORA_INICIO", e);
		}
		try {
		    tipo.setHoraTermino(hourFormat.parse(rs.getString("HORA_TERMINO")));
		} catch (Exception e) {
		    LOGGER.info("Error al parsear el parametro HORA_TERMINO", e);
		}
	    }

	    obd.cerrarResultSet();
	} catch (Exception e) {
	    LOGGER.error("Error al obtener el listado de los logs");
	    LOGGER.error(e);
	}
	

	cerrarConexion();

	return tipo;
    }
    
    public void createLog(TDIExecutionLogBean log) {
	    setConexion();
	    SimpleDateFormat dia = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	    String sql = "INSERT INTO " + esquema+".TBL_TDI_EXECUTION_LOG (PROCESO, ARCHIVO, ARCHIVO_EXT, CTL_EXT, HORA_INICIO, EST_ID, JOB_ID, INTENTO) VALUES ('"+log.getProceso()+"', '"+log.getArchivo()+"', '"+log.getArchivoExt()+"' , '"+log.getControlExt()+"' , to_date('"+dia.format(log.getHoraInicio())+"', 'dd-mm-yyyy hh24:mi'), '"+log.getEstadoBean().getSid()+"', '"+log.getTdiJob().getSid()+"', '0') ";
	    obd.ejecutar(sql);
	    cerrarConexion();
    }
}
