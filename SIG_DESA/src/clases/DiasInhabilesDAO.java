package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import Beans.DiaInhabilBeans;

import properties.PropertiesConfiguration;
import db.OracleBD;

/**
 * 
 * @author dmedina
 * 
 */
public class DiasInhabilesDAO {
	static final Logger log = Logger.getLogger(DiasInhabilesDAO.class);

	private Properties prop = new Properties();

	private String esquema;

	private static final String SQL_QUERY_VALIDATE_DATE = "SELECT COUNT(*) FROM :schema:.TBL_DIAS_INHABILES WHERE TRUNC(FECHA) = to_date('?','DD-MM-YYYY')";
	private static final String FORMAT_DATE = "DD/MM/YYYY";

	private String listaParametros;
	private String updateDiasInhabiles;

	private ResultSet rs;

	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}

	/**
	 * Constructor
	 */

	public DiasInhabilesDAO() {
		try {
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();
			esquema = propiedades.getProperty("esquema");

		} catch (Exception e) {
			log.error("Error al leer el archivo de propiedades.");
			log.error(e.getMessage(), e);
		}
	}

	/*
	 * Descripci�n: Obtiene la lista de dias inh�biles.
	 * 
	 * @param Ninguno.
	 * 
	 * @throw Exception si hay un error.
	 * 
	 * @return listaParametros con datos serializados
	 * 
	 * @version 1.0
	 */
	public String getListaDiasInhabiles(String ano) {

		Connection conexion = null;
		db.OracleBD obd = null;

		obd = new OracleBD();
		conexion = obd.conectar();
		obd.setConexion(conexion);

		listaParametros = "esttrx:0~";

		StringBuilder str = new StringBuilder(
				"SELECT SID, TO_CHAR(FECHA, 'DD/MM/YYYY') AS FECHA, GLOSA, ESTADO FROM ");
		str.append(esquema);
		str.append(".TBL_DIAS_INHABILES where TO_CHAR(FECHA,'YYYY') = ");

		ano = ano != null ? ano : "TO_CHAR(SYSDATE,�YYYY�)";

		str.append(ano);
		str.append(" ORDER BY FECHA ASC");

		// rs =
		// obd.consultar("SELECT SID, TO_CHAR(FECHA, 'DD/MM/YYYY') AS FECHA, GLOSA, ESTADO FROM "+esquema+".TBL_DIAS_INHABILES");
		log.info(str.toString());
		rs = obd.consultar(str.toString());

		obd.setResultado(rs);
		try {
			if (rs.next()) {
				rs.previous();
				while (rs.next()) {
					listaParametros += rs.getInt("SID") + "|";
					listaParametros += rs.getString("GLOSA") + "|";
					listaParametros += rs.getString("FECHA") + "|";
					listaParametros += rs.getString("ESTADO") + ("~");
				}
			} else {
				listaParametros = "esttrx:1~No hay datos";
			}
			rs.close();

		} catch (SQLException e) {

			e.printStackTrace();
			log.error(e.getMessage());
		} finally {
			cerrarConexion(conexion, obd);
		}

		return listaParametros;
	}

	/*
	 * Descripci�n: Inactiva un registro.
	 * 
	 * @param sid, estado.
	 * 
	 * @throw Exception si hay un error.
	 * 
	 * @return Ok si no hay exceptiones
	 * 
	 * @version 1.0
	 */

	public String cambiarEstadoDiaInhabil(String sid, String estado) {

		Connection conexion = null;
		db.OracleBD obd = null;

		obd = new OracleBD();
		conexion = obd.conectar();
		obd.setConexion(conexion);

		try {
			String sql = "update " + esquema
					+ ".TBL_DIAS_INHABILES SET ESTADO = " + estado
					+ " WHERE SID = " + sid;
			obd.ejecutar(sql);
		} catch (Exception e) {
			e.getMessage();
		} finally {
			cerrarConexion(conexion, obd);
		}
		return updateDiasInhabiles;
	}

	/**
	 * Descripci�n: Agrega un registro.
	 * 
	 * @param sid
	 *            , estado.
	 * @throw Exception si hay un error.
	 * @return Ok si no hay exceptiones
	 * @version 1.0
	 */
	public int guardarDiaInhabil(DiaInhabilBeans nuevoDia) {
		Connection conexion = null;
		db.OracleBD obd = null;

		try {
			obd = new OracleBD();
			conexion = obd.conectar();
			obd.setConexion(conexion);

			int numeroDiasIguales = contarFechasIguales(nuevoDia);
			if (numeroDiasIguales == 0) {
				String sql = "INSERT INTO " + esquema
						+ ".TBL_DIAS_INHABILES (SID, GLOSA, FECHA, ESTADO) ";
				sql = sql + " VALUES (" + esquema + ".SEQ_DIA_INHABIL.nextval"
						+ ",";
				sql = sql + "'" + nuevoDia.getGlosa() + "',TO_DATE('"
						+ nuevoDia.getFecha() + "','" + FORMAT_DATE + "'),"
						+ nuevoDia.getEstado() + ")";
				//log.info("Ejecutando SQL = " + sql);
				if (obd.ejecutar(sql)) {
					return 1;
				}
			} else {
				return 0;
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return 2;
		} finally {
			cerrarConexion(conexion, obd);
		}
		return 3;
	}

	/**
	 * 
	 * @param nuevoDia
	 * @return
	 */
	private int contarFechasIguales(DiaInhabilBeans nuevoDia) {

		Connection conexion = null;
		db.OracleBD obd = null;
		Integer count = 0;

		try {
			obd = new OracleBD();
			conexion = obd.conectar();
			obd.setConexion(conexion);

			String sqlValDate = SQL_QUERY_VALIDATE_DATE.replace(":schema:",
					esquema).replace("?", nuevoDia.getFecha());
			obd.conectar();

			ResultSet rs = obd.consultar(sqlValDate);
			if (rs != null) {

				while (rs.next()) {
					count = rs.getInt(1);
				}

			}
		} catch (SQLException e) {
			log.error("Error: No se pudo cerrar conexion a la base de datos.");
			e.printStackTrace();
		} finally {
			cerrarConexion(conexion, obd);
		}

		return count;

	}

	/**
	 * 
	 * @param conexion
	 * @param obd
	 */
	private void cerrarConexion(Connection conexion, db.OracleBD obd) {
		if (obd.cerrarResultSet() == false) {
			log.error("Error: No se pudo cerrar resulset.");
			if (obd.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}
		} else {
			if (obd.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}
		}
		try {
			if (conexion != null && !conexion.isClosed()) {
				conexion.close();
			}
		} catch (SQLException e) {
			log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	}
}
