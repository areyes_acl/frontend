package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import Beans.TDIConexionTipoBean;
import properties.UtilProperties;
import db.OracleBD;

public class TDIConexionTipoDAO {
    static final Logger log = Logger.getLogger(TDIConexionTipoDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    private OracleBD obd = new OracleBD();

    private String esquema;

    private ResultSet rs;

    private Connection conexion;

    /*
     * Constructor
     */
    public TDIConexionTipoDAO() {
	this.esquema = UtilProperties.getProperty("esquema");
    }

    public void setConexion() {
	this.conexion = obd.conectar();
	obd.setResultado(rs);
    }

    public Connection getConexion() {
	return conexion;
    }

    public void setConexion(Connection conexion) {
	this.conexion = conexion;
    }

    public List<TDIConexionTipoBean> findAll() {
	setConexion();
	String valor = "SELECT * FROM " + esquema + ".TBL_TDI_CONEXION_TIPO";
	rs = obd.consultar(valor);
	obd.setResultado(rs);
	
	List<TDIConexionTipoBean> lista = new ArrayList<TDIConexionTipoBean>();
	
	try {
	    log.info("Obteniendo tipos de conexion");

	    while (rs.next()) {
		TDIConexionTipoBean tipo = new TDIConexionTipoBean();
		tipo.setSid(Long.valueOf(rs.getInt("ID")));
		tipo.setTipo(rs.getString("TIPO"));
		lista.add(tipo);
	    }

	    obd.cerrarResultSet();
	} catch (Exception e) {
	    log.info("Error al obtener los resultados de los tipos de conexion");
	    log.error(e);
	}

	cerrarConexion();

	return lista;
    }

    private void cerrarConexion(){
   	if (!obd.cerrarResultSet()) {
   	    log.error("Error: No se pudo cerrar resulset.");
   	    if (!obd.cerrarConexionBD()) {
   		log.error("Error: No se pudo cerrar conexion a la base de datos.");
   	    }
   	} else {
   	    if (!obd.cerrarConexionBD()) {
   		log.error("Error: No se pudo cerrar conexion a la base de datos.");
   	    }
   	}
   	try {
   	    if (conexion != null && !conexion.isClosed()) {
   		conexion.close();
   	    }
   	} catch (SQLException e) {
   	    log.error(e.getMessage(), e);
   	}
       }
}
