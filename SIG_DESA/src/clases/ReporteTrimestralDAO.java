package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.InternacionalType;
import types.NacionalType;
import types.OnusType;
import Beans.DetalleTrimestral;
import Beans.ReporteTrimestral;
import db.OracleBD;

/**
 * 
 * @author dmedina
 * 
 */
public class ReporteTrimestralDAO {
	static final Logger log = Logger.getLogger(ReporteTrimestralDAO.class);

	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	private final String SP_OBTENER_REPORTE_TRIMESTRAL = "SP_SIG_OBTENER_REP_TRIMESTRAL(?, ?, ?, ?, ?); END;";

	private String esquema;

	/**
	 * Constructor
	 */
	public ReporteTrimestralDAO() {
		try {
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();
			esquema = propiedades.getProperty("esquema");
		} catch (Exception e) {
			log.error("Error al leer el archivo de propiedades.");
			log.error(e.getMessage(), e);
		}
	}

	
	/**
	 * 
	 * 
	 * @param a�o
	 * @param trimestre
	 * @return
	 */
	public List<ReporteTrimestral> obtenerReporteTrimestral(Integer a�o, Integer trimestre) {
		Connection conexion = conectarBD();
		CallableStatement stmtProcedure = null;
		List<ReporteTrimestral> reporteTrimestral = null;
		ResultSet rs = null;
		log.info(SP_OBTENER_REPORTE_TRIMESTRAL);
		log.info("Trimestre : " + trimestre + " | A�o" + a�o);

		try {
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + "."
					+ SP_OBTENER_REPORTE_TRIMESTRAL);

			stmtProcedure.setInt("trimestre", trimestre);
			stmtProcedure.setInt("ano", a�o);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			stmtProcedure.execute();

			int codigoError = stmtProcedure.getInt("cod_error");
			String warning = stmtProcedure.getString("warning");
			rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

			if (codigoError == 0) {		
				reporteTrimestral=obtenerResumenPorTrimestre(rs);
				 
			}else{
				log.warn(codigoError +" - "+ warning);
			}

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		
		}finally{
			cerrarConexiones(conexion, stmtProcedure);
		}
		
		return reporteTrimestral;

	}

	
	/**
	 * 
	 * @param rs
	 */
	private List<ReporteTrimestral> obtenerResumenPorTrimestre(ResultSet rs) {
		List<ReporteTrimestral> trimestres = new ArrayList<ReporteTrimestral>();
		
		try {
			// recorre los trimestres
			while (rs.next()) {

				ReporteTrimestral repo = new ReporteTrimestral();
				repo.setFechaTrimestre(rs.getString("FECHA_TRIMESTRAL"));

				// ONUS
				repo.setDetalleOnus(obtenerListaOnus(rs));

				// NACIONAL
				repo.setDetalleNacional(obtenerListaNacional(rs));

				// INTERNACIONAL
				repo.setDetalleInternacional(obtenerListaInternacional(rs));
				
				trimestres.add(repo);

			}

		} catch (SQLException e) {
			log.error("Ha ocurrido un error al intentar parsear la respuesta del SP "
					+ SP_OBTENER_REPORTE_TRIMESTRAL);
			log.error(e.getMessage(), e);
		}
		
		return trimestres;
	}

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private List<DetalleTrimestral> obtenerListaInternacional(ResultSet rs)
			throws SQLException {
		List<DetalleTrimestral> listaTrimestral = new ArrayList<DetalleTrimestral>();
		
		DetalleTrimestral detalleInt = new DetalleTrimestral();
		detalleInt.setTipo(InternacionalType.INTERNACIONAL_TOTAL);
		detalleInt.setDescripcionCant(InternacionalType.INTERNACIONAL_TOTAL.getDescripcionCant());
		detalleInt.setDescripcionMont(InternacionalType.INTERNACIONAL_TOTAL.getDescripcionMont());
		detalleInt.setCantidad(rs.getInt("CANT_TOTAL_INT"));
		detalleInt.setMonto(rs.getString("MNT_TOTAL_INT"));
		listaTrimestral.add(detalleInt);
		
		detalleInt = new DetalleTrimestral();
		detalleInt.setTipo(InternacionalType.INTERNACIONAL_CHIP);
		detalleInt.setDescripcionCant(InternacionalType.INTERNACIONAL_CHIP.getDescripcionCant());
		detalleInt.setDescripcionMont(InternacionalType.INTERNACIONAL_CHIP.getDescripcionMont());
		detalleInt.setCantidad(rs.getInt("CANT_CHIP_INT"));
		detalleInt.setMonto(rs.getString("MNT_CHIP_INT"));
		listaTrimestral.add(detalleInt);
		
		detalleInt = new DetalleTrimestral();
		detalleInt.setTipo(InternacionalType.INTERNACIONAL_COMERCIO_ELECTRONICO);
		detalleInt.setDescripcionCant(InternacionalType.INTERNACIONAL_COMERCIO_ELECTRONICO.getDescripcionCant());
		detalleInt.setDescripcionMont(InternacionalType.INTERNACIONAL_COMERCIO_ELECTRONICO.getDescripcionMont());
		detalleInt.setCantidad(rs.getInt("CANT_COM_ELECT_INT"));
		detalleInt.setMonto(rs.getString("MNT_COM_ELECT_INT"));
		listaTrimestral.add(detalleInt);
		
		detalleInt = new DetalleTrimestral();
		detalleInt.setTipo(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_EN_CAJA);
		detalleInt.setDescripcionCant(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_EN_CAJA.getDescripcionCant());
		detalleInt.setDescripcionMont(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_EN_CAJA.getDescripcionMont());
		detalleInt.setCantidad(rs.getInt("CANT_DISP_EFECT_INT"));
		detalleInt.setMonto(rs.getString("MNT_DISP_EFECT_INT"));
		listaTrimestral.add(detalleInt);
		
		detalleInt = new DetalleTrimestral();
		detalleInt.setTipo(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_CAJERO);
		detalleInt.setDescripcionCant(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_CAJERO.getDescripcionCant());
		detalleInt.setDescripcionMont(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_CAJERO.getDescripcionMont());
		detalleInt.setCantidad(rs.getInt("CANT_DISP_EFECT_CAJ_INT"));
		detalleInt.setMonto(rs.getString("MNT_DISP_EFECT_CAJ_INT"));
		listaTrimestral.add(detalleInt);
		
		
		return listaTrimestral;
	}

	
	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private List<DetalleTrimestral> obtenerListaNacional(ResultSet rs) throws SQLException {
		List<DetalleTrimestral> listaTrimestral = new ArrayList<DetalleTrimestral>();
		
		
		DetalleTrimestral detalleNac = new DetalleTrimestral();
		detalleNac.setTipo(NacionalType.NACIONAL_TOTAL);
		detalleNac.setDescripcionCant(NacionalType.NACIONAL_TOTAL.getDescripcionCant());
		detalleNac.setDescripcionMont(NacionalType.NACIONAL_TOTAL.getDescripcionMonto());
		detalleNac.setCantidad(rs.getInt("CANT_TOTAL_NAC"));
		detalleNac.setMonto(rs.getString("MNT_TOTAL_NAC"));
		listaTrimestral.add(detalleNac);
		
		detalleNac = new DetalleTrimestral();
		detalleNac.setTipo(NacionalType.NACIONAL_CHIP);
		detalleNac.setDescripcionCant(NacionalType.NACIONAL_TOTAL.getDescripcionCant());
		detalleNac.setDescripcionMont(NacionalType.NACIONAL_TOTAL.getDescripcionMonto());
		detalleNac.setCantidad(rs.getInt("CANT_CHIP_NAC"));
		detalleNac.setMonto(rs.getString("MNT_CHIP_NAC"));
		listaTrimestral.add(detalleNac);
		
		detalleNac = new DetalleTrimestral();
		detalleNac.setTipo(NacionalType.NACIONAL_COMERCIO_ELECTRONICO);
		detalleNac.setDescripcionCant(NacionalType.NACIONAL_COMERCIO_ELECTRONICO.getDescripcionCant());
		detalleNac.setDescripcionMont(NacionalType.NACIONAL_COMERCIO_ELECTRONICO.getDescripcionMonto());
		detalleNac.setCantidad(rs.getInt("CANT_COM_ELECT_NAC"));
		detalleNac.setMonto(rs.getString("MNT_COM_ELECT_NAC"));
		listaTrimestral.add(detalleNac);
		
		detalleNac = new DetalleTrimestral();
		detalleNac.setTipo(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_EN_CAJA);
		detalleNac.setDescripcionCant(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_EN_CAJA.getDescripcionCant());
		detalleNac.setDescripcionMont(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_EN_CAJA.getDescripcionMonto());
		detalleNac.setCantidad(rs.getInt("CANT_DISP_EFECT_NAC"));
		detalleNac.setMonto(rs.getString("MNT_DISP_EFECT_NAC"));
		listaTrimestral.add(detalleNac);
		
		detalleNac = new DetalleTrimestral();
		detalleNac.setTipo(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_CAJERO);
		detalleNac.setDescripcionCant(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_CAJERO.getDescripcionCant());
		detalleNac.setDescripcionMont(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_CAJERO.getDescripcionMonto());
		detalleNac.setCantidad(rs.getInt("CANT_DISP_EFECT_CAJ_NAC"));
		detalleNac.setMonto(rs.getString("MNT_DISP_EFECT_CAJ_NAC"));
		listaTrimestral.add(detalleNac);
		
		return listaTrimestral;
	}


	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private List<DetalleTrimestral> obtenerListaOnus(ResultSet rs)
			throws SQLException {
		List<DetalleTrimestral> listaTrimestral = new ArrayList<DetalleTrimestral>();
		
		DetalleTrimestral detalleOnus = new DetalleTrimestral();
		detalleOnus.setTipo(OnusType.ONUS_TOTAL);
		detalleOnus.setDescripcionCant(OnusType.ONUS_TOTAL.getDescripcionCant());
		detalleOnus.setDescripcionMont(OnusType.ONUS_TOTAL.getDescripcionMont());
		detalleOnus.setCantidad(rs.getInt("CANT_TOTAL_ONUS"));
		detalleOnus.setMonto(rs.getString("MNT_TOTAL_ONUS"));
		listaTrimestral.add(detalleOnus);
		
		detalleOnus = new DetalleTrimestral();
		detalleOnus.setTipo(OnusType.ONUS_CHIP);
		detalleOnus.setDescripcionCant(OnusType.ONUS_CHIP.getDescripcionCant());
		detalleOnus.setDescripcionMont(OnusType.ONUS_CHIP.getDescripcionMont());
		detalleOnus.setCantidad(rs.getInt("CANT_CHIP_ONUS"));
		detalleOnus.setMonto(rs.getString("MNT_CHIP_ONUS"));
		listaTrimestral.add(detalleOnus);
		
		detalleOnus = new DetalleTrimestral();
		detalleOnus.setTipo(OnusType.ONUS_COMERCIO_ELECTRONICO);
		detalleOnus.setDescripcionCant(OnusType.ONUS_COMERCIO_ELECTRONICO.getDescripcionCant());
		detalleOnus.setDescripcionMont(OnusType.ONUS_COMERCIO_ELECTRONICO.getDescripcionMont());
		detalleOnus.setCantidad(rs.getInt("CANT_COM_ELECT_ONUS"));
		detalleOnus.setMonto(rs.getString("MNT_COM_ELECT_ONUS"));
		listaTrimestral.add(detalleOnus);
		
		detalleOnus = new DetalleTrimestral();
		detalleOnus.setTipo(OnusType.ONUS_SERVICIOS_BANCARIOS);
		detalleOnus.setDescripcionCant(OnusType.ONUS_SERVICIOS_BANCARIOS.getDescripcionCant());
		detalleOnus.setDescripcionMont(OnusType.ONUS_SERVICIOS_BANCARIOS.getDescripcionMont());
		detalleOnus.setCantidad(rs.getInt("CANT_SERV_BANC_ONUS"));
		detalleOnus.setMonto(rs.getString("MNT_SERV_BANC_ONUS"));
		listaTrimestral.add(detalleOnus);
		
		detalleOnus = new DetalleTrimestral();
		detalleOnus.setTipo(OnusType.ONUS_TIENDAS_CLIENTE);
		detalleOnus.setDescripcionCant(OnusType.ONUS_TIENDAS_CLIENTE.getDescripcionCant());
		detalleOnus.setDescripcionMont(OnusType.ONUS_TIENDAS_CLIENTE.getDescripcionMont());
		detalleOnus.setCantidad(rs.getInt("CANT_TNDA_CLTE_ONUS"));
		detalleOnus.setMonto(rs.getString("MNT_TNDA_CLTE_ONUS"));
		listaTrimestral.add(detalleOnus);
		
		detalleOnus = new DetalleTrimestral();
		detalleOnus.setTipo(OnusType.ONUS_DISPOSICION_EFECTIVO_EN_CAJA);
		detalleOnus.setDescripcionCant(OnusType.ONUS_DISPOSICION_EFECTIVO_EN_CAJA.getDescripcionCant());
		detalleOnus.setDescripcionMont(OnusType.ONUS_DISPOSICION_EFECTIVO_EN_CAJA.getDescripcionMont());
		detalleOnus.setCantidad(rs.getInt("CANT_DISP_EFECT_ONUS"));
		detalleOnus.setMonto(rs.getString("MNT_DISP_EFECT_ONUS"));
		listaTrimestral.add(detalleOnus);
		
		detalleOnus = new DetalleTrimestral();
		detalleOnus.setTipo(OnusType.ONUS_DISPOSICION_EFECTIVO_CAJERO);
		detalleOnus.setDescripcionCant(OnusType.ONUS_DISPOSICION_EFECTIVO_CAJERO.getDescripcionCant());
		detalleOnus.setDescripcionMont(OnusType.ONUS_DISPOSICION_EFECTIVO_CAJERO.getDescripcionMont());
		detalleOnus.setCantidad(rs.getInt("CANT_DISP_EFECT_CAJ_ONUS"));
		detalleOnus.setMonto(rs.getString("MNT_DISP_EFECT_CAJ_ONUS"));
		listaTrimestral.add(detalleOnus);
		
		return listaTrimestral;
	}
	



	/**
	 * Conecta la base de datos
	 */
	private Connection conectarBD() {
		Connection conexion = null;
		db.OracleBD bdOracle = null;
		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);

		return conexion;
	}
	
	
	/**
	 * Metodo encargado de cerrar las conexioness
	 * @param oracleBD
	 * @param rs
	 * @param conexion
	 */
	private void cerrarConexiones( Connection conexion,CallableStatement stmtProcedure ){

		try {
			if (conexion != null && !conexion.isClosed()) {
				conexion.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}
		
		try {
			if(stmtProcedure!= null && !stmtProcedure.isClosed()){
				stmtProcedure.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(),e);
		}
		
	}

}
