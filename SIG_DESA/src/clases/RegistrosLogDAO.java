package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import mapper.MapperRegistroLog;
import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.SearchType;
import Beans.RegistroLog;
import cl.filter.FiltroRegistroLog;
import cl.util.Utils;
import db.OracleBD;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RegistrosLogDAO {

    static final Logger log = Logger.getLogger(RegistrosLogDAO.class);
    private Properties propiedades;
    private String esquema;

    // paginacion
    private int ultimaPagina = 0;
    private int pagActual;
    private String cantidadRegistros = "";

    /**
     * Constructor
     */
    public RegistrosLogDAO() {
	this.propiedades = PropertiesConfiguration.getProperty();
	this.esquema = propiedades.getProperty("esquema");
	cantidadRegistros = propiedades.getProperty("cantidadRegistros");

    }

    public int getPagActual() {
	return pagActual;
    }

    public void setPagActual(int pagActual) {
	this.pagActual = pagActual;
    }

    public int getUltimaPagina() {
	return ultimaPagina;
    }

    public void setUltimaPagina(int ultimaPagina) {
	this.ultimaPagina = ultimaPagina;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public List<RegistroLog> buscarRegistros(FiltroRegistroLog filtro,
	    String numPagina) {
	
	if (filtro.getSearchType() == SearchType.CON_PAGINACION) {
	    return buscarRegistrosLogPorFiltroConPaginacion(filtro, numPagina);
	} else {
	    return buscarRegistrosLogPorFiltroSinPaginacion(filtro);
	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param filtro
     * @param numPagina
     * @return
     * @since 1.X
     */
    private List<RegistroLog> buscarRegistrosLogPorFiltroConPaginacion(
	    FiltroRegistroLog filtro, String numPagina) {

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet resultados = null;
	List<RegistroLog> listaRegistros = new ArrayList<RegistroLog>();
	String storeProcedure = "BEGIN "
		+ esquema
		+ ".SP_SIG_BUSCAR_REGISTROS_LOG_PG(?, ?, ?, ?, ?, ?, ?, ?); END;";

	try {

	    log.info("Ejecutando SP - SP_SIG_BUSCAR_REGISTROS_LOG_PG");

	    if (numPagina == null) {
		pagActual = 1;
	    } else {
		pagActual = Integer.parseInt(numPagina);
	    }
	    log.info(pagActual);
	    log.info(filtro);
	    this.setPagActual(pagActual);
	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);
	    
	    stmtProcedure = conexion.prepareCall(storeProcedure);

	    // ENTRADA
	    stmtProcedure.setString("p_fecha_busqueda", filtro.getFecha());
	    stmtProcedure.setString("tabla_log", filtro.getLogTableType()
		    .getTableName());
	    stmtProcedure.setInt("numPagina", pagActual);
	    stmtProcedure
		    .setInt("cantReg", Integer.parseInt(cantidadRegistros));

	    log.debug("Filtro : " + filtro);

	    // SALIDA
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
	    stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);

	    stmtProcedure.execute();

	    resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
	    float totReg = stmtProcedure.getInt("totReg");
	    float ultimaPagina;

	    ultimaPagina = Utils.calcularUltimaPagina(totReg,
		    Integer.parseInt(cantidadRegistros));
	    this.setUltimaPagina((int) ultimaPagina);
	    MapperRegistroLog mapper = new MapperRegistroLog();

	    // PARSEA LA LISTA DE RESULTADOS
	    while (resultados.next()) {

		RegistroLog tb = mapper.mapperResultset(resultados,
			filtro.getLogTableType());
		listaRegistros.add(tb);
	    }

	    resultados.close();

	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    e.printStackTrace();
	} finally {

	    if (stmtProcedure != null) {
		bdOracle.cerrarStatement(stmtProcedure);
	    }

	    if (bdOracle != null && !bdOracle.cerrarConexionBD()) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }

	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaRegistros;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param filtro
     * @param numPagina
     * @return
     * @since 1.X
     */
    private List<RegistroLog> buscarRegistrosLogPorFiltroSinPaginacion(
	    FiltroRegistroLog filtro) {

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet resultados = null;
	List<RegistroLog> listaRegistros = new ArrayList<RegistroLog>();
	String storeProcedure = "BEGIN " + esquema
		+ ".SP_SIG_BUSCAR_REGISTROS_LOG(?, ?, ?, ?, ?); END;";

	try {

	    log.info("Ejecutando SP - SP_SIG_BUSCAR_REGISTROS_LOG");

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    stmtProcedure = conexion.prepareCall(storeProcedure);

	    // ENTRADA
	    stmtProcedure.setString("p_fecha_busqueda", filtro.getFecha());
	    stmtProcedure.setString("tabla_log", filtro.getLogTableType()
		    .getTableName());

	    log.debug("Filtro : " + filtro);

	    // SALIDA
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
	    MapperRegistroLog mapper = new MapperRegistroLog();

	    // PARSEA LA LISTA DE RESULTADOS
	    while (resultados.next()) {

		RegistroLog tb = mapper.mapperResultset(resultados,
			filtro.getLogTableType());
		listaRegistros.add(tb);
	    }

	    resultados.close();

	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	} finally {

	    if (stmtProcedure != null) {
		bdOracle.cerrarStatement(stmtProcedure);
	    }

	    if (bdOracle != null && !bdOracle.cerrarConexionBD()) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }

	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaRegistros;
    }

}
