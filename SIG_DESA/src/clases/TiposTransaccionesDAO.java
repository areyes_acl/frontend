package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.AccionType;
import types.StatusType;
import types.TransactionRadioSearchType;
import Beans.AccionTransaccion;
import db.OracleBD;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2015, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class TiposTransaccionesDAO {

    static final Logger log = Logger.getLogger(TransaccionesDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    private String esquema;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * Constructor de clase
     * 
     * @since 1.X
     */
    public TiposTransaccionesDAO() {

	try {
	    Properties propiedades = new Properties();
	    // propiedades.load( getClass().getResourceAsStream(
	    // "/properties/conexion.properties" ) );
	    propiedades = PropertiesConfiguration.getProperty();
	    esquema = propiedades.getProperty("esquema");
	} catch (Exception e) {
	    log.error("Error al leer el archivo de propiedades.");
	    log.error(e.getMessage(), e);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que carga los tipos de busquedas de las transacciones de acuerdo
     * al typo de busqueda de radio buttons ingresado
     * 
     * @param type
     *            Tipo de busqueda Gestion � Consulta
     * @return
     * @since 1.X
     */
    public List<AccionTransaccion> cargarTiposDeBusquedaDeTransacciones(
	    TransactionRadioSearchType searchType) {
	// VARIABLES CONEXXION
	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet resultados = null;
	List<AccionTransaccion> listaAcciones = null;

	try {

	    bdOracle = new OracleBD();
	    conexion = bdOracle.conectar();
	    bdOracle.setConexion(conexion);

	    log.info("Method: cargarTiposDeBusquedaDeTransacciones - Parametros Entrada : "
		    + " tipo Busqueda = " + searchType.getLabel());
	    log.info("Ejecutando SP - SP_SIG_OBTENERTIPOBUSQUEDATRSC");

	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_OBTENERTIPOBUSQUEDATRSC(?, ?, ?, ?); END;");

	    stmtProcedure.setString("tipoBusqueda", searchType.getLabel());
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

	    int codigoError = stmtProcedure.getInt("cod_error");
	    String mensajeError = stmtProcedure.getString("warning");

	    log.info("codigoError :" + codigoError);
	    log.info("mensajeError :" + mensajeError);

	    if (codigoError == 0) {
		listaAcciones = parser(resultados);
	    }

	    // resultados.close();
	    stmtProcedure.close();

	} catch (SQLException e) {
	    bdOracle.setResultado(resultados);
	    bdOracle.cerrarResultSet();
	    bdOracle.cerrarStatement(stmtProcedure);
	    log.error(e.getMessage(), e);

	} finally {
	    bdOracle.cerrarConexiones(null, resultados, conexion, stmtProcedure);
	}

	return listaAcciones;
    }/* Fin buscarTransaccionPresentacion */

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que dado un resulset transforma el cursor a una lista de
     * accionTransaccion
     * 
     * @param result
     * @return
     * @since 1.X
     */
    private List<AccionTransaccion> parser(ResultSet rs) {
	List<AccionTransaccion> listaAcciones = new ArrayList<AccionTransaccion>();

	try {
	    while (rs.next()) {

		AccionTransaccion accionTr = new AccionTransaccion();
		accionTr.setSid(rs.getLong("SID"));
		accionTr.setCodigoxKey(rs.getString("XKEY"));
		accionTr.setCodigoTransaccion(rs.getString("COD_TRANSAC"));
		accionTr.setCodigoFuncion(rs.getString("COD_FUNCION"));
		accionTr.setDescripcion(rs.getString("DESCRIPCION"));
		accionTr.setTipoAccion(AccionType.getTypefromLabelValue(rs
			.getString("TIPO_ACCION")));
		accionTr.setActivo(StatusType.fromOrdinalValue(Integer
			.valueOf(rs.getString("ACTIVO"))));
		accionTr.setVerConsulta(StatusType.fromOrdinalValue(Integer
			.valueOf(rs.getString("VER_CONSULTA"))));
		accionTr.setVerGestion(StatusType.fromOrdinalValue(Integer
			.valueOf(rs.getString("VER_GESTION"))));
		listaAcciones.add(accionTr);
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	}
	log.info("Method: parser - listaAccion " + listaAcciones);
	return listaAcciones;

    }

}
