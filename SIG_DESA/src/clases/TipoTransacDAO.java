/**
 * Descripcion: DAO para mantenedor Tipo Transacción.
 */

package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

public class TipoTransacDAO {
    static final Logger log = Logger.getLogger(ObjecionesDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    private OracleBD obd = new OracleBD();
    private Properties prop = new Properties();

    private String esquema;

    private String listaParametros;
    private String updateTransac;

    private ResultSet rs;

    private Connection conexion;

    /*
     * Constructor
     */
    public TipoTransacDAO() {
	this.setProp();
	esquema = prop.getProperty("esquema");
    }

    private void setProp() {
	// this.prop.load(getClass().getResourceAsStream("/properties/conexion.properties"));
	prop = PropertiesConfiguration.getProperty();
    }

    public void setConexion() {
	this.conexion = obd.conectar();
    }

    public Connection getConexion() {
	return conexion;
    }

    /*
     * Descripción: Obtiene la lista de transacciones.
     * 
     * @param Ninguno.
     * 
     * @throw Exception si hay un error.
     * 
     * @return listaParametros con datos serializados
     * 
     * @version 1.0
     */
    public String getListaTipoTransac() {

	Connection conexion = null;
	setConexion();

	listaParametros = "esttrx:0~";
	rs = obd.consultar("select * from " + esquema + ".TBL_TIPO_TRANSAC");

	obd.setResultado(rs);
	try {
	    if (rs.next()) {
		rs.previous();
		while (rs.next()) {
		    listaParametros += rs.getInt("SID") + "|";
		    listaParametros += rs.getString("DESCRIPCION") + "|";
		    listaParametros += rs.getString("TIPO") + "|";
		    listaParametros += rs.getString("PRODUCTO") + "|";
		    listaParametros += rs.getString("ACTIVO") + ("~");
		}
	    } else {
		listaParametros = "esttrx:1~NO hay datos";
	    }
	    rs.close();
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	} finally {
	    obd.cerrarConexiones(null, rs, conexion, null);
	}

	return listaParametros;
    }

    /*
     * Descripción: Inactiva un registro.
     * 
     * @param sid, estado.
     * 
     * @throw Exception si hay un error.
     * 
     * @return Ok si no hay exceptiones
     * 
     * @version 1.0
     */
    public String cambiarEstadoTipo(String sid, String estado) {
	updateTransac = "esttrx:0~";

	try {
	    setConexion();
	    String sql = "update " + esquema
		    + ".TBL_TIPO_TRANSAC set ACTIVO = " + estado
		    + " where SID = " + sid;

	    obd.ejecutar(sql);
	} catch (Exception e) {
	    updateTransac = "esttrx:1~" + e.getMessage();
	    log.error(e.getMessage(), e);

	} finally {
	   obd.cerrarConexiones(null, rs, conexion, null);
	}

	return updateTransac;
    }
}
