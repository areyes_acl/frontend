package clases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;

import properties.PropertiesConfiguration;
import Beans.TDIExecutionLogBean;

public class TDIExportarDAO {
	
	static final Category log = Logger.getLogger(TDIExportarDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private String directorioExportacion;
	
	private File   archivoSalida;
	public  String fileName;
	
	
	/**
	 * Metodo que genera la planilla excel.
	 * @return Archivo de exportacion .xls
	 * @throws Mensaje de excepcion controlado.
	 * @param dataExcel Datos a ser exprotacdos en planilla.
	 * @param nombreArchivo Nombre del archivo generado.
	 */
	public File generarPlanillaExcel(String dataExcel, String nombreArchivo, List<TDIExecutionLogBean> data){
		
		try {
			
			log.info("Method: generarPlanillaExcel - Obteniendo parametros: nombreArchivo-> "+nombreArchivo);
			
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();
			directorioExportacion = propiedades.getProperty("exportarexcel");
			
		}catch (Exception e){          
			
		    log.error( e.getMessage(), e );
    		return null;
    	}
		
		try{
		    
			String generaArchivoExcel = directorioExportacion + nombreArchivo + ".xls";
			archivoSalida = new File(generaArchivoExcel);
		    
			File file = new File(generaArchivoExcel);
			FileOutputStream fileOut = new FileOutputStream(file);
			
			
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet worksheet = workbook.createSheet("TDI-EJECUCIONES");
			worksheet.setDefaultColumnWidth(30);
			// create style for header cells
		        CellStyle style = workbook.createCellStyle();
		        HSSFFont font = workbook.createFont();
		        font.setBoldweight(Font.BOLDWEIGHT_BOLD); 
		        style.setFont(font);
		        int rowCount=0;
		        int celCount=0;
		        HSSFRow headerliqRow = worksheet.createRow(rowCount++);
	        	headerliqRow.createCell(celCount).setCellValue("PROCESO");
	        	headerliqRow.getCell(celCount++).setCellStyle(style);
	        	headerliqRow.createCell(celCount).setCellValue("INICIO");
	        	headerliqRow.getCell(celCount++).setCellStyle(style);
	        	headerliqRow.createCell(celCount).setCellValue("TERMINO");
	        	headerliqRow.getCell(celCount++).setCellStyle(style);
	        	headerliqRow.createCell(celCount).setCellValue("EJECUCION");
	        	headerliqRow.getCell(celCount++).setCellStyle(style);
	        	headerliqRow.createCell(celCount).setCellValue("ARCHIVO");
	        	headerliqRow.getCell(celCount++).setCellStyle(style);
	        	headerliqRow.createCell(celCount).setCellValue("ESTADO");
	        	headerliqRow.getCell(celCount++).setCellStyle(style);
	        	headerliqRow.createCell(celCount).setCellValue("DESCRIPCION");
	        	headerliqRow.getCell(celCount++).setCellStyle(style);
	        	headerliqRow.createCell(celCount).setCellValue("ORIGEN");
	        	headerliqRow.getCell(celCount++).setCellStyle(style);
	        	headerliqRow.createCell(celCount).setCellValue("DESTINO");
	        	headerliqRow.getCell(celCount).setCellStyle(style);
	        	
		        
			log.info("Cantidad de filas a imprimir: "+data.size());
			
			for(int i=0; i<data.size() && i<65000; i++){
			    
	        		HSSFRow liqRow = worksheet.createRow(rowCount++);
	        		int celcont = 0;
	        		liqRow.createCell(celcont++).setCellValue(data.get(i).getProceso());
	        		liqRow.createCell(celcont++).setCellValue(data.get(i).getHoraInicioShow());
	        		liqRow.createCell(celcont++).setCellValue(data.get(i).getHoraTerminoShow());
	        		liqRow.createCell(celcont++).setCellValue(data.get(i).getDeltaEjecucionShow());
	        		liqRow.createCell(celcont++).setCellValue(procesarString(data.get(i).getArchivoReal()));
	        		liqRow.createCell(celcont++).setCellValue(data.get(i).getEstado());
	        		liqRow.createCell(celcont++).setCellValue(data.get(i).getDescripcionErrorShow());
	        		liqRow.createCell(celcont++).setCellValue(data.get(i).getOrigen());
	        		liqRow.createCell(celcont).setCellValue(data.get(i).getDestino());
			}
			
			workbook.write(fileOut);
			fileOut.flush();
			fileOut.close();
			
			log.info("Fin de impresión");
		
		} catch (FileNotFoundException e) {
		    log.error( e.getMessage(), e );
			return null;
			
		} catch (IOException e) {
		    log.error( e.getMessage(), e );
			return null;
		} catch (Exception e) {
		    log.error( e.getMessage(), e );
			return null;
		}
	
		return archivoSalida;
		
	}
	
	private String procesarString(String[] arreglo){
	    String valor = "";
	    for(int i=0;i<arreglo.length;i++){
		String separador = "";
		if(i>0){
		    separador = "; ";
		}
		valor+=separador+arreglo[i];
	    }
	    return valor;
	}

}
