package clases;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.CobroCargoVisaBean;
import au.com.bytecode.opencsv.CSVWriter;
import cl.util.Utils;
import db.OracleBD;

public class CobroCargoVisaDAO {
    
    static final Logger log =  Logger.getLogger(ObjecionRechazoDiaDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    
    private OracleBD obd = new OracleBD();
    private Properties prop = new Properties();
    private String esquema;
    private String listaCodigosRazon;
    private Map<String, Object> sessionMap;
    private String cantidadRegistros="";
    private int ultimaPagina = 0;
    private int pagActual;
    private Connection conexion;
	
    private ResultSet rs;
    private ResultSet rs2;
    
    Vector<CobroCargoVisaBean> listaCobroCargoVisa = new Vector<CobroCargoVisaBean>();
    
    public CobroCargoVisaDAO(){
	try {
	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    cantidadRegistros = propiedades.getProperty("cantidadRegistros");
	
	    
	}catch (Exception e){          
	   log.error("Error al leer el archivo de propiedades.");
	   log.error(e.getMessage(), e);
	}	
    }
    
    public void setConexion(){
	this.conexion = obd.conectar();
	obd.setResultado(rs);
    }
	
    public Connection getConexion(){
	return conexion;
    }
    public void setConexion(Connection conexion) {
	this.conexion = conexion;
    }
    
    public int getUltimaPagina() {
        return ultimaPagina;
    }

    public void setUltimaPagina(int ultimaPagina) {
        this.ultimaPagina = ultimaPagina;
    }

    public int getPagActual() {
        return pagActual;
    }

    public void setPagActual(int pagActual) {
        this.pagActual = pagActual;
    }
    
    public Vector<CobroCargoVisaBean> listaCobroCargo(String fechaDesde, String fechaHasta, String TipoTrx, String numPag, String estadoTrx, int operador) throws SQLException{
	log.info("Se buscan las trx de cobro cargo");
	
	setConexion();
	int pagActual;
	
	if(numPag == null)
	{
		pagActual = 1;
	}else{
		pagActual = Integer.parseInt(numPag);
	}
	
	int cantReg = Integer.parseInt(cantidadRegistros);
	String sql = "";
	String sql2 = "";
	
	
	log.info( " numPagina : " + pagActual  +" - cantReg: " + cantidadRegistros );
	PreparedStatement stmt = null;
	PreparedStatement stmt2 = null;
	
	
	log.info(estadoTrx);
	
	if(estadoTrx.equals("0")){
	    
	    // Busca todas las transacciones
	    sql += "select * from (SELECT row_number() OVER (ORDER BY CCV.SID)  AS REGISTRO, CCV.SID AS SID, CCV.MIT AS MIT, CCV.ACCOUNT_NUMBER AS ACCOUNT_NUMBER, CCV.SOURCE_BIN AS SOURCE_BIN, CRV.COD_MOTIVO_INI AS COD_MOTIVO_INI, CRV.DESCRIPCION AS DESCRIPCION, TP.COD_PAIS AS COD_PAIS, CCV.SOURCE_AMOUNT AS SOURCE_AMOUNT, CCV.SOURCE_CURRENCY_CODE AS SOURCE_CURRENCY_CODE, CCV.DESTINATION_AMOUNT AS DESTINATION_AMOUNT, CCV.DESTINATION_CURRENCY_CODE AS DESTINATION_CURRENCY_CODE, TO_CHAR(CCV.CENTRAL_PROCESSING_DATE, 'dd/mm/yyyy') AS CENTRAL_PROCESSING_DATE, TO_CHAR(CCV.EVENT_DATE, 'dd/mm/yyyy') AS EVENT_DATE, CCV.ACCOUNT_NUMBER_EXTENSION AS ACCOUNT_NUMBER_EXTENSION, CCV.DESTINATION_BIN AS DESTINATION_BIN, CCV.SETTLEMENT_FLAG AS SETTLEMENT_FLAG, CCV.TRANSACTION_IDENTIFIER AS TRANSACTION_IDENTIFIER, CCV.RESERVED AS RESERVED, CCV.REIMBURSEMENT_ATTRIBUTE AS REIMBURSEMENT_ATTRIBUTE, CCV.MESSAGE_TEXT AS MESSAGE_TEXT, GCV.TRANSACCION AS TRANSACGEST, GCV.MIT AS MITGEST, UIG.NOMBRE AS USUARIO, GCV.MENSAJE AS MENSAJE, TO_CHAR(GCV.FECHA_INGRESO, 'dd/mm/yyyy') AS FECHA_INGRESO, (SELECT CR.COD_MOTIVO_INI FROM TBL_COD_RAZON_VISA CR WHERE CR.SID = GCV.REASON_CODE) AS CODMOTGEST FROM TBL_COBRO_CARGO_VISA CCV LEFT JOIN TBL_COD_RAZON_VISA CRV ON(CCV.REASON_CODE = CRV.SID) LEFT JOIN TBL_PAIS TP ON (CCV.COUNTRY_CODE = TP.SID) LEFT JOIN TBL_GESTION_CC_VISA GCV ON(GCV.TRANSACCION = CCV.SID) LEFT JOIN TBL_USUARIO_IG UIG ON(GCV.USUARIO = UIG.SID) WHERE CCV.CENTRAL_PROCESSING_DATE BETWEEN TO_DATE(?, 'DD-MM-YY') AND TO_DATE(?, 'DD-MM-YY') AND CCV.MIT = ? AND CCV.OPERADOR = ? ORDER BY CCV.SID) WHERE REGISTRO BETWEEN (? - 1) * ? + 1 AND  ? * ?";
	    //query del total de registros para paginacion
	    sql2 += "select count(*) AS TOTAL FROM (SELECT ROWNUM  AS REGISTRO,t.* FROM (SELECT CCV.SID AS SID, CCV.MIT AS MIT, CCV.ACCOUNT_NUMBER AS ACCOUNT_NUMBER, CCV.SOURCE_BIN AS SOURCE_BIN, CRV.COD_MOTIVO_INI AS COD_MOTIVO_INI, CRV.DESCRIPCION AS DESCRIPCION, TP.COD_PAIS AS COD_PAIS, CCV.SOURCE_AMOUNT AS SOURCE_AMOUNT, CCV.SOURCE_CURRENCY_CODE AS SOURCE_CURRENCY_CODE, CCV.DESTINATION_AMOUNT AS DESTINATION_AMOUNT, CCV.DESTINATION_CURRENCY_CODE AS DESTINATION_CURRENCY_CODE, TO_CHAR(CCV.CENTRAL_PROCESSING_DATE, 'dd/mm/yyyy') AS CENTRAL_PROCESSING_DATE, TO_CHAR(CCV.EVENT_DATE, 'dd/mm/yyyy') AS EVENT_DATE, CCV.ACCOUNT_NUMBER_EXTENSION AS ACCOUNT_NUMBER_EXTENSION, CCV.DESTINATION_BIN AS DESTINATION_BIN, CCV.SETTLEMENT_FLAG AS SETTLEMENT_FLAG, CCV.TRANSACTION_IDENTIFIER AS TRANSACTION_IDENTIFIER, CCV.RESERVED AS RESERVED, CCV.REIMBURSEMENT_ATTRIBUTE AS REIMBURSEMENT_ATTRIBUTE, CCV.MESSAGE_TEXT AS MESSAGE_TEXT, GCV.TRANSACCION AS TRANSACGEST FROM TBL_COBRO_CARGO_VISA CCV LEFT JOIN TBL_COD_RAZON_VISA CRV ON(CCV.REASON_CODE = CRV.SID) LEFT JOIN TBL_PAIS TP ON (CCV.COUNTRY_CODE = TP.SID) LEFT JOIN TBL_GESTION_CC_VISA GCV ON(GCV.TRANSACCION = CCV.SID) WHERE CCV.CENTRAL_PROCESSING_DATE BETWEEN TO_DATE(?, 'DD-MM-YY') AND TO_DATE(?, 'DD-MM-YY') AND CCV.MIT = ? AND CCV.OPERADOR = ? ORDER BY CCV.SID)t )";
	    
	    stmt = this.conexion.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE , ResultSet.CONCUR_READ_ONLY);
	    stmt.setString(1, fechaDesde);
	    stmt.setString(2, fechaHasta);
	    stmt.setString(3, TipoTrx);
	    stmt.setInt(4, operador);
	    stmt.setInt(5, pagActual);
	    stmt.setInt(6, cantReg);
	    stmt.setInt(7, pagActual);
	    stmt.setInt(8, cantReg);
	    
	    stmt2 = this.conexion.prepareStatement(sql2, ResultSet.TYPE_SCROLL_INSENSITIVE , ResultSet.CONCUR_READ_ONLY);
	    stmt2.setString(1, fechaDesde);
	    stmt2.setString(2, fechaHasta);
	    stmt2.setString(3, TipoTrx);
	    stmt2.setInt(4, operador);
	    
	}
	else if(estadoTrx.equals("1")){
	    
	    //busca las transacciones ingresadas que no tengan gestion
	    sql += "select * from (SELECT row_number() OVER (ORDER BY CCV.SID)  AS REGISTRO, CCV.SID AS SID, CCV.MIT AS MIT, CCV.ACCOUNT_NUMBER AS ACCOUNT_NUMBER, CCV.SOURCE_BIN AS SOURCE_BIN, CRV.COD_MOTIVO_INI AS COD_MOTIVO_INI, CRV.DESCRIPCION AS DESCRIPCION, TP.COD_PAIS AS COD_PAIS, CCV.SOURCE_AMOUNT AS SOURCE_AMOUNT, CCV.SOURCE_CURRENCY_CODE AS SOURCE_CURRENCY_CODE, CCV.DESTINATION_AMOUNT AS DESTINATION_AMOUNT, CCV.DESTINATION_CURRENCY_CODE AS DESTINATION_CURRENCY_CODE, TO_CHAR(CCV.CENTRAL_PROCESSING_DATE, 'dd/mm/yyyy') AS CENTRAL_PROCESSING_DATE, TO_CHAR(CCV.EVENT_DATE, 'dd/mm/yyyy') AS EVENT_DATE, CCV.ACCOUNT_NUMBER_EXTENSION AS ACCOUNT_NUMBER_EXTENSION, CCV.DESTINATION_BIN AS DESTINATION_BIN, CCV.SETTLEMENT_FLAG AS SETTLEMENT_FLAG, CCV.TRANSACTION_IDENTIFIER AS TRANSACTION_IDENTIFIER, CCV.RESERVED AS RESERVED, CCV.REIMBURSEMENT_ATTRIBUTE AS REIMBURSEMENT_ATTRIBUTE, CCV.MESSAGE_TEXT AS MESSAGE_TEXT, GCV.TRANSACCION AS TRANSACGEST, GCV.MIT AS MITGEST, UIG.NOMBRE AS USUARIO, GCV.MENSAJE AS MENSAJE, TO_CHAR(GCV.FECHA_INGRESO, 'dd/mm/yyyy') AS FECHA_INGRESO, (SELECT CR.COD_MOTIVO_INI FROM TBL_COD_RAZON_VISA CR WHERE CR.SID = GCV.REASON_CODE) AS CODMOTGEST FROM TBL_COBRO_CARGO_VISA CCV LEFT JOIN TBL_COD_RAZON_VISA CRV ON(CCV.REASON_CODE = CRV.SID)  LEFT JOIN TBL_PAIS TP  ON (CCV.COUNTRY_CODE = TP.SID) LEFT JOIN TBL_GESTION_CC_VISA GCV ON (CCV.SID = GCV.TRANSACCION)LEFT JOIN TBL_USUARIO_IG UIG ON(GCV.USUARIO = UIG.SID) WHERE  CCV.CENTRAL_PROCESSING_DATE BETWEEN TO_DATE(?, 'DD-MM-YY') AND TO_DATE(?, 'DD-MM-YY') AND CCV.MIT = ? AND CCV.OPERADOR = ? AND GCV.TRANSACCION IS NULL ORDER BY CCV.SID) WHERE REGISTRO BETWEEN (? - 1) * ? + 1 AND  ? * ?";  
	    
	    sql2 += "select count(*) AS TOTAL FROM (SELECT ROWNUM  AS REGISTRO,t.* FROM (SELECT CCV.SID AS SID, CCV.MIT AS MIT, CCV.ACCOUNT_NUMBER AS ACCOUNT_NUMBER, CCV.SOURCE_BIN AS SOURCE_BIN, CRV.COD_MOTIVO_INI AS COD_MOTIVO_INI, CRV.DESCRIPCION AS DESCRIPCION, TP.COD_PAIS AS COD_PAIS, CCV.SOURCE_AMOUNT AS SOURCE_AMOUNT, CCV.SOURCE_CURRENCY_CODE AS SOURCE_CURRENCY_CODE, CCV.DESTINATION_AMOUNT AS DESTINATION_AMOUNT, CCV.DESTINATION_CURRENCY_CODE AS DESTINATION_CURRENCY_CODE, TO_CHAR(CCV.CENTRAL_PROCESSING_DATE, 'dd/mm/yyyy') AS CENTRAL_PROCESSING_DATE, TO_CHAR(CCV.EVENT_DATE, 'dd/mm/yyyy') AS EVENT_DATE, CCV.ACCOUNT_NUMBER_EXTENSION AS ACCOUNT_NUMBER_EXTENSION, CCV.DESTINATION_BIN AS DESTINATION_BIN, CCV.SETTLEMENT_FLAG AS SETTLEMENT_FLAG, CCV.TRANSACTION_IDENTIFIER AS TRANSACTION_IDENTIFIER, CCV.RESERVED AS RESERVED, CCV.REIMBURSEMENT_ATTRIBUTE AS REIMBURSEMENT_ATTRIBUTE, CCV.MESSAGE_TEXT AS MESSAGE_TEXT, GCV.TRANSACCION AS TRANSACGEST FROM TBL_COBRO_CARGO_VISA CCV LEFT JOIN TBL_COD_RAZON_VISA CRV ON(CCV.REASON_CODE = CRV.SID) LEFT JOIN TBL_PAIS TP  ON (CCV.COUNTRY_CODE = TP.SID) LEFT JOIN TBL_GESTION_CC_VISA GCV ON (CCV.SID = GCV.TRANSACCION) WHERE  CCV.CENTRAL_PROCESSING_DATE BETWEEN TO_DATE(?, 'DD-MM-YY') AND TO_DATE(?, 'DD-MM-YY') AND CCV.MIT = ? AND CCV.OPERADOR = ? AND GCV.TRANSACCION IS NULL ORDER BY CCV.SID)t )";
	    
	    stmt = this.conexion.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE , ResultSet.CONCUR_READ_ONLY);
	    stmt.setString(1, fechaDesde);
	    stmt.setString(2, fechaHasta);
	    stmt.setString(3, TipoTrx);
	    stmt.setInt(4, operador);
	    stmt.setInt(5, pagActual);
	    stmt.setInt(6, cantReg);
	    stmt.setInt(7, pagActual);
	    stmt.setInt(8, cantReg);
	    
	    stmt2 = this.conexion.prepareStatement(sql2, ResultSet.TYPE_SCROLL_INSENSITIVE , ResultSet.CONCUR_READ_ONLY);
	    stmt2.setString(1, fechaDesde);
	    stmt2.setString(2, fechaHasta);
	    stmt2.setString(3, TipoTrx);
	    stmt2.setInt(4, operador);
	    
	    
	    
	}
	else{
	    
	    //busca las transacciones con alguna gestion asociada
	    sql += "select * from (SELECT  row_number() OVER (ORDER BY CCV.SID)  AS REGISTRO, CCV.SID AS SID, CCV.MIT AS MIT, CCV.ACCOUNT_NUMBER AS ACCOUNT_NUMBER, CCV.SOURCE_BIN AS SOURCE_BIN, CRV.COD_MOTIVO_INI AS COD_MOTIVO_INI, CRV.DESCRIPCION AS DESCRIPCION, TP.COD_PAIS AS COD_PAIS, CCV.SOURCE_AMOUNT AS SOURCE_AMOUNT, CCV.SOURCE_CURRENCY_CODE AS SOURCE_CURRENCY_CODE, CCV.DESTINATION_AMOUNT AS DESTINATION_AMOUNT, CCV.DESTINATION_CURRENCY_CODE AS DESTINATION_CURRENCY_CODE, TO_CHAR(CCV.CENTRAL_PROCESSING_DATE, 'dd/mm/yyyy') AS CENTRAL_PROCESSING_DATE, TO_CHAR(CCV.EVENT_DATE, 'dd/mm/yyyy') AS EVENT_DATE, CCV.ACCOUNT_NUMBER_EXTENSION AS ACCOUNT_NUMBER_EXTENSION, CCV.DESTINATION_BIN AS DESTINATION_BIN, CCV.SETTLEMENT_FLAG AS SETTLEMENT_FLAG, CCV.TRANSACTION_IDENTIFIER AS TRANSACTION_IDENTIFIER, CCV.RESERVED AS RESERVED, CCV.REIMBURSEMENT_ATTRIBUTE AS REIMBURSEMENT_ATTRIBUTE, CCV.MESSAGE_TEXT AS MESSAGE_TEXT, GCV.TRANSACCION AS TRANSACGEST,GCV.MIT AS MITGEST, UIG.NOMBRE AS USUARIO, GCV.MENSAJE AS MENSAJE, TO_CHAR(GCV.FECHA_INGRESO, 'dd/mm/yyyy') AS FECHA_INGRESO, (SELECT CR.COD_MOTIVO_INI FROM TBL_COD_RAZON_VISA CR WHERE CR.SID = GCV.REASON_CODE) AS CODMOTGEST FROM TBL_COBRO_CARGO_VISA CCV LEFT JOIN TBL_COD_RAZON_VISA CRV ON(CCV.REASON_CODE = CRV.SID) LEFT JOIN TBL_PAIS TP  ON (CCV.COUNTRY_CODE = TP.SID) LEFT JOIN TBL_GESTION_CC_VISA GCV ON (CCV.SID = GCV.TRANSACCION) LEFT JOIN TBL_ESTADO_PROCESO EP ON (GCV.ESTADO_PROCESO = EP.SID)LEFT JOIN TBL_USUARIO_IG UIG ON(GCV.USUARIO = UIG.SID) WHERE  CCV.CENTRAL_PROCESSING_DATE BETWEEN TO_DATE(?, 'DD-MM-YY') AND TO_DATE(?, 'DD-MM-YY') AND CCV.MIT = ? AND CCV.OPERADOR = ? AND EP.XKEY = ? ORDER BY CCV.SID) WHERE REGISTRO BETWEEN (? - 1) * ? + 1 AND  ? * ?";    
	    sql2 += "select count(*) AS TOTAL FROM (SELECT ROWNUM  AS REGISTRO,t.* FROM (SELECT CCV.SID AS SID, CCV.MIT AS MIT, CCV.ACCOUNT_NUMBER AS ACCOUNT_NUMBER, CCV.SOURCE_BIN AS SOURCE_BIN, CRV.COD_MOTIVO_INI AS COD_MOTIVO_INI, CRV.DESCRIPCION AS DESCRIPCION, TP.COD_PAIS AS COD_PAIS, CCV.SOURCE_AMOUNT AS SOURCE_AMOUNT, CCV.SOURCE_CURRENCY_CODE AS SOURCE_CURRENCY_CODE, CCV.DESTINATION_AMOUNT AS DESTINATION_AMOUNT, CCV.DESTINATION_CURRENCY_CODE AS DESTINATION_CURRENCY_CODE, TO_CHAR(CCV.CENTRAL_PROCESSING_DATE, 'dd/mm/yyyy') AS CENTRAL_PROCESSING_DATE, TO_CHAR(CCV.EVENT_DATE, 'dd/mm/yyyy') AS EVENT_DATE, CCV.ACCOUNT_NUMBER_EXTENSION AS ACCOUNT_NUMBER_EXTENSION, CCV.DESTINATION_BIN AS DESTINATION_BIN, CCV.SETTLEMENT_FLAG AS SETTLEMENT_FLAG, CCV.TRANSACTION_IDENTIFIER AS TRANSACTION_IDENTIFIER, CCV.RESERVED AS RESERVED, CCV.REIMBURSEMENT_ATTRIBUTE AS REIMBURSEMENT_ATTRIBUTE, CCV.MESSAGE_TEXT AS MESSAGE_TEXT, GCV.TRANSACCION AS TRANSACGEST FROM TBL_COBRO_CARGO_VISA CCV LEFT JOIN TBL_COD_RAZON_VISA CRV ON(CCV.REASON_CODE = CRV.SID) LEFT JOIN TBL_PAIS TP  ON (CCV.COUNTRY_CODE = TP.SID) LEFT JOIN TBL_GESTION_CC_VISA GCV ON (CCV.SID = GCV.TRANSACCION) LEFT JOIN TBL_ESTADO_PROCESO EP ON (GCV.ESTADO_PROCESO = EP.SID) WHERE  CCV.CENTRAL_PROCESSING_DATE BETWEEN TO_DATE(?, 'DD-MM-YY') AND TO_DATE(?, 'DD-MM-YY') AND CCV.MIT = ? AND CCV.OPERADOR = ? AND EP.XKEY = ? ORDER BY CCV.SID )t )";
	    
	    
	    stmt = this.conexion.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE , ResultSet.CONCUR_READ_ONLY);
	    stmt.setString(1, fechaDesde);
	    stmt.setString(2, fechaHasta);
	    stmt.setString(3, TipoTrx);
	    stmt.setInt(4, operador);
	    stmt.setString(5, estadoTrx);
	    stmt.setInt(6, pagActual);
	    stmt.setInt(7, cantReg);
	    stmt.setInt(8, pagActual);
	    stmt.setInt(9, cantReg);
	    
	    stmt2 = this.conexion.prepareStatement(sql2, ResultSet.TYPE_SCROLL_INSENSITIVE , ResultSet.CONCUR_READ_ONLY);
	    stmt2.setString(1, fechaDesde);
	    stmt2.setString(2, fechaHasta);
	    stmt2.setString(3, TipoTrx);
	    stmt2.setInt(4, operador);
	    stmt2.setString(5, estadoTrx);
	    
	    
	}
	
        rs = stmt.executeQuery();
        rs2 = stmt2.executeQuery();
        log.info(rs);
        log.info(rs2);
        obd.setResultado(rs);
        obd.setResultado(rs2);
        
        try {
            float totReg = 0;
            while(rs2.next()){
        	totReg = rs2.getInt("TOTAL");
            }
            
            int ultimaPagina;

            ultimaPagina = (int)Utils.calcularUltimaPagina(totReg, cantReg);
            
            this.setPagActual(pagActual);
    	    this.setUltimaPagina(ultimaPagina);
            
            
		if(rs.next()){
		    log.info("Method: listaCobroCargo - Obteniendo resultados");
		    rs.previous();
		    while(rs.next()){
			CobroCargoVisaBean cobroCargoVisa = new CobroCargoVisaBean();
			cobroCargoVisa.setSid(rs.getInt("SID"));
			cobroCargoVisa.setMit(rs.getString("MIT"));
			cobroCargoVisa.setAccountNumber(rs.getString("ACCOUNT_NUMBER"));
			cobroCargoVisa.setSourceBin(rs.getString("SOURCE_BIN"));
			cobroCargoVisa.setReasonCode(rs.getString("COD_MOTIVO_INI"));
			cobroCargoVisa.setCountryCode(rs.getString("COD_PAIS"));
			cobroCargoVisa.setSourceAmount(rs.getInt("SOURCE_AMOUNT"));
			cobroCargoVisa.setSourceCurrencyCode(rs.getString("SOURCE_CURRENCY_CODE"));
			cobroCargoVisa.setDestinationAmount(rs.getInt("DESTINATION_AMOUNT"));
			cobroCargoVisa.setDestinationCurrencyCode(rs.getString("DESTINATION_CURRENCY_CODE"));
			cobroCargoVisa.setCentralProcessingDate(rs.getString("CENTRAL_PROCESSING_DATE"));
			cobroCargoVisa.setEventDate(rs.getString("EVENT_DATE"));
			cobroCargoVisa.setAccountNumberExtension(rs.getString("ACCOUNT_NUMBER_EXTENSION"));
			cobroCargoVisa.setDestinationBin(rs.getString("DESTINATION_BIN"));
			cobroCargoVisa.setSettlementFlag(rs.getString("SETTLEMENT_FLAG"));
			cobroCargoVisa.setTransactionIdentifier(rs.getString("TRANSACTION_IDENTIFIER"));
			cobroCargoVisa.setReserved(rs.getString("RESERVED"));
			cobroCargoVisa.setReimbursementAttribute(rs.getString("REIMBURSEMENT_ATTRIBUTE"));
			cobroCargoVisa.setMessageText(rs.getString("MESSAGE_TEXT"));
			cobroCargoVisa.setTransaccionConGestion(rs.getString("TRANSACGEST"));
			cobroCargoVisa.setDescripcionCodRazon(rs.getString("DESCRIPCION"));
			cobroCargoVisa.setMitReversa(rs.getString("MITGEST"));
			cobroCargoVisa.setUsuarioReversa(rs.getString("USUARIO"));
			cobroCargoVisa.setMensajeReversa(rs.getString("MENSAJE"));
			cobroCargoVisa.setFechaReversa(rs.getString("FECHA_INGRESO"));
			cobroCargoVisa.setCodigoMotivoReversa(rs.getString("CODMOTGEST"));
			
			//cobroCargoVisa.setOperador(rs.getString("OPERADOR"));
			//cobroCargoVisa.setFechaIncoming(rs.getString("FECHA_INCOMING"));
			//cobroCargoVisa.setDatosAdicionales(rs.getString("DATOS_ADICIONALES"));
			
			listaCobroCargoVisa.add(cobroCargoVisa);
			
		    }
		    rs.close();
			
		}else{
		    log.info("Method: getListaParametros - No hay datos");
		}
		log.info("Method: getListaParametros - Serializando lista");
        } catch (SQLException e) {
		
		log.error(e.getMessage(),e);
		
        }finally{
		if (obd.cerrarResultSet()==false){
		    log.error("Error: No se pudo cerrar resulset.");
		    if (obd.cerrarConexionBD()==false){
			 log.error("Error: No se pudo cerrar conexion a la base de datos.");
		    }			
		}else{
		    if (obd.cerrarConexionBD()==false){
			 log.error("Error: No se pudo cerrar conexion a la base de datos.");
		    }	
		}
		try {
		    if(conexion != null && !conexion.isClosed()){
			conexion.close();
		    }
		}
		catch ( SQLException e ) {
		    log.error(e.getMessage(),e);
		}
        } 
	log.info(listaCobroCargoVisa);
	return listaCobroCargoVisa;
    }
    
    
    public String getCodigosRazonVisa() {
	log.info("Method: getListaOperadores - Obtencion de parametros: ninguno");

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs = null;

	bdOracle = new OracleBD();
	conexion = bdOracle.conectar();
	bdOracle.setConexion(conexion);

	log.info("Method: getListaOperadores - Obteniendo operadores");
	listaCodigosRazon = "esttrx:0~";
	rs = bdOracle.consultar("SELECT * FROM TBL_COD_RAZON_VISA WHERE ACTIVO = 1 ORDER BY SID");
	bdOracle.setResultado(rs);

	try {

	    if (rs.next()) {
		log.info("Method: getListaOperadores - Obteniendo resulstados");
		rs.previous();
		while (rs.next()) {
		    listaCodigosRazon += rs.getInt("SID") + "|";
		    listaCodigosRazon += rs.getString("DESCRIPCION") + "|";
		    listaCodigosRazon += rs.getString("COD_MOTIVO_INI") + "|";
		    listaCodigosRazon += rs.getInt("MAX_AMOUNT") + "|";
		    listaCodigosRazon += rs.getString("INFORMACION") + "~";
		    
		}
		rs.close();
		log.info("Method: getListaOperadores - " + listaCodigosRazon);
	    } else {
		listaCodigosRazon = "esttrx:1~No hay datos";
	    }
	    log.info("Method: getListaOperadores - Serializando lista");
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    bdOracle.cerrarConexionBD();
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaCodigosRazon;
    }
    
    public void reversarTransaccion(int sidTrx, String mitTrx, String codRazon, String glosa, Long sidUsuario){
	try{
	    
	    if(mitTrx.equals("10")){
		mitTrx = "20";
	    }
	    else if(mitTrx.equals("20")){
		mitTrx = "10";
	    }
	    
	    setConexion();
	    String sql = "INSERT INTO TBL_GESTION_CC_VISA VALUES (SEQ_TBL_GESTION_CC_VISA.NEXTVAL, "+sidTrx+", '"+mitTrx+"', "+sidUsuario+", '"+glosa+"', SYSTIMESTAMP, (SELECT SID FROM TBL_ESTADO_PROCESO WHERE XKEY = 'OU_PEN'), null, null, '"+codRazon+"')"; 
	    
	    rs = obd.consultar(sql);
	    obd.setResultado(rs);
	} 
	catch (Exception e) {
	    log.info(e.getMessage());
	    
	}
	finally {
	    if (obd.cerrarResultSet()==false){
		log.error("Error: No se pudo cerrar resulset.");
		if (obd.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}			
	    }else{
		if (obd.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}	
	    }
	    try {
		if(conexion != null && !conexion.isClosed()){
		    conexion.close();
		}
	    }
	    catch ( SQLException e ) {
		log.error(e.getMessage(),e);
	    }
	}
	
	
    }
    
    public boolean eliminarReversa(int sidTrx){
	
	try{
	    setConexion();
	    String sql = "DELETE FROM TBL_GESTION_CC_VISA WHERE TRANSACCION = "+sidTrx+""; 
	    //log.info(sql);
	    rs = obd.consultar(sql);
	    obd.setResultado(rs);
	    return true;
	} 
	catch (Exception e) {
	    log.info(e.getMessage());
	    return false;
	}
	finally {
	    if (obd.cerrarResultSet()==false){
		log.error("Error: No se pudo cerrar resulset.");
		if (obd.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}			
	    }else{
		if (obd.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}	
	    }
	    try {
		if(conexion != null && !conexion.isClosed()){
		    conexion.close();
		}
	    }
	    catch ( SQLException e ) {
		log.error(e.getMessage(),e);
	    }
	}
	
	
    }
    

}
