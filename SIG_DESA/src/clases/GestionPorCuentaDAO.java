package clases;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Vector;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import cl.util.Utils;
import properties.PropertiesConfiguration;
import Beans.GestionPresentacionBean;
import db.OracleBD;

/**
 * Clase DAO permite consultar los movimientos historicos de las transacciones.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class GestionPorCuentaDAO {

	static final Category log =  Logger.getLogger(GestionPorCuentaDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	private int ultimaPagina = 0;
	private int pagActual;
	private String esquema;
	private String cantidadRegistros;
	private String fechaProceso = "";
	private String[] fechaGregoriana = null;
	private String fechaProcesoFinal="";
	
	private Vector<GestionPresentacionBean> gestionesBeanVector = new Vector<GestionPresentacionBean>();
	
	public GestionPorCuentaDAO(){
		try {
			Properties propiedades = new Properties();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
		    esquema = propiedades.getProperty("esquema");
		    cantidadRegistros = propiedades.getProperty("cantidadRegistros");
		    
		}catch (Exception e){          
		    log.error( e.getMessage(), e );
    	}
	}
	
	/**
	 * Metodo que obtiene las transacciones de presentacion.
	 * @return lista de transacciones de presentacion.
	 * @throws Mensaje de excepcion controlado.
	 * @param numeroTarjeta Numero de tarjeta.
	 * @param fechaInicio Fecha de inicio.
	 * @param fechaTermino Fecha de termino.
	 */
	public String buscarGestionPorCuentaPresentaciones(String numeroTarjeta, String fechaInicio, String fechaTermino){

		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		String listaPresentaciones="";
		
		
		try {
			log.info("Method: buscarGestionPorCuentaPresentaciones - Obteniendo parametros: numeroTarjeta-> "+numeroTarjeta+" | fechaInicio-> "+fechaInicio+" | fechaTermino-> "+fechaTermino);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarGestionPorCuenta - Ejecutando SP_SIG_GESTIONPORCUENTAPRESENT");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_GESTIONPORCUENTAPRESENT(?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
				if (resultados.next()){
					log.info("Method: buscarGestionPorCuentaPresentaciones - Obteniendo resultados");
					listaPresentaciones="esttrx|0~";
					listaPresentaciones += resultados.getInt("SIDINCOMMING") + "|";
					listaPresentaciones += resultados.getString("MIT") + "|";
					listaPresentaciones += resultados.getString("CODIGOFUNCION") + "|";
					listaPresentaciones += resultados.getString("NUMEROTARJETA") + "|";
					listaPresentaciones += resultados.getString("FECHATRANSAC") + "|";
					
					String valor = resultados.getString("COMERCIOPAIS");
					String[] comerciopais;
					String comercio    = "";
					String regionpais  = "";
					String region      = "";
					String provincia   = "";
					String pais        = "";
					if(valor != null){
						if (valor.length() == 36 || valor.length() == 43) {
							comerciopais = valor.split("\\|");
							comercio = comerciopais[0];
							regionpais = comerciopais[1];
							region = comerciopais[1];
							provincia = comerciopais[1];
							pais = comerciopais[2];
						} else {
							comerciopais = valor.split("\\|");
							comercio = comerciopais[0];
							regionpais = comerciopais[3];
							region = regionpais.substring(0, 9);
							provincia = regionpais.substring(10, 13);
							pais = regionpais.substring(13);
						}
					}
					
					listaPresentaciones += comercio + "|";
					listaPresentaciones += pais + "|";
					
					listaPresentaciones += resultados.getBigDecimal("MONTOTRANSACCION") + "|"; 
					listaPresentaciones += resultados.getBigDecimal("MONTOFACTURACION") + "|"; 
					listaPresentaciones += resultados.getBigDecimal("MONTOCONCILIACION") + "|"; 
					listaPresentaciones += resultados.getString("CODIGORAZON") + "|";
					listaPresentaciones += resultados.getString("CODMONEDATRX") + "|";
					listaPresentaciones += resultados.getString("GLOSATIPO") + "|";
					listaPresentaciones += resultados.getString("GLOSATIPOMONEDA") + "|";
					listaPresentaciones += resultados.getString("ESTATUS") + "~";
					
					while (resultados.next()) {
						
						listaPresentaciones += resultados.getInt("SIDINCOMMING") + "|";
						listaPresentaciones += resultados.getString("MIT") + "|";
						listaPresentaciones += resultados.getString("CODIGOFUNCION") + "|";
						listaPresentaciones += resultados.getString("NUMEROTARJETA") + "|";
						listaPresentaciones += resultados.getString("FECHATRANSAC") + "|";
						
						valor = resultados.getString("COMERCIOPAIS");
						if(valor != null){
							if (valor.length() == 36 || valor.length() == 43) {
								comerciopais = valor.split("\\|");
								comercio = comerciopais[0];
								regionpais = comerciopais[1];
								region = comerciopais[1];
								provincia = comerciopais[1];
								pais = comerciopais[2];
							} else {
								comerciopais = valor.split("\\|");
								comercio = comerciopais[0];
								regionpais = comerciopais[3];
								region = regionpais.substring(0, 9);
								provincia = regionpais.substring(10, 13);
								pais = regionpais.substring(13);
							}
						}
						
						listaPresentaciones += comercio + "|";
						listaPresentaciones += pais + "|";
						
						listaPresentaciones += resultados.getBigDecimal("MONTOTRANSACCION") + "|"; 
						listaPresentaciones += resultados.getBigDecimal("MONTOFACTURACION") + "|"; 
						listaPresentaciones += resultados.getBigDecimal("MONTOCONCILIACION") + "|"; 
						listaPresentaciones += resultados.getString("CODIGORAZON") + "|";
						listaPresentaciones += resultados.getString("CODMONEDATRX") + "|";
						listaPresentaciones += resultados.getString("GLOSATIPO") + "|";
						listaPresentaciones += resultados.getString("GLOSATIPOMONEDA") + "|";
						listaPresentaciones += resultados.getString("ESTATUS") + "~";
					}
					
					log.info("Method: buscarGestionPorCuentaPresentaciones - "+listaPresentaciones);
				}else{
					listaPresentaciones="esttrx|1~MSG|No existe informacion con los parametros seleccionados.~";
				}
				
			}else{
				log.error("Problemas al leer las presentaciones desde la base de datos.");
				listaPresentaciones="esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
			}
			
			resultados.close();
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaPresentaciones = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				return listaPresentaciones;
			}
			
		}catch (SQLException e){  
			
				bdOracle.setResultado(resultados);
				bdOracle.cerrarResultSet();
				bdOracle.cerrarStatement(stmtProcedure);
				
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaPresentaciones = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
					return listaPresentaciones;
				}
				
				log.error( e.getMessage(), e );
        		listaPresentaciones = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaPresentaciones;
	}/* Fin buscarGestionPorCuentaPresentaciones*/
	
	public Vector<GestionPresentacionBean> buscarGestionPorCuentaPresentacionesPag(String numeroTarjeta, String fechaInicio, String fechaTermino, String numPagina){

		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		String listaPresentaciones="";
		GestionPresentacionBean gestionBean = new GestionPresentacionBean(); 
		
		try {
			log.info("Method: buscarGestionPorCuentaPresentaciones - Obteniendo parametros: numeroTarjeta-> "+numeroTarjeta+" | fechaInicio-> "+fechaInicio+" | fechaTermino-> "+fechaTermino);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			int pagActual;
			
			if(numPagina == null)
			{
				pagActual = 1;
			}else{
				pagActual = Integer.parseInt(numPagina);
			}
			this.setPagActual(pagActual);
			
			log.info("Method: buscarGestionPorCuenta - Ejecutando SP_SIG_GESTPORCUENTAPRESENTPG");
			log.info("Parametros :  numPagina = " + numPagina );
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_GESTPORCUENTAPRESENTPG(?,  ?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			if(numeroTarjeta == null)
				numeroTarjeta = "";
			
			if (numeroTarjeta.length()==0){
				
				numeroTarjeta="0";
			}
			
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			
			stmtProcedure.setString("fechaInicio", fechaInicio);
			
			stmtProcedure.setString("fechaTermino", fechaTermino);
			
			stmtProcedure.setInt("numPagina", pagActual);
			
			stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
			
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
			
			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
					
				
					
				if (resultados != null){
					
					float totReg = stmtProcedure.getInt("totReg");
					float ultimaPagina;

					ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
					this.setUltimaPagina((int) ultimaPagina);
					
					
					
				    String auxMonto = null;
					while(resultados.next()) {
					auxMonto = "";
					gestionBean = new GestionPresentacionBean();
					
					log.info("Method: buscarGestionPorCuentaPresentaciones - Obteniendo resultados");
					listaPresentaciones="esttrx|0~";
					
					
					
					
					String valor = resultados.getString("COMERCIOPAIS");
					String[] comerciopais;
					String comercio    = "";
					String regionpais  = "";
					String region      = "";
					String provincia   = "";
					String pais        = "";
					if(valor != null){
						if (valor.length() == 36 || valor.length() == 43) {
							comerciopais = valor.split("\\|");
							comercio = comerciopais[0];
							regionpais = comerciopais[1];
							region = comerciopais[1];
							provincia = comerciopais[1];
							pais = comerciopais[2];
						} else {
							comerciopais = valor.split("\\|");
							comercio = comerciopais[0];
							regionpais = comerciopais[3];
							region = regionpais.substring(0, 9);
							provincia = regionpais.substring(10, 13);
							pais = regionpais.substring(13);
						}
					}
					
					gestionBean.setMit(resultados.getString("MIT"));//ok
					gestionBean.setCodigoFuncion(resultados.getString("CODIGOFUNCION"));//ok
					gestionBean.setNumeroTarjeta(resultados.getString("NUMEROTARJETA"));//ok
					gestionBean.setFechaTransac(resultados.getString("FECHATRANSAC"));//ok
					gestionBean.setComercio(comercio);//ok
					gestionBean.setPais(pais);//ok
					gestionBean.setMontoTransac(resultados.getString("MONTOTRANSACCION"));//ok
					gestionBean.setMontoFacturacion(resultados.getString("MONTOFACTURACION"));//ok
					gestionBean.setCodigoRazon(resultados.getString("CODIGORAZON"));//ok
					
					gestionBean.setCodMonedaTrx(resultados.getString("CODMONEDATRX"));
					gestionBean.setGlosaTipo(resultados.getString("GLOSATIPO"));
					gestionBean.setGlosaTipoMoneda(resultados.getString("GLOSATIPOMONEDA"));
					gestionBean.setEstatus(resultados.getString("ESTATUS"));
					
					gestionBean.setSidIncoming(resultados.getInt("SIDINCOMMING"));//ok
					
					gestionBean.setMontoConciliacion(resultados.getString("MONTOCONCILIACION"));//ok
					
					gestionBean.setPatpass(resultados.getString("PATPASS"));
					
					gestionBean.setOperador(resultados.getString("OPERADOR"));
					gestionBean.setMicrofilm(resultados.getString("MICROFILM"));
					gestionBean.setCodigoAutorizacion(resultados.getString("CODIGOAUTORIZACION"));
					gestionBean.setOtrodato1(resultados.getString("OTRODATO1"));
					gestionBean.setFechaEfectiva(resultados.getString("FECHAEFECTIVA"));
					gestionBean.setFechaProceso(resultados.getString("FECHAPROCESO"));
					gestionBean.setOtrodato2(resultados.getString("OTRODATO2"));
					gestionBean.setBinAdquiriente(resultados.getString("BINADQUIRIENTE"));
					gestionBean.setLeeBanda(resultados.getString("LEEBANDA"));
					gestionBean.setOtrodato3(resultados.getString("OTRODATO3"));
					gestionBean.setRubroComercio(resultados.getString("RUBROCOMERCIO"));
					gestionBean.setOtrodato4(resultados.getString("OTRODATO4"));
					gestionBean.setCodMonedaTrx(resultados.getString("GLOSATIPOMONEDA"));
					
					
					 // Formateo de Monto Conciliacion
                    auxMonto = Utils.formateaMonto( gestionBean.getMontoConciliacion());
                    gestionBean.setMontoConciliacion( auxMonto );
                    
                    // formateo de monto Monto Facturacion
                    auxMonto = Utils.formateaMonto( gestionBean.getMontoFacturacion());
                    gestionBean.setMontoFacturacion(auxMonto);
                    
                    // formateo de Monto de Transaccion
                    auxMonto = Utils.formateaMonto( gestionBean.getMontoTransac());
                    gestionBean.setMontoTransac(auxMonto);
					
					
						
					gestionesBeanVector.add(gestionBean);
					
					}
					
					log.info("Method: buscarGestionPorCuentaPresentaciones - "+listaPresentaciones);
				
				}else{
					listaPresentaciones="esttrx|1~MSG|No existe informacion con los parametros seleccionados.~";
				}
				
			}else{
				log.error("Problemas al leer las presentaciones desde la base de datos.");
				listaPresentaciones="esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
			}
			
			resultados.close();
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaPresentaciones = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
				return gestionesBeanVector;
			}
			
		}catch (SQLException e){  
			
				//bdOracle.setResultado(resultados);
				//bdOracle.cerrarResultSet();
				bdOracle.cerrarStatement(stmtProcedure);
				
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaPresentaciones = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
					return gestionesBeanVector;
				}
				
				 log.error( e.getMessage(), e );
        		listaPresentaciones = "esttrx|1~MSG|Problemas al leer las presentaciones desde la base de datos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return gestionesBeanVector;
	}/* Fin buscarGestionPorCuentaPresentaciones*/
	
	
	/**
	 * Metodo que obtiene el historial de movimientos de una transaccion.
	 * @return lista de historial de movimientos de la transaccion.
	 * @throws Mensaje de excepcion controlado.
	 * @param numeroTarjeta Numero de tarjeta.
	 * @param fechaInicio Fecha de inicio.
	 * @param fechaTermino Fecha Termino.
	 * @param sidIncomming sid de la transaccion.
	 */
	public String buscarGestionPorCuenta(String numeroTarjeta, String fechaInicio, String fechaTermino, int sidIncomming){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		String listaGestionPorCuenta="";
		
		try {
			log.info("Method: buscarGestionPorCuenta - Obteniendo parametros: numeroTarjeta-> "+numeroTarjeta+" | fechaInicio-> "+fechaInicio+" | fechaTermino-> "+fechaTermino);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarGestionPorCuenta - Ejecutando SP_SIG_GESTIONPORCUENTAS");
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_GESTIONPORCUENTAS(?, ?, ?, ?, ?, ?, ?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setInt("sidIncomming", sidIncomming);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
					if (resultados.next()){
						log.info("Method: buscarGestionPorCuenta - Obteniendo resultados");
								listaGestionPorCuenta="esttrx|0~";
								listaGestionPorCuenta += resultados.getInt("SIDTRANSACCION") + "|";
								listaGestionPorCuenta += resultados.getString("NUMEROTARJETA") + "|";
								listaGestionPorCuenta += resultados.getString("FECHAINSERCION") + "|";
								
								String valor = resultados.getString("COMERCIOPAIS");
								
								String[] comerciopais;
								String comercio    = "";
								String regionpais  = "";
								String region      = "";
								String provincia   = "";
								String pais        = "";
								if(valor != null){
									if (valor.length() == 36 || valor.length() == 43) {
										comerciopais = valor.split("\\|");
										comercio = comerciopais[0];
										regionpais = comerciopais[1];
										region = comerciopais[1];
										provincia = comerciopais[1];
										pais = comerciopais[2];
									} else {
										comerciopais = valor.split("\\|");
										comercio = comerciopais[0];
										regionpais = comerciopais[3];
										region = regionpais.substring(0, 9);
										provincia = regionpais.substring(10, 13);
										pais = regionpais.substring(13);
									}
								}

								
								listaGestionPorCuenta += comercio + "|";
								listaGestionPorCuenta += pais + "|";
								
								listaGestionPorCuenta += resultados.getString("CODIGOACCION") + "|";
								listaGestionPorCuenta += resultados.getString("XKEY") + "|";
								listaGestionPorCuenta += resultados.getString("DESCRIPCIONACCION") + "|"; 
								listaGestionPorCuenta += resultados.getBigDecimal("MONTOTRANSACCION") + "|"; 
								listaGestionPorCuenta += resultados.getBigDecimal("MONTOFACTURACION") + "|"; 
								listaGestionPorCuenta += resultados.getBigDecimal("MONTOCONCILIACION") + "|"; 
								listaGestionPorCuenta += resultados.getString("CODIGORAZON") + "|";
								listaGestionPorCuenta += resultados.getString("DESCRICPIONCAUSALOBJREC") + "|";
								listaGestionPorCuenta += resultados.getString("DESCRIPCIONCAUSALACCION") + "|";
								listaGestionPorCuenta += resultados.getString("REFERENCIA") + "|";
								listaGestionPorCuenta += resultados.getString("CODMONEDATRX") + "|";
								listaGestionPorCuenta += resultados.getString("ESTADOTRX") + "|";
								listaGestionPorCuenta += resultados.getString("GLOSAESCRITA") + "|";
								listaGestionPorCuenta += resultados.getString("USUARIO") + "|";
								listaGestionPorCuenta += resultados.getString("DATOSADICIONALES5") + "|";
								listaGestionPorCuenta += resultados.getString("PATPASS") + "~";
								
								while (resultados.next()) {
										
									listaGestionPorCuenta += resultados.getInt("SIDTRANSACCION") + "|";
									listaGestionPorCuenta += resultados.getString("NUMEROTARJETA") + "|";
									listaGestionPorCuenta += resultados.getString("FECHAINSERCION") + "|";
									
									valor = resultados.getString("COMERCIOPAIS");
					
									comercio    = "";
									regionpais  = "";
									region      = "";
									provincia   = "";
									pais        = "";
									if(valor != null){
										if (valor.length() == 36 || valor.length() == 43) {
											comerciopais = valor.split("\\|");
											comercio = comerciopais[0];
											regionpais = comerciopais[1];
											region = comerciopais[1];
											provincia = comerciopais[1];
											pais = comerciopais[2];
										} else {
											comerciopais = valor.split("\\|");
											comercio = comerciopais[0];
											regionpais = comerciopais[3];
											region = regionpais.substring(0, 9);
											provincia = regionpais.substring(10, 13);
											pais = regionpais.substring(13);
										}
									}

									
									listaGestionPorCuenta += comercio + "|";
									listaGestionPorCuenta += pais + "|";
									
									listaGestionPorCuenta += resultados.getString("CODIGOACCION") + "|";
									listaGestionPorCuenta += resultados.getString("XKEY") + "|";
									listaGestionPorCuenta += resultados.getString("DESCRIPCIONACCION") + "|"; 
									listaGestionPorCuenta += resultados.getBigDecimal("MONTOTRANSACCION") + "|"; 
									listaGestionPorCuenta += resultados.getBigDecimal("MONTOFACTURACION") + "|"; 
									listaGestionPorCuenta += resultados.getBigDecimal("MONTOCONCILIACION") + "|"; 
									listaGestionPorCuenta += resultados.getString("CODIGORAZON") + "|";
									listaGestionPorCuenta += resultados.getString("DESCRICPIONCAUSALOBJREC") + "|";
									listaGestionPorCuenta += resultados.getString("DESCRIPCIONCAUSALACCION") + "|";
									listaGestionPorCuenta += resultados.getString("REFERENCIA") + "|";
									listaGestionPorCuenta += resultados.getString("CODMONEDATRX") + "|";
									listaGestionPorCuenta += resultados.getString("ESTADOTRX") + "|";
									listaGestionPorCuenta += resultados.getString("GLOSAESCRITA") + "|";
									listaGestionPorCuenta += resultados.getString("USUARIO") + "|";
									listaGestionPorCuenta += resultados.getString("DATOSADICIONALES5") + "|";
									listaGestionPorCuenta += resultados.getString("PATPASS") + "~";
													                    
								}
								log.info("Method: buscarGestionPorCuenta - "+listaGestionPorCuenta);
					}else{
						listaGestionPorCuenta="esttrx|1~MSG|No existe informacion de gestion de transacciones con los parametros seleccionados.~";
					}		
			}else{
				listaGestionPorCuenta = "esttrx|1~MSG|Problemas al leer el historial de gestiones desde la base de datos...~";
			}
			
			resultados.close();
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaGestionPorCuenta =  "esttrx|1~MSG|Problemas al leer el historial de gestiones desde la base de datos...~";
				return listaGestionPorCuenta;
			}

		}catch (SQLException e){
			
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			
			if (bdOracle.cerrarConexionBD()==false){
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaGestionPorCuenta = "esttrx|1~MSG|Problemas al leer el historial de gestiones desde la base de datos...~";
				return listaGestionPorCuenta;
			}
			
			log.error( e.getMessage(), e );
			listaGestionPorCuenta = "esttrx|1~MSG|Problemas al leer el historial de gestiones desde la base de datos...~";
			
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaGestionPorCuenta;
	}/* Fin buscarGestionPorCuenta*/
	
	
	/**
	
	public Vector<GestionPresentacionBean> buscarGestionPorCuentaPg(String numeroTarjeta, String fechaInicio, String fechaTermino, int sidIncomming){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		String listaGestionPorCuenta="";
		GestionPresentacionBean gestionBean = new GestionPresentacionBean(); 
		
		
		try {
			log.info("Method: buscarGestionPorCuenta - Obteniendo parametros: numeroTarjeta-> "+numeroTarjeta+" | fechaInicio-> "+fechaInicio+" | fechaTermino-> "+fechaTermino);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarGestionPorCuenta - Ejecutando SP_SIG_GESTIONPORCUENTASPG");
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_GESTIONPORCUENTASPG(?, ?, ?, ?, ?, ?, ?, ? ,?,?); END;");
			
			if (numeroTarjeta.length()==0){
				numeroTarjeta="0";
			}
			
			
			stmtProcedure.setString("numeroTarjeta", numeroTarjeta); //Numero Tarjeta
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setInt("sidIncomming", sidIncomming);
			stmtProcedure.setInt("numPagina", pagActual);
			stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
			
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));

			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
				while(resultados.next())
				{
					log.info("Method: buscarGestionPorCuenta - Obteniendo resultados");
					listaGestionPorCuenta="esttrx|0~";
					
					
					gestionBean.setSidTransaccion(resultados.getInt("SIDTRANSACCION"));
					gestionBean.setNumeroTarjeta(resultados.getString("NUMEROTARJETA"));
					gestionBean.setFechaInsercion(resultados.getString("FECHAINSERCION"));
					
					String valor = resultados.getString("COMERCIOPAIS");
					String[] comerciopais;
					comerciopais       = valor.split("\\|");
					String comercio    = comerciopais[0];
					String regionpais  = comerciopais[3];
					String region      = regionpais.substring(0, 9);
					String provincia   = regionpais.substring(10, 13);
					String pais        = regionpais.substring(13);
					
					
					
										
					
					
					gestionBean.setCodigoAccion(resultados.getString("CODIGOACCION"));
					gestionBean.setXkey(resultados.getString("XKEY"));
					gestionBean.setDescripcionAccion(resultados.getString("DESCRIPCIONACCION")); 
					gestionBean.setMontoTransac(resultados.getBigDecimal("MONTOTRANSACCION"));
					gestionBean.setMontoFacturacion(resultados.getBigDecimal("MONTOFACTURACION")); 
					gestionBean.setMontoConciliacion(resultados.getBigDecimal("MONTOCONCILIACION")); 
					gestionBean.setCodigoRazon(resultados.getString("CODIGORAZON"));
					gestionBean.setDescripcionCausalObjRec(resultados.getString("DESCRICPIONCAUSALOBJREC"));
					gestionBean.setDescripcionCaudalAccion(resultados.getString("DESCRIPCIONCAUSALACCION"));
					gestionBean.setReferencia(resultados.getString("REFERENCIA"));
					gestionBean.setCodMonedaTrx(resultados.getString("CODMONEDATRX"));
					gestionBean.setEstadoTrx(resultados.getString("ESTADOTRX"));
					gestionBean.setGlosaEscrita(resultados.getString("GLOSAESCRITA"));
					gestionBean.setUsuario(resultados.getString("USUARIO"));
						
					gestionesBeanVector.add(gestionBean);
								
					}
			}else{
				listaGestionPorCuenta = "esttrx|1~MSG|Problemas al leer el historial de gestiones desde la base de datos...~";
			}
			
			resultados.close();
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaGestionPorCuenta =  "esttrx|1~MSG|Problemas al leer el historial de gestiones desde la base de datos...~";
				return gestionesBeanVector;
			}

		}catch (SQLException e){
			
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			
			if (bdOracle.cerrarConexionBD()==false){
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaGestionPorCuenta = "esttrx|1~MSG|Problemas al leer el historial de gestiones desde la base de datos...~";
				return gestionesBeanVector;
			}
			
			log.error(e.getMessage());
			e.printStackTrace();
			listaGestionPorCuenta = "esttrx|1~MSG|Problemas al leer el historial de gestiones desde la base de datos...~";
			
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    	}
		
		return gestionesBeanVector;
	}/* Fin buscarGestionPorCuenta*/
	
	
	
	/**
	 * Metodo que obtiene el historial de los cargos y abonos realizados a la transaccion.
	 * @return lista de cargos y abonos.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaInicio Fecha inicio.
	 * @param fechaTermino Fecha termino.
	 * @param sidIncomming Sid de la transaccion. 
	 */
	public String buscarGestionCuentaCargosAbonos(String fechaInicio, String fechaTermino, int sidIncomming){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;
		String listaGestionCargoAbono="";
		
		try {
			log.info("Method: buscarGestionCuentaCargosAbonos - Obteniendo parametros: fechaInicio-> "+fechaInicio+" | fechaTermino-> "+fechaTermino+" | sidIncomming-> "+sidIncomming);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarGestionCuentaCargosAbonos - Ejecutando SP_SIG_GESTIONPORCUENTASCARABO");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_GESTIONPORCUENTASCARABO(?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaTermino", fechaTermino);
			stmtProcedure.setInt("sidIncomming", sidIncomming);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
					if (resultados.next()){
						log.info("Method: buscarGestionCuentaCargosAbonos - Obteniendo resultados");
							
								listaGestionCargoAbono="esttrx|0~";
								
								listaGestionCargoAbono += resultados.getInt("SIDTRANSACCION") + "|";
								listaGestionCargoAbono += resultados.getString("FECHACARGOABONO") + "|";
								listaGestionCargoAbono += resultados.getString("CODIGOMONEDA") + "|";
								listaGestionCargoAbono += resultados.getBigDecimal("MONTOCARGOABONO") + "|";
								listaGestionCargoAbono += resultados.getInt("SIDCARGOABONO") + "|";
								listaGestionCargoAbono += resultados.getString("TIPOCARGOABONO") + "|";
								listaGestionCargoAbono += resultados.getString("DESCRIPCION") + "|";
								listaGestionCargoAbono += resultados.getString("USUARIO") + "~";
								
								while (resultados.next()) {
									
									listaGestionCargoAbono += resultados.getInt("SIDTRANSACCION") + "|";
									listaGestionCargoAbono += resultados.getString("FECHACARGOABONO") + "|";
									listaGestionCargoAbono += resultados.getString("CODIGOMONEDA") + "|";
									listaGestionCargoAbono += resultados.getBigDecimal("MONTOCARGOABONO") + "|";
									listaGestionCargoAbono += resultados.getInt("SIDCARGOABONO") + "|";
									listaGestionCargoAbono += resultados.getString("TIPOCARGOABONO") + "|";
									listaGestionCargoAbono += resultados.getString("DESCRIPCION") + "|";
									listaGestionCargoAbono += resultados.getString("USUARIO") + "~";
								}
								log.info("Method: buscarGestionCuentaCargosAbonos - "+listaGestionCargoAbono);
					}else{
						listaGestionCargoAbono="esttrx|1~MSG|No existe informacion de gestion de cargos y abonos con los parametros seleccionados.~";
					}		
			}else{
				listaGestionCargoAbono = "esttrx|1~MSG|Problemas al leer el historial de gestiones de cargos abonos desde la base de datos...~";
			}
			
			resultados.close();
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaGestionCargoAbono = "esttrx|1~MSG|Problemas al leer el historial de gestiones cargos abonos desde la base de datos...~";
				return listaGestionCargoAbono;
			}
			
		}catch (SQLException e){  
			
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			
			if (bdOracle.cerrarConexionBD()==false){
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaGestionCargoAbono = "esttrx|1~MSG|Problemas al leer el historial de gestiones cargos abonos desde la base de datos...~";
				return listaGestionCargoAbono;
			}
			
			log.error( e.getMessage(), e );
    		listaGestionCargoAbono = "esttrx|1~MSG|Problemas al leer el historial de gestiones cargos abonos desde la base de datos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaGestionCargoAbono;
	}/* Fin buscarGestionCuentaCargosAbonos*/
	
	/**
	 * Descripcion: Funci�n que cierra el proceso de una transacci�n
	 * @param sid
	 * @return Si la trasaccion fue correcta o erronea
	 * @throws IOException 
	 */
	public String cerrarTransaccion(int sid) throws IOException
	{
		log.info("Method: cerrarTransaccion - Obteniendo parametros: sid-> "+sid);
		
		db.OracleBD bdOracle = new OracleBD();
		Connection conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);
		ResultSet rs = null;
		String cerrarTransacResp;
		
		cerrarTransacResp = "esttrx|0~";
		
		String sql;
		String existe = null;
		
		
		try{
			log.info("Method: cerrarTransaccion - Ejecutando cerrado...");
			sql = "select XKEY from " + esquema + ".TBL_ESTADO_TRANSAC where sid = (select max(ESTADO_TRANSAC) from " + esquema + ".TBL_GESTION_TRANSAC where TRANSACCION = "+sid+")";
			rs = bdOracle.consultar(sql);
			
			log.info(sql);
			
			while(rs.next()){
			    existe = rs.getString(1);
			}
			log.info(rs);
			log.info(existe);
			if(existe.equals("OK"))
			{
				log.info("Method: cerrarTransaccion - La transacci�n ya est� cerrada");
				return cerrarTransacResp = "esttrx|1~La transacci�n ya est� cerrada";
			}
			
			sql = "update "+esquema+".TBL_GESTION_TRANSAC GTR set GTR.FECHA_ACTUALIZACION=SYSDATE  ,GTR.ESTADO_TRANSAC = (select SID from "+esquema+".TBL_ESTADO_TRANSAC where XKEY = 'OK') where GTR.TRANSACCION = "+sid;

			bdOracle.ejecutar(sql);
			
			sql = "update " + esquema + ".TBL_TRANSACCIONES TR set TR.FECHAACTUALIZACION=SYSDATE  ,TR.ESTADO_TRANSAC = (select SID from "+esquema+".TBL_ESTADO_TRANSAC where XKEY = 'OK') where TR.SID ="+sid;
			bdOracle.ejecutar(sql);
			
			log.info("Method: cerrarTransaccion - La transacci�n cerrada!");
			cerrarTransacResp +="Transaccion cerrada.";
			rs.close();
			
		} catch (Exception e) {
			
			bdOracle.setResultado(rs);
			bdOracle.cerrarResultSet();
			
			cerrarTransacResp = "esttrx|1~"+e.getMessage();
			 log.error( e.getMessage(), e );
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
    	}
		}
		
		return cerrarTransacResp;
	}

	public void setUltimaPagina(int ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}

	public int getUltimaPagina() {
		return ultimaPagina;
	}

	public void setPagActual(int pagActual) {
		this.pagActual = pagActual;
	}

	public int getPagActual() {
		return pagActual;
	}
}
