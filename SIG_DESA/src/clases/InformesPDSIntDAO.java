package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

/**
 * Clase DAO que consulta cuadratura de transacciones.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class InformesPDSIntDAO {
	
	static final Logger log =  Logger.getLogger(InformesPDSIntDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private String esquema;
	
	public InformesPDSIntDAO(){
		try {
			Properties propiedades = new Properties();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
			    log.error("Error al leer el archivo de propiedades.");
			    log.error( e.getMessage(), e );
    	}
	}
	/**
	 * Metodo que consulta las cuadraturas de transacciones diarias.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso Fecha de sonulta de infromacion.
	 */
	public Object[] buscarInformePDSIntRangoFechas(String fechaInicio, String fechaFin){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		String listaInformePDS="";
		Object[] o = new Object[2];
		try {
			log.info("Method: buscarInformePDSIntRangoFechas - Obteniendo parametros: fechaInicio-> "+fechaInicio + ", fechaFin-> " +fechaFin);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarInformePDSIntRangoFechas - Ejecutando SP_SIG_INFORMEPDSINTRANGOFECHA");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_INFORMEPDSINTRANGOFECHA(?,?,?,?); END;");
			fechaInicio = fechaInicio + " 00:00:00";
			fechaFin = fechaFin + " 23:59:59";
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaFin", fechaFin);
			
			stmtProcedure.registerOutParameter("listaPDS", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			
			
			stmtProcedure.execute();
				
			
			
			ResultSet rs = (ResultSet) stmtProcedure.getObject("listaPDS");
			
			
			
			
			//ResultSet rs = (ResultSet) stmtProcedure.getObject("listaPDS");
			
			
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			o[0] = rs;
			o[1] = codigoError;
			
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaInformePDS =  "esttrx|1~MSG|Problemas al leer el informe PDS Int CloseBD...~";
				 return o;
			}
			
		}catch (SQLException e){  
			
				bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaInformePDS =  "esttrx|1~MSG|Problemas al leer el informe PDS Int CloseSP...~";
					return o;
				}
		
				log.error( e.getMessage(), e );
				listaInformePDS =  "esttrx|1~MSG|Problemas al leer el informe PDS Internacional...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return o;
	}/* Fin buscarInformePDSrangoFechas*/
	
	private String validaDatoPDS(String s){
		
		String valor = " ";
		
		if(s == null)
			return valor;
		else
			return s;
		
		
	}
	

}
