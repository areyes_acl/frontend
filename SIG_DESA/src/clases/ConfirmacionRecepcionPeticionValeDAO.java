package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

/**
 * Clase DAO que administra las generacion de confirmacion de recepcion de peticion de vales. 
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */

public class ConfirmacionRecepcionPeticionValeDAO {
	
	static final Category log = Logger.getLogger(ConfirmacionRecepcionPeticionValeDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
		private String esquema;
		
		public ConfirmacionRecepcionPeticionValeDAO(){
			try {
				Properties propiedades = new Properties();
				propiedades =  PropertiesConfiguration.getProperty();

			    esquema = propiedades.getProperty("esquema");
			    
			}catch (Exception e){          
				    log.error("Error al leer el archivo de propiedades.");
	        		log.error(e.getMessage(), e);
	    	}
		}
	
		
		/**
		 * Metodo que graba la transaccion de confirmacion de recepcion de vales.
		 * @return Mensaje de exito o error de grabacion.
		 * @throws Mensaje de excepcion controlado.
		 * @param vxkey xkey de la transaccion.
		 * @param sidtransaccion sid de la transaccion.
		 * @param sidusuario del usuario.
		 * Qparam glosadescriptiva glosa descriptiva.
		 */
		public String grabarConfirmacionRecepcionPeticionVale(String vxkey, Integer sidtransaccion, int sidusuario, String glosadescriptiva){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
			
		String msg="";
		
		try {
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: grabarConfirmacionRecepcionPeticionVale - Obteniendo parametros: vxkey-> "+vxkey+" | sidtransaccion-> "+sidtransaccion+" | sidusuario-> "+sidusuario+" | glosadescriptiva->"+glosadescriptiva);
			log.info("Method: grabarConfirmacionRecepcionPeticionVale - Ejecutando SP_SIG_GRABARCONFIRMRECPETVAL(vxkey,sidtransaccion,sidusuario,glosadescriptiva,out,out,out");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_GRABARCONFIRMRECPETVAL(?, ?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("vxkey", vxkey);
			stmtProcedure.setInt("sidtransaccion", sidtransaccion);
			stmtProcedure.setInt("sidusuario", sidusuario); 
			stmtProcedure.setString("glosadescriptiva", glosadescriptiva);
		
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("flag", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int flag = stmtProcedure.getInt("flag");
			
			stmtProcedure.close();
			
			log.info("Method: grabarConfirmacionRecepcionPeticionVale - Cerrando SP_SIG_GRABARCONFIRMRECPETVAL");
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 msg= "esttrx|1~MSG|Problemas al grabar confirmacion de recepcion de vales...~";
				 return msg;
			}

			if (flag==0){
				log.info("Method: grabarConfirmacionRecepcionPeticionVale - La operación ha sido realizada con exito.");
				msg = "esttrx|0~MSG|La operacion ha sido realizada con exito...~";
			}else{
				log.error("Problemas al grabar confirmacion de recepción de vales.");
				msg = "esttrx|1~MSG|Problemas al grabar confirmacion de recepcion de vales...~";
			}

		}
		catch (SQLException e){ 
			
				bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					msg = "esttrx|1~MSG|Problemas al grabar confirmacion de recepcion de vales...~";
					return msg;
				}
			
				log.error(e.getMessage(), e);
				msg="esttrx|1~MSG|Problemas al grabar confirmacion de recepcion de vales...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error(e.getMessage(), e);
            }
    	}
		
		return msg;
	}/* Fin grabarConfirmacionRecepcionPeticionVale*/

}
