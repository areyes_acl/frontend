package clases;

/*
 * Descripcion: Funci�n que convierne un String serializado en una planilla excel preformateada.
 * 
 * @param Serie - |: Representa columna. ~: Representa fila.
 * @throw FileNotFoundException si el documento est� abierto.
 * @return OK si no hay problemas en la exportaci�n
 * @version 1.0
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import properties.PropertiesConfiguration;

public class ExportExcel {
	
	/**
	 * Metodo de creaci�n de excel. Recibe los parametros 'serie' y 'componente' para armar el archivo .xls
	 */
	private String directorioExportacion;
	private String nombreArchivoExport;
	static final Logger log = Logger.getLogger(ExportExcel.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private File fileS;
	public String fileName;
	
	public File crearExcel(String serie, String componente)
	{
		try {			
			log.info("Method: buscarCuadraturaLogProcesoIncoming - Obteniendo parametros: componente-> "+componente);
			
			log.info("--"+componente+"--");
			log.info("--Inicio de exportaci�n--");
			Date ahora = new Date();
	        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
	        
			String fecha = formateador.format(ahora);
			if(componente == null){
				componente = "Objeciones Diarias";
			}
			componente = componente.trim().replaceAll(" ", "_");
			log.info("--"+componente+"--");
			
			Properties propiedades = new Properties();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
			directorioExportacion = propiedades.getProperty("exportarexcel");
			nombreArchivoExport = propiedades.getProperty("nombreArchivoExport");
			
			log.info("Crea carpeta temp - Si no existe, lo crea. " + directorioExportacion + nombreArchivoExport);
			File  dir = new File (directorioExportacion);
			
			if (!dir.exists())
				  if (!dir.mkdir())
				    //return "Error creando caperta";
			
			//Se crea el libro Excel
			log.info("creaci�n de archivo Reporte.xls");
			
			//fileName= "c:\\Reportes_SIG\\Reporte.xls";
			//fileS = new File(fileName);
			fileS = new File(directorioExportacion+nombreArchivoExport);
			
			FileOutputStream fileOut = new FileOutputStream(fileS);
			
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet worksheet = workbook.createSheet("POI Worksheet");
			
			String[] filas = serie.split("~");
			
			log.info("Deserealizaci�n parametro 'serie'");
			log.info("Cantidad de filas a imprimir: "+filas.length);
			
			for(int i=0;i<filas.length;i++)
			{
				HSSFRow row = worksheet.createRow((short) i);
								
				String[] columna = filas[i].split("\\|");
				
				for(int x=0;x<columna.length;x++)
				{
					worksheet.autoSizeColumn(x, true);
					
					HSSFCell cell = row.createCell(x);
					HSSFCellStyle cellStyle = workbook.createCellStyle();
					HSSFFont font = workbook.createFont();
					
					cell.setCellValue(columna[x]);
					if(i==0)
					{	
						cellStyle.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
						cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
						//cellStyle.setFillPattern(HSSFCellStyle.ALIGN_CENTER);
						
						cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
						
						cellStyle.setBorderBottom((short)1);
						cellStyle.setBorderTop((short)1);
						cellStyle.setBorderLeft((short)1);
						cellStyle.setBorderRight((short)1);
						
						font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
						cellStyle.setFont(font);
					}
					
					if(NumberUtils.isNumber(columna[x]))
					{
						cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
					}
					
					cell.setCellStyle(cellStyle);
				}
			}
			
			workbook.write(fileOut);
			fileOut.flush();
			fileOut.close();
			
			log.info("--Fin de exportaci�n--");
			
		} catch (FileNotFoundException e) {
		    log.error( e.getMessage(), e );
			//return "El archivo est� siendo utilizado, favor de cerrar.";
		} catch (IOException e) {
		    log.error( e.getMessage(), e );
			//return "Error: "+e.getMessage();
		} catch (Exception e) {
		    log.error( e.getMessage(), e );
			//return "Error: "+e.getMessage();
		}
		
		return fileS;
	}
}
