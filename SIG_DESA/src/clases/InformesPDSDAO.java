package clases;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import cl.util.Utils;
import properties.PropertiesConfiguration;
import au.com.bytecode.opencsv.CSVWriter;
import db.OracleBD;

/**
 * Clase DAO que consulta cuadratura de transacciones.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class InformesPDSDAO {
	
	static final Logger log =  Logger.getLogger(InformesPDSDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private String esquema;
	private String ListaOperadores;
	
	public InformesPDSDAO(){
		try {
			Properties propiedades = new Properties();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
			    log.error("Error al leer el archivo de propiedades.");
			    log.error(e.getMessage(),e);
    	}
	}
	/**
	 * Metodo que consulta las cuadraturas de transacciones diarias.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso Fecha de sonulta de infromacion.
	 */
	public Object[] buscarInformePDSrangoFechas(String fechaInicio, String fechaFin){

		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		String listaInformePDS="";
		ResultSet rs = null;
		
		Object[] o = new Object[2];
		
		try {
			log.info("Method: buscarInformePDSrangoFechas - Obteniendo parametros: fechaInicio-> "+fechaInicio + ", fechaFin-> " +fechaFin);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarInformePDSrangoFechas - Ejecutando SP_SIG_INFORMEPDSRANGOFECHA");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_INFORMEPDSRANGOFECHA(?,?,?,?); END;");
			fechaInicio = fechaInicio + " 00:00:00";
			fechaFin = fechaFin + " 23:59:59";
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaFin", fechaFin);
			
			stmtProcedure.registerOutParameter("listaPDS", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			
			
			stmtProcedure.execute();
				
			rs = ( ResultSet ) ( ( stmtProcedure )
                    .getObject( "listaPDS" ) );
			
			
					
			int codigoError = stmtProcedure.getInt("cod_error");
			
			o[0] = rs;
			o[1] = codigoError;
			
			log.info("Method: buscarInformePDSrangoFechas - "+listaInformePDS);
			
			
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaInformePDS =  "esttrx|1~MSG|Problemas al leer el informe PDS CloseBD...~";
				 
				 return o;
			}
			
		}catch (SQLException e){  
			
				bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaInformePDS =  "esttrx|1~MSG|Problemas al leer el informe PDS CloseSP...~";
					return o;
				}
		
				log.error(e.getMessage(),e);
				listaInformePDS =  "esttrx|1~MSG|Problemas al leer el informe PDS...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error(e.getMessage(),e);
            }
    	}
		
		return o;
	}/* Fin buscarInformePDSrangoFechas*/
	/**
	 * Metodo que consulta las cuadraturas de transacciones diarias.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso Fecha de sonulta de infromacion.
	 */
	public Object[] buscarInformePDSrangoFechas(String fechaInicio, String fechaFin,String archivoPrueba, String operador){

		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		String listaInformePDS="";
		ResultSet rs = null;
		
		Object[] o = new Object[2];
		
		try {
			log.info("Method: buscarInformePDSrangoFechas - Obteniendo parametros: fechaInicio-> "+fechaInicio + ", fechaFin-> " +fechaFin);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarInformePDSrangoFechas - Ejecutando SP_SIG_INFORMEPDSRANGOFECHA");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_INFORMEPDSRANGOFECHA(?,?,?,?,?); END;");
//			fechaInicio = fechaInicio + " 00:00:00";
//			fechaFin = fechaFin + " 23:59:59";
			log.info("fechaInicio = "+ fechaInicio);
			log.info("fechaFin = "+ fechaFin);
			stmtProcedure.setString("fechaInicio", fechaInicio);
			stmtProcedure.setString("fechaFin", fechaFin);
			stmtProcedure.setString("FILTRO_OPERADOR", operador);
			
			stmtProcedure.registerOutParameter("listaPDS", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			
			
			stmtProcedure.execute();
				
			rs = ( ResultSet ) ( ( stmtProcedure )
                    .getObject( "listaPDS" ) );
			
			
					
			int codigoError = stmtProcedure.getInt("cod_error");
			
			o[0] = rs;
			o[1] = codigoError;
			
			log.info("Method: buscarInformePDSrangoFechas - "+listaInformePDS);
			
			// Forma antigua de realizar al exportacion a excel
//			CSVWriter writer;
//			try {
//				writer = new CSVWriter(new FileWriter(archivoPrueba), ';');
//				writer.writeAll(rs,true);
//				writer.close();
//			} catch (Exception e) {
//				log.error(e.getMessage(),e);
//				e.printStackTrace();
//			}
			
			// Nueva forma de realizar la exportaci�n a excel
			exportarExcel( rs, archivoPrueba );
			
			
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaInformePDS =  "esttrx|1~MSG|Problemas al leer el informe PDS CloseBD...~";
				 
				 return o;
			}
			
		}catch (SQLException e){  
			
				bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaInformePDS =  "esttrx|1~MSG|Problemas al leer el informe PDS CloseSP...~";
					return o;
				}
		
				log.error(e.getMessage(),e);
				listaInformePDS =  "esttrx|1~MSG|Problemas al leer el informe PDS...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error(e.getMessage(),e);
            }
    	}
		
		return o;
	}/* Fin buscarInformePDSrangoFechas*/
	
	private String validaDatoPDS(String s){
		
		String valor = " ";
		
		if(s == null)
			return valor;
		else
			return s;
		
		
	}
	

	
	 /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param rs2
     * @since 1.X
     */
    private void exportarExcel( ResultSet rs, String ruta ) {
        List<String[]> dataList = new ArrayList<String[]>();
        CSVWriter writer = null;
        
        try {
            // GENERA CABECERA
            String[] header = generateHeaderToExcel();
            
            // PARSEO DE DATOS DEL EXCEL
            while ( rs.next() ) {
                dataList.add( getRegistrosToExcel( rs ) );
            }
            
            writer = new CSVWriter( new FileWriter( ruta ), ';' );
            writer.writeNext( header );
            writer.writeAll( dataList );
            
        }
        catch ( SQLException e ) {
            log.error( e.getMessage(), e );
        }
        catch ( IOException e ) {
            log.error( e.getMessage(), e );
        }
        finally {
            if ( writer != null ) {
                try {
                    writer.close();
                }
                catch ( IOException e ) {
                    log.error( e.getMessage(), e );
                }
            }
        }
    }
    
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private String[] generateHeaderToExcel() {
       return new String[]{
              "FECHA_TRX",
              "FECHA_COMP",
              "MONTO_TRANSAC",
              "MONEDA_TRANSAC",
              "MONTO_CONCILIACION",
              "MONEDA_CONCILIACION",
              "MONTO_FACTURACION",
              "MONEDA_FACTURACION",
              "NRO_TARJETA",
              "COD_AUTORIZACION",
              "COD_COMERCIO",
              "NOM_COMERCIO",
              "TIPO_TERMINAL",
              "ACT_COMERCIAL",
              "IND_LIQUIDACION",
              
              
              "COM_COMERCIO",
              "NRO_CUOTAS",
              "CUOTA_ACTUAL",
              "RUBRO",
              "VAL_CUOTA_COMERCIO",
              "TASA_CUOTA_COMERCIO",
              "COM_ADIC",
              "CRED_DEB",
              "FECHA_PROC",
              "TIP_TR",
              "TIPO_VENTA",
              "MONTO_PAGO",
              "PATPASS"
              
       };
 
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) -  versi�n inicial 
     * </ul>
     * <p>
     * 
     * @param rs
     * @return
     * @since 1.X
     */
    private String[] getRegistrosToExcel( ResultSet rs ) {
        try {
            String registro = "";
            registro += rs.getString( "FECHA_TRX") + ";";
            registro += rs.getString( "FECHA_COMP" )+ ";";
            // SE REALIZA CONVERSION YA QUE EL SP LE QUITA LOS DECIMALES 
            
            Integer val = Integer.valueOf( rs.getString( "MONTO_TRX" ));
            registro += Utils.formateaMonto( String.valueOf( val*100))+";";
            
            registro += rs.getString("Cod_Moneda_Transac")+ ";";
            
            BigInteger val2 = BigInteger.valueOf(Integer.valueOf(rs.getString("Monto_Conciliacion")));
            BigInteger vl = new BigInteger("100");
            
            registro += Utils.formateaMonto( String.valueOf( val2.multiply(vl)))+";";
            registro += rs.getString("Cod_Moneda_Conciliacion")+ ";";
            
            BigInteger val3 = BigInteger.valueOf(Integer.valueOf(rs.getString("Monto_Facturacion")));
            registro += Utils.formateaMonto( String.valueOf( val3.multiply(vl)))+";";
            
            registro += rs.getString("Cod_Moneda_Facturacion")+ ";";
            registro += rs.getString( "NRO_TARJETA" )+ ";";
            registro += rs.getString( "COD_AUTORIZACION" )+ ";";
            registro += rs.getString( "COD_COMERCIO" )+ ";";
            registro += rs.getString( "NOM_COMERCIO" )+ ";";
            registro += rs.getString( "TIPO_TERMINAL" )+ ";";
            registro += rs.getString( "ACT_COMERCIAL" )+ ";";
            registro += rs.getString( "IND_LIQUIDACION" )+ ";";
            registro += rs.getString( "COM_COMERCIO" )+ ";";
            registro += rs.getString( "NRO_CUOTAS" )+ ";";
            registro += rs.getString( "CUOTA_ACTUAL" ) + ";";
            registro += rs.getString( "RUBRO" )+ ";";
            registro += rs.getString( "VAL_CUOTA_COMERCIO" )+ ";";
            registro += rs.getString( "TASA_CUOTA_COMERCIO" )+ ";";
            registro += rs.getString( "COM_ADIC" )+ ";";
            registro += rs.getString( "CRE_DEB" )+ ";";
            registro += rs.getString( "FECHA_PROC" )+ ";";
            registro += rs.getString( "TIP_TR" )+ ";";
            registro += rs.getString( "TIPO_VENTA" )+ ";";
            BigInteger val4 = BigInteger.valueOf(Integer.valueOf(rs.getString("MONTO_PAGO_TBK")));
            registro += Utils.formateaMonto( String.valueOf( val4.multiply(vl)))+";";
            
            if (rs.getString( "PATPASS" ).equals(" ") || rs.getString( "PATPASS" ).equals("") || rs.getString( "PATPASS" ).equals("-") || rs.getString( "PATPASS" ) == null || rs.getString("OPERADOR").equals("VN") || rs.getString("OPERADOR").equals("VI")) {
        	registro += "No"+ ";";
	    }else{
		registro += "Si"+ ";";
	    }
            return registro.split( ";" );
        }
        catch ( SQLException e ) {
          log.error( e.getMessage(), e );
        }
        return null;
    }
    
    public String getOperadores() {
	log.info("Method: getOperadores - Obtencion de parametros: ninguno");

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs = null;

	bdOracle = new OracleBD();
	conexion = bdOracle.conectar();
	bdOracle.setConexion(conexion);

	log.info("Method: getOperadores - Obteniendo operadores");
	ListaOperadores = "esttrx:0~";
	rs = bdOracle.consultar("OPERADOR FROM " + esquema
		+ ".TBL_OPERADOR WHERE ACTIVO = 1 ORDER BY SID ");
	bdOracle.setResultado(rs);

	try {

	    if (rs.next()) {
		log.info("Method: getOperadores - Obteniendo resulstados");
		rs.previous();
		while (rs.next()) {
		    ListaOperadores += rs.getInt("SID") + "|";
		 //   ListaOperadores += rs.getString("OPERADOR") + "~";
		    ListaOperadores += rs.getString("NOMBRE") + ("~");
		}
		log.info(ListaOperadores);
		rs.close();
		log.info("Method: getOperadores - " + ListaOperadores);
	    } else {
		ListaOperadores = "esttrx:1~No hay datos";
	    }
	    log.info("Method: getOperadores - Serializando lista");
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    bdOracle.cerrarConexionBD();
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return ListaOperadores;
    }
    

}
