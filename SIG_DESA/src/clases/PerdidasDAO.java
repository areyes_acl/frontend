package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.VentaType;
import Beans.PerdidaBean;
import Beans.PerdidaDTO;
import cl.filter.FiltroPerdida;
import cl.util.Utils;
import db.OracleBD;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * @author afreire
 * 
 *         Clase que gestiona la interacci�n con la base de datos para el modulo
 *         de p�rdida
 * 
 *         <p>
 *         <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PerdidasDAO {

    private static final Logger log = Logger.getLogger(PerdidasDAO.class);
    private String esquema;
    private final String ORACLE_EXCEPTION = "ORA-00001";
    
    // PAGINACION
    private int ultimaPagina = 0;
    private int pagActual;
    private String cantidadRegistros="";
    
    
    
    // ATRIBUTOS PARA PAGINACION
    public int getUltimaPagina() {
        return ultimaPagina;
    }

    public int getPagActual() {
        return pagActual;
    }

    

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Constructor de la clase
     * 
     * @since 1.X
     */
	public PerdidasDAO() {
		try {
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();
			esquema = propiedades.getProperty("esquema");
			cantidadRegistros = propiedades.getProperty("cantidadRegistros");
		} catch (Exception e) {
			log.error("Error al leer el archivo de propiedades.");
			log.error(e.getMessage(), e);
		}
	}

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String obtenerTipoPerdida() {

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	CallableStatement stmtProcedure = null;
	ResultSet resultados = null;

	String listaTipoGestion = "";
	bdOracle = new OracleBD();
	conexion = bdOracle.conectar();
	bdOracle.setConexion(conexion);

	try {
	    log.info("***** SE EJECUTA SP_SIG_OBTTIPO_OTRGEST(?, ?, ?, ?)");
	    
	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_OBTTIPO_OTRGEST(?, ?, ?, ?); END;");

	    stmtProcedure.setString("tipoGestion", "P");	
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
	    int codigoError = stmtProcedure.getInt("cod_error");

	    if (codigoError == 0) {

		log.info("Method: buscarReporteVisa - Recorriendo resultados");
		while (resultados.next()) {
		    listaTipoGestion += resultados.getString("SID") + "|";
		    listaTipoGestion += resultados.getString("DESCRIPCION")
			    + "~";
		}

	    } else {
		log.error("Method: obtenerTipoGestion - Problemas al leer reporte visa desde la base de datos.");
		listaTipoGestion = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
	    }

	    log.info("Method: buscarReporteVisa - " + listaTipoGestion);
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    listaTipoGestion = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
	} finally {
	    cerrarConexiones(conexion, bdOracle,stmtProcedure,resultados);
	}

	return listaTipoGestion;
    }

    
    

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * 
     * @param otraGestionBean
     * @return
     * @throws Exception 
     * @throws  
     * @since 1.X
     */
    public void guardarPerdida(PerdidaDTO perdida) throws Exception {

	CallableStatement stmtProcedure = null;
	OracleBD bdOracle = new OracleBD();
	Connection conexion = bdOracle.conectar();

	try {

	    bdOracle.setConexion(conexion);

	    log.info("*** SE EJECUTA SP_SIG_REGISTRAR_PERDIDA(?, ?, ?, ?, ?, ?, ?) ***");

	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_REGISTRAR_PERDIDA(?, ?, ?, ?, ?, ?, ?); END;");
	    stmtProcedure.setLong("p_sid_Incoming", perdida.getSidIncoming());
	    stmtProcedure.setLong("p_sid_TipoTransac", perdida.getSidTipoGestion());
	    stmtProcedure.setLong("p_sid_Usuario", perdida.getSidUsuario());
	    stmtProcedure.setString("p_glosa", perdida.getGlosa());
	    stmtProcedure.setString("p_numeroIncidente",perdida.getNumeroIncidente());
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);

	    stmtProcedure.execute();
	    
	    int codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");

	    log.info("CODIGO DE ERROR  : " + codigoError);
	    if (codigoError == 0) {
		return;
	    }else{
		if( warning != null && warning.contains(ORACLE_EXCEPTION)){
		    throw new Exception("Error: se ha intentado ingresar una p�rdida duplicada para una transacci�n.");
		}else{
		    throw new Exception("Ha ocurrido un error al ingresar la p�rdida.");
		}
	    }
	} finally {
	    cerrarConexiones(conexion, bdOracle,stmtProcedure,null);
	}
    }
    
    

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Obtiene una lista de perdidas
     * 
     * 
     * @param Perdida
     * @return
     * @throws Exception 
     * @throws  
     * @since 1.X
     */
    public List<PerdidaBean> buscarPerdidasPorPagina(FiltroPerdida filtro, String numPagina) throws Exception {

   	CallableStatement stmtProcedure = null;
   	ResultSet rs = null;
   	OracleBD bdOracle = new OracleBD();
   	Connection conexion = bdOracle.conectar();
   	List<PerdidaBean> listaPerdidas = null;

   	try {
   	    
	    // LOGICA PAGINACION
	    if (numPagina == null) {
		pagActual = 1;
	    } else {
		pagActual = Integer.parseInt(numPagina);
	    }
		
   	    bdOracle.setConexion(conexion);
   	    log.info("*** SE EJECUTA SP_SIG_BUSCAR_PERDIDAS(?, ?, ?, ?, ?, ?, ?, ? , ?, ?) ***");
   	    
   	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_BUSCAR_PERDIDAS(?, ?, ?, ?, ?, ?, ?, ? ,? , ?); END;");
   	    
   	    stmtProcedure.setString("p_numeroTarjeta", filtro.getNumeroTarjeta());
   	    stmtProcedure.setString("p_fechaInicio", filtro.getFechaInicio());
   	    stmtProcedure.setString("p_fechaTermino", filtro.getFechaTermino());
   	    stmtProcedure.setInt("p_tipo_busqueda", filtro.getTipoBusqueda().getValor());
   	    stmtProcedure.setInt("numPagina", pagActual);
   	    stmtProcedure.setInt("cantReg", Integer.parseInt(cantidadRegistros));
   	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
   	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
   	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
   	    stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);

   	    stmtProcedure.execute();
   	    
   	    rs= (ResultSet) ((stmtProcedure).getObject("prfCursor"));
   	    int codigoError = stmtProcedure.getInt("cod_error");
   	    String warning = stmtProcedure.getString("warning");
   	    
   	    
   	    if(codigoError == 0 ){
   		
		// PAGINACION
		switch (filtro.getTipoBusqueda()) {
		case CON_PAGINACION:
		    float totReg = stmtProcedure.getInt("totReg");
		    ultimaPagina = Utils.calcularUltimaPagina(totReg,
			    Integer.parseInt(cantidadRegistros));
		    break;

		case SIN_PAGINACION:
		    // do nothing
		    break;
		}
   		
   		listaPerdidas = parsearListaPerdida(rs);
   	    }else{
   		throw new Exception(warning);
   	    }
   	    
   	    
	} finally {
   	    cerrarConexiones(conexion, bdOracle, stmtProcedure, rs);
   	}
	return listaPerdidas;
       }

    
    

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     *  Cerrar conexiones 
     * 
     * @param conexion
     * @param bdOracle
     * @since 1.X
     */
	private void cerrarConexiones(Connection conexion, OracleBD bdOracle,
			CallableStatement statement, ResultSet rs) {
		
		if (bdOracle.cerrarResultSet() == false) {
			log.error("Error: No se pudo cerrar resulset.");
			if (bdOracle.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}
		} else {
			if (bdOracle.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}
		}
		try {
			if (conexion != null && !conexion.isClosed()) {
				conexion.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}
		
		try{
			if(statement != null && !statement.isClosed()){
				statement.close();
			}
		}catch (Exception e) {
				log.error(e.getMessage(),e);
		}
		
		try {
			if(rs!= null && !rs.isClosed()){
				rs.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(),e);
		}
	}
    
    
   
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     * 
     * @param rs
     * @return
     * @throws SQLException 
     * @since 1.X
     */
    private List<PerdidaBean> parsearListaPerdida(ResultSet rs) throws SQLException {
	List<PerdidaBean> listaPerdidas = new ArrayList<PerdidaBean>();
	
	while (rs.next()) {
	    PerdidaBean perdida = new PerdidaBean();
	    perdida.setFechaRegistro(rs.getString("FECHA_REGISTRO"));
	    perdida.setMonto(rs.getString("MONTO"));
	    perdida.setNumeroIncidente(rs.getString("NUM_INCIDENTE"));
	    perdida.setMotivo(rs.getString("MOTIVO"));
	    perdida.setUsuario(rs.getString("USUARIO"));
	    perdida.setNumeroTarjeta(rs.getString("NUM_TARJETA"));
	    perdida.setFechaTransaccion(rs.getString("FECHA_TRANSACCION"));
	    perdida.setComercio(rs.getString("COMERCIO"));
	    perdida.setTipoVenta(VentaType.getVentaTypefromCode(rs.getInt("TIPO_VENTA")));
	    perdida.getLog().setEstadoContable(rs.getString("ESTADO_CONTABLE"));
	    perdida.getLog().setFechaContable(rs.getString("FECHA_CONTABLE"));	
	    listaPerdidas.add(perdida);
	    
	}
	return listaPerdidas;
    }

  
}
