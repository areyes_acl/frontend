package clases;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.ObjecionRechazoDiaBean;
import au.com.bytecode.opencsv.CSVWriter;
import cl.util.Utils;
import db.OracleBD;

public class ObjecionRechazoDiaDAO {
	static final Logger log =  Logger.getLogger(ObjecionRechazoDiaDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private OracleBD obd = new OracleBD();
	private Properties prop = new Properties();
	
	private String esquema;
	
	private Connection conexion;
	
	private ResultSet rs;
	
	
	private String listaTransaccion;
	
	private String fechaProceso = "";
	
	private String[] fechaGregoriana = null;
	
	private String fechaProcesoFinal="";
	private String cantidadRegistros="";
	
	private int ultimaPagina = 0;
	
	private int pagActual;
	
	//Alberto Contreras
	Vector<ObjecionRechazoDiaBean> objecionRechazoDiaVector = new Vector<ObjecionRechazoDiaBean>(); 
	
	public ObjecionRechazoDiaDAO()
	{
		setProp();
		this.esquema = prop.getProperty("esquema");
		this.cantidadRegistros = prop.getProperty("cantidadRegistros");
		
		
		this.listaTransaccion = "";
	}
	
	private void setProp()
	{
		//			this.prop.load(getClass().getResourceAsStream("/properties/conexion.properties"));
        this.prop = PropertiesConfiguration.getProperty();
	}
	
	public void setConexion()
	{
		this.conexion = obd.conectar();
	}
	
	public Connection getConexion()
	{
		return conexion;
	}
	
	public Object[] listaRechazoObjecion(String fechaInicio, String fechaTermino, String tipoObjecionRechazo)
	{
		log.info("Method: listaRechazoObjecion - Obtencion de parametros: fechaInicio-> "+fechaInicio+" | fechaTermino-> "+fechaTermino+" | tipoObjecionRechazo-> "+tipoObjecionRechazo);
		setConexion();
		obd.setConexion(conexion);
		
		Object[] o = new Object[2];
		
		CallableStatement stmtProcedure = null;
		
		try{
			log.info("Method: listaRechazoObjecion - Ejecutando SP_SIG_TRANSACOBJRECGESTTRAN");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACOBJRECGESTTRAN(?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaInicio",fechaInicio);
			stmtProcedure.setString("fechaTermino",fechaTermino);
			stmtProcedure.setString("xkeyAtr",tipoObjecionRechazo);
			
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			int error = stmtProcedure.getInt("cod_error");
			
			rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			o[0] = rs;
			o[1] = error;
			
			
		}catch(Exception e)
		{
		    log.error( e.getMessage(),e );
     		obd.cerrarCallableStatement(stmtProcedure);
     		return o;
		}finally{
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
    	}
		
		
		
		return o;
	}

	
	public Vector<ObjecionRechazoDiaBean> listaRechazoObjecion(String fechaInicio, String fechaTermino, String tipoObjecionRechazo, String numPagina)
	{
		log.info("Method: listaRechazoObjecion - Obtencion de parametros: fechaInicio-> "+fechaInicio+" | fechaTermino-> "+fechaTermino+" | tipoObjecionRechazo-> "+tipoObjecionRechazo);
		setConexion();
		obd.setConexion(conexion);
		
		ObjecionRechazoDiaBean objecionRechazoDia = new ObjecionRechazoDiaBean();
		
		CallableStatement stmtProcedure = null;
		
		try{
			log.info("Method: listaRechazoObjecion - Ejecutando SP_SIG_TRANSACOBJRECGESTTRANPG");
			/*stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACOBJRECGESTTRAN(?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaInicio",fechaInicio);
			stmtProcedure.setString("fechaTermino",fechaTermino);
			stmtProcedure.setString("xkeyAtr",tipoObjecionRechazo);
			*/
			int pagActual;
			
			if(numPagina == null)
			{
				pagActual = 1;
			}else{
				pagActual = Integer.parseInt(numPagina);
			}
			
			this.setPagActual(pagActual);
			
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACOBJRECGESTTRANPG(?, ?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaInicio",fechaInicio);
			stmtProcedure.setString("fechaTermino",fechaTermino);
			stmtProcedure.setString("xkeyAtr",tipoObjecionRechazo);
			stmtProcedure.setInt("numPagina",pagActual);
			stmtProcedure.setInt("cantReg",Integer.parseInt(cantidadRegistros));
			
			
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int error = stmtProcedure.getInt("cod_error");
			
			if(error != 100)
			{
				log.info("Method: listaRechazoObjecion - Obteniendo resultados");
				//rs = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
				
				int totReg = stmtProcedure.getInt("totReg");
				int ultimaPagina;

				ultimaPagina = Utils.calcularUltimaPagina(totReg, Integer.parseInt(cantidadRegistros));
				this.setUltimaPagina(ultimaPagina);
				
				rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
				
				listaTransaccion="esttrx:0~";
				
				
				// Alberto Contreras
				String montoAux;
				while(rs.next())
				{
				    montoAux ="";
					objecionRechazoDia = new ObjecionRechazoDiaBean();  
					
					String valor = rs.getString("COMERCIOPAIS");
					
					
					String comercio = "";
					String regionpais     = "";
					String region = "";
					String provincia   = "";
					String pais        = "";
					String[] comerciopais;
					
					if(valor != null){
						if (valor.length() == 36 || valor.length() == 43) {
							comerciopais = valor.split("\\|");
							comercio = comerciopais[0];
							regionpais = comerciopais[1];
							region = comerciopais[1];
							provincia = comerciopais[1];
							pais = comerciopais[2];
						} else {
							comerciopais = valor.split("\\|");
							comercio = comerciopais[0];
							regionpais = comerciopais[3];
							region = regionpais.substring(0, 9);
							provincia = regionpais.substring(10, 13);
							pais = regionpais.substring(13);
						}
					}
					
					
					
					//listaTransaccion += rs.getString("MIT")+"|";
					objecionRechazoDia.setMit(rs.getString("MIT"));
					
					//listaTransaccion += rs.getString("CODIGO_FUNCION")+"|";
					objecionRechazoDia.setCodigoFuncion(rs.getString("CODIGO_FUNCION"));
					
					//listaTransaccion += rs.getString("NRO_TARJETA")+"|";
					objecionRechazoDia.setNumeroTarjeta(rs.getString("NRO_TARJETA"));
					
					//listaTransaccion += rs.getString("FECHATRANSAC")+"|";
					objecionRechazoDia.setFechaTransac(rs.getString("FECHATRANSAC"));
					
					//listaTransaccion += comercio+"|";
					objecionRechazoDia.setComercio(comercio);
					
					//listaTransaccion += pais+"|";
					objecionRechazoDia.setPais(pais);
					
					//listaTransaccion += rs.getString("MONTO_TRANSAC")+"|";
					objecionRechazoDia.setMontoTransac(rs.getString("MONTO_TRANSAC"));
					
					//listaTransaccion += rs.getString("MONTO_FACTURACION")+"|";
					objecionRechazoDia.setMontoFacturacion(rs.getString("MONTO_FACTURACION"));
					
					//listaTransaccion += rs.getString("CODRAZON")+"|";
					objecionRechazoDia.setCodRazon(rs.getString("CODRAZON"));
					
					//listaTransaccion += rs.getString("MICROFILM")+"|";
					objecionRechazoDia.setMicroFilm(rs.getString("MICROFILM"));
					
					//listaTransaccion += rs.getString("CODIGO_AUTORIZACION")+"|";
					objecionRechazoDia.setCodigoAutorizacion(rs.getString("CODIGO_AUTORIZACION"));
					
					//listaTransaccion += rs.getString("FECHAEFECTIVA")+"|";
					objecionRechazoDia.setFechaEfectiva(rs.getString("FECHAEFECTIVA"));
					
					fechaProceso = rs.getString("FECHAPROCESO");
					fechaGregoriana = this.fromJulian(fechaProceso);
					fechaProcesoFinal =  fechaGregoriana[2]+"/" + fechaGregoriana[1] + "/" +fechaGregoriana[0];
					//listaTransaccion += fechaProcesoFinal   + "|";
					objecionRechazoDia.setFechaProceso(fechaProcesoFinal);
					
					
					//listaTransaccion += rs.getString("BINADQUIRENTE")+"|";
					objecionRechazoDia.setBinAdquiriente(rs.getString("BINADQUIRENTE"));
					
					//listaTransaccion += rs.getString("LEEBANDA")+"|";
					objecionRechazoDia.setLeeBanda(rs.getString("LEEBANDA"));
					
					//listaTransaccion += rs.getString("ESTADOTRX")+"|";
					objecionRechazoDia.setEstadoTrx(rs.getString("ESTADOTRX"));
					
					//listaTransaccion += rs.getString("OTRODATO1")+"|";
					objecionRechazoDia.setOtrosDatos1(rs.getString("OTRODATO1"));
					
					//listaTransaccion += rs.getString("OTRODATO2")+"|";
					objecionRechazoDia.setOtrosDatos2(rs.getString("OTRODATO2"));
					
					//listaTransaccion += rs.getString("OTRODATO3")+"|";
					objecionRechazoDia.setOtrosDatos3(rs.getString("OTRODATO3"));
					
					//listaTransaccion += rs.getString("OTRODATO4")+"|";
					objecionRechazoDia.setOtrosDatos4(rs.getString("OTRODATO4"));
					
					//listaTransaccion += rs.getString("CODMONEDATRX")+"|";
					objecionRechazoDia.setCodMonedaTrx(rs.getString("CODMONEDATRX"));
					
					//listaTransaccion += rs.getString("RUBROCOMERCIO")+"|";
					objecionRechazoDia.setRubroComercio(rs.getString("RUBROCOMERCIO"));
					
					//listaTransaccion += rs.getString("SID")+"|";
					objecionRechazoDia.setSid(rs.getString("SID"));
					
					//listaTransaccion += rs.getString("GLOSAGENERAL")+"|";
					objecionRechazoDia.setGlosaGeneral(rs.getString("GLOSAGENERAL"));
					
					//listaTransaccion += rs.getString("REFERENCIA")+"|";
					objecionRechazoDia.setReferencia(rs.getString("REFERENCIA"));
					
					//listaTransaccion += rs.getString("MONTOCONCILIACION")+"~";
					objecionRechazoDia.setMontoConciliacion(rs.getString("MONTO_CONCILIACION"));
					
					objecionRechazoDia.setPatpass(rs.getString("PATPASS"));
					
					objecionRechazoDia.setOperador(rs.getString("OPERADOR"));
					
					// Formateo de monto Conciliacion
	                montoAux = Utils.formateaMonto( objecionRechazoDia.getMontoConciliacion());
	                objecionRechazoDia.setMontoConciliacion(montoAux);
	                
	                // Formateo de monto Facturacion
	                montoAux =  Utils.formateaMonto(objecionRechazoDia.getMontoFacturacion());
	                objecionRechazoDia.setMontoFacturacion(montoAux);
	                
	                // Formateo de monto MontoTransac
	                montoAux =  Utils.formateaMonto(objecionRechazoDia.getMontoTransac());
	                objecionRechazoDia.setMontoTransac(montoAux);
					
					
					objecionRechazoDiaVector.add(objecionRechazoDia);
					
					
					
				}
				rs.close();
				stmtProcedure.close();
			}else{
				log.info("Method: listaRechazoObjecion - Lista vac�a");
				//listaTransaccion="esttrx:1~Lista vac�a";
			}
			
		}catch(Exception e)
		{
		    log.error( e.getMessage(),e );
     		obd.cerrarCallableStatement(stmtProcedure);
     		objecionRechazoDia.setError("esttrx:1~"+e.getMessage());
     		objecionRechazoDiaVector.add(objecionRechazoDia);
     		//return "esttrx:1~"+e.getMessage();
		}finally{
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
    	}
		
		
		
		//return listaTransaccion;
		return objecionRechazoDiaVector;
	}
 public static String[] fromJulian(String injulian) {
		 
		 String miF = "";
		 miF = "" + injulian;
		 
		 String ano = miF.substring(0, 1);
		 int anoTx = Integer.parseInt(ano);
		 
		 
		 
		 Calendar c1 = Calendar.getInstance();
		 Calendar c2 = new GregorianCalendar();
		 
		 int anoActual = c2.get(Calendar.YEAR);
		 
		 
		 String anoActualS = anoActual + "";
		 
		 String anoActualF = anoActualS.substring(3, 4);
		 
		 int anoActualInt = Integer.parseInt(anoActualF);
		 
		 
		 String anoActualFinalS = "";
		 if(anoActualInt<anoTx){
			 anoActual = anoActual - 10;
			 anoActualFinalS = (anoActual + "").substring(0, 3) + anoTx;
		 }else{
			 anoActualFinalS = (anoActual + "").substring(0, 3) + anoTx;
		 }
		 
		 int diasJulianos = Integer.parseInt(miF.substring(1, 4));
		 
		 Calendar fechaFinal = new GregorianCalendar(Integer.parseInt(anoActualFinalS),0,0);
		 
		 fechaFinal.add(Calendar.DATE, diasJulianos);
		 
		 String diaFinal = "";
		 if(fechaFinal.get(Calendar.DAY_OF_MONTH)<10)
			 diaFinal = "0"+fechaFinal.get(Calendar.DAY_OF_MONTH);
		 else{
			 diaFinal = ""+fechaFinal.get(Calendar.DAY_OF_MONTH);
		 }
		 String mesFinal = "";
		 int mesTX = fechaFinal.get(Calendar.MONTH) + 1;
		if(mesTX<10)
			mesFinal = "0"+mesTX;
		 else
			 mesFinal= ""+mesTX;
			 
		 
		   return new String[] {fechaFinal.get(Calendar.YEAR)+"",mesFinal , diaFinal};
 	}

	public int getUltimaPagina() {
		return ultimaPagina;
	}
	
	public void setUltimaPagina(int ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}
	
	public int getPagActual() {
		return pagActual;
	}

	public void setPagActual(int pagActual) {
		this.pagActual = pagActual;
	}


	public Object[] generarPlanillaExcelObjRech(String fechaInicio,
			String fechaFin, String tipoObjecionRechazo, String generaArchivoExcel) {
		log.info("Method: listaRechazoObjecion - Obtencion de parametros: fechaInicio-> "+fechaInicio+" | fechaTermino-> "+fechaFin+" | tipoObjecionRechazo-> "+tipoObjecionRechazo);
		setConexion();
		obd.setConexion(conexion);
		
		Object[] o = new Object[2];
		
		CallableStatement stmtProcedure = null;
		CSVWriter writer = null;
		try{
			log.info("Method: listaRechazoObjecion - Ejecutando SP_SIG_TRANSACOBJRECGESTTRAN");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_TRANSACOBJRECGESTTRAN(?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("fechaInicio",fechaInicio);
			stmtProcedure.setString("fechaTermino",fechaFin);
			stmtProcedure.setString("xkeyAtr",tipoObjecionRechazo);
			
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			int error = stmtProcedure.getInt("cod_error");
			
			rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			o[0] = rs;
			o[1] = error;
			
			// ANTIGUA FORMA DE EXPORTAR
			//writer = new CSVWriter(new FileWriter(generaArchivoExcel), ';');
			//writer.writeAll(rs,true);
			//writer.close();
			
			// NUEVA FORMA DE EXPORTARS
			exportarExcel(rs, generaArchivoExcel);
			
			
			
		}catch(Exception e)
		{
			log.error(e.getMessage(), e);
     		obd.cerrarCallableStatement(stmtProcedure);
     		return o;
		}finally{
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
    	}
		
		
		
		return o;
	}

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param rs2
     * @since 1.X
     */
    private void exportarExcel( ResultSet rs, String ruta ) {
        List<String[]> dataList = new ArrayList<String[]>();
        CSVWriter writer = null;
        
        try {
            // GENERA CABECERA
            String[] header = generateHeaderToExcelTrxGenerales();
            
            // PARSEO DE DATOS DEL EXCEL
            while ( rs.next() ) {
                dataList.add( getRegistrosToExcelTrxGenerales( rs ) );
            }
            
            writer = new CSVWriter( new FileWriter( ruta ), ';' );
            writer.writeNext( header );
            writer.writeAll( dataList );
            
        }
        catch ( SQLException e ) {
            log.error( e.getMessage(), e );
        }
        catch ( IOException e ) {
            log.error( e.getMessage(), e );
        }
        finally {
            if ( writer != null ) {
                try {
                    writer.close();
                }
                catch ( IOException e ) {
                    log.error( e.getMessage(), e );
                }
            }
        }
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private String[] generateHeaderToExcelTrxGenerales() {
       return new String[]{
              "NRO_TARJETA",
              "FECHATRANSAC",
              "COMERCIO",
              "PAIS",
              "MONTO_TRANSAC",
              "MONTO_FACTURACION",
              "CODRAZON",
              "ESTADOTRX",
              "PATPASS",
              "OPERADOR"
       };
 
    }
    
    
    private String[] getRegistrosToExcelTrxGenerales( ResultSet rs ) {
        try {
            String registro = "";
            String patpass="";
            registro += rs.getString( "NRO_TARJETA") + ";";
            registro += rs.getString( "FECHATRANSAC" )+ ";";
            registro += rs.getString( "COMERCIO" )+";";
            registro += rs.getString( "PAIS" )+ ";";
            registro += Utils.formateaMonto( rs.getString( "MONTO_TRANSAC" ))+ ";";
            registro += Utils.formateaMonto( rs.getString( "MONTO_FACTURACION" ))+ ";";
            registro += rs.getString( "CODRAZON" )+ ";";
            registro += rs.getString( "ESTADOTRX" )+ ";";
            if (rs.getString( "PATPASS" ).equals(" ") || rs.getString( "PATPASS" ).equals("") || rs.getString( "PATPASS" ).equals("-") || rs.getString( "PATPASS" ) == null || rs.getString("OPERADOR").equals("VN") || rs.getString("OPERADOR").equals("VI")) {
        	registro += "No"+ ";";
	    }else{
		registro += "Si"+ ";";
	    }
            registro += rs.getString("OPERADOR")+";";
            return registro.split( ";" );
        }
        catch ( SQLException e ) {
          log.error( e.getMessage(), e );
        }
        return null;
    }

}
