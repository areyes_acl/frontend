package clases;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

/**
 * Clase que encripta clave del usuario.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class EncriptacionClave {
	  //algoritmos
	static final Category log = Logger.getLogger(EncriptacionClave.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
    public static String SHA256 = "SHA-256";
    /***
    * Convierte un arreglo de bytes a String usando valores hexadecimales
    * @param digest arreglo de bytes a convertir
    * @return String creado a partir de <code>digest</code>
    */
   private static String toHexadecimal(byte[] digest){
       String hash = "";
       for(byte aux : digest) {
           int b = aux & 0xff;
           if (Integer.toHexString(b).length() == 1) hash += "0";
           hash += Integer.toHexString(b);
       }
       return hash;
   }

   /***
    * Encripta un mensaje de texto mediante algoritmo de resumen de mensaje.
    * @param message texto a encriptar
    * @param algorithm algoritmo de encriptacion, puede ser: MD2, MD5, SHA-1, SHA-256, SHA-384, SHA-512
    * @return mensaje encriptado
    */
   public static String getStringMessageDigest(String message, String algorithm){
       byte[] digest = null;
       byte[] buffer = message.getBytes();
       try {
           MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
           messageDigest.reset();
           messageDigest.update(buffer);
           digest = messageDigest.digest();
       } catch (NoSuchAlgorithmException ex) {
           log.error( ex.getMessage(), ex );
       }
       return toHexadecimal(digest);
   }
}
