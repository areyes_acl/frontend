package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.ParametrosType;
import Beans.Menu;
import Beans.Parametros;
import Beans.PermisosUsuario;
import Beans.Rol;
import Beans.SubMenu;
import Beans.Usuario;
import cl.filter.FiltroParametro;
import cl.util.DateTools;

import com.opensymphony.xwork2.ActionContext;

import constant.DatabaseConstant;

import db.OracleBD;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
 * </ul>
 * <p>
 * @author mrojas
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class LoginDAO{
	static final Logger log =  Logger.getLogger(LoginDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private OracleBD obd = new OracleBD();
	private Properties prop = new Properties();
	
	private ResultSet rs;
	private String esquema;
	
	private Connection conexion;
	
	public Map session;
	public String error;
	private String PARAMETRO_ACTIVO = "1";
	
	public LoginDAO(){		
		setConexion();
		
		this.session = ActionContext.getContext().getSession();
		setProp();
		
		this.esquema = prop.getProperty("esquema");
	}
	
	public void setConexion(){
		this.conexion = obd.conectar();
		obd.setResultado(rs);
	}
	
	/*
	public void setConexion()
	{
		this.conexion = obd.conectar("", "", "", "", "");
	}*/
	
	public Connection getConexion(){
		return conexion;
	}
	
	private void setProp(){
		//			this.prop.load(getClass().getResourceAsStream("/properties/conexion.properties"));
        prop = PropertiesConfiguration.getProperty();
	}

	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}
	
	@SuppressWarnings("unchecked")
	public Integer valCredenciales(String username, String password){
		log.info("Versi�n aplicaci�n V2.1 21-01-2014.");
		log.info("Method: valCredenciales - Obteniendo parametros: username-> "+username);

		Integer resol = 0;
		session.put("error",null);

		int id_usuario = 0;
		int cant_intentos = 0;
		int max_intentos = 0;
		int actualizar = 0;
		Integer error_clave = 0;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String fecha_buscar = dateFormat.format(date);
		
		Usuario usuarioLog = new Usuario();
		usuarioLog.setSid(Long.valueOf(0));
		usuarioLog.setPreguntas(0);

		EncriptacionClave encriptacionClave = new EncriptacionClave();
		log.info("Method: valCredenciales - Encriptando contrase�a");
		String claveUsuarioEncriptada = encriptacionClave.getStringMessageDigest(password, encriptacionClave.SHA256);

		String strsql = "SELECT U.SID AS SIDUSUARIO, U.RUT, U.DV, U.NOMBRE, U.APEPAT, U.APEMAT, U.ESTADO AS ESTADO_USUARIO, U.INTENTOS, PRTS1.VALOR AS NRO_INTENTOS , U.EMAIL, U.FONO, U.SU, " +
				"CONTRA.SID_CONTRASENA_ESTADO AS ESTADO_CLAVE, CONTRA.CONTRASENA AS CLAVE, CONTRA.FECHA_VENCIMIENTO AS VENC, PRTS2.VALOR AS DIAS_ALERTA " +
				"FROM SIG."+DatabaseConstant.TABLA_USUARIO+" U " +
				"INNER JOIN TBL_POLITICAS_USUARIO_PRTS PRTS1 ON PRTS1.NOMBRE_PARAMETRO = 'NRO_REINTENTOS_BLOQUEAR_CUENTA' " +
				"INNER JOIN TBL_POLITICAS_USUARIO_PRTS PRTS2 ON PRTS2.NOMBRE_PARAMETRO = 'DIAS_ALERTA_VENCIMIENTO' " +
				"INNER JOIN TBL_CONTRASENA CONTRA ON CONTRA.SID_USR = U.SID AND CONTRA.SID_CONTRASENA_ESTADO > 1 " +
				"WHERE CONCAT(U.RUT, U.DV) = '" + username + "' AND U.ESTADO = 1 AND U.SU = 0";

		try {
		    	rs = obd.consultar(strsql);
		    	obd.setResultado(rs);
		    	if (rs.next()){
		    	    	id_usuario = rs.getInt("SIDUSUARIO");
		    	    	cant_intentos = rs.getInt("INTENTOS");
		    	    	max_intentos = rs.getInt("NRO_INTENTOS");

		    	    	if(cant_intentos > max_intentos){
		    	    	    log.warn("Usuario bloqueado por superar maximo de intentos de loguearse.");
		    	    	    setError("Usuario bloqueado por superar maximo de intentos de loguearse.");
		    	    	    error = "Usuario bloqueado por superar maximo de intentos de loguearse";

		    	    	    //bloquear
		    	    	    strsql = "UPDATE "+DatabaseConstant.TABLA_USUARIO+" SET ESTADO = 2 WHERE SID = " + id_usuario;
		    	    	    try {
		    	    		Boolean res = obd.ejecutar(strsql);
        
		    	    		if (res != true){
		    	    		    log.error("Error al aumentar cantidad de intentos.");
		    	    		}
		    	    	    }catch (Exception e){
		    	    		log.error( e.getMessage(), e );
		    	    	    }
		    	    	    id_usuario = 0;
		    	    	}

		    	    	if(rs.getString("CLAVE").equals(claveUsuarioEncriptada)){
		    	    	    resol = 1;

		    	    	    String fechaVence = rs.getString("VENC").toString();

		    	    	    //comprobar vigencia de la clave
		    	    	    if (fechaVence.substring(0, 10).compareTo(fecha_buscar) < 0){
		    	    		resol = 0;
		    	    		error = "La clave ha caducado";
		    	    		id_usuario = 0;
		    	    	    }else{
		    	    		if(rs.getInt("ESTADO_CLAVE") == 3){
		    	    		    resol = 2;
		    	    		    actualizar = 1;
		    	    		}else{
		    	    		    //consular dias por vencer
		    	    		    Integer diasVencimiento = rs.getInt("DIAS_ALERTA");

		    	    		    Calendar fechaVencCal = Calendar.getInstance();
		    	    		    Integer yearVence = Integer.parseInt(fechaVence.substring(0, 4));
		    	    		    Integer mesVence = Integer.parseInt(fechaVence.substring(5, 7));
		    	    		    Integer diaVence = Integer.parseInt(fechaVence.substring(8, 10));
		    	    		    fechaVencCal.set(yearVence, mesVence, diaVence);
		    	    		    Date fechaVencimiento = fechaVencCal.getTime();

		    	    		    Calendar fechaActCal = Calendar.getInstance();
		    	    		    Integer yearActual = Integer.parseInt(fecha_buscar.substring(0, 4));
		    	    		    Integer mesActual = Integer.parseInt(fecha_buscar.substring(5, 7));
		    	    		    Integer diaActual = Integer.parseInt(fecha_buscar.substring(8, 10));
		    	    		    fechaActCal.set(yearActual, mesActual, diaActual);
		    	    		    Date fechaActual = fechaActCal.getTime();

		    	    		    Long diasDif = DateTools.getDateDiff(fechaActual, fechaVencimiento, TimeUnit.DAYS);

		    	    		    if(diasDif <= diasVencimiento){
		    	    			session.put("mensajeVencimiento", "[AVISO] Quedan " + diasDif + " d�as para que su contrase�a caduque, por favor modifique su contrase�a");
		    	    		    }

		    	    		   
		    	    		}
        		    	    	    
		    	    		    usuarioLog.setSid(Long.valueOf(rs.getInt("SIDUSUARIO")));
        	    	    		    usuarioLog.setRut(rs.getInt("RUT"));
        	    	    		    usuarioLog.setDv(rs.getString("DV"));
        	    	    		    usuarioLog.setNombres(rs.getString("NOMBRE"));
        	    	    		    usuarioLog.setApellidoPaterno(rs.getString("APEPAT"));
        	    	    		    usuarioLog.setApellidoMaterno(rs.getString("APEMAT"));
        	    	    		    usuarioLog.setEmail(rs.getString("EMAIL"));
        	    	    		    usuarioLog.setFono(rs.getString("FONO"));
        	    	    		    usuarioLog.setEsSuperUsuario(rs.getBoolean("SU"));
        	    	    		    usuarioLog.setEstado(rs.getBoolean("ESTADO_USUARIO"));
		    	    	    }
		    	    	}else{
		    	    	    //error de clave
		    	    	    error = "Error de Usuario o Password";
		    	    	    strsql = "UPDATE "+DatabaseConstant.TABLA_USUARIO+" SET INTENTOS = (INTENTOS + 1) WHERE SID = " + id_usuario;
		    	    	    try {
		    	    		Boolean res = obd.ejecutar(strsql);

		    	    		if (res != true){
		    	    		    log.error("Error al aumentar cantidad de intentos.");
		    	    		}
		    	    	    }catch (Exception e){
		    	    		log.error( e.getMessage(), e );
		    	    	    }
    
		    	    	    id_usuario = 0;
		    	    	    error_clave = 1;
		    	    	}
		    	}else{
		    	    	log.warn("Usuario inexistente o inactivo.");
		    	    	setError("Usuario inexistente o inactivo.");
		    	    	error = "Error de Usuario o Password";
		    	}
		    	obd.cerrarResultSet();
		} catch (SQLException e){
		    	log.error( e.getMessage(), e );
		}

		if(usuarioLog.getSid() > 0){
		    	//resetar intentos en el usuario
		    	strsql = "UPDATE "+DatabaseConstant.TABLA_USUARIO+" SET INTENTOS = 0 WHERE SID = " + id_usuario;
	    		try {
	    		    Boolean res = obd.ejecutar(strsql);

	    		    if (res != true){
	    			log.error("Error al resetear cantidad de intentos.");
	    		    }
	    		}catch (Exception e){
	    		    log.error( e.getMessage(), e );
	    		}

		    	strsql = "SELECT ";
		    	strsql += " ROLU.SID_ROL AS ID_ROL, ";
		    	strsql += " ROL.NOMBRE_ROL AS ROL_NOMBRE, ";
		    	strsql += " ROL.DESCRIPCION AS ROL_DESC, ";
		    	strsql += " ROLSUB.SID AS ID_ROLSUB, ";
		    	strsql += " ROLSUB.SID_SUB_MENU AS ID_SUBMENU, ";
		    	strsql += " SUBMENU.NOMBRE_SUB_MENU AS SUBMENU_NOMBRE, ";
		    	strsql += " SUBMENU.DESCRIPCION AS SUBMENU_DESC, ";
		    	strsql += " SUBMENU.POSICION AS SUBMENU_POS, ";
		    	strsql += " SUBMENU.LINK AS SUBMENU_LINK, ";
		    	strsql += " MENU.SID AS MENU_ID, ";
		    	strsql += " MENU.NOMBRE_MENU AS MENU_NOMBRE, ";
		    	strsql += " MENU.DESCRIPCION AS MENU_DESC, ";
		    	strsql += " MENU.POSICION AS MENU_POS, ";
		    	strsql += " RMTP.SID_TIPO_PERMISO AS ID_PERMISO, ";
		    	strsql += " TPERMISO.TIPO_PERMISO AS PERMISO ";
    	    	    	strsql += " FROM "+DatabaseConstant.TABLA_USUARIO+" USUARIO ";
    	    	    	strsql += " INNER JOIN TBL_ROL_USUARIO ROLU ON ROLU.SID_USR = USUARIO.SID ";
    	    	    	strsql += " INNER JOIN "+DatabaseConstant.TABLA_ROL+" ROL ON ROL.SID = ROLU.SID_ROL AND ROL.ESTADO = 1 ";
    	    	    	strsql += " INNER JOIN TBL_ROL_SUBMENU ROLSUB ON ROLSUB.SID_ROL = ROL.SID AND ROLSUB.ESTADO = 1 ";
    	    	    	strsql += " INNER JOIN TBL_SUB_MENU SUBMENU ON SUBMENU.SID = ROLSUB.SID_SUB_MENU ";
    	    	    	strsql += " INNER JOIN TBL_MENU MENU ON MENU.SID = SUBMENU.SID_MENU ";
    	    	    	strsql += " INNER JOIN TBL_ROL_MENU_TIPO_PERMISO RMTP ON RMTP.SID_ROL_SUB_MENU = ROLSUB.SID AND RMTP.ESTADO = 1 ";
    	    	    	strsql += " INNER JOIN TBL_TIPO_PERMISO TPERMISO ON TPERMISO.SID = RMTP.SID_TIPO_PERMISO AND TPERMISO.ESTADO = 1 ";
    	    	    	strsql += " WHERE USUARIO.SID = " + usuarioLog.getSid() + " AND USUARIO.ESTADO = 1 AND ROLU.ESTADO = 1";
    	    	    	strsql += " ORDER BY MENU_POS, SUBMENU_POS ASC";

    	    	    	List<Rol> listaRoles = new ArrayList<Rol>();

    	    	    	List<Menu> listaMenu = new ArrayList<Menu>();
    	    	    	usuarioLog.setListaMenu(listaMenu);
    	    	    	usuarioLog.setActualizar(actualizar);

    	    	    	List<SubMenu> listaSubMenu = new ArrayList<SubMenu>();
    	    	    	//List<PermisosUsuario> listaPermisos = new ArrayList<PermisosUsuario>();

    	    	    	Menu menu_actual = new Menu();
    	    	    	SubMenu submenu_actual = new SubMenu();
    	    	    	Integer cont = 0;

        		try {
        		    	rs = obd.consultar(strsql);
        		    	obd.setResultado(rs);
        		    	while (rs.next()){
        		    	    	Rol rol_usuario = new Rol();
        		    	    	rol_usuario.setSid(Long.valueOf(rs.getInt("ID_ROL")));
        		    	    	rol_usuario.setNombre(rs.getString("ROL_NOMBRE"));
        		    	    	rol_usuario.setDescripcion(rs.getString("ROL_DESC"));
        		    	    	rol_usuario.setActivo(true);

        		    	    	if(!listaRoles.contains(rol_usuario)){
        		    	    	    listaRoles.add(rol_usuario);
        		    	    	}

        		    	    	//Menu
        		    	    	Menu menu = new Menu();
        		    	    	menu.setSid(Long.valueOf(rs.getInt("MENU_ID")));
        		    	    	menu.setNombre(rs.getString("MENU_NOMBRE"));
        		    	    	menu.setDescripcion(rs.getString("MENU_DESC"));
        		    	    	menu.setPosicion(rs.getInt("MENU_POS"));

        		    	    	if(menu_actual.getSid() == null){
        		    	    	    menu_actual = menu;
        		    	    	    menu_actual.setListaSubMenus(listaSubMenu); //lista de submenu vacia
        		    	    	}else{
        		    	    	    if(menu_actual.getSid() != menu.getSid()){
        		    	    		//a�adir menu a usuario
        		    	    		menu_actual.getListaSubMenus().add(submenu_actual);
        		    	    		usuarioLog.getListaMenu().add(menu_actual);

        		    	    		//menu nuevo, creo nueva lista de submenu y nueva lista de permisos
        		    	    		menu_actual = menu;

        		    	    		listaSubMenu = new ArrayList<SubMenu>();
        		    	    		menu_actual.setListaSubMenus(listaSubMenu); //lista de submenu vacia
        		    	    		//listaPermisos = new ArrayList<PermisosUsuario>();
        		    	    		
        		    	    		submenu_actual = new SubMenu();
        		    	    	    }else{
        		    	    		//seguir ocupando menu_actual
        		    	    	    }
        		    	    	}

        		    	    	//submenu
        		    	    	SubMenu submenu = new SubMenu();
        		    	    	submenu.setSid(Long.valueOf(rs.getInt("ID_SUBMENU")));
        		    	    	submenu.setNombre(rs.getString("SUBMENU_NOMBRE"));
        		    	    	submenu.setDescripcion(rs.getString("SUBMENU_DESC"));
        		    	    	submenu.setPosicion(rs.getInt("SUBMENU_POS"));
        		    	    	submenu.setLink(rs.getString("SUBMENU_LINK"));

        		    	    	ArrayList listaPermisosTemp = new ArrayList<PermisosUsuario>();
        		    	    	submenu.setListaPermisos(listaPermisosTemp);

        		    	    	if(submenu_actual.getSid() == null){
        		    	    	    submenu_actual = submenu;
        		    	    	    //listaPermisos = new ArrayList<PermisosUsuario>();
        		    	    	}else{
        		    	    	    if(submenu_actual.getSid() != submenu.getSid()){
        		    	    		//a�adir submenu a menu_actual
        		    	    		menu_actual.getListaSubMenus().add(submenu_actual);
        		    	    		//listaPermisos = new ArrayList<PermisosUsuario>();
        		    	    		submenu_actual = submenu;
        		    	    	    }else{
        		    	    		//seguir ocupando submenu_actual
        		    	    	    }
        		    	    	}

        		    	    	//Permisos
        		    	    	PermisosUsuario permiso = new PermisosUsuario();
        		    	    	permiso.setPermisoId(rs.getInt("ID_PERMISO"));
        		    	    	permiso.setNombre(rs.getString("PERMISO"));

        		    	    	if(!submenu_actual.getListaPermisos().contains(permiso)){
        		    	    	    submenu_actual.getListaPermisos().add(permiso);
        		    	    	}

        		    	    	cont++;
        		    	}
        		    	obd.cerrarResultSet();

        		} catch (SQLException e){
        		    	log.error( e.getMessage(), e );
        		}

        		if(cont > 0){
        		    //submenu_actual.setListaPermisos(listaPermisos);
        		    menu_actual.getListaSubMenus().add(submenu_actual);
        		    usuarioLog.getListaMenu().add(menu_actual);

        		    usuarioLog.setListaRoles(listaRoles);
        		    session.put("usuarioLog", usuarioLog);
        		    session.put("sidusuario", usuarioLog.getSid());

        		    //verificar si tiene las preguntas secretas
        		    strsql = "SELECT COUNT(SID) AS CONT FROM TBL_PREGUNTAS_USUARIO WHERE SID_REF_USUARIO = " + id_usuario + " AND ESTADO = 1";

        		    try {
        			rs = obd.consultar(strsql);
        			obd.setResultado(rs);
        			
            		    /**
            		     * Se debe validar que el parametro de recuperacion por
            		     * preguntas secretas se encuentre activo o no
            		     * 
            		     */
        			if (rs.next()){
        			    if(rs.getInt("CONT") < 3 && resol != 2 && parametroRecuperacionPorPreguntasActiva()){
        				resol = 3;
        				usuarioLog.setPreguntas(1);
        			   }else{
        			       resol = 1;
        			    }
        			}else{
        			    log.warn("Usuario inexistente o inactivo.");
        			    setError("Usuario inexistente o inactivo.");
        			}
        			obd.cerrarResultSet();
        		    } catch (SQLException e){
        			log.error( e.getMessage(), e );
        		    }
        		}else{
        		    //Usuario sin permisos
        		    resol = 4;
        		}
		}

        	session.put("error", error);

		if (obd.cerrarResultSet()==false){
		    log.error("Error: No se pudo cerrar resulset.");
		    if (obd.cerrarConexionBD()==false){
			log.error("Error: No se pudo cerrar conexion a la base de datos.");
		    }			
		}else{
		    if (obd.cerrarConexionBD()==false){
			log.error("Error: No se pudo cerrar conexion a la base de datos.");
		    }
		}

  		try {
  		    if(conexion != null && !conexion.isClosed()){
  			conexion.close();
  		    }
  		}catch ( SQLException e ) {
  			log.error( e.getMessage(), e );
  		}

		return resol;
	}

	@SuppressWarnings("unchecked")
	public boolean valCredencialesLdap(String username)
	{
		log.info("Versi�n aplicaci�n V2.1 06-08-2015.");
		log.info("Method: valCredencialesLdap - Obteniendo parametros: username-> "+username);
		
		boolean resol = false;
		session.put("error",null);
		
		
		String strsql = "SELECT U.SID AS SIDUSUARIO, U.ID_USUARIO, U.NOMBRE, U.APELLIDO, U.CLAVE, U.ACTIVO, R.SID AS SIDUSUARIOROL, R.ROL AS SIDROLUSUARIOROL,";
		strsql += " RO.XKEY, RO.DESCRIPCION AS DESCRIPCION, RO.SID as SIDROL FROM "+esquema+"."+DatabaseConstant.TABLA_USUARIO+"  U, "+esquema+".TBL_USUARIO_ROL R, "+esquema+"."+DatabaseConstant.TABLA_ROL+" RO";
		strsql += " WHERE U.ID_USUARIO =  '" + username + "' AND ";
		strsql += " U.SID = R.USUARIO_IG AND ";
        strsql += " R.ROL = RO.SID AND";
        strsql += " U.ACTIVO = 1 ";

        try {

        	rs = obd.consultar(strsql);
        	obd.setResultado(rs);
			if (rs.next()) {
				log.info("Method: valCredencialesLdap - Seteando resultados");

				resol = true;
				session.put("xkeyrol", rs.getString("XKEY"));
				session.put("logged-in", "true");
				session.put("nombre", rs.getString("nombre"));
				session.put("sidusuario", rs.getInt("SIDUSUARIO"));
				session.put("sidrol", rs.getInt("SIDROL"));

			} else {
				log.warn("Usuario inexistente o inactivo.");
				setError("Usuario inexistente o inactivo.");
			}
        	obd.cerrarResultSet();

        } catch (SQLException e){
            log.error( e.getMessage(), e );
        }finally{
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
    		 if (obd.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
    		 }
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
        return resol;
	}

	public String getError(){
		return error;
	}

	public void setError(String error){
		this.error = error;
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @since 1.X
	 */
	public Boolean parametroRecuperacionPorPreguntasActiva() {

		ParametrosDAO parametrosDAO = new ParametrosDAO();
		FiltroParametro filtro = new FiltroParametro(
			ParametrosType.PREGUNTAS_SEGURIDAD.getLabel());
		Parametros parametro = parametrosDAO.buscarPorFiltro(filtro).get(0);

		if (parametro != null && PARAMETRO_ACTIVO.equals(parametro.getValor())) {
		    return Boolean.TRUE;
		}

		return Boolean.FALSE;
	    }
}
