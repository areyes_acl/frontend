package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.StatusPagoType;
import Beans.PagoContableBean;
import db.OracleBD;

/**
 * 
 * @author afreire
 *
 */
public class PagoDAO {
	// CONSTANTES DEL SISTEMA
	static final Category log =  Logger.getLogger(PagoDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	private String esquema;
	
	// QUERYS 
	private static final String SQL_UPDATE_XLS_BY_ID = "UPDATE TBL_PAGO SET NOMBRE_ARCH_XLS=':archivo_xls' WHERE SID = :sidPago";

	/**
	 * Constructor
	 */
	public PagoDAO(){
	    try {
		Properties propiedades = new Properties();
		propiedades = PropertiesConfiguration.getProperty();
		esquema = propiedades.getProperty("esquema");    
	    }catch (Exception e){
		log.error("Error al leer el archivo de propiedades.");
		log.error( e.getMessage(),e );
	    }	
	}

	/**
	 * 
	 * Metodo de gestion sobre los pagos,
	 * se recibe un pago y se llama al SP_SIG_GESTIONAR con un estado del pago,
	 * de acuerdo a este parametro el SP sabe si se trata de un ingreso, un rechazo o una autorizacion
	 * del pago  y se realiza la logica correspondiente
	 * 
	 * @param pago
	 * @return
	 * @throws Exception
	 */
	public void gestionarPago(PagoContableBean pago) throws Exception {
	    Connection conexion = null;
	    db.OracleBD bdOracle = null;
	    CallableStatement stmtProcedure = null;
	    ResultSet resultados = null;
	    int codigoError = 0;
	    

	    try {
		log.info(" ===== Se ejecuta el procedimiento : SP_SIG_GESTIONAR_PAGO =====");
		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);
		stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_GESTIONAR_PAGO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); END;");
		stmtProcedure.setString("p_estado", pago.getEstadoPago().getXkey()); 
		stmtProcedure.setString("p_fecha_pago", pago.getFechaPago()); 
		stmtProcedure.setLong("p_monto_pago", pago.getMontoBruto());
		stmtProcedure.setLong("p_monto_desc", pago.getMontoDescuento());
		stmtProcedure.setLong("p_monto_total", pago.getMontoFinal());
		stmtProcedure.setLong("p_sid_usuario", pago.getSidUsuario()); 
		stmtProcedure.setString("p_nombre_arch_adj", pago.getFilenameAdjuntoImagen()); 
		stmtProcedure.setString("p_nombre_arch_xls", pago.getFilenameAdjuntoExcel()); 
		stmtProcedure.setString("p_motivo_rechazo", pago.getMotivoRechazo());
		stmtProcedure.setInt("filtro_operador", pago.getOperador());
		stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR); 
		stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);

		stmtProcedure.execute();

		codigoError = stmtProcedure.getInt("cod_error");
		String warning = stmtProcedure.getString("warning");

		log.info("======= RESULTADOS SP_SIG_GESTIONAR_PAGO ========");
		log.info("codigoError : " + codigoError);
		log.info("warning : " + warning);

		if( codigoError != 0){
		    throw new Exception(warning);
		}
	    } finally {
		cerrarConexiones(bdOracle, resultados, conexion,stmtProcedure);
	    }
	}

	/**
	 * Metodo que busca pagos de acuerdo a una fecha y a un estado
	 * 
	 * @param fechaDesde
	 * @param fechaHasta
	 * @param estado
	 * @return
	 * @throws Exception 
	 */
	public List<PagoContableBean> buscarPago(String fechaDesde, String fechaHasta, StatusPagoType estado, int operador) throws Exception{
	    Connection conexion = null;
	    db.OracleBD bdOracle = null;
	    CallableStatement stmtProcedure = null;
	    ResultSet rs = null;
	    int codigoError = 0;
	    List<PagoContableBean> listaPagos = null;
	    

	    try {
		log.info(" ===== Se ejecuta el procedimiento : SP_SIG_BUSCA_PAGOS =====");
		log.info(" fecha_desde : " + fechaDesde ); 
		log.info(" fecha_hasta : "+ fechaHasta);
		log.info("estado : " + estado.getXkey());
		log.info(" Operador : " + operador);

		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);
		stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_BUSCA_PAGOS(?, ?, ?, ?, ?, ?, ?); END;");
		stmtProcedure.setString("fecha_desde", fechaDesde);
		stmtProcedure.setString("fecha_hasta", fechaHasta);
		stmtProcedure.setString("p_estado", estado.getXkey());
		stmtProcedure.setInt("filtro_operador", operador);
		stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
		stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
		stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

		stmtProcedure.execute();

		codigoError = stmtProcedure.getInt("cod_error");
		String warning = stmtProcedure.getString("warning");
		rs= (ResultSet) ((stmtProcedure).getObject("prfCursor"));

		log.info("======= RESULTADOS :  BuscarPago ========");
		log.info("codigoError : " + codigoError);
		log.info("warning : " + warning);

		if (codigoError == 0){
		    listaPagos = new ArrayList<PagoContableBean>();

		    // BUSCA DATOS DEL CURSOR
		    while(rs.next()){
			PagoContableBean pago = new PagoContableBean();
			pago.setSid(rs.getLong("SID"));
			pago.setFechaPago(rs.getString("FECHA_PAGO"));
			pago.setMontoBruto(rs.getLong("MONTO_PAGO"));
			pago.setMontoDescuento(rs.getLong("DESCUENTO"));
			pago.setMontoFinal(rs.getLong("MONTO_FINAL"));
			pago.getLog().setNombreUsuarioIngreso(rs.getString("USUARIO_INGRESO"));
			pago.getLog().setFechaIngreso(rs.getString("FECHA_INGRESO"));
			pago.getLog().setNombreUsuarioAutorizador(rs.getString("USUARIO_AUTOR"));
			pago.getLog().setFechaAutorizacion(rs.getString("FECHA_AUTOR"));
			pago.setMotivoRechazo(rs.getString("MOTIVO_RECHAZO"));
			pago.setEstadoPago(StatusPagoType.findByXkey(rs.getString("XKEY")));
			pago.setFilenameAdjuntoImagen(rs.getString("NOMBRE_ARCH_ADJ"));
			pago.setFilenameAdjuntoExcel(rs.getString("NOMBRE_ARCH_XLS"));
			pago.getLog().setEstadoContable(rs.getString("ESTADO_CONTABLE"));
			pago.getLog().setFechaContable(rs.getString("FECHA_CONTABLE"));
			pago.setOperador(rs.getInt("OPERADOR"));
			listaPagos.add(pago);
		    }
		    log.info(listaPagos);
		}else{
		    throw new Exception(warning);
		}			
	    } catch (SQLException e) {
		throw e;
	    } finally {
		cerrarConexiones(bdOracle,rs,conexion,stmtProcedure); 
	    }
	    return listaPagos;
	}

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param sid
     * @param xlsFile
     * @return
     * @since 1.X
     */
    public int actualizarArchivoXlsPagoPorId(Long sid, String xlsFile) {
	Connection conexion = null;
	OracleBD obd = new OracleBD();
	obd = new OracleBD();
	conexion = obd.conectar();
	obd.setConexion(conexion);

	String sql = SQL_UPDATE_XLS_BY_ID.replace(":archivo_xls", xlsFile)
		.replace(":sidPago", String.valueOf(sid));
	
	if(obd.ejecutar(sql)){
	    return 0;
	}else{
	    return -1;
	}

    }

	/**
	 * Metodo encargado de cerrar las conexioness
	 * @param oracleBD
	 * @param rs
	 * @param conexion
	 */
	private void cerrarConexiones(OracleBD oracleBD, ResultSet rs, Connection conexion,CallableStatement stmtProcedure ){
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
			}
		}

		if (oracleBD.cerrarResultSet() == false) {
			log.error("Error: No se pudo cerrar resulset.");
			if (oracleBD.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}
		} else {
			if (oracleBD.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
			}
		}
		try {
			if (conexion != null) {
				conexion.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}
		
		try {
			if(stmtProcedure!= null){
				stmtProcedure.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(),e);
		}
		
	}
	
}
