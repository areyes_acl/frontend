package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import Beans.Parametros;
import Beans.Rol;

import cl.filter.FiltroParametro;

import properties.PropertiesConfiguration;
import types.EstadoType;
import db.OracleBD;

public class ParametrosDAO {
	static final Logger log =  Logger.getLogger(ParametrosDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

	private OracleBD obd = new OracleBD();
	private Properties prop = new Properties();

	private String esquema;

	private String listaParametros;
	private String updateParametros;

	private ResultSet rs;
	
	private ResultSet rs1;

	private Connection conexion;

	/*
	 * Constructor 
	 */
	public ParametrosDAO(){
		setProp();
		this.esquema = prop.getProperty("esquema");
		//setConexion();
	}
	
	private void setProp(){
		//			this.prop.load(getClass().getResourceAsStream("/properties/conexion.properties"));
        prop= PropertiesConfiguration.getProperty();
	}
	
	public void setConexion(){
		this.conexion = obd.conectar();
		obd.setResultado(rs);
	}
	
	public Connection getConexion(){
		return conexion;
	}
	
	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}
	
	public String getListaParametros(){
		setConexion();
		log.info("Method: getListaParametros - Obtencion de parametros: ninguno");
		listaParametros = "esttrx:0~";
		rs = obd.consultar("select null as MAX_AMOUNT, TPR.*, ATR.DESCRIPCION as ACCION, ATR.COD_TRANSAC as COD_TRANSAC from "+esquema+".tbl_transac_por_nro_razon TPR, "+esquema+".tbl_accion_transac ATR where TPR.accion_transac = ATR.sid and TPR.ACTIVO=1");
		rs1 = obd.consultar("select CRV.*, ATR.DESCRIPCION as ACCION, ATR.COD_TRANSAC as COD_TRANSAC from "+esquema+".tbl_cod_razon_visa CRV, "+esquema+".tbl_accion_transac ATR where CRV.accion_transac = ATR.sid and CRV.ACTIVO=1");
		obd.setResultado(rs);
		obd.setResultado(rs1);
		try {
			if(rs.next()){
				log.info("Method: getListaParametros - Obteniendo resultados");
				rs.previous();
				while(rs.next()){
					listaParametros += rs.getInt("SID")+"|";
					listaParametros += rs.getString("COD_MOTIVO_INI")+"|";
					listaParametros += rs.getString("COD_TRANSAC")+"|";
					listaParametros += rs.getString("DESCRIPCION")+"|";
					listaParametros += rs.getInt("PLAZO")+"|";
					listaParametros += rs.getString("MAX_AMOUNT")+"|";
					listaParametros += rs.getString("ACCION")+("~");
				}
				rs.close();
				if(rs1.next()){
					log.info("Method: getListaParametros - Obteniendo resultados");
					rs1.previous();
					while(rs1.next()){
						listaParametros += rs1.getInt("SID")+"|";
						listaParametros += rs1.getString("COD_MOTIVO_INI")+"|";
						listaParametros += rs1.getString("COD_TRANSAC")+"|";
						listaParametros += rs1.getString("DESCRIPCION")+"|";
						listaParametros += rs1.getInt("PLAZO")+"|";
						listaParametros += rs1.getInt("MAX_AMOUNT")+"|";
						listaParametros += rs1.getString("ACCION")+("~");
					}
					rs1.close();
				}
			}else{
				log.info("Method: getListaParametros - No hay datos");
				listaParametros = "esttrx:1~No hay datos";
			}
			log.info("Method: getListaParametros - Serializando lista");
		} catch (SQLException e) {
			
			log.error(e.getMessage(),e);
			
		}finally{
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error(e.getMessage(),e);
            }
    	}

		return listaParametros;
	}
	
	public String updatePlazo(String sid, String plazo){
		setConexion();
		log.info("Method: getListaParametros - Obteniendo parametros: sid-> "+sid+" | plazo-> "+plazo);
		updateParametros = "esttrx:0~";
		
		try{
			String sql = "update "+esquema+".TBL_TRANSAC_POR_NRO_RAZON set PLAZO = '"+plazo+"' where SID = "+sid;
			
			obd.ejecutar(sql);
		} catch (Exception e) {
		    log.error(e.getMessage(),e);
			updateParametros = "esttrx:1~"+e.getMessage();
			obd.cerrarConexionBD();
		}finally{
    			if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}

    			try {
    			    if(conexion != null && !conexion.isClosed()){
    				conexion.close();
    			    }
    			}catch ( SQLException e ) {
    			    log.error(e.getMessage(),e);
    			}
		}
		return updateParametros;
	}

	public List<Parametros> buscarPorFiltro(FiltroParametro filtro) {
	    String nombre = filtro.getNombre();
	    EstadoType estado = filtro.getEstado();

	    log.info("Se ejecuta Function ParametrosDAO.buscarPorFiltro");
	    obd.cerrarResultSet();
	    setConexion();

	    List<Parametros> listaParametros = new ArrayList<Parametros>();


	    String strsql = "SELECT PRTS.SID AS ID, PRTS.NOMBRE_PARAMETRO AS NOMBRE, PRTS.VALOR AS VALOR, PRTS.ESTADO AS ESTADO FROM "+esquema+".TBL_POLITICAS_USUARIO_PRTS PRTS WHERE 1=1";
	    
	    if( filtro != null && filtro.getNombre() != null){
		strsql = strsql + " AND  PRTS.NOMBRE_PARAMETRO = '" + filtro.getNombre()+"'";
	    }
	    
	    if( filtro != null && filtro.getEstado() != null){
		strsql = strsql + " AND  PRTS.ESTADO = '" + filtro.getEstado().getValue()+"'";
	    }
	   log.info(strsql);
	    rs = obd.consultar(strsql);
	    obd.setResultado(rs);

	    try {
		log.info("Method: getListaParametros - Obteniendo resultados");

		while (rs.next()) {
		    Parametros para = new Parametros();
		    para.setSid(Long.valueOf(rs.getInt("ID")));
		    para.setNombre(rs.getString("NOMBRE"));
		    para.setValor(rs.getString("VALOR"));
		    para.setEstado(estado);

		    listaParametros.add(para);
		}
	    
		obd.cerrarResultSet();
	    } catch (Exception e) {
		log.error(e.getMessage(), e);
	    }

	    if (obd.cerrarResultSet()==false){
		log.error("Error: No se pudo cerrar resulset.");
		if (obd.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}			
	    }else{
		if (obd.cerrarConexionBD()==false){
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if(conexion != null && !conexion.isClosed()){
		    conexion.close();
		}
	    }catch (SQLException e) {
		log.error( e.getMessage(), e );
	    }

	    return listaParametros;
	}
}
