package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

public class ObjecionesDAO {
	
	static final Logger log =  Logger.getLogger(ObjecionesDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	private static final String ERROR_NOT_UNIQUE = "ORA-00001";
	
	private OracleBD obd = new OracleBD();
	private Properties prop = new Properties();
	
	private String listaTipoObjecion;
	
	private ResultSet rs;
	
	private Connection conexion;
	
	private String esquema;
	
	
	/*
	 * Constructor 
	 */
	public ObjecionesDAO()
	{
		setProp();
		this.esquema = prop.getProperty("esquema");
	}
	
	private void setProp()
	{
		//			this.prop.load(getClass().getResourceAsStream("/properties/conexion.properties"));
        prop = PropertiesConfiguration.getProperty();
	}
	
	public void setConexion()
	{
		this.conexion = obd.conectar();
	}
	
	public Connection getConexion()
	{
		return conexion;
	}
	
	public String getListaTipoObjecion()
	{
		log.info("Method: getListaTipoObjecion - Obtencion de parametros: ninguno");
	
		setConexion();
		
		log.info("Method: getListaTipoObjecion - Obteniendo tipo objeciones");
		listaTipoObjecion = "esttrx:0~";
		rs = obd.consultar("select* from "+esquema+".TBL_TIPO_TRANSAC where TIPO = 'O' and ACTIVO = 1");
		obd.setResultado(rs);

		try {
			if(rs.next())
			{
				log.info("Method: getListaTipoObjecion - Obteniendo resulstados");
				rs.previous();
				while(rs.next())
				{
					listaTipoObjecion += rs.getInt("SID")+"|";
					listaTipoObjecion += rs.getString("DESCRIPCION")+("~");
				}
				rs.close();
				log.info("Method: getListaTipoObjecion - "+listaTipoObjecion);
			}else{
				listaTipoObjecion = "esttrx:1~No hay datos";
			}
			log.info("Method: getListaTipoObjecion - Serializando lista");
		} catch (SQLException e) {
		    log.error( e.getMessage(), e );
			obd.cerrarConexionBD();
		}finally{
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaTipoObjecion;
	}
	
	public String ejecutaObjecion(String sid, String numeroTarjeta, String fechaTrx, String codigoAutorizacion, String glosa, String tipoObj, String sidusuario, String numIncidente)
	{	
		log.info("Method: ejecutaObjecion - Obteniendo parametros: sid-> "+sid+" | numeroTarjeta-> "+numeroTarjeta+" | fechaTrx-> "+fechaTrx+" | codigoAutorizacion-> "+codigoAutorizacion+" | glosa-> "+glosa+" | tipoObj-> "+tipoObj+" | sidusuario-> "+sidusuario+" | numIncidente-> "+numIncidente);
		
		setConexion();
		obd.setConexion(conexion);
		CallableStatement stmtProcedure = null;
		
		log.info("Method: ejecutaObjecion - Tipo Objecion:" + tipoObj);
		
		String dev = "";

		try {

			log.info("Method: ejecutaObjecion - Ejecutando SP_SIG_ObjetarTransac");	
			stmtProcedure = conexion.prepareCall("BEGIN "+esquema+".SP_SIG_ObjetarTransac(?, ?, ?, ?, ?, ?, ?, ?, ?,?); END;");
			
			stmtProcedure.setInt("SIDin", Integer.parseInt(sid));
			stmtProcedure.setString("NRO_TARJETA", numeroTarjeta);
			stmtProcedure.setString("FECHA_HR_TRASAC", fechaTrx);
			stmtProcedure.setString("CODIGO_AUTORIZACION", codigoAutorizacion);
			stmtProcedure.setString("GLOSA", glosa);
			stmtProcedure.setString("TIPO_OBJ", tipoObj);
			stmtProcedure.setString("USUARIO", sidusuario);
			stmtProcedure.setString("NUM_INCIDENTE", numIncidente);
			
			stmtProcedure.registerOutParameter("FLAG", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("ERROR_OUT", OracleTypes.VARCHAR);
			
			stmtProcedure.execute();
			
			String error = stmtProcedure.getString("ERROR_OUT");
			
			if(error.equals("0"))
			{
				log.info("Method: ejecutaObjecion - Salida Flag: "+stmtProcedure.getString("FLAG"));	
				dev = stmtProcedure.getString("FLAG");
			}else{
				log.error("Method: ejecutaObjecion - "+stmtProcedure.getString("ERROR_OUT"));
				
				String msgError = stmtProcedure.getString("ERROR_OUT");
				
				
				dev = "esttrx:1|"+msgError;
			}
			stmtProcedure.close();
		} catch (SQLException e) {
			obd.cerrarCallableStatement(stmtProcedure);
			dev = "esttrx:1"+e.getMessage();
			log.error( e.getMessage(), e );
		}finally{
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		return dev;
	}
}
