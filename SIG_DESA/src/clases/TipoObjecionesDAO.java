package clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

public class TipoObjecionesDAO {
	static final Logger log =  Logger.getLogger(TipoObjecionesDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private OracleBD obd = new OracleBD();
	private Properties prop = new Properties();
	
	private String listaTipoObjecion;
	
	private ResultSet rs;
	private String esquema;
	private Connection conexion;
	
	
	/*
	 * Constructor 
	 */
	public TipoObjecionesDAO(){
		this.setProp();
		esquema = prop.getProperty("esquema");
		setConexion();
	}
	
	private void setProp()
	{
		//			this.prop.load(getClass().getResourceAsStream("/properties/conexion.properties"));
        prop = PropertiesConfiguration.getProperty();
	}
	
	public void setConexion()
	{
		this.conexion = obd.conectar();
	}
	
	public Connection getConexion()
	{
		return conexion;
	}
	
	public String getListaTipoObjecion()
	{
		log.info("Method: getListaTipoObjecion - Obtencion de parametros: ninguno");
		listaTipoObjecion = "esttrx:0~";
		rs = obd.consultar("select SID, DESCRIPCION from TMP_TIPO_TRANSAC where TIPO = 'O'");

		obd.setResultado(rs);
		try {
			if(rs.next())
			{
				rs.previous();
				while(rs.next())
				{
					listaTipoObjecion += rs.getInt("SID")+"|";
					listaTipoObjecion += rs.getString("DESCRIPCION")+("~");
				}
			}else{
				listaTipoObjecion = "esttrx:1~No hay datos";
			}
			
		} catch (SQLException e) {
		    log.error( e.getMessage(),e );
		}finally{
    		if (obd.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (obd.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(),e );
            }
    	}
		
		
		return listaTipoObjecion;
	}
}
