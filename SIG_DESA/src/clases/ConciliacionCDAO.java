package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

public class ConciliacionCDAO {
	
	
	static final Logger log = Logger.getLogger(ConciliacionCDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private String esquema;
	
	public ConciliacionCDAO(){
		try {
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();

		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
		    log.error("Error al leer el archivo de propiedades.");
		    log.error(e.getMessage(), e);
		}	
    }
	
	public String listarConciliacionCPago(String fecDesde, String fecHasta){

		Connection conexion = null;
		db.OracleBD bdOracle = null;
		CallableStatement stmtProcedure = null;
		ResultSet resultados = null;
		ResultSet resultadosDev = null;
		ResultSet resultadosCom = null;
		ResultSet resultadosNcl = null;
		long cantTrx = 0;
		long montoTrx = 0;

		String listaCPago = "";
		String fecPDSDesde = fecDesde.split("/")[2].substring( 2,4 ) + "/" + fecDesde.split("/")[1] + "/" + fecDesde.split("/")[0];
		String fecPDSHasta = fecHasta.split("/")[2].substring( 2,4 ) + "/" + fecHasta.split("/")[1] + "/" + fecHasta.split("/")[0];
		bdOracle = new OracleBD();
		conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);

		try {
		    log.info( " *** Se llama al : SP_SIG_OBTENER_OBTENER_CC (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?) **** "  );
		    log.info( " desde : "+fecDesde+ " | hasta : "+fecHasta+ "  | desdePDS = "+fecPDSDesde+" |hastaPDS :"+fecPDSHasta);
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_OBTENER_OBTENER_CC(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); END;");

			stmtProcedure.setString("desde", fecDesde);
			stmtProcedure.setString("hasta", fecHasta);
			stmtProcedure.setString("desdePDS", fecPDSDesde);
			stmtProcedure.setString("hastaPDS", fecPDSHasta);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursorCec", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("prfCursorDev", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("prfCursorCom", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("prfCursorNcl", OracleTypes.CURSOR);
			stmtProcedure.registerOutParameter("cantidadoutnormales", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("cantidadoutpatpass", OracleTypes.NUMBER);

			stmtProcedure.execute();

			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursorCec"));
			resultadosDev = (ResultSet) ((stmtProcedure).getObject("prfCursorDev"));
			resultadosCom = (ResultSet) ((stmtProcedure).getObject("prfCursorCom"));
			resultadosNcl = (ResultSet) ((stmtProcedure).getObject("prfCursorNcl"));
			int codigoError = stmtProcedure.getInt("cod_error");
			int cantidadoutnormales       = stmtProcedure.getInt("cantidadoutnormales");
			int cantidadoutpatpass        = stmtProcedure.getInt("cantidadoutpatpass");
			
			if (codigoError==0){
				
				log.info("Method: buscarReporteVisa - Recorriendo resultados");
				
				while (resultados.next()) {
					listaCPago += resultados.getString("CANTIDAD") + "|";
					listaCPago += resultados.getString("TOTAL") + ";";
				}

				if (resultadosCom.next()) {
					cantTrx = cantTrx + resultadosCom.getLong("CANTIDAD");
					montoTrx = montoTrx + resultadosCom.getLong("TOTAL");
				}

				if (resultadosDev.next()) {
					cantTrx = cantTrx - resultadosDev.getLong("CANTIDAD");
					montoTrx = montoTrx - resultadosDev.getLong("TOTAL");
				}
						
				listaCPago += cantTrx + "|";
				listaCPago += montoTrx + ";";
				listaCPago += cantidadoutnormales+ "|";
				listaCPago += cantidadoutpatpass + ";";
				
				if(resultadosNcl.next()){
					listaCPago += resultadosNcl.getString("CANTIDAD") + "|";
					listaCPago += resultadosNcl.getString("TOTAL") + ";";
				}
			}else{
				log.warn("Method: buscarReporteVisa - Problemas al leer reporte visa desde la base de datos.");
				listaCPago="esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
			}
			listaCPago += "-" + fecDesde + "-" + fecHasta ;
			resultados.close();
			resultadosCom.close();
			resultadosDev.close();
			resultadosNcl.close();
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCPago = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
				 return listaCPago;
			}
			
			log.info("Method: buscarReporteVisa - " + listaCPago);
		} catch (SQLException e) {

			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.setResultado(resultadosCom);
			bdOracle.cerrarResultSet();
			bdOracle.setResultado(resultadosDev);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			if (bdOracle.cerrarConexionBD() == false) {
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaCPago = "esttrx|1~MSG|Problemas al leer reporte vissa desde la base de datos...~";
				return listaCPago;
			}
			log.error(e.getMessage(), e);
			
			listaCPago = "esttrx|1~MSG|Problemas al leer reporte visa desde la base de datos...~";
		} finally {
			if (bdOracle.cerrarResultSet() == false) {
				log.error("Error: No se pudo cerrar resulset.");
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			} else {
				if (bdOracle.cerrarConexionBD() == false) {
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}
			}
			try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error(e.getMessage(), e);
            }
			
		}

		return listaCPago;
	}
}
