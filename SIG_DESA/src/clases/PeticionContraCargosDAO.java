package clases;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

/**
 * Clase DAO que administra la informacion para peticion de vales y contra cargos.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class PeticionContraCargosDAO {

	static final Logger log =  Logger.getLogger(PeticionContraCargosDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private String esquema;
	
	public PeticionContraCargosDAO(){
		
		try {
			Properties propiedades = new Properties();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
			    log.error("Error al leer el archivo de propiedades.");
			    log.error(e.getMessage(),e);
    	}
		
	}
	
	/**
	 * Metodo que consulta las razones para peticion de vales y contracargos.
	 * @return  Lista de razones para peticiones de vales o contracargos.
	 * @throws Mensaje de excepcion controlado.
	 * @param xkey xkey a consultar.
	 */
	public String buscarRazonesPeticionContraCargos(String xkey){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		ResultSet resultados = null;

		String listaRazones="";
		
		try {
			log.info("Method: getListaTipoObjecion - Obtencion de parametros: xkye-> "+xkey);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: getListaTipoObjecion - Ejecutando SP_SIG_RAZONESPETICONTRACARGO");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_RAZONESPETICONTRACARGO(?, ?, ?, ?); END;");
			
			stmtProcedure.setString("vxkey", xkey); 
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);
			
			stmtProcedure.execute();
			
			//ResultSet resultados = (ResultSet) ((OracleCallableStatement)stmtProcedure).getObject("prfCursor");
			resultados = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
			
			if (resultados.next()){
						
						log.info("Method: getListaTipoObjecion - Obteniendo resultados");
						
						listaRazones="esttrx|0~";
						listaRazones += resultados.getInt("SID") + "|";
						listaRazones += resultados.getString("COD_MOTIVO_INI") + "|";
						listaRazones += resultados.getString("COD_MOTIVO_FIN") + "|";
						listaRazones += resultados.getString("DESCRIPCION") + "|";
						listaRazones += resultados.getInt("PLAZO") + "~";         				
						
						String strprint = "";
						strprint = resultados.getInt("SID") + " " + resultados.getString("COD_MOTIVO_INI") + " ";
						strprint+= resultados.getString("COD_MOTIVO_FIN") + " " + resultados.getString("DESCRIPCION") + " ";
						strprint+= resultados.getInt("PLAZO") + " ";

						while (resultados.next()) {
						
								listaRazones += resultados.getInt("SID") + "|";
								listaRazones += resultados.getString("COD_MOTIVO_INI") + "|";
								listaRazones += resultados.getString("COD_MOTIVO_FIN") + "|";
								listaRazones += resultados.getString("DESCRIPCION") + "|";
								listaRazones += resultados.getInt("PLAZO") + "~";         				
							
								strprint = resultados.getInt("SID") + " " + resultados.getString("COD_MOTIVO_INI") + " ";
								strprint+= resultados.getString("COD_MOTIVO_FIN") + " " + resultados.getString("DESCRIPCION") + " ";
								strprint+= resultados.getInt("PLAZO") + " ";
						}
						log.info("Method: getListaTipoObjecion - "+listaRazones);
			}else{
				log.info("Method: getListaTipoObjecion - No existe información con los parametros seleccionados");
				listaRazones="esttrx|1~MSG|No existe informacion con los parametros seleccionados.~";
			}
			
			resultados.close();
			stmtProcedure.close();
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaRazones = "esttrx|1~MSG|Problemas al leer las razones para peticon de vales y contracargos...~";
				 return listaRazones;
			}
			
		}catch (SQLException e){
			
			bdOracle.setResultado(resultados);
			bdOracle.cerrarResultSet();
			bdOracle.cerrarStatement(stmtProcedure);
			
			if (bdOracle.cerrarConexionBD()==false){
				log.error("Error: No se pudo cerrar conexion a la base de datos.");
				listaRazones =  "esttrx|1~MSG|Problemas al leer las razones para peticon de vales y contracargos...~";
			    return listaRazones;
			}
			
			log.error(e.getMessage(),e);
        	listaRazones =  "esttrx|1~MSG|Problemas al leer las razones para peticon de vales y contracargos...~";
        	
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error(e.getMessage(),e);
            }
    	}
		
		return listaRazones;
		
	}/* Fin buscarRazonesPeticionContraCargos*/
	
	/**
	 * Metodo que graba la peticion de vale contracargo.
	 * @return Mensaje de grabacion.
	 * @throws Mensaje de excepcion controlado.
	 * @param vxkey Xkey de la accion.
	 * @param vmit  Mit de la transaccion.
	 * @param sidtransaccion Sid de la transaccion. 
	 * @param codigorazon Codigo de razon.
	 * @param vmontoconciliacion Monto de conciliacion ingresado.
	 * @param sidusuario Sid del usuario.
	 */
	public String grabarPeticionValeContraCargos(String vxkey, String vmit, Integer sidtransaccion, String codigorazon, BigDecimal vmontoconciliacion, int sidusuario){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;

		try {
			
			log.info("Method: grabarPeticionValeContraCargos - Obtencion de parametros: vxkey-> "+vxkey+" | vmit-> "+vmit+" | sidtransaccion-> "+sidtransaccion+" | codigorazon-> "+codigorazon+" | vmontoconciliacion-> "+vmontoconciliacion+" | sidusuario-> "+sidusuario);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: grabarPeticionValeContraCargos - Ejecutando SP_SIG_GRABARPETICONTRACARGO");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_GRABARPETICONTRACARGO(?, ?, ?, ?, ?, ?, ?, ?, ?); END;");
			
			stmtProcedure.setString("vxkey", vxkey);
			stmtProcedure.setString("vmit", vmit);
			stmtProcedure.setInt("sidtransaccion", sidtransaccion); 
			stmtProcedure.setString("codigorazon", codigorazon);
			stmtProcedure.setBigDecimal("vmontoconciliacion", vmontoconciliacion); // Se tiene que ir con punto //
			stmtProcedure.setInt("sidusuario", sidusuario);
			stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("flag", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int error = stmtProcedure.getInt("cod_error");
			String warn = stmtProcedure.getString("warning");
			int flag = stmtProcedure.getInt("flag");
			
			log.info("error : " + error);
			log.info("warning : " + warn);
			log.info("flag"+flag);
			
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 return "esttrx|1~MSG|Problemas al grabar peticiones de vales, contracargos...~";
			}

			if (flag==0){
				log.info("Method: grabarPeticionValeContraCargos - La operacion ha sido realizada con exito.");
				return "esttrx|0~MSG|La operacion ha sido realizada con exito...~";
			}else{
				log.error("Method: grabarPeticionValeContraCargos - Problemas al grabar peticiones de vales, contracargos.");
				return "esttrx|1~MSG|Problemas al grabar peticiones de vales, contracargos...~";
			}

		}catch (SQLException e){     
			
			bdOracle.cerrarStatement(stmtProcedure);
			
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 return "esttrx|1~MSG|Problemas al grabar peticiones de vales, contracargos...~";
			}
			
			log.error(e.getMessage(),e);
        	return "esttrx|1~MSG|Problemas al grabar peticiones de vales, contracargos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error(e.getMessage(),e);
            }
    	}
		
		
	}/* Fin grabarPeticionValeContraCargos*/

}
