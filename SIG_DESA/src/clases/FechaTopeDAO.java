package clases;

import db.OracleBD;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.CallableStatement;
import oracle.jdbc.*;

import java.util.Properties;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
/**
 * Clase que permite rescatar la fecha tope.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class FechaTopeDAO {

    static final Logger log = Logger.getLogger(FechaTopeDAO.class);
	private String esquema;
	
	public FechaTopeDAO(){
		try {
			Properties propiedades = new Properties();
//			propiedades.load( getClass().getResourceAsStream("/properties/conexion.properties"));
			propiedades = PropertiesConfiguration.getProperty();
		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
		    log.error( e.getMessage(), e );
    	}
	}
	
	/**
	 * Metodo que obtiene la fecha tope.
	 * @return fecha tope.
	 * @throws Mensaje de excepcion controlado.
	 * @param sidtransaccion sid de la transaccion.
	 * @param vxkey xkey de la transaccion.
	 * @param codigorazon codigo de razon.
	 */
	public String obtenerFechaTope(int sidtransaccion, String vxkey, String codigorazon){
		
		db.OracleBD bdOracle = new OracleBD();
		Connection conexion = bdOracle.conectar();
		bdOracle.setConexion(conexion);
		try {
			log.info("==== Se ejecuta SP_SIG_calcFechaTope ====");
			log.info("Parametros de entrada :  SIDtran = " + sidtransaccion + " - XKEYaccTran = " + vxkey + " CODRAZON = " + codigorazon);
			CallableStatement stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_calcFechaTope(?, ?, ?, ?, ?, ?); END;");
			
			
			stmtProcedure.setInt("SIDtran", sidtransaccion); //Numero Tarjeta
			stmtProcedure.setString("XKEYaccTran", vxkey);
			stmtProcedure.setString("CODRAZON", codigorazon);
			
			stmtProcedure.registerOutParameter("FechaTope", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("FLAG", OracleTypes.NUMBER);
			stmtProcedure.registerOutParameter("ERROR_OUT", OracleTypes.VARCHAR);
			
			stmtProcedure.execute();
			
			String fechatope  = stmtProcedure.getString("FechaTope");
			int    flag       = stmtProcedure.getInt("FLAG");
			String error      = stmtProcedure.getString("ERROR_OUT");

			String listaFechaTope="";
			
			if (fechatope!="" && fechatope!=null && (flag==0 || flag==1)){
				
				listaFechaTope ="esttrx|0~";
				listaFechaTope += fechatope + "|";
				listaFechaTope += flag + "~";
				
			}else{
					listaFechaTope = "esttrx|1~MSG|Problemas al obtener fecha tope desde la base de datos...~";
			}
			
			stmtProcedure.close();
			
			if (bdOracle.cerrarResultSet()==false){
				 return "esttrx|1~MSG|Problemas al obtener fecha tope desde la base de datos...~";
			}
		
			if (bdOracle.cerrarConexionBD()==false){
				 return "esttrx|1~MSG|Problemas al obtener fecha tope desde la base de datos...~";
			}
			
			return listaFechaTope;
		}
		catch (SQLException e){          
		    log.error( e.getMessage(), e );
        		return "esttrx|1~MSG|Problemas al obtener fecha tope desde la base de datos...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 if (bdOracle.cerrarConexionBD()==false){
				     log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
				    log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
	}/* Fin obtenerFechaTope*/
	
}
