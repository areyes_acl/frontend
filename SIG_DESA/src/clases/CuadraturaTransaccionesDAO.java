package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import db.OracleBD;

/**
 * Clase DAO que consulta cuadratura de transacciones.
 * 
 * @version 1.0, 20/08/2013
 * @author  RADS
 */
public class CuadraturaTransaccionesDAO {
	
	static final Logger log = Logger.getLogger(CuadraturaTransaccionesDAO.class);
	static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
	
	private String esquema;
	
	public CuadraturaTransaccionesDAO(){
		try {
			Properties propiedades = new Properties();
			propiedades = PropertiesConfiguration.getProperty();
		    esquema = propiedades.getProperty("esquema");
		    
		}catch (Exception e){          
			    log.error("Error al leer el archivo de propiedades.");
			    log.error( e.getMessage(), e );
    	}
	}
	/**
	 * Metodo que consulta las cuadraturas de transacciones diarias.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso Fecha de sonulta de infromacion.
	 */
	public String buscarCuadraturaTransaccionDiaria(String fechaProceso, int operador){

		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		String listaCuadraturaDiaria="";

		try {
			log.info("Method: buscarCuadraturaTransaccionDiaria - Obteniendo parametros: fechaProceso-> "+fechaProceso);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			
			bdOracle.setConexion(conexion);
			
			
			
			log.info("Method: buscarCuadraturaTransaccionDiaria - Ejecutando SP_SIG_CUADRATURADIARIA");

			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_CUADRATURADIARIA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); END;");
			
			stmtProcedure.setString("fechaProceso", fechaProceso);
			
			stmtProcedure.registerOutParameter("l_compra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_1", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_1", OracleTypes.NUMBER);
	             
			stmtProcedure.registerOutParameter("l_creditos_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_2", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_2", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_avances_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_3", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_3", OracleTypes.NUMBER);
	           
			stmtProcedure.registerOutParameter("l_representaciones_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_4", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_4", OracleTypes.NUMBER);    
	          
			stmtProcedure.registerOutParameter("l_confirmvales_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_5", OracleTypes.NUMBER);           
			  
			stmtProcedure.registerOutParameter("l_compra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_1", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_1", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_creditos_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_2", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_2", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_avances_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_3", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_3", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_representaciones_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_4", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_4", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_miscelaneos_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_5", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_5", OracleTypes.NUMBER);
	       
			stmtProcedure.registerOutParameter("l_confirmvales_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_6", OracleTypes.NUMBER);           
			
	        
			stmtProcedure.registerOutParameter("l_1contra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_1con", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_1", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_2contra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_2con", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_2", OracleTypes.NUMBER);	
				     
			stmtProcedure.registerOutParameter("l_petval_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_petval", OracleTypes.NUMBER);           
			
			stmtProcedure.registerOutParameter("l_1contra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_1con_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_1_us", OracleTypes.NUMBER);	

			stmtProcedure.registerOutParameter("l_2contra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_2con_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_2_us", OracleTypes.NUMBER);
				
			stmtProcedure.registerOutParameter("l_misc_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_mis_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_mis_us", OracleTypes.NUMBER);	
				
			stmtProcedure.registerOutParameter("l_petval_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_petval_us", OracleTypes.NUMBER);           
				
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
				log.info("Method: buscarCuadraturaTransaccionDiaria - Obteniendo resultados.");
				listaCuadraturaDiaria = "esttrx|0~";
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_compra_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_1") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_1") + "|";
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_creditos_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_2") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_2") + "|";
		        
				listaCuadraturaDiaria += stmtProcedure.getString("l_avances_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_3") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_3") + "|";
			
				listaCuadraturaDiaria += stmtProcedure.getString("l_representaciones_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_4") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_4") + "|";
			
				listaCuadraturaDiaria += stmtProcedure.getString("l_confirmvales_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_5") + "|";
		          
				listaCuadraturaDiaria += stmtProcedure.getString("l_compra_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_1") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_1") + "|";          
				  
				listaCuadraturaDiaria += stmtProcedure.getString("l_creditos_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_2") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_2") + "|"; 
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_avances_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_3") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_3") + "|";
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_representaciones_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_4") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_4") + "|"; 
		        
				listaCuadraturaDiaria += stmtProcedure.getString("l_miscelaneos_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_5") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_5") + "|"; 
		        
				listaCuadraturaDiaria += stmtProcedure.getString("l_confirmvales_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_6") + "|";
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_1contra_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_1con") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_monto_1") + "|";
			          
				listaCuadraturaDiaria += stmtProcedure.getString("l_2contra_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_2con") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_monto_2") + "|";
		  
				listaCuadraturaDiaria += stmtProcedure.getString("l_petval_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_petval") + "|";
					     
				listaCuadraturaDiaria += stmtProcedure.getString("l_1contra_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_1con_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_monto_1_us") + "|";      
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_2contra_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_2con_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_monto_2_us") + "|"; 

				listaCuadraturaDiaria += stmtProcedure.getString("l_misc_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_mis_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_mis_us") + "|"; 
					
				listaCuadraturaDiaria += stmtProcedure.getString("l_petval_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_petval_us") + "~";
				
				log.info("Method: buscarCuadraturaTransaccionDiaria - "+listaCuadraturaDiaria);
			}else{
				listaCuadraturaDiaria="esttrx|1~MSG|Problemas al leer la cuadratura de transacciones diarias...~";
			}
			
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCuadraturaDiaria =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones diarias...~";
				 return listaCuadraturaDiaria;
			}
			
		}catch (SQLException e){  
			
				bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaCuadraturaDiaria =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones diarias...~";
					return listaCuadraturaDiaria;
				}
		
				log.error( e.getMessage(), e );
				listaCuadraturaDiaria =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones diarias...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaCuadraturaDiaria;
	}/* Fin buscarCuadraturaTransaccionDiaria*/
	
	/**
	 * Metodo que consulta las cuadraturas de transacciones diarias.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaProceso Fecha de sonulta de infromacion.
	 */
	public String buscarCuadraturaTransaccionDiariaVisa(String fechaProceso, int operador){

		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		String listaCuadraturaDiaria="";

		try {
			log.info("Method: buscarCuadraturaTransaccionDiaria - Obteniendo parametros: fechaProceso-> "+fechaProceso);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info(operador);
			log.info("Method: buscarCuadraturaTransaccionDiaria - Ejecutando SP_SIG_CUADRATURADIARIA_VISA");

			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_CUADRATURADIARIA_VISA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); END;");
			
			stmtProcedure.setString("fechaProceso", fechaProceso);
			//operador = Integer.parseInt(operador);
			stmtProcedure.setInt("FILTRO_OPERADOR", operador);
			
			stmtProcedure.registerOutParameter("l_compra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_1", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_1", OracleTypes.NUMBER);
	             
			stmtProcedure.registerOutParameter("l_creditos_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_2", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_2", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_avances_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_3", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_3", OracleTypes.NUMBER);
	           
			stmtProcedure.registerOutParameter("l_representaciones_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_4", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_4", OracleTypes.NUMBER);    
	          
			stmtProcedure.registerOutParameter("l_confirmvales_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_5", OracleTypes.NUMBER);           
			  
			stmtProcedure.registerOutParameter("l_compra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_1", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_1", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_creditos_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_2", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_2", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_avances_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_3", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_3", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_representaciones_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_4", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_4", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_miscelaneos_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_5", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_5", OracleTypes.NUMBER);
	       
			stmtProcedure.registerOutParameter("l_confirmvales_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_6", OracleTypes.NUMBER);           
			
	        
			stmtProcedure.registerOutParameter("l_1contra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_1con", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_1", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_2contra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_2con", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_2", OracleTypes.NUMBER);	
				     
			stmtProcedure.registerOutParameter("l_petval_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_petval", OracleTypes.NUMBER);           
			
			stmtProcedure.registerOutParameter("l_1contra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_1con_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_1_us", OracleTypes.NUMBER);	

			stmtProcedure.registerOutParameter("l_2contra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_2con_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_2_us", OracleTypes.NUMBER);
				
			stmtProcedure.registerOutParameter("l_misc_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_mis_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_mis_us", OracleTypes.NUMBER);	
				
			stmtProcedure.registerOutParameter("l_petval_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_petval_us", OracleTypes.NUMBER);           
				
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);

			stmtProcedure.execute();
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
				log.info("Method: buscarCuadraturaTransaccionDiaria - Obteniendo resultados.");
				listaCuadraturaDiaria = "esttrx|0~";
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_compra_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_1") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_1") + "|";
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_creditos_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_2") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_2") + "|";
		        
				listaCuadraturaDiaria += stmtProcedure.getString("l_avances_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_3") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_3") + "|";
			
				listaCuadraturaDiaria += stmtProcedure.getString("l_representaciones_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_4") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_4") + "|";
			
				listaCuadraturaDiaria += stmtProcedure.getString("l_confirmvales_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_clp_5") + "|";
		          
				listaCuadraturaDiaria += stmtProcedure.getString("l_compra_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_1") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_1") + "|";          
				  
				listaCuadraturaDiaria += stmtProcedure.getString("l_creditos_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_2") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_2") + "|"; 
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_avances_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_3") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_3") + "|";
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_representaciones_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_4") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_4") + "|"; 
		        
				listaCuadraturaDiaria += stmtProcedure.getString("l_miscelaneos_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_5") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_monto_us_5") + "|"; 
		        
				listaCuadraturaDiaria += stmtProcedure.getString("l_confirmvales_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_us_6") + "|";
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_1contra_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_1con") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_monto_1") + "|";
			          
				listaCuadraturaDiaria += stmtProcedure.getString("l_2contra_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_2con") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_monto_2") + "|";
		  
				listaCuadraturaDiaria += stmtProcedure.getString("l_petval_clp") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_petval") + "|";
					     
				listaCuadraturaDiaria += stmtProcedure.getString("l_1contra_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_1con_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_monto_1_us") + "|";      
				
				listaCuadraturaDiaria += stmtProcedure.getString("l_2contra_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_2con_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_monto_2_us") + "|"; 

				listaCuadraturaDiaria += stmtProcedure.getString("l_misc_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_mis_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getBigDecimal("l_outgoing_mis_us") + "|"; 
					
				listaCuadraturaDiaria += stmtProcedure.getString("l_petval_us") + "|";
				listaCuadraturaDiaria += stmtProcedure.getInt("l_qtrx_petval_us") + "~";
				
				log.info("Method: buscarCuadraturaTransaccionDiaria - "+listaCuadraturaDiaria);
			}else{
				listaCuadraturaDiaria="esttrx|1~MSG|Problemas al leer la cuadratura de transacciones diarias...~";
			}
			
			stmtProcedure.close();
		
			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCuadraturaDiaria =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones diarias...~";
				 return listaCuadraturaDiaria;
			}
			
		}catch (SQLException e){  
			
				bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaCuadraturaDiaria =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones diarias...~";
					return listaCuadraturaDiaria;
				}
		
				log.error( e.getMessage(), e );
				listaCuadraturaDiaria =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones diarias...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaCuadraturaDiaria;
	}/* Fin buscarCuadraturaTransaccionDiariaVisa*/
	/**************************************************************/
	/* Metodo que consulta la cuadratura mensual de transacciones */
	/**************************************************************/
	/**
	 * Metodo que consulta las cuadraturas de transacciones mensual.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaMensual Fecha mensual a consultar.
	 */
	public String buscarCuadraturaTransaccionMensual(String fechaMensual){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		String listaCuadraturaMensual="";
		
		try {
			log.info("Method: buscarCuadraturaTransaccionMensual - Obteniendo parametros: fechaMensual-> "+fechaMensual);
			
			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarCuadraturaTransaccionMensual - Ejecutando SP_SIG_CUADRATURAMENSUAL");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_CUADRATURAMENSUAL(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); END;");
			
			stmtProcedure.setString("fechaProceso", fechaMensual);
			
			stmtProcedure.registerOutParameter("l_compra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_1", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_1", OracleTypes.NUMBER);
	             
			stmtProcedure.registerOutParameter("l_creditos_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_2", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_2", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_avances_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_3", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_3", OracleTypes.NUMBER);
	           
			stmtProcedure.registerOutParameter("l_representaciones_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_4", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_4", OracleTypes.NUMBER);    
	          
			stmtProcedure.registerOutParameter("l_confirmvales_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_5", OracleTypes.NUMBER);           
			  
			stmtProcedure.registerOutParameter("l_compra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_1", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_1", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_creditos_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_2", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_2", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_avances_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_3", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_3", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_representaciones_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_4", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_4", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_miscelaneos_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_5", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_5", OracleTypes.NUMBER);
	       
			stmtProcedure.registerOutParameter("l_confirmvales_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_6", OracleTypes.NUMBER);           
			
	        
			stmtProcedure.registerOutParameter("l_1contra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_1con", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_1", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_2contra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_2con", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_2", OracleTypes.NUMBER);	
				     
			stmtProcedure.registerOutParameter("l_petval_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_petval", OracleTypes.NUMBER);           
			
			stmtProcedure.registerOutParameter("l_1contra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_1con_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_1_us", OracleTypes.NUMBER);	

			stmtProcedure.registerOutParameter("l_2contra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_2con_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_2_us", OracleTypes.NUMBER);
				
			stmtProcedure.registerOutParameter("l_misc_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_mis_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_mis_us", OracleTypes.NUMBER);	
				
			stmtProcedure.registerOutParameter("l_petval_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_petval_us", OracleTypes.NUMBER);           
				
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
				log.info("Method: buscarCuadraturaTransaccionMensual - Obteniendo resultados.");
				
				listaCuadraturaMensual = "esttrx|0~";
				
				listaCuadraturaMensual += stmtProcedure.getString("l_compra_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_1") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_1") + "|";
				
				listaCuadraturaMensual += stmtProcedure.getString("l_creditos_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_2") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_2") + "|";
		        
				listaCuadraturaMensual += stmtProcedure.getString("l_avances_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_3") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_3") + "|";
			
				listaCuadraturaMensual += stmtProcedure.getString("l_representaciones_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_4") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_4") + "|";
			
				listaCuadraturaMensual += stmtProcedure.getString("l_confirmvales_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_5") + "|";
		          
				listaCuadraturaMensual += stmtProcedure.getString("l_compra_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_1") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_1") + "|";          
				  
				listaCuadraturaMensual += stmtProcedure.getString("l_creditos_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_2") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_2") + "|"; 
				
				listaCuadraturaMensual += stmtProcedure.getString("l_avances_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_3") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_3") + "|";
				
				listaCuadraturaMensual += stmtProcedure.getString("l_representaciones_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_4") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_4") + "|"; 
		        
				listaCuadraturaMensual += stmtProcedure.getString("l_miscelaneos_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_5") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_5") + "|"; 
		        
				listaCuadraturaMensual += stmtProcedure.getString("l_confirmvales_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_6") + "|";
				
				listaCuadraturaMensual += stmtProcedure.getString("l_1contra_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_1con") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_monto_1") + "|";
			          
				listaCuadraturaMensual += stmtProcedure.getString("l_2contra_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_2con") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_monto_2") + "|";
		  
				listaCuadraturaMensual += stmtProcedure.getString("l_petval_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_petval") + "|";
					     
				listaCuadraturaMensual += stmtProcedure.getString("l_1contra_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_1con_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_monto_1_us") + "|";      
				
				listaCuadraturaMensual += stmtProcedure.getString("l_2contra_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_2con_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_monto_2_us") + "|"; 

				listaCuadraturaMensual += stmtProcedure.getString("l_misc_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_mis_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_mis_us") + "|"; 
					
				listaCuadraturaMensual += stmtProcedure.getString("l_petval_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_petval_us") + "~";
				
				log.info("Method: buscarCuadraturaTransaccionMensual - "+listaCuadraturaMensual);
			}else{
				log.error("Problemas al leer la cuadratura de transacciones mensual.");
				listaCuadraturaMensual="esttrx|1~MSG|Problemas al leer la cuadratura de transacciones mensual...~";
			}
			
			stmtProcedure.close();

			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCuadraturaMensual =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones mensual...~";
				 return listaCuadraturaMensual;
			}
			
		}catch (SQLException e){ 
			
				bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaCuadraturaMensual =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones mensual...~";
					return listaCuadraturaMensual;
				}
			
				log.error( e.getMessage(), e );
        		listaCuadraturaMensual = "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones mensual...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		
    		try {
                if(conexion != null && !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                log.error( e.getMessage(), e );
            }
    	}
		
		return listaCuadraturaMensual;
	}/* Fin buscarCuadraturaTransaccionMensual*/
	
	
	/**************************************************************/
	/* Metodo que consulta la cuadratura mensual de transacciones */
	/**************************************************************/
	/**
	 * Metodo que consulta las cuadraturas de transacciones mensual.
	 * @return Lista de acciones de incomming y outgoing totalizadas con sus montos correspondientes.
	 * @throws Mensaje de excepcion controlado.
	 * @param fechaMensual Fecha mensual a consultar.
	 */
	public String buscarCuadraturaTransaccionMensualVisa(String fechaMensual, int operador){
		
		Connection conexion=null;
		db.OracleBD bdOracle=null;
		CallableStatement stmtProcedure=null;
		String listaCuadraturaMensual="";
		
		try {
			log.info("Method: buscarCuadraturaTransaccionMensual - Obteniendo parametros: fechaMensual-> "+fechaMensual +"operador->" +operador);

			bdOracle = new OracleBD();
			conexion = bdOracle.conectar();
			bdOracle.setConexion(conexion);
			
			log.info("Method: buscarCuadraturaTransaccionMensual - Ejecutando SP_SIG_CUADRATURAMENSUAL_VISA");
			stmtProcedure = conexion.prepareCall("BEGIN " + esquema + ".SP_SIG_CUADRATURAMENSUAL_VISA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); END;");
			
			stmtProcedure.setString("fechaProceso", fechaMensual);

			stmtProcedure.setInt("FILTRO_OPERADOR", operador);
			log.info(operador);

			stmtProcedure.registerOutParameter("l_compra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_1", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_1", OracleTypes.NUMBER);
	             
			stmtProcedure.registerOutParameter("l_creditos_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_2", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_2", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_avances_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_3", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_3", OracleTypes.NUMBER);
	           
			stmtProcedure.registerOutParameter("l_representaciones_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_4", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_4", OracleTypes.NUMBER);    
	          
			stmtProcedure.registerOutParameter("l_confirmvales_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_clp_5", OracleTypes.NUMBER);           
			  
			stmtProcedure.registerOutParameter("l_compra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_1", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_1", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_creditos_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_2", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_2", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_avances_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_3", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_3", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_representaciones_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_4", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_4", OracleTypes.NUMBER);
	        
			stmtProcedure.registerOutParameter("l_miscelaneos_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_5", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_monto_us_5", OracleTypes.NUMBER);
	       
			stmtProcedure.registerOutParameter("l_confirmvales_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_us_6", OracleTypes.NUMBER);           
			
	        
			stmtProcedure.registerOutParameter("l_1contra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_1con", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_1", OracleTypes.NUMBER);
			
			stmtProcedure.registerOutParameter("l_2contra_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_2con", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_2", OracleTypes.NUMBER);	
				     
			stmtProcedure.registerOutParameter("l_petval_clp", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_petval", OracleTypes.NUMBER);           
			
			stmtProcedure.registerOutParameter("l_1contra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_1con_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_1_us", OracleTypes.NUMBER);	

			stmtProcedure.registerOutParameter("l_2contra_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_2con_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_monto_2_us", OracleTypes.NUMBER);
				
			stmtProcedure.registerOutParameter("l_misc_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_mis_us", OracleTypes.NUMBER);           
			stmtProcedure.registerOutParameter("l_outgoing_mis_us", OracleTypes.NUMBER);	
				
			stmtProcedure.registerOutParameter("l_petval_us", OracleTypes.VARCHAR);
			stmtProcedure.registerOutParameter("l_qtrx_petval_us", OracleTypes.NUMBER);           
				
			stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
			
			stmtProcedure.execute();
			
			int codigoError = stmtProcedure.getInt("cod_error");
			
			if (codigoError==0){
				
				log.info("Method: buscarCuadraturaTransaccionMensual - Obteniendo resultados.");
				
				listaCuadraturaMensual = "esttrx|0~";
				
				listaCuadraturaMensual += stmtProcedure.getString("l_compra_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_1") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_1") + "|";
				
				listaCuadraturaMensual += stmtProcedure.getString("l_creditos_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_2") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_2") + "|";
		        
				listaCuadraturaMensual += stmtProcedure.getString("l_avances_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_3") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_3") + "|";
			
				listaCuadraturaMensual += stmtProcedure.getString("l_representaciones_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_4") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_4") + "|";
			
				listaCuadraturaMensual += stmtProcedure.getString("l_confirmvales_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_clp_5") + "|";
		          
				listaCuadraturaMensual += stmtProcedure.getString("l_compra_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_1") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_1") + "|";          
				  
				listaCuadraturaMensual += stmtProcedure.getString("l_creditos_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_2") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_2") + "|"; 
				
				listaCuadraturaMensual += stmtProcedure.getString("l_avances_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_3") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_3") + "|";
				
				listaCuadraturaMensual += stmtProcedure.getString("l_representaciones_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_4") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_4") + "|"; 
		        
				listaCuadraturaMensual += stmtProcedure.getString("l_miscelaneos_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_5") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_monto_us_5") + "|"; 
		        
				listaCuadraturaMensual += stmtProcedure.getString("l_confirmvales_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_us_6") + "|";
				
				listaCuadraturaMensual += stmtProcedure.getString("l_1contra_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_1con") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_monto_1") + "|";
			          
				listaCuadraturaMensual += stmtProcedure.getString("l_2contra_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_2con") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_monto_2") + "|";
		  
				listaCuadraturaMensual += stmtProcedure.getString("l_petval_clp") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_petval") + "|";
					     
				listaCuadraturaMensual += stmtProcedure.getString("l_1contra_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_1con_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_monto_1_us") + "|";      
				
				listaCuadraturaMensual += stmtProcedure.getString("l_2contra_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_2con_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_monto_2_us") + "|"; 

				listaCuadraturaMensual += stmtProcedure.getString("l_misc_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_mis_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getBigDecimal("l_outgoing_mis_us") + "|"; 
					
				listaCuadraturaMensual += stmtProcedure.getString("l_petval_us") + "|";
				listaCuadraturaMensual += stmtProcedure.getInt("l_qtrx_petval_us") + "~";
				
				log.info("Method: buscarCuadraturaTransaccionMensual - "+listaCuadraturaMensual);
			}else{
				log.error("Problemas al leer la cuadratura de transacciones mensual.");
				listaCuadraturaMensual="esttrx|1~MSG|Problemas al leer la cuadratura de transacciones mensual...~";
			}
			
			stmtProcedure.close();

			if (bdOracle.cerrarConexionBD()==false){
				 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				 listaCuadraturaMensual =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones mensual...~";
				 return listaCuadraturaMensual;
			}
			
		}catch (SQLException e){ 
			
				bdOracle.cerrarStatement(stmtProcedure);
				if (bdOracle.cerrarConexionBD()==false){
					log.error("Error: No se pudo cerrar conexion a la base de datos.");
					listaCuadraturaMensual =  "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones mensual...~";
					return listaCuadraturaMensual;
				}
			
				log.error( e.getMessage(), e );
        		listaCuadraturaMensual = "esttrx|1~MSG|Problemas al leer la cuadratura de transacciones mensual...~";
		}finally{
    		if (bdOracle.cerrarResultSet()==false){
				 log.error("Error: No se pudo cerrar resulset.");
				 if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}			
			}else{
				if (bdOracle.cerrarConexionBD()==false){
					 log.error("Error: No se pudo cerrar conexion a la base de datos.");
				}	
			}
    		
    		try {
                if(conexion != null &&  !conexion.isClosed()){
                    conexion.close();
                }
            }
            catch ( SQLException e ) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    	}
		
		return listaCuadraturaMensual;
	}/* Fin buscarCuadraturaTransaccionMensualVisa*/
}
