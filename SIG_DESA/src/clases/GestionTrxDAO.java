/**
 * Descripcion: Clase que administra los cargos y abonos
 * @param SID de transaccion
 * @param listaCargoAbono
 * @return devuevle listaCargoAbono y updateCargoAbono
 */

package clases;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.TransactionType;
import weblogic.application.ApplicationException;
import Beans.TransaccionBean;
import cl.filter.FiltroTransacciones;
import cl.util.Utils;
import db.OracleBD;
import exception.AppException;

/**
 * 
 * @author afreire
 * 
 */
public class GestionTrxDAO {

    static final Logger log = Logger.getLogger(GestionTrxDAO.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    private String esquema;

    // PAGINACION
    private int ultimaPagina = 0;
    private int pagActual;
    private String cantidadRegistros = "";
    private String listaOperadores;
    private String listaProductos;

    // ATRIBUTOS PARA PAGINACION
    public int getUltimaPagina() {
	return ultimaPagina;
    }

    public int getPagActual() {
	return pagActual;
    }

    /**
     * Constructor
     */
    public GestionTrxDAO() {
	try {
	    Properties propiedades = new Properties();
	    propiedades = PropertiesConfiguration.getProperty();
	    esquema = propiedades.getProperty("esquema");
	    cantidadRegistros = propiedades.getProperty("cantidadRegistros");
	} catch (Exception e) {
	    log.error("Error al leer el archivo de propiedades.");
	    log.error(e.getMessage(), e);
	}
    }

    /**
     * Metodo que busca las transacciones pendientes de acuerdo a los filstros
     * seleeciones
     * 
     * @throws Exception
     */
    public List<TransaccionBean> buscarTrx(FiltroTransacciones filtro,
	    String numPagina) throws Exception {
	List<TransaccionBean> listaTrx = null;

	switch (filtro.getTipoBusqueda()) {
	case CON_PAGINACION:
	    listaTrx = busquedaPorPagina(filtro, numPagina);
	    break;

	case SIN_PAGINACION:
	    listaTrx = busquedaSinPaginacion(filtro);
	    break;

	}

	return listaTrx;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param filtro
     * @return
     * @throws ApplicationException
     * @since 1.X
     */
    private List<TransaccionBean> busquedaSinPaginacion(
	    FiltroTransacciones filtro) throws ApplicationException {

	CallableStatement stmtProcedure = null;
	ResultSet rs = null;
	OracleBD bdOracle = new OracleBD();
	Connection conexion = bdOracle.conectar();

	String storeProcedure = "BEGIN " + esquema
		+ ".SP_SIG_BUSCAR_TRX(?, ?, ?, ?, ?, ?, ?, ?, ?, ?); END;";

	bdOracle.setConexion(conexion);
	log.info("*** SE EJECUTA SP_SIG_BUSCAR_TRX(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ***");
	log.info(filtro);

	log.info("operador sin paginar: " + filtro.getOperador());

	try {

	    stmtProcedure = conexion.prepareCall(storeProcedure);
	    stmtProcedure.setString("p_fechaDesde", filtro.getFechaDesde());
	    stmtProcedure.setString("p_fechaHasta", filtro.getFechaHasta());
	    stmtProcedure.setString("p_xkey_estado", filtro.getEstado()
		    .getXkey());
	    stmtProcedure.setString("p_nro_tarjeta", filtro.getNumeroTarjeta());
	    stmtProcedure.setString("p_tipo_transac", filtro
		    .getTipoTransaccion().getXkey());
	    stmtProcedure.setLong("p_sid_user", filtro.getSidUsuario());
	    stmtProcedure.setString("FILTRO_OPERADOR", filtro.getOperador());
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
	    int codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");

	    if (codigoError == 0) {

		return parsearListaPerdida(rs);

	    } else {
		throw new ApplicationException(warning);
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	} finally {
	    cerrarConexiones(conexion, bdOracle);
	}

	return null;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     *
     * @param filtro
     * @param numPagina
     * @return
     * @throws ApplicationException
     * @throws SQLException
     * @since 1.X
     */
    private List<TransaccionBean> busquedaPorPagina(FiltroTransacciones filtro,
	    String numPagina) throws ApplicationException {

	CallableStatement stmtProcedure = null;
	ResultSet rs = null;
	OracleBD bdOracle = new OracleBD();
	Connection conexion = bdOracle.conectar();

	String storeProcedure = "BEGIN "
		+ esquema
		+ ".SP_SIG_BUSCAR_TRX_PG(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ); END;";

	// LOGICA PAGINACION
	if (numPagina == null) {
	    pagActual = 1;
	} else {
	    pagActual = Integer.parseInt(numPagina);
	}

	bdOracle.setConexion(conexion);
	log.info("*** SE EJECUTA SP_SIG_BUSCAR_TRX_PG(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ***");
	log.info(filtro);
	log.info("nro pagina : " + numPagina);
	log.info(filtro.getTipoTransaccion());
	log.info("operador por paginar: " + filtro.getOperador());

	try {
	    stmtProcedure = conexion.prepareCall(storeProcedure);
	    stmtProcedure.setString("p_fechaDesde", filtro.getFechaDesde());
	    stmtProcedure.setString("p_fechaHasta", filtro.getFechaHasta());
	    stmtProcedure.setString("p_xkey_estado", filtro.getEstado()
		    .getXkey());
	    stmtProcedure.setString("p_nro_tarjeta", filtro.getNumeroTarjeta());
	    stmtProcedure.setString("p_tipo_transac", filtro
		    .getTipoTransaccion().getXkey());
	    stmtProcedure.setLong("p_sid_user", filtro.getSidUsuario()); //
	    
	    stmtProcedure.setInt("numPagina", pagActual);
	    stmtProcedure
		    .setInt("cantReg", Integer.parseInt(cantidadRegistros));
	    stmtProcedure.setString("FILTRO_OPERADOR", filtro.getOperador());
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("totReg", OracleTypes.NUMBER);
	    stmtProcedure.registerOutParameter("prfCursor", OracleTypes.CURSOR);

	    stmtProcedure.execute();

	    rs = (ResultSet) ((stmtProcedure).getObject("prfCursor"));
	    int codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");
	    
	    
	    log.info(codigoError);
	    if (codigoError == 0) {
		float totReg = stmtProcedure.getInt("totReg");
		ultimaPagina = Utils.calcularUltimaPagina(totReg,
			Integer.parseInt(cantidadRegistros));
		return parsearListaPerdida(rs);
		

	    } else {
		throw new ApplicationException(warning);
		 
	    }
	  
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	} finally {
	    cerrarConexiones(conexion, bdOracle);
	}

	return null;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private List<TransaccionBean> parsearListaPerdida(ResultSet rs)
	    throws SQLException {
	List<TransaccionBean> listaTrx = new ArrayList<TransaccionBean>();

	while (rs.next()) {
	    TransaccionBean transaccion = new TransaccionBean();
	    transaccion.setSid(rs.getString("SID_TRANSACCION"));
	    transaccion.setNumeroTarjeta(rs.getString("NRO_TARJETA"));
	    transaccion.setFechaTransac(rs.getString("FECHATRANSAC"));
	    transaccion.setComercio(rs.getString("COMERCIO"));
	    transaccion.setPais(rs.getString("PAIS"));
	    transaccion.setMontoTransac(Utils.formateaMonto(rs
		    .getString("MONTO_TRANSAC")));
	    transaccion.setCodRazon(rs.getString("CODRAZON"));
	    transaccion.setSidEstado(rs.getString("ESTADO_PROCESO"));
	    transaccion.setDescEstado(rs.getString("DESC_ESTADO"));
	    transaccion.setNumIncidente(rs.getString("NUM_INCIDENTE"));
	    transaccion.setIdUsuario(rs.getString("USUARIO"));
	    transaccion.setFechaGestion(rs.getString("FECHA_GESTION"));
	    transaccion.setTipoTransc(rs.getString("TIPO_TRANSACCION"));
	    transaccion.setPatpass(rs.getString("PATPASS"));
	    transaccion.setOperador(rs.getString("OPERADOR"));
	    listaTrx.add(transaccion);

	}
	return listaTrx; 
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Cerrar conexiones
     * 
     * @param conexion
     * @param bdOracle
     * @throws SQLException
     * @throws ApplicationException
     * @throws AppException
     * @since 1.X
     */
    public void eliminarTransaccion(String sidTransaccion,
	    TransactionType tipoTrx) throws AppException {
	CallableStatement stmtProcedure = null;
	OracleBD bdOracle = new OracleBD();
	Connection conexion = bdOracle.conectar();

	try {
	    bdOracle.setConexion(conexion);
	    log.info("*** SE EJECUTA SP_SIG_DELETE_TRX_PEND(?, ?, ? ) ***");
	    log.info("SID_TRX :" + sidTransaccion);
	    log.info("tipoTrx :" + tipoTrx.getXkey());

	    stmtProcedure = conexion.prepareCall("BEGIN " + esquema
		    + ".SP_SIG_DELETE_TRX_PEND(?, ?, ?, ? ); END;");

	    stmtProcedure.setString("p_xkey_tipo_trx", tipoTrx.getXkey());
	    stmtProcedure.setString("p_sid_transaccion", sidTransaccion);
	    stmtProcedure.registerOutParameter("warning", OracleTypes.VARCHAR);
	    stmtProcedure.registerOutParameter("cod_error", OracleTypes.NUMBER);
	    stmtProcedure.execute();

	    int codigoError = stmtProcedure.getInt("cod_error");
	    String warning = stmtProcedure.getString("warning");

	    if (codigoError != 0) {
		throw new AppException(codigoError + " - " + warning);
	    } else {
		log.info("borrado correcto");
	    }

	} catch (SQLException e) {
	    throw new AppException(e);
	} finally {
	    cerrarConexiones(conexion, bdOracle);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Cerrar conexiones
     * 
     * @param conexion
     * @param bdOracle
     * @since 1.X
     */
    private void cerrarConexiones(Connection conexion, OracleBD bdOracle) {
	if (bdOracle.cerrarResultSet() == false) {
	    log.error("Error: No se pudo cerrar resulset.");
	    if (bdOracle.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	} else {
	    if (bdOracle.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	}
	try {
	    if (conexion != null && !conexion.isClosed()) {
		conexion.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/04/2019, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @throws AppException
     * @since 1.X
     */
    
    public String getListaProductos(int operador) {
	log.info("Method: getListaProductos - Obtencion de parametros: ninguno");

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs = null;

	bdOracle = new OracleBD();
	conexion = bdOracle.conectar();
	bdOracle.setConexion(conexion);

	log.info("Method: getListaProductos - Obteniendo productos");
	listaProductos = "esttrx:0~";
	rs = bdOracle.consultar("SELECT P.SID, P.BIN, P.COD_IC, P.NOMBRE FROM " + esquema
		+ ".TBL_PRODUCTO P INNER JOIN "+esquema+".TBL_OPERADOR_PRODUCTO OP ON (P.SID = OP.ID_PRODUCTO) WHERE OP.ID_OPERADOR = "+operador+" ORDER BY P.SID");
	bdOracle.setResultado(rs);

	try {

	    if (rs.next()) {
		log.info("Method: getListaProductos - Obteniendo resulstados");
		rs.previous();
		while (rs.next()) {
		    listaProductos += rs.getInt("SID") + "|";
		    listaProductos += rs.getString("BIN") + ("|");
		    listaProductos += rs.getString("COD_IC") + ("|");
		    listaProductos += rs.getString("NOMBRE") + "~";
		    
		}
		rs.close();
		log.info("Method: getListaProductos - " + listaProductos);
	    } else {
		listaProductos = "esttrx:1~No hay datos";
	    }
	    log.info("Method: getListaProductos - Serializando lista");
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    bdOracle.cerrarConexionBD();
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaProductos;
    }

    public String getListaOperadores() {
	log.info("Method: getListaOperadores - Obtencion de parametros: ninguno");

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs = null;

	bdOracle = new OracleBD();
	conexion = bdOracle.conectar();
	bdOracle.setConexion(conexion);

	log.info("Method: getListaOperadores - Obteniendo operadores");
	listaOperadores = "esttrx:0~";
	rs = bdOracle.consultar("SELECT SID, OPERADOR, NOMBRE FROM " + esquema
		+ ".TBL_OPERADOR WHERE ACTIVO = 1 ORDER BY SID");
	bdOracle.setResultado(rs);

	try {

	    if (rs.next()) {
		log.info("Method: getListaOperadores - Obteniendo resulstados");
		rs.previous();
		while (rs.next()) {
		    listaOperadores += rs.getInt("SID") + "|";
		    listaOperadores += rs.getString("NOMBRE") + ("|");
		    listaOperadores += rs.getString("OPERADOR") + "~";
		    
		}
		rs.close();
		log.info("Method: getListaOperadores - " + listaOperadores);
	    } else {
		listaOperadores = "esttrx:1~No hay datos";
	    }
	    log.info("Method: getListaOperadores - Serializando lista");
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    bdOracle.cerrarConexionBD();
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaOperadores;
    }
    
    public String getListOperadorSinTbk() {
	log.info("Method: getListaOperadores - Obtencion de parametros: ninguno");

	Connection conexion = null;
	db.OracleBD bdOracle = null;
	ResultSet rs = null;

	bdOracle = new OracleBD();
	conexion = bdOracle.conectar();
	bdOracle.setConexion(conexion);

	log.info("Method: getListaOperadores - Obteniendo operadores");
	listaOperadores = "esttrx:0~";
	rs = bdOracle.consultar("SELECT SID, NOMBRE FROM " + esquema
		+ ".TBL_OPERADOR WHERE ACTIVO = 1 AND OPERADOR != 'TBK' ORDER BY SID ");
	bdOracle.setResultado(rs);
//ORDER BY NOMBRE
	try {

	    if (rs.next()) {
		log.info("Method: getListaOperadores - Obteniendo resulstados");
		rs.previous();
		while (rs.next()) {
		    listaOperadores += rs.getInt("SID") + "|";
		    listaOperadores += rs.getString("NOMBRE") + ("~");
		}
		rs.close();
		log.info("Method: getListaOperadores - " + listaOperadores);
	    } else {
		listaOperadores = "esttrx:1~No hay datos";
	    }
	    log.info("Method: getListaOperadores - Serializando lista");
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    bdOracle.cerrarConexionBD();
	} finally {
	    if (bdOracle.cerrarResultSet() == false) {
		log.error("Error: No se pudo cerrar resulset.");
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    } else {
		if (bdOracle.cerrarConexionBD() == false) {
		    log.error("Error: No se pudo cerrar conexion a la base de datos.");
		}
	    }
	    try {
		if (conexion != null && !conexion.isClosed()) {
		    conexion.close();
		}
	    } catch (SQLException e) {
		log.error(e.getMessage(), e);
	    }
	}

	return listaOperadores;
    }

}