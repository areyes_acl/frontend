package types;

/**
 * 
 * @author dmedina
 * 
 *         Enum que contiene los estados de un pago
 */
public enum CargoAbonoStatusType {

	TODOS(0,"TDO","TODOS"),
	PENDIENTE(6, "CA_PEN", "PENDIENTE SOL60"), 
	ENVIADO(7, "CA_OK", "ENVIADO SOL60");

	private final Integer sid;
	private final String xkey;
	private final String description;

	private CargoAbonoStatusType(Integer sid, String xkey, String description) {
		this.sid = sid;
		this.xkey = xkey;
		this.description = description;
	}

	public Integer getSid() {
		return sid;
	}

	public String getXkey() {
		return xkey;
	}

	public String getDescription() {
		return this.description;
	}

	public static CargoAbonoStatusType findByXkey(String xkey) {
		for (CargoAbonoStatusType stats : CargoAbonoStatusType.values()) {
			if (stats.getXkey().contains(xkey)) {
				return stats;
			}
		}
		return null;
	}
	
	public static CargoAbonoStatusType findById(Integer sid) {
		for (CargoAbonoStatusType stats : CargoAbonoStatusType.values()) {
			if (stats.getSid().compareTo(sid) == 0) {
				return stats;
			}
		}
		return null;
	}

}