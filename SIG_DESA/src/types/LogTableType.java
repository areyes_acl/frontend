package types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public enum LogTableType {

    ERROR("LOG_ERROR", "SID,FECHA_INSERCION,NOMBRE_SP,MSG_ERROR"),
    EXCEPT_INCOMING("TBL_EXCEPT_INCOMING", "SID, LOG_CARGA, FECHA_INCOMING, MIT, NRO_TARJETA, CODIGO_FUNCION"),
    BOL("TBL_LOG_BOL", "SID,FECHA,FILENAME,FILE_FLAG,FILE_TS_DOWNLOAD,FILE_TS_UPLOAD"),
    CARGO_ABONO("TBL_LOG_CARG_ABO","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    COMISION("TBL_LOG_COMISION","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    CONTABILIDAD("TBL_LOG_CONTABLE","SID, FILE_TS, FILE_NAME, FILE_FLAG, FECHA"),
    CPAGOS("TBL_LOG_CPAGO","SID, FILE_TS, FILE_NAME, FILE_FLAG, FECHA"),
    INCOMING("TBL_LOG_INC","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    ONUS("TBL_LOG_ONUS","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    OUTGOING("TBL_LOG_OUT","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    ONUS_OUT("TBL_LOG_OUT_ONUS","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    VISA_OUT("TBL_LOG_OUT_VISA","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    VISA_INT_OUT("TBL_LOG_OUT_VISA_INT", "SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    RECHAZO("TBL_LOG_RCH","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    LOG_TRX_AUTORIZADAS("TBL_LOG_TRANSAC_AUTOR","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    VISA("TBL_LOG_VISA","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS"),
    VISA_INT("TBL_LOG_VISA_INT","SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS")
    ;

    private final String tableName;
    private final String tableFields;

    private LogTableType(String tableName, String tableFields) {
	this.tableName = tableName;
	this.tableFields = tableFields;
    }

    public String getTableName() {
	return this.tableName;
    }

    public String getTableFields() {
	return this.tableFields;
    }
    
    
    public static LogTableType getFromLabelValue( String pValue ) {
        
        for ( LogTableType accionType : LogTableType.values() ) {
            if ( null != pValue && accionType.tableName.equalsIgnoreCase( pValue ) ) {
                return accionType;
            }
        }
        
        return null;
        
    }

}
