package types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public enum EstadoType {

    INACTIVO("INACTIVO", 0),
    ACTIVO("ACTIVO", 1);

    private final String label;
    private final Integer position;

    private EstadoType(String label, Integer position) {
	this.label = label;
	this.position = position;

    }

    public String getLabel() {
	return label;
    }

    public Integer getPosition() {
	return position;
    }

    public int getValue() {
	return ordinal();
    }

    public static EstadoType fromOrdinal(int ordinal) {

	EstadoType response = null;
	for (EstadoType type : EstadoType.values()) {

	    if (type.ordinal() == ordinal)
		response = type;
	}
	return response;
    }
}