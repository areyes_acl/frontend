package types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * Enum que contine los tipos de moneda con su respetivo codigo iso y numero de
 * moneda, adem�s de una descripcion
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public enum MoneyType {

	DOLAR("840", "USD", "Dolar Estadounidense"),
	PESO_CHILENO("152", "CLP","Peso Chileno");

	private final String numero;
	private final String isoCode;
	private final String descripcion;

	private MoneyType(String numero, String isoCode, String descripcion) {
		this.numero = numero;
		this.isoCode = isoCode;
		this.descripcion = descripcion;
	}

	public String getNumero() {
		return numero;
	}

	public String getIsoCode() {
		return isoCode;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public static MoneyType findByNumero(String numero) {
		for (MoneyType stats : MoneyType.values()) {
			if (stats.getNumero().equalsIgnoreCase(numero)) {
				return stats;
			}
		}
		return null;
	}

	public static MoneyType findByIsoCode(String isoCode) {
		for (MoneyType stats : MoneyType.values()) {
			if (stats.getIsoCode().equalsIgnoreCase(isoCode)) {
				return stats;
			}
		}
		return null;
	}

}
