package types;

/**
 * 
 * @author dmedina
 * 
 *         Enum que contiene los estados de un pago
 */
public enum StatusTransactionType {

    	INGRESADO("ING", "INGRESADO","3"),
	PENDIENTES("OU_PEN", "PENDIENTE","4"), 
	ENVIADOS("OU_OK", "ENVIADO","5"),
	TODOS("TDO", "TODO","0"),
	;

	private final String xkey;
	private final String description;
	private final String sid;

	private StatusTransactionType(String xkey, String description, String sid) {
		this.xkey = xkey;
		this.description = description;
		this.sid = sid;
	}

	public String getDescription() {
		return this.description;
	}

	public String getXkey() {
		return xkey;
	}
	
	public String getSid() {
		return sid;
	}

	public static StatusTransactionType findByXkey(String xkey) {
		for (StatusTransactionType stats : StatusTransactionType.values()) {
			if (stats.getXkey().contains(xkey)) {
				return stats;
			}
		}
		return null;
	}
	
	public static StatusTransactionType findBySid(String sid) {
		for (StatusTransactionType stats : StatusTransactionType.values()) {
			if (stats.getSid().contains(sid)) {
				return stats;
			}
		}
		return null;
	}
	
	

}