package types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public enum FileType {
    INCOMING("INCOMING", "PATH_ABC_INC_BKP","FORMAT_FILENAME_INC"),
    RECHAZOS("RECHAZOS", "PATH_ABC_RCH_BKP","FORMAT_FILE_ABC_RCH"),
    ONUS("ONUS", "PATH_ABC_ONUS_BKP","FORMAT_FILENAME_ONUS"),
    LOG_AUTORIZACION("LOG_AUTORIZACION", "PATH_ABC_LOG_TRX_BKP","FORMAT_FILE_ABC_LOG"),
    CARGO_ABONO("CARGO_ABONO", "PATH_CAR_ABO_BKP", "FORMAT_FILENAME_CAR_ABO"),
    CONTABILIDAD("CONTABILIDAD", "PATH_TRX_CONTABLE_BKP","FORMAT_FILE_ACCOUNT_FILE"),
    COMISIONES("COMISIONES", "PATH_TBK_COM_BKP","FORMAT_FILE_TBK_COM"), 
    OUTGOING("OUTGOING", "PATH_ACL_INC_BKP","FORMAT_FILENAME_OUT"),
    NCL("NCL","PATH_CPAGO_BKP","FORMAT_FILENAME_CPAGO"),
    BOL("BOL","PATH_BOL_BKP","FORMAT_FILENAME_BOL"),
    INCOMING_VN("INCOMING_VN", "PATH_OUT_VISA_BKP","FORMAT_FILENAME_INC_VISA"),
    INCOMING_VI("INCOMING_VI", "PATH_OUT_VISA_INT_BKP","FORMAT_FILENAME_INC_VISA_INT"),
    OUTGOING_VN("OUTGOING_VN", "PATH_ACL_INC_VISA_NAC_BKP", "FORMAT_FILENAME_OUT_VISA_NAC"),
    OUTGOING_VI("OUTGOING_VI", "PATH_ACL_INC_VISA_INT_BKP", "FORMAT_FILENAME_OUT_VISA_INT"),

    ;

    private final String label;
    private final String codigoRutaArchivo;
    private final String codigoStarWith;


    

    private FileType(String label, String codigoRutaArchivo,
	    String codigoStarWith) {
	this.label = label;
	this.codigoRutaArchivo = codigoRutaArchivo;
	this.codigoStarWith = codigoStarWith;
    }

    public String getLabel() {
	return this.label;
    }

    public String getCodigoRutaArchivo() {
        return codigoRutaArchivo;
    }

    public String getCodigoStarWith() {
        return codigoStarWith;
    }

    public int getValue() {
	return ordinal();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     * 
     * @param p_value
     * @return
     * @since 1.X
     */
    public static FileType getFileTypeBy( String pValue ) {
        
        for ( FileType fileType : FileType.values() ) {
            if ( null != pValue && fileType.label.equalsIgnoreCase( pValue ) ) {
                return fileType;
            }
        }
        
        return null;
        
    }

}