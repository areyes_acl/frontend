package types;

import java.io.Serializable;

/**
 * 
 * @author dmedina
 * 
 */
public enum OnusType implements IType,Serializable {

	ONUS_TOTAL("ONUS","Cantidad total de compras Onus", "Monto total de compras Onus"),
	ONUS_CHIP("ONUS","Cantidad tarjetas de microcircuito","Monto tarjetas de microcircuito"),
	ONUS_COMERCIO_ELECTRONICO("ONUS","Cantidad comercio electonico","Monto comercio electonico"),
	ONUS_SERVICIOS_BANCARIOS("ONUS","Cantidad servicios bancarios","Monto servicios bancarios"),
	ONUS_TIENDAS_CLIENTE("ONUS","Cantidad tiendas cliente","Monto tiendas cliente"),
	ONUS_DISPOSICION_EFECTIVO_EN_CAJA("ONUS","Cantidad disposiciones efectivo","Monto disposiciones efectivo"),
	ONUS_DISPOSICION_EFECTIVO_CAJERO("ONUS","Cantidad disposiciones efectivo en cajero","Monto disposiciones efectivo en cajero")
	;
	
	private String clasificacion;
	private String descripcionCant;
	private String descripcionMont;
	
	public String getClasificacion() {
		return this.clasificacion;
	}

	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	public String getDescripcionCant() {
		return descripcionCant;
	}

	public void setDescripcionCant(String descripcionCant) {
		this.descripcionCant = descripcionCant;
	}

	public String getDescripcionMont() {
		return descripcionMont;
	}

	public void setDescripcionMont(String descripcionMont) {
		this.descripcionMont = descripcionMont;
	}

	public int getValue() {
		return ordinal();
	}
	
	

	private OnusType(String clasificacion,String descripcionCant, String descripcionMont) {
		this.clasificacion = clasificacion;
		this.descripcionCant = descripcionCant;
		this.descripcionMont = descripcionMont;
	}
	
	

}
