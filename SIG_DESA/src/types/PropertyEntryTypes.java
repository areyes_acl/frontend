package types;

public enum PropertyEntryTypes {
   
    //sbpay
    JNDI("sbpay.cron.jndi")
    ;
    
    
    /**
     * Valor ENUM.
     */
    private final String key;

    /**
     * Constructor.
     * 
     * @param text
     */
    private PropertyEntryTypes(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
    
    


}
