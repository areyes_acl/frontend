package types;

/**
 * 
 * @author dmedina
 *
 */
public enum NacionalType implements IType {
	
	
	NACIONAL_TOTAL("Cantidad total de compras Nacionales","Monto total de compras Nacionales"),
	NACIONAL_CHIP("Cantidad tarjetas de microcircuito","Monto tarjetas de microcircuito"),
	NACIONAL_COMERCIO_ELECTRONICO("Cantidad comercio electonico","Monto comercio electonico"),
	NACIONAL_DISPOSICION_EFECTIVO_EN_CAJA("Cantidad disposiciones efectivo","Monto disposiciones efectivo"),
	NACIONAL_DISPOSICION_EFECTIVO_CAJERO("Cantidad disposiciones efectivo en cajero","Monto disposiciones efect. en cajero");
	
	private String descripcionCant;
	private String descripcionMonto;
	
	public String getDescripcionCant() {
		return descripcionCant;
	}
	
	public String getDescripcionMonto() {
		return descripcionMonto;
	}

	private NacionalType(String descripcionCant, String descripcionMonto) {
		this.descripcionCant = descripcionCant;
		this.descripcionMonto = descripcionMonto;
	}
	
	public int getValue() {
		return ordinal();
	}

	

}