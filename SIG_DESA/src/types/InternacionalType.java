package types;

/**
 * 
 * @author dmedina
 *
 */
public enum InternacionalType implements IType {
	
	
	INTERNACIONAL_TOTAL("Cant. total de compras Internacionales","Monto total de compras Internacionales"),
	INTERNACIONAL_CHIP("Cant. tarjetas de microcircuito","Monto tarjetas de microcircuito"),
	INTERNACIONAL_COMERCIO_ELECTRONICO("Cant. comercio electonico","Monto comercio electonico"),
	INTERNACIONAL_DISPOSICION_EFECT_EN_CAJA("Cant. disposiciones efectivo","Monto disposiciones efectivo"),
	INTERNACIONAL_DISPOSICION_EFECT_CAJERO("Cant. disposiciones efect. en cajero","Monto disposiciones efect. en cajero");
	
	private String descripcionCant;
	private String descripcionMont;

	public String getDescripcionCant() {
		return descripcionCant;
	}

	public void setDescripcionCant(String descripcionCant) {
		this.descripcionCant = descripcionCant;
	}

	public String getDescripcionMont() {
		return descripcionMont;
	}

	public void setDescripcionMont(String descripcionMont) {
		this.descripcionMont = descripcionMont;
	}

	private InternacionalType(String descripcionCant, String descripcionMont) {
		this.descripcionCant = descripcionCant;
		this.descripcionMont = descripcionMont;
	}
	
	public int getValue() {
		return ordinal();
	}


}