package types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2015, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * Tipo de estado 1: ACTIVO , 0 : INACTIVO
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public enum StatusType {
    INACTIVO, ACTIVO;
    
    public int getValue() {
        return ordinal();
    }
    
    public static StatusType fromOrdinalValue( int value )
            throws IllegalArgumentException {
        try {
            return StatusType.values()[value];
        }
        catch ( ArrayIndexOutOfBoundsException e ) {
            throw new IllegalArgumentException( "Unknown enum value :" + value );
        }
    }
    
}
