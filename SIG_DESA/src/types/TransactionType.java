package types;

/**
 * 
 * @author
 * 
 */
public enum TransactionType {

	PRIMER_CONTRACARGO("15_00", "PRIMER CONTRACARGO"), 
	SEGUNDO_CONTRACARGO("15_205", "SEGUNDO CONTRACARGO"), 
	PETICION_VALE("52_00", "PETICION DE VALE"),
	RECHAZO("REC", "RECHAZO"),
	OBJECION("OBJ", "OBJECION"),
	TODOS("00_00", "TODOS")
	;

	private final String xkey;
	private final String description;

	private TransactionType(String xkey, String description) {
		this.xkey = xkey;
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}

	public String getXkey() {
		return xkey;
	}

	public static TransactionType findByXkey(String xkey) {
		for (TransactionType stats : TransactionType.values()) {
			if (stats.getXkey().contains(xkey)) {
				return stats;
			}
		}
		return null;
	}

}
