package types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public enum SearchType {

    SIN_PAGINACION( 0 , "B�squeda todos los resultados sin paginar."), 
    CON_PAGINACION( 1 , "B�squeda con paginaci�n.");
    

    private final Integer valor;
    private final String label;

    private SearchType(Integer valor, String label) {
	this.label = label;
	this.valor = valor;
    }

    public String getLabel() {
	return label;
    }

    public Integer getValor() {
	return valor;
    }

}
