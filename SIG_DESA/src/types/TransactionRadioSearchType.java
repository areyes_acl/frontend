package types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2015, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * Enum que contiene los tipos de b�squeda para los combo box de
 * transacciones
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public enum TransactionRadioSearchType {
    GESTION("GESTION"), CONSULTA("CONSULTA");
    
    private final String label;
    
    private TransactionRadioSearchType( String label ) {
        this.label = label;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public int getValue() {
        return ordinal();
    }
}
