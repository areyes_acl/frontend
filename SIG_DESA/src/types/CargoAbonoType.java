package types;

public enum CargoAbonoType {
	TODOS(0, "TDO"), CARGO(1, "CARGO"), ABONO(2, "ABONO");

	private final Integer sid;
	private final String xkey;

	private CargoAbonoType(Integer sid, String xkey) {
		this.sid = sid;
		this.xkey = xkey;

	}

	public String getXkey() {
		return xkey;
	}

	public Integer getSid() {
		return this.sid;
	}

	public static CargoAbonoType findByXkey(String xkey) {
		for (CargoAbonoType stats : CargoAbonoType.values()) {
			if (stats.getXkey().contains(xkey)) {
				return stats;
			}
		}
		return null;
	}
	
	public static CargoAbonoType findById(Integer sid) {
		for (CargoAbonoType stats : CargoAbonoType.values()) {
			if (stats.getSid().compareTo(sid) == 0) {
				return stats;
			}
		}
		return null;
	}

}
