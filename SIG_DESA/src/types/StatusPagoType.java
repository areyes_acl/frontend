package types;

/**
 * 
 * @author dmedina
 * 
 *         Enum que contiene los estados de un pago
 */
public enum StatusPagoType {

	INGRESADO("ING", "INGRESADO"), RECHAZADO("RCH", "RECHAZADO"), AUTORIZADO(
			"AUT", "AUTORIZADO"), TODOS(
					"TDO", "TODOS");

	private final String xkey;
	private final String description;

	private StatusPagoType(String xkey, String description) {
		this.xkey = xkey;
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}

	public String getXkey() {
		return xkey;
	}

	public static StatusPagoType findByXkey(String xkey) {
		for (StatusPagoType stats : StatusPagoType.values()) {
			if (stats.getXkey().contains(xkey)) {
				return stats;
			}
		}
		return null;
	}

}