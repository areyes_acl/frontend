package types;

public enum VentaType {
	
	VENTA_NORMAL(1,"Venta Normal"),
	VENTA_CARGO_INMEDIATO(2,"Venta de cargo inmediato"),
	VENTA_POR_CONVENIO_CUOTA_NORMAL(5,"Venta por convenio de Cuota Normal"),
	VENTA_POR_TRES_CUOTAS_SIN_INTERES(6,"Venta por 3 cuotas sin intereses"),
	VENTA_POR_CONVENIO_INTERES_UNICO(8,"Venta por convenio de cuotas con tasa de inter�s �nica"),
	PAGO_IMPUESTO_FISCAL(9,"Pago de Impuestos Fiscal");
	
	private final Integer codigo;
	private final String descripcion;

	private VentaType(Integer codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	/**
	 * M�todo que retorna el 
	 * @param p_value
	 * @return
	 */
	public static VentaType getVentaTypefromCode(Integer p_value) {

		for (VentaType ventaType : VentaType.values()) {
			if (null != p_value && ventaType.codigo == p_value) {
				return ventaType;
			}
		}

		return null;

	}

}
