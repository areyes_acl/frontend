package types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2015, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public enum AccionType {
    ENTRADA("E"), SALIDA("S"), NUEVO("N"), ACTUALIZACION("A");
    
    private final String label;
    
    private AccionType( String label ) {
        this.label = label;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public int getValue() {
        return ordinal();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (ACL Chile) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que dado un label se busca y el enum correspondiente y
     * se retorna
     * 
     * @param p_value
     *            Valor a buscar en el enum
     * @return
     * @since 1.X
     */
    public static AccionType getTypefromLabelValue( String p_value ) {
        
        for ( AccionType accionType : AccionType.values() ) {
            if ( null != p_value && accionType.label.equalsIgnoreCase( p_value ) ) {
                return accionType;
            }
        }
        
        return null;
        
    }
    
}
