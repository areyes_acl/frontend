package mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import types.LogTableType;
import Beans.RegistroLog;
import Beans.RegistroLogBOL;
import Beans.RegistroLogError;
import Beans.RegistroLogExeptIncoming;
import Beans.RegistroLogGeneric;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class MapperRegistroLog {

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param resultados
     * @param logTable
     * @return
     * @throws SQLException
     * @since 1.X
     */
    public RegistroLog mapperResultset(ResultSet resultados,
	    LogTableType logTable) throws SQLException {

	RegistroLog registro;

	switch (logTable) {
	case ERROR:
	    registro = parsearTablaLogError(resultados, logTable);
	    break;

	case EXCEPT_INCOMING:
	    registro = parsearTablaExeptIncoming(resultados, logTable);
	    break;

	case BOL:
	    registro = parsearTablaLogBol(resultados, logTable);
	    break;

	default:
	    registro = parsearTablaGenerica(resultados, logTable);
	    break;
	}

	return registro;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param resultados
     * @param logTable
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private RegistroLog parsearTablaGenerica(ResultSet resultados,
	    LogTableType logTable) throws SQLException {

	// Extrae campos que se deber�n parsear
	String campos[] = logTable.getTableFields().split(",");
	RegistroLogGeneric registro = new RegistroLogGeneric();

	// "SID,FECHA,FILENAME,FILE_FLAG,FILE_TS_DOWNLOAD,FILE_TS_UPLOAD"
	registro.setSid(resultados.getLong(campos[0].trim()));
	registro.setFecha(resultados.getString(campos[1].trim()));
	registro.setFilename(resultados.getString(campos[2].trim()));
	registro.setFileFlag(resultados.getString(campos[3].trim()));
	registro.setFileTs(resultados.getString(campos[4].trim()));

	return registro;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param resultados
     * @param logTable
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private RegistroLog parsearTablaExeptIncoming(ResultSet resultados,
	    LogTableType logTable) throws SQLException {
	RegistroLogExeptIncoming registro = new RegistroLogExeptIncoming();

	// Extrae campos que se deber�n parsear
	String campos[] = logTable.getTableFields().split(",");

	// "SID,FECHA,FILENAME,FILE_FLAG,FILE_TS_DOWNLOAD,FILE_TS_UPLOAD"
	registro.setSid(resultados.getLong(campos[0].trim()));
	registro.setLogCarga(resultados.getString(campos[1].trim()));
	registro.setFechaIncoming(resultados.getString(campos[2].trim()));
	registro.setMit(resultados.getString(campos[3].trim()));
	registro.setNumeroTarjeta(resultados.getString(campos[4].trim()));
	registro.setCodigoFuncion(resultados.getString("CODIGO_FUNCION"));
	registro.setCodigoAutorizacion(resultados.getString("CODIGO_AUTORIZACION"));

	return registro;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param resultados
     * @param logTable
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private RegistroLog parsearTablaLogBol(ResultSet resultados,
	    LogTableType logTable) throws SQLException {
	RegistroLogBOL registroLogBol = new RegistroLogBOL();

	// Extrae campos que se deber�n parsear
	String campos[] = logTable.getTableFields().split(",");

	// "SID,FECHA,FILENAME,FILE_FLAG,FILE_TS_DOWNLOAD,FILE_TS_UPLOAD"
	registroLogBol.setSid(resultados.getLong(campos[0].trim()));
	registroLogBol.setFecha(resultados.getString(campos[1].trim()));
	registroLogBol.setFilename(resultados.getString(campos[2].trim()));
	registroLogBol.setFileFlag(resultados.getString(campos[3].trim()));
	registroLogBol
		.setFileTsDownload(resultados.getString(campos[4].trim()));
	registroLogBol.setFileTsUpload(resultados.getString(campos[5].trim()));

	return registroLogBol;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param logTable
     * @param resultados
     * 
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private RegistroLog parsearTablaLogError(ResultSet resultados,
	    LogTableType logTable) throws SQLException {
	RegistroLogError registroLogError = new RegistroLogError();

	// Extrae campos que se deber�n parsear
	String campos[] = logTable.getTableFields().split(",");

	registroLogError.setSid(resultados.getLong(campos[0].trim()));
	registroLogError.setFechaInsercion(resultados.getString(campos[1]
		.trim()));
	registroLogError.setNombreSp(resultados.getString(campos[2].trim()));
	registroLogError.setMsgError(resultados.getString(campos[3].trim()));

	return registroLogError;
    }
}
