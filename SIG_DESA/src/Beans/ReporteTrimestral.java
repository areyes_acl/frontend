package Beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ReporteTrimestral implements Serializable,
	Comparable<ReporteTrimestral> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String fechaTrimestre;
    private List<DetalleTrimestral> detalleOnus = new ArrayList<DetalleTrimestral>();
    private List<DetalleTrimestral> detalleNacional = new ArrayList<DetalleTrimestral>();
    private List<DetalleTrimestral> detalleInternacional = new ArrayList<DetalleTrimestral>();

    public String getFechaTrimestre() {
	return fechaTrimestre;
    }

    public void setFechaTrimestre(String fechaTrimestre) {
	this.fechaTrimestre = fechaTrimestre;
    }

    public List<DetalleTrimestral> getDetalleOnus() {
	return detalleOnus;
    }

    public void setDetalleOnus(List<DetalleTrimestral> detalleOnus) {
	this.detalleOnus = detalleOnus;
    }

    public List<DetalleTrimestral> getDetalleNacional() {
	return detalleNacional;
    }

    public void setDetalleNacional(List<DetalleTrimestral> detalleNacional) {
	this.detalleNacional = detalleNacional;
    }

    public List<DetalleTrimestral> getDetalleInternacional() {
	return detalleInternacional;
    }

    public void setDetalleInternacional(
	    List<DetalleTrimestral> detalleInternacional) {
	this.detalleInternacional = detalleInternacional;
    }

    @Override
    public String toString() {
	return "ReporteTrimestral [fechaTrimestre =" + fechaTrimestre
		+ " detalleOnus=" + detalleOnus + ", detalleNacional="
		+ detalleNacional + ", detalleInternacional="
		+ detalleInternacional + "]";
    }

    @Override
    public int compareTo(ReporteTrimestral rp) {
	if (rp.getFechaTrimestre() != null && this.getFechaTrimestre() != null) {
	    int a = Integer.valueOf(rp.getFechaTrimestre().split("/")[0]);
	    int b = Integer.valueOf(this.getFechaTrimestre().split("/")[0]);
	    return b - a;
	} else {
	    return 0;
	}

    }

}
