package Beans;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PermisosUsuario implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Integer permisoId;
    private String nombre;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param permisoId
     * @param nombre
     * @since 1.X
     */
    public PermisosUsuario(Integer permisoId, String nombre) {
	super();
	this.permisoId = permisoId;
	this.nombre = nombre;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public PermisosUsuario() {
    }

    public Integer getPermisoId() {
	return permisoId;
    }

    public void setPermisoId(Integer permisoId) {
	this.permisoId = permisoId;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    @Override
    public String toString() {
	return "Menu [permisoId=" + permisoId + ", nombre=" + nombre +  "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((permisoId == null) ? 0 : permisoId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	PermisosUsuario other = (PermisosUsuario) obj;
	if (permisoId == null) {
	    if (other.permisoId != null)
		return false;
	} else if (!permisoId.equals(other.permisoId))
	    return false;
	return true;
    }

}