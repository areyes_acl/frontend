package Beans;

import java.io.Serializable;

public class PreguntasSecretas implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long sid;
    private String pregunta;
    private Integer estado;

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public PreguntasSecretas(){
	
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    @Override
    public String toString() {
	return "PreguntasSecretas [sid=" + sid + ", pregunta=" + pregunta
		+ ", estado=" + estado + "]";
    }
}
