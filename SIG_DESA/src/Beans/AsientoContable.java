package Beans;

import java.io.Serializable;

import types.AccionType;
import types.StatusType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2015, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class AsientoContable implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3223842465605190522L;
    private long sid;
    private String concepto;
    private String debe;
    private String haber;
    private String estado;
    private String agrupa;
    private String mit;
    private String codigoFunction;
    private String tipoMov;

    public long getSid() {
	return sid;
    }

    public void setSid(long sid) {
	this.sid = sid;
    }

    public String getConcepto() {
	return concepto;
    }

    public void setConcepto(String concepto) {
	this.concepto = concepto;
    }

    public String getDebe() {
	return debe;
    }

    public void setDebe(String debe) {
	this.debe = debe;
    }

    public String getHaber() {
	return haber;
    }

    public void setHaber(String haber) {
	this.haber = haber;
    }

    public String getEstado() {
	return estado;
    }

    public void setEstado(String estado) {
	this.estado = estado;
    }

    public String getAgrupa() {
	return agrupa;
    }

    public void setAgrupa(String agrupa) {
	this.agrupa = agrupa;
    }

    public String getMit() {
	return mit;
    }

    public void setMit(String mit) {
	this.mit = mit;
    }

    public String getCodigoFunction() {
	return codigoFunction;
    }

    public void setCodigoFunction(String codigoFunction) {
	this.codigoFunction = codigoFunction;
    }

    public String getTipoMov() {
	return tipoMov;
    }

    public void setTipoMov(String tipoMov) {
	this.tipoMov = tipoMov;
    }

    @Override
    public String toString() {
	return "TipoAsientoContable [sid=" + sid + ", concepto=" + concepto
		+ ", debe=" + debe + ", haber=" + haber + ", estado=" + estado
		+ ", agrupa=" + agrupa + ", mit=" + mit + ", codigoFunction="
		+ codigoFunction + ", tipoMov=" + tipoMov + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (int) (sid ^ (sid >>> 32));
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	AsientoContable other = (AsientoContable) obj;
	if (sid != other.sid)
	    return false;
	return true;
    }

}
