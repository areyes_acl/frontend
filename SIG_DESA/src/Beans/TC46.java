package Beans;

import java.io.Serializable;

public class TC46 implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long sid;
    private String settlementDate;
    private int totalInterchangeCount;
    private int totalInterchaneValue;
    private String interchangeValueSign;
    private int totalReimbursementFees;
    private String reimbursementFeeSign;
    private int totalVisaCharges;
    private String visaChargesSign;
    private int netSettlementAmount;
    private String netSettlementAmountSign;
    public Long getSid() {
        return sid;
    }
    public void setSid(Long sid) {
        this.sid = sid;
    }
    public String getSettlementDate() {
        return settlementDate;
    }
    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }
    public int getTotalInterchangeCount() {
        return totalInterchangeCount;
    }
    public void setTotalInterchangeCount(int totalInterchangeCount) {
        this.totalInterchangeCount = totalInterchangeCount;
    }
    public int getTotalInterchaneValue() {
        return totalInterchaneValue;
    }
    public void setTotalInterchaneValue(int totalInterchaneValue) {
        this.totalInterchaneValue = totalInterchaneValue;
    }
    public String getInterchangeValueSign() {
        return interchangeValueSign;
    }
    public void setInterchangeValueSign(String interchangeValueSign) {
        this.interchangeValueSign = interchangeValueSign;
    }
    public int getTotalReimbursementFees() {
        return totalReimbursementFees;
    }
    public void setTotalReimbursementFees(int totalReimbursementFees) {
        this.totalReimbursementFees = totalReimbursementFees;
    }
    public String getReimbursementFeeSign() {
        return reimbursementFeeSign;
    }
    public void setReimbursementFeeSign(String reimbursementFeeSign) {
        this.reimbursementFeeSign = reimbursementFeeSign;
    }
    public int getTotalVisaCharges() {
        return totalVisaCharges;
    }
    public void setTotalVisaCharges(int totalVisaCharges) {
        this.totalVisaCharges = totalVisaCharges;
    }
    public String getVisaChargesSign() {
        return visaChargesSign;
    }
    public void setVisaChargesSign(String visaChargesSign) {
        this.visaChargesSign = visaChargesSign;
    }
    public int getNetSettlementAmount() {
        return netSettlementAmount;
    }
    public void setNetSettlementAmount(int netSettlementAmount) {
        this.netSettlementAmount = netSettlementAmount;
    }
    public String getNetSettlementAmountSign() {
        return netSettlementAmountSign;
    }
    public void setNetSettlementAmountSign(String netSettlementAmountSign) {
        this.netSettlementAmountSign = netSettlementAmountSign;
    }
    @Override
    public String toString() {
	return "TC46 [sid=" + sid + ", settlementDate=" + settlementDate
		+ ", totalInterchangeCount=" + totalInterchangeCount
		+ ", totalInterchaneValue=" + totalInterchaneValue
		+ ", interchangeValueSign=" + interchangeValueSign
		+ ", totalReimbursementFees=" + totalReimbursementFees
		+ ", reimbursementFeeSign=" + reimbursementFeeSign
		+ ", totalVisaCharges=" + totalVisaCharges
		+ ", visaChargesSign=" + visaChargesSign
		+ ", netSettlementAmount=" + netSettlementAmount
		+ ", netSettlementAmountSign=" + netSettlementAmountSign + "]";
    }
   
    
    
    

}
