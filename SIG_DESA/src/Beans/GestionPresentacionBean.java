package Beans;

import java.io.Serializable;
import java.math.BigDecimal;

public class GestionPresentacionBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public GestionPresentacionBean()
	{
		//Constructor sin argumentos
	}
	

	private String montoTransac;
	private String montoFacturacion;
	private String montoConciliacion;
	private String mit;
	private String codigoFuncion;
	
	private String fechaTransac;
	private String codigoRazon;
	private String codMonedaTrx;
	private String glosaTipo;	
	private String glosaTipoMoneda;
	
	private String estatus;
	private int sidIncoming;
	private String usuario;
	private String comercio;
	private String pais;
	private String numeroTarjeta;
	private String patpass;
	
	// Datos adicionales a la consulta
	
	private String operador;
	private String microfilm;
	private String codigoAutorizacion;
	private String otrodato1;
	private String fechaEfectiva;
	private String fechaProceso;
	private String otrodato2;
	private String binAdquiriente;
	private String leeBanda;
	private String otrodato3;
	private String rubroComercio;
	private String otrodato4;
	
	
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getMontoTransac() {
		return montoTransac;
	}
	public void setMontoTransac(String montoTransac) {
		this.montoTransac = montoTransac;
	}
	public String getMontoFacturacion() {
		return montoFacturacion;
	}
	public void setMontoFacturacion(String montoFacturacion) {
		this.montoFacturacion = montoFacturacion;
	}
	public String getMontoConciliacion() {
		return montoConciliacion;
	}
	public void setMontoConciliacion(String montoConciliacion) {
		this.montoConciliacion = montoConciliacion;
	}
	public String getMit() {
		return mit;
	}
	public void setMit(String mit) {
		this.mit = mit;
	}
	public String getCodigoFuncion() {
		return codigoFuncion;
	}
	public void setCodigoFuncion(String codigoFuncion) {
		this.codigoFuncion = codigoFuncion;
	}
	public String getFechaTransac() {
		return fechaTransac;
	}
	public void setFechaTransac(String fechaTransac) {
		this.fechaTransac = fechaTransac;
	}
	public String getCodigoRazon() {
		return codigoRazon;
	}
	public void setCodigoRazon(String codigoRazon) {
		this.codigoRazon = codigoRazon;
	}
	public String getCodMonedaTrx() {
		return codMonedaTrx;
	}
	public void setCodMonedaTrx(String codMonedaTrx) {
		this.codMonedaTrx = codMonedaTrx;
	}
	public String getGlosaTipo() {
		return glosaTipo;
	}
	public void setGlosaTipo(String glosaTipo) {
		this.glosaTipo = glosaTipo;
	}
	public String getGlosaTipoMoneda() {
		return glosaTipoMoneda;
	}
	public void setGlosaTipoMoneda(String glosaTipoMoneda) {
		this.glosaTipoMoneda = glosaTipoMoneda;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public int getSidIncoming() {
		return sidIncoming;
	}
	public void setSidIncoming(int sidIncoming) {
		this.sidIncoming = sidIncoming;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getComercio() {
		return comercio;
	}
	public void setComercio(String comercio) {
		this.comercio = comercio;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public String getPatpass() {
	    return patpass;
	}
	public void setPatpass(String patpass) {
	    this.patpass = patpass;
	}
	
    public String getOperador() {
	    return operador;
	}
	public void setOperador(String operador) {
	    this.operador = operador;
	}
	public String getMicrofilm() {
	    return microfilm;
	}
	public void setMicrofilm(String microfilm) {
	    this.microfilm = microfilm;
	}
	public String getCodigoAutorizacion() {
	    return codigoAutorizacion;
	}
	public void setCodigoAutorizacion(String codigoAutorizacion) {
	    this.codigoAutorizacion = codigoAutorizacion;
	}
	public String getOtrodato1() {
	    return otrodato1;
	}
	public void setOtrodato1(String otrodato1) {
	    this.otrodato1 = otrodato1;
	}
	public String getFechaEfectiva() {
	    return fechaEfectiva;
	}
	public void setFechaEfectiva(String fechaEfectiva) {
	    this.fechaEfectiva = fechaEfectiva;
	}
	public String getFechaProceso() {
	    return fechaProceso;
	}
	public void setFechaProceso(String fechaProceso) {
	    this.fechaProceso = fechaProceso;
	}
	public String getOtrodato2() {
	    return otrodato2;
	}
	public void setOtrodato2(String otrodato2) {
	    this.otrodato2 = otrodato2;
	}
	public String getBinAdquiriente() {
	    return binAdquiriente;
	}
	public void setBinAdquiriente(String binAdquiriente) {
	    this.binAdquiriente = binAdquiriente;
	}
	public String getLeeBanda() {
	    return leeBanda;
	}
	public void setLeeBanda(String leeBanda) {
	    this.leeBanda = leeBanda;
	}
	public String getOtrodato3() {
	    return otrodato3;
	}
	public void setOtrodato3(String otrodato3) {
	    this.otrodato3 = otrodato3;
	}
	public String getRubroComercio() {
	    return rubroComercio;
	}
	public void setRubroComercio(String rubroComercio) {
	    this.rubroComercio = rubroComercio;
	}
	public String getOtrodato4() {
	    return otrodato4;
	}
	public void setOtrodato4(String otrodato4) {
	    this.otrodato4 = otrodato4;
	}
    @Override
    public String toString() {
        return "GestionPresentacionBean [montoTransac=" + montoTransac
                + ", montoFacturacion=" + montoFacturacion
                + ", montoConciliacion=" + montoConciliacion + ", mit=" + mit
                + ", codigoFuncion=" + codigoFuncion + ", fechaTransac="
                + fechaTransac + ", codigoRazon=" + codigoRazon
                + ", codMonedaTrx=" + codMonedaTrx + ", glosaTipo=" + glosaTipo
                + ", glosaTipoMoneda=" + glosaTipoMoneda + ", estatus="
                + estatus + ", sidIncoming=" + sidIncoming + ", usuario="
                + usuario + ", comercio=" + comercio + ", pais=" + pais
                + ", numeroTarjeta=" + numeroTarjeta + ",patpass="+patpass+"]";
    }
	
	
	

}
