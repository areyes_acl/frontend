package Beans;

import java.io.Serializable;

import types.AccionType;
import types.StatusType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2015, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class AccionTransaccion implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5508977265132754329L;
    private long sid;
    private String codigoxKey;
    private String codigoTransaccion;
    private String codigoFuncion;
    private String descripcion;
    private AccionType tipoAccion;
    private StatusType activo;
    private StatusType verConsulta;
    private StatusType verGestion;
    
    public String getCodigoxKey() {
        return codigoxKey;
    }
    
    public long getSid() {
        return sid;
    }
    
    public void setSid( long sid ) {
        this.sid = sid;
    }
    
    public void setCodigoxKey( String codigoxKey ) {
        this.codigoxKey = codigoxKey;
    }
    
    public String getCodigoTransaccion() {
        return codigoTransaccion;
    }
    
    public void setCodigoTransaccion( String codigoTransaccion ) {
        this.codigoTransaccion = codigoTransaccion;
    }
    
    public String getCodigoFuncion() {
        return codigoFuncion;
    }
    
    public void setCodigoFuncion( String codigoFuncion ) {
        this.codigoFuncion = codigoFuncion;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion( String descripcion ) {
        this.descripcion = descripcion;
    }
    
    public AccionType getTipoAccion() {
        return tipoAccion;
    }
    
    public void setTipoAccion( AccionType tipoAccion ) {
        this.tipoAccion = tipoAccion;
    }
    
    public StatusType getActivo() {
        return activo;
    }
    
    public void setActivo( StatusType activo ) {
        this.activo = activo;
    }
    
    public StatusType getVerConsulta() {
        return verConsulta;
    }
    
    public void setVerConsulta( StatusType verConsulta ) {
        this.verConsulta = verConsulta;
    }
    
    public StatusType getVerGestion() {
        return verGestion;
    }
    
    public void setVerGestion( StatusType verGestion ) {
        this.verGestion = verGestion;
    }
    
    @Override
    public String toString() {
        return "AccionTransaccion [id=" + sid + ", codigoXKEY=" + codigoxKey
                + ", codigoTransaccion=" + codigoTransaccion
                + ", codigoFuncion=" + codigoFuncion + ", descripcion="
                + descripcion + ", tipoAccion=" + tipoAccion + ", activo="
                + activo + ", verConsulta=" + verConsulta + ", verGestion="
                + verGestion + "]";
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( int ) ( sid ^ ( sid >>> 32 ) );
        return result;
    }
    
    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        AccionTransaccion other = ( AccionTransaccion ) obj;
        if ( sid != other.sid )
            return false;
        return true;
    }
    
}
