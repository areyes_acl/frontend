package Beans;

import java.io.Serializable;

public class solicitudBean implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private int sid;
    private String fechaInicio;
    private String fechaFin;
    private String usuario;
    private String tipoMov;
    private String estado;
    private String momento;
    private String fichero;
    private String operador;
    private String bin;
    private int total;
    
    public int getSid() {
        return sid;
    }
    public void setSid(int sid) {
        this.sid = sid;
    }
    public String getFechaInicio() {
        return fechaInicio;
    }
    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    public String getFechaFin() {
        return fechaFin;
    }
    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getTipoMov() {
        return tipoMov;
    }
    public void setTipoMov(String tipoMov) {
        this.tipoMov = tipoMov;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getMomento() {
        return momento;
    }
    public void setMomento(String momento) {
        this.momento = momento;
    }
    public String getFichero() {
        return fichero;
    }
    public void setFichero(String fichero) {
        this.fichero = fichero;
    }
    public String getOperador() {
        return operador;
    }
    public void setOperador(String operador) {
        this.operador = operador;
    }
    public String getBin() {
        return bin;
    }
    public void setBin(String bin) {
        this.bin = bin;
    }
    public int getTotal() {
        return total;
    }
    public void setTotal(int total) {
        this.total = total;
    }
    
    

}
