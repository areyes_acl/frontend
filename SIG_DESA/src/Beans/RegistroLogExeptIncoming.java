package Beans;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RegistroLogExeptIncoming implements Serializable, RegistroLog {

    /**
     * 
     */
    private static final long serialVersionUID = -8359466889324678410L;
    private long sid;
    private String logCarga;
    private String fechaIncoming;
    private String mit;
    private String numeroTarjeta;
    private String codigoFuncion;
    private String codigoAutorizacion;

    public long getSid() {
	return sid;
    }

    public void setSid(long sid) {
	this.sid = sid;
    }

    public String getLogCarga() {
	return logCarga;
    }

    public void setLogCarga(String logCarga) {
	this.logCarga = logCarga;
    }

    public String getFechaIncoming() {
	return fechaIncoming;
    }

    public void setFechaIncoming(String fechaIncoming) {
	this.fechaIncoming = fechaIncoming;
    }

    public String getMit() {
	return mit;
    }

    public void setMit(String mit) {
	this.mit = mit;
    }

    public String getNumeroTarjeta() {
	return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
	this.numeroTarjeta = numeroTarjeta;
    }

    public String getCodigoFuncion() {
	return codigoFuncion;
    }

    public void setCodigoFuncion(String codigoFuncion) {
	this.codigoFuncion = codigoFuncion;
    }

    public String getCodigoAutorizacion() {
	return codigoAutorizacion;
    }

    public void setCodigoAutorizacion(String codigoAutorizacion) {
	this.codigoAutorizacion = codigoAutorizacion;
    }

    @Override
    public String toString() {
	return "RegistroLogExeptIncoming [sid=" + sid + ", logCarga="
		+ logCarga + ", fechaIncoming=" + fechaIncoming + ", mit="
		+ mit + ", numeroTarjeta=" + numeroTarjeta + ", codigoFuncion="
		+ codigoFuncion + ", codigoAutorizacion=" + codigoAutorizacion
		+ "]";
    }

}
