package Beans;

import java.io.Serializable;


public class TDIJobValidacionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3532155344728065735L;
	private Long sid;
	private TDITipoValidacionBean validacionId;
	private String origen;
	private String destino;
	
	
	public TDIJobValidacionBean() {
		super();
	}


	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}


	public TDITipoValidacionBean getValidacionId() {
	    return validacionId;
	}


	public void setValidacionId(TDITipoValidacionBean validacionId) {
	    this.validacionId = validacionId;
	}


	public String getOrigen() {
	    return origen;
	}
	
	public String[] getOrigenEdit() {
	    return origen.split(";");
	}


	public void setOrigen(String origen) {
	    this.origen = origen;
	}

	public String[] getDestinoEdit() {
	    return destino.split(";");
	}

	public String getDestino() {
	    return destino;
	}


	public void setDestino(String destino) {
	    this.destino = destino;
	}


	public TDIJobValidacionBean(Long sid, TDITipoValidacionBean validacionId, String origen, String destino) {
	    super();
	    this.sid = sid;
	    this.validacionId = validacionId;
	    this.origen = origen;
	    this.destino = destino;
	}

}
