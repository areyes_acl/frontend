package Beans;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RegistroLogGeneric implements Serializable, RegistroLog {

    /**
     * 
     */
    private static final long serialVersionUID = -2551013790798656936L;
    private long sid;
    private String fecha;
    private String filename;
    private String fileFlag;
    private String fileTs;

    public long getSid() {
	return sid;
    }

    public void setSid(long sid) {
	this.sid = sid;
    }

    public String getFecha() {
	return fecha;
    }

    public void setFecha(String fecha) {
	this.fecha = fecha;
    }

    public String getFilename() {
	return filename;
    }

    public void setFilename(String filename) {
	this.filename = filename;
    }

    public String getFileFlag() {
	return fileFlag;
    }

    public void setFileFlag(String fileFlag) {
	this.fileFlag = fileFlag;
    }

    public String getFileTs() {
	return fileTs;
    }

    public void setFileTs(String fileTs) {
	this.fileTs = fileTs;
    }

    @Override
    public String toString() {
	return "RegistroLogGeneric [sid=" + sid + ", fecha=" + fecha
		+ ", filename=" + filename + ", fileFlag=" + fileFlag
		+ ", fileTs=" + fileTs + "]";
    }

}