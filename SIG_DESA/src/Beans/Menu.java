package Beans;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Menu implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long sid;
    private String nombre;
    private String descripcion;
    private Integer posicion;
    private List<SubMenu> listaSubMenus;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param sid
     * @param nombre
     * @param descripcion
     * @param posicion
     * @since 1.X
     */
    public Menu(Long sid, String nombre, String descripcion, Integer posicion) {
	super();
	this.sid = sid;
	this.nombre = nombre;
	this.descripcion = descripcion;
	this.posicion = posicion;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public Menu() {
    }

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    public Integer getPosicion() {
	return posicion;
    }

    public void setPosicion(Integer posicion) {
	this.posicion = posicion;
    }

    public List<SubMenu> getListaSubMenus() {
	return listaSubMenus;
    }

    public void setListaSubMenus(List<SubMenu> listaSubMenus) {
	this.listaSubMenus = listaSubMenus;
    }

    @Override
    public String toString() {
	return "Menu [sid=" + sid + ", nombre=" + nombre + ", descripcion="
		+ descripcion + ", posicion=" + posicion + ", listaSubMenus="
		+ listaSubMenus + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((sid == null) ? 0 : sid.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Menu other = (Menu) obj;
	if (sid == null) {
	    if (other.sid != null)
		return false;
	} else if (!sid.equals(other.sid))
	    return false;
	return true;
    }

    
}