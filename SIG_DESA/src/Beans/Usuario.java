package Beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Usuario implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8109472420457810282L;
    private Long sid;
    private Integer rut;
    private String dv;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String email;
    private String fono;
    private Boolean esSuperUsuario;
    private Boolean estado;
    private Contrasena contrasena;
    private List<Rol> listaRoles;
    private List<Menu> listaMenu;
    private Integer lectura;
    private Integer creacion;
    private Integer actualizacion;
    private Integer eliminacion;
    private Integer preguntas;
    private Integer actualizar;    

    public Integer getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(Integer preguntas) {
        this.preguntas = preguntas;
    }

    public Integer getActualizar() {
        return actualizar;
    }

    public void setActualizar(Integer actualizar) {
        this.actualizar = actualizar;
    }

    public Integer getLectura() {
        return lectura;
    }

    public void setLectura(Integer lectura) {
        this.lectura = lectura;
    }

    public Integer getCreacion() {
        return creacion;
    }

    public void setCreacion(Integer creacion) {
        this.creacion = creacion;
    }

    public Integer getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Integer actualizacion) {
        this.actualizacion = actualizacion;
    }

    public Integer getEliminacion() {
        return eliminacion;
    }

    public void setEliminacion(Integer eliminacion) {
        this.eliminacion = eliminacion;
    }

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public Integer getRut() {
	return rut;
    }

    public void setRut(Integer rut) {
	this.rut = rut;
    }

    public String getDv() {
	return dv;
    }

    public void setDv(String dv) {
	this.dv = dv;
    }

    public String getNombres() {
	return nombres;
    }

    public void setNombres(String nombres) {
	this.nombres = nombres;
    }

    public String getApellidoPaterno() {
	return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
	this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
	return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
	this.apellidoMaterno = apellidoMaterno;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getFono() {
	return fono;
    }

    public void setFono(String fono) {
	this.fono = fono;
    }

    public Boolean getEsSuperUsuario() {
	return esSuperUsuario;
    }

    public void setEsSuperUsuario(Boolean esSuperUsuario) {
	this.esSuperUsuario = esSuperUsuario;
    }

    public Boolean getEstado() {
	return estado;
    }

    public void setEstado(Boolean estado) {
	this.estado = estado;
    }

    public Contrasena getContrasena() {
	return contrasena;
    }

    public void setContrasena(Contrasena contrasena) {
	this.contrasena = contrasena;
    }

    public List<Rol> getListaRoles() {
	return listaRoles;
    }

    public void setListaRoles(List<Rol> listaRoles) {
	this.listaRoles = listaRoles;
    }

    public List<Menu> getListaMenu() {
	return listaMenu;
    }

    public void setListaMenu(List<Menu> listaMenu) {
	this.listaMenu = listaMenu;
    }

    @Override
    public String toString() {
	return "Usuario [sid=" + sid + ", rut=" + rut + ", dv=" + dv
		+ ", nombres=" + nombres + ", apellidoPaterno="
		+ apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno
		+ ", email=" + email + ", fono=" + fono + ", esSuperUsuario="
		+ esSuperUsuario + ", estado=" + estado + ", contrasena="
		+ contrasena + ", listaRoles=" + listaRoles + ", listaMenu="
		+ listaMenu + ", lectura=" + lectura + ", creacion=" 
		+ creacion + ", actualizacion=" + actualizacion 
		+ ", eliminacion=" + eliminacion + "]";
    }

    public Integer getPermisoSubMenu(Long idMenu, Long idSubMenu, Integer idPermiso){
	for (Menu menu: this.listaMenu) {
	    List<SubMenu> listaSubMenu = menu.getListaSubMenus();
	    for (SubMenu submenu: listaSubMenu) {
		List<PermisosUsuario> listaPermisos = submenu.getListaPermisos();
		for (PermisosUsuario permiso: listaPermisos) {
		    if(permiso.getPermisoId() == idPermiso && submenu.getSid() == idSubMenu){
			    return 1;
		    }
		}
	    }
	}
	return 0;
    }
}