package Beans;


/**
 * 
 * @author afreire
 *
 */
public class ParametroBean {

	private String codGrupo;
	private String codDato;
	private String valor;
	private String descripcion;
	private String sidUsuario;
	private String fechaCreacion;
	private String fechaModificacion;

	public String getCodGrupo() {
		return codGrupo;
	}

	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
	}

	public String getCodDato() {
		return codDato;
	}

	public void setCodDato(String codDato) {
		this.codDato = codDato;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSidUsuario() {
		return sidUsuario;
	}

	public void setSidUsuario(String sidUsuario) {
		this.sidUsuario = sidUsuario;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Override
	public String toString() {
		return "ParametroBean [codGrupo=" + codGrupo + ", codDato=" + codDato
				+ ", valor=" + valor + ", descripcion=" + descripcion
				+ ", sidUsuario=" + sidUsuario + ", fechaCreacion="
				+ fechaCreacion + ", fechaModificacion=" + fechaModificacion
				+ "]";
	}

}
