package Beans;

import java.io.Serializable;

public class TransaccionOnusBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransaccionOnusBean() {
		// Constructor sin argumentos
	}

	private long sid;
	private long registro;
	private String codigoTransaccion;
	private String numeroTarjeta;
	private String fechaCompra;
	private String fechaAutorizacion;
	private String fechaPosteo;
	private String tipoVenta;
	private String numCuotas;
	private String numMicroFilm;
	private String numComercio;
	private String montoTransaccion;
	private String valorCuota;
	private String nombreComercio;
	private String ciudadComercio;
	private String rubroComercio;
	private String codAutor;
	private String error;
	// Paginacion

	private String pagActual;
	private String pagCambio;
	private String total;

	public long getSid() {
		return sid;
	}

	public void setSid(long sid) {
		this.sid = sid;
	}

	public long getRegistro() {
		return registro;
	}

	public void setRegistro(long registro) {
		this.registro = registro;
	}

	public String getCodigoTransaccion() {
		return codigoTransaccion;
	}

	public void setCodigoTransaccion(String codigoTransaccion) {
		this.codigoTransaccion = codigoTransaccion;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(String fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	public String getFechaPosteo() {
		return fechaPosteo;
	}

	public void setFechaPosteo(String fechaPosteo) {
		this.fechaPosteo = fechaPosteo;
	}

	public String getTipoVenta() {
		return tipoVenta;
	}

	public void setTipoVenta(String tipoVenta) {
		this.tipoVenta = tipoVenta;
	}

	public String getNumCuotas() {
		return numCuotas;
	}

	public void setNumCuotas(String numCuotas) {
		this.numCuotas = numCuotas;
	}

	public String getNumMicroFilm() {
		return numMicroFilm;
	}

	public void setNumMicroFilm(String numMicroFilm) {
		this.numMicroFilm = numMicroFilm;
	}

	public String getNumComercio() {
		return numComercio;
	}

	public void setNumComercio(String numComercio) {
		this.numComercio = numComercio;
	}

	public String getMontoTransaccion() {
		return montoTransaccion;
	}

	public void setMontoTransaccion(String montoTransaccion) {
		this.montoTransaccion = montoTransaccion;
	}

	public String getValorCuota() {
		return valorCuota;
	}

	public void setValorCuota(String valorCuota) {
		this.valorCuota = valorCuota;
	}

	public String getNombreComercio() {
		return nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}

	public String getCiudadComercio() {
		return ciudadComercio;
	}

	public void setCiudadComercio(String ciudadComercio) {
		this.ciudadComercio = ciudadComercio;
	}

	public String getRubroComercio() {
		return rubroComercio;
	}

	public void setRubroComercio(String rubroComercio) {
		this.rubroComercio = rubroComercio;
	}

	public String getCodAutor() {
		return codAutor;
	}

	public void setCodAutor(String codAutor) {
		this.codAutor = codAutor;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getPagActual() {
		return pagActual;
	}

	public void setPagActual(String pagActual) {
		this.pagActual = pagActual;
	}

	public String getPagCambio() {
		return pagCambio;
	}

	public void setPagCambio(String pagCambio) {
		this.pagCambio = pagCambio;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "TransaccionOnusBean [sid=" + sid + ", codigoTransaccion="
				+ codigoTransaccion + ", numeroTarjeta=" + numeroTarjeta
				+ ", fechaCompra=" + fechaCompra + ", fechaAutorizacion="
				+ fechaAutorizacion + ", fechaPosteo=" + fechaPosteo
				+ ", tipoVenta=" + tipoVenta + ", numCuotas=" + numCuotas
				+ ", numMicroFilm=" + numMicroFilm + ", numComercio="
				+ numComercio + ", montoTransaccion=" + montoTransaccion
				+ ", valorCuota=" + valorCuota + ", nombreComercio="
				+ nombreComercio + ", ciudadComercio=" + ciudadComercio
				+ ", rubroComercio=" + rubroComercio + ", codAutor=" + codAutor
				+ ", pagActual=" + pagActual + ", pagCambio=" + pagCambio
				+ ", total=" + total + "]";
	}

}
