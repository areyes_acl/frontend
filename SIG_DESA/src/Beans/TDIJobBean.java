package Beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TDIJobBean implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 3532155344728065735L;
    private Long sid;
    private String proceso;
    private String origenPath;
    private String destinoPath;
    
    private TDIConexionTipoBean origenTipo;
    private TDIConexionTipoBean destinoTipo;
    
    private String origenIP;
    private String destinoIP;

    private int origenPuerto;
    private int destinoPuerto;

    private String origenUsuario;
    private String origenPass;
    private String destinoUsuario;
    private String destinoPass;
    
    private String archivo;
    private String archivoExt;
    private int control;
    private String controlExt;

    private int notificacion1;
    private String notificacion1Cadena;

    private int reintentos;
    private int reintentosMin;
    
    private Date ejecucion;
    
    private int activo;
    
    private int cantidadDeLogs;
    
    private List<TDIJobValidacionBean> validaciones;
    
    private int variabilidad;
    private int variabilidadPorciento;
    private int variabilidadLineas;
    
    public int getCantidadDeLogs() {
        return cantidadDeLogs;
    }

    public void setCantidadDeLogs(int cantidadDeLogs) {
        this.cantidadDeLogs = cantidadDeLogs;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getNotificacion1Show() {
        return (notificacion1 == 1)?"SI":"NO";
    }
    
    public String getVariabilidadShow() {
        return (variabilidad == 1)?"SI":"NO";
    }
    
    public int getVariabilidad() {
        return variabilidad;
    }

    public void setVariabilidad(int variabilidad) {
        this.variabilidad = variabilidad;
    }

    public int getVariabilidadPorciento() {
        return variabilidadPorciento;
    }

    public void setVariabilidadPorciento(int variabilidadPorciento) {
        this.variabilidadPorciento = variabilidadPorciento;
    }

    public int getVariabilidadLineas() {
        return variabilidadLineas;
    }

    public void setVariabilidadLineas(int variabilidadLineas) {
        this.variabilidadLineas = variabilidadLineas;
    }

    public int getReintentos() {
        return reintentos;
    }

    public void setReintentos(int reintentos) {
        this.reintentos = reintentos;
    }

    public int getReintentosMin() {
        return reintentosMin;
    }

    public void setReintentosMin(int reintentosMin) {
        this.reintentosMin = reintentosMin;
    }

    public int getNotificacion1() {
        return notificacion1;
    }

    public void setNotificacion1(int notificacion1) {
        this.notificacion1 = notificacion1;
    }

    public String getNotificacion1Cadena() {
        return notificacion1Cadena;
    }

    public void setNotificacion1Cadena(String notificacion1Cadena) {
        this.notificacion1Cadena = notificacion1Cadena;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getArchivoExt() {
        return archivoExt;
    }

    public void setArchivoExt(String archivoExt) {
        this.archivoExt = archivoExt;
    }

    public int getControl() {
        return control;
    }

    public void setControl(int control) {
        this.control = control;
    }

    public String getControlExt() {
        return controlExt;
    }

    public void setControlExt(String controlExt) {
        this.controlExt = controlExt;
    }

    public String getOrigenUsuario() {
        return origenUsuario;
    }

    public void setOrigenUsuario(String origenUsuario) {
        this.origenUsuario = origenUsuario;
    }

    public String getOrigenPass() {
        return origenPass;
    }

    public void setOrigenPass(String origenPass) {
        this.origenPass = origenPass;
    }

    public String getDestinoUsuario() {
        return destinoUsuario;
    }

    public void setDestinoUsuario(String destinoUsuario) {
        this.destinoUsuario = destinoUsuario;
    }

    public String getDestinoPass() {
        return destinoPass;
    }

    public void setDestinoPass(String destinoPass) {
        this.destinoPass = destinoPass;
    }

    public String getOrigenIP() {
	return origenIP;
    }

    public int getOrigenPuerto() {
	return origenPuerto;
    }

    public void setOrigenPuerto(int origenPuerto) {
	this.origenPuerto = origenPuerto;
    }

    public int getDestinoPuerto() {
	return destinoPuerto;
    }

    public void setDestinoPuerto(int destinoPuerto) {
	this.destinoPuerto = destinoPuerto;
    }

    public TDIJobBean(Long sid, String proceso, String origenPath,
	    String destinoPath, TDIConexionTipoBean origenTipo, TDIConexionTipoBean destinoTipo,
	    String origenIP, String destinoIP, int origenPuerto,
	    int destinoPuerto, String origenUsuario, String origenPass,
	    String destinoUsuario, String destinoPass, String archivo,
	    String archivoExt, int control, String controlExt,
	    int notificacion1, String notificacion1Cadena, int reintentos, int reintentosMin,
	    Date ejecucion, int activo, List<TDIJobValidacionBean> validaciones, int variabilidad, int variabilidadPorciento, int variabilidadLineas) {
	super();
	this.sid = sid;
	this.proceso = proceso;
	this.origenPath = origenPath;
	this.destinoPath = destinoPath;
	this.origenTipo = origenTipo;
	this.destinoTipo = destinoTipo;
	this.origenIP = origenIP;
	this.destinoIP = destinoIP;
	this.origenPuerto = origenPuerto;
	this.destinoPuerto = destinoPuerto;
	this.origenUsuario = origenUsuario;
	this.origenPass = origenPass;
	this.destinoUsuario = destinoUsuario;
	this.destinoPass = destinoPass;
	this.archivo = archivo;
	this.archivoExt = archivoExt;
	this.control = control;
	this.controlExt = controlExt;
	this.notificacion1 = notificacion1;
	this.notificacion1Cadena = notificacion1Cadena;
	this.reintentos = reintentos;
	this.reintentosMin = reintentosMin;
	this.ejecucion = ejecucion;
	this.activo = activo;
	this.validaciones = validaciones;
	this.variabilidad = variabilidad;
	this.variabilidadPorciento = variabilidadPorciento;
	this.variabilidadLineas = variabilidadLineas;
    }


    public void setOrigenIP(String origenIP) {
	this.origenIP = origenIP;
    }

    public String getDestinoIP() {
	return destinoIP;
    }

    public void setDestinoIP(String destinoIP) {
	this.destinoIP = destinoIP;
    }

    public String getOrigenPath() {
	return origenPath;
    }

    public void setOrigenPath(String origenPath) {
	this.origenPath = origenPath;
    }

    public String getDestinoPath() {
	return destinoPath;
    }

    public void setDestinoPath(String destinoPath) {
	this.destinoPath = destinoPath;
    }

    public TDIConexionTipoBean getOrigenTipo() {
        return origenTipo;
    }

    public void setOrigenTipo(TDIConexionTipoBean origenTipo) {
        this.origenTipo = origenTipo;
    }

    public TDIConexionTipoBean getDestinoTipo() {
        return destinoTipo;
    }

    public void setDestinoTipo(TDIConexionTipoBean destinoTipo) {
        this.destinoTipo = destinoTipo;
    }

    public TDIJobBean() {
	super();
	this.control = 0;
	this.variabilidad = 0;
	this.notificacion1 = 0;
	this.activo = 1;
    }

    public String getProceso() {
	return proceso;
    }

    public void setProceso(String proceso) {
	this.proceso = proceso;
    }

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getArchivoReal(){
	return this.getArchivo()+"."+this.getArchivoExt();
    }
    
    public String getEjecucionShow(){
	SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
	return formatoHora.format(ejecucion);
    }
    
    public String getEjecucionHoraShow(){
	SimpleDateFormat formatoHora = new SimpleDateFormat("HH");
	return formatoHora.format(ejecucion);
    }
    
    public String getEjecucionMinShow(){
   	SimpleDateFormat formatoHora = new SimpleDateFormat("mm");
   	return formatoHora.format(ejecucion);
    }
    
    public Date getEjecucion() {
        return ejecucion;
    }

    public void setEjecucion(Date ejecucion) {
        this.ejecucion = ejecucion;
    }

    public String getActivado(){
	return (this.activo == 1)?"SI":"NO";
    }

    public List<TDIJobValidacionBean> getValidaciones() {
        return validaciones;
    }

    public void setValidaciones(List<TDIJobValidacionBean> validaciones) {
        this.validaciones = validaciones;
    }
    
}
