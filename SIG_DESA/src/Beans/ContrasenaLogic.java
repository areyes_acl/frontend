/**
 * 
 */
package Beans;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import types.AccionType;
import types.ContrasenaType;
import types.EstadoContrasenaType;
import types.EstadoType;
import types.ParametrosType;
import cl.filter.FiltroParametro;
import clases.ContrasenaByUsuarioDAO;
import clases.EncriptacionClave;
import clases.ParametrosDAO;
import exception.UsuarioServiceException;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * CLASE QUE SE ENCARGA DE VALIDAR LA CONTRASE�A � Leading (Comiencen con
 * n�meros) � Trailing (Que terminen con n�meros) � Largo � (M�nimo y m�ximo) �
 * Num�rica, alfab�tica, alfanum�rica � Uso de caracteres especiales � Vigencia
 * de contrase�a � Permite caracteres consecutivos � Permite caracteres iguales
 * seguidos � Sensibilidad a may�sculas y min�sculas � Obliga uso may�scula
 * 
 * 
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ContrasenaLogic implements IContrasena {

    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(ContrasenaLogic.class);

    /**
     * VARIABLES
     */
    private String NO_PERMITE = "0";
    private List<Parametros> listaPoliticasActivas;
    private ParametrosDAO politicaDAO;
    private ContrasenaByUsuarioDAO contrasenaByUsuarioDAO;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que realiza la validacion de una contrase�a
     * 
     * @param contrasena
     * @return
     * @throws UserServiceException
     * @throws
     * @since 1.X
     */
    public void validarContrasena(Usuario usuario, AccionType accion)
	    throws UsuarioServiceException {
	
	logger.info("======= Realiza validaciones de politicas activas de contrase�a =======");

	Contrasena contrasena = usuario.getContrasena();

	// Obtiene las politicas activas
	politicaDAO = new ParametrosDAO();
	FiltroParametro filtro = new FiltroParametro(EstadoType.ACTIVO);
	listaPoliticasActivas = politicaDAO.buscarPorFiltro(filtro);

	// Validaci�n com�n independiente del tipo de contrase�a utilizado
	PasswordUtils.validarContrasenaIgualARut(usuario.getRut(),
		usuario.getDv(), contrasena);

	if (listaPoliticasActivas != null && !listaPoliticasActivas.isEmpty()) {

	    // Buscar parametro del tipo de contrasena
	    Parametros parametro = buscarTipoContrasena();

	    if (parametro != null
		    && ContrasenaType.NUMERICA.getLabel().equalsIgnoreCase(
			    parametro.getValor())) {
		validacionesTipoNumerica(contrasena, accion, usuario.getSid());

	    } else if (parametro != null
		    && ContrasenaType.ALFABETICA.getLabel().equalsIgnoreCase(
			    parametro.getValor())) {
		validacionesTipoAlfabetica(contrasena, accion, usuario.getSid());

	    } else if (parametro != null
		    && ContrasenaType.ALFANUMERICA.getLabel().equalsIgnoreCase(
			    parametro.getValor())) {
		validacionesTipoAlfanumerica(contrasena, accion,
			usuario.getSid());

	    }
	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param listaPoliticasActivas2
     * @return
     * @since 1.X
     */
    private Parametros buscarTipoContrasena() {

	for (Parametros politica : listaPoliticasActivas) {
	    if (ParametrosType.TIPO_CLAVE.equals(politica.getParametro())) {
		return politica;
	    }
	}
	return null;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param listaPoliticas
     * @throws UsuarioServiceException
     * @throws
     * @since 1.X
     */
    private void validacionesTipoAlfanumerica(Contrasena contrasena,
	    AccionType accion, Long sidUsuario) throws UsuarioServiceException {

	for (Parametros parametro : listaPoliticasActivas) {

	    // VALIDAR EL TIPO DE CONTRASE�A
	    validacionesGenericas(contrasena, parametro);

	    // 1- Comience con numeros
	    if (parametro.getParametro().equals(ParametrosType.OBLIGA_LEADING)
		    && parametro.getValor().equalsIgnoreCase(NO_PERMITE)) {
		PasswordUtils.validarLeading(contrasena.getPassword());
	    }

	    // 2- Termine con numeros
	    if (parametro.getParametro().equals(ParametrosType.OBLIGA_TRAILING)
		    && parametro.getValor().equalsIgnoreCase(NO_PERMITE)) {
		PasswordUtils.validarTrailing(contrasena.getPassword());
	    }

	    // 7- Validar cantidad de veces de rehuso (CONTRASENA NO NUEVA)
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_RECHAZO_RE_USO)
		    && accion.equals(AccionType.ACTUALIZACION)) {
		validarNroRechazoRehuso(contrasena,
			Integer.valueOf(parametro.getValor()), sidUsuario);
	    }

	    // 8- Permite Caracteres consecutivos
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_CARACTERES_CONSECUTIVOS)) {
		PasswordUtils.validarCaracteresConsecutivos(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }

	    // 9- Permite Caracteres iguales seguidos
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_CARACT_IGUALES_CONSECUTIVOS)) {
		PasswordUtils.validarCaracteresRepetidosConsecutivos(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }

	    // 11- Obliga uso de mayuscula
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_DE_MAYUSCULAS_OBLIGATORIAS)) {
		PasswordUtils.validaObligatoriedadMayuscula(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }
	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param listaPoliticasActivas2
     * @throws UsuarioServiceException
     * @since 1.X
     */
    private void validacionesTipoAlfabetica(Contrasena contrasena,
	    AccionType accion, Long sidUsuario) throws UsuarioServiceException {

	for (Parametros parametro : listaPoliticasActivas) {

	    validacionesGenericas(contrasena, parametro);


	    // 7- Validar cantidad de veces de rehuso (CONTRASENA NO NUEVA)
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_RECHAZO_RE_USO)
		    && accion.equals(AccionType.ACTUALIZACION)) {
		validarNroRechazoRehuso(contrasena,
			Integer.valueOf(parametro.getValor()), sidUsuario);
	    }

	    // 8- Permite Caracteres consecutivos
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_CARACTERES_CONSECUTIVOS)) {
		PasswordUtils.validarCaracteresConsecutivos(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }

	    // 9- Permite Caracteres iguales seguidos
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_CARACT_IGUALES_CONSECUTIVOS)) {
		PasswordUtils.validarCaracteresRepetidosConsecutivos(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }

	    // 11- Obliga uso de mayuscula
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_DE_MAYUSCULAS_OBLIGATORIAS)) {
		PasswordUtils.validaObligatoriedadMayuscula(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }

	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param listaPoliticasActivas2
     * @throws UsuarioServiceException
     * @since 1.X
     */
    private void validacionesTipoNumerica(Contrasena contrasena,
	    AccionType accion, Long sidUsuario) throws UsuarioServiceException {

	for (Parametros parametro : listaPoliticasActivas) {

	    validacionesGenericas(contrasena, parametro);

	    // 7- Validar cantidad de veces de rehuso (CONTRASENA NO NUEVA)
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_RECHAZO_RE_USO)) {
		validarNroRechazoRehuso(contrasena,
			Integer.valueOf(parametro.getValor()), sidUsuario);
	    }

	    // 8- Permite Caracteres consecutivos
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_CARACTERES_CONSECUTIVOS)) {
		PasswordUtils.validarCaracteresConsecutivos(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }

	    // 9- Permite Caracteres iguales seguidos
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_CARACT_IGUALES_CONSECUTIVOS)) {
		PasswordUtils.validarCaracteresRepetidosConsecutivos(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }

	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Consultar a Bd de datos la cantidad de veces que la contrasena ha sido
     * utilizada
     * 
     * @param contrasena
     * @throws Beans.UsuarioServiceException
     * @throws UserServiceException
     * @since 1.X
     */
    private void validarNroRechazoRehuso(Contrasena contrasenaOriginal,
	    Integer cantidadAnteriores, Long sidUsuario)
	    throws UsuarioServiceException {
	contrasenaByUsuarioDAO = new ContrasenaByUsuarioDAO();
	List<Contrasena> listaUltimasContrasenas = contrasenaByUsuarioDAO
		.obtenerContrasenasAnterioresPorUsuario(
			EstadoContrasenaType.ACTIVA, sidUsuario,
			cantidadAnteriores);

	EncriptacionClave encriptacionClave = new EncriptacionClave();
	String claveUsuarioEncriptada = encriptacionClave
		.getStringMessageDigest(contrasenaOriginal.getPassword(),
			encriptacionClave.SHA256);

	PasswordUtils.validarNroRechazoRehuso(listaUltimasContrasenas,
		claveUsuarioEncriptada);
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que comprara si la fecha de hoy es mayor a la de vencimiento de la
     * contrasena para validar que no haya expirado
     * 
     * @param contrasena
     * @param valor
     * @return
     * @since 1.X
     */
    public Boolean validarVigenciaContrasena(Date fechaVencimiento) {
	Date hoy = Calendar.getInstance().getTime();

	if (fechaVencimiento.after(hoy)) {
	    return Boolean.FALSE;
	} else {
	    return Boolean.TRUE;
	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param contrasena
     * @param parametro
     * @throws UsuarioServiceException
     * @since 1.X
     */
    private void validacionesGenericas(Contrasena contrasena,
	    Parametros parametro) throws UsuarioServiceException {

	if (parametro.getParametro().equals(
		ParametrosType.PERMITE_ESPACIOS_BLANCOS)
		&& parametro.getValor().equalsIgnoreCase(NO_PERMITE)) {
	    PasswordUtils.validarEspaciosEnBlanco(contrasena.getPassword());
	}
	
	// 4- Validar tipo de clave
		if (parametro.getParametro().equals(ParametrosType.TIPO_CLAVE)) {
		    PasswordUtils.validarTipo(contrasena.getPassword(),
			    parametro.getValor());
		}

	// validar largo minimo de contrasena
	if (parametro.getParametro().equals(ParametrosType.LARGO_MINIMO)) {
	    PasswordUtils.validarLargoMinimo(contrasena.getPassword(),
		    Integer.parseInt(parametro.getValor()));
	}

	// validar largo max de contrasena
	if (parametro.getParametro().equals(ParametrosType.LARGO_MAXIMO)) {
	    PasswordUtils.validarLargoMaximo(contrasena.getPassword(),
		    Integer.parseInt(parametro.getValor()));
	}
	

	// 5- Validar caracteres especiales
	if (parametro.getParametro().equals(
		ParametrosType.OBLIGA_CARACT_ESPECIALES)
		&& parametro.getValor().equalsIgnoreCase(NO_PERMITE)) {
	    PasswordUtils.validarCaracteresEspeciales(contrasena.getPassword());
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param politicaDAO
     * @see com.acl.service.logic.IContrasena#setPoliticaDAO(com.acl.dao.ParametrosDAO)
     * @since 1.X
     */
    @Override
    public void setPoliticaDAO(ParametrosDAO politicaDAO) {
	this.politicaDAO = politicaDAO;

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param contrasenaByUsuarioDAO
     * @see com.acl.service.logic.IContrasena#setContrasenaByUsuarioDAO(com.acl.dao.ContrasenaByUsuarioDAO)
     * @since 1.X
     */
    @Override
    public void setContrasenaByUsuarioDAO(
	    ContrasenaByUsuarioDAO contrasenaByUsuarioDAO) {
	this.contrasenaByUsuarioDAO = contrasenaByUsuarioDAO;

    }

}