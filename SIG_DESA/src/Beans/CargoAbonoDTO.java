package Beans;

import java.io.Serializable;

import types.CargoAbonoStatusType;
import types.MoneyType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class CargoAbonoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5870066928961138860L;
	private long sid;
	private long transaccion;
	private long sidUsuario;
	private String fechaCargoAbono;
	private String fechaActualizacion;
	private MoneyType tipoMoneda;
	private long monto;
	private long idTipoCargoAbono;
	private CargoAbonoStatusType estadoProceso;

	public long getSid() {
		return sid;
	}

	public void setSid(long sid) {
		this.sid = sid;
	}

	public long getTransaccion() {
		return transaccion;
	}

	public void setTransaccion(long transaccion) {
		this.transaccion = transaccion;
	}

	public String getFechaCargoAbono() {
		return fechaCargoAbono;
	}

	public void setFechaCargoAbono(String fechaCargoAbono) {
		this.fechaCargoAbono = fechaCargoAbono;
	}

	public String getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(String fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public MoneyType getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(MoneyType tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	public long getMonto() {
		return monto;
	}

	public void setMonto(long monto) {
		this.monto = monto;
	}

	public long getIdTipoCargoAbono() {
		return idTipoCargoAbono;
	}

	public void setIdTipoCargoAbono(long idTipoCargoAbono) {
		this.idTipoCargoAbono = idTipoCargoAbono;
	}

	public CargoAbonoStatusType getEstadoProceso() {
		return estadoProceso;
	}

	public void setEstadoProceso(CargoAbonoStatusType estadoProceso) {
		this.estadoProceso = estadoProceso;
	}

	public long getSidUsuario() {
		return sidUsuario;
	}

	public void setSidUsuario(long sidUsuario) {
		this.sidUsuario = sidUsuario;
	}

	@Override
	public String toString() {
		return "CargoAbonoDTO [sid=" + sid + ", transaccion=" + transaccion
				+ ", sidUsuario=" + sidUsuario + ", fechaCargoAbono="
				+ fechaCargoAbono + ", fechaActualizacion="
				+ fechaActualizacion + ", tipoMoneda=" + tipoMoneda
				+ ", monto=" + monto + ", idTipoCargoAbono=" + idTipoCargoAbono
				+ ", estadoProceso=" + estadoProceso + "]";
	}

}
