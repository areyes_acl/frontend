package Beans;

import java.io.Serializable;

import types.EstadoType;
import types.ParametrosType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Parametros implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private Long sid;
    private String nombre;
    private String valor;
    private Log log = new Log();
    private EstadoType estado;
    private ParametrosType parametro;

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	boolean st=false;
	for (ParametrosType x:ParametrosType.values()){
	    if (x.name().equals(nombre)){
		parametro=x;
		st=true;
	    }
	}
	if (!st) {
	    parametro=ParametrosType.OTHER;
	}
	this.nombre = nombre;
    }

    public String getValor() {
	return valor;
    }

    public void setValor(String valor) {
	this.valor = valor;
    }

    public Log getLog() {
	return log;
    }

    public void setLog(Log log) {
	this.log = log;
    }

    public EstadoType getEstado() {
	return estado;
    }

    public void setEstado(EstadoType estado) {
	this.estado = estado;
    }

    public ParametrosType getParametro() {
	return parametro;
    }

    public void setParametro(ParametrosType parametro) {
	this.parametro = parametro;
    }

    @Override
    public String toString() {
	return "Parametros [sid=" + sid + ", nombre=" + nombre + ", valor="
		+ valor + ", log=" + log + ", estado=" + estado
		+ ", parametro=" + parametro + "]";
    }

}