package Beans;

import java.io.Serializable;

import actions.LogBean;

import types.VentaType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PerdidaBean implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = -1800091020449975216L;
	private String fechaRegistro;
	private String monto;
	private String numeroIncidente;
	private String motivo;
	private String usuario;
	private String numeroTarjeta;
	private String fechaTransaccion;
	private String comercio;
	private VentaType tipoVenta;
	private LogBean log = new LogBean();

	public VentaType getTipoVenta() {
		return tipoVenta;
	}

	public void setTipoVenta(VentaType tipoVenta) {
		this.tipoVenta = tipoVenta;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getNumeroIncidente() {
		return numeroIncidente;
	}

	public void setNumeroIncidente(String numeroIncidente) {
		this.numeroIncidente = numeroIncidente;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getFechaTransaccion() {
		return fechaTransaccion;
	}

	public void setFechaTransaccion(String fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}

	public String getComercio() {
		return comercio;
	}

	public void setComercio(String comercio) {
		this.comercio = comercio;
	}

	public LogBean getLog() {
		return log;
	}

	public void setLog(LogBean log) {
		this.log = log;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo para mostrar el corto de la descripcion
	 * 
	 * @return
	 * @since 1.X
	 */
	public String getShortDescription() {
		if (this.motivo != null && this.motivo.length() > 10) {
			return motivo.substring(0, 10).concat("...");
		} else if (this.motivo != null && this.motivo.length() <= 10) {
			return motivo;
		}
		return "";
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * METODO QUE SETEA EL TEXTO CON HTML PARA PODER MOSTRAR LA DESCRIPCION
	 * 
	 * @return
	 * @since 1.X
	 */
	public String getLargeDescription() {
		if (this.motivo != null) {
			return this.motivo.replaceAll("\\r\\n", "<br>");
		} else {
			return this.motivo;
		}
	}

	@Override
	public String toString() {
		return "PerdidaBean [fechaRegistro=" + fechaRegistro + ", monto="
				+ monto + ", numeroIncidente=" + numeroIncidente + ", motivo="
				+ motivo + ", usuario=" + usuario + ", numeroTarjeta="
				+ numeroTarjeta + ", fechaTransaccion=" + fechaTransaccion
				+ ", comercio=" + comercio + ", tipoVenta=" + tipoVenta
				+ ", log=" + log + "]";
	}

	

}
