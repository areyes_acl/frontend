package Beans;

import java.io.Serializable;

import cl.util.Utils;

import types.StatusPagoType;
import actions.LogBean;

/**
 * 
 * @author afreire
 * 
 */
public class PagoContableBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6838175060136945537L;
	private Long sid;
	private StatusPagoType estadoPago;
	private String fechaPago;
	private Long montoBruto;
	private Long montoDescuento;
	private Long montoFinal;
	private Long sidUsuario;
	private String filenameAdjuntoImagen;
	private String filenameAdjuntoExcel;
	private String motivoRechazo;
	private int operador;
	private LogBean log = new LogBean();

	public Long getSid() {
	    return sid;
	}

	public void setSid(Long sid) {
	    this.sid = sid;
	}

	public StatusPagoType getEstadoPago() {
	    return estadoPago;
	}

	public void setEstadoPago(StatusPagoType estadoPago) {
	    this.estadoPago = estadoPago;
	}

	public String getFechaPago() {
	    return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
	    this.fechaPago = fechaPago;
	}

	public Long getMontoBruto() {
	    return montoBruto;
	}

	public void setMontoBruto(Long montoBruto) {
	    this.montoBruto = montoBruto;
	}
	
	public Long getMontoDescuento() {
	    return montoDescuento;
	}

	public void setMontoDescuento(Long montoDescuento) {
	    this.montoDescuento = montoDescuento;
	}

	public Long getMontoFinal() {
	    return montoFinal;
	}

	public void setMontoFinal(Long montoFinal) {
	    this.montoFinal = montoFinal;
	}

	public Long getSidUsuario() {
	    return sidUsuario;
	}

	public void setSidUsuario(Long sidUsuario) {
	    this.sidUsuario = sidUsuario;
	}

	public String getFilenameAdjuntoImagen() {
	    return filenameAdjuntoImagen;
	}

	public void setFilenameAdjuntoImagen(String filenameAdjuntoImagen) {
	    this.filenameAdjuntoImagen = filenameAdjuntoImagen;
	}

	public String getFilenameAdjuntoExcel() {
	    return filenameAdjuntoExcel;
	}

	public void setFilenameAdjuntoExcel(String filenameAdjuntoExcel) {
	    this.filenameAdjuntoExcel = filenameAdjuntoExcel;
	}

	public String getMotivoRechazo() {
	    return motivoRechazo;
	}

	public void setMotivoRechazo(String motivoRechazo) {
	    this.motivoRechazo = motivoRechazo;
	}

	public LogBean getLog() {
	    return log;
	}

	public void setLog(LogBean log) {
	    this.log = log;
	}
	
	public String getMontoFormateado(){
	    String numero = Utils.formateaMonto(String.valueOf(this.montoBruto*100));
	    return numero;
	}
	
	public String getDescuentoFormateado(){
	    String numero = Utils.formateaMonto(String.valueOf(this.montoDescuento*100));
	    return numero;
	}

	public String getMontoFinalFormateado(){
	    String numero = Utils.formateaMonto(String.valueOf(this.montoFinal*100));
	    return numero;
	}
	
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo para mostrar el corto de la descripcion
	 * 
	 * @return
	 * @since 1.X
	 */
	public String getShortDescription() {
	    if (this.motivoRechazo != null && this.motivoRechazo.length() > 10) {
		return motivoRechazo.substring(0, 10).concat("...");
	    } else if (this.motivoRechazo != null && this.motivoRechazo.length() <= 10) {
		return motivoRechazo;
	    }
	    return "";
	}
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * METODO QUE SETEA EL TEXTO CON HTML PARA PODER MOSTRAR LA DESCRIPCION
	 * 
	 * @return
	 * @since 1.X
	 */
	public String getLargeDescription() {
	    if (this.motivoRechazo != null) {
		return this.motivoRechazo.replaceAll("\\r\\n", "<br>");
	    } else {
		return this.motivoRechazo;
	    }
	}

	@Override
	public String toString() {
	    return "PagoContableBean [sid=" + sid + ", estadoPago="
		    + estadoPago + ", fechaPago=" + fechaPago + ", montoBruto="
		    + montoBruto + ", montoDescuento=" + montoDescuento
		    + ", montoFinal=" + montoFinal + ", sidUsuario="
		    + sidUsuario + ", filenameAdjuntoImagen="
		    + filenameAdjuntoImagen + ", filenameAdjuntoExcel="
		    + filenameAdjuntoExcel + ", motivoRechazo=" + motivoRechazo
		    + ", log=" + log + ", operador=" + operador + "]";
	}
	
	public int getOperador() {
	    return operador;
	}

	public void setOperador(int operador) {
	    this.operador = operador;
	}
}

