package Beans;

import java.io.Serializable;

public class ObjecionRechazoDiaBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ObjecionRechazoDiaBean()
	{
		//Constructor sin argumentos
	}
	
	private String mit;
	private String codigoFuncion;
	private String numeroTarjeta;
	private String fechaTransac;
	private String comercio;
	private String pais;
	private String montoTransac;
	private String montoFacturacion;
	private String codRazon;
	private String microFilm;
	private String codigoAutorizacion;
	private String fechaEfectiva;
	private String fechaProceso;
	private String binAdquiriente;
	private String leeBanda;
	private String estadoTrx;
	private String otrosDatos1;
	private String otrosDatos2;
	private String otrosDatos3;
	private String otrosDatos4;
	private String codMonedaTrx;
	private String rubroComercio;
	private String sid;
	private String glosaGeneral;
	private String referencia;
	private String montoConciliacion;
	private String error;
	private String patpass;
	private String operador;
	
	//Paginacion
	
	private String pagActual;
	private String pagCambio;
	private String total;
	
	public String getMit() {
		return mit;
	}
	public void setMit(String mit) {
		this.mit = mit;
	}
	public String getCodigoFuncion() {
		return codigoFuncion;
	}
	public void setCodigoFuncion(String codigoFuncion) {
		this.codigoFuncion = codigoFuncion;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getFechaTransac() {
		return fechaTransac;
	}
	public void setFechaTransac(String fechaTransac) {
		this.fechaTransac = fechaTransac;
	}
	public String getComercio() {
		return comercio;
	}
	public void setComercio(String comercio) {
		this.comercio = comercio;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getMontoTransac() {
		return montoTransac;
	}
	public void setMontoTransac(String montoTransac) {
		this.montoTransac = montoTransac;
	}
	public String getMontoFacturacion() {
		return montoFacturacion;
	}
	public void setMontoFacturacion(String montoFacturacion) {
		this.montoFacturacion = montoFacturacion;
	}
	public String getCodRazon() {
		return codRazon;
	}
	public void setCodRazon(String codRazon) {
		this.codRazon = codRazon;
	}
	public String getMicroFilm() {
		return microFilm;
	}
	public void setMicroFilm(String microFilm) {
		this.microFilm = microFilm;
	}
	public String getCodigoAutorizacion() {
		return codigoAutorizacion;
	}
	public void setCodigoAutorizacion(String codigoAutorizacion) {
		this.codigoAutorizacion = codigoAutorizacion;
	}
	public String getFechaEfectiva() {
		return fechaEfectiva;
	}
	public void setFechaEfectiva(String fechaEfectiva) {
		this.fechaEfectiva = fechaEfectiva;
	}
	public String getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(String fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public String getBinAdquiriente() {
		return binAdquiriente;
	}
	public void setBinAdquiriente(String binAdquiriente) {
		this.binAdquiriente = binAdquiriente;
	}
	public String getLeeBanda() {
		return leeBanda;
	}
	public void setLeeBanda(String leeBanda) {
		this.leeBanda = leeBanda;
	}
	public String getEstadoTrx() {
		return estadoTrx;
	}
	public void setEstadoTrx(String estadoTrx) {
		this.estadoTrx = estadoTrx;
	}
	public String getOtrosDatos1() {
		return otrosDatos1;
	}
	public void setOtrosDatos1(String otrosDatos1) {
		this.otrosDatos1 = otrosDatos1;
	}
	public String getOtrosDatos2() {
		return otrosDatos2;
	}
	public void setOtrosDatos2(String otrosDatos2) {
		this.otrosDatos2 = otrosDatos2;
	}
	public String getOtrosDatos3() {
		return otrosDatos3;
	}
	public void setOtrosDatos3(String otrosDatos3) {
		this.otrosDatos3 = otrosDatos3;
	}
	public String getOtrosDatos4() {
		return otrosDatos4;
	}
	public void setOtrosDatos4(String otrosDatos4) {
		this.otrosDatos4 = otrosDatos4;
	}
	public String getCodMonedaTrx() {
		return codMonedaTrx;
	}
	public void setCodMonedaTrx(String codMonedaTrx) {
		this.codMonedaTrx = codMonedaTrx;
	}
	public String getRubroComercio() {
		return rubroComercio;
	}
	public void setRubroComercio(String rubroComercio) {
		this.rubroComercio = rubroComercio;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getGlosaGeneral() {
		return glosaGeneral;
	}
	public void setGlosaGeneral(String glosaGeneral) {
		this.glosaGeneral = glosaGeneral;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getMontoConciliacion() {
		return montoConciliacion;
	}
	public void setMontoConciliacion(String montoConciliacion) {
		this.montoConciliacion = montoConciliacion;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	//Paginacion
	public String getPagActual() {
		return pagActual;
	}
	public void setPagActual(String pagActual) {
		this.pagActual = pagActual;
	}
	public String getPagCambio() {
		return pagCambio;
	}
	public void setPagCambio(String pagCambio) {
		this.pagCambio = pagCambio;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getPatpass() {
	    return patpass;
	}
	public void setPatpass(String patpass) {
	    this.patpass = patpass;
	}
	public String getOperador() {
	    return operador;
	}
	public void setOperador(String operador) {
	    this.operador = operador;
	}
	
}
