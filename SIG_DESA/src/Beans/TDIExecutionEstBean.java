package Beans;

import java.io.Serializable;

public class TDIExecutionEstBean implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 3532155344728065735L;
    private Long sid;
    private String estado;
    
	
	public TDIExecutionEstBean(Long sid, String estado) {
		super();
		this.sid = sid;
		this.estado = estado;
	}
	
	
	
	public TDIExecutionEstBean() {
		super();
	}



	public Long getSid() {
		return sid;
	}
	public void setSid(Long sid) {
		this.sid = sid;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

}
