package Beans;

public class OtraGestionBean {

	private Integer refTransaccion;
	private Integer refTpGestion;
	private Integer refUsuario;
	private String glosa;
	private String numeroIncidente;

	public OtraGestionBean() {
	}

	public Integer getRefTransaccion() {
		return refTransaccion;
	}

	public void setRefTransaccion(Integer refTransaccion) {
		this.refTransaccion = refTransaccion;
	}

	public Integer getRefTpGestion() {
		return refTpGestion;
	}

	public void setRefTpGestion(Integer refTpGestion) {
		this.refTpGestion = refTpGestion;
	}

	public Integer getRefUsuario() {
		return refUsuario;
	}

	public void setRefUsuario(Integer refUsuario) {
		this.refUsuario = refUsuario;
	}

	public String getGlosa() {
		return glosa;
	}

	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}

	public String getNumeroIncidente() {
		return numeroIncidente;
	}

	public void setNumeroIncidente(String numeroIncidente) {
		this.numeroIncidente = numeroIncidente;
	}

    @Override
    public String toString() {
        return "OtraGestionBean [refTransaccion=" + refTransaccion
                + ", refTpGestion=" + refTpGestion + ", refUsuario="
                + refUsuario + ", glosa=" + glosa + ", numeroIncidente="
                + numeroIncidente + "]";
    }

}
