package Beans;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Rol implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private Long sid;
    private String nombre;
    private String descripcion;
    private Log log = new Log();
    private Boolean activo;

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    public Log getLog() {
	return log;
    }

    public void setLog(Log log) {
	this.log = log;
    }

    public Boolean getActivo() {
	return activo;
    }

    public void setActivo(Boolean activo) {
	this.activo = activo;
    }

    @Override
    public String toString() {
	return "Rol [sid=" + sid + ", nombre=" + nombre + ", descripcion="
		+ descripcion + ", log=" + log + ", activo=" + activo + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((sid == null) ? 0 : sid.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Rol other = (Rol) obj;
	if (sid == null) {
	    if (other.sid != null)
		return false;
	} else if (!sid.equals(other.sid))
	    return false;
	return true;
    }

}