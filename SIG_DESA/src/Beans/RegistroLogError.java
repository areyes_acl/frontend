package Beans;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RegistroLogError implements Serializable, RegistroLog {

    /**
     * 
     */
    private static final long serialVersionUID = 1375269774952440961L;
    private long sid;
    private String fechaInsercion;
    private String nombreSp;
    private String msgError;

    public long getSid() {
	return sid;
    }

    public void setSid(long sid) {
	this.sid = sid;
    }

    public String getFechaInsercion() {
	return fechaInsercion;
    }

    public void setFechaInsercion(String fechaInsercion) {
	this.fechaInsercion = fechaInsercion;
    }

    public String getNombreSp() {
	return nombreSp;
    }

    public void setNombreSp(String nombreSp) {
	this.nombreSp = nombreSp;
    }

    public String getMsgError() {
	return msgError;
    }

    public void setMsgError(String msgError) {
	this.msgError = msgError;
    }

    @Override
    public String toString() {
	return "RegistroLogError [sid=" + sid + ", fechaInsercion="
		+ fechaInsercion + ", nombreSp=" + nombreSp + ", msgError="
		+ msgError + "]";
    }

}
