package Beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TDIExecutionLogBean implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 3532155344728065735L;
    private Long sid;
    private String proceso;
    private String archivo;
    private String archivoExt;
    private String controlExt;
    private Date horaInicio;
    private Date horaTermino;
    private Integer deltaEjecucion;
    private String estado;
    private String descripcionError;
    private TDIJobBean tdiJob;
    private int intento;
    private String origen;
    private String destino;

    private TDIExecutionEstBean estadoBean;

    public TDIExecutionLogBean() {
	super();
    }

    public TDIExecutionEstBean getEstadoBean() {
	return estadoBean;
    }

    public void setEstadoBean(TDIExecutionEstBean estadoBean) {
	this.estadoBean = estadoBean;
    }

    public String getOrigen() {
	return origen;
    }

    public void setOrigen(String origen) {
	this.origen = origen;
    }

    public String getDestino() {
	return destino;
    }

    public void setDestino(String destino) {
	this.destino = destino;
    }

    public String getArchivoExt() {
	return archivoExt;
    }

    public void setArchivoExt(String archivoExt) {
	this.archivoExt = archivoExt;
    }

    public String getControlExt() {
	return controlExt;
    }

    public void setControlExt(String controlExt) {
	this.controlExt = controlExt;
    }

    public TDIExecutionLogBean(Long sid, String proceso, String archivo,
	    String archivoExt, String controlExt, Date horaInicio,
	    Date horaTermino, Integer deltaEjecucion, String estado,
	    String descripcionError, TDIJobBean tdiJob, int intento) {
	super();
	this.sid = sid;
	this.proceso = proceso;
	this.archivo = archivo;
	this.archivoExt = archivoExt;
	this.controlExt = controlExt;
	this.horaInicio = horaInicio;
	this.horaTermino = horaTermino;
	this.deltaEjecucion = deltaEjecucion;
	this.estado = estado;
	this.descripcionError = descripcionError;
	this.tdiJob = tdiJob;
	this.intento = intento;
    }

    public int getIntento() {
	return intento;
    }

    public void setIntento(int intento) {
	this.intento = intento;
    }

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getProceso() {
	return proceso;
    }

    public void setProceso(String proceso) {
	this.proceso = proceso;
    }

    public String getArchivo() {
	return archivo;
    }

    public void setArchivo(String archivo) {
	this.archivo = archivo;
    }

    public Date getHoraInicio() {
	return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
	this.horaInicio = horaInicio;
    }

    public String getHoraInicioShow() {
	String valor = "";
	SimpleDateFormat hourFormat = new SimpleDateFormat(
		"dd-MM-yyyy HH:mm:ss");
	if (this.horaInicio != null) {
	    valor = hourFormat.format(this.horaInicio);
	}
	return valor;
    }

    public String getHoraTerminoShow() {
	String valor = "";
	SimpleDateFormat hourFormat = new SimpleDateFormat(
		"dd-MM-yyyy HH:mm:ss");
	if (this.horaTermino != null) {
	    valor = hourFormat.format(this.horaTermino);
	}
	return valor;
    }

    public Date getHoraTermino() {
	return horaTermino;
    }

    public void setHoraTermino(Date horaTermino) {
	this.horaTermino = horaTermino;
    }

    public Integer getDeltaEjecucion() {
	return deltaEjecucion;
    }

    public String getDeltaEjecucionShow() {
	String valor = "";
	if (this.deltaEjecucion != null) {
	    valor = (this.deltaEjecucion) + " s";

	    if (this.deltaEjecucion > 59) {
		int calculo = this.deltaEjecucion * 60;
		valor = (calculo == 1) ? calculo + " minuto" : calculo + " m";
	    }
	}
	return valor;
    }

    public void setDeltaEjecucion(Integer deltaEjecucion) {
	this.deltaEjecucion = deltaEjecucion;
    }

    public String getDescripcionError() {
	return descripcionError;
    }

    public String getDescripcionErrorShow() {
	String valor = "";
	if (!"ENTREGADO".equals(estado)) {
	    valor = descripcionError;
	}
	return valor;
    }

    public void setDescripcionError(String descripcionError) {
	this.descripcionError = descripcionError;
    }

    public TDIJobBean getTdiJob() {
	return tdiJob;
    }

    public void setTdiJob(TDIJobBean tdiJob) {
	this.tdiJob = tdiJob;
    }

    public String getEstado() {
	return estado;
    }

    public void setEstado(String estado) {
	this.estado = estado;
    }

    public String[] getArchivoReal() {
	String[] valores = { this.archivo + "." + this.archivoExt };
	if ("ENTREGADO".equals(this.estado) && this.descripcionError != null) {
	    valores = this.descripcionError.split(";");
	}
	return valores;
    }

    public String getArchivoControlReal() {
	return this.archivo + "." + this.controlExt;
    }

    public String getJsonData() {
	return "{\"hora_termino\": \"" + this.getHoraTerminoShow()
		+ "\", \"delta\":\"" + this.getDeltaEjecucionShow()
		+ "\", \"estado\":\"" + this.getEstado() + "\", \"error\":\""
		+ this.getDescripcionError() + "\" }";
    }

}
