package Beans;

import java.io.Serializable;


public class TDIConexionTipoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3532155344728065735L;
	private Long sid;
	private String tipo;
	
	
	public TDIConexionTipoBean() {
		super();
	}


	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}


	public String getTipo() {
	    return tipo;
	}


	public void setTipo(String tipo) {
	    this.tipo = tipo;
	}

	public TDIConexionTipoBean(Long sid) {
	    super();
	    this.sid = sid;
	}

	public TDIConexionTipoBean(Long sid, String tipo) {
	    super();
	    this.sid = sid;
	    this.tipo = tipo;
	}

	

}
