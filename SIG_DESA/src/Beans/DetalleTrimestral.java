package Beans;

import java.io.Serializable;

import types.IType;

/**
 * 
 * @author dmedina
 * 
 */
public class DetalleTrimestral implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IType tipo;
	private long cantidad;
	private String monto;
	private String descripcionCant;
	private String descripcionMont;

	public IType getTipo() {
		return tipo;
	}

	public long getCantidad() {
		return cantidad;
	}

	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public void setTipo(IType tipo) {
		this.tipo = tipo;
	}

	public void setDescripcionCant(String descripcionCant) {
		this.descripcionCant = descripcionCant;
	}

	public void setDescripcionMont(String descripcionMont) {
		this.descripcionMont = descripcionMont;
	}

	public String getDescripcionMont() {
		return descripcionMont;
	}

	public String getDescripcionCant() {
		return descripcionCant;
	}

	@Override
	public String toString() {
		return "InfoTrimestral [tipo=" + tipo + ", cantidad=" + cantidad
				+ ", monto=" + monto + "]";
	}

}
