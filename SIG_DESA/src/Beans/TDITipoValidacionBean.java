package Beans;

import java.io.Serializable;


public class TDITipoValidacionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3532155344728065735L;
	private Long sid;
	private String detalle;
	private int clasif;
	private String validaOD;
	
	
	public TDITipoValidacionBean() {
		super();
	}


	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}


	public String getDetalle() {
	    return detalle;
	}


	public void setDetalle(String detalle) {
	    this.detalle = detalle;
	}

	public int getClasif() {
	    return clasif;
	}


	public void setClasif(int clasif) {
	    this.clasif = clasif;
	}


	public String getValidaOD() {
	    return validaOD;
	}


	public void setValidaOD(String validaOD) {
	    this.validaOD = validaOD;
	}


	public TDITipoValidacionBean(Long sid) {
	    super();
	    this.sid = sid;
	}
	
	public TDITipoValidacionBean(Long sid, String detalle) {
	    super();
	    this.sid = sid;
	    this.detalle = detalle;
	}
	
	public TDITipoValidacionBean(Long sid, String detalle, int clasif, String validaOD) {
	    super();
	    this.sid = sid;
	    this.detalle = detalle;
	    this.clasif = clasif;
	    this.validaOD = validaOD;
	}

	

}
