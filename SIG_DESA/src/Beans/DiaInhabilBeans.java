package Beans;

import java.io.Serializable;

/**
 * 
 * @author Carlos Alarcon
 * 
 */
public class DiaInhabilBeans implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3532155344728065735L;
	private Long sid;
	private String glosa;
	private String fecha;
	private Integer estado;
	
	public DiaInhabilBeans() {
		super();
	}

	public DiaInhabilBeans(Long sid, String glosa, String fecha, Integer estado) {
		super();
		this.sid = sid;
		this.glosa = glosa;
		this.fecha = fecha;
		this.estado = estado;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public String getGlosa() {
		return glosa;
	}

	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sid == null) ? 0 : sid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiaInhabilBeans other = (DiaInhabilBeans) obj;
		if (sid == null) {
			if (other.sid != null)
				return false;
		} else if (!sid.equals(other.sid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DiaInhabil [sid=" + sid + ", glosa=" + glosa + ", fecha="
				+ fecha + ", estado=" + estado + "]";
	}
	
	

}
