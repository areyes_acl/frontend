package Beans;

import java.io.Serializable;

public class ContableAvancesBean implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String id;
    private String fechaTs;
    private String fileName;
    private String estado;
    private String fecha;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getFechaTs() {
        return fechaTs;
    }
    public void setFechaTs(String fechaTs) {
        this.fechaTs = fechaTs;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getFecha() {
        return fecha;
    }
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    @Override
    public String toString() {
	return "ContableAvancesBean [id=" + id + ", fechaTs=" + fechaTs
		+ ", fileName=" + fileName + ", estado=" + estado + ", fecha="
		+ fecha + "]";
    }
        
}
