package Beans;

import java.io.Serializable;

public class CargaMasivaDTO implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5870066928961138860L;
	private long sid;
	private String comentario;
	private long gestion;
	private long idUser;
	
	public long getSid() {
		return sid;
	}

	public void setSid(long sid) {
		this.sid = sid;
	}

	public String getComentario() {
	    return comentario;
	}

	public void setComentario(String comentario) {
	    this.comentario = comentario;
	}

	public long getGestion() {
	    return gestion;
	}

	public void setGestion(long gestion) {
	    this.gestion = gestion;
	}

	public long getIdUser() {
	    return idUser;
	}

	public void setIdUser(long idUser) {
	    this.idUser = idUser;
	}

	@Override
	public String toString() {
	    return "CargaMasivaDTO [sid=" + sid + ", comentario=" + comentario
		    + ", gestion=" + gestion + ", idUser=" + idUser + "]";
	}


	
}


