/**
 * 
 */
package Beans;

import types.AccionType;
import clases.ContrasenaByUsuarioDAO;
import clases.ParametrosDAO;
import Beans.Usuario;
import exception.UsuarioServiceException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface IContrasena {

    public void validarContrasena(Usuario usuario, AccionType accion)
	    throws UsuarioServiceException;

    public void setPoliticaDAO(ParametrosDAO politicaDAO);

    public void setContrasenaByUsuarioDAO(
	    ContrasenaByUsuarioDAO contrasenaByUsuarioDAO);
}
