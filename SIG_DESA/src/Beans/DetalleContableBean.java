package Beans;

import java.io.Serializable;

import cl.util.Utils;

import types.StatusPagoType;
import actions.LogBean;

/**
 * 
 * @author afreire
 * 
 */
public class DetalleContableBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6838175060136945537L;
	private Long sid;
	private String fechaContable;
	private String tipoMovimiento;
	private String cuentaDebito;
	private String cuentaCredito;
	private String monto;
	private String subledger;
	private String numeroTarjeta;
	private String microfilm;
	private String tipoVenta;
	private String cantidadCuotas;
	private String nombreArchivo;
	private String operador;
	private LogBean log = new LogBean();

	public Long getSid() {
	    return sid;
	}

	public void setSid(Long sid) {
	    this.sid = sid;
	}

	public String getFechaContable() {
	    return fechaContable;
	}

	public void setFechaContable(String fechaContable) {
	    this.fechaContable = fechaContable;
	}

	public String getTipoMovimiento() {
	    return tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
	    this.tipoMovimiento = tipoMovimiento;
	}
	
	public String getCuentaDebito() {
	    return cuentaDebito;
	}

	public void setCuentaDebito(String cuentaDebito) {
	    this.cuentaDebito = cuentaDebito;
	}

	public String getCuentaCredito() {
	    return cuentaCredito;
	}

	public void setCuentaCredito(String cuentaCredito) {
	    this.cuentaCredito = cuentaCredito;
	}

	public String getMonto() {
	    return monto;
	}

	public void setMonto(String monto) {
	    this.monto = monto;
	}

	public String getSubledger() {
	    return subledger;
	}

	public void setSubledger(String subledger) {
	    this.subledger = subledger;
	}

	public String getNumeroTarjeta() {
	    return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
	    this.numeroTarjeta = numeroTarjeta;
	}

	public String getMicrofilm() {
	    return microfilm;
	}

	public void setMicrofilm(String microfilm) {
	    this.microfilm = microfilm;
	}

	public String getTipoVenta() {
	    return tipoVenta;
	}

	public void setTipoVenta(String tipoVenta) {
	    this.tipoVenta = tipoVenta;
	}

	public String getCantidadCuotas() {
	    return cantidadCuotas;
	}

	public void setCantidadCuotas(String cantidadCuotas) {
	    this.cantidadCuotas = cantidadCuotas;
	}

	public String getNombreArchivo() {
	    return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
	    this.nombreArchivo = nombreArchivo;
	}

	public LogBean getLog() {
	    return log;
	}

	public void setLog(LogBean log) {
	    this.log = log;
	}

	@Override
	public String toString() {
	    return "DetalleContableBean [sid=" + sid + ", fechaContable="
		    + fechaContable + ", tipoMovimiento=" + tipoMovimiento
		    + ", cuentaDebito=" + cuentaDebito + ", cuentaCredito="
		    + cuentaCredito + ", monto=" + monto + ", subledger="
		    + subledger + ", numeroTarjeta=" + numeroTarjeta
		    + ", microfilm=" + microfilm + ", tipoVenta=" + tipoVenta
		    + ", cantidadCuotas=" + cantidadCuotas + ", nombreArchivo="
		    + nombreArchivo + ", log=" + log + "]";
	}

	public String getOperador() {
	    return operador;
	}

	public void setOperador(String operador) {
	    this.operador = operador;
	}
}
