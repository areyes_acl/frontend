package Beans;

import java.io.Serializable;

import types.CargoAbonoStatusType;
import types.CargoAbonoType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class CargoAbonoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -201891567282849286L;
	private long sid;
	private String numeroTarjeta;
	private String fechaTransaccion;
	private String comercio;
	private String montoOriginal;
	private String montoCargoAbono;
	private CargoAbonoType tipo;
	private CargoAbonoStatusType estado;
	private String codRazon;
	private String numIncidente;
	private String username;
	private String fechaCargoAbono;

	public long getSid() {
		return sid;
	}

	public void setSid(long sid) {
		this.sid = sid;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getFechaTransaccion() {
		return fechaTransaccion;
	}

	public void setFechaTransaccion(String fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}

	public String getComercio() {
		return comercio;
	}

	public void setComercio(String comercio) {
		this.comercio = comercio;
	}

	public String getMontoOriginal() {
		return montoOriginal;
	}

	public void setMontoOriginal(String montoOriginal) {
		this.montoOriginal = montoOriginal;
	}

	public String getMontoCargoAbono() {
		return montoCargoAbono;
	}

	public void setMontoCargoAbono(String montoCargoAbono) {
		this.montoCargoAbono = montoCargoAbono;
	}

	public CargoAbonoStatusType getEstado() {
		return estado;
	}

	public void setEstado(CargoAbonoStatusType estado) {
		this.estado = estado;
	}

	public String getCodRazon() {
		return codRazon;
	}

	public void setCodRazon(String codRazon) {
		this.codRazon = codRazon;
	}

	public String getNumIncidente() {
		return numIncidente;
	}

	public void setNumIncidente(String numIncidente) {
		this.numIncidente = numIncidente;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFechaCargoAbono() {
		return fechaCargoAbono;
	}

	public void setFechaCargoAbono(String fechaCargoAbono) {
		this.fechaCargoAbono = fechaCargoAbono;
	}

	public CargoAbonoType getTipo() {
		return tipo;
	}

	public void setTipo(CargoAbonoType tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "CargoAbonoBean [sid=" + sid + ", numeroTarjeta="
				+ numeroTarjeta + ", fechaTransaccion=" + fechaTransaccion
				+ ", comercio=" + comercio + ", montoOriginal=" + montoOriginal
				+ ", montoCargoAbono=" + montoCargoAbono + ", estado=" + estado
				+ ", codRazon=" + codRazon + ", numIncidente=" + numIncidente
				+ ", username=" + username + ", fechaCargoAbono="
				+ fechaCargoAbono + "]";
	}

}
