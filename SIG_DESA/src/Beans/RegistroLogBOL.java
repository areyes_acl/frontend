package Beans;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RegistroLogBOL implements Serializable, RegistroLog {

    /**
     * 
     */
    private static final long serialVersionUID = -2331008140412097727L;
    private long sid;
    private String fecha;
    private String filename;
    private String fileFlag;
    private String fileTsDownload;
    private String fileTsUpload;

    public long getSid() {
	return sid;
    }

    public void setSid(long sid) {
	this.sid = sid;
    }

    public String getFecha() {
	return fecha;
    }

    public void setFecha(String fecha) {
	this.fecha = fecha;
    }

    public String getFilename() {
	return filename;
    }

    public void setFilename(String filename) {
	this.filename = filename;
    }

    public String getFileFlag() {
	return fileFlag;
    }

    public void setFileFlag(String fileFlag) {
	this.fileFlag = fileFlag;
    }

    public String getFileTsDownload() {
	return fileTsDownload;
    }

    public void setFileTsDownload(String fileTsDownload) {
	this.fileTsDownload = fileTsDownload;
    }

    public String getFileTsUpload() {
	return fileTsUpload;
    }

    public void setFileTsUpload(String fileTsUpload) {
	this.fileTsUpload = fileTsUpload;
    }

    @Override
    public String toString() {
	return "RegistroLogBOL [sid=" + sid + ", fecha=" + fecha
		+ ", filename=" + filename + ", fileFlag=" + fileFlag
		+ ", fileTsDownload=" + fileTsDownload + ", fileTsUpload="
		+ fileTsUpload + "]";
    }

}
