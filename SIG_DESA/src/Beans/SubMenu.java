package Beans;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class SubMenu implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long sid;
    private String nombre;
    private String descripcion;
    private Integer posicion;
    private String link;
    private List<PermisosUsuario> listaPermisos;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param sid
     * @param nombre
     * @param descripcion
     * @param posicion
     * @since 1.X
     */
    public SubMenu(Long sid, String nombre, String descripcion, Integer posicion, String link) {
	super();
	this.sid = sid;
	this.nombre = nombre;
	this.descripcion = descripcion;
	this.posicion = posicion;
	this.link = link;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public SubMenu() {
    }

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }
    
    public String getLink() {
	return link;
    }

    public void setLink(String link) {
	this.link = link;
    }

    public Integer getPosicion() {
	return posicion;
    }

    public void setPosicion(Integer posicion) {
	this.posicion = posicion;
    }
    
    public List<PermisosUsuario> getListaPermisos() {
	return listaPermisos;
    }

    public void setListaPermisos(List<PermisosUsuario> listaPermisos) {
	this.listaPermisos = listaPermisos;
    }

    @Override
    public String toString() {
	return "Menu [sid=" + sid + ", nombre=" + nombre + ", descripcion="
		+ descripcion + ", posicion=" + posicion + ", link=" + link + ", listaSubMenus="
		+ listaPermisos + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((sid == null) ? 0 : sid.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	SubMenu other = (SubMenu) obj;
	if (sid == null) {
	    if (other.sid != null)
		return false;
	} else if (!sid.equals(other.sid))
	    return false;
	return true;
    }

}