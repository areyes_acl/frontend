package Beans;

import java.io.Serializable;

public class HistoriaContableAvancesBean implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String id;
    private String fechaRegistro;
    private String sidContable;
    private String usuario;
    private String motivo;
    private String estado;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getFechaRegistro() {
        return fechaRegistro;
    }
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    public String getSidContable() {
        return sidContable;
    }
    public void setSidContable(String sidContable) {
        this.sidContable = sidContable;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getMotivo() {
        return motivo;
    }
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    @Override
    public String toString() {
	return "HistoriaContableAvancesBean [id=" + id + ", fechaRegistro="
		+ fechaRegistro + ", sidContable=" + sidContable + ", usuario="
		+ usuario + ", motivo=" + motivo + ", estado=" + estado + "]";
    }
    
    

}
