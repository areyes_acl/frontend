package Beans;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PerdidaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2523220791789919152L;
    private long sidIncoming;
    private long sidTipoGestion;
    private long sidUsuario;
    private String glosa;
    private String numeroIncidente;

    public long getSidIncoming() {
	return sidIncoming;
    }

    public void setSidIncoming(long sidIncoming) {
	this.sidIncoming = sidIncoming;
    }

    public long getSidTipoGestion() {
	return sidTipoGestion;
    }

    public void setSidTipoGestion(long sidTipoGestion) {
	this.sidTipoGestion = sidTipoGestion;
    }

    public long getSidUsuario() {
	return sidUsuario;
    }

    public void setSidUsuario(long sidUsuario) {
	this.sidUsuario = sidUsuario;
    }

    public String getGlosa() {
	return glosa;
    }

    public void setGlosa(String glosa) {
	this.glosa = glosa;
    }

    public String getNumeroIncidente() {
	return numeroIncidente;
    }

    public void setNumeroIncidente(String numeroIncidente) {
	this.numeroIncidente = numeroIncidente;
    }

    @Override
    public String toString() {
	return "PerdidaDTO [sidIncoming=" + sidIncoming + ", sidTipoGestion="
		+ sidTipoGestion + ", sidUsuario=" + sidUsuario + ", glosa="
		+ glosa + ", numeroIncidente=" + numeroIncidente + "]";
    }

}
