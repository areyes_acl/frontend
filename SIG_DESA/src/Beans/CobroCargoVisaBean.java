package Beans;

import java.io.Serializable;

public class CobroCargoVisaBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private int sid;
    private String mit;
    private String codigoFuncion;
    private String destinationBin;
    private String sourceBin;
    private String reasonCode;
    private String countryCode;
    private String eventDate;
    private String accountNumber;
    private String accountNumberExtension;
    private int destinationAmount;
    private String destinationCurrencyCode;
    private int sourceAmount;
    private String sourceCurrencyCode;
    private String messageText;
    private String settlementFlag;
    private String transactionIdentifier;
    private String reserved;
    private String centralProcessingDate;
    private String reimbursementAttribute;
    private String operador;
    private String fechaIncoming;
    private String datosAdicionales;
    private String TransaccionConGestion;
    private String estadoProceso;
    private String descripcionCodRazon;
    private String mitReversa;
    private String usuarioReversa;
    private String mensajeReversa;
    private String fechaReversa;
    private String codigoMotivoReversa;
       
    public int getSid() {
        return sid;
    }
    public void setSid(int sid) {
        this.sid = sid;
    }
    public String getMit() {
        return mit;
    }
    public void setMit(String mit) {
        this.mit = mit;
    }
    public String getCodigoFuncion() {
        return codigoFuncion;
    }
    public void setCodigoFuncion(String codigoFuncion) {
        this.codigoFuncion = codigoFuncion;
    }
    public String getDestinationBin() {
        return destinationBin;
    }
    public void setDestinationBin(String destinationBin) {
        this.destinationBin = destinationBin;
    }
    public String getSourceBin() {
        return sourceBin;
    }
    public void setSourceBin(String sourceBin) {
        this.sourceBin = sourceBin;
    }
    public String getReasonCode() {
        return reasonCode;
    }
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }
    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public String getEventDate() {
        return eventDate;
    }
    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }
    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public String getAccountNumberExtension() {
        return accountNumberExtension;
    }
    public void setAccountNumberExtension(String accountNumberExtension) {
        this.accountNumberExtension = accountNumberExtension;
    }
    public int getDestinationAmount() {
        return destinationAmount;
    }
    public void setDestinationAmount(int destinationAmount) {
        this.destinationAmount = destinationAmount;
    }
    public String getDestinationCurrencyCode() {
        return destinationCurrencyCode;
    }
    public void setDestinationCurrencyCode(String destinationCurrencyCode) {
        this.destinationCurrencyCode = destinationCurrencyCode;
    }
    public int getSourceAmount() {
        return sourceAmount;
    }
    public void setSourceAmount(int sourceAmount) {
        this.sourceAmount = sourceAmount;
    }
    public String getSourceCurrencyCode() {
        return sourceCurrencyCode;
    }
    public void setSourceCurrencyCode(String sourceCurrencyCode) {
        this.sourceCurrencyCode = sourceCurrencyCode;
    }
    public String getMessageText() {
        return messageText;
    }
    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
    public String getSettlementFlag() {
        return settlementFlag;
    }
    public void setSettlementFlag(String settlementFlag) {
        this.settlementFlag = settlementFlag;
    }
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }
    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }
    public String getReserved() {
        return reserved;
    }
    public void setReserved(String reserved) {
        this.reserved = reserved;
    }
    public String getCentralProcessingDate() {
        return centralProcessingDate;
    }
    public void setCentralProcessingDate(String centralProcessingDate) {
        this.centralProcessingDate = centralProcessingDate;
    }
    public String getReimbursementAttribute() {
        return reimbursementAttribute;
    }
    public void setReimbursementAttribute(String reimbursementAttribute) {
        this.reimbursementAttribute = reimbursementAttribute;
    }
    public String getOperador() {
        return operador;
    }
    public void setOperador(String operador) {
        this.operador = operador;
    }
    public String getFechaIncoming() {
        return fechaIncoming;
    }
    public void setFechaIncoming(String fechaIncoming) {
        this.fechaIncoming = fechaIncoming;
    }
    public String getDatosAdicionales() {
        return datosAdicionales;
    }
    public void setDatosAdicionales(String datosAdicionales) {
        this.datosAdicionales = datosAdicionales;
    }
    public String getTransaccionConGestion() {
	return TransaccionConGestion;
    }
    public void setTransaccionConGestion(String transaccionConGestion) {
	TransaccionConGestion = transaccionConGestion;
    }
    public String getEstadoProceso() {
	return estadoProceso;
    }
    public void setEstadoProceso(String estadoProceso) {
	this.estadoProceso = estadoProceso;
    }
    public String getDescripcionCodRazon() {
	return descripcionCodRazon;
    }
    public void setDescripcionCodRazon(String descripcionCodRazon) {
	this.descripcionCodRazon = descripcionCodRazon;
    }
    public String getMitReversa() {
	return mitReversa;
    }
    public void setMitReversa(String mitReversa) {
	this.mitReversa = mitReversa;
    }
    public String getUsuarioReversa() {
	return usuarioReversa;
    }
    public void setUsuarioReversa(String usuarioReversa) {
	this.usuarioReversa = usuarioReversa;
    }
    public String getMensajeReversa() {
	return mensajeReversa;
    }
    public void setMensajeReversa(String mensajeReversa) {
	this.mensajeReversa = mensajeReversa;
    }
    public String getFechaReversa() {
	return fechaReversa;
    }
    public void setFechaReversa(String fechaReversa) {
	this.fechaReversa = fechaReversa;
    }
    public String getCodigoMotivoReversa() {
	return codigoMotivoReversa;
    }
    public void setCodigoMotivoReversa(String codigoMotivoReversa) {
	this.codigoMotivoReversa = codigoMotivoReversa;
    }
    
    @Override
    public String toString() {
	return "CobroCargoVisaBean [sid=" + sid + ", mit=" + mit
		+ ", codigoFuncion=" + codigoFuncion + ", destinationBin="
		+ destinationBin + ", sourceBin=" + sourceBin + ", reasonCode="
		+ reasonCode + ", countryCode=" + countryCode + ", eventDate="
		+ eventDate + ", accountNumber=" + accountNumber
		+ ", accountNumberExtension=" + accountNumberExtension
		+ ", destinationAmount=" + destinationAmount
		+ ", destinationCurrencyCode=" + destinationCurrencyCode
		+ ", sourceAmount=" + sourceAmount + ", sourceCurrencyCode="
		+ sourceCurrencyCode + ", messageText=" + messageText
		+ ", settlementFlag=" + settlementFlag
		+ ", transactionIdentifier=" + transactionIdentifier
		+ ", reserved=" + reserved + ", centralProcessingDate="
		+ centralProcessingDate + ", reimbursementAttribute="
		+ reimbursementAttribute + ", operador=" + operador
		+ ", fechaIncoming=" + fechaIncoming + ", datosAdicionales="
		+ datosAdicionales + ", TransaccionConGestion="
		+ TransaccionConGestion + ", estadoProceso=" + estadoProceso
		+ ", descripcionCodRazon=" + descripcionCodRazon
		+ ", mitReversa=" + mitReversa + ", usuarioReversa="
		+ usuarioReversa + ", mensajeReversa=" + mensajeReversa
		+ ", fechaReversa=" + fechaReversa + ", codigoMotivoReversa="
		+ codigoMotivoReversa + "]";
    }
}
