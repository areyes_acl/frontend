package cl.srv;

import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import cl.util.CommonSrv;

public final class LdapSrv extends CommonSrv {
	
	private static LdapSrv singleINSTANCE = null;
	private String servidor = null;
	private String tipoAuth = null;
	private Properties propiedades = null;
	

	/**
	 * Variable LOG.
	 */
	private final static Logger LOG = Logger.getLogger(LdapSrv.class);

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (LdapSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new LdapSrv();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return LdapSrv retorna instancia del servicio.
	 */
	public static LdapSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private LdapSrv() {
		super();
		setProperties();
		setPropertieServer();
	}

	/**
	 * Metodo setea parametros del servidor LDAP
	 */
	private void setPropertieServer() {
		if (this.propiedades != null) {
			this.servidor = this.propiedades.getProperty("ldap.server");
			this.tipoAuth = this.propiedades.getProperty("ldap.typeAuth");
		}
	}

	/**
	 * Metodo setea el archivo properties en el constructor
	 */
	private void setProperties() {
		this.propiedades = new Properties();
		//			this.propiedades.load(getClass().getResourceAsStream(
//					"/properties/conexion.properties"));
        propiedades = PropertiesConfiguration.getProperty();
	}

	public boolean valCredencialesLdap(final String username,
			final String password) {
		String dn = null;
		boolean flag = false;
		if (this.propiedades != null) {
			dn = this.propiedades.getProperty("ldap.common.name") + username
					+ ","
					+ this.propiedades.getProperty("ldap.dominio");
			flag = ldapAuth(dn, password);
		}
		return flag;

	}

	private boolean ldapAuth(final String dn, final String password) {
		DirContext dc = null;
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, this.servidor);
		env.put(Context.SECURITY_AUTHENTICATION, this.tipoAuth);
		env.put(Context.SECURITY_PRINCIPAL, dn);
		env.put(Context.SECURITY_CREDENTIALS, password);

		try {
			if(!this.servidor.equals("")){
				dc = new InitialDirContext(env);
				return true;
			}else{
				return false;
			}
			
		} catch (NamingException ex) {
			System.out
					.println("Error Autenticando mediante LDAP, Error causado por : "
							+ ex.toString());
			return false;
		}
	}
}
