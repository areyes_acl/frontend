package cl.util;

import java.io.Closeable;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;


/**
 * <p>
 * Clase Servicio Commons.
 * <p>
 * <br/>
 * @author Dante Medina (ACL).
 * @version 1.7
 * @since 2015-08-06
 */
public class CommonSrv {
    /**
     * Var para imprimir en el log.
     */
    private static final Logger LOGGER = Logger.getLogger(CommonSrv.class);

    /**
     * Valida si un string puede ser convertivo a Float.
     * @param obj Objeto a validar.
     * @return True si es posible convertirlo en Float. False si no es posible
     *         covertirlo en Float.
     */
    protected boolean isNumber(final String obj) {
        try {
            if (obj == null) {
                return true;
            } else {
                Float.valueOf(obj);
                return true;
            }
        } catch (Exception ex) {
            LOGGER.debug(ex.getMessage(), ex);
            return false;
        }
    }



 
    /**
     * Calculo del Digito verificador.
     * @param rut RUT.
     * @return Digito.
     */
    protected String getDvRut(final Long rut) {
        if (rut == null) {
            return "";
        }
        long mod = 0;
        long sum = 1;
        long iRut = rut;
        for (; iRut != 0; iRut /= 10) {
            sum = (sum + iRut % 10 * (9 - mod++ % 6)) % 11;
        }
        if (sum == 0) {
            return String.valueOf((char) 75);
        } else {
            return String.valueOf((char) (sum + 47));
        }
    }

    /**
     * Cierra un port de servicio web.
     * @param port Port a cerrar.
     */
    protected void closeWs(final Closeable port) {
        try {
            port.close();
        } catch (IOException ex) {
            LOGGER.error("ERROR AL CERRA WS", ex);
        }
    }

    /**
     * Previene la imprecion de "null".
     * @param value valor.
     * @return salida.
     */
    protected String getStringNull(final String value) {
        if (value == null) {
            return "";
        } else {
            return value;
        }
    }
    
    /**
     * Previene la imprecion de "null".
     * @param value valor.
     * @return salida.
     */
    protected String getStringNull(final int value) {
		String cadena = null;
		try {
			cadena = String.valueOf(value);
		} catch (Exception e) {
			cadena = "";
		}
		return cadena;

	}
    
    /**
     * Previene la imprecion de "null".
     * @param value valor.
     * @return salida.
     */
    protected String getStringNull(final long value) {
		String cadena = null;
		try {
			cadena = String.valueOf(value);
		} catch (Exception e) {
			cadena = "";
		}
		return cadena;

	}
    
    /**
     * Obtiene fecha de acuerdo al formato
     * @param fecha fecha que desea formatear.
     * @param formato formato en que se desplegara la fecha.
     * @return String fecha formateada.
     */
    protected String getDateFormat(final Date fecha, final String formato) {
		DateFormat formatoFecha = new SimpleDateFormat(formato);
		return formatoFecha.format(fecha);
	}
    
    protected String getConcatCadenaCaracter(final int largo, final String valor, final String concatenar){
    	String res = valor;
    	for (int i = valor.length(); i < largo; i++) {
    		res = concatenar + res;
		}
    	return res.substring(0, largo);
    }
}

