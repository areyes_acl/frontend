package cl.util.excel.service;

import java.io.File;
import java.util.List;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * @param <T>
 * 
 *            <p>
 *            <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface ExcelUtils<T> {

    public File export(List<T> list);

}
