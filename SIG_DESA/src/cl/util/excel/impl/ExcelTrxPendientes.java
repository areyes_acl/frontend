package cl.util.excel.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;
import cl.util.Utils;
import cl.util.excel.service.ExcelUtils;
import clases.ExportExcel;
import Beans.TransaccionBean;
import properties.PropertiesConfiguration;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ExcelTrxPendientes implements ExcelUtils<TransaccionBean> {

	static final Logger log = Logger.getLogger(ExcelTrxPendientes.class);
	private String ruta;
	private final String EXTENSION = ".csv";

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public ExcelTrxPendientes() {
	    
		Properties propiedades = new Properties();
		
		propiedades = PropertiesConfiguration.getProperty();
		String directorioExportacion = propiedades.getProperty("exportarexcel");
		String filename = propiedades.getProperty("nombreArchivoExport");
		
		this.ruta = directorioExportacion + filename + EXTENSION;
		
		log.info(ruta +propiedades);
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Mertodo que exporta una lista de elementos
	 * 
	 * @param lista
	 * @return
	 * @since 1.X
	 */
	@Override
	public File export(List<TransaccionBean> lista) {
		List<String[]> dataList = null;
		CSVWriter writer = null;
		File file = null;

		try {

			file = new File(ruta);

			// GENERA CABECERA EXCEL
			String[] header = generateHeader();

			// PARSEO DE DATOS DEL EXCEL
			dataList = parseListToString(lista);

			writer = new CSVWriter(new FileWriter(ruta), ';');
			writer.writeNext(header);
			writer.writeAll(dataList);

		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}

		return file;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de generar la cabecera del archivo cvs
	 * 
	 * @return
	 * @since 1.X
	 */
	private String[] generateHeader() {
		return new String[] { "NUMERO TC", "FECHA_TRANSAC", "TIPO", "COMERCIO",
				"PAIS", "MONTO ORIGINAL", "CODIGO RAZON", "ESTADO",
				"NRO. INCIDENTE", "USUARIO", "FECHA GESTION","PATPASS"};
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo encarado de transaformar una lista de objetos a un registro
	 * 
	 * @param rs
	 * @return
	 * @since 1.X
	 */
	private List<String[]> parseListToString(List<TransaccionBean> listTrx) {
		List<String[]> dataList = new ArrayList<String[]>();

		for (TransaccionBean transaccion : listTrx) {
			String registro = "";
			registro += transaccion.getNumeroTarjeta() + ";";
			registro += transaccion.getFechaTransac() + ";";
			registro += transaccion.getTipoTransc() + ";";
			registro += transaccion.getComercio() + ";";
			registro += transaccion.getPais() + ";";
			registro += Utils.formateaMonto(transaccion.getMontoTransac())+ ";";
			registro += transaccion.getCodRazon() + ";";
			registro += transaccion.getDescEstado() + ";";
			registro += transaccion.getNumIncidente() + ";";
			registro += transaccion.getIdUsuario() + ";";
			registro += transaccion.getFechaGestion() + ";";			
			if (transaccion.getPatpass().equals(" ") || transaccion.getPatpass().equals("") || transaccion.getPatpass().equals("0") || transaccion.getPatpass() == null || transaccion.getOperador().equals("VN") || transaccion.getOperador().equals("VI")) {
		        	registro += "No"+ ";";
			    }else{
				registro += "Si"+ ";";
			    }
			
			dataList.add(registro.split(";"));

		}
		return dataList;
	}

}
