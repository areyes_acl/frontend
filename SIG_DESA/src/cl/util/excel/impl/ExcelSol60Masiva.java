package cl.util.excel.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;
import cl.util.Utils;
import cl.util.excel.service.ExcelUtils;
import Beans.TransaccionBean;
import properties.PropertiesConfiguration;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ExcelSol60Masiva implements ExcelUtils<TransaccionBean> {

	static final Logger log = Logger.getLogger(ExcelSol60Masiva.class);
	private String ruta;
	private final String EXTENSION = ".csv";

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public ExcelSol60Masiva() {
		Properties propiedades = new Properties();
		propiedades = PropertiesConfiguration.getProperty();
		String directorioExportacion = propiedades.getProperty("exportarexcel");
		String filename = propiedades.getProperty("nombreArchivoExport");

		this.ruta = directorioExportacion + filename + EXTENSION;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Mertodo que exporta una lista de elementos
	 * 
	 * @param lista
	 * @return
	 * @since 1.X
	 */
	@Override
	public File export(List<TransaccionBean> lista) {
		List<String[]> dataList = null;
		CSVWriter writer = null;
		File file = null;

		try {

			file = new File(ruta);

			// GENERA CABECERA EXCEL
			String[] header = generateHeader();

			// PARSEO DE DATOS DEL EXCEL
			dataList = parseListToString(lista);

			writer = new CSVWriter(new FileWriter(ruta), ';');
			writer.writeNext(header);
			writer.writeAll(dataList);

		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}

		return file;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de generar la cabecera del archivo cvs
	 * 
	 * @return
	 * @since 1.X
	 */
	private String[] generateHeader() {
		return new String[] { "NUMERO TC", "FECHA_TRANSAC", "COMERCIO", "PAIS",
				"MONTO ORIGINAL", "MONTO", "COD. RAZON", "ESTADO",
				"DESCRIPCION", "NRO. INCIDENTE", "FECHA GESTION", "USUARIO","PATPASS" };
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo encarado de transaformar una lista de objetos a un registro
	 * 
	 * @param rs
	 * @return
	 * @since 1.X
	 */
	private List<String[]> parseListToString(List<TransaccionBean> listTrx) {
		List<String[]> dataList = new ArrayList<String[]>();

		for (TransaccionBean transaccion : listTrx) {
			String registro = "";
			registro += transaccion.getNumeroTarjeta() + ";";
			registro += transaccion.getFechaTransac() + ";";
			registro += transaccion.getComercio() + ";";
			registro += transaccion.getPais() + ";";
			registro += Utils.formateaMonto(transaccion.getMontoTransac())
					+ ";";
			registro += Utils.formateaMonto(transaccion.getMontoFacturacion())
					+ ";";
			registro += transaccion.getCodRazon() + ";";
			registro += transaccion.getEstadoTrx() + ";";
			registro += transaccion.getGlosaGeneral() + ";";
			registro += transaccion.getNumIncidente() + ";";
			registro += transaccion.getFechaGestion() + ";";
			registro += transaccion.getIdUsuario() + ";";
			if (transaccion.getPatpass().equals(" ") || transaccion.getPatpass().equals("") || transaccion.getPatpass().equals("-") || transaccion.getPatpass() == null) {
		        	registro += "No"+ ";";
			    }else{
				registro += "Si"+ ";";
			    }
			
			
			
			dataList.add(registro.split(";"));

		}
		return dataList;
	}

}
