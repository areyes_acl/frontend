package cl.util.excel.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.CargoAbonoBean;
import au.com.bytecode.opencsv.CSVWriter;
import cl.util.Utils;
import cl.util.excel.service.ExcelUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ExcelCargoAbonoPend implements ExcelUtils<CargoAbonoBean> {
	static final Logger log = Logger.getLogger(ExcelCargoAbonoPend.class);
	private String ruta;
	private final String EXTENSION = ".csv";

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public ExcelCargoAbonoPend() {
		Properties propiedades = new Properties();
		propiedades = PropertiesConfiguration.getProperty();
		String directorioExportacion = propiedades.getProperty("exportarexcel");
		String filename = propiedades.getProperty("nombreArchivoExport");

		this.ruta = directorioExportacion + filename + EXTENSION;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @param listaPerdidas
	 * @return
	 * @see cl.util.excel.service.ExcelUtils#exportPerdidas(java.util.List)
	 * @since 1.X
	 */
	@Override
	public File export(List<CargoAbonoBean> lista) {

		List<String[]> dataList = null;
		CSVWriter writer = null;
		File file = null;

		try {

			file = new File(ruta);

			// GENERA CABECERA EXCEL
			String[] header = generateHeader();

			// PARSEO DE DATOS DEL EXCEL
			dataList = parseListToString(lista);

			writer = new CSVWriter(new FileWriter(ruta), ';');
			writer.writeNext(header);
			writer.writeAll(dataList);

		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}

		return file;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de generar la cabecera del archivo cvs
	 * 
	 * @return
	 * @since 1.X
	 */
	private String[] generateHeader() {
		return new String[] { "NUMERO TC", "FECHA_TRANSAC", "COMERCIO",
				"MONTO TRANSACCION", "MONTO CARGO/ABONO", "ESTADO",
				"CODIGO RAZON", "NUMERO INCIDENTE", "TIPO", "USUARIO",
				"FECHA CARGO/ABONO" };
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo encarado de transaformar una lista de objetos a un registro
	 * 
	 * @param rs
	 * @return
	 * @since 1.X
	 */
	private List<String[]> parseListToString(
			List<CargoAbonoBean> listaCargoAbono) {
		List<String[]> dataList = new ArrayList<String[]>();

		for (CargoAbonoBean cargoAboPend : listaCargoAbono) {
			String registro = "";
			registro += cargoAboPend.getNumeroTarjeta() + ";";
			registro += cargoAboPend.getFechaTransaccion() + ";";
			registro += cargoAboPend.getComercio() + ";";
			registro += Utils.formateaMonto(cargoAboPend.getMontoOriginal())+ ";";
			registro += Utils.formateaMonto(cargoAboPend.getMontoCargoAbono())+ ";";
			registro += cargoAboPend.getEstado() + ";";
			registro += cargoAboPend.getCodRazon() + ";";
			registro += cargoAboPend.getNumIncidente() + ";";
			registro += cargoAboPend.getTipo() + ";";
			registro += cargoAboPend.getUsername() + ";";
			registro += cargoAboPend.getFechaCargoAbono() + ";";
			dataList.add(registro.split(";"));

		}
		return dataList;
	}

}
