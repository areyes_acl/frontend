package cl.util.excel.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.LogTableType;
import Beans.RegistroLog;
import Beans.RegistroLogBOL;
import Beans.RegistroLogError;
import Beans.RegistroLogExeptIncoming;
import Beans.RegistroLogGeneric;
import au.com.bytecode.opencsv.CSVWriter;
import cl.util.excel.service.ExcelUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ExcelRegistrosLog implements ExcelUtils<RegistroLog> {

    static final Logger log = Logger.getLogger(ExcelRegistrosLog.class);
    private String ruta;
    private static final String EXTENSION = ".csv";
    private LogTableType tablaLog;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public ExcelRegistrosLog() {
	Properties propiedades = new Properties();
	propiedades = PropertiesConfiguration.getProperty();
	String directorioExportacion = propiedades.getProperty("exportarexcel");
	String filename = propiedades.getProperty("nombreArchivoExport");

	this.ruta = directorioExportacion + filename + EXTENSION;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public ExcelRegistrosLog(LogTableType tablaLog) {
	Properties propiedades = new Properties();
	propiedades = PropertiesConfiguration.getProperty();
	String directorioExportacion = propiedades.getProperty("exportarexcel");
	String filename = propiedades.getProperty("nombreArchivoExport");

	this.ruta = directorioExportacion + filename + EXTENSION;
	this.tablaLog = tablaLog;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param lista
     * @return
     * @see cl.util.excel.service.ExcelUtils#export(java.util.List)
     * @since 1.X
     */
    @Override
    public File export(List<RegistroLog> lista) {
	List<String[]> dataList = null;
	CSVWriter writer = null;
	File file = null;
	FileWriter fileWriter = null;
	try {

	    file = new File(ruta);

	    // GENERA CABECERA EXCEL
	    String[] header = generateHeaderByTableType();

	    // PARSEO DE DATOS DEL EXCEL
	    dataList = parseListToString(lista);
	    fileWriter = new FileWriter(ruta);
	    writer = new CSVWriter(fileWriter, ';');
	    writer.writeNext(header);
	    writer.writeAll(dataList);

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	} finally {
	    if (writer != null) {
		try {
		    writer.close();
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}
	    }
	    if (fileWriter != null) {
		try {
		    fileWriter.close();
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}
	    }
	}

	return file;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private String[] generateHeaderByTableType() {

	String[] header;

	switch (tablaLog) {
	case ERROR:
	    header = new String[] { "SID", "FECHA INSERCION", "NOMBRE SP",
		    "MSG ERROR" };
	    break;

	case EXCEPT_INCOMING:
	    header = new String[] { "SID", "LOG CARGA", "FECHA",
		    "MIT", "COD. FUNCION", "NRO. TARJETA","COD. AUTORIZACION" };
	    break;

	case BOL:
	    header = new String[] { "SID", "FECHA", "FILENAME","FILE_FLAG",
		    "FILE_TS_DOWNLOAD", "FILE_TS_UPLOAD" };
	    break;

	default:
	    header = new String[] { "SID", "FECHA", "FILENAME", "FILE_FLAG",
		    "FILE_TS" };
	    break;
	}

	return header;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param listaRegistrosLogs
     * @return
     * @since 1.X
     */
    private List<String[]> parseListToString(
	    List<RegistroLog> listaRegistrosLogs) {
	List<String[]> dataList;

	switch (tablaLog) {
	case ERROR:
	    dataList = parserLogError(listaRegistrosLogs);
	    break;

	case EXCEPT_INCOMING:
	    dataList = parserExcepIncoming(listaRegistrosLogs);
	    break;

	case BOL:
	    dataList = parserBol(listaRegistrosLogs);
	    break;

	default:
	    dataList = parserGeneric(listaRegistrosLogs);
	    break;
	}

	return dataList;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param dataList
     * @return
     * @since 1.X
     */
    private List<String[]> parserLogError(List<RegistroLog> listaRegistrosLogs) {

	List<String[]> dataList = new ArrayList<String[]>();

	for (RegistroLog registroLog : listaRegistrosLogs) {
	    String registro = "";
	    registro += ((RegistroLogError) registroLog).getSid() + ";";
	    registro += ((RegistroLogError) registroLog).getFechaInsercion()
		    + ";";
	    registro += ((RegistroLogError) registroLog).getNombreSp() + ";";
	    registro += ((RegistroLogError) registroLog).getMsgError() + ";";
	    dataList.add(registro.split(";"));

	}

	return dataList;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param listaRegistrosLogs
     * @param dataList
     * @since 1.X
     */
    private List<String[]> parserGeneric(List<RegistroLog> listaRegistrosLogs) {

	List<String[]> dataList = new ArrayList<String[]>();

	for (RegistroLog registroLog : listaRegistrosLogs) {
	    String registro = "";
	    registro += ((RegistroLogGeneric) registroLog).getSid() + ";";
	    registro += ((RegistroLogGeneric) registroLog).getFecha() + ";";
	    registro += ((RegistroLogGeneric) registroLog).getFilename() + ";";
	    registro += ((RegistroLogGeneric) registroLog).getFileFlag() + ";";
	    registro += ((RegistroLogGeneric) registroLog).getFileTs() + ";";
	    dataList.add(registro.split(";"));

	}
	return dataList;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param listaRegistrosLogs
     * @param dataList
     * @since 1.X
     */
    private List<String[]> parserBol(List<RegistroLog> listaRegistrosLogs) {

	List<String[]> dataList = new ArrayList<String[]>();
	for (RegistroLog registroLog : listaRegistrosLogs) {
	    String registro = "";
	    registro += ((RegistroLogBOL) registroLog).getSid() + ";";
	    registro += ((RegistroLogBOL) registroLog).getFecha() + ";";
	    registro += ((RegistroLogBOL) registroLog).getFilename() + ";";
	    registro += ((RegistroLogBOL) registroLog).getFileFlag() + ";";
	    registro += ((RegistroLogBOL) registroLog).getFileTsDownload()
		    + ";";
	    registro += ((RegistroLogBOL) registroLog).getFileTsUpload() + ";";
	    dataList.add(registro.split(";"));

	}

	return dataList;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param listaRegistrosLogs
     * @param dataList
     * @since 1.X
     */
    private List<String[]> parserExcepIncoming(
	    List<RegistroLog> listaRegistrosLogs) {

	List<String[]> dataList = new ArrayList<String[]>();
	for (RegistroLog registroLog : listaRegistrosLogs) {
	    String registro = "";
	    registro += ((RegistroLogExeptIncoming) registroLog).getSid() + ";";
	    registro += ((RegistroLogExeptIncoming) registroLog).getLogCarga()
		    + ";";
	    registro += ((RegistroLogExeptIncoming) registroLog)
		    .getFechaIncoming() + ";";
	    registro += ((RegistroLogExeptIncoming) registroLog).getMit() + ";";
	    registro += ((RegistroLogExeptIncoming) registroLog)
		    .getCodigoFuncion() + ";";
	    registro += ((RegistroLogExeptIncoming) registroLog)
		    .getNumeroTarjeta() + ";";
	    registro += ((RegistroLogExeptIncoming) registroLog)
		    .getCodigoAutorizacion() + ";";
	    dataList.add(registro.split(";"));

	}

	return dataList;
    }

}
