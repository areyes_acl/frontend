package cl.util.excel.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import Beans.DetalleContableBean;
import au.com.bytecode.opencsv.CSVWriter;
import cl.util.Utils;
import cl.util.excel.service.ExcelUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ExcelDetalleContable implements ExcelUtils<DetalleContableBean> {
    static final Logger log = Logger.getLogger(ExcelDetalleContable.class);
    private String ruta;
    private final String EXTENSION = ".TXT";

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public ExcelDetalleContable() {
	Properties propiedades = new Properties();
	propiedades = PropertiesConfiguration.getProperty();
	String directorioExportacion = propiedades.getProperty("exportarexcel");
	String filename = propiedades.getProperty("nombreArchivoExport");

	this.ruta = directorioExportacion + filename + EXTENSION;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param lista
     * @return
     * @see cl.util.excel.service.ExcelUtils#export(java.util.List)
     * @since 1.X
     */
    @Override
    public File export(List<DetalleContableBean> lista) {

	List<String[]> dataList = null;
	CSVWriter writer = null;
	File file = null;

	try {

	    file = new File(ruta);

	    // GENERA CABECERA EXCEL
	    String[] header = generateHeader();

	    // PARSEO DE DATOS DEL EXCEL
	    dataList = parseListToString(lista);

	    writer = new CSVWriter(new FileWriter(ruta), ';');
	    writer.writeNext(header);
	    writer.writeAll(dataList);

	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	} finally {
	    if (writer != null) {
		try {
		    writer.close();
		} catch (IOException e) {
		    log.error(e.getMessage(), e);
		}
	    }
	}

	return file;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private String[] generateHeader() {
	return new String[] { "FECHA CONTABLE", "TIPO MOVIMIENTO",
		"CUENTA DEBITO", "CUENTA CREDITO", "MONTO", "SUBLEDGET",
		"NUMERO TARJETA", "MICROFILM", "TIPO VENTA",
		"CANTIDAD DE CUOTAS", "NOMBRE ARCHIVO CONTABLE" };
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param listaDetalle
     * @return
     * @since 1.X
     */
    private List<String[]> parseListToString(
	    List<DetalleContableBean> listaDetalle) {
	List<String[]> dataList = new ArrayList<String[]>();

	for (DetalleContableBean detalle : listaDetalle) {
	    String registro = "";
	    registro += detalle.getFechaContable() + ";";
	    registro += detalle.getTipoMovimiento() + ";";
	    registro += detalle.getCuentaDebito() + ";";
	    registro += detalle.getCuentaCredito() + ";";
	    registro += Utils.formateaMontoPorTipoMovimiento(detalle.getTipoMovimiento(),detalle.getMonto()) + ";";
	    registro += detalle.getSubledger() + ";";
	    registro += (detalle.getNumeroTarjeta()==null) ? "" : detalle.getNumeroTarjeta() + ";";
	    registro += (detalle.getMicrofilm()==null) ? "" : detalle.getMicrofilm() + ";";
	    registro += (detalle.getTipoVenta()==null) ? "" : detalle.getTipoVenta() +";";
	    registro += (detalle.getCantidadCuotas() == null) ? "" : detalle.getCantidadCuotas() + ";";
	    registro += detalle.getNombreArchivo() + ";";

	    dataList.add(registro.split(";"));

	}
	return dataList;
    }

}
