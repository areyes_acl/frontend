package cl.util;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import org.apache.log4j.Logger;

import Beans.PermisosUsuario;
import Beans.SubMenu;
import Beans.Usuario;

import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class Utils {
    public static final Logger log = Logger.getLogger(Utils.class);

    private static final String PATTERN_AMOUNT = "###,##0.00";
    private static final String REGULAR_EXPRESION_ONLY_NUMBER = "-?\\d+(\\.\\d+)?";
    private static final Character DECIMAL_SEPARATOR = ',';
    private static final Character THOUSAND_SEPARATOR = '.';
    private static final String EMPTY = "";
    private static final String COMMA = ",";
    private static final String POINT = ".";
    private static final String DEFAULT_VALUE = "0,00";
    public static final String SUMATORIA_INCOMING_MAP_NAME = "SUM_INCOMING";
    public static final String SUMATORIA_DESCUENTO_MAP_NAME = "SUM_DESCUENTO";
    public static final String SUMATORIA_CPAGO_MAP_NAME = "SUM_CPAGO";
    public static final String GRUPO_PRTS_PAGO = "WEB_SIG";
    public static final String SUMA_CARGO_MAP_NAME = "SUMA_CARGOS";
    public static final String SUMA_ABONO_MAP_NAME = "SUMA_ABONOS";
    public static final String COD_DESCUENTO = "100007";
    public static final String COD_PAGO = "100006";
    public static final String COD_DESCUENTO_VN = "200007";
    public static final String COD_PAGO_VN = "200006";
    public static final String COD_DESCUENTO_VI = "300007";
    public static final String COD_PAGO_VI = "300006";

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * METODO QUE CALCULA LA CANTIDAD DE PAGINAS TOTALES DADO EL TOTAL DE
     * REGISTROS EXISTENTES Y LA CANTIDAD DE REGISTROS QUE SE DEBESA MOSTRAR
     * 
     * 
     * @param totReg
     *            , Cantidad de registros existentes.
     * @param cantidadRegistros
     *            , Cantidad de registros que se desean mostrar.
     * @return nro de paginas
     * @since 1.X
     */
    public static int calcularUltimaPagina(float totReg, float cantidadRegistros) {
	int ultimaPagina = 0;
	if (totReg != 0) {
	    ultimaPagina = (int) Math.ceil(totReg / cantidadRegistros);
	}

	return ultimaPagina;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param obj
     * @return
     * @since 1.X
     */
    public static boolean isNumber(final String obj) {

	try {
	    if (obj == null) {
		return true;
	    } else {
		Long.valueOf(obj);
		return true;
	    }
	} catch (Exception ex) {

	    return false;
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * Le da formato a un rut, concatenandole puntos y guion.
     * 
     * @param rut
     *            Rut a formatear.
     * @return Un nuevo String, con el rut formateado.
     * 
     */
    public static String formatearRut(String rut) {
	int cont = 0;
	String format;
	rut = rut.replace(".", "");
	rut = rut.replace("-", "");
	format = "-" + rut.substring(rut.length() - 1);
	for (int i = rut.length() - 2; i >= 0; i--) {
	    format = rut.substring(i, i + 1) + format;
	    cont++;
	    if (cont == 3 && i != 0) {
		format = "." + format;
		cont = 0;
	    }
	}
	return format;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * metodo que formatea un numero
     * 
     * 10,000,000,00 ===> 10.000.000,00
     * 
     * 
     * si se produce algun error retorna un valor por defecto declarado
     * 
     * si es 1 => 1,00
     * 
     * 
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
    public static String formateaMonto(String monto) {

	if (monto != null && monto.length() >= 0 && monto.length() <= 1) {
	    monto = monto + "00";
	}
	
	log.info(monto);
	
	DecimalFormatSymbols simb = new DecimalFormatSymbols();
	simb.setDecimalSeparator(DECIMAL_SEPARATOR);
	simb.setGroupingSeparator(THOUSAND_SEPARATOR);
	DecimalFormat formatea = new DecimalFormat(PATTERN_AMOUNT, simb);

	try {
	    return formatea.format(limpiarMonto(monto));
	} catch (Exception e) {
	    log.error(e.getMessage() + "Se retornara 0,00 ", e);
	    return DEFAULT_VALUE;
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param tipoMovimiento
     * @param monto
     * @return
     * @since 1.X
     */
    public static String formateaMontoPorTipoMovimiento(String tipoMovimiento,
	    String monto) {
	if (COD_DESCUENTO.equalsIgnoreCase(tipoMovimiento)
	   || COD_PAGO.equalsIgnoreCase(tipoMovimiento)
	   || COD_PAGO_VN.equalsIgnoreCase(tipoMovimiento)
	   || COD_DESCUENTO_VN.equalsIgnoreCase(tipoMovimiento)
	   || COD_PAGO_VI.equalsIgnoreCase(tipoMovimiento)
	   || COD_DESCUENTO_VI.equalsIgnoreCase(tipoMovimiento)) {
	    return monto;
	}

	return String.valueOf(Integer.valueOf(monto) / 100);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que dado un numero como cadena y algun separador de miles o
     * decimal lo transforma a un double separando la parte decimal (de largo 2)
     * por puntos
     * 
     * @param monto
     * @return
     * @throws Exception
     * @since 1.X
     */
    public static Double limpiarMonto(String monto) throws Exception {

	if (monto == null) {
	    throw new Exception(
		    "Numero a convertir no contiene un formato valido ==> valor : "
			    + monto);
	}

	if (EMPTY.equalsIgnoreCase(monto)) {
	    throw new Exception(
		    "Numero a convertir no contiene un formato valido ==> valor : "
			    + monto);
	}

	monto = monto.replace(COMMA, EMPTY).replace(POINT, EMPTY);

	if (!isNumeric(monto)) {
	    throw new Exception(
		    "Numero a convertir no es un numero valido ==> valor : "
			    + monto);
	}

	String parteEntera = monto.substring(0, monto.length() - 2);
	String parteDecimal = monto.substring(monto.length() - 2,
		monto.length());

	monto = parteEntera.concat(POINT).concat(parteDecimal);

	return Double.parseDouble(monto);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que valida si un string es un numero (con signo negativo anterior
     * y con decimal al final)
     * 
     * 
     * @param str
     * @return
     * @since 1.X
     */
    public static boolean isNumeric(String str) {
	// match a number with optional '-' and decimal.
	return str.matches(REGULAR_EXPRESION_ONLY_NUMBER);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que obtiene la fecha en un formato aaaammdd
     * 
     * 
     * @param str
     * @return
     * @since 1.X
     */
    public static String getDateToday() {
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
	return sdf.format(date);
    }

    public static Date addDays(Date date, int days) {
	Calendar cal = Calendar.getInstance();
	cal.setTime(date);
	cal.add(Calendar.DATE, days); // minus number would decrement the days
	return cal.getTime();
    }
    
	public static Date addMinutes(Date date, int amount){
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.add(Calendar.MINUTE, amount);

	    return calendar.getTime();
	}

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que mueve un archivo a a una ruta especifica y le pone un nuevo
     * nombre
     * 
     * 
     * @param str
     * @return
     * @since 1.X
     */
    public static Boolean moveFileTo(String newFilename, String path,
	    File oldFile) {

	// RENOMBRA ARCHIVOS
	File newFile = new File(path.concat(newFilename));

	boolean success = oldFile.renameTo(newFile);

	return success;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que obtiene la extension de un archivo, dado el nombre del archivo
     * por ej: EJEMPLO.txt => txt
     * 
     * 
     * 
     * @param str
     * @return
     * @since 1.X
     */
    public static String obtenerExtensionArchivo(String nombreArchivo) {

	if (nombreArchivo != null) {
	    String[] str = nombreArchivo.split("\\.");
	    return str[str.length - 1];
	}
	return null;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que transforma un String dado en fecha Julian a un String de la
     * fecha con el formato definido en al entrada
     * 
     * @param julianDate
     * @return
     * @since 1.X
     */
    public static String parseJulianDateToGregorianDate(String date) {

	Double julianDate = Double.parseDouble(date);
	String miF = "";
	miF = "" + julianDate;

	String ano = miF.substring(0, 1);
	int anoTx = Integer.parseInt(ano);

	Calendar c2 = new GregorianCalendar();

	int anoActual = c2.get(Calendar.YEAR);

	String anoActualS = anoActual + "";

	String anoActualF = anoActualS.substring(3, 4);

	int anoActualInt = Integer.parseInt(anoActualF);

	String anoActualFinalS = "";
	if (anoActualInt < anoTx) {
	    anoActual = anoActual - 10;
	    anoActualFinalS = (anoActual + "").substring(0, 3) + anoTx;
	} else {
	    anoActualFinalS = (anoActual + "").substring(0, 3) + anoTx;
	}

	int diasJulianos = Integer.parseInt(miF.substring(1, 4));

	Calendar fechaFinal = new GregorianCalendar(
		Integer.parseInt(anoActualFinalS), 0, 0);

	fechaFinal.add(Calendar.DATE, diasJulianos);

	String diaFinal = "";
	if (fechaFinal.get(Calendar.DAY_OF_MONTH) < 10)
	    diaFinal = "0" + fechaFinal.get(Calendar.DAY_OF_MONTH);
	else {
	    diaFinal = "" + fechaFinal.get(Calendar.DAY_OF_MONTH);
	}
	String mesFinal = "";
	int mesTX = fechaFinal.get(Calendar.MONTH) + 1;
	if (mesTX < 10)
	    mesFinal = "0" + mesTX;
	else
	    mesFinal = "" + mesTX;

	String[] gregoria = new String[] { fechaFinal.get(Calendar.YEAR) + "",
		mesFinal, diaFinal };

	String fechaProcesoFinal = gregoria[2] + "/" + gregoria[1] + "/"
		+ gregoria[0];

	return fechaProcesoFinal;
    }

    public static SubMenu getSubMenuFromUsuario(Integer idSubMenu) {
	Map session = ActionContext.getContext().getSession();
	Usuario usuarioLog = (Usuario) session.get("usuarioLog");

	return null;
    }

    public static Boolean checkUserSubMenuPermiso(SubMenu submenu,
	    Integer idPermiso) {
	for (PermisosUsuario permiso : submenu.getListaPermisos()) {
	    if (permiso.getPermisoId() == idPermiso) {
		return Boolean.TRUE;
	    }
	}
	return Boolean.FALSE;
    }
}
