package cl.util;

import java.io.UnsupportedEncodingException;

import javax.xml.bind.DatatypeConverter;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class CryptUtils {

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private CryptUtils() {

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param word
     * @return
     * @throws UnsupportedEncodingException
     * @since 1.X
     */
    public static String encodeBase64(String word){
	byte[] message;
	try {
	    message = word.getBytes("UTF-8");
	    return DatatypeConverter.printBase64Binary(message);
	} catch (UnsupportedEncodingException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    return "";
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param word
     * @return
     * @throws UnsupportedEncodingException
     * @since 1.X
     */
    public static String decodeBase64(String wordEncoded){
	byte[] decoded = DatatypeConverter.parseBase64Binary(wordEncoded);

	try {
	    return new String(decoded, "UTF-8");
	} catch (UnsupportedEncodingException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    return "";
	}

    }

}