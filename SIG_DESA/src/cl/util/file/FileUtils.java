package cl.util.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import types.FileType;
import cl.filter.FiltroArchivo;
import clases.ArchivosDAO;
import exception.AppException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class FileUtils {
    static final Category log = Logger.getLogger(FileUtils.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que dado un path de una carpeta entrega la lista de archivos que
     * contiene
     * 
     * @param folderPath
     * @return
     * @throws AppException
     * @since 1.X
     */
    private static File[] getFilesInFolder(String folderPath) {
	File[] listOfFiles;
	File folder;

	// OBTIENE EL FOLDER
	folder = new File(folderPath);

	listOfFiles = folder.listFiles();
	return listOfFiles;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Metodo que dado un prefijo una extension y un archivo busca si existen
     * archivos con aquellos filtros
     * 
     * @param folderPath
     * @param starWith
     * @param endsWith
     * @return
     * @throws AppException
     * @since 1.X
     */
    public static List<File> getFilesInFolder(String folderPath,
	    String starWith, String endsWith) {

	File[] filesInFolder = getFilesInFolder(folderPath);
	List<File> listOfOutgoingsFiles;
	if (filesInFolder == null) {
	    return new ArrayList<File>();
	} else {
	    listOfOutgoingsFiles = new ArrayList<File>();
	    for (File file : filesInFolder) {
		if (!file.isDirectory() && file.isFile()
			&& file.getName().startsWith(starWith)
			&& file.getName().endsWith(endsWith)) {
		    listOfOutgoingsFiles.add(file);
		}
	    }
	}
	return listOfOutgoingsFiles;
    }
    
    
   
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     * 
     * @param folderPath
     * @return
     * @since 1.X
     */
    public static List<File> getFilesByFilter(String folderPath,FiltroArchivo filtro) {

	File[] filesInFolder = getFilesInFolder(folderPath);
	List<File> listOfFiles; 
	
	if (filesInFolder == null) {
	    return new ArrayList<File>();
	} else {
	    listOfFiles = new ArrayList<File>();
	    
	    // Filtro por fecha
	    for (File file : filesInFolder) {
		if (isFileFounded(file,filtro)) {
		    listOfFiles.add(file);
		}
	    }
	}
	return listOfFiles;
    }
    

    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     * 
     * @param file
     * @param filtro
     * @return
     * @since 1.X
     */
    private static boolean isFileFounded(File file, FiltroArchivo filtro) {
	
	log.info(file);
	log.info(filtro);
	String fecha;
	String filename = null;
	
	// mmdd
	if(filtro.getTipoArchivo().equals(FileType.OUTGOING)|| filtro.getTipoArchivo().equals(FileType.BOL)
		|| filtro.getTipoArchivo().equals(FileType.LOG_AUTORIZACION)|| filtro.getTipoArchivo().equals(FileType.ONUS)||
		 filtro.getTipoArchivo().equals(FileType.RECHAZOS)||filtro.getTipoArchivo().equals(FileType.INCOMING)){
	     fecha = filtro.getFecha().substring(3,5).concat(filtro.getFecha().substring(0,2));		
	     filename = filtro.getStarwith() + fecha;
	        	
	}
	
	if(filtro.getTipoArchivo().equals(FileType.OUTGOING_VI) || filtro.getTipoArchivo().equals(FileType.OUTGOING_VN) 
		|| filtro.getTipoArchivo().equals(FileType.INCOMING_VN) || filtro.getTipoArchivo().equals(FileType.INCOMING_VI)){
	     
	     fecha = filtro.getFecha().substring(6,10).concat(filtro.getFecha().substring(3,5).concat(filtro.getFecha().substring(0,2)));		
	     log.info(fecha);
	     filename = filtro.getStarwith() + fecha;
	     log.info(filename);
	}
	
	//03/12/2017 => ddmm
	if(filtro.getTipoArchivo().equals(FileType.COMISIONES)){
	     fecha = filtro.getFecha().substring(0,2).concat(filtro.getFecha().substring(3,5));		
	     filename = filtro.getStarwith() + fecha;
    	
	}
	
	// 03/12/2017=> NCL5ABCD.211117
	if(filtro.getTipoArchivo().equals(FileType.NCL)){
	    fecha =filtro.getFecha().substring(0,2).concat(filtro.getFecha().substring(3,5).concat(filtro.getFecha().substring(8,10)));	
	    filename = filtro.getStarwith() + fecha;
	}
	
	
	//   ID_20160317
	if(filtro.getTipoArchivo().equals(FileType.CONTABILIDAD) || filtro.getTipoArchivo().equals(FileType.CARGO_ABONO)){
	    fecha =filtro.getFecha().substring(6,10).concat(filtro.getFecha().substring(3,5)).concat(filtro.getFecha().substring(0,2));	
	    filename = filtro.getStarwith() + fecha;
	}
	
	
	if(file.getName().startsWith(filename)){
	    return true;
	}
	
	return false;
    }
    
    
 

}
