package cl.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * Clase utilitataria para el manejo de fechas
 * 
 * @author afreire
 * 
 * 
 *         <p>
 *         <B>Todos los derechos reservados por ACL SPA .</B>
 */
public final class DateTools {

    public static final Logger log = Logger.getLogger(Utils.class);
    /**
     * 
     */
    public static final String DATE_FORMAT_JAVA = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_FORMAT_LABEL = "dd/MM/yyyy";
    public static final String DATE_FORMAT_DB = "yyyy-mm-dd hh24:mi:ss";
    public static final String DATE_FORMAT_WS = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_CONTABLE_EXCEL = "yyyyMMdd";
    private static final long MILLIS_IN_ONE_DAY = 86400000;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private DateTools() {

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param date
     * @param format
     * @return
     * @since 1.X
     */
    public static String formatDateTime(Date date, String format) {
	SimpleDateFormat formatDate = new SimpleDateFormat(format,
		Locale.ENGLISH);
	return formatDate.format(date);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param date
     * @return
     * @since 1.X
     */
    public static String formatDateTime(Date date) {
	return formatDateTime(date, DATE_FORMAT_JAVA);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param date
     * @param formatIn
     * @param formatOut
     * @return
     * @since 1.X
     */
    public static String formatStringDate(String date, String formatIn,
	    String formatOut) {
	SimpleDateFormat formatDate = new SimpleDateFormat(formatIn);
	try {
	    Date fecha = formatDate.parse(date);
	    formatDate = new SimpleDateFormat(formatOut);
	    return formatDate.format(fecha);
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	return "";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param dateTimeString
     * @param format
     * @return
     * @since 1.X
     */
    public static Date transformDateTime(String dateTimeString, String formatIn) {
	SimpleDateFormat formatDate = new SimpleDateFormat(formatIn,
		Locale.ENGLISH);
	try {
	    return formatDate.parse(dateTimeString);
	} catch (ParseException e) {
	    log.error(e.getMessage(), e);
	    return null;
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param dateTimeString
     * @return
     * @since 1.X
     */
    public static Date transformDateTime(String dateTimeString) {
	return transformDateTime(dateTimeString, DATE_FORMAT_JAVA);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_year
     * @param p_month
     * @param p_day
     * @return
     * @since 1.X
     */
    public static Date transformDateTime(int p_year, int p_month, int p_day) {
	return transformDateTime(p_year, p_month, p_day, 0, 0, 0);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_year
     * @param p_month
     * @param p_day
     * @param p_hour
     * @param p_min
     * @param p_sec
     * @return
     * @since 1.X
     */
    public static Date transformDateTime(int p_year, int p_month, int p_day,
	    int p_hour, int p_min, int p_sec) {

	Calendar p_cal = Calendar.getInstance();

	p_cal.set(Calendar.YEAR, p_year);
	p_cal.set(Calendar.MONTH, (p_month - 1) + Calendar.JANUARY);
	p_cal.set(Calendar.DAY_OF_MONTH, p_day);
	p_cal.set(Calendar.HOUR_OF_DAY, p_hour);
	p_cal.set(Calendar.MINUTE, p_min);
	p_cal.set(Calendar.SECOND, p_sec);
	p_cal.set(Calendar.MILLISECOND, 0);
	// p_cal.set(Calendar.AM_PM, getAmPm());

	return p_cal.getTime();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_date
     * @param p_days
     * @return
     * @since 1.X
     */
    public static Date addDays(Date p_date, double p_days) {
	int completeDays = (int) p_days;
	double datePart = (p_days - completeDays) * MILLIS_IN_ONE_DAY;

	Calendar c = Calendar.getInstance();
	c.setTime(p_date);
	c.add(Calendar.DATE, (int) p_days);
	c.add(Calendar.MILLISECOND, (int) datePart);

	return c.getTime();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_date
     * @return
     * @since 1.X
     */
    public static int getYear(Date p_date) {
	Calendar p_cal = Calendar.getInstance();
	p_cal.setTime(p_date);
	int year = p_cal.get(Calendar.YEAR);
	return year;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_date
     * @return
     * @since 1.X
     */
    public static int getMonth(Date p_date) {
	Calendar p_cal = Calendar.getInstance();
	p_cal.setTime(p_date);
	int month = p_cal.get(Calendar.MONTH) - Calendar.JANUARY + 1;
	return month;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_date
     * @return
     * @since 1.X
     */
    public static int getDay(Date p_date) {
	Calendar p_cal = Calendar.getInstance();
	p_cal.setTime(p_date);
	int day = p_cal.get(Calendar.DAY_OF_MONTH);
	return day;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_date
     * @return
     * @since 1.X
     */
    public static int getHour(Date p_date) {
	Calendar p_cal = Calendar.getInstance();
	p_cal.setTime(p_date);
	int hour = p_cal.get(Calendar.HOUR_OF_DAY);
	return hour;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_date
     * @return
     * @since 1.X
     */
    public static int getMinute(Date p_date) {
	Calendar p_cal = Calendar.getInstance();
	p_cal.setTime(p_date);
	int min = p_cal.get(Calendar.MINUTE);
	return min;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_date
     * @return
     * @since 1.X
     */
    public static int getSecond(Date p_date) {
	Calendar p_cal = Calendar.getInstance();
	p_cal.setTime(p_date);
	int sec = p_cal.get(Calendar.SECOND);
	return sec;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param from
     * @param to
     * @return
     * @since 1.X
     */
    public static boolean isValidDateRange(Date from, Date to) {
	long fromDateMillis = from.getTime();
	long toDateMillis = to.getTime();

	if (fromDateMillis <= toDateMillis) {
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param from
     * @param to
     * @param millisAllowedRange
     * @return
     * @since 1.X
     */
    public static boolean isDateRangeWithinAllowedRange(Date from, Date to,
	    Long millisAllowedRange) {
	long fromDateMillis = from.getTime();
	long toDateMillis = to.getTime();

	if ((toDateMillis - fromDateMillis) <= millisAllowedRange) {
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param from
     * @param to
     * @param days
     * @return
     * @since 1.X
     */
    public static boolean isDateRangeWithinAllowedDaysRange(Date from, Date to,
	    int days) {
	Long millisAllowedRange = days * MILLIS_IN_ONE_DAY;
	return isDateRangeWithinAllowedRange(from, to, millisAllowedRange);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param maxAllowedRange
     * @return
     * @since 1.X
     */
    public static Integer getMillisAsDays(Long maxAllowedRange) {
	return (int) ((((maxAllowedRange / 1000) / 60) / 60) / 24);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param d
     * @return
     * @since 1.X
     */
    public static int getDayOfTheWeek(Date d) {
	GregorianCalendar cal = new GregorianCalendar();
	cal.setTime(d);

	return cal.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param dateTime
     * @return
     * @since 1.X
     */
    public static Date transformDateTimeNoHour(Date dateTime) {
	SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String date = sdf.format(dateTime);

	try {
	    Date fecha = formatoFecha.parse(date);
	    return fecha;
	} catch (ParseException e) {
	    log.error(e.getMessage(), e);
	    return null;
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param p_date
     * @param p_month
     * @return
     * @since 1.X
     */
    public static Date addMonth(Date p_date, int p_month) {
	Calendar c = Calendar.getInstance();
	c.setTime(p_date);
	c.add(Calendar.MONTH, p_month);
	return c.getTime();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * * Get a diff between two dates *
     * 
     * @param date1
     *            the oldest date *
     * @param date2
     *            the newest date *
     * @param timeUnit
     *            the unit in which you want the diff *
     * @return the diff value, in the provided unit
     * 
     * 
     * @param date1
     * @param date2
     * @param timeUnit
     * @return
     * @since 1.X
     */
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	long diffInMillies = date2.getTime() - date1.getTime();
	return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versi�n inicial 
     * </ul>
     * <p>
     * 
     * @param args
     * @since 1.X
     */
    public static void main(String[] args) {
	String fecha2 = "05/12/2016";
	
	Date fec1 = new Date();
	Date fec2 = transformDateTime(fecha2, DATE_FORMAT_LABEL);
	System.out.println("fec1 :" + fec1);
	System.out.println("fec2 :" + fec2);
	System.out.println(getDateDiff(fec2,fec1 , TimeUnit.DAYS));
    }
    
}
