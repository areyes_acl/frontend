package cl.util;

public class Constantes {

    public static final String KEY_DATA_EXCEL = "dataExcel";
    public static final String KEY_FILENAME = "filename";
    public static final String TRX_NO_AUTORIZADAS_POR_IC = "TRX_NO_AUTORIZADAS_POR_IC";
    public static final String TRX_NO_PAREADAS_TRANSBANK = "TRX_NO_PAREADAS_TRANSBANK";
    
    //AVANCES
    public static final String KEY_DATA_EXCEL_AVA = "dataExcel";
    public static final String KEY_FILENAME_AVA = "filename";
    public static final String TRX_AVA = "TRX_AVA";
    
    
    //CONTABLE AVANCES
    public static final String KEY_DATA_EXCEL_CTBLE_AVA = "dataExcel";
    public static final String KEY_FILENAME_CTBLE_AVA = "filename";
    public static final String RTRO_CTBLE_AVA= "REGISTROS_CTBLE_AVA";

}
