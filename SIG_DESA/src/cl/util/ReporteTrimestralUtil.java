package cl.util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import types.IType;
import types.InternacionalType;
import types.NacionalType;
import types.OnusType;
import Beans.DetalleTrimestral;
import Beans.ReporteTrimestral;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * CLASE UTILITARIA PARA EL REPORTE TRIMESTRAL
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ReporteTrimestralUtil {
    public static final Logger log = Logger
	    .getLogger(ReporteTrimestralUtil.class);

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Obtener el ENUM correpondiente a la transaccion de acuerdo a la posicion
     * y cualquier instancia de un enum (ONUS, NACIONAL, INTERNACIONAL)
     * 
     * @param posicion
     * @param type
     * @return
     * @since 1.X
     */
    public IType obtenerTipoDeTransaccionTrimestral(int posicion, IType type) {

	if (type instanceof OnusType) {
	    return OnusType.values()[posicion];

	} else if (type instanceof NacionalType) {
	    return NacionalType.values()[posicion];

	} else {
	    return InternacionalType.values()[posicion];
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param trimestre
     * @return
     * @since 1.X
     */
    public List<String> obtenerMesesEnNroDeTrimestre(Integer trimestre) {
	List<String> meses = new ArrayList<String>();

	switch (trimestre) {
	case 1:
	    meses.add("01");
	    meses.add("02");
	    meses.add("03");
	    break;
	case 2:
	    meses.add("04");
	    meses.add("05");
	    meses.add("06");
	    break;
	case 3:
	    meses.add("07");
	    meses.add("08");
	    meses.add("09");
	    break;
	case 4:
	    meses.add("10");
	    meses.add("11");
	    meses.add("12");
	    break;
	}

	return meses;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo que recupera el/los mes/es faltante y retorna una lista por
     * defecto
     * 
     * @param listaTrimestral
     * @param trimestre
     * @param anio
     * @return
     * @since 1.X
     */
    public List<ReporteTrimestral> obtenerTrimestreFaltante(
	    List<ReporteTrimestral> listaTrimestral, Integer trimestre,
	    Integer anio) {
	List<String> mesesEnBD = new ArrayList<String>();
	List<ReporteTrimestral> listaReporteFaltantes = new ArrayList<ReporteTrimestral>();

	try {

	    // Crea una lista con los meses que vienen desde BD (xeje:
	    // 01,02,03)
	    for (ReporteTrimestral reporteTrimestral : listaTrimestral) {
		mesesEnBD
			.add(reporteTrimestral.getFechaTrimestre().split("/")[0]);
	    }

	    // Dado un trimestre obtiene una lista de meses asociado a
	    // este.
	    List<String> mesesEnTrimestre = obtenerMesesEnNroDeTrimestre(trimestre);

	    // Se obtienen los meses que no estan en lo informado por
	    // BD
	    mesesEnTrimestre.removeAll(mesesEnBD);

	    // Obtiene los reportes faltantes por mes
	    for (String mes : mesesEnTrimestre) {
		listaReporteFaltantes.add(obtieneReporteDefault(mes, anio));
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error(e);
	}
	log.info("reporte faltante : " + listaReporteFaltantes);

	return listaReporteFaltantes;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * Metodo encargado de crear un reporte trimestral con todas las listas en
     * 0. dado el mes y el a�o
     * 
     * @param mes
     * @param anio
     * @since 1.X
     */
    private ReporteTrimestral obtieneReporteDefault(String mes, Integer anio) {
	ReporteTrimestral reporteFaltante = new ReporteTrimestral();
	String fecha = String.valueOf(mes).concat("/")
		.concat(String.valueOf(anio));

	// Setea la fecha del reporte
	reporteFaltante.setFechaTrimestre(fecha);

	// Obtiene lista defecto ONUS
	reporteFaltante.setDetalleOnus(obtenerListaOnusDefault());

	// Obtiene lista defecto Nacional
	reporteFaltante.setDetalleNacional(obtenerListaNacionalDefault());

	// Obtiene lista defecto Internacional
	reporteFaltante.setDetalleInternacional(obtenerListaInternacional());

	return reporteFaltante;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Obtiene la lista onus por defecto
     * 
     * @param rs
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private List<DetalleTrimestral> obtenerListaOnusDefault() {
	List<DetalleTrimestral> listaTrimestral = new ArrayList<DetalleTrimestral>();

	DetalleTrimestral detalleOnus = new DetalleTrimestral();
	detalleOnus.setTipo(OnusType.ONUS_TOTAL);
	detalleOnus
		.setDescripcionCant(OnusType.ONUS_TOTAL.getDescripcionCant());
	detalleOnus
		.setDescripcionMont(OnusType.ONUS_TOTAL.getDescripcionMont());
	detalleOnus.setCantidad(0);
	detalleOnus.setMonto("0");
	listaTrimestral.add(detalleOnus);

	detalleOnus = new DetalleTrimestral();
	detalleOnus.setTipo(OnusType.ONUS_CHIP);
	detalleOnus.setDescripcionCant(OnusType.ONUS_CHIP.getDescripcionCant());
	detalleOnus.setDescripcionMont(OnusType.ONUS_CHIP.getDescripcionMont());
	detalleOnus.setCantidad(0);
	detalleOnus.setMonto("0");
	listaTrimestral.add(detalleOnus);

	detalleOnus = new DetalleTrimestral();
	detalleOnus.setTipo(OnusType.ONUS_COMERCIO_ELECTRONICO);
	detalleOnus.setDescripcionCant(OnusType.ONUS_COMERCIO_ELECTRONICO
		.getDescripcionCant());
	detalleOnus.setDescripcionMont(OnusType.ONUS_COMERCIO_ELECTRONICO
		.getDescripcionMont());
	detalleOnus.setCantidad(0);
	detalleOnus.setMonto("0");
	listaTrimestral.add(detalleOnus);

	detalleOnus = new DetalleTrimestral();
	detalleOnus.setTipo(OnusType.ONUS_SERVICIOS_BANCARIOS);
	detalleOnus.setDescripcionCant(OnusType.ONUS_SERVICIOS_BANCARIOS
		.getDescripcionCant());
	detalleOnus.setDescripcionMont(OnusType.ONUS_SERVICIOS_BANCARIOS
		.getDescripcionMont());
	detalleOnus.setCantidad(0);
	detalleOnus.setMonto("0");
	listaTrimestral.add(detalleOnus);

	detalleOnus = new DetalleTrimestral();
	detalleOnus.setTipo(OnusType.ONUS_TIENDAS_CLIENTE);
	detalleOnus.setDescripcionCant(OnusType.ONUS_TIENDAS_CLIENTE
		.getDescripcionCant());
	detalleOnus.setDescripcionMont(OnusType.ONUS_TIENDAS_CLIENTE
		.getDescripcionMont());
	detalleOnus.setCantidad(0);
	detalleOnus.setMonto("0");
	listaTrimestral.add(detalleOnus);

	detalleOnus = new DetalleTrimestral();
	detalleOnus.setTipo(OnusType.ONUS_DISPOSICION_EFECTIVO_EN_CAJA);
	detalleOnus
		.setDescripcionCant(OnusType.ONUS_DISPOSICION_EFECTIVO_EN_CAJA
			.getDescripcionCant());
	detalleOnus
		.setDescripcionMont(OnusType.ONUS_DISPOSICION_EFECTIVO_EN_CAJA
			.getDescripcionMont());
	detalleOnus.setCantidad(0);
	detalleOnus.setMonto("0");
	listaTrimestral.add(detalleOnus);

	detalleOnus = new DetalleTrimestral();
	detalleOnus.setTipo(OnusType.ONUS_DISPOSICION_EFECTIVO_CAJERO);
	detalleOnus
		.setDescripcionCant(OnusType.ONUS_DISPOSICION_EFECTIVO_CAJERO
			.getDescripcionCant());
	detalleOnus
		.setDescripcionMont(OnusType.ONUS_DISPOSICION_EFECTIVO_CAJERO
			.getDescripcionMont());
	detalleOnus.setCantidad(0);
	detalleOnus.setMonto("0");
	listaTrimestral.add(detalleOnus);

	return listaTrimestral;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Obtiene la lista nacional por defecto, es decir con todos sus valores en
     * 0
     * 
     * @param rs
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private List<DetalleTrimestral> obtenerListaNacionalDefault() {
	List<DetalleTrimestral> listaTrimestral = new ArrayList<DetalleTrimestral>();

	DetalleTrimestral detalleNac = new DetalleTrimestral();
	detalleNac.setTipo(NacionalType.NACIONAL_TOTAL);
	detalleNac.setDescripcionCant(NacionalType.NACIONAL_TOTAL
		.getDescripcionCant());
	detalleNac.setDescripcionMont(NacionalType.NACIONAL_TOTAL
		.getDescripcionMonto());
	detalleNac.setCantidad(0);
	detalleNac.setMonto("0");
	listaTrimestral.add(detalleNac);

	detalleNac = new DetalleTrimestral();
	detalleNac.setTipo(NacionalType.NACIONAL_CHIP);
	detalleNac.setDescripcionCant(NacionalType.NACIONAL_TOTAL
		.getDescripcionCant());
	detalleNac.setDescripcionMont(NacionalType.NACIONAL_TOTAL
		.getDescripcionMonto());
	detalleNac.setCantidad(0);
	detalleNac.setMonto("0");
	listaTrimestral.add(detalleNac);

	detalleNac = new DetalleTrimestral();
	detalleNac.setTipo(NacionalType.NACIONAL_COMERCIO_ELECTRONICO);
	detalleNac
		.setDescripcionCant(NacionalType.NACIONAL_COMERCIO_ELECTRONICO
			.getDescripcionCant());
	detalleNac
		.setDescripcionMont(NacionalType.NACIONAL_COMERCIO_ELECTRONICO
			.getDescripcionMonto());
	detalleNac.setCantidad(0);
	detalleNac.setMonto("0");
	listaTrimestral.add(detalleNac);

	detalleNac = new DetalleTrimestral();
	detalleNac.setTipo(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_EN_CAJA);
	detalleNac
		.setDescripcionCant(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_EN_CAJA
			.getDescripcionCant());
	detalleNac
		.setDescripcionMont(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_EN_CAJA
			.getDescripcionMonto());
	detalleNac.setCantidad(0);
	detalleNac.setMonto("0");
	listaTrimestral.add(detalleNac);

	detalleNac = new DetalleTrimestral();
	detalleNac.setTipo(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_CAJERO);
	detalleNac
		.setDescripcionCant(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_CAJERO
			.getDescripcionCant());
	detalleNac
		.setDescripcionMont(NacionalType.NACIONAL_DISPOSICION_EFECTIVO_CAJERO
			.getDescripcionMonto());
	detalleNac.setCantidad(0);
	detalleNac.setMonto("0");
	listaTrimestral.add(detalleNac);

	return listaTrimestral;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * Obtiene la lista internacional por defecto, es decir con todos sus
     * valores en 0
     * 
     * @param rs
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private List<DetalleTrimestral> obtenerListaInternacional() {
	List<DetalleTrimestral> listaTrimestral = new ArrayList<DetalleTrimestral>();

	DetalleTrimestral detalleInt = new DetalleTrimestral();
	detalleInt.setTipo(InternacionalType.INTERNACIONAL_TOTAL);
	detalleInt.setDescripcionCant(InternacionalType.INTERNACIONAL_TOTAL
		.getDescripcionCant());
	detalleInt.setDescripcionMont(InternacionalType.INTERNACIONAL_TOTAL
		.getDescripcionMont());
	detalleInt.setCantidad(0);
	detalleInt.setMonto("0");
	listaTrimestral.add(detalleInt);

	detalleInt = new DetalleTrimestral();
	detalleInt.setTipo(InternacionalType.INTERNACIONAL_CHIP);
	detalleInt.setDescripcionCant(InternacionalType.INTERNACIONAL_CHIP
		.getDescripcionCant());
	detalleInt.setDescripcionMont(InternacionalType.INTERNACIONAL_CHIP
		.getDescripcionMont());
	detalleInt.setCantidad(0);
	detalleInt.setMonto("0");
	listaTrimestral.add(detalleInt);

	detalleInt = new DetalleTrimestral();
	detalleInt
		.setTipo(InternacionalType.INTERNACIONAL_COMERCIO_ELECTRONICO);
	detalleInt
		.setDescripcionCant(InternacionalType.INTERNACIONAL_COMERCIO_ELECTRONICO
			.getDescripcionCant());
	detalleInt
		.setDescripcionMont(InternacionalType.INTERNACIONAL_COMERCIO_ELECTRONICO
			.getDescripcionMont());
	detalleInt.setCantidad(0);
	detalleInt.setMonto("0");
	listaTrimestral.add(detalleInt);

	detalleInt = new DetalleTrimestral();
	detalleInt
		.setTipo(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_EN_CAJA);
	detalleInt
		.setDescripcionCant(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_EN_CAJA
			.getDescripcionCant());
	detalleInt
		.setDescripcionMont(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_EN_CAJA
			.getDescripcionMont());
	detalleInt.setCantidad(0);
	detalleInt.setMonto("0");
	listaTrimestral.add(detalleInt);

	detalleInt = new DetalleTrimestral();
	detalleInt
		.setTipo(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_CAJERO);
	detalleInt
		.setDescripcionCant(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_CAJERO
			.getDescripcionCant());
	detalleInt
		.setDescripcionMont(InternacionalType.INTERNACIONAL_DISPOSICION_EFECT_CAJERO
			.getDescripcionMont());
	detalleInt.setCantidad(0);
	detalleInt.setMonto("0");
	listaTrimestral.add(detalleInt);

	return listaTrimestral;
    }

    // public static void main(String[] args) {
    // List<ReporteTrimestral> listaTrimestral = new
    // ArrayList<ReporteTrimestral>();
    // ReporteTrimestral repo = new ReporteTrimestral();
    // repo.setFechaTrimestre("03/2016");
    //
    // listaTrimestral.add(repo);
    //
    // repo = new ReporteTrimestral();
    // repo.setFechaTrimestre("02/2016");
    //
    // listaTrimestral.add(repo);
    //
    // repo = new ReporteTrimestral();
    //
    // listaTrimestral.add(repo);
    //
    // repo = new ReporteTrimestral();
    // repo.setFechaTrimestre("10/2016");
    //
    // listaTrimestral.add(repo);
    //
    // Collections.sort(listaTrimestral);
    //
    //
    // }

}
