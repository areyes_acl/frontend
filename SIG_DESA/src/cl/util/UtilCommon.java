package cl.util;

import org.apache.log4j.Logger;

public class UtilCommon {
	/**
	 * Var para imprimir en el log.
	 */
	private static final Logger LOGGER = Logger.getLogger(UtilCommon.class);

	public static boolean isNumber(final String obj) {

		try {
			if (obj == null) {
				return true;
			} else {
				Long.valueOf(obj);
				return true;
			}
		} catch (Exception ex) {

			return false;
		}
	}
}
