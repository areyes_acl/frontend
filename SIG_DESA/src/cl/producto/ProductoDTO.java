/**
 * ProductoDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.producto;

public class ProductoDTO  implements java.io.Serializable {
    private java.lang.String descripcionProducto;

    private java.lang.Long idProducto;

    public ProductoDTO() {
    }

    public ProductoDTO(
           java.lang.String descripcionProducto,
           java.lang.Long idProducto) {
           this.descripcionProducto = descripcionProducto;
           this.idProducto = idProducto;
    }


    /**
     * Gets the descripcionProducto value for this ProductoDTO.
     * 
     * @return descripcionProducto
     */
    public java.lang.String getDescripcionProducto() {
        return descripcionProducto;
    }


    /**
     * Sets the descripcionProducto value for this ProductoDTO.
     * 
     * @param descripcionProducto
     */
    public void setDescripcionProducto(java.lang.String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }


    /**
     * Gets the idProducto value for this ProductoDTO.
     * 
     * @return idProducto
     */
    public java.lang.Long getIdProducto() {
        return idProducto;
    }


    /**
     * Sets the idProducto value for this ProductoDTO.
     * 
     * @param idProducto
     */
    public void setIdProducto(java.lang.Long idProducto) {
        this.idProducto = idProducto;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductoDTO)) return false;
        ProductoDTO other = (ProductoDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.descripcionProducto==null && other.getDescripcionProducto()==null) || 
             (this.descripcionProducto!=null &&
              this.descripcionProducto.equals(other.getDescripcionProducto()))) &&
            ((this.idProducto==null && other.getIdProducto()==null) || 
             (this.idProducto!=null &&
              this.idProducto.equals(other.getIdProducto())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescripcionProducto() != null) {
            _hashCode += getDescripcionProducto().hashCode();
        }
        if (getIdProducto() != null) {
            _hashCode += getIdProducto().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductoDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://producto.cl/", "productoDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcionProducto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descripcionProducto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idProducto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idProducto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
