package cl.filter;

import java.io.Serializable;

import types.SearchType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class FiltroPerdida implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 1155500543634906672L;
	private String fechaInicio;
	private String fechaTermino;
	private String numeroTarjeta;
	private SearchType tipoBusqueda;

	public SearchType getTipoBusqueda() {
		return tipoBusqueda;
	}

	public void setTipoBusqueda(SearchType tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(String fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	@Override
	public String toString() {
		return "FiltroPerdida [fechaInicio=" + fechaInicio + ", fechaTermino="
				+ fechaTermino + ", numeroTarjeta=" + numeroTarjeta
				+ ", tipoBusqueda=" + tipoBusqueda + "]";
	}

}
