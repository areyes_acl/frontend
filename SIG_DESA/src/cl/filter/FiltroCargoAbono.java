package cl.filter;

import java.io.Serializable;

import types.CargoAbonoStatusType;
import types.CargoAbonoType;
import types.SearchType;

/**
 * 
 * @author afreire
 * 
 */
public class FiltroCargoAbono implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 5792719370115281263L;
    private String fechaDesde;
    private String fechaHasta;
    private String numeroTarjeta;
    private CargoAbonoType tipoCargo;
    private CargoAbonoStatusType estado;
    private SearchType tipoBusqueda;
    private String username;
    private Long sidUsuario;

    public SearchType getTipoBusqueda() {
	return tipoBusqueda;
    }

    public void setTipoBusqueda(SearchType tipoBusqueda) {
	this.tipoBusqueda = tipoBusqueda;
    }

    public CargoAbonoStatusType getEstado() {
	return estado;
    }

    public void setEstado(CargoAbonoStatusType estado) {
	this.estado = estado;
    }

    public String getFechaDesde() {
	return fechaDesde;
    }

    public void setFechaDesde(String fechaDesde) {
	this.fechaDesde = fechaDesde;
    }

    public String getFechaHasta() {
	return fechaHasta;
    }

    public void setFechaHasta(String fechaHasta) {
	this.fechaHasta = fechaHasta;
    }

    public String getNumeroTarjeta() {
	return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
	this.numeroTarjeta = numeroTarjeta;
    }

    public CargoAbonoType getTipoCargo() {
	return tipoCargo;
    }

    public void setTipoCargo(CargoAbonoType tipoCargo) {
	this.tipoCargo = tipoCargo;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public Long getSidUsuario() {
	return sidUsuario;
    }

    public void setSidUsuario(Long sidUsuario) {
	this.sidUsuario = sidUsuario;
    }

    @Override
    public String toString() {
	return "FiltroCargoAbono [fechaDesde=" + fechaDesde + ", fechaHasta="
		+ fechaHasta + ", numeroTarjeta=" + numeroTarjeta
		+ ", tipoCargo=" + tipoCargo + ", estado=" + estado
		+ ", tipoBusqueda=" + tipoBusqueda + ", username=" + username
		+ ", sidUsuario=" + sidUsuario + "]";
    }

}