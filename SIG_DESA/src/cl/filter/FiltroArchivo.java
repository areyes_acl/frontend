package cl.filter;

import java.io.Serializable;

import types.FileType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class FiltroArchivo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3210388980024692730L;
    private String fecha;
    private String starwith;
    private FileType tipoArchivo;

    public String getFecha() {
	return fecha;
    }

    public void setFecha(String fecha) {
	this.fecha = fecha;
    }

    public FileType getTipoArchivo() {
	return tipoArchivo;
    }

    public void setTipoArchivo(FileType tipoArchivo) {
	this.tipoArchivo = tipoArchivo;
    }

    public String getStarwith() {
	return starwith;
    }

    public void setStarwith(String starwith) {
	this.starwith = starwith;
    }

    @Override
    public String toString() {
	return "FiltroArchivo [fecha=" + fecha + ", starwith=" + starwith
		+ ", tipoArchivo=" + tipoArchivo + "]";
    }

}
