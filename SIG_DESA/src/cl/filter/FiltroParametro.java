/**
 * 
 */
package cl.filter;

import java.io.Serializable;

import types.EstadoType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class FiltroParametro implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2751731454264624591L;
    private String nombre;
    private EstadoType estado;

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public EstadoType getEstado() {
	return estado;
    }

    public void setEstado(EstadoType estado) {
	this.estado = estado;
    }

    public FiltroParametro(EstadoType estado) {
	super();
	this.estado = estado;
    }

    public FiltroParametro(String nombre, EstadoType estado) {
	super();
	this.nombre = nombre;
	this.estado = estado;
    }

    public FiltroParametro(String nombre) {
	super();
	this.nombre = nombre;
    }

    @Override
    public String toString() {
	return "FiltroParametro [nombre=" + nombre + ", estado=" + estado + "]";
    }

}