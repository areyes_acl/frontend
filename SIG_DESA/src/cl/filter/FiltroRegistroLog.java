package cl.filter;

import java.io.Serializable;

import types.LogTableType;
import types.SearchType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class FiltroRegistroLog implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 497350022297552408L;
    private String fecha;
    private LogTableType logTableType;
    private SearchType searchType;

    public String getFecha() {
	return fecha;
    }

    public void setFecha(String fecha) {
	this.fecha = fecha;
    }

    public LogTableType getLogTableType() {
	return logTableType;
    }

    public void setLogTableType(LogTableType logTableType) {
	this.logTableType = logTableType;
    }

    public SearchType getSearchType() {
	return searchType;
    }

    public void setSearchType(SearchType searchType) {
	this.searchType = searchType;
    }

    @Override
    public String toString() {
	return "FiltroRegistroLog [fecha=" + fecha + ", logTableType="
		+ logTableType + ", searchType=" + searchType + "]";
    }

}
