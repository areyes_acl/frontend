package cl.filter;

import java.io.Serializable;

import types.SearchType;
import types.StatusTransactionType;
import types.TransactionType;

/**
 * 
 * @author afreire
 * 
 */
public class FiltroTransacciones implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -1348213671264554401L;
    private String fechaDesde;
    private String fechaHasta;
    private String numeroTarjeta;
    private StatusTransactionType estado;
    private TransactionType tipoTransaccion;
    private SearchType tipoBusqueda;
    private String tipoFecha;
    private String username;
    private Long sidUsuario;
    private String patPass;
    private String operador;


    public String getFechaDesde() {
	return fechaDesde;
    }

    public void setFechaDesde(String fechaDesde) {
	this.fechaDesde = fechaDesde;
    }

    public String getFechaHasta() {
	return fechaHasta;
    }

    public void setFechaHasta(String fechaHasta) {
	this.fechaHasta = fechaHasta;
    }

    public String getNumeroTarjeta() {
	return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
	this.numeroTarjeta = numeroTarjeta;
    }

    public StatusTransactionType getEstado() {
	return estado;
    }

    public void setEstado(StatusTransactionType estado) {
	this.estado = estado;
    }

    public TransactionType getTipoTransaccion() {
	return tipoTransaccion;
    }

    public void setTipoTransaccion(TransactionType tipoTransaccion) {
	this.tipoTransaccion = tipoTransaccion;
    }

    public SearchType getTipoBusqueda() {
	return tipoBusqueda;
    }

    public void setTipoBusqueda(SearchType tipoBusqueda) {
	this.tipoBusqueda = tipoBusqueda;
    }

    public String getTipoFecha() {
	return tipoFecha;
    }

    public void setTipoFecha(String tipoFecha) {
	this.tipoFecha = tipoFecha;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public Long getSidUsuario() {
	return sidUsuario;
    }

    public void setSidUsuario(Long sidUsuario) {
	this.sidUsuario = sidUsuario;
    }
    
    public String getPatPass() {
        return patPass;
    }

    public void setPatPass(String patPass) {
        this.patPass = patPass;
    }
    
    public String getOperador() {
	return operador;
    }

    public void setOperador(String operador) {
	this.operador = operador;
    }

    @Override
    public String toString() {
	return "FiltroTransacciones [fechaDesde=" + fechaDesde
		+ ", fechaHasta=" + fechaHasta + ", numeroTarjeta="
		+ numeroTarjeta + ", estado=" + estado + ", tipoTransaccion="
		+ tipoTransaccion + ", tipoBusqueda=" + tipoBusqueda
		+ ", tipoFecha=" + tipoFecha + ", username=" + username
		+ ", sidUsuario=" + sidUsuario + ", patPass=" + patPass
		+ ", operador=" + operador + "]";
    }

}
