/**
 * SOAPWSListenerImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.service;

public class SOAPWSListenerImplServiceLocator extends org.apache.axis.client.Service implements cl.service.SOAPWSListenerImplService {

    public SOAPWSListenerImplServiceLocator() {
    }


    public SOAPWSListenerImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SOAPWSListenerImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SOAPWSListenerImplPort
    private java.lang.String SOAPWSListenerImplPort_address = "http://192.168.75.116:7011/MH/SOAPWS/Service";

    public java.lang.String getSOAPWSListenerImplPortAddress() {
        return SOAPWSListenerImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SOAPWSListenerImplPortWSDDServiceName = "SOAPWSListenerImplPort";

    public java.lang.String getSOAPWSListenerImplPortWSDDServiceName() {
        return SOAPWSListenerImplPortWSDDServiceName;
    }

    public void setSOAPWSListenerImplPortWSDDServiceName(java.lang.String name) {
        SOAPWSListenerImplPortWSDDServiceName = name;
    }

    public cl.service.SOAPWSListener getSOAPWSListenerImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SOAPWSListenerImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSOAPWSListenerImplPort(endpoint);
    }

    public cl.service.SOAPWSListener getSOAPWSListenerImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.service.SOAPWSListenerImplServiceSoapBindingStub _stub = new cl.service.SOAPWSListenerImplServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getSOAPWSListenerImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSOAPWSListenerImplPortEndpointAddress(java.lang.String address) {
        SOAPWSListenerImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.service.SOAPWSListener.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.service.SOAPWSListenerImplServiceSoapBindingStub _stub = new cl.service.SOAPWSListenerImplServiceSoapBindingStub(new java.net.URL(SOAPWSListenerImplPort_address), this);
                _stub.setPortName(getSOAPWSListenerImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SOAPWSListenerImplPort".equals(inputPortName)) {
            return getSOAPWSListenerImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://listeners.webtools.integrator.polaris.com/", "SOAPWSListenerImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://listeners.webtools.integrator.polaris.com/", "SOAPWSListenerImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SOAPWSListenerImplPort".equals(portName)) {
            setSOAPWSListenerImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
