/**
 * SOAPWSListenerImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.service;

public interface SOAPWSListenerImplService extends javax.xml.rpc.Service {
    public java.lang.String getSOAPWSListenerImplPortAddress();

    public cl.service.SOAPWSListener getSOAPWSListenerImplPort() throws javax.xml.rpc.ServiceException;

    public cl.service.SOAPWSListener getSOAPWSListenerImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
