package cl.service;

public class SOAPWSListenerProxy implements cl.service.SOAPWSListener {
  private String _endpoint = null;
  private cl.service.SOAPWSListener sOAPWSListener = null;
  
  public SOAPWSListenerProxy() {
    _initSOAPWSListenerProxy();
  }
  
  public SOAPWSListenerProxy(String endpoint) {
    _endpoint = endpoint;
    _initSOAPWSListenerProxy();
  }
  
  private void _initSOAPWSListenerProxy() {
    try {
      sOAPWSListener = (new cl.service.SOAPWSListenerImplServiceLocator()).getSOAPWSListenerImplPort();
      if (sOAPWSListener != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sOAPWSListener)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sOAPWSListener)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sOAPWSListener != null)
      ((javax.xml.rpc.Stub)sOAPWSListener)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.service.SOAPWSListener getSOAPWSListener() {
    if (sOAPWSListener == null)
      _initSOAPWSListenerProxy();
    return sOAPWSListener;
  }
  
  public java.lang.String processMessage(java.lang.String input) throws java.rmi.RemoteException{
    if (sOAPWSListener == null)
      _initSOAPWSListenerProxy();
    return sOAPWSListener.processMessage(input);
  }
  
  
}