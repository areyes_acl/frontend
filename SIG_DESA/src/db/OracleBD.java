package db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import oracle.jdbc.pool.OracleDataSource;

import org.apache.log4j.Logger;

import properties.PropertiesConfiguration;
import types.PropertyEntryTypes;

public class OracleBD {

    private Connection conexion;
    private ResultSet resultado;
    private Statement sentencia;

    static final Logger log = Logger.getLogger(OracleBD.class);
    static final String LOG_PROPERTIES_FILE = "lib/Log4J.properties";

    public OracleBD() {

    }

    public Connection getConexion() {
	return conexion;
    }

    public void setConexion(Connection conexion) {
	this.conexion = conexion;
    }

    public ResultSet getResultado() {
	return resultado;
    }

    public void setResultado(ResultSet resultado) {
	this.resultado = resultado;
    }

    /*****************************************************/
    /* Metodo que devuelve registros segun sentencia */
    /* select. Devuelve conjunto de ResultSet */
    /*****************************************************/
    public ResultSet consultar(String sql) {
	resultado = null;
	try {
	    log.debug("Ingresando a conexion");
	    Connection conex = getConexion();
	    sentencia = null;
	    sentencia = conex.createStatement(
		    ResultSet.TYPE_SCROLL_INSENSITIVE,
		    ResultSet.CONCUR_READ_ONLY);
	    resultado = sentencia.executeQuery(sql);
	    log.debug("Ingresando a conexion");
	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error(e.getMessage());
	    return null;
	}
	return resultado;
    }

    /*****************************************************/
    /* Metodo que devuelve la coneccion de la BD */
    /*                                                   */
    /*****************************************************/
    // public Connection conectar(String servidor, String puerto,
    // String sig, String usuario, String clave)//

    public Connection conectar() {
	try {
	    log.debug("Method: conectar - Cargar parametros de conexi�n");
	    String jndi = getInstanceJNDI();
	    log.debug("JNDI : " + jndi);

	    Context initContext = new InitialContext();
	    // Locale.setDefault(Locale.ENGLISH);
	    DataSource ds = (DataSource) initContext.lookup(jndi);

	    conexion = ds.getConnection();
	    if (conexion != null) {
		log.debug("Method: conectar - Conexion exitosa!");
	    } else {
		log.warn("Method: conectar - Conexion fallida!");
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error(e.getMessage());
	}
	return conexion;
    }

    /*****************************************************/
    /* Metodo para ejecutar INSERT, UPDATE, DELETE */
    /*                                                   */
    /*****************************************************/
    public boolean ejecutar(String sql) {
	try {
	    log.debug("Method: ejecutar - Ejecuci�n de consultas insert, update, delete");
	    Statement sentenciaIUD;
	    sentenciaIUD = getConexion().createStatement(
		    ResultSet.TYPE_SCROLL_INSENSITIVE,
		    ResultSet.CONCUR_READ_ONLY);
	    sentenciaIUD.executeUpdate(sql);
	    getConexion().commit();
	    sentenciaIUD.close();
	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error(e.getMessage());
	    return false;
	}
	return true;
    }

    public void cerrarStatement(Statement st) {
	try {
	    st.close();
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    e.printStackTrace();
	}
    }

    public void cerrarCallableStatement(CallableStatement cst) {
	try {
	    cst.close();
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	    e.printStackTrace();
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public boolean cerrarResultSet() {

	try {
	    if (resultado != null && !resultado.isClosed()) {
		resultado.close();
	    }
	    if (sentencia != null && !sentencia.isClosed()) {
		sentencia.close();
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error(e.getMessage());
	    return false;
	}
	return true;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public boolean cerrarConexionBD() {
	try {
	    log.debug("Method: cerrarConexionBD - Cerra conexion BD");
	    if (conexion != null && !conexion.isClosed()) {
		conexion.close();
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error(e.getMessage());
	    return false;
	}
	return true;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public boolean rollback() {
	try {
	    log.debug("Method: rollback - Ejecici�n rollback");
	    conexion.rollback();
	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error(e.getMessage());
	    return false;
	}
	return true;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * IMPORTANTE : ESTE METODO SE DEBE UTILIZAR SOLO PARA REALIZAR TEST O
     * CORRER MAIN PARA PRUEBAS UNITARIAS
     * 
     * @return
     * @since 1.X
     */
    private Connection getConnection() {
	// Configuraci�n del DataSource
	String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
	String user = "SIG";
	String password = "SIG";
	OracleDataSource oracleDS = null;
	try {
	    oracleDS = new OracleDataSource();
	    oracleDS.setURL(url);
	    oracleDS.setUser(user);
	    oracleDS.setPassword(password);
	    return oracleDS.getConnection();
	} catch (SQLException e) {
	    log.error(e);
	}
	return null;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private String getInstanceJNDI() {
	Properties prop = PropertiesConfiguration.getProperty();
	return prop.getProperty(PropertyEntryTypes.JNDI.getKey());

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param oracleBD
     * @param rs
     * @param conexion
     * @param stmtProcedure
     * @since 1.X
     */
    public void cerrarConexiones(OracleBD oracleBD, ResultSet rs,
	    Connection conexion, CallableStatement stmtProcedure) {

	try {
	    if (rs != null && !rs.isClosed()) {
		rs.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

	try {
	    if (conexion != null && !conexion.isClosed()) {
		conexion.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

	try {
	    if (stmtProcedure != null && !stmtProcedure.isClosed()) {
		stmtProcedure.close();
	    }
	} catch (SQLException e) {
	    log.error(e.getMessage(), e);
	}

	if (this.cerrarResultSet() == false) {
	    if (this.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	} else {
	    if (this.cerrarConexionBD() == false) {
		log.error("Error: No se pudo cerrar conexion a la base de datos.");
	    }
	}

    }
}