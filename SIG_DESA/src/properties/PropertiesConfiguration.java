package properties;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class PropertiesConfiguration {
    
    /** Constante de directorio aplicaci�n. */
    private static final String DIRAPPCONFIG = "dirAppConfig";
    /** Constante de serverPath. */
    private static final String USERDIR = "user.dir";
    /** Constante de properties config **/
    private static final String CONFIG_PROPERTY_ENTRY_NAME = "sigAppConfig";
    private static final Properties prop = new Properties();
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param context
     * @since 1.X
     */
    public static void loadApplicationProperties( ServletContext context ) {
        cargaAppConfig( context );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @param context
     * @since 1.X
     */
    private static void cargaAppConfig( ServletContext context ) {
        InputStream input = null;
        String serverPath = System.getProperty( USERDIR ).replaceAll( "\\\\",
                "/" );
        FileInputStream fileInput = null;
        
        try {
            String file = context.getInitParameter( CONFIG_PROPERTY_ENTRY_NAME );
            String dirApp = context.getInitParameter( DIRAPPCONFIG );
            
            fileInput = new FileInputStream( serverPath + dirApp + file );
            System.out.println("Revisar -------------" );
            System.out.println(serverPath + dirApp + file );
            input = new BufferedInputStream( fileInput );
            System.out.println(serverPath + dirApp + file);
            // load a properties file
            prop.load( input );
            
        }
        catch ( IOException ex ) {
            ex.printStackTrace();
        }
        finally {
            if ( input != null ) {
                try {
                    input.close();
                }
                catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public static Properties getProperty() {
        return prop;
    }
    
}
