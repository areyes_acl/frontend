package properties;

import java.util.Calendar;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL.</B>
 */
public class ConfigListener implements ServletContextListener {
	private static final Logger log = Logger.getLogger(ConfigListener.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @param contextListener
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 * @since 1.X
	 */
	@Override
	public void contextDestroyed(ServletContextEvent contextListener) {
		log.info("======= SE DETIENE LA APP ========");

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @param contextListener
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 * @since 1.X
	 */
	@Override
	public void contextInitialized(ServletContextEvent contextListener) {
		log.info("======= CARGANDO ARCHIVO DE CONFIGURACION DE LA APP ========");
		ServletContext context = contextListener.getServletContext();
		PropertiesConfiguration.loadApplicationProperties(context);
		log.info("======= Se ha inciado la App la fecha  : "
				+ Calendar.getInstance().getTime() + "  ========");
	}

}
