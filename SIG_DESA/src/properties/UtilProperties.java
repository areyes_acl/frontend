package properties;

import properties.PropertiesConfiguration;

public class UtilProperties {
    private UtilProperties(){}
    
    public static String getProperty(String propiedad){
	return PropertiesConfiguration.getProperty().getProperty(propiedad);
    }

}
