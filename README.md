## Descripción de las principales ramas del proyecto:
```bash
- master: Contiene los proyectos AVT (Avance con transferencia), PATPASS, TDI y es la rama que se entregó al cliente.
- develop: Hija de master. Contiene el desarrollo del proyecto Visa Internacional (pero con la version antigua, poco vistosa del soft)
- fase2: Hija de develop. Contiene el desarrollo de Visa Internacional con vistas mejoradas con bootstrap, fue la version que se entrego al cliente.
- fase3: Hija de fase2. Contiene nuevas mejoras visuales con cambio de plantilla pero necesita ser pasada por QA, pero deberia ser la version mas vistosa.
- DEMO_SGIC: Hija de fase 2. Elimina los textos referentes a ABCDIN, rama para la demo de Visa.
```
## Arbol de dependencia de ramas:
```bash
- master (sin hijas)
- develop
  - leonel
  - hector
  - camilo1
- fase2
  - hector-fasedos
  - camilo
  - leonelf2
- fase3
  - hectorf3
  - camilof3
- DEMO_SGIC
```
